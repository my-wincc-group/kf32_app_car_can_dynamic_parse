版本：V0.0.0.S0C0

从内部FLASH中启动地址配置，需要2部操作；

步骤1：需要修改KF32A152MQV.ld中的第9行信息即可实现：

设置bootloader在内部FLASH启动地址
flash      : ORIGIN =  0x00000000, LENGTH = 0x00010000
注：起始地址为0，大小为64KB。

设置应用程序在内部FLASH启动地址
flash      : ORIGIN =  0x00010000, LENGTH = 0x00032000
注：起始地址为0x10000，大小为200KB。

步骤2：修了个如上配置后，仍需需要如下操作才能使其生效
项目属性-C/C++构建-设置-工具设置-通过设定-芯片脚本文件
将内容
-T"${CHIP_NAME_SCRIPT}"
改成
 -T"../KF32A152MQV.ld"



默认配置
flash      : ORIGIN =  0x00000000, LENGTH = 0x00080000
注：起始地址为0，大小为512KB。

---------------------------------查询指令-----------------------------------
通过USART1设置，115200，8，1，0，0
(1) <ZKHYCHK*VERSION>							获取版本号
(2) <ZKHYSET*RUNVERSION:V1.1.0.S1C6>			设置运行版本号
(3) <ZKHYSET*UPVERSION:V1.1.0.S1C6>				设置升级版本号
(4)	<ZKHYSET*DEVICESN:SN号>  					设置SN号
(5) <ZKHYCHK*DEVICESN> 							查询SN号
(6) <ZKHYSET*PACKAGESIZE:V1.1.0.S1C6,32000> 	控制串口升级
(7) <ZKHYSET*ERASESECTOR>						清空配置参数，可理解为恢复到出厂设置，有风险需谨慎 ,功能已被阉割
(8) <ZKHYCHK*UPDATE:NEWEST>						OTA检测更新，若有更新会直接更新
(9) <ZKHYSET*UPDATE:V1.1.0.L1C6,60436>			手动设置OTA升级到固定版本，参数1：版本号；参数2：升级包大小
(10) <ZKHYSET*DEVICEPN:PN号>						设置PN号
(11) <ZKHYCHK*DEVICEPN>							获取PN号
