/****************************************************************************************
 *
 * File Name: kf_it.c
 * Project Name: kungfu_aebs
 * Version: v1.0
 * Date: 2021-06-16- 16:51:48
 * Author: shuai
 * 
 ****************************************************************************************/
//#include<KF32A151MQV.h>
#include "system_init.h"
#include "canhl.h"
#include "usart.h"
#include "EC200U.h"
#include "common.h"
#include "usart_upgrade.h"
#include "_4g_upgrade.h"
#include "_4g_data_upload.h"
#include "at_parse_alg_para.h"
#include "_4g_para_config.h"
#include "can_task.h"
#include "./short_range_radar/srr_can.h"
#include "imu.h"
#include "parse.h"
#include "serial_v1.h"
uint8_t speed_level 	= 0;
uint32_t first_time 	= 0;
uint32_t second_time 	= 0;

//uint32_t Time14_CNT;
uint8_t Time15_CNT;

uint8_t test_ttt = 0;
//asm(".include		\"KF32A151MQV.inc\"	");	 

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	
	test_ttt = 0;
}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************	

void __attribute__((interrupt)) _HardFault_exception (void)
{
	test_ttt = 1;
}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{
	test_ttt = 2;
}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************	
//void __attribute__((interrupt)) _SVC_exception (void)
//{

//}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************	
//void __attribute__((interrupt)) _SoftSV_exception (void)
//{

//}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************	
//void __attribute__((interrupt)) _SysTick_exception (void)
//{
	
//}

//*****************************************************************************************
//                              CAN0 摄像头can
//*****************************************************************************************	//
void __attribute__((interrupt))_CAN0_exception (void)
{
	uint8_t i;
	static uint8_t Receice_addr=0x00;//接收RAM偏移地址
	CAN_MessageTypeDef CAN_MessageStructure;//接收报文结构体
//	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	//struct can_frame rx_frame;
	/* 清CAN中断标志 */
	INT_Clear_Interrupt_Flag(INT_CAN0);

	/* 判断是否为CAN发送标志 */
	if(CAN_Get_INT_Flag(CAN0_SFR,CAN_INT_TRANSMIT) != RESET)
	{
		CAN_Clear_INT_Flag(CAN0_SFR,CAN_INT_TRANSMIT);
		CAN0_SFR->CTLR &= ~0x300;//清除发送使能位

	}

	/* 判断总线错误标志 */
	if(CAN_Get_INT_Flag(CAN0_SFR,CAN_INT_BUS_ERROR) != RESET)
	{
		CAN_Clear_INT_Flag(CAN0_SFR,CAN_INT_BUS_ERROR);
		CAN0_SFR->CTLR &= ~0x300; //总线错误不重发
	}

	/* 判断是否为CAN接收标志 */
	if(CAN_Get_INT_Flag(CAN0_SFR,CAN_INT_RECEIVE) != RESET)
	{
//		GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_8);
		/* 接收RAM数据 */
		CAN_Receive_Message_Configuration(CAN0_SFR,Receice_addr,&CAN_MessageStructure);
		/* RAM地址自跳 */
		Receice_addr+=0x10;
		/* 释放一次计数器 */
		CAN_Release_Receive_Buffer(CAN0_SFR,1);

		if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_SFF)//标准帧
		{
			can0_rx_frame.TargetID = CAN_MessageStructure.m_StandardID;			 //标准帧ID
		}
		else if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_EFF)//扩展帧
		{
			can0_rx_frame.TargetID = CAN_MessageStructure.m_ExtendedID;			 //扩展帧ID
		}

		if(CAN_MessageStructure.m_RemoteTransmit != CAN_DATA_FRAME)//远程帧
		{
			can0_rx_frame. MsgType = CAN_MessageStructure.m_RemoteTransmit;
			can0_rx_frame.data[0] =0xAA;
			//用户代码
			//USART_Send(USART2_SFR,ReceiveData,1);

		}
		else  //数据帧
		{

			can0_rx_frame.lenth = CAN_MessageStructure.m_DataLength;			    	 //长度
			can0_rx_frame.RmtFrm = CAN_MessageStructure.m_FrameFormat;             	 //帧格式
			can0_rx_frame. MsgType= CAN_MessageStructure.m_RemoteTransmit;		 	 //帧类型

			for(i=0;i<CAN_MessageStructure.m_DataLength;i++)
			{
				can0_rx_frame.data[i] = CAN_MessageStructure.m_Data[i];
			}
			/*
			if(CAN0_RX_COUNT<(CAN_BUFFER_MAX-1))
				CAN0_RX_COUNT++;
			else
				CAN0_RX_COUNT=0;
			*/
			//fprintf(USART1_STREAM, "*\r\n");
			can0_receive(can0_rx_frame);

		}
	}

}

//*****************************************************************************************
//                              CAN1 毫米波雷达can
//*****************************************************************************************

void __attribute__((interrupt))_CAN1_exception (void)
{
	uint8_t i;
	static uint8_t Receice_addr=0x00;//接收RAM偏移地址
	CAN_MessageTypeDef CAN_MessageStructure;//接收报文结构体
//	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	//struct can_frame rx_frame;
	/* 清CAN中断标志 */
	INT_Clear_Interrupt_Flag(INT_CAN1);

	/* 判断是否为CAN发送标志 */
	if(CAN_Get_INT_Flag(CAN1_SFR,CAN_INT_TRANSMIT) != RESET)
	{
		CAN_Clear_INT_Flag(CAN1_SFR,CAN_INT_TRANSMIT);
		CAN1_SFR->CTLR &= ~0x300;//清除发送使能位

	}

	/* 判断总线错误标志 */
	if(CAN_Get_INT_Flag(CAN1_SFR,CAN_INT_BUS_ERROR) != RESET)
	{
		CAN_Clear_INT_Flag(CAN1_SFR,CAN_INT_BUS_ERROR);
		CAN1_SFR->CTLR &= ~0x300; //总线错误不重发
	}

	/* 判断是否为CAN接收标志 */
	if(CAN_Get_INT_Flag(CAN1_SFR,CAN_INT_RECEIVE) != RESET)
	{
//		GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_8);
		/* 接收RAM数据 */
		CAN_Receive_Message_Configuration(CAN1_SFR,Receice_addr,&CAN_MessageStructure);
		/* RAM地址自跳 */
		Receice_addr+=0x10;
		/* 释放一次计数器 */
		CAN_Release_Receive_Buffer(CAN1_SFR,1);

		if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_SFF)//标准帧
		{
			can1_rx_frame.TargetID = CAN_MessageStructure.m_StandardID;			 //标准帧ID
		}
		else if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_EFF)//扩展帧
		{
			can1_rx_frame.TargetID = CAN_MessageStructure.m_ExtendedID;			 //扩展帧ID
		}

		if(CAN_MessageStructure.m_RemoteTransmit != CAN_DATA_FRAME)//远程帧
		{
			can1_rx_frame. MsgType = CAN_MessageStructure.m_RemoteTransmit;
			can1_rx_frame.data[0] =0xAA;
			//用户代码
			//USART_Send(USART2_SFR,ReceiveData,1);

		}
		else  //数据帧
		{
			can1_rx_frame.lenth = CAN_MessageStructure.m_DataLength;			    	 //长度
			can1_rx_frame.RmtFrm = CAN_MessageStructure.m_FrameFormat;             	 //帧格式
			can1_rx_frame. MsgType= CAN_MessageStructure.m_RemoteTransmit;		 	 //帧类型

		  //fprintf(USART1_STREAM,"CAN1_Uradar %x\r\n", can1_rx_frame.TargetID);
			/* 获取数据 */
			for(i=0;i<CAN_MessageStructure.m_DataLength;i++)
			{
				can1_rx_frame.data[i] = CAN_MessageStructure.m_Data[i];
			}

			if((can1_rx_frame.TargetID == 0x750) ||
        (can1_rx_frame.TargetID == 0x751) ||
        (can1_rx_frame.TargetID == 0x752) ||
        (can1_rx_frame.TargetID == 0x753) ||
        (can1_rx_frame.TargetID == 0x754) ||
        (can1_rx_frame.TargetID == 0x755) ||
        (can1_rx_frame.TargetID == 0x756) ||
        (can1_rx_frame.TargetID == 0x757) ||
        (can1_rx_frame.TargetID == 0x758))// Angle Radar receive
			{
      //gcz 2022-08-10合并离线状态判断所需程序代码段
			  srr_receive(&can1_rx_frame);
			}
			//if(CAN1_RX_COUNT<(CAN_BUFFER_MAX-1))
			//	CAN1_RX_COUNT++;
			//else
			//	CAN1_RX_COUNT=0;
			//can3_receive(can1_rx_frame);
			//can1_receive(can1_rx_frame);
			//Ureader_receive(can1_rx_frame);
			//
		}
	}

}

//*****************************************************************************************
//                              CAN2整车can
//*****************************************************************************************	//

void __attribute__((interrupt))_CAN2_exception (void)
{
	uint8_t i;
	static uint8_t Receice_addr=0x00;//接收RAM偏移地址
	CAN_MessageTypeDef CAN_MessageStructure;//接收报文结构体
//	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	//struct can_frame rx_frame;
	/* 清CAN中断标志 */
	INT_Clear_Interrupt_Flag(INT_CAN2);

	/* 判断是否为CAN发送标志 */
	if(CAN_Get_INT_Flag(CAN2_SFR,CAN_INT_TRANSMIT) != RESET)
	{
		CAN_Clear_INT_Flag(CAN2_SFR,CAN_INT_TRANSMIT);
		CAN2_SFR->CTLR &= ~0x300;//清除发送使能位
	}

	/* 判断总线错误标志 */
	if(CAN_Get_INT_Flag(CAN2_SFR,CAN_INT_BUS_ERROR) != RESET)
	{
		CAN_Clear_INT_Flag(CAN2_SFR,CAN_INT_BUS_ERROR);
		CAN2_SFR->CTLR &= ~0x300; //总线错误不重发
	}

	/* 判断是否为CAN接收标志 */
	if(CAN_Get_INT_Flag(CAN2_SFR,CAN_INT_RECEIVE) != RESET)
	{
//		GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_8);
		/* 接收RAM数据 */
		CAN_Receive_Message_Configuration(CAN2_SFR,Receice_addr,&CAN_MessageStructure);
		/* RAM地址自跳 */
		Receice_addr+=0x10;
		/* 释放一次计数器 */
		CAN_Release_Receive_Buffer(CAN2_SFR,1);

		if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_SFF)//标准帧
		{
			can2_rx_frame.TargetID = CAN_MessageStructure.m_StandardID;			 //标准帧ID
		}
		else if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_EFF)//扩展帧
		{
			can2_rx_frame.TargetID = CAN_MessageStructure.m_ExtendedID;			 //扩展帧ID
		}

		if(CAN_MessageStructure.m_RemoteTransmit != CAN_DATA_FRAME)//远程帧
		{
			can2_rx_frame. MsgType = CAN_MessageStructure.m_RemoteTransmit;
			can2_rx_frame.data[0] =0xAA;
			//用户代码
			//USART_Send(USART2_SFR,ReceiveData,1);

		}
		else  //数据帧
		{
			can2_rx_frame.lenth = CAN_MessageStructure.m_DataLength;			    	 //长度
			can2_rx_frame.RmtFrm = CAN_MessageStructure.m_FrameFormat;             	 //帧格式
			can2_rx_frame. MsgType= CAN_MessageStructure.m_RemoteTransmit;		 	 //帧类型
			/* 获取数据 */
			for(i=0;i<CAN_MessageStructure.m_DataLength;i++)
			{
				can2_rx_frame.data[i] = CAN_MessageStructure.m_Data[i];
			}
			can2_receive(can2_rx_frame);

			Imu_receiveData(&can2_rx_frame);
		}
	}

}

//***********************************************************************************
//								CAN3比例阀can
//**********************************************************************************
void __attribute__((interrupt))_CAN3_exception (void)
{
	uint8_t i;
	static uint8_t Receice_addr=0x00;//接收RAM偏移地址
	CAN_MessageTypeDef CAN_MessageStructure;//接收报文结构体
//	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	//struct can_frame rx_frame;
	/* 清CAN中断标志 */
	INT_Clear_Interrupt_Flag(INT_CAN3);

	/* 判断是否为CAN发送标志 */
	if(CAN_Get_INT_Flag(CAN3_SFR,CAN_INT_TRANSMIT) != RESET)
	{
		CAN_Clear_INT_Flag(CAN3_SFR,CAN_INT_TRANSMIT);
		CAN3_SFR->CTLR &= ~0x300;//清除发送使能位
	}

	/* 判断总线错误标志 */
	if(CAN_Get_INT_Flag(CAN3_SFR,CAN_INT_BUS_ERROR) != RESET)
	{
		CAN_Clear_INT_Flag(CAN3_SFR,CAN_INT_BUS_ERROR);
		CAN3_SFR->CTLR &= ~0x300; //总线错误不重发
	}

	/* 判断是否为CAN接收标志 */
	if(CAN_Get_INT_Flag(CAN3_SFR,CAN_INT_RECEIVE) != RESET)
	{
//		GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_8);
		/* 接收RAM数据 */
		CAN_Receive_Message_Configuration(CAN3_SFR,Receice_addr,&CAN_MessageStructure);
		/* RAM地址自跳 */
		Receice_addr+=0x10;
		/* 释放一次计数器 */
		CAN_Release_Receive_Buffer(CAN3_SFR,1);

		if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_SFF)//标准帧
		{
			can3_rx_frame.TargetID = CAN_MessageStructure.m_StandardID;			 //标准帧ID
		}
		else if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_EFF)//扩展帧
		{
			can3_rx_frame.TargetID = CAN_MessageStructure.m_ExtendedID;			 //扩展帧ID
		}

		if(CAN_MessageStructure.m_RemoteTransmit != CAN_DATA_FRAME)//远程帧
		{
			can3_rx_frame. MsgType = CAN_MessageStructure.m_RemoteTransmit;
			can3_rx_frame.data[0] =0xAA;
			//用户代码
			//USART_Send(USART2_SFR,ReceiveData,1);

		}
		else  //数据帧
		{
			can3_rx_frame.lenth = CAN_MessageStructure.m_DataLength;			    	 //长度
			can3_rx_frame.RmtFrm = CAN_MessageStructure.m_FrameFormat;             	 //帧格式
			can3_rx_frame. MsgType= CAN_MessageStructure.m_RemoteTransmit;		 	 //帧类型
			/* 获取数据 */
			for(i=0;i<CAN_MessageStructure.m_DataLength;i++)
			{
				can3_rx_frame.data[i] = CAN_MessageStructure.m_Data[i];
			}
			//if(CAN3_RX_COUNT<(CAN_BUFFER_MAX-1))
			//	CAN3_RX_COUNT++;
			//else
			//	CAN3_RX_COUNT=0;
			can3_receive(can3_rx_frame);
		}
	}

}

//*****************************************************************************************
//                              CAN4 小屏幕
//*****************************************************************************************	//
void __attribute__((interrupt))_CAN4_exception (void)
{
	uint8_t i;
	static uint8_t Receice_addr=0x00;//接收RAM偏移地址
	CAN_MessageTypeDef CAN_MessageStructure;//接收报文结构体
//	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	//struct can_frame rx_frame;
	// 清CAN中断标志
	INT_Clear_Interrupt_Flag(INT_CAN4);

	// 判断是否为CAN发送标志
	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_TRANSMIT) != RESET)
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_TRANSMIT);
		CAN4_SFR->CTLR &= ~0x300;//清除发送使能位
	}

	// 判断总线错误标志
	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_BUS_ERROR) != RESET)
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_BUS_ERROR);
		CAN4_SFR->CTLR &= ~0x300; //总线错误不重发
	}

	// 判断是否为CAN接收标志
	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_RECEIVE) != RESET)
	{
//		GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_8);
		// 接收RAM数据
		CAN_Receive_Message_Configuration(CAN4_SFR,Receice_addr,&CAN_MessageStructure);
		// RAM地址自跳
		Receice_addr+=0x10;
		// 释放一次计数器
		CAN_Release_Receive_Buffer(CAN4_SFR,1);

		if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_SFF)//标准帧
		{
			can4_rx_frame.TargetID = CAN_MessageStructure.m_StandardID;			 //标准帧ID
		}
		else if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_EFF)//扩展帧
		{
			can4_rx_frame.TargetID = CAN_MessageStructure.m_ExtendedID;			 //扩展帧ID
		}

		if(CAN_MessageStructure.m_RemoteTransmit != CAN_DATA_FRAME)//远程帧
		{
			can4_rx_frame. MsgType = CAN_MessageStructure.m_RemoteTransmit;
			can4_rx_frame.data[0] =0xAA;
			//用户代码
			//USART_Send(USART2_SFR,ReceiveData,1);

		}
		else  //数据帧
		{
			can4_rx_frame.lenth = CAN_MessageStructure.m_DataLength;			    	//长度
			can4_rx_frame.RmtFrm = CAN_MessageStructure.m_FrameFormat;             	 //帧格式
			can4_rx_frame. MsgType= CAN_MessageStructure.m_RemoteTransmit;		 	 //帧类型
			// 获取数据
			for(i=0;i<CAN_MessageStructure.m_DataLength;i++)
			{
				can4_rx_frame.data[i] = CAN_MessageStructure.m_Data[i];
			}
			// 小屏幕 OTA升级
//			Analysis_CAN4_Screen_OTA_CMD(can4_rx_frame.TargetID, can4_rx_frame.data);	// add lmz 20220620
			//if(CAN4_RX_COUNT<(CAN_BUFFER_MAX-1))
			//	CAN4_RX_COUNT++;
			//else
			//	CAN4_RX_COUNT=0;
			//fprintf(BLUETOOTH_STREAM,"can4_receive");
			can4_receive(can4_rx_frame);
			//Ureader_receive(can1_rx_frame);
		}
	}

}

//*****************************************************************************************
//                              CAN5 超声波雷达can
//*****************************************************************************************
void __attribute__((interrupt))_CAN5_exception (void)
{
	uint8_t i;
	static uint8_t Receice_addr=0x00;//接收RAM偏移地址
	CAN_MessageTypeDef CAN_MessageStructure;//接收报文结构体
//	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	//struct can_frame rx_frame;
	/* 清CAN中断标志 */
	INT_Clear_Interrupt_Flag(INT_CAN5);

	/* 判断是否为CAN发送标志 */
	if(CAN_Get_INT_Flag(CAN5_SFR,CAN_INT_TRANSMIT) != RESET)
	{
		CAN_Clear_INT_Flag(CAN5_SFR,CAN_INT_TRANSMIT);
		CAN5_SFR->CTLR &= ~0x300;//清除发送使能位
	}

	/* 判断总线错误标志 */
	if(CAN_Get_INT_Flag(CAN5_SFR,CAN_INT_BUS_ERROR) != RESET)
	{
		CAN_Clear_INT_Flag(CAN5_SFR,CAN_INT_BUS_ERROR);
		CAN5_SFR->CTLR &= ~0x300; //总线错误不重发
	}

	/* 判断是否为CAN接收标志 */
	if(CAN_Get_INT_Flag(CAN5_SFR,CAN_INT_RECEIVE) != RESET)
	{
//		GPIO_Toggle_Output_Data_Config (GPIOB_SFR,GPIO_PIN_MASK_8);
		/* 接收RAM数据 */
		CAN_Receive_Message_Configuration(CAN5_SFR,Receice_addr,&CAN_MessageStructure);
		/* RAM地址自跳 */
		Receice_addr+=0x10;
		/* 释放一次计数器 */
		CAN_Release_Receive_Buffer(CAN5_SFR,1);

		if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_SFF)//标准帧
		{
			can5_rx_frame.TargetID = CAN_MessageStructure.m_StandardID;			 //标准帧ID
		}
		else if(CAN_MessageStructure.m_FrameFormat==CAN_FRAME_FORMAT_EFF)//扩展帧
		{
			can5_rx_frame.TargetID = CAN_MessageStructure.m_ExtendedID;			 //扩展帧ID
		}

		if(CAN_MessageStructure.m_RemoteTransmit != CAN_DATA_FRAME)//远程帧
		{
			can5_rx_frame. MsgType = CAN_MessageStructure.m_RemoteTransmit;
			can5_rx_frame.data[0] =0xAA;
			//用户代码
			//USART_Send(USART2_SFR,ReceiveData,1);

		}
		else  //数据帧
		{
			can5_rx_frame.lenth = CAN_MessageStructure.m_DataLength;			    	 //长度
			can5_rx_frame.RmtFrm = CAN_MessageStructure.m_FrameFormat;             	 //帧格式
			can5_rx_frame. MsgType= CAN_MessageStructure.m_RemoteTransmit;		 	 //帧类型

			//fprintf(USART1_STREAM,"CAN5_Uradar %x\r\n", can5_rx_frame.TargetID);

			/* 获取数据 */
			for(uint8_t i=0;i<CAN_MessageStructure.m_DataLength;i++)
			{
				can5_rx_frame.data[i] = CAN_MessageStructure.m_Data[i];
			}
			//if(CAN4_RX_COUNT<(CAN_BUFFER_MAX-1))
			//	CAN4_RX_COUNT++;
			//else
			//	CAN4_RX_COUNT=0;
			//can5_receive(can5_rx_frame);
			if((can5_rx_frame.TargetID == 0x700) ||
				(can5_rx_frame.TargetID == 0x701) ||
				(can5_rx_frame.TargetID == 0x702) ||
				(can5_rx_frame.TargetID == 0x703) ||
				(can5_rx_frame.TargetID == 0x318))
			{
				//fprintf(USART1_STREAM,"CAN5_Uradar\r\n");
				Ureader_receive(can5_rx_frame);
			}else
			{ }
		}
	}
}

//*****************************************************************************************
//                              UART0 - EC200U-CN 4G模块
//*****************************************************************************************	//

void __attribute__((interrupt)) _DMA0_exception (void)
{

//	if(DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR,USART1_RXDMA_CHAN))
//	{
//		DMA0Receiveover_flag = 1;
//		DMA_Clear_INT_Flag(DMA0_SFR,USART1_RXDMA_CHAN,DMA_INT_FINISH_TRANSFER);
//	}
//	if(DMA_Get_Half_Transfer_INT_Flag (DMA0_SFR,USART1_RXDMA_CHAN)){
//		DMA0ReceiveHalf_flag = 1;
//		DMA_Clear_INT_Flag(DMA0_SFR,USART1_RXDMA_CHAN,DMA_INT_HALF_TRANSFER);
//
//	}
//	if(DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR,USART1_TXDMA_CHAN))
//	{
//		DMA0Txover_flag = 1;
////		DMA_Clear_INT_Flag(DMA0_SFR,USART1_TXDMA_CHAN,DMA_INT_FINISH_TRANSFER);
//	}
}

//*****************************************************************************************
//                              UART0中断函数
//*****************************************************************************************	//
void __attribute__((interrupt))_USART0_exception (void)
{
	if (USART_Get_Receive_Frame_Idel_Flag(USART0_SFR))
	{
		USART_Clear_IDLE_INT_Flag(USART0_SFR);
		usart0_dma.recv_idle_flag = 1;
	}
}
//*****************************************************************************************
//                              UART1 - 用户调试串口--线刷升级
//*****************************************************************************************	//
//char *btStrx = NULL;
void __attribute__((interrupt))_USART1_exception (void)
{
	if(USART_Get_Receive_Frame_Idel_Flag(USART1_SFR))
	{
		USART_Clear_IDLE_INT_Flag(USART1_SFR);
		usart1_dma.recv_idle_flag = 1;

	}
}

//*****************************************************************************************
//                              USART2 - 复用：最初接才库的4G模块；用到行车记录仪；
//*****************************************************************************************	//
uint8_t uart2_buf[200] = {0};
uint16_t usart2_buf_len = 0;
uint8_t usart2_idel_flag = 0;
void __attribute__((interrupt))_USART2_exception (void)
{
	uint8_t data;
	if(USART_Get_Receive_BUFR_Ready_Flag(USART2_SFR))
	{
		data = USART_ReceiveData(USART2_SFR);
//		USART_SendData(USART2_SFR, data);

		uart2_buf[usart2_buf_len] = data;
		usart2_buf_len++;

		if(usart2_buf_len > 200) usart2_buf_len = 0;
	}

	if (USART_Get_Receive_Frame_Idel_Flag(USART2_SFR))
	{
		USART_Clear_IDLE_INT_Flag(USART2_SFR);
		usart2_idel_flag = 1;
	}

}
//*****************************************************************************************
//                            UART4 - 外接蓝牙
//*****************************************************************************************	//
void __attribute__((interrupt))_USART4_exception (void)
{
	uint8_t data;
	if(USART_Get_Receive_BUFR_Ready_Flag(USART4_SFR))
	{
		data=USART_ReceiveData(USART4_SFR);
		g_st_serial_opt.serial_rx_handler(SERIAL_UART4,data);
		HC02_BT_Analysis_Alg_Para(data);
		Parse_ReceiveData(data, USART4_STREAM);
	}
//	if(USART_Get_Receive_Frame_Idel_Flag(USART4_SFR))
//	{
//		USART_Clear_IDLE_INT_Flag(USART4_SFR);
//		Usart4_ReceiveIdlE_flag=1;
//	}
}


void __attribute__((interrupt))_T14_exception (void)
{

	BTIM_Clear_Updata_INT_Flag(T14_SFR);										//清更新时间标志位
	BTIM_Clear_Overflow_INT_Flag (T14_SFR);										//清T14溢出中断标志位
}

//*****************************************************************************************
//                              T15中断函数
//*****************************************************************************************	//
uint32_t PLUSE_WIDTH=0;//捕捉到PWM周期变量
void __attribute__((interrupt))_T15_exception (void)
{

	BTIM_Clear_Updata_INT_Flag(T15_SFR);									 //清更新时间标志位
	BTIM_Clear_Overflow_INT_Flag (T15_SFR);									//清T15溢出中断标志位

	Time15_CNT++;
	if(Time15_CNT>=10)  //100ms进行一
	{
		Time15_CNT =0;
	}
}

//*****************************************************************************************
//                              SysTick中断函数，维护系统时钟 1ms
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception (void)
{
	static uint32_t send_time = 0;
	struct can_frame tx_frame_valve;
	//节拍定时器的中断无需软件清除标志位
	SystemtimeClock ++;

	UTCTIME_Count_In_SysClick_IT(SystemtimeClock);

	if(stSpeedPara.uSingnalType == 1)
	{
		if((0 == speed_level) && (1 == GPIO_Read_Input_Data_Bit (GPIOC_SFR, GPIO_PIN_MASK_4)))
		{
			first_time = SystemtimeClock - second_time;
			second_time = SystemtimeClock;
		}
		speed_level = GPIO_Read_Input_Data_Bit (GPIOC_SFR, GPIO_PIN_MASK_4);
	}
	if(stSysPara.ucControlMode == 2)	//BYD公交需要发送AEB存在命令20ms一次
	{
		if((SystemtimeClock - send_time) > 20)
		{
			Valve_AEBdecActive_Get(&tx_frame_valve);
			tx_frame_valve.TargetID = 0x1C01F025;
			CAN_Transmit_DATA(CAN2_SFR,tx_frame_valve);
		}
	}

}

