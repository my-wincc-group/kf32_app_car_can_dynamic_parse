/*
 * at_receiver.c
 *
 *  Created on: 2022-8-3
 *      Author: acai
 */
#include "./cmd_receiver.h"
#include <ctype.h>
#include <stdio.h>
#include "usart.h"
#include "../c_queue.h"

#define MAX_BUFFER_SIZE 256

// private:
static bool is_have_cmd_ = false;
static char data_buf_[MAX_BUFFER_SIZE] = { 0 };

static uint8_t buff_index_ = 0;
static struct CQueue queue_ = { 0 };
//private:
static const int at_header_length_ = 3;
static const char at_header_cmd_[] = "AT+";

// private:
static inline bool InitAtHeader();
static inline bool IsAtBegin(uint8_t data);
static inline bool IsAtEnd();

STREAM *usart_ = NULL;
// public:
bool CmdRec_Init() {
  is_have_cmd_ = false;
  // step 1
  CQueue_Create(&queue_, data_buf_, MAX_BUFFER_SIZE, sizeof(uint8_t), C_QUEUE_UPDATE_HOLD_OLD);

//  fprintf(USART1_STREAM, "CmdRec_Init1111: %d\r\n", CQueue_GetSize(&queue_));
  // step 2
  InitAtHeader();

//  fprintf(USART1_STREAM, "CmdRec_Init22222: %d\r\n", CQueue_GetSize(&queue_));

}

bool CmdRec_ReceiveData(uint8_t data) {
  if (IsAtBegin(data)) {
    return true;
  }

  //fprintf(USART1_STREAM, "CmdRec_ReceiveData: %c\r\n", data);
  //USART1_Bebug_Print_Num("CmdRec_ReceiveData", 2, 1, 1);

  CQueue_Push(&queue_, &data);

  if (IsAtEnd()) {
    is_have_cmd_ = true;
  }
//  USART1_Bebug_Print_Num("CmdRec_ReceiveData", 3, 1, 1);
}

int CmdRec_WriteData(char data) {
//	USART1_Bebug_Print("data", &data, 1);
	if(usart_ == USART4_STREAM){			// 20220909 lmz add 修复蓝牙（USART4）获取不到反馈数据的BUG
		USART_Send(USART4_SFR, &data, 1);
	}else{
		Usart1_DMA_Transmit(&data, 1);
	//	USART1_Bebug_Print("asdasd", "22222222222", 1);
	}
  return 1;
}

int CmdRec_ReadReceivedData(char *data) {
  if (!is_have_cmd_) {
    return 0;
  }

  if (CQueue_GetSize(&queue_) <= at_header_length_) {
    *data = '\0';
    is_have_cmd_ = false;
    return 1;
  }

  CQueue_GetData(&queue_, data);
  CQueue_Pop(&queue_);
 // fprintf(USART1_STREAM, "CmdRec_ReadReceivedData: %c\r\n", *data);

  return 1;
}

static inline bool InitAtHeader() {
  buff_index_ = 0;
  CQueue_Push(&queue_, at_header_cmd_);
  CQueue_Push(&queue_, at_header_cmd_ + 1);
  CQueue_Push(&queue_, at_header_cmd_ + 2);
}

static inline bool IsAtBegin(uint8_t data) {
  if (buff_index_ >= at_header_length_) {
    return false;
  }

  uint8_t upper_data = toupper(data), buf_data;

  CQueue_GetReverseIndexData(&queue_, at_header_length_ - buff_index_ - 1, &buf_data);

  if (upper_data == buf_data) {
    buff_index_++;
  } else {
    buff_index_ = 0;
  }

  //fprintf(USART1_STREAM,"upper_data, buf_data, buff_index_ %c, %c, %d\r\n",upper_data, buf_data, buff_index_);
  return true;
}

static inline bool IsAtEnd() {
  uint8_t at_end;
  CQueue_GetReverseIndexData(&queue_, 0, &at_end);

  //fprintf(USART1_STREAM,"IsAtEnd %c\r\n", at_end);
  if ('\0' == at_end || '\n' == at_end) {
    InitAtHeader();
    return true;
  }
  return false;
}
