/*
 * parse.c
 *
 *  Created on: 2022-8-3
 *      Author: acai
 */

#include "../parse.h"

#include "cat.h"
#include "./cmd_receiver.h"
#include "./params/srr_params.h"
#include "./params/flash_params.h"
#include "./params/vehicle_params.h"
#include "at_parse_alg_para.h"
#include "usart.h"

#define AT_BUFFER_SIZE 			128		// 256 20220906 lmz update
#define MAX_CMD_GROUP_NUM 		6		// 10 20220906 lmz update

static char at_buf_[AT_BUFFER_SIZE];
static struct cat_command_group *cmd_groups_[MAX_CMD_GROUP_NUM] = { 0 };

static struct cat_descriptor desc_ = { .cmd_group = cmd_groups_, .cmd_group_num = MAX_CMD_GROUP_NUM,
    .buf = at_buf_, .buf_size = sizeof(at_buf_) };

static struct cat_io_interface iface_ =
    { .read = CmdRec_ReadReceivedData, .write = CmdRec_WriteData, };

struct cat_object self[1] = { 0 };

extern STREAM *usart_;

bool Parse_Init() {
  CmdRec_Init();

  VehParam_GetAtCmd((void **) &(cmd_groups_[0]));
  SrrParam_GetAtCmd((void **) &(cmd_groups_[1]));
  AlgPrase_GetAtCmd((void **) &(cmd_groups_[2]));
  FlashParam_GetAtCmd((void **) &(cmd_groups_[3]));

  desc_.cmd_group_num = 4;

  cat_init(self, &desc_, &iface_, NULL);
}
bool Parse_Process() {
  cat_status status = cat_service(self);
  if (CAT_STATUS_OK == status) {
    return true;
  }

  while (CAT_STATUS_OK != cat_service(self)) {
  }

  return true;
}
bool Parse_ReceiveData(uint8_t data, STREAM *it_usart) {
  usart_ = it_usart;
  CmdRec_ReceiveData(data);
}
