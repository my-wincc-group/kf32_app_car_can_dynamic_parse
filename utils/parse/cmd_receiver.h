/*
 * at_receiver.h
 *
 *  Created on: 2022-8-3
 *      Author: acai
 */

#ifndef CMD_RECEIVER_H_
#define CMD_RECEIVER_H_

#include <stdint.h>
#include <stdbool.h>

bool CmdRec_Init();
bool CmdRec_ReceiveData(uint8_t data);

int CmdRec_WriteData(char data);
int CmdRec_ReadReceivedData(char *data);

#endif /* CMD_RECEIVER_H_ */
