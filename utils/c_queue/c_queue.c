/*
 * c_queue.c
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */
#include <malloc.h>
#include <string.h>
#include "../c_queue.h"
#include "usart.h"
#include "canhl.h"
#include "tool.h"
#include "srr_can.h"
static inline int CircularIndex(uint16_t index, uint16_t range);
static inline void GetElemData(struct CQueue * queue, int index, void *data);

void CQueue_Create(struct CQueue *self, void *elem_buffer, int elem_num, int elem_size,
    CQueueUpdate update) {
  self->update = update;
  self->state = C_QUEUE_IS_EMPTY;

  self->back = 0;
  self->front = 0;
  self->element_num = elem_num;
  self->element_size = elem_size;
  self->element_buffer = elem_buffer;
}

void CQueue_Clear(struct CQueue * self) {
  self->back = 0;
  self->front = 0;
}

bool CQueue_Pop(struct CQueue * self) {
  if (C_QUEUE_IS_EMPTY == self->state || 0 == self->element_num) {
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"#####C_QUEUE_IS_EMPTY111######\r\n" );
    return false;
  }
//  if(self == srr_can_queue_ )
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"==F1:%d B:%d s:%d\r\n",self->front,self->back,self->state);

//  if(self->back == self->front && C_QUEUE_IS_FULL != self->state){
//	  return false;
//  }
  self->state = C_QUEUE_IS_OK;
  self->back = CircularIndex(self->back, self->element_num);
//  if(self == srr_can_queue_ )
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"==F2:%d B:%d s:%d\r\n",self->front,self->back,self->state);
  if(self->back == self->front){
    self->state = C_QUEUE_IS_EMPTY;
//    gcz_serial_v1_dma_printf(SERIAL_UART1,"#####C_QUEUE_IS_EMPTY######\r\n" );
  }
//  static uint32_t CQueue_Pop_clk = 0;
//  USART1_Bebug_Print_Num("CQueue_Pop", SystemtimeClock - CQueue_Pop_clk,1,0);
//  CQueue_Pop_clk = SystemtimeClock;
  return true;
}

bool CQueue_Push(struct CQueue * self, const void * data) {
  if (0 == self->element_num) {
    return false;
  }

  if(C_QUEUE_IS_FULL == self->state && C_QUEUE_UPDATE_NEW == self->update){
    self->back = CircularIndex(self->back, self->element_num);
  }

  void *cur_data = self->element_buffer + self->element_size * self->front;
  memcpy(cur_data, data, self->element_size);

  self->state = C_QUEUE_IS_OK;
//  if(self == srr_can_queue_ ){
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"CQueue_GetSize = %d \r\n", CQueue_GetSize(self));
//  }
//  if(self == srr_can_queue_ )
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"F1:%d B:%d\r\n",self->front,self->back);
  self->front = CircularIndex(self->front, self->element_num);
//  if(self == srr_can_queue_ )
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"F2:%d B:%d s:%d\r\n",self->front,self->back,self->state);
  if(self->back == self->front){
    self->state = C_QUEUE_IS_FULL;
    if(self == srr_can_queue_ )
    {
    	gcz_serial_v1_dma_printf(SERIAL_UART1,"###########\r\n" );
    }

  }

  return true;
}

int CQueue_GetSize(struct CQueue * self) {
  if(C_QUEUE_IS_FULL == self->state){
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"A.elemNbr %d\r\n" ,self->element_num);
    return self->element_num;
  }

  int len = self->front - self->back;
  if (len >= 0) {
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"B.len %d\r\n" ,len);
    return len;
  } else {
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"C.elemNbr %d\r\n" ,self->element_num);
    return len + self->element_num;
  }
}

bool CQueue_GetData(struct CQueue * self, void * data) {
  if (C_QUEUE_IS_EMPTY == self->state) {
    memset(data, 0, self->element_size);
    return false;
  }

  GetElemData(self, self->back, data);

  return true;
}

bool CQueue_GetIndexData(struct CQueue *self, int index, void *data) {
  int size = CQueue_GetSize(self);
  if (index >= size) {
    memset(data, 0, self->element_size);
    return false;
  }

  int cur_index = (self->back + index) % self->element_num;

  GetElemData(self, cur_index, data);
  return true;
}

bool CQueue_GetReverseIndexData(struct CQueue *self, int index, void *data) {
  int size = CQueue_GetSize(self);
  //fprintf(USART1_STREAM,"GetReverseIndexData %d, %d\r\n", index, size);
  if (index >= size) {
    memset(data, 0, self->element_size);
    return false;
  }

  //fprintf(USART1_STREAM,"GetReverseIndexData %d, %d\r\n", index, size);
  int cur_index = (self->front - index -1);
  if(cur_index < 0){
    cur_index += self->element_num;
  }

  GetElemData(self, cur_index, data);
  return true;
}

static inline int CircularIndex(uint16_t index, uint16_t range) {
  index++;
  return index % range;
}
//void GetElemData(struct CQueue * self, int index, void *data);
//void GetElemData(struct CQueue * self, int index, void *data) {
static inline void GetElemData(struct CQueue * self, int index, void *data) {
  void *cur_data = self->element_buffer + self->element_size * index;
  memcpy(data, cur_data, self->element_size);

//  if(self == srr_can_queue_ )
//  {
//	  uint32_t ID;
//	  memcpy(&ID,data,4);
//	  gcz_serial_v1_dma_printf(SERIAL_UART1,"D.%d %d ID:%04x\r\n" ,self->front,self->back,ID);
//  }
}
void GetElemData_test(struct CQueue * self, int index, void *data);
void GetElemData_test(struct CQueue * self, int index, void *data) {
  void *cur_data = self->element_buffer + self->element_size * index;
  memcpy(data, cur_data, self->element_size);

}
