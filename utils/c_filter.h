/*
 * c_filter.h
 *
 *  Created on: 2022-8-22
 *      Author: acai
 */

#ifndef C_FILTER_H_
#define C_FILTER_H_
#include "c_queue.h"

// 比较元素的最大尺寸
#define   C_FILTER_BUFFER_LENGTH 20
//自定义回调函数
//返回值为正，则max_value > min_value , 返回值为0 则 max_value == min_value ,否则返回值小于0.
typedef int (*CFilter_Compare)(void * max_value, void *min_value);

struct CFilter{
  struct CQueue *queue;
  CFilter_Compare compare;

  char buffer1[C_FILTER_BUFFER_LENGTH];
  char buffer2[C_FILTER_BUFFER_LENGTH];
};

// queue需提前初始化。且注意此操作不支持互斥操作。
bool CFilter_Init(struct CFilter *self, struct CQueue *queue, CFilter_Compare compare);

// 不支持互斥操作，输入不要在中断中调用。
bool CFilter_InputData(struct CFilter *self, const void * data);

bool CFilter_GetMaxValue(struct CFilter *self, void * data);
bool CFilter_GetMinValue(struct CFilter *self, void * data);
bool CFilter_GetMeanValue(struct CFilter *self, void * data);

#endif /* C_FILTER_H_ */
