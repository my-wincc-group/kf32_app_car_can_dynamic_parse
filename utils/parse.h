/*
 * parse.h
 *
 *  Created on: 2022-8-3
 *      Author: acai
 */

#ifndef PARSE_H_
#define PARSE_H_
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

bool Parse_Init();
bool Parse_Process();
bool Parse_ReceiveData(uint8_t data, STREAM *it_usart);

#endif /* PARSE_H_ */
