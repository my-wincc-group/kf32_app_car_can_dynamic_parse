/*
 * params.h
 *
 *  Created on: 2022-8-1
 *      Author: acai
 */

#ifndef SYSTEM_PARAMS_H_
#define SYSTEM_PARAMS_H_

#include <stdbool.h>

struct VehicleParams{
  float width;
  float height;
  float length;
  float front_to_rear_axle;
};

struct SrrParams{
  float braking_time;
  float braking_force;
  float dist_to_vehicle_front;
  float dist_to_vehicle_center;
};

bool Param_Init();

struct VehicleParams * Param_GetVehicleParams();
bool Param_SetVechicleParams(struct VehicleParams *params);

struct SrrParams * Param_GetSrrParams();
bool Param_SetSrrParams(struct SrrParams *params);

#endif /* SYSTEM_PARAMS_H_ */
