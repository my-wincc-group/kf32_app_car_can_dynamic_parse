/*
 * c_queue.h
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#ifndef C_QUEUE_H_
#define C_QUEUE_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum {
  C_QUEUE_IS_OK = 0, C_QUEUE_IS_EMPTY, C_QUEUE_IS_FULL,
} CQueueState;

typedef enum {
  C_QUEUE_UPDATE_NEW = 0, C_QUEUE_UPDATE_HOLD_OLD,
} CQueueUpdate;

struct CQueue {
  CQueueState state;
  CQueueUpdate update;

volatile  uint16_t back;        // 指向最先压入元素
volatile  uint16_t front;       // 指向最新压入元素
  uint16_t element_num;
  uint16_t element_size;


  char * element_buffer;
};

// self : 队列对象
// elem_buffer : 队列的缓冲内存
// elem_num : 队列中元素个数
// elem_size : 队列中每个元素的大小
// update :
void CQueue_Create(struct CQueue *self, void *elem_buffer, int elem_num, int elem_size,
    CQueueUpdate update);

// 清空队列中的元素
void CQueue_Clear(struct CQueue * self);

// 队列压入和弹出
bool CQueue_Pop(struct CQueue * self);
bool CQueue_Push(struct CQueue * self, const void * data);

int CQueue_GetSize(struct CQueue * self);

bool CQueue_GetData(struct CQueue * self, void * data);
bool CQueue_GetIndexData(struct CQueue *self, int index, void *data);
bool CQueue_GetReverseIndexData(struct CQueue *self, int index, void *data);


inline bool CQueue_IsFull(struct CQueue *self){
  return C_QUEUE_IS_FULL == self->state;
}

inline bool CQueue_IsEmpty(struct CQueue *self){
  return C_QUEUE_IS_EMPTY == self->state;
}

#endif /* C_QUEUE_H_ */

