/*
 * srr_params.c
 *
 *  Created on: 2022-8-8
 *      Author: acai
 */

#include "srr_params.h"
#include "cat.h"
#include "w25qxx.h"

#define SRR_EXTERN_FLASH_ADDRESS_OFFSET 0
#define SRR_EXTERN_FLASH_ADDRESS (OUT_FLASH_SRR_START + SRR_EXTERN_FLASH_ADDRESS_OFFSET)

static struct SrrParams at_params_[1] = {0};
static struct SrrParams srr_params_[1] = {0};

#define AT_INT_VAR(d, size) {.type = CAT_VAR_INT_DEC, .data = d, .data_size = size,}
#define AT_FLOAT_VAR(d, size, dec_places) {.type = CAT_VAR_FLOAT_DEC, .data = d, .data_size = size, .decimal_places = dec_places}

static struct cat_variable at_veh_params_[] = {
    AT_FLOAT_VAR(&(at_params_->braking_time), sizeof(float),  3),
    AT_FLOAT_VAR(&(at_params_->braking_force), sizeof(float),  3),
    AT_FLOAT_VAR(&(at_params_->dist_to_vehicle_front), sizeof(float), 3),
    AT_FLOAT_VAR(&(at_params_->dist_to_vehicle_center), sizeof(float), 3),
};

static struct cat_command cmd_veh_params_[] = {
    { .name = "+SrrBrakingTime", 			.var = at_veh_params_,		.var_num = 1, },
    { .name = "+SrrBrakingForce", 			.var = at_veh_params_ + 1,	.var_num = 1, },
    { .name = "+SrrDistToVechicleFront", 	.var = at_veh_params_ + 2,	.var_num = 1, },
    { .name = "+SrrDistToVechicleCenter", 	.var = at_veh_params_ + 3,	.var_num = 1, },
};

static struct cat_command_group cmd_group_ = {
    .cmd = cmd_veh_params_, .cmd_num = sizeof(cmd_veh_params_) / sizeof(cmd_veh_params_[0])
};


bool SrrParam_Init(){
  if(W25QXX_Read_Chk2((uint8_t*)srr_params_, SRR_EXTERN_FLASH_ADDRESS, sizeof(struct SrrParams))){
    memcpy(at_params_, srr_params_, sizeof(struct SrrParams));
    return true;
  }
  return false;
}
/*
 * 对外获取AT指令组 API
 * 参数：AT指令组指针，会在cat_init(self, &desc_, &iface_, NULL);中desc初始赋值时被赋值（其实就是回调函数）
 * 说明：AT指令组可以有多个。
 */
bool SrrParam_GetAtCmd(void **cmd){
  *cmd = &cmd_group_;
}

struct SrrParams * SrrParam_GetParams(){
  return srr_params_;
}

bool SrrParam_SetParams(struct SrrParams *params){
  const int size = sizeof(struct SrrParams);

  W25QXX_Write_Chk2((uint8_t*)params, SRR_EXTERN_FLASH_ADDRESS, size);

  const int check_num = 5;
  int times = 0;
  while(times < check_num){
    if(W25QXX_Read_Chk2((uint8_t*)params, SRR_EXTERN_FLASH_ADDRESS, size)){
      break;
    }
    times++;
  }
  if(check_num <=  times){
    return false;
  }
  else {
    return true;
  }
}

bool SrrParam_SyncExternFlash(){
  if(SrrParam_SetParams(at_params_)){
    memcpy(srr_params_, at_params_, sizeof(struct SrrParams));
    return true;
  }
  return false;
}

// set srr parameters  add lmz 20221011
void SrrParam_Set_At_Params(struct SrrParams *p_srr_params){
	memcpy(at_params_, p_srr_params, sizeof(struct SrrParams));
}
