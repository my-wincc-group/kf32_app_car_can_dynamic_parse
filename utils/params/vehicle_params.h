/*
 * vehicle_params.h
 *
 *  Created on: 2022-8-8
 *      Author: acai
 */

#ifndef VEHICLE_PARAMS_H_
#define VEHICLE_PARAMS_H_
#include <stdint.h>
#include <stdbool.h>
#include "params.h"

bool VehParam_Init();
bool VehParam_GetAtCmd(void **cmd);

struct VehicleParams * VehParam_GetParams();
bool VehParam_SetParams(struct VehicleParams *params);
bool VehParam_SyncExternFlash();
void VehParam_Set_At_Params(struct VehicleParams *p_veh_params);	// add lmz 20221011

#endif /* VEHICLE_PARAMS_H_ */
