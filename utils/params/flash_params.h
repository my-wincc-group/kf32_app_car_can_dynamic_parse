/*
 * flash_params.h
 *
 *  Created on: 2022-8-11
 *      Author: acai
 */

#ifndef FLASH_PARAMS_H_
#define FLASH_PARAMS_H_
#include <stdbool.h>

bool FlashParam_Init();
bool FlashParam_GetAtCmd(void **cmd);

#endif /* FLASH_PARAMS_H_ */
