/*
 * params.c
 *
 *  Created on: 2022-8-10
 *      Author: acai
 */

#include "../params.h"
#include "srr_params.h"
#include "vehicle_params.h"
bool Param_Init(){
  VehParam_Init();
  SrrParam_Init();
}

struct VehicleParams * Param_GetVehicleParams(){
  return VehParam_GetParams();
}
bool Param_SetVechicleParams(struct VehicleParams *params){
  VehParam_SetParams(params);
}

struct SrrParams * Param_GetSrrParams(){
  return SrrParam_GetParams();
}
bool Param_SetSrrParams(struct SrrParams *params){
  SrrParam_SetParams(params);
}

