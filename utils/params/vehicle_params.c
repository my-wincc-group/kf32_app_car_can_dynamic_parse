/*
 * vehicle_params.c
 *
 *  Created on: 2022-8-8
 *      Author: acai
 */

#include "vehicle_params.h"

#include "cat.h"
#include "w25qxx.h"

#define VEHICLE_EXTERN_FLASH_ADDRESS_OFFSET 0X100
#define VEHICLE_EXTERN_FLASH_ADDRESS (OUT_FLASH_SRR_START + VEHICLE_EXTERN_FLASH_ADDRESS_OFFSET)

static struct VehicleParams at_params_[1] = {0};
static struct VehicleParams veh_params_[1] = {0};

#define AT_INT_VAR(d, size) {.type = CAT_VAR_INT_DEC, .data = d, .data_size = size,}
#define AT_FLOAT_VAR(d, size, dec_places) {.type = CAT_VAR_FLOAT_DEC, .data = d, .data_size = size, .decimal_places = dec_places}

static struct cat_variable at_veh_params_[] = {
    AT_FLOAT_VAR(&(at_params_->width), sizeof(float), 3),
    AT_FLOAT_VAR(&(at_params_->height), sizeof(float),  3),
    AT_FLOAT_VAR(&(at_params_->length), sizeof(float), 3),
    AT_FLOAT_VAR(&(at_params_->front_to_rear_axle), sizeof(float), 3),
};

static struct cat_command cmd_veh_params_[] = {
    { .name = "+VehicleWidth", 			.var = at_veh_params_,		.var_num = 1, },
    { .name = "+VehicleHeight", 		.var = at_veh_params_ + 1, 	.var_num = 1, },
    { .name = "+VehicleLength", 		.var = at_veh_params_ + 2,	.var_num = 1, },
    { .name = "+VehicleFrontToRearAxle", .var = at_veh_params_ + 3,	.var_num = 1, },
};

static struct cat_command_group cmd_group_ = {
    .cmd = cmd_veh_params_, .cmd_num = sizeof(cmd_veh_params_) / sizeof(cmd_veh_params_[0]),
};

bool VehParam_Init(){
  if(W25QXX_Read_Chk2((uint8_t* )veh_params_, VEHICLE_EXTERN_FLASH_ADDRESS,
      sizeof(struct VehicleParams))){
    memcpy(at_params_, veh_params_, sizeof(struct VehicleParams));
    return true;
  }else{
    return false;
  }

}
/*
 * 对外获取AT指令组 API
 * 参数：AT指令组指针，会在cat_init(self, &desc_, &iface_, NULL);中desc初始赋值时被赋值（其实就是回调函数）
 * 说明：AT指令组可以有多个。
 */
bool VehParam_GetAtCmd(void **cmd){
  *cmd = &cmd_group_;
}

struct VehicleParams * VehParam_GetParams(){
  return veh_params_;
}

bool VehParam_SetParams(struct VehicleParams *params){
  const int size = sizeof(struct VehicleParams);

  W25QXX_Write_Chk2((uint8_t* )params, VEHICLE_EXTERN_FLASH_ADDRESS, size);

  const int check_num = 5;
  int times = 0;
  while(times < check_num){
    if(W25QXX_Read_Chk2((uint8_t* )params, VEHICLE_EXTERN_FLASH_ADDRESS, size)){
      break;
    }
    times++;
  }
  if(check_num <=  times){
    return false;
  }
  else {
    return true;
  }
}

bool VehParam_SyncExternFlash(){
  if(VehParam_SetParams(at_params_)){
    memcpy(veh_params_, at_params_, sizeof(struct VehicleParams));
    return true;
  }else{
    return false;
  }
}

// set vehicle parameters  add lmz 20221011
void VehParam_Set_At_Params(struct VehicleParams *p_veh_params){
	memcpy(at_params_, p_veh_params, sizeof(struct VehicleParams));
}
