/*
 * flash_params.c
 *
 *  Created on: 2022-8-11
 *      Author: acai
 */

#include "flash_params.h"
#include "cat.h"
#include "srr_params.h"
#include "vehicle_params.h"

static cat_return_state cmd_run(const struct cat_command *cmd);
static struct cat_command cmd_veh_params_[] = {
    { .name = "+SyncExternFlash", .run = cmd_run},
};

static struct cat_command_group cmd_group_ = {
    .cmd = cmd_veh_params_, .cmd_num = sizeof(cmd_veh_params_) / sizeof(cmd_veh_params_[0])
};

bool FlashParam_GetAtCmd(void **cmd){
  *cmd = &cmd_group_;
}

static cat_return_state cmd_run(const struct cat_command *cmd){
  if(SrrParam_SyncExternFlash() && VehParam_SyncExternFlash()){
    return CAT_RETURN_STATE_OK;
  }
  return CAT_RETURN_STATE_ERROR;
}
