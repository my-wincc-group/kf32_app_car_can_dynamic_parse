/*
 * srr_params.h
 *
 *  Created on: 2022-8-8
 *      Author: acai
 */

#ifndef SRR_PARAMS_H_
#define SRR_PARAMS_H_
#include <stdbool.h>
#include "../params.h"

bool SrrParam_Init();
bool SrrParam_GetAtCmd(void **cmd);

struct SrrParams * SrrParam_GetParams();
bool SrrParam_SetParams(struct SrrParams *params);
bool SrrParam_SyncExternFlash();
void SrrParam_Set_At_Params(struct SrrParams *p_srr_params);	// add lmz 20221011

#endif /* SRR_PARAMS_H_ */
