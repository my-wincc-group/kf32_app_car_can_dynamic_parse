/*
 * c_filter.c
 *
 *  Created on: 2022-8-22
 *      Author: acai
 */

#include "../c_filter.h"
#include <stdio.h>
#include <string.h>
bool CFilter_Init(struct CFilter *self, struct CQueue *queue, CFilter_Compare compare){
  self->queue = queue;
  self->compare = compare;

  if(self->queue->element_size > C_FILTER_BUFFER_LENGTH){
    fprintf(USART1_STREAM, "[ERROR] CFilter init");
    return false;
  }
  return true;
}

bool CFilter_InputData(struct CFilter *self, const void * data){
  return CQueue_Push(self->queue, data);
}


bool CFilter_GetMaxValue(struct CFilter *self, void * data){
  int size = CQueue_GetSize(self->queue);
  if(0 == size){
    memset(data, 0, self->queue->element_size);
    return false;
  }

  void *max_value = self->buffer1;
  void *tmp_value = self->buffer2;

  const int element_size = self->queue->element_size;

  CQueue_GetIndexData(self->queue, 0, max_value);
  for(int i = 1; i < size; i++){
    CQueue_GetIndexData(self->queue, i, tmp_value);
    if(self->compare(tmp_value, max_value) > 0){
      memcpy(max_value, tmp_value, element_size);
    }
  }

  memcpy(data, max_value, element_size);
}

bool CFilter_GetMinValue(struct CFilter *self, void * data){
  int size = CQueue_GetSize(self->queue);
    if(0 == size){
      memset(data, 0, self->queue->element_size);
      return false;
    }

    void *min_value = self->buffer1;
    void *tmp_value = self->buffer2;

    const int element_size = self->queue->element_size;

    CQueue_GetIndexData(self->queue, 0, min_value);
    for(int i = 1; i < size; i++){
      CQueue_GetIndexData(self->queue, i, tmp_value);
      if(self->compare(tmp_value, min_value) < 0){
        memcpy(min_value, tmp_value, element_size);
      }
    }

    memcpy(data, min_value, element_size);
}
