# 升级

升级满足的前提条件：

​	1、需先将bootloader_production和AEB_APP_production程序通过ChipON KF32软件烧录到自研设备中；

​	2、需先通过上位机将设备的PN号、SN号、软件版本号、硬件版本号写入到自研设备的外FLASH中；



## OTA升级

通过bootloader引导后，启动APP应用程序的过程。

方式1：上电后通过自检检测运行版本与平台版本高低，若平台版本高就执行OTA升级过程；

方式2：通过串口1发送指令<ZKHYCHK*UPDATE:NEWEST>检测是否需要升级；



## 线刷升级

需操作人员通过上位机经由串口1完成烧录程序。主要为了外场测试人员不方便烧录程序而设计；



## 整体流程



![OTA升级流程图](image\upgrade\OTA升级流程图.png)
