# TL;DR（太长不读）

* [X] 试运行昆仑山项目程序静态分析服务，
* [X] 天津同事可在办公室访问 [Cppcheck - HTML report - kungfu_aebs](http://xiadan/) 查阅相关代码缺陷，反馈相关规则尺度
* [X] 当前BUS_AEB_alg(head)上，至少有24处error必须修复，其中数组越界4处，整形溢出2处，变量未初始化5处…
* [ ] 规则尺度确定后，协同IT同事，将此服务部署到公司服务器上并对接GitLab CI/CD管道。

  **目标：至少每个发布版本都经过静态代码测试，争取每个提交都经过静态代码测试。**

# 背景

## 静态程序分析

程序静态分析（Program Static Analysis）是指在不运行[代码](https://baike.baidu.com/item/%E4%BB%A3%E7%A0%81/86048)的方式下，通过[词法分析](https://baike.baidu.com/item/%E8%AF%8D%E6%B3%95%E5%88%86%E6%9E%90/8853461)、[语法分析](https://baike.baidu.com/item/%E8%AF%AD%E6%B3%95%E5%88%86%E6%9E%90/8853407)、[控制流](https://baike.baidu.com/item/%E6%8E%A7%E5%88%B6%E6%B5%81/854473)、[数据流分析](https://baike.baidu.com/item/%E6%95%B0%E6%8D%AE%E6%B5%81%E5%88%86%E6%9E%90/21496987)等技术对程序代码进行扫描，验证代码是否满足规范性、安全性、可靠性、可维护性等指标的一种代码分析技术。

## MISRA C

**MISRA C**是由[汽车产业软件可靠性协会](https://zh.wikipedia.org/w/index.php?title=%E6%B1%BD%E8%BB%8A%E7%94%A2%E6%A5%AD%E8%BB%9F%E9%AB%94%E5%8F%AF%E9%9D%A0%E6%80%A7%E5%8D%94%E6%9C%83&action=edit&redlink=1)（MISRA）提出的[C语言](https://zh.wikipedia.org/wiki/C%E8%AA%9E%E8%A8%80 "C语言")开发标准。其目的是在增进[嵌入式系统](https://zh.wikipedia.org/wiki/%E5%B5%8C%E5%85%A5%E5%BC%8F%E7%B3%BB%E7%BB%9F "嵌入式系统")的安全性及[可移植性](https://zh.wikipedia.org/wiki/%E8%BB%9F%E9%AB%94%E5%8F%AF%E7%A7%BB%E6%A4%8D%E6%80%A7 "软件可移植性")。针对[C++](https://zh.wikipedia.org/wiki/C%2B%2B "C++")语言也有对应的标准[MISRA C++](https://zh.wikipedia.org/w/index.php?title=MISRA_C%2B%2B&action=edit&redlink=1 "MISRA C++（页面不存在）")。

MISRA C一开始主要是针对汽车产业^[[1]](https://zh.wikipedia.org/wiki/MISRA_C#cite_note-1)^ ，不过其他产业也逐渐开始使用MISRA C：包括航空、电信、国防、医疗设备、铁路等领域中都已有厂商使用MISRA C。 ^[[2]](https://zh.wikipedia.org/wiki/MISRA_C#cite_note-2)^ ^[[3]](https://zh.wikipedia.org/wiki/MISRA_C#cite_note-3)^

MISRA C的第一版《Guidelines for the use of the C language in vehicle based software》是在1998年发行，一般称为MISRA-C:1998. ^[[4]](https://zh.wikipedia.org/wiki/MISRA_C#cite_note-4)^ 。MISRA-C:1998有127项规则，规则从1号编号到127号，其中有93项是强制要求，其余的34项是推荐使用的规则。

在2004年时发行了第二版的MISRA C的第一版《Guidelines for the use of the C language in critical systems》（或称作MISRA-C:2004），其中有许多重要建议事项的变更，其规则也重新编号。MISRA-C:2004有141项规则，其中121项是强制要求，其余的20项是推荐使用的规则。规则分为21类，从“开发环境”到“运行期错误” ^[[5]](https://zh.wikipedia.org/wiki/MISRA_C#cite_note-5)^ 。

基于目前昆仑山产品技术特性及团队资源状况，**对昆仑山项目，暂定采用Cppcheck 1.90作为代码检查工具，MISRA C：2012作为代码检查规则。**

MISRA C：2012中针对手写代码是Required的规则，对自动生成的代码变为Advisory规则。

# 查阅报告

访问静态代码服务地址 http://xiadan

在页面搜索"error", 点击链接即可到达相应代码位置修改

# 生成报告

前提：已安装cmake, cppcheck，并将Eclipse项目转成CMake项目（CMakeList.txt）

```
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .

cppcheck --project=compile_commands.json --xml --addon=misra.json 2>report-src.xml

cppcheck-htmlreport --source-dir=. --title=kungfu_aebs --file=report-src.xml --report-dir=htmlreport --source-encoding="gbk"
```
