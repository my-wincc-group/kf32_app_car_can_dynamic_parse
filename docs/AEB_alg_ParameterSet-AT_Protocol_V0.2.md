> Author: LMZ
>
> Data：2022-01-25
>
> Company：ZKHY

-------------------------------------------------------------------------------------------------------------------------------------------
**通讯方式：USART1，115200,8,1,0,0**
AT命令格式：
	命令：AT+参数名称=version,parameter,status
	返回：OK							# 成功
		  ERROR,valid range:[0.0-60.0]	# 失败，返回参数设置区间
		  

	version:版本号，或者成为参数ID，区间[1-100]，用于区分每一组参数；
	paratmeter:参数变量，若为float参数，需写成string,举例：AT+Brake_Cooling_Time=1,"10.1",0	
	status:作为了结束符，统一写成0；（因为厂家提供的上位机，不给发送\r\n，所以用此作为结束符）

所有配置参数命令
/*******************功能开关*******************/
AT+AEB_CMS_Enable=1,1,0							// bool	AEB_CMS全系统使能【1：开】 【0：关】
AT+CMS_HMW_Enable=1,1,0							// bool	CMS_HMW使能【1：开】 【0：关】
AT+CMS_TTC_Enable=1,1,0							// bool	CMS_TTC使能【1：开】 【0：关】
AT+AEB_TTC_Enable=1,0,0							// bool	AEB_TTC使能【1：开】 【0：关】
AT+SSS_Enable=1,1,0								// bool	SSS使能【1：开】 【0：关】
AT+HMW_Dynamic_Thr_Enable=1,1,0					// bool	HMW动态阈值使能【1：开】 【0：关】
AT+Chassis_Safe_Strategy_Enable=1,1,0			// bool	底盘安全策略使能【1：开】 【0：关】
AT+LDW_Enable=1,1,0								// bool	车道偏离预警使能【1：开】 【0：关】

/*********************参数配置**************************/

AT+Brake_Cooling_Time=1,"20.0",0				// float 精度小数点后1位 [0.0-60.0]
AT+Driver_Brake_Cooling_Time=1,"20.0",0			// float 精度小数点后1位[0.0-60.0]
AT+Max_Brake_Keep_Time=1,"5.0",0				// float 精度小数点后2位[0.0-30.0]
AT+Air_Brake_Delay_Time=1,"0.2",0				// float 精度小数点后2位[0.00-1.00]
AT+Max_Percent_Decelerate=1,"0.5",0				// float 精度小数点后1位[0.00-1.00]
AT+Min_Enable_Speed=1,"10.0",0					// float 精度小数点后2位[0.0-120.0]
AT+Ratio_Force_To_Deceleration=1,"1.0",0		// float 精度小数点后2位[0.00-10.00]

AT+CMS_HMW_Brake_Time_Thr=1,"0.8",0				// float 精度小数点后2位[0.00-2.00]
AT+CMS_HMW_Brake_Force_Feel_Para=1,"3.3",0		// float 精度小数点后2位[0.00-10.00]
AT+CMS_HMW_Warning_Time_Thr=1,"1.0",0			// float 精度小数点后1位[0.00-3.00]
AT+CMS_HMW_Dynamic_Offset_Speed_L=1,"30.0",0	// float 精度小数点后2位[10.0-60.0]
AT+CMS_HMW_Dynamic_Offset_Value_L=1,"0.5",0		// float 精度小数点后1位[0.00-2.00]
AT+CMS_HMW_Dynamic_Offset_Speed_H=1,"60.0",0	// float 精度小数点后2位[50.0-120.0]
AT+CMS_HMW_Dynamic_Offset_Value_H=1,"0.5",0		// float 精度小数点后2位[0.00-2.00]

AT+CMS_TTC_Brake_Time_Thr=1,"4.0",0				// float 精度小数点后2位[0.00-5.00]
AT+CMS_TTC_Brake_Force_Feel_Para=1,"0.7",0		// float 精度小数点后2位[0.00-10.00]
AT+CMS_TTC_Warning_Time_Level_First=1,"5.4",0	// float 精度小数点后2位[0.00-6.00]
AT+CMS_TTC_Warning_Time_Level_Second=1,"6.8",0	// float 精度小数点后2位[0.00-6.00]

AT+AEB_Decelerate_Set=1,"8.0",0					// float 精度小数点后2位[0.00-10.00]
AT+AEB_Stop_Distance=1,"0.3",0					// float 精度小数点后2位[0.00-10.00]
AT+AEB_TTC_Warning_Time_Level_First=1,"3.4",0	// float 精度小数点后2位[0.00-5.00]
AT+AEB_TTC_Warning_Time_Level_Second=1,"4.2",0	// float 精度小数点后2位[0.00-5.00]

AT+SSS_Brake_Force=1,"5.0",0					// float 精度小数点后1位[0.0-10.0]
AT+SSS_Break_Enable_Distance=1,"0.8",0			// float 精度小数点后2位[0.00-2.50]
AT+SSS_Warning_Enable_Distance=1,"2.0",0		// float 精度小数点后2位[0.00-5.00]
AT+SSS_Max_Enable_Speed=1,"20.0",0				// float 精度小数点后1位[0.0-40.0]
AT+SSS_FR_FL_Install_Distance_To_Side=1,"0.3",0// float 精度小数点后2位[0.00-1.00]
AT+SSS_Stop_Distance=1,"0.3",0					// float 精度小数点后2位[0.00-1.00]
AT+SSS_Default_Turn_Angle=1,"30.0",0			// float 精度小数点后2位[0.00-60.00]
AT+WheelSpeed_Coefficient=1,"680.0",1			// float 精度小数点后2位[0.00-10000.00]

/*********************参数配置写入并生效**************************/

AT+SETALL=1,1,0									// 存储所有，parameter非0就执行存储，status预留

/*********************当前参数配置查询**************************/

查询指令
	获取AEB ALG 配置参数
	命令：<ZKHYCHK*RALGPARA>
	返回：

-------------------------------------
​	AEB_CMS_Enable:1							
​	CMS_HMW_Enable:1							
​	CMS_TTC_Enable:1						
​	AEB_TTC_Enable:0							
​	SSS_Enable:1								
​	HMW_Dynamic_Thr_Enable:1					
​	Chassis_Safe_Strategy_Enable:1			
​	LDW_Enable:1		
​	Brake_Cooling_Time:20.00
​	Driver_Brake_Cooling_Time:20.00
​	Max_Brake_Keep_Time:5.00
​	Air_Brake_Delay_Time:0.20
​	Max_Percent_Decelerate:0.50
​	Min_Enable_Speed:10.00
​	Ratio_Force_To_Deceleration:1.00
​	CMS_HMW_Brake_Time_Thr:0.80
​	CMS_HMW_Brake_Force_Feel_Para:3.30
​	CMS_HMW_Warning_Time_Thr:1.00
​	CMS_HMW_Dynamic_Offset_Speed_L:30.00
​	CMS_HMW_Dynamic_Offset_Value_L:0.50
​	CMS_HMW_Dynamic_Offset_Speed_H:60.00
​	CMS_HMW_Dynamic_Offset_Value_H:0.50
​	CMS_TTC_Brake_Time_Thr:4.00
​	CMS_TTC_Brake_Force_Feel_Para:0.7
​	CMS_TTC_Warning_Time_Level_First:5.40
​	CMS_TTC_Warning_Time_Level_Second:6.2
​	AEB_Decelerate_Set:5.00
​	AEB_Stop_Distance:0.30
​	AEB_TTC_Warning_Time_Level_First:2.40
​	AEB_TTC_Warning_Time_Level_Second:3.20
​	SSS_Brake_Force:5.00
​	SSS_Break_Enable_Distance:0.80
​	SSS_Warning_Enable_Distance:4.10
​	SSS_Max_Enable_Speed:20.00
​	SSS_FR_FL_Install_Distance_To_Side:0.30
​	SSS_Stop_Distance:0.10
​	SSS_Default_Turn_Angle:30.00
​	WheelSpeed_Coefficient:680.0

	-------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------