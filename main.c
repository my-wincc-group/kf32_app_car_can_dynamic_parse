/****************************************************************************************
 *
 * File Name: main 
 * Project Name: kungfu_aebs
 * Version: v1.0
 * Date: 2021-06-16- 16:51:48
 * Author: shuai
 * 
 ****************************************************************************************/
/* MCU运行所需头文件 */
#include "system_init.h"
#include "usart.h"
#include "canhl.h"
#include "watchdog.h"
#include "common.h"
#include "flash.h"
#include "set_parameter.h"
#include "can_task.h"
#include "gpio.h"
#include "EC200U.h"
#include "wheelSpeed.h"
#include "usart_upgrade.h"
#include "_4g_upgrade.h"
#include "_4G_data_upload.h"
#include "timer.h"
#include "AEB_CMS_alg.h"
#include "sss_alg.h"
#include "aeb_cms_sss_para_config.h"
#include "at_parse_alg_para.h"
#include "_4g_para_config.h"
#include "_4g_server.h"
#include "aebs_version_management.h"
#include "Urader.h"
#include "AEB_upload_info.h"
#include "tooling.h"
#include "can_task.h"
#include "data_loss_prevention.h"
#include "short_range_radar/srr.h"
#include "ego/ego_info.h"
#include "dma.h"
#include "aeb_sensor_management.h"
#include "parse.h"
#include "params.h"
#include "board_system_init.h"
#include "board_config.h"
#include "tool.h"
#include "alg_lib.h"
#include "app_program.h"
#include "car_can_analysis.h"
//#define _JIN_LV_BUS
#define _JIE_FANG_TRUCK

volatile int64_t SystemtimeClock 	= 0;
extern uint8_t ToolingFlag; //yuhong 测试工装设置标志位
extern _4G_Signal_Strength_S signal_strength;

//-------------------------gcz 2022-06-01--------------------------------------------------
enum
{
	CAN_PKG_TX_CHK = 0,
	CAN_PKG_TX_SEND,
	CAN_PKG_TX_WAIT,
	CAN_PKG_TX_FINISH
};
#define CAN_TX_FRAME_NBR		5
typedef struct __can_frame_tx_cache
{
	struct can_frame tx_frame;
	bool Flag;
}CAN_TX_CACHE;
typedef struct __can_frame_tx_queue
{
	CAN_TX_CACHE tx_frame_cache[CAN_TX_FRAME_NBR];
	uint8_t PutPtr;
	uint8_t GetPtr;
}CAN_TX_Q;
static CAN_TX_Q s_st_can0_tx_frame_q;
//canx发送帧缓存初始化
void can_x_frame_tx_cache_init(CAN_TX_Q *p_st_can_tx_cach,uint8_t nbr)
{
	uint8_t i1;
	for (i1 = 0;i1 < nbr;i1++)
	{
		memset(&p_st_can_tx_cach->tx_frame_cache[i1].tx_frame,0,sizeof(struct can_frame));
		p_st_can_tx_cach->PutPtr = 0;
		p_st_can_tx_cach->GetPtr = 0;
		p_st_can_tx_cach->tx_frame_cache[i1].Flag = false;
	}
}
//初始化can0缓存发送
void InitcCan0TxCache()
{
	can_x_frame_tx_cache_init(&s_st_can0_tx_frame_q,CAN_TX_FRAME_NBR);
}
//插入缓存到队列
//参数：待缓存帧
//返回：缓存成功或失败
bool put_can_tx_frame_to_cache(struct can_frame *p_st_canframe,CAN_TX_Q *p_st_can_tx_cache)
{
	bool res = false;
	if (p_st_can_tx_cache->tx_frame_cache[p_st_can_tx_cache->PutPtr].Flag == false)
	{
		memcpy(&p_st_can_tx_cache->tx_frame_cache[p_st_can_tx_cache->PutPtr].tx_frame,p_st_canframe,sizeof(struct can_frame));
		p_st_can_tx_cache->tx_frame_cache[p_st_can_tx_cache->PutPtr].Flag = true;
		p_st_can_tx_cache->PutPtr++;
		if (p_st_can_tx_cache->PutPtr >= CAN_TX_FRAME_NBR)
			p_st_can_tx_cache->PutPtr = 0;
		res = true;
	}
	return res;
}
//获取缓存到队列
//参数：获取缓存帧的变量
//返回：获取成功或失败
bool get_can_tx_frame_from_cache(struct can_frame *p_st_canframe,CAN_TX_Q *p_st_can_tx_cache)
{
	bool res = false;
	if (p_st_can_tx_cache->tx_frame_cache[p_st_can_tx_cache->GetPtr].Flag == true)
	{
		memcpy(p_st_canframe,&p_st_can_tx_cache->tx_frame_cache[p_st_can_tx_cache->GetPtr].tx_frame,sizeof(struct can_frame));
		p_st_can_tx_cache->tx_frame_cache[p_st_can_tx_cache->GetPtr].Flag = false;
		p_st_can_tx_cache->GetPtr++;
		if (p_st_can_tx_cache->GetPtr >= CAN_TX_FRAME_NBR)
			p_st_can_tx_cache->GetPtr = 0;
		res = true;
	}
	return res;
}
void Send_Message_To_Cammera(void)
{
	struct can_frame tx_frame;

	//打印当前函数所在文件名和行号
	Camera_CAN_Transmition(&tx_frame, stVehicleParas.fVehicleSpeed);			//发送车速到相机
	tx_frame.data[0] = 0x00;

	if(stVehicleParas.LeftFlag){
		tx_frame.data[0] |= 0x02;
	}

	if(stVehicleParas.RightFlag){
		tx_frame.data[0] |= 0x01;
	}

	if(stVehicleParas.BreakState){
		tx_frame.data[0] |= stVehicleParas.BreakState<<4;
	}

	if(stVehicleParas.ReverseGear){
		tx_frame.data[0] |= 0x40;
	}

	put_can_tx_frame_to_cache(&tx_frame,&s_st_can0_tx_frame_q);
}
void Send_Brake_Message_To_Info_Pool(void)
{
	struct can_frame tx_frame_to_pool;
	//打印当前函数所在文件名和行号
	CAN_Pool_Transmition(&tx_frame_to_pool);
	put_can_tx_frame_to_cache(&tx_frame_to_pool,&s_st_can0_tx_frame_q);

}
//canx限速发送控制状态机
//参数：canx	 	缓存队列
void canx_tx_pkg_machine(CAN_SFRmap* CANx,CAN_TX_Q *p_st_can_tx_cache)
{
	static uint8_t state = CAN_PKG_TX_CHK;
	static uint32_t mchn_time = 0;
	static struct can_frame st_canframe;
	switch(state)
	{
	case CAN_PKG_TX_CHK:		if (get_can_tx_frame_from_cache(&st_canframe,p_st_can_tx_cache) == true)
								{
									state = CAN_PKG_TX_SEND;
								}
								break;

	case CAN_PKG_TX_SEND:		CAN_Transmit_DATA(CANx, st_canframe);
								mchn_time = SystemtimeClock;
								state = CAN_PKG_TX_WAIT;
								break;

	case CAN_PKG_TX_WAIT:		if ((SystemtimeClock - mchn_time) >= 3)
								{
									state = CAN_PKG_TX_CHK;
								}
								break;

	default: 	state = CAN_PKG_TX_CHK;
				break;
	}
}
//can0的限速发送
void send_can0_pkg_machine()
{
	static uint32_t old_systime = 0;

	if(SystemtimeClock - old_systime > 100)
	{
		old_systime = SystemtimeClock;
		Send_Message_To_Cammera();
		Send_Brake_Message_To_Info_Pool();
	}
	canx_tx_pkg_machine(CAN0_SFR,&s_st_can0_tx_frame_q);

}

//--------------------------------------------------------------------------------------------------------
void Send_Speed_To_Urader(void)
{
	struct can_frame tx_frame;
	static uint32_t Uoldsystime = 0;

	if(SystemtimeClock - Uoldsystime > 100)
	{
		Uoldsystime 		= SystemtimeClock;

		Urader_CAN_Transmition(&tx_frame, stVehicleParas.fVehicleSpeed);		//发送车速到超声波雷达
		tx_frame.TargetID 	= 0x3f5;
		CAN_Transmit_DATA(CAN_UREADER, tx_frame);
	}
}

void Close_Interrupt_Function()
{
	// CAN0, CAN1, CAN2, CAN3, CAN4, CAN5
	Disable_CAN_Interrupt(CAN0_SFR);
	Disable_CAN_Interrupt(CAN1_SFR);
	Disable_CAN_Interrupt(CAN2_SFR);
	Disable_CAN_Interrupt(CAN3_SFR);
	Disable_CAN_Interrupt(CAN4_SFR);
	Disable_CAN_Interrupt(CAN5_SFR);

	// USART2, USART4
	USART_IDLE_INT_Enable(USART0_SFR,FALSE);//gcz   2022-06-04
//	Disable_Usart_Interrupt(USART0_SFR, INT_USART0);
//	Disable_Usart_Interrupt(USART1_SFR, INT_USART1);
	Disable_Usart_Interrupt(USART2_SFR, INT_USART2);
//	Disable_Usart_Interrupt(USART3_SFR, INT_USART3);
	Disable_Usart_Interrupt(USART4_SFR, INT_USART4);

	USART1_Bebug_Print("INT", "Disable Interrupt.", 1);
}

void Reopen_Interrupt_Function()
{
	Enable_CAN_Interrupt(CAN0_SFR);
	Enable_CAN_Interrupt(CAN1_SFR);
	Enable_CAN_Interrupt(CAN2_SFR);
	Enable_CAN_Interrupt(CAN3_SFR);
	Enable_CAN_Interrupt(CAN4_SFR);
	Enable_CAN_Interrupt(CAN5_SFR);

	USART_IDLE_INT_Enable(USART0_SFR,TRUE);//gcz   2022-06-04
//	Enable_Usart_Interrupt(USART0_SFR, INT_USART0);
//	Enable_Usart_Interrupt(USART1_SFR, INT_USART1);
	Enable_Usart_Interrupt(USART2_SFR, INT_USART2);
//	Enable_Usart_Interrupt(USART3_SFR, INT_USART3);
	Enable_Usart_Interrupt(USART4_SFR, INT_USART4);

	INT_All_Enable (TRUE);
	USART1_Bebug_Print("INT", "Enable Interrupt.", 1);
}

//gcz 2022-06-03
//滴答定时器配置初始化
void sys_tick_cfg()
{
	//gcz 2022-06-03  修改主优先级和子优先级，滴答定时器的主优先级应小于串口0，避免字节缺失影响通讯
	INT_Interrupt_Priority_Config(INT_SysTick,1,1);
	SysTick_Configuration(120000);		// 滴答定时器1ms一次中断
}


void Parameter_Init()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();

	Car_Can_Analysis_Init();	// 车CAN解析配置

	EC200U_INIT();			// EC200U-CN 4G模块初始化
	WheelSpeed_Init();		// init Calc Wheel Speed
	CarDvr_Param_Init();    //行车记录仪参数初始化
	Init_Config_Parameter();
	Init_Sys_Parameter();

	Valve_Init();
	if(m_sensor_mgt.m_dbl_cam.m_isOpen){
		Camera_Info_Init();
		Camera_Init();
	}
	Displayer_Init();

  initSrr();
  Param_Init();
  Parse_Init();
	sss_init();
	AEB_CMS_alg_init();

	// 若4G
	if(m_sensor_mgt.m_4g.m_isOpen){
		EC200U_Server_Init();			// Connect Server Init
		EC200U_Upgrade_Init();			// OTA upgrade Init
		EC200U_Data_Upload_Init();		// Data Uploade Init
	}else{
		GPS_Start();					// 开启GPS功能
	}

	Line_Upgrade_Init();			// Line Upgrade Init
	AlgParse_Init();				// AT CMD Init, 采用了第三防的SDK
	Init_Flash_Parameter();			//gcz 2022-06-06	将明珠屏蔽的许杰所需函数放到此处，因为其所需的车辆类型参数在AT_Init中从外部FLASH读取

	AEBS_Version_Init();
	InitWaringEventCache();			//gcz  2022-05-26初始化二进制协议事件的缓存队列
	InitcCan0TxCache();				//gcz  2022-06-01初始化can0的缓存发送队列
	InitUart0RcvCache();			//gcz  2022-06-02 配套优化lyj串口0缓存机制的缓存对象的初始化
	InitSpeedOptCache();			//gcz  2022-06-05初始化速度判断预测缓存
	init_propor_var();				//gcz  2022-06-09初始化比例阀参数
	// 防止小屏幕闪动
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_15, GPIO_MODE_OUT);		// PB15 -- 输出控制供电拐角
	GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_15, Bit_SET);

}

/*
 * 修订版本信息
 */
void Revised_Version()
{
	memset(upgrade_info.run_v, 0, VERSION_SIZE);
	sprintf(upgrade_info.run_v, "V%d.%d.%d.L%dC%dH%d", MAIN_V, SUB_V, REV_V, PRODUCT_N, UT_N, MMW_N);	// 正式

	// 设置运行版本
	if(!Set_App_Run_Version(upgrade_info.run_v)){
		Set_App_Run_Version(upgrade_info.run_v);
	}

	fprintf(USART1_STREAM, "------Run_V-------%s-------------\r\n",  upgrade_info.run_v);
}

/*
 * 平台升级
 */
void Program_Upgrade()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return ;

	// 线刷升级
	Line_Load_Upgrade();

	// OTA升级
	if(OTA_Upgrade_ASCII() != 0){
		// 上报车辆类型、AEB配置参数、GPS时间间隔信息
		Request_Vehicle_Config_Param_ASCII();

		GPS_Start();					// 开启GPS功能
		signal_strength.isStart = 1;	// 开启检测4G信号强度
	}
}
/*
 * 写升级包
 */
void Write_Flash()
{
	// 警示灯
	Upgrade_Warning_And_OTA_Overtime_Judge();

	// 写操作
	if(upgrade_info.upgrade_mode == UPGRADE_LINE){
		Analysis_DMA_USART1_Data();
		Line_Upgrade_Write_Operation();					// 线刷升级写操作
	}else{
//		if(upgrade_info.screen_isUpgrade){				// 小屏幕升级
//			Send_Package_To_Small_Screen_By_Can_Way();
//		}else{
			Analysis_DMA_USART0_Data();
			OTA_Sub_PKG_Dwn_Write_Data_In_Main();		// 下载升级包
//		}
	}
}

extern uint8_t test_ttt;
//安装检测程序所需状态机控制
void installation_detection_machine_state_ctrl()
{
	check_aebs_4g_all_detection_rsps_string();			//gcz 安装检测程序检测4G信号
	check_all_can_status();								//gcz 循环实时检测CAN总线状态
	show_rt_ult_radar_info();							//gcz 检测超声波雷达
	install_detection_4G_ctrl_machine();				//gcz 检测4G通讯
}

/*
 * 决定车辆类型分支
 */
uint32_t previousMillis = 0;
extern void show_jf1939_can_speed();
void Vehicle_Type_Branch()
{
	if(rParms.Vehicle_Speed_Obtain_Method == 0 || rParms.Vehicle_State_Obtain_Method == 0){
		Vehicle_Parameter_Analysis(SystemtimeClock);		//obd口报文解析及io端口输入解析
	}

	//show_jf1939_can_speed();								//gcz	2022-08-20	角雷达解放车CAN车速验证程序
	Got_Vehicle_Para_form_GPIO(&stVehicleParas);
	Vehicle_Turn_Signal_Keep(&stVehicleParas);

	Re_Init_Can();
}

/*
 * AEB设备的输入数据
 */
void AEB_Input_Data()
{
	// GPIO的输入
//	Calc_Bus_Encoder_Velocity();				// 轮速
	//gcz 2022-05-27安装检测程序
	installation_detection_machine_state_ctrl();

	// 协议的输入
	Analysis_DMA_USART1_Data();

	Analysis_AT_CMD_InMainLoop();
	Analysis_USART1_Config_CMD();
	Analysis_USART1_4G_CMD();
	Analysis_USART1_Data_Upload_CMD();
	Debug_Triggle_CMD();
	Clear_UART1_Buffer();

	//阀can报文解析
	Proprot_RxValve_Data_Analysis(SystemtimeClock);
	Urader_RxValve_Data_Analysis(SystemtimeClock);
	//zyh 20220712 Angle Radar Analysis
	processSrr();
	Parse_Process();
	/*处理行车记录仪的数据*/
	CAR_DVR_Message_Handle();
	// 适配不同车型分支
	Vehicle_Type_Branch();
	main_task();//gcz 2022-08-27
}

/*
 * AEB设备的输出数据
 */
extern float g_f_srr_fVehicleSpeed;
void AEB_Output_Data()
{
	// GPIO的输出
	Led_Control();								//显示灯控制
	Check_SysErr_Alarm();						//状态检测

	// 协议输出
	Sys_Send_Brake();							//报警及刹车
	Send_Display_Message();						//报警显示器显示信息

	Send_Car_Dvr_Cmd();							//发送行车记录仪命令 触发间隔10s

	send_can0_pkg_machine();					//gcz   2022-06-01 修改原有7B0和790帧的发送，合并到一起统一底层发送处理，让其间隔2~5ms发送
	Send_Speed_To_Urader();        // 超声波
	SendSrrCanInfo(&stVehicleParas,g_f_srr_fVehicleSpeed); // 角雷达
}

/*
 * 内存自检或者轮训
 */
void AEB_Internal_Loop()
{
	// 内存轮训获取
	Get_GPS_Info_In_2_Second();				// 定期获取GPS信息，时间间隔2秒
	Get_4G_Signal_Strength_In_4_Second();	// 获取4G信号强度，时间间隔4秒
	Accumulate_Car_Meliage_In_1_Second();	// 计算本次车辆行驶里程数，时间间隔1秒

	Upload_Info_Collection_ASCII();		// 车辆基本信息、车身信息、预警信息、GPS信息、心跳信息
	Upload_Data_To_Platform_By_Queue();

	// 内部轮训
//	Dynamic_Control_Megnetic_Valve();
}

void System_Init()
{
	SystemInit();
	sys_tick_cfg();

	// 用户调试串口
	USART_Init();
	GPIO_Init();

	W25QXX_Init();					// init external flash
}
#include "movement_trail_lib.h"
//主函数
void main()
{
	System_Init();							// 系统配置
	Revised_Version();						// 校准版本信息
	Sensor_Management_Init();				// 传感器管理配置参数
	Parameter_Init();						// 配置参数初始化
	//gcz 2022-08-27 新增框架，板级初始化
	system_board_init();//板级初始化
	//yuhong 20220817 move the CAN init here after all complete init
	Init_Vihecle_Type_Can();
	// 应用程序运行后，用于警示串口升级上位机
	USART1_Bebug_Print("START", "application is runing...", 1);		// 勿删除
	IWDT_Config(4000, IWDT_PRESCALER_64);	//独立看门狗配置 32KHZ 1：64分频，计数4000次， 8秒钟后溢出系统复位
	//g_sensor_mgt.m_imu.m_producer =3;

	whole_module_app_init();				//2022-09-14 所有子模块程序的初始化，不涉及硬件，这里目前仅有IMU状态检测功能
	while(1)
	{
		if(upgrade_info.rx_mode == RECV_BIN){
			Write_Flash();
		}else if(ToolingFlag == 1){  		//工装测试模式
			Tooling_In_MainLoop();
			IWDT_Feed_The_Dog();
		}else{
			dispose_4g_rcv_msg();
			// 连接服务器
			Connect_Server();
			Program_Upgrade();

			AEB_Input_Data();
			AEB_Internal_Loop();
			AEB_Output_Data();
			IWDT_Feed_The_Dog();
		}
	}
}
