/*
 * kunlun_fcw_ldw.h
 *
 *  Created on: 2022-4-16
 *      Author: zkhy
 */
#include "common.h"
#include "aeb_cms_alg.h"
#ifndef KUNLUN_FCW_LDW_H_
#define KUNLUN_FCW_LDW_H_

void AEB_TTC_warning(Obstacle_Information* cipv,
		Obstacle_Basic_Data *obs_basic_data,
					float level1_time_gap,
					float level2_time_gap,
					float aeb_break_dec_set,
					float aeb_break_stop_distance_set,
					float air_break_delay_time,
					uint8_t* fcw_warning);
void CMS_HMW_warning(_VEHICLE_PARA* stVehicleParas,uint8_t TrackNumber,uint8_t min_tracknumber,float hmw_thr, float hmw, uint8_t warning_grade, uint8_t appr_ornot);
void CMS_TTC_warning(_VEHICLE_PARA* stVehicleParas,uint8_t TrackNumber,uint8_t min_tracknumber,float cms_ttc_warning_thr, float aeb_ttc_thr_L1,float aeb_ttc_thr_L2,float ttc);
uint8_t LDW_Lane_Departure_Warning(_VEHICLE_PARA* stVehicleParas,Camera_Info camera_share,Camera_LDW_data camera_ldw_data);
#endif /* KUNLUN_FCW_LDW_H_ */
