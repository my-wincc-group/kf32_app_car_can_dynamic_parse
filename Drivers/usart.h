/**
  ******************************************************************************
  *
  * 文件名  Usart.h
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了串口例程相关配置函数
  *
  ******************************************************************************/

#ifndef USART_H_
#define USART_H_
#include "system_init.h"
#include "upgrade_common.h"

//#define	BLUETOOTH_STREAM			USART4_STREAM
#define EC200U_BAUD_OTA				19200
#define EC200U_BAUD					230400 	// 115200 //230400
#define USART1_BAUD					115200
#define USART2_BAUD					115200
//#define USART3_BAUD					115200
#define USART4_BAUD					9600

#define RXBUFFER_SIZE				1024

/* ------------------------ 全局变量 external ------------------------------- */
extern uint8_t 				User_Rxbuffer[RXBUFFER_SIZE];
extern volatile uint16_t 	User_Rxcount;
//gcz 2022-05-27-------------------
extern uint8_t 				User_Rxbuffer_BT[256];
extern volatile uint16_t 	User_Rxcount_BT;

/* ------------------------全局函数------------------------------- */
void 	USART_Init();
void 	USART0_Init();
void 	USART1_Init();
void 	USART_Async_config(USART_SFRmap *USARTx,uint32_t baud);						// 串口异步全双工配置
void 	USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);		// 串口发送函数
void 	Clear_UART1_Buffer();					// 清空USART1用户缓冲区
void 	Enable_Usart_Interrupt(USART_SFRmap *USARTx, InterruptIndex Peripheral);
void 	Disable_Usart_Interrupt(USART_SFRmap *USARTx, InterruptIndex Peripheral);
//gcz 2022-05-27
void 	Clear_UART4_Buffer();
void 	USART1_Bebug_Print(uint8_t *mark_info, uint8_t *string, uint8_t use_dma);
void 	USART1_Bebug_Print_Num(uint8_t *mark_info, uint32_t num, uint8_t print_type, uint8_t use_dma);
void 	USART1_Bebug_Print_Flt(uint8_t *mark_info, float float_data, uint8_t print_type, uint8_t use_dma);
extern void Usart1_DMA_Transmit(volatile uint8_t *pData,uint16_t nSize);
void  USART1_Bebug_Print_float(uint8_t *mark_info, float num, uint8_t flag);
#endif /* USART_H_ */
