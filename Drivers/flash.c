/**
  ********************************************************************
  * 文件名  flash.c
  * 作  者   ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了flash应用读写函数。
  *
  *********************************************************************
*/
#include "flash.h"
#include "usart.h"

FLASH_ProgramTypeDef flash_write;
uint8_t internal_flash_buf[IN_FLASH_SECTOR_SIZE] = {0};

#if 0
//uint32_t data[256] ;
//void FLASH_Read_NByte(uint32_t page_address,uint8_t *p_DataBuffer,uint32_t offset,uint32_t leng)
//{
//	uint32_t i;
//	for(i=0;i<256;i++)//一页1024byte
//	{
//		data[i]=FLASH_ReadWord(page_address+i*4,FLASH_PROGRAM_CODE);
//	}
//	memcpy ( p_DataBuffer,(uint8_t *)data+offset, leng);
//
//}
//void FLASH_Modify_NByte(uint32_t page_address,uint8_t *p_DataBuffer,uint32_t offset,uint32_t leng)
//{
//
//	uint32_t i;
//	for(i=0;i<256;i++)//一页1024byte
//	{
//		data[i]=FLASH_ReadWord(page_address+i*4,FLASH_PROGRAM_CODE);
//	}
//	memcpy ( (uint8_t *)data +offset, p_DataBuffer, leng);
//
//    //程序区整页写入数据
//	FLASH_PageWrite_fun( page_address, data, 128);
//}


/*
 * 打印输出页的数据
 * 参数1：打印数据
 * 参数2：打印数据长度	页大小256B
 */
//void print_page_data(uint32_t *buff, uint16_t length)
//{
//	uint8_t read_data[length];
//	uint8_t times = length/16;	// 因为页大小为256B，uint32_t大小为4B，所以总共取数据256/4=64次
//	for(uint8_t i=0; i<times; i++){
//		read_data[i*16] 	= buff[i*4]>>8;
//		read_data[i*16+1] 	= buff[i*4];
//		read_data[i*16+2] 	= buff[i*4]>>24;
//		read_data[i*16+3] 	= buff[i*4]>>16;
//
//		read_data[i*16+4] 	= buff[i*4+1]>>8;
//		read_data[i*16+5] 	= buff[i*4+1];
//		read_data[i*16+6] 	= buff[i*4+1]>>24;
//		read_data[i*16+7] 	= buff[i*4+1]>>16;
//
//		read_data[i*16+8] 	= buff[i*4+2]>>8;
//		read_data[i*16+9] 	= buff[i*4+2];
//		read_data[i*16+10] 	= buff[i*4+2]>>24;
//		read_data[i*16+11] 	= buff[i*4+2]>>16;
//
//		read_data[i*16+12] 	= buff[i*4+3]>>8;
//		read_data[i*16+13] 	= buff[i*4+3];
//		read_data[i*16+14] 	= buff[i*4+3]>>24;
//		read_data[i*16+15] 	= buff[i*4+3]>>16;
//	}
//	fprintf(USART1_STREAM, "FLASH_BUFFER_Read:");
//	for(int w=0; w<length; w++){
//		fprintf(USART1_STREAM, "%02X ", read_data[w]);
//	}
//	fprintf(USART1_STREAM, "\r\n");
//}

/**
  * 描述  flash 程序区写单个双字节数据（写入两个32位数据）。
  * 输入  address： 指定flash  程序区地址
  *  p_FlashBuffer: 写入数据的指针
  *
  * 返回  无。
  * 写flash注意说明：页擦会把指定地址所在的一页128个双字地址的数据都擦除掉
  */
//void FLASH_WriteCODE_ONE(uint32_t address,uint32_t *p_FlashBuffer)
//{
//
//	//	//--------------------------------------------------------------------------------------------
//		FLASH_Wipe_Configuration_RAM(FLASH_WIPE_CODE_PAGE,address);//程序区页擦
//
//		flash_write.m_Mode=FLASH_PROGRAM_WORD;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CODE;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=1;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=p_FlashBuffer;
//		FLASH_Program_Configuration_RAM(&flash_write);
//}

/*
*
* 描述：程序区半页编程，按半页或小于半页数据写入数据到FLASH，个数参数1~63，地址为页的首地址 如十六进制下结尾000 400 800 C00
* 			如果地址不是页的首地址，必须确定后续页结果为0xFFFF，或前面操作过页首写，使后续块值被0xFFFF，否则写结果异常。
* 输入参数：address 待写地址
*          p_FlashBuffer 待写地址的数据指针
*          length        待写数据长度取值：1~63
*  （一次写64位的双字节，如设最大63时，实际最大写入（63+1）*2=128个32位的数据，如没有补齐数据，系统会生成随机数一起写入flash）
* 返回          ：无
* 写flash注意说明：页擦会把指定地址所在的那一页128个双字地址的数据都擦除掉，
*/
//void FLASH_HALFPAGE_WRITECODE_fun(uint32_t address,uint32_t *p_FlashBuffer,uint32_t length)
//{
//
//		FLASH_Wipe_Configuration_RAM(FLASH_WIPE_CODE_PAGE,address);//程序区页擦
//
//		flash_write.m_Mode=FLASH_PROGRAM_HALF_PAGE;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CODE;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=length;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=p_FlashBuffer;
//		FLASH_Program_Configuration_RAM(&flash_write);
//}

/*
*
* 描述：程序区按页写入数据到FLASH，个数参数只能为128 ，地址必须为页的首地址 如十六进制下结尾000 400 800 C00
* 			如果地址不是页的首地址，必须确定后续块结果为0xFFFF，或前面操作过块首写，使后续页值被0xFFFF，否则写结果异常。
* 输入参数：address 待写地址
*          p_FlashBuffer 待写地址的数据指针
*          length        待写数据长度(个数参数只能为128)
* 返回          ：无
* 写flash注意说明：页擦会把指定地址所在的一页128个双字地址的数据都擦除掉，半页编程只能64个双字进行写操作，页写需要进行两次半页写。
*/
//void FLASH_PageWrite_fun(uint32_t address,uint32_t *p_FlashBuffer,uint8_t length)
//{
//	volatile uint8_t i;
//	static uint32_t Flash_Buff1[128];
//	static uint32_t Flash_Buff2[128];
//
//	//	//--------------------------------------------------------------------------------------------
//		FLASH_Wipe_Configuration_RAM(FLASH_WIPE_CODE_PAGE,address);//程序区页擦
//
//		//--------------------------------------------------------------------------------------------
//		for (i = 0; i < length; i++)
//		{
//			Flash_Buff1[i] =*p_FlashBuffer;
//			p_FlashBuffer ++;
//		}
//
//		flash_write.m_Mode=FLASH_PROGRAM_HALF_PAGE;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CODE;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=63;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=Flash_Buff1;
//		FLASH_Program_Configuration_RAM(&flash_write);
//
//
//		for (i = 0; i < length; i++)
//		{
//			Flash_Buff2[i] = *p_FlashBuffer;
//			p_FlashBuffer ++;
//		}
//
//		flash_write.m_Mode=FLASH_PROGRAM_HALF_PAGE;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CODE;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address+0x200;
//		flash_write.m_WriteSize=63;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=Flash_Buff2;
//		FLASH_Program_Configuration_RAM(&flash_write);
//}


/**
  * 描述  flash 程序区读多字节数据
  * 输入  address： 指定flash地址
  *  p_FlashBuffer: 写入数据的指针
  *
  * 返回  无。
  */
//void FLASH_READCODE_fun(uint32_t address,uint32_t *p_FlashBuffer,uint32_t length)
//{
//
//	volatile uint32_t read_num;
//
//	for(read_num=0;read_num<length;read_num++)
//	{
//		*p_FlashBuffer=Read_Flash_or_CFR_RAM(address,FLASH_PROGRAM_CODE);
//		p_FlashBuffer++;
//		address=address+0x0004;
//	}
//}






/*******************************************信息用户区读写函数*********************************************/
/**
  * 描述  flash 信息用户区写单个双字节数据（写入两个32位数据）。
  * 输入  address： 指定flash用户区地址（0x1C00~0X1FF8）地址十六进制下结尾0x1C00 0x1C08  0x1E00 0x1E08
  * p_FlashBuffer: 写入数据的指针
  *
  * 返回  无。
  * 写flash注意说明：页擦会把指定地址所在的一页128个双字地址的数据都擦除掉
  */
//void FLASH_WriteCFG_ONE(uint32_t address,uint32_t *p_FlashBuffer)
//{
//
//	//	//--------------------------------------------------------------------------------------------
//		FLASH_Wipe_Configuration_RAM(FLASH_WIPE_CFG_PAGE,address);//程序区页擦
//
//		flash_write.m_Mode=FLASH_PROGRAM_WORD;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CFG;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=1;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=p_FlashBuffer;
//		FLASH_Program_Configuration_RAM(&flash_write);
//}

/*
* 描述：用户区半页编程，按半页或小于半页数据写入数据到FLASH，个数参数1~63，地址为页的首地址 如十六进制下结尾000 400 800 C00
* 			如果地址不是页的首地址，必须确定后续页结果为0xFFFF，或前面操作过页首写，使后续块值被0xFFFF，否则写结果异常。
* 输入参数：address 指定用户区地址（0x1C00~0X1FF8）
*          p_FlashBuffer 待写地址的数据指针
*          length        待写数据长度取值：1~63
*  （一次写64位的双字节，如设最大63时，实际最大写入（63+1）*2=128个32位的数据，如没有补齐数据，系统会生成随机数一起写入flash）
* 返回          ：无
* 写flash注意说明：页擦会把指定地址所在的那一页128个双字地址的数据都擦除掉，
*/
//void FLASH_HALFPAGE_WRITECFG_fun(uint32_t address,uint32_t *p_FlashBuffer,uint32_t length)
//{
//
//		FLASH_Wipe_Configuration_RAM(FLASH_WIPE_CFG_PAGE,address);//程序区页擦
//
//		flash_write.m_Mode=FLASH_PROGRAM_HALF_PAGE;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CFG;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=length;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=p_FlashBuffer;
//		FLASH_Program_Configuration_RAM(&flash_write);
//}


/*
*
* 描述：用户区按页写入数据到FLASH，个数参数只能为128 ，地址必须为页的首地址 如十六进制下结尾000 400 800 C00
* 			如果地址不是页的首地址，必须确定后续块结果为0xFFFF，或前面操作过块首写，使后续页值被0xFFFF，否则写结果异常。
* 输入参数：address 待写地址因目前开放的只有最后一个PAGE 因此首地址只能从0x1C00开始
*          p_FlashBuffer 待写地址的数据指针
*          length        待写数据长度(个数参数只能为128)
* 返回          ：无
* 写flash注意说明：页擦会把指定地址所在的一页128个双字地址的数据都擦除掉，半页编程只能64个双字进行写操作，页写需要进行两次半页写。
*/
//void FLASH_PageWrite_CFG_fun(uint32_t address,uint32_t *p_FlashBuffer,uint8_t length)
//{
//	volatile uint8_t i;
//	static uint32_t Flash_Buff1[128];
//	static uint32_t Flash_Buff2[128];
//
//	//	//--------------------------------------------------------------------------------------------
//		FLASH_Wipe_Configuration_RAM(FLASH_WIPE_CFG_PAGE,address);//信息用户区页擦
//
//		//--------------------------------------------------------------------------------------------
//		for (i = 0; i < length; i++)
//		{
//			Flash_Buff1[i] =*p_FlashBuffer;
//			p_FlashBuffer ++;
//		}
//
//		flash_write.m_Mode=FLASH_PROGRAM_HALF_PAGE;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CFG;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=63;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=Flash_Buff1;
//		FLASH_Program_Configuration_RAM(&flash_write);
//
//
//		for (i = 0; i < length; i++)
//		{
//			Flash_Buff2[i] = *p_FlashBuffer;
//			p_FlashBuffer ++;
//		}
//
//		flash_write.m_Mode=FLASH_PROGRAM_HALF_PAGE;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CFG;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address+0x200;
//		flash_write.m_WriteSize=63;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=Flash_Buff2;
//		FLASH_Program_Configuration_RAM(&flash_write);
//
//}


/**
  * 描述  flash 程序区写单字节数据
  * 输入  address： 指定flash地址
  *  p_FlashBuffer: 写入数据的指针
  *
  * 返回  无。
  * 注：写入前要先擦除地址所在的页
  */
//void FLASH_WriteByte(uint32_t address,uint8_t p_FlashBuffer)
//{
//	    uint32_t num32[2];
//	    uint32_t *flash_write_num;
//	    num32[0]=p_FlashBuffer;
//	    num32[1]=0;
//		flash_write_num=num32;
//		//--------------------------------------------------------------------------------------------
//
//	    flash_write.m_Mode=FLASH_PROGRAM_WORD;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CODE;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=1;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=flash_write_num;
//		FLASH_Program_Configuration_RAM(&flash_write);
//}

//void FLASH_WriteByte_4(uint32_t address,uint8_t buffer1,uint8_t buffer2,uint8_t buffer3,uint8_t buffer4)
//{
//	    uint32_t num32[1];
//	    uint32_t *flash_write_num;
//	    num32[0]=buffer1<<24|buffer2<<16|buffer3<<8|buffer4;
////	    num32[1]=0;
//		flash_write_num=num32;
//		//--------------------------------------------------------------------------------------------
//
//	    flash_write.m_Mode=FLASH_PROGRAM_WORD;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CODE;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=1;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=flash_write_num;
//		FLASH_Program_Configuration_RAM(&flash_write);
//}

/**
  * 描述  flash 程序区写半字数据
  * 输入  address： 指定flash地址
  *  p_FlashBuffer: 写入数据的指针
  *
  * 返回  无。
  * 注：写入前要先擦除地址所在的页
  */
//void FLASH_WriteHalfWord(uint32_t address,uint16_t p_FlashBuffer)
//{
//	    uint32_t num32[2];
//	    uint32_t *flash_write_num;
//	    num32[0]=p_FlashBuffer;
//	    num32[1]=0;
//		flash_write_num=num32;
//		//--------------------------------------------------------------------------------------------
//
//	    flash_write.m_Mode=FLASH_PROGRAM_WORD;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CODE;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=1;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=flash_write_num;
//		FLASH_Program_Configuration_RAM(&flash_write);
//}

/**
  * 描述  flash 程序区写单字数据
  * 输入  address： 指定flash地址
  *  p_FlashBuffer: 写入数据的指针
  *
  * 返回  无。
  * 注：写入前要先擦除地址所在的页
  */
//void FLASH_WriteWord(uint32_t address,uint32_t p_FlashBuffer)
//{
//	    uint32_t num32[2];
//	    uint32_t *flash_write_num;
//	    num32[0]=p_FlashBuffer;
//	    num32[1]=0;
//		flash_write_num=num32;
//		//--------------------------------------------------------------------------------------------
//
//	    flash_write.m_Mode=FLASH_PROGRAM_WORD;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
//		flash_write.m_Zone=FLASH_PROGRAM_CODE;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
//		flash_write.m_Addr=address;
//		flash_write.m_WriteSize=1;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
//		flash_write.m_Data=flash_write_num;
//		FLASH_Program_Configuration_RAM(&flash_write);
//}

/**
  * 描述  flash 程序区写多个字节数据
  * 输入  address： 指定flash地址
  *  p_FlashBuffer: 写入数据的指针
  *    leng：数据的长度
  * 返回  无。
  * 注：写入前要先擦除地址所在的页
  */
//void FLASH_WriteNByte(uint32_t address,uint8_t *p_FlashBuffer,uint32_t leng)//地址必须为被8整除
//{
//	uint32_t i;
//	for(i=0;i<leng;i++)//一页1024byte，缓冲一次写64bit=8byte，128个缓冲块
//	{
//		FLASH_WriteByte(address,*p_FlashBuffer);//
//		p_FlashBuffer++;
//		address+=8;
//	}
//}


//void FLASH_WriteNByte(uint32_t address,uint8_t *p_FlashBuffer,uint32_t leng)//地址必须为被8整除
//{
//	uint32_t i;
//	uint16_t num 	= leng/4;
//	uint16_t remain = leng%4;
//	if(num > 0)
//	{
//		for(i=0;i<leng;i+=4)//一页1024byte，缓冲一次写64bit=8byte，128个缓冲块
//		{
//			FLASH_WriteByte_4(address,p_FlashBuffer[i],p_FlashBuffer[i+1],p_FlashBuffer[i+2],p_FlashBuffer[i+3]);//
//			p_FlashBuffer+4;
//			address+=8;
//		}
//	}
//
//	if(remain > 0)
//	{
//		switch(remain)
//		{
//		case 1:FLASH_WriteByte_4(address,p_FlashBuffer[i],0,0,0); break;
//		case 2:FLASH_WriteByte_4(address,p_FlashBuffer[i],p_FlashBuffer[i+1],0,0); break;
//		case 3:FLASH_WriteByte_4(address,p_FlashBuffer[i],p_FlashBuffer[i+1],p_FlashBuffer[i+2],0); break;
//		}
//	}
//}


/**
  * 描述  flash 程序区读单字节数据
  * 输入  address： 指定flash地址
  *  p_FlashBuffer: 读出数据的指针
  *
  * 返回  read_buf:读出单字节数据
  *
  */
//uint32_t FLASH_ReadByte(uint32_t address,uint8_t *p_FlashBuffer) //读代码区
//{
//	uint32_t read_buf;
//	read_buf=Read_Flash_or_CFR_RAM(address,FLASH_PROGRAM_CODE);
//	*p_FlashBuffer=read_buf;
//
//	return read_buf;
//}

/**
  * 描述  flash 程序区读半字数据
  * 输入  address： 指定flash地址
  *  p_FlashBuffer: 读出数据的指针
  *
  * 返回  read_buf:读出半字数据
  *
  */
//uint32_t FLASH_ReadHalWord(uint32_t address,uint16_t *p_FlashBuffer)
//{
//	uint32_t read_buf;
//	read_buf=Read_Flash_or_CFR_RAM(address,FLASH_PROGRAM_CODE);
//	*p_FlashBuffer=read_buf;
//
//	return read_buf;
//}

/**
  * 描述  flash 程序区读单字数据
  * 输入  address： 指定flash地址
  *  p_FlashBuffer: 读出数据的指针
  *
  * 返回  read_buf:读出单字数据
  *
  */
//uint32_t FLASH_ReadWord(uint32_t address,uint32_t *p_FlashBuffer)
//{
//	uint32_t read_buf;
//	read_buf=Read_Flash_or_CFR_RAM(address,FLASH_PROGRAM_CODE);
//	*p_FlashBuffer=read_buf;
//
//	return read_buf;
//}

/**
  * 描述  flash 程序区多次读单字节数据
  * 输入  address： 指定flash地址
  *  p_FlashBuffer: 读出数据的指针
  *   leng：指定数据长度
  * 返回  无
  *
  */
//void FLASH_ReadNByte(uint32_t address,uint8_t *p_FlashBuffer,uint32_t leng)//地址必须为被8整除
//{
//	uint32_t i=0;
//	uint32_t read_buf;
//	for(i=0;i<leng;i++)//一页1024byte，缓冲一次写64bit=8byte，128个缓冲块
//	{
//		read_buf=Read_Flash_or_CFR_RAM(address,FLASH_PROGRAM_CODE);
//		*p_FlashBuffer=read_buf;
//		p_FlashBuffer++;
//		address+=8;
//	}
//}
#endif
/**
  * 描述  flash 程序区写8个字字数据
  * 输入  address： 指定flash地址
  * p_FlashBuffer1:前四个字节
  * p_FlashBuffer2:后四个字节
  * 返回  无。
  * 注：写入前要先擦除地址所在的页
  */
void FLASH_WriteWord_8_BYTE(uint32_t address, uint32_t p_FlashBuffer1, uint32_t p_FlashBuffer2)
{
	uint32_t num32[2];
	uint32_t *flash_write_num;
	num32[0]=p_FlashBuffer1;
	num32[1]=p_FlashBuffer2;
	flash_write_num=num32;

	flash_write.m_Mode=FLASH_PROGRAM_WORD;//FLASH_PROGRAM_HALF_PAGE //FLASH_PROGRAM_WORD
	flash_write.m_Zone=FLASH_PROGRAM_CODE;//FLASH_PROGRAM_CFG  FLASH_PROGRAM_CODE
	flash_write.m_Addr=address;
	flash_write.m_WriteSize=1;//半页编程不得为0，如果写1（+1）实际写的是2个双字 单字编程无影响
	flash_write.m_Data=flash_write_num;
	FLASH_Program_Configuration_RAM(&flash_write);
}

/*
 * 内部FLASH中写多个扇区（单个扇区大小为1024B）
 * 参数1：地址
 * 参数2：数据指针
 * 参数3：写扇区的个数
 */
//void Interal_Flash_8_Byte_to_Write_one_Sector(uint32_t write_addr,uint8_t *write_buff, uint8_t sector_num)
void Interal_Flash_8_Byte_to_Write_one_Sector(uint32_t write_addr,uint8_t *write_buff)
{
	volatile uint32_t buf32_front	= 0;
	volatile uint32_t buf32_back		= 0;

	//程序区页擦划分1K 空间  page202=202*1024=0x0003 2800;
	FLASH_Wipe_Configuration_RAM(FLASH_WIPE_CODE_PAGE, write_addr);

	for(uint8_t i =0; i<IN_FLASH_SIZE_128; i++){	// 每次写1KB，对4个字节进行操作
		uint16_t id = i * IN_FLASH_SIZE_8;
		buf32_front	= write_buff[id]<<0 | write_buff[id+1]<<8 | write_buff[id+2]<<16 | write_buff[id+3]<<24;
		buf32_back	= write_buff[id+4]<<0 | write_buff[id+5]<<8 | write_buff[id+6]<<16 | write_buff[id+7]<<24;

		FLASH_WriteWord_8_BYTE(write_addr, buf32_front, buf32_back);	// 一次写入8个Byte,64Bit
		write_addr += IN_FLASH_SIZE_8;
	}
}
/*
 * 在内部FLASh读取1024B一个扇区大小的数据
 * 参数1：地址
 * 参数2：数据指针
 */
void Interal_Flash_8_Byte_To_Read_One_Sector(uint32_t read_addr, uint8_t *read_buff)
{
	volatile uint32_t data;
	for(uint16_t i=0; i<IN_FLASH_SIZE_256; i++){
		data = Read_Flash_or_CFR_RAM(read_addr, FLASH_PROGRAM_CODE);	// read 4 Byte

		read_buff[i*4] 		= data & 0xFF;
		read_buff[i*4+1] 	= (data>>8) & 0xFF;
		read_buff[i*4+2] 	= (data>>16) & 0xFF;
		read_buff[i*4+3] 	= (data>>24) & 0xFF;
		read_addr += 4;
	}
}

/*
 * 添加了校验的写FLASH
 * 参数1：数据指针
 * 参数2：地址
 * 参数3：写长度，要求必须是8的倍数
 */
uint8_t Internal_Flash_Write_Chk(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
	if(NumByteToWrite % 4 != 0){
		USART1_Bebug_Print("ERROR", "The Length Has To Be A Multiple Of 4.", 1);
		return 0;
	}

	// 将所有写数据相加求和，取最后一个字节作为校验
	uint8_t chk_sum = 0;
	// 去掉1B校验和1B的\0
	for(uint16_t j=0; j<NumByteToWrite-2; j++){
		chk_sum += pBuffer[j];
	}
//	fprintf(USART1_STREAM, "write:%02X\r\n", chk_sum);
	// 存储时将校验存放到最后1字节
	if(chk_sum == 0xFF) chk_sum = 0xFE;	// 为了区分格式化的FLASH的默认数据时0xFF
	pBuffer[NumByteToWrite-2] = chk_sum;
	pBuffer[NumByteToWrite-1] = '\0';

	// 测试打印
//	fprintf(USART1_STREAM, "\r\n------In FLASH------write-----------\r\n");
//	for(uint16_t i=0; i< NumByteToWrite; i++){
//		fprintf(USART1_STREAM, "%02X ",pBuffer[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n------------write-----------\r\n");

	uint32_t pos	= WriteAddr / IN_FLASH_SECTOR_SIZE;		//扇区地址
	uint16_t off	= WriteAddr % IN_FLASH_SECTOR_SIZE;		//在扇区内的偏移
	memset(internal_flash_buf, 0, IN_FLASH_SECTOR_SIZE);

	Interal_Flash_8_Byte_To_Read_One_Sector(pos*IN_FLASH_SECTOR_SIZE, internal_flash_buf);//读出整个扇区的内容 SECTOR_SIZE

	for(uint16_t i=0; i< NumByteToWrite; i++)	   		//复制
	{
		internal_flash_buf[off+i] = pBuffer[i];
	}

	/* 写入整个扇区 */
	Interal_Flash_8_Byte_to_Write_one_Sector(pos*IN_FLASH_SECTOR_SIZE, internal_flash_buf);
	return 1;
}


/*
 * 加了校验的读取FLASH数据
 * 仅是针对普通的参数配置
 * 参数1：数据指针
 * 参数2：地址
 * 参数3：数据长度，要求必须是4的倍数
 */
uint8_t Internal_Flash_Read_Chk(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
	if(NumByteToRead % 4 != 0){
		USART1_Bebug_Print("ERROR", "The Length Has To Be A Multiple Of 4.", 1);
		return 0;
	}

	uint32_t data;
	for(uint16_t i=0; i < NumByteToRead/4; i++){
		data = Read_Flash_or_CFR_RAM(ReadAddr, FLASH_PROGRAM_CODE);	// read 4 Byte
		pBuffer[i*4] = data&0xFF;
		pBuffer[i*4+1] = (data>>8)&0xFF;
		pBuffer[i*4+2] = (data>>16)&0xFF;
		pBuffer[i*4+3] = (data>>24)&0xFF;
		ReadAddr += 4;
	}

	// 测试打印
//	fprintf(USART1_STREAM, "\r\n----In Flash--------read----1-------\r\n");
//	for(uint16_t j=0; j<NumByteToRead; j++){
//		fprintf(USART1_STREAM, "%02X ", pBuffer[j]);
//	}
//	fprintf(USART1_STREAM, "\r\n------------read----1-------\r\n");
    // 去掉无用的,计算结束符号
	uint16_t end = 0;
	for(end=0; end<NumByteToRead; end++){
		if(pBuffer[end]==0xFF && pBuffer[end+1]==0xFF && pBuffer[end+2]==0xFF && pBuffer[end+3]==0xFF){	// 去除0XFF空字符
			break;
		}
	}
	// 若被清空扇区后
	if(end == 0) {
		memset(pBuffer, 0, NumByteToRead);
		return 0;
	}

	// 获取校验字节
	uint8_t src_Chk = pBuffer[end-2];

	// 获取校验值
	uint8_t chk_sum = 0;
	for(uint16_t j=0; j<end-2; j++){
		chk_sum += pBuffer[j];
	}
	if(chk_sum == 0xFF) chk_sum = 0xFE;

//	fprintf(USART1_STREAM, "\r\nchk:%02X, src:%02X\r\n", chk_sum, src_Chk);
	// 校验
	if(chk_sum == src_Chk){
		pBuffer[end-2] = '\0';
//		fprintf(USART1_STREAM, "read chk success\r\n\r\n");
		return 1;
	}

	fprintf(USART1_STREAM, "\r\n------------read----E-------\r\n");
	for(uint16_t j=0; j<NumByteToRead; j++){
		fprintf(USART1_STREAM, "%02X ", pBuffer[j]);
	}
	fprintf(USART1_STREAM, "\r\n------------read----E-------\r\n");

	memset(pBuffer, 0, NumByteToRead);

	return 0;
}

/*
 * 获取刷机方式
 * 返回： 1：线刷升级；0第三方工具刷机
 */
uint8_t	Get_Flash_Way_InFlash()
{
	uint8_t flash_way[FLASH_WAY_SIZE] = {0};

	if(Internal_Flash_Read_Chk(flash_way, IN_FLASH_PARAM_UPGRADE_FLAG_START, FLASH_WAY_SIZE)){
		uint8_t way = flash_way[0];
		if(way == 0xFF) way = 0;
		return way;
	}

	return 0;
}
/*
 * 设置刷机方式
 * 参数1：1：线刷升级；0第三方工具刷机
 */
uint8_t	Set_Flahs_Way_InFlash(uint8_t way)
{
	uint8_t flash_way[FLASH_WAY_SIZE] = {0};
	flash_way[0] = way;
	flash_way[1] = 0;
	flash_way[2] = 0;
	flash_way[3] = 0;

	Internal_Flash_Write_Chk(flash_way, IN_FLASH_PARAM_UPGRADE_FLAG_START, FLASH_WAY_SIZE);
	memset(flash_way, 0, FLASH_WAY_SIZE);
	return Internal_Flash_Read_Chk(flash_way, IN_FLASH_PARAM_UPGRADE_FLAG_START, FLASH_WAY_SIZE);
}
/*
 * 在内部FLASH中设置传感器参数配置
 * 参数1：参数数据
 * 说明：将参数大小设置成了固定长度为512B
 */
uint8_t Set_Sensor_MGT_Param_InFlash(uint8_t *mgt_param)
{
//	uint16_t store_size = strlen(mgt_param) / 4 * 8;
	uint16_t store_size = SENSOR_CFG_SIZE;
	Internal_Flash_Write_Chk(mgt_param, IN_FLASH_PARAM_SENSOR_MGT_START, store_size);

	memset(mgt_param, 0, strlen(mgt_param));
	return Internal_Flash_Read_Chk(mgt_param, IN_FLASH_PARAM_SENSOR_MGT_START, store_size);
}

/*
 * 在内部FLASH中获取传感器参数配置
 * 参数1：参数数据
 * 说明：将参数大小设置成了固定长度为512B
 */
uint8_t Get_Sensor_MGT_Param_InFlash(uint8_t *mgt_param)
{
//	uint16_t store_size = strlen(mgt_param) / 4 * 8;
	uint16_t store_size = SENSOR_CFG_SIZE;
	return Internal_Flash_Read_Chk(mgt_param, IN_FLASH_PARAM_SENSOR_MGT_START, store_size);
}
