/*
 * circular_queue.h
 *
 *  Created on: 2022-1-18
 *      Author: zkhy
 */

#ifndef CIRCULAR_QUEUE_H_
#define CIRCULAR_QUEUE_H_
#include "common.h"

typedef struct{
	// ID for Target
	uint8_t cipv_ID;
	//Parameters
	uint8_t size_of_queue;
	uint8_t size_double;
	uint8_t right_now_head;
	uint8_t next_head_index;
	uint8_t next_tail_index;
	// flag
	uint8_t is_full;
	uint8_t is_empty;
	uint8_t num_of_member;//not full
	// data
	uint8_t max_effective_size;
	float data[20];
}Circular_Queue;

uint8_t Circular_Queue_Init(Circular_Queue* queue, uint8_t size_set);
uint8_t In_Queue(Circular_Queue* queue, float data);
void 	Clean_Queue(Circular_Queue* queue);
uint8_t Circular_is_empty(Circular_Queue* queue);
uint8_t Circular_is_Full(Circular_Queue* queue);
float 	Get_data(Circular_Queue* queue, uint8_t index);
#endif /* CIRCULAR_QUEUE_H_ */
