/*
 * EC200U.h
 *
 *  Created on: 2021-7-10
 *      Author: chenq
 */

#ifndef EC200U_H_
#define EC200U_H_
#include "system_init.h"
#include "upgrade_common.h"

#define EC200U_POWERON(x)  GPIO_Set_Output_Data_Bits(GPIOD_SFR,GPIO_PIN_MASK_1, x)
#define EC200U_POWERKEY(x) GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_8, x)
#define EC200U_RESET(x)    GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_7, x)
#define EC200U_WAKEUP(x)   GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_6, x)
#define EC200U_DISABLE(x)  GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_9, x)

typedef enum
{
    EC200U_WORKMODE_NET = 0,  		/*TCP/IP网络透传*/
    EC200U_WORKMODE_HTTP,     		/*HTTP网络透传*/
    EC200U_WORKMODE_MQTT,    		/*MQTT网络透传*/
    EC200U_WORKMODE_ALIYUN,   		/*阿里云透传*/
    EC200U_WORKMODE_BAIDUYUN, 		/*百度云透传*/
} work_mode_eu;


/* -----------------------全局函数声明------------------------------- */
void	Module_4G_Software_Start();
void 	EC200U_INIT();
//void 	EC200U_SendData(uint8_t* Databuf, uint16_t length);
void 	EC200U_SendData(uint8_t* Databuf,  uint16_t length, uint8_t use_dma);
bool 	dispose_4g_rcv_msg();//4G接收处理函数
/* -----------------------全局变量声明------------------------------- */
extern bool 	find_ec200u_baud;
#endif /* EC200U_H_ */
