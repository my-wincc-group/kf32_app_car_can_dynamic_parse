/*
 * circular_queue.c
 *
 *  Created on: 2022-1-18
 *      Author: zkhy
 */
#include "circular_queue.h"

#define MAX_EFFECTIVE_SIZE 200

uint8_t Circular_Queue_Init(Circular_Queue* queue, uint8_t size_set){
	if(size_set > MAX_EFFECTIVE_SIZE){
		queue->max_effective_size = 0;
		return 0;// false
	}
	queue->cipv_ID 			= 0;
	queue->right_now_head 	= 0;
	queue->next_head_index 	= 0;
	queue->next_tail_index 	= 0;
	queue->is_empty 		= 1;
	queue->is_full 			= 0;
	queue->max_effective_size = MAX_EFFECTIVE_SIZE;
	queue->num_of_member 	= 0;
	queue->size_double 		= size_set*2 -1;
	queue->size_of_queue 	= size_set;
	return 1; // ture:succed
}

uint8_t In_Queue(Circular_Queue* queue, float data){
	if(queue->max_effective_size == 0){
		return 0;//ERROR
	}
	if(queue->size_of_queue >= 0){
		if((queue->next_head_index < queue->size_of_queue)){
			queue->data[queue->next_head_index] = data;
			queue->right_now_head 				= queue->next_head_index;
			queue->next_head_index 				+= 1;
			queue->num_of_member 				= queue->next_head_index;
			if(queue->next_head_index < queue->size_of_queue){
				queue->is_full 	= 0;
				queue->is_empty = 1;
			}else{
				queue->is_full 	= 1;
				queue->is_empty = 0;
			}

			//fprintf(USART1_STREAM,"In_Queue_OK1\r\n");
		}else {
			queue->is_full 		= 1;
			queue->is_empty 	= 0;
			queue->data[queue->next_head_index - queue->size_of_queue] = data;
			queue->right_now_head 	= queue->next_head_index;
			queue->next_head_index 	+= 1;
			if(queue->next_head_index > queue->size_double){
				queue->next_head_index = queue->size_of_queue;
			}
			queue->next_tail_index = queue->next_head_index + 1;
			if(queue->next_tail_index > queue->size_double){
				queue->next_tail_index = queue->size_of_queue;
			}
			//fprintf(USART1_STREAM,"In_Queue_OK2\r\n");
		}
	}
	return 1;// succed
}
void Clean_Queue(Circular_Queue* queue){
	queue->cipv_ID 			= 0;
	queue->right_now_head 	=0;
	queue->next_head_index 	= 0;
	queue->next_tail_index 	= 0;
	queue->is_empty 		= 1;
	queue->is_full 			= 0;
	queue->num_of_member 	= 0;
	for(int i_n = 0;i_n < queue->size_of_queue;i_n++){
		queue->data[i_n] = 0.0;
	}
}
uint8_t Circular_is_empty(Circular_Queue* queue){
	if(queue->is_empty == 1){
		return 1;
	}
	else{
		return 0;
	}
}
uint8_t Circular_is_Full(Circular_Queue* queue){
	if(queue->is_full == 1){
		return 1;
	}
	else{
		return 0;
	}
}
float Get_data(Circular_Queue* queue, uint8_t index){
	if(index > queue->size_double){
		fprintf(USART1_STREAM,"size_double\r\n");
		return 0;
	}
	if(queue->is_full == 0){
		fprintf(USART1_STREAM,"is_full\r\n");
		return 0;
	}
	/*uint8_t p_index = queue.head_index + index;
	if(p_index > queue.size_double){
		p_index = queue.size_of_queue;
	}*/
	return queue->data[index];
}
