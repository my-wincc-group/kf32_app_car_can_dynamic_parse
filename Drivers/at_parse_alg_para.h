/*
 * at_parse_alg_para.h
 *
 *  Created on: 2021-12-30
 *      Author: Administrator
 */

#ifndef AT_PARSE_ALG_PARA_H_
#define AT_PARSE_ALG_PARA_H_
#include "aeb_cms_sss_para_config.h"
#include "aeb_cms_alg.h"
//gcz 2022-05-08------------------------------------------------------------------------------------------------
typedef struct __detection_para
{
	uint8_t flag;
	uint16_t cnt;
}DETEC_PARA;

extern DETEC_PARA g_st_detection_cal_sp_para;
extern DETEC_PARA g_st_detection_ult_radra_para;

#define NEW_UART0_CACHE_NBR			5
#define NEW_UART0_DATA_LEN			120
//利用AT+QPING检测4G是否连通
typedef struct __check_state
{
	uint8_t rsps_buffer[NEW_UART0_DATA_LEN];	//缓存
	volatile uint8_t state;				//状态
	uint32_t timer;				//时间片
}CHECK_STATE;


typedef struct __uart_cache
{
	uint8_t UartRcvBuffer[NEW_UART0_DATA_LEN];
	uint32_t DataLen;
	uint8_t used_flag;
}UART_CACHE;
typedef struct __uart_obj//串口中断接收缓存数据结构
{
	UART_CACHE st_uart_opt[NEW_UART0_CACHE_NBR];
	uint8_t GetPrt;
	uint8_t PutPrt;
}UART_OBJ;
typedef struct __check_opt
{
	CHECK_STATE check_state_manchine;
	uint8_t check_flag;

}CHECK_OPT;
enum
{
	INIT_STATE = 0,
	WAIT_RCV_PING_RSPS,
	RCV_RSPS_PING_SUCCEED,
	RCV_RSPS_PING_FINISH,
	WAIT_RCV_SET_SIMSTAT_RSPS,
	RCV_RSPS_SET_SIMSTAT_SUCCEED,
	RCV_RSPS_SET_SIMSTAT_FINISH,
	WAIT_RCV_GET_SIMSTAT_REPORT_RSPS,
	RCV_RSPS_GET_SIMSTAT_REPORT_SUCCEED,
	RCV_RSPS_GET_SIMSTAT_REPORT_FINISH,
	WAIT_RCV_GPS_OPEN,
	RCV_GPS_OPEN_SUCCEED,
	WAIT_RCV_SET_QGPSLOC_RSPS,
	RCV_SET_QGPSLOC_SUCCEED,
	GET_GPS_DATA_FINISH,
};
enum
{
	CMD_INIT = 0,
	CMD_SRC_DEBUG,
	CMD_SRC_BT,
	CMD_END
};
extern uint8_t g_DetectionCfgWay;
extern uint8_t g_u8_instlltion_detection_cmd_src;
extern uint8_t g_u8_check_ult_radra_cmd_src;
//串口0接收缓存队列
void uart_rcv_to_cache(uint8_t data);
//AEBS安装检测命令处理
void check_aebs_4g_all_detection_rsps_string();
//-----------------------------------------------------------------------------------------------------------------------

/**************************** 宏/枚举 ****************************************************/
#define ALG_PARSE_BUFFER_SIZE			300            //AT接收串口的缓存命令字符串长度+后续参数长度（参数长度最长设置为300） lyj
/**************************** 全局变量 ****************************************************/

//gcz 2022-05-08------------------------------------------------------------------------------------------------
typedef struct _uart_opt
{
	uint8_t usart1_At_Buf[ALG_PARSE_BUFFER_SIZE];
	uint8_t usart_used_id;
}UART_OPT;
extern UART_OPT g_st_uart_opt;
//extern uint8_t usart1_At_Buf[AT_BUFFER_SIZE];
#define UART_1_ID	1
#define UART_4_ID	4
//gcz   2022-05-27
extern uint8_t g_u8UartFlag;
//-----------------------------------------------------------------------------------------------------------------------
/**************************** 全局函数 ****************************************************/
void 	AlgParse_Init();
void  	AlgPrase_GetAtCmd(void **cmd);
void 	Analysis_AT_CMD_InMainLoop();
void 	HC02_BT_Analysis_Alg_Para(uint8_t data);
uint8_t Dynamic_Control_Megnetic_Valve();
//int 	PWM_Magnetic_Valve_set(uint16_t p1_time, uint16_t p2_num, uint8_t swt);

bool 	Write_AEB_Config_Param(KunLun_AEB_Para_Cfg aebAlgPara);
bool 	Read_AEB_Config_Param(KunLun_AEB_Para_Cfg *aebAlgPara);
void	Load_And_Write_AEB_Default_Para();

//gcz 2022-05-13
extern uint8_t algConfigway;
//实时显示超声波雷达数据
void show_rt_ult_radar_info();
//安装检测4G的状态机
void install_detection_4G_ctrl_machine();
//AEBS安装检测命令处理
uint8_t aebs_detection_in_installation(STREAM* USART_Print,uint8_t *buffer);

//bool IMU_Analysis_Alg_Para(uint8_t data);
#endif /* AT_PARSE_ALG_PARA_H_ */
