/*
 * ego_filter.h
 *
 *  Created on: 2022-9-8
 *      Author: acai
 */

#ifndef EGO_FILTER_H_
#define EGO_FILTER_H_
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

bool EgoInfo_InitFilter(float speed, float acc, uint64_t sys_tm);
bool EgoInfo_FilterVehicleSpeed(float speed, float acc, uint64_t sys_tm,
    float *filted_speed, float *filted_acc);

#endif /* EGO_FILTER_H_ */
