/*
 * ego_info.h
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#ifndef EGO_INFO_H_
#define EGO_INFO_H_
#include <stdint.h>
#include <stdbool.h>
#include "../../Tasks/common.h"

struct EgoInfo {
  bool left_turn;
  bool right_turn;

  uint8_t frame_id;
  uint8_t brake_status;
  uint8_t wiper_status;
  uint8_t moving_direction;

  float vel;
  float acc;
  float yawrate;
  float steering_angle;
};

void initSrrCanFrame();
bool SendSrrCanInfo(_VEHICLE_PARA *vehicle,float srr_exclusive_speed);

#endif /* EGO_INFO_H_ */
