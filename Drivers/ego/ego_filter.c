/*
 * ego_filter.c
 *
 *  Created on: 2022-9-8
 *      Author: acai
 */

#include "ego_filter.h"
#include <math.h>
#include "alg_lib.h"
#include "usart.h"

static KLM_CAL_2ND_ORDER klm_[1];
static float data_[2][1] = { 0 };

static uint64_t pre_sys_tm_ = 0;
static uint64_t diff_pre_sys_tm_ = 0;

static float pre_vehicle_speed = 0.f;

static bool EgoInfo_DifferenceFilter(float speed, uint64_t sys_tm);

bool EgoInfo_InitFilter(float speed, float acc, uint64_t sys_tm) {
  memset(klm_, 0, sizeof(KLM_CAL_2ND_ORDER));

  klm_->A[0][0] = 1.f;
  klm_->A[1][1] = 1.f;
  klm_->Q[0][0] = 0.5f;
  klm_->Q[1][1] = 0.1f;
  klm_->R[0][0] = 1.f;
  klm_->R[1][1] = 0.5f;

  klm_->kg[0][0] = 1.f;
  klm_->kg[1][1] = 1.f;

  klm_->P_last[0][0] = 1.f;
  klm_->P_last[1][1] = 1.f;
  klm_->X_last[0][0] = speed;
  klm_->X_last[1][0] = acc;

//  USART1_Bebug_Print_Flt("klm_->X_last[0][0]",klm_->X_last[0][0], 1, 1);
//  USART1_Bebug_Print_Flt("klm_->X_last[1][0]", klm_->X_last[1][0], 1, 1);

  pre_sys_tm_ = sys_tm;
  diff_pre_sys_tm_ = sys_tm;

  pre_vehicle_speed = speed;
  return true;
}
bool EgoInfo_FilterVehicleSpeed(float speed, float acc, uint64_t sys_tm, float *filted_spd,
    float *filted_acc) {
  float gap_tm = (float) (sys_tm - pre_sys_tm_) / 1000.f; // ms-> s
  pre_sys_tm_ = sys_tm;

  //if (EgoInfo_DifferenceFilter(speed, sys_tm) || fabs(acc) < 0.01f) {
  if (EgoInfo_DifferenceFilter(speed, sys_tm)) {
    EgoInfo_InitFilter(speed, acc, sys_tm);
    *filted_spd = speed;
    *filted_acc = acc;
    return false;
  } else if (speed < 0.25f) {
    EgoInfo_InitFilter(speed, acc, sys_tm);
    *filted_spd = 0.f;
    *filted_acc = 0.f;
    return false;
  }

  data_[0][0] = speed;
  data_[1][0] = acc;

  klm_->A[0][1] = gap_tm;
  kalman_2nd_order_filter(klm_, data_);

  *filted_spd = klm_->X_now[0][0];
  *filted_acc = klm_->X_now[1][0];
  return true;
}

static bool EgoInfo_DifferenceFilter(float speed, uint64_t sys_tm) {
  static float diff_speed = 0.f;

  diff_speed += speed - pre_vehicle_speed;

  if (sys_tm - diff_pre_sys_tm_ < 1000) { // 1��
    return false;
  }

  diff_pre_sys_tm_ = sys_tm;

  if (diff_speed < 1.f) {
    return true;
  } else {
    return false;
  }
}

