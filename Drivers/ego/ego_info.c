/*
 * ego_info.c
 *
 *  Created on: 2022-7-12
 *      Author: acai
 */
#include "ego_info.h"

#include <math.h>
#include <float.h>
#include <string.h>

#include "KF32A_BASIC.H"
#include "canhl.h"
#include "imu.h"
#include "usart.h"
#include "imu_filter.h"
#include "ego_filter.h"

#pragma pack(1)
struct VehicleCan {
  uint8_t right_turn :1;
  uint8_t left_turn :1;
  uint8_t reversing :2;
  uint8_t brake_status :2;
  uint8_t :0;
  uint16_t car_speed :16;
  uint16_t yaw_rate :16;
  uint16_t x_accel :16;
};
#pragma pack()

static struct can_frame can_;
static struct VehicleCan v_c_;

static bool IsResetSrr(float speed, int64_t sys_tm);

void initSrrCanFrame(_VEHICLE_PARA *vehicle) {
  can_.TargetID = 0x760;
  can_.lenth = 8;
  can_.MsgType = CAN_DATA_FRAME;
  can_.RmtFrm = CAN_FRAME_FORMAT_SFF;
  can_.RefreshRate = CAN_BAUDRATE_500K;

  Imu_Init();
  EgoInfo_InitFilter(0.f, 0.f, SystemtimeClock); // Ĭ�ϼ��ٶ�����Ϊ0
}

bool SendSrrCanInfo(_VEHICLE_PARA *vehicle,float srr_exclusive_speed) {
  static uint32_t old_systime = 0;
  static bool is_reverse = false;
  uint16_t imu_x_acc = 0xFFFF;
  uint16_t imu_z_deg_sp = 0xFFFF;
  if (SystemtimeClock - old_systime < 25) {
    return false;
  } else {
    old_systime = SystemtimeClock;
  }

  struct ImuData imu_data;
  if (!Imu_TranmitData(&imu_data)) {
    imu_x_acc = 0xFFFF;
    imu_z_deg_sp = 0xFFFF;
  }

  else {
    imu_x_acc = (uint16_t) (0.5f + (imu_data.x_acc + 16) * 32768 / 16.0f);
    imu_z_deg_sp = (uint16_t) (0.5f + (imu_data.z_deg_sp + 327.67) * 100);

//    USART1_Bebug_Print_Flt("1.[x ACC]", imu_data.x_acc, 1, 1); // ��ɾ��
//    USART1_Bebug_Print_Flt("2.[z dep_sp]", imu_data.z_deg_sp, 1, 1); // ��ɾ��
//    USART1_Bebug_Print_Num("3.[SRR x ACC]", imu_x_acc, 2, 1); // ��ɾ��
//    USART1_Bebug_Print_Num("4.[SRR z dep_sp]", imu_z_deg_sp, 2, 1); // ��ɾ��

//	  USART1_Bebug_Print_Num("[imu_data x ACC]", imu_x_acc, 2, 1); // ��ɾ��
//	  USART1_Bebug_Print_Num("[imu_data z Yaw Rate]", imu_z_deg_sp, 2, 1); // ��ɾ��
  }

  float imu_x_acc_filter = 0.f;
  int reverse_gear;

  ImuSpeedFilter_Process((float)srr_exclusive_speed, imu_data.x_acc *9.8f, 0.5f, &imu_x_acc_filter, &reverse_gear);

  if(IsResetSrr(vehicle->fVehicleSpeed, SystemtimeClock)){
    reverse_gear = 1;
  }

  float klm_vehicle_acc, klm_vehicle_speed;

  if(fabs(imu_x_acc_filter) < 0.5f){
    imu_x_acc_filter = 0.f;
  }

  EgoInfo_FilterVehicleSpeed(vehicle->fVehicleSpeed / 3.6, imu_x_acc_filter, SystemtimeClock,
      &klm_vehicle_speed, &klm_vehicle_acc);

 //USART1_Bebug_Print_Flt("\n[VehicleSpeed]", vehicle->fVehicleSpeed, 0, 1);
//  USART1_Bebug_Print_Flt("[klm_vehicle_speed]", klm_vehicle_speed * 3.6, 1, 1);
//  USART1_Bebug_Print_Flt("[x ACC]", imu_data.x_acc *9.8f, 0, 1); // ��ɾ��
//  USART1_Bebug_Print_Flt("[imu_x_acc_filter]", imu_x_acc_filter, 0, 1);
//  USART1_Bebug_Print_Flt("[klm_vehicle_acc]", klm_vehicle_acc, 1, 1);


  //USART1_Bebug_Print_Num("[KLM_CAL_2ND_ORDER]", sizeof(KLM_CAL_2ND_ORDER), 1, 1);

  //USART1_Bebug_Print_Flt("[>>>car_speed <<<<]", vehicle->fVehicleSpeed, 1, 1);
  v_c_.right_turn = vehicle->RightFlag;
  v_c_.left_turn = vehicle->LeftFlag;
  v_c_.reversing = reverse_gear || vehicle->ReverseGear;
  //v_c_.car_speed = (uint16_t) (klm_vehicle_speed * 3.6f * 128.f + 0.5f);
  v_c_.car_speed = (uint16_t) (vehicle->fVehicleSpeed * 128.f + 0.5f);


  //USART1_Bebug_Print_Num("ReverseGear ", vehicle->ReverseGear, 1, 1);
//  USART1_Bebug_Print_Flt("srr_v_s",vehicle->fVehicleSpeed, 1,1);
//  USART1_Bebug_Print_Num("srr_v_c",v_c_.car_speed, 1,1);
//  USART1_Bebug_Print_Num("--2.sysclk= ", SystemtimeClock, 1, 1);
  v_c_.yaw_rate = imu_z_deg_sp;
  v_c_.x_accel = imu_x_acc;
  memset(can_.data, 0xff, can_.lenth);
  memcpy(can_.data, &v_c_, sizeof(struct VehicleCan));

  CAN_Transmit_DATA(CAN1_SFR, can_);

  return true;
}


static bool IsResetSrr(float speed, int64_t sys_tm){
  static uint8_t state = 0;
  static int64_t pre_tm = 0;

  if(speed <= FLT_EPSILON){
    state = 0;
    return false;
  }
  // �ٶ�=0ʱ��Ϊ��ʼ״̬�����ٶȴ���1km/hʱ����500ms��
  switch(state){
  case 0:
    if(speed > 1.f){
      state += 1;
    }
    break;
  case 1:
    if(sys_tm - pre_tm > 500){//500ms
      state += 1;
    }
    break;
  case 2:
    if(speed <= 1.f){
      state = 0;
      break;
    }
  }

  if(1 == state){
    return true;
  }else{
    return false;
  }
}
