/*
 * i2c.h
 *
 *  Created on: 2022-03-30
 *      Author: yuhong
 */

#ifndef I2C_H_
#define I2C_H_
#include "stdio.h"
#include "system_init.h"

//ATC24C128C ADDR
#define EEPROM_WRITE_ADDR 0xA8
#define EEPROM_READ_ADDR  0xA9

#define TEMP_SENSOR_WRITE_ADDR 0x90
#define TEMP_SENSOR_READ_ADDR 0x91
#define POINTER_ADDR 0x00


#define AT24C128C_SIZE  16383

#define I2C_OK                 1
#define I2C_FAIL               0

/**
  * 描述   I2C GPIO重映射初始化
  * 输入   无
  * 返回   无
*/
void I2C_GPIO_init(void);

/**
  * 描述   I2C3 的启动
  * Bit_SET为 0x1
  * Bit_RESET为0x0
  *
  * 返回   无
*/
void I2C_Start( void );

/**
  * 描述   I2C3 的停止
  * Bit_SET为 0x1
  * Bit_RESET为0x0
  *
  * 返回   无
*/
void I2C_Stop( void );

/**
  * 等待ACK
  * 返回   0： ACK
  *  	 1：NACK
  * Bit_SET为 0x1
  * Bit_RESET为0x0
*/
uint8_t I2C_Wait_Ack( void );

/**
  * 发送一个字节地址数据或其他数据
  * 返回   无
  *
  * Bit_SET为 0x1
  * Bit_RESET为0x0
*/
void I2C_Send_Byte( uint8_t byte );

/**
  * 读取一个字节，并发送ACK或NACK
  * 输入    ack：收发发送ACK
  *      TRUE发送，FALSE不发送
  * 返回   返回接收到的值
*/
uint8_t I2C_Read_Byte(uint8_t ack);

/**
  * 读取一个字（两个字节），并发送ACK或NACK
  * 输入    ack：收发发送ACK
  *      TRUE发送，FALSE不发送
  * 返回   返回接收到的值
*/
uint16_t I2C_Read_Word();

/**
  * 发送ACK应答
  * 输入    无
  * 返回    无
*/
void I2C_Ack(void);

/**
  * 发送NACK应答
  * 输入    无
  * 返回    无
*/
void I2C_Nack(void);

/**
  * 描述   I2C 写一个字节数据
  * 输入   write_addr：写入数据的AT24C128C的存储地址
  *      data: 待写入的值
  * 返回   无
*/
void AT24CXX_WriteOneByte(uint16_t write_addr,uint32_t data);

/**
  * 描述   I2C 读取一个字节数据
  * 输入   read_addr：
  * 返回   uint8_t：读取到的数据
*/
uint8_t AT24CXX_ReadOneByte(uint16_t read_addr);

/**
  * 描述   I2C 读多个字节数据
  * 输入   Read_I2C_Addr：指定要读的I2C地址
  *          p_buffer: 读入数据地址指针
  *          number_of_byte：读取数据个数
  * 返回   0: 写入成功；1：写入失败
  *
*/
uint8_t AT24CXX_ReadBytes(uint16_t read_addr,uint8_t *p_buffer,uint16_t num_of_read);


/**
  * 描述   I2C 写多个字节数据
  * 输入   Write_i2c_Addr：要写入指定的I2C地址
  *          p_buffer: 写入数据地址指针
  *          number_of_byte：写入数据个数
  * 返回   0: 写入成功；1：写入失败
*/
uint8_t AT24CXX_WriteBytes(uint16_t page_addr,uint8_t *p_buffer,uint16_t num_of_write);

/**
  * 描述   检查AT24CXX设备是否存在，首次在末尾写入0x55，然后读取末尾字节是否是0x55
  * 输入  无
  * 返回   0: 写入成功；1：写入失败
*/
uint8_t AT24CXX_Check( void );

/**
  * 描述   获取温度
  * 输入  无
  * 返回  环境温度
*/
float TMP102A_Get( );

#endif /* I2C_H_ */
