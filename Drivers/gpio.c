/*
 * gpio.c
 *
 *  Created on: 2021-7-7
 *      Author: chenq
 */
#include "gpio.h"
#include "i2c.h"

extern void delay_ms(uint32_t nms);
/// Brake control type
// GPIO_POWER_TYPE == 1 ：CAN_PORT Power to "proportional valve"
// GPIO_POWER_TYPE == 2 ：PWM_GPIO Power to "magnetic valve"
#define GPIO_POWER_TYPE 		1

void GPIO_Out_Config(void);
void GPIO_Singal_init(void);
void GPIO_Power_CANFA_Control(uint8_t control_type);

//gcz 2022-05-29
void set_brake_led_output(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOH_SFR, GPIO_PIN_MASK_14, BitsValue);			// 正控刹车灯亮 1 0灭
}

void GPIO_Init()
{
	GPIO_Out_Config();			// 输出口初始化  led1- 7   刹车灯正向输出和刹车灯负向输出
	GPIO_Singal_init();			// 输入口初始化
	set_brake_led_output(Bit_RESET);
	I2C_GPIO_init();           // I2C IO管脚初始化
//	GPIO_Power_CANFA_Control(GPIO_POWER_TYPE);
}

void GPIO_Out_Config(void)
{
	GPIO_Write_Mode_Bits(GPIOE_SFR, GPIO_PIN_MASK_6, GPIO_MODE_OUT);						//  LED1--超声波雷达
	GPIO_Write_Mode_Bits(GPIOE_SFR, GPIO_PIN_MASK_3, GPIO_MODE_OUT);						//  PE3 -- 控制申工电磁阀
	GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_0|GPIO_PIN_MASK_1, GPIO_MODE_OUT);		// LED2--4G | LED3--工作电源
	GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_4|GPIO_PIN_MASK_7, GPIO_MODE_OUT);		// LED6--执行 | LED7--毫米波雷达
	GPIO_Write_Mode_Bits(GPIOC_SFR, GPIO_PIN_MASK_7|GPIO_PIN_MASK_8, GPIO_MODE_OUT);		// LED4--相机 |  LED5--整车

	GPIO_Write_Mode_Bits(GPIOH_SFR, GPIO_PIN_MASK_14 | GPIO_PIN_MASK_15, GPIO_MODE_OUT );	// PH14--正控输出--stoplight+ | PH15--负控输出--stoplight-
	//gcz 2022-05-29
	GPIO_Write_Mode_Bits(GPIOE_SFR, GPIO_PIN_MASK_3, GPIO_MODE_OUT);						// CanFA_PowerControl
}

void GPIO_Power_CANFA_Control(uint8_t control_type)
{
	GPIO_Write_Mode_Bits(GPIOE_SFR, GPIO_PIN_MASK_3, GPIO_MODE_OUT);	// CanFA_PowerControl
	//gcz 2022-05-29
	//GPIO_Set_Output_Data_Bits(GPIOH_SFR, GPIO_PIN_MASK_14, 0);			// 正控刹车灯亮 1 0灭
	//  control_type == 1 ：CAN_PORT Power to "proportional valve"
	//  control_type == 2 ：PWM_GPIO Power to "magnetic valve"
	if(control_type == 1){
		GPIO_Set_Output_Data_Bits(GPIOE_SFR, GPIO_PIN_MASK_3, 1);
	}
	else if(control_type == 2){
		// GPIO_Set_Output_Data_Bits(GPIOE_SFR,GPIO_PIN_MASK_3, 1);
	}
}

void GPIO_Singal_init(void)
{
	GPIO_Write_Mode_Bits(GPIOG_SFR , GPIO_PIN_MASK_6 | GPIO_PIN_MASK_7, GPIO_MODE_IN );  // PG6--刹车  | PG7--左转
	GPIO_Write_Mode_Bits(GPIOH_SFR , GPIO_PIN_MASK_5 | GPIO_PIN_MASK_6, GPIO_MODE_IN );  // PH5--右转   |  PH6 --    back
	GPIO_Write_Mode_Bits(GPIOH_SFR , GPIO_PIN_MASK_8 | GPIO_PIN_MASK_9, GPIO_MODE_IN );  // PH8--IN1-预留1 | PH9--IN2--预留2
	GPIO_Write_Mode_Bits(GPIOH_SFR , GPIO_PIN_MASK_12 | GPIO_PIN_MASK_13, GPIO_MODE_IN );// PH12--AEB switch--负控输入 | PH13--LDW switch--负控输入
	GPIO_Write_Mode_Bits(GPIOC_SFR,GPIO_PIN_MASK_4,GPIO_MODE_IN);
	// GPIO_Pin_RMP_Config(GPIOE_SFR,GPIO_PIN_MASK_4,GPIO_RMP_AF3_T21);
	// GPIO_Write_Mode_Bits(GPIOC_SFR , GPIO_PIN_MASK_4 , GPIO_MODE_IN );// speedin
}

void Set_LED1(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOE_SFR,GPIO_PIN_MASK_6, BitsValue);
}
void Set_LED2(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOF_SFR,GPIO_PIN_MASK_0, BitsValue);
}
void Set_LED3(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOF_SFR,GPIO_PIN_MASK_1, BitsValue);
}
void Set_LED4(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOC_SFR,GPIO_PIN_MASK_7, BitsValue);
}
void Set_LED5(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOC_SFR,GPIO_PIN_MASK_8, BitsValue);
}
void Set_LED6(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOF_SFR,GPIO_PIN_MASK_4, BitsValue);
}
void Set_LED7(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOF_SFR,GPIO_PIN_MASK_7, BitsValue);
}
/*
 * 设置电磁阀
 */
void Set_Magnetic_Valve(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOE_SFR,GPIO_PIN_MASK_3, BitsValue);
}
BitAction Read_stop_signal(void)
{
	return GPIO_Read_Input_Data_Bit (GPIOG_SFR, GPIO_PIN_MASK_6);
}

BitAction Read_turnleft_signal(void)
{
	return GPIO_Read_Input_Data_Bit (GPIOG_SFR, GPIO_PIN_MASK_7);
}

BitAction Read_turnright_signal(void)
{
	return GPIO_Read_Input_Data_Bit (GPIOH_SFR, GPIO_PIN_MASK_5);
}

BitAction Read_turnback_signal(void)
{
	return GPIO_Read_Input_Data_Bit (GPIOH_SFR, GPIO_PIN_MASK_6);
}

BitAction Read_AEB_switch(void)
{
	return GPIO_Read_Input_Data_Bit (GPIOH_SFR, GPIO_PIN_MASK_12);
}

BitAction Read_LDW_switch(void)
{
	return GPIO_Read_Input_Data_Bit (GPIOH_SFR, GPIO_PIN_MASK_13);
}
void Set_Main_Power_Valve(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOB_SFR,GPIO_PIN_MASK_15, BitsValue);
}
void Set_Brake_Positive_Control_Value(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOH_SFR,GPIO_PIN_MASK_14, BitsValue);
}
void Set_Brake_Negative_Control_Value(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOH_SFR,GPIO_PIN_MASK_15, BitsValue);
}
void Power_CANFA_Reset( void )
{
	GPIO_Write_Mode_Bits(GPIOE_SFR, GPIO_PIN_MASK_3, GPIO_MODE_OUT);
	GPIO_Set_Output_Data_Bits(GPIOE_SFR,GPIO_PIN_MASK_3, 0);
	delay_ms(1000);
	GPIO_Set_Output_Data_Bits(GPIOE_SFR,GPIO_PIN_MASK_3, 1);
}
