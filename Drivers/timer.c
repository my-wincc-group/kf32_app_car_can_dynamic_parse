/*
 * timer.c
 *
 *  Created on: 2022-5-23
 *      Author: Administrator
 */
#include "timer.h"
void SysTick_Configuration(uint32_t Reload)
{
	SYSTICK_Cmd (FALSE);
	SYSTICK_Reload_Config(Reload);
	SYSTICK_Counter_Updata();                           //向ST_CV寄存器写任意值，以清零当前值寄存器
	SYSTICK_Clock_Config(SYSTICK_SYS_CLOCK_DIV_1);      //系统节拍定时器时钟源选择，SCLK作为时钟源
	SYSTICK_Systick_INT_Enable(TRUE);
	SYSTICK_Cmd(TRUE);
    INT_Interrupt_Enable(INT_SysTick, TRUE);				//使能SYSTICK中断
	INT_All_Enable (TRUE);
}


//void BASIC_TIMER_Config(BTIM_SFRmap* BTIMx,  InterruptIndex Peripheral)
//{
//	//定时器时钟源选用SCLK  设周期为50000 设预分频23+1=24分频 120M主频 定时10ms进一次中断
//
//	TIM_Reset(BTIMx);												//定时器外设复位，使能外设时钟
//	BTIM_Updata_Immediately_Config(BTIMx, TRUE);						//立即更新控制
//	BTIM_Updata_Enable(BTIMx, TRUE);									//配置更新使能
//	BTIM_Work_Mode_Config(BTIMx, BTIM_TIMER_MODE);					//定时模式选择
//	BTIM_Set_Counter(BTIMx, 0);										//定时器计数值
//	BTIM_Set_Period(BTIMx, 50000);									//定时器周期值50000
//	BTIM_Set_Prescaler(BTIMx, 23);//23);								    //定时器预分频值23+1=24
//	//BTIM_Set_Period(BTIMx, 50000);
//	//BTIM_Set_Prescaler(BTIMx, 23);
//	BTIM_Counter_Mode_Config(BTIMx, BTIM_COUNT_UP_OF);				//向上计数, 上溢产生中断标志
//	BTIM_Clock_Config(BTIMx, BTIM_SCLK);								//选用SCLK时钟
//	INT_Interrupt_Priority_Config(Peripheral, 4, 0);					//抢占优先级4, 子优先级0
//	BTIM_Overflow_INT_Enable(BTIMx, TRUE);							//计数溢出中断使能
//	INT_Interrupt_Enable(Peripheral, TRUE);						    //外设中断使能
//	INT_Clear_Interrupt_Flag(Peripheral);							//清中断标志
//	BTIM_Cmd(BTIMx, TRUE);											//定时器启动控制使能
//	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);					//中断自动堆栈使用单字对齐
//	INT_All_Enable (TRUE);											//全局可屏蔽中断使能, 该中断使能控制不包含复位/NMI/硬件错误中断
//}


//void CCPx_Capture_Mode_init(CCP_SFRmap* CCPx)
//{
//	/*设置定时器的预分频值 以及捕捉通道的模式*/
//	TIM_Reset(CCPx);										//定时器外设复位，使能外设时钟
//	CCP_PWM_Input_Measurement_Config(CCPx, TRUE);            //PWM输入测量模式使能
//	GPTIM_Slave_Mode_Config(CCPx, GPTIM_SLAVE_RESET_MODE);   //设置从模式：复位模式
//	//GPTIM_Trigger_Select_Config(CCPx, GPTIM_TRIGGER_CCPXCH1);  //选择触发源为CH1
//	GPTIM_Trigger_Select_Config(CCPx, GPTIM_TRIGGER_CCPXCH2);
//	CCP_Capture_Mode_Config(CCPx,  CCP_CHANNEL_2, CCP_CAP_RISING_EDGE);      ///设置捕捉通道 模式:每个下降沿发生捕捉
//	//CCP_Capture_Mode_Config(CCPx,  CCP_CHANNEL_2, CCP_CAP_FALLING_EDGE);      ///设置捕捉通道 模式:每个下降沿发生捕捉
//
//	GPTIM_Updata_Immediately_Config(CCPx, TRUE);				//立即更新控制
//	GPTIM_Updata_Enable(CCPx, TRUE);							//配置更新使能
//	GPTIM_Work_Mode_Config(CCPx, GPTIM_TIMER_MODE);			//定时模式选择
//	GPTIM_Set_Counter(CCPx, 0);								//定时器计数值
//
//	GPTIM_Set_Prescaler(CCPx, 119);							//定时器预分频值 预分频为119+1=120分频，主时钟120M, 1us计数一次
//	GPTIM_Counter_Mode_Config(CCPx, GPTIM_COUNT_UP_OF);		//向上, 上溢产生中断标志
//	GPTIM_Clock_Config(CCPx, GPTIM_SCLK);					//选用SCLK时钟为定时器时钟源
//	GPTIM_Cmd(CCPx, TRUE);                                   //使能通用定时器
//}
