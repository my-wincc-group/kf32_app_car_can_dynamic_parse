/*
 * play_sound.h
 *
 *  Created on: 2022-3-9
 *      Author: Administrator
 */

#ifndef PLAY_SOUND_H_
#define PLAY_SOUND_H_

#define OS_START_S					0		// 系统已启动
#define OS_CHK_OK_S					1		// 系统自检正常
#define OS_CHK_ERR_S				2		// 系统自检失败
#define OS_ERR_S					3		// 系统故障
#define FCW_OS_CHK_START_S			4		// FCW系统已启动
#define FCW_OS_CHK_OK_S				5		// FCW自检正常
#define FCW_OS_CHK_ERR_S			6		// FCW系统自检失败
#define FCW_OS_ERR_S				7		// FCW系统故障
#define AEB_OS_START_S				8		// AEB系统已启动
#define AEB_OS_CHK_OK_S				9		// AEB系统自检正常

#define AEB_OS_CHK_ERR_S			10		// AEB系统自检失败
#define AEB_OS_ERR_S				11		// AEB系统故障
#define CAMERA_ERR_S				12		// 摄像头故障
#define LIDAR_ERR_S					13		// 雷达故障
#define ULTR_LIDAR_ERR_S			14		// 超声波雷达故障
#define ACTUATOR_ERR_S				15		// 执行机构故障
#define VEHICLE_CAN_ERR_S			16		// 整车CAN故障
#define NETWORK_ERR_S				17		// 网络故障
#define GPS_ERR_S					18		// GPS故障
#define VEHICLE_SPEED_ERR_S			19		// 车速故障

#define LDW_OS_START_S				20		// 车道偏离预警系统已启动
#define LDW_OS_CHK_OK_S				21		// 车道偏离预警系统自检正常
#define LDW_OS_CHK_ERR_S			22		// 车道偏离预警系统自检失败
#define LDW_OS_ERR_S				23		// 车道偏离预警系统故
#define LDW_S						24		// 车道偏离
#define VEHICLE_DIS_TOO_CLOSE_S		25		// 车距过近
#define NOTICE_HEAD_VEHICLE_S		26		// 注意前车
#define NOTICE_PEDESTRIAN_S			27		// 注意行人
#define OS_UPGRADE_OK_S				28		// 系统升级成功
#define OS_FUN_CLOSE_S				29		// 系统功能已关闭

#define BIBI_1S_S					30		// 哔哔声1秒
#define BIBI_05S_S					31		// 哔哔声0.5秒
#define BIBI_03S_S					32		// 哔哔声0.3秒
#define BIBI_02S_S					33		// 哔哔声0.2秒
#define DIDI_50MS_S					34		// 50ms di
#define DIDI_100MS_S				35		// 100ms di
#define DIDI_200MS_S				36		// 200ms di
#define DIDI_500MS_S				37		// 500ms di
#define DIDI_800MS_S				38		// 800ms di
#define DIDI_S						39		// 滴滴声

#define DIDI_HARSH_S				40		// 刺耳滴滴声
#define AEB_OS_CLOSE_S				41		// AEB系统已关闭
#define LDW_OS_CLOSE_S				42		// 车道偏离预警系统已关闭
#define UPGRADING_NOT_POWER_S		43		// 系统升级中，请勿断电
#define PARAM_CONFIG_OK_S			44		// 系统参数配置成功
#define PARAM_CONFIG_ERR_S			45		// 系统参数配置失败
#define DATA_UPLOADING_S			46		// 数据上传中
#define DATA_DOWNLOADING_S			47		// 数据下载中
#define BT_CONNECT_OK_S				48		// 蓝牙连接成功
#define BT_DISCONNECT_OK_S			49		// 蓝牙连接断开

#define SOUND_IS_BIG_S				50		// 音量最大
#define OS_ERR_WILL_CLOSE_S			51		// 检测到系统故障，系统已关闭
#define FCW_CLOSE_S					52		// FCW系统已关闭
#define CAMERA_OK_S					53		// 摄像头正常
#define LIDAR_OK_S					54		// 雷达正常
#define ULTR_LIDAR_OK_S				55		// 超声波雷达正常
#define ACTAUTOR_OK_S				56		// 执行机构正常
#define VEHICLE_CAN_OK_S			57		// 整车CAN正常
#define NETWORK_OK_S				58		// 网络正常
#define GPS_OK_S					59		// GPS正常

#define VEHICLE_SPEED_OK_S			60		// 车速正常
#define OS_UPGRADE_ERR_S			61		// 系统升级失败
#define DATA_UPLOAD_ERR_S			62		// 数据上传失败
#define DATA_UPLOAD_OK_S			63		// 数据上传成功
#define DATA_DOWNLOAD_OK_S			64		// 数据下载成功
#define DATA_DOWNLOAD_ERR_S			65		// 数据下载失败
#define SOUND_IS_SMALL_S			66		// 音量最小

#endif /* PLAY_SOUND_H_ */
