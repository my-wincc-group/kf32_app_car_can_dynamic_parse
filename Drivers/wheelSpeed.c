/*
 * wheelSpeed.c
 *
 *  Created on: 2021-11-30
 *      Author: Administrator
 */
#include "wheelSpeed.h"
#include "usart.h"
#include "stdio.h"
#include "common.h"
#include "aeb_cms_sss_para_config.h"
#include "at_parse_alg_para.h"

/* -----------------------全局变量------------------------------- */
volatile uint16_t pulse				= 0;
volatile uint16_t oldpulse_count	= 0;
uint16_t wheelSpeed 				= 0;
uint32_t clock_old 					= 0;
uint32_t clock_old_forcal			= 0;

static uint8_t cir_num             	= 0;
static uint8_t min_plus            	= 5;
static uint32_t save_plus_num      	= 0;

static uint8_t pointer_first       	= 0;
static float cache_first[9]			= {0.0f};

static uint8_t pointer_second      	= 0;
static float cache_second[9]       	= {0.0f};

static uint8_t loop_time_gap       	= 10;
static uint8_t filter_size         	= 4;
static uint8_t double_size     		= 7;
static uint8_t min_time_count  		= 10;//最短0.1s才需要计算一次车速
static uint8_t max_time_count  		= 40;//最长0.4s就需要计算一次车速

static uint16_t time_count     		= 0;
static uint8_t wheel_or_eigen    	= 0;

/* -----------------------局部函数声明------------------------------- */
void Calc_Wheel_Speed(uint16_t pulse,int64_t cur_speed_clk);//gcz 2022-06-05  增加系统时间戳
/*
 * 轮速初始化优
 */
void WheelSpeed_Init()
{
	//将管脚重映射PC4为T2CK
	GPIO_Write_Mode_Bits (GPIOC_SFR,GPIO_PIN_MASK_4, GPIO_MODE_RMP);  	//开启重映射
	GPIO_Pin_RMP_Config(GPIOC_SFR,GPIO_Pin_Num_4,GPIO_RMP_AF1_T2);     	//重映射功能

	TIM_Reset(T2_SFR);         								 //定时器外设复位，使能外设时钟
	GPTIM_InitTypeDef gptimInitStruct_2;
	GPTIM_Struct_Init (&gptimInitStruct_2);

	gptimInitStruct_2.m_Counter		= 0;    				//写入cnt
	gptimInitStruct_2.m_CounterMode	= GPTIM_COUNT_UP_OF;
	gptimInitStruct_2.m_Period		= 0xFFFF;
	gptimInitStruct_2.m_WorkMode	= GPTIM_COUNTER_MODE;	//计数模式
	gptimInitStruct_2.m_Prescaler	= 0;    				//分频
//	gptimInitStruct_2.m_Clock		= GPTIM_SCLK;
//	gptimInitStruct_2.m_SlaveMode	= GPTIM_SLAVE_COUNTER_MODE;
//	gptimInitStruct_2.m_EXPulseSync	= GPTIM_NO_SYNC_MODE;

	GPTIM_Updata_Immediately_Config(T2_SFR,TRUE);    		//立即更新控制
	GPTIM_Updata_Enable(T2_SFR,TRUE);       				//配置更新使能
	GPTIM_Configuration(T2_SFR,&gptimInitStruct_2);
	GPTIM_Cmd(T2_SFR,TRUE);	  								//使能t2计数器
	INT_Interrupt_Priority_Config(INT_T2,1,2);
	// filter init
	double_size = filter_size * 2 - 1;
}
/*
 * 低速计算
 */
uint32_t clock_old_low_speed 				= 0;
uint32_t clock_old_forcal_low_speed			= 0;
uint16_t pulse_count_low_speed = 0;
uint16_t Calc_Bus_Encoder_Velocity_LowSpeed()
{

	if(SystemtimeClock - clock_old_low_speed >= loop_time_gap){
		clock_old_low_speed = SystemtimeClock;
		uint16_t pulse_count= (uint16_t) (T2_SFR->CNT);
		if(pulse_count_low_speed == 0){
			pulse_count_low_speed = pulse_count;
			clock_old_forcal_low_speed = SystemtimeClock;
			pulse = 0;
			time_count = 1.0;
			//fprintf(USART1_STREAM,"pulse_count_low_speed==0\r\n");
		}else{
			if(pulse_count > pulse_count_low_speed){
				pulse = pulse_count - pulse_count_low_speed;
				//fprintf(USART1_STREAM,"pulse_count=%d > pulse_count_low_speed=%d,time_count=%d\r\n",pulse_count,pulse_count_low_speed,SystemtimeClock - clock_old_forcal_low_speed);
				pulse_count_low_speed = pulse_count;
				time_count = SystemtimeClock - clock_old_forcal_low_speed;
				clock_old_forcal_low_speed = SystemtimeClock;
				Calc_Wheel_Speed(pulse,clock_old_forcal_low_speed);

			}else if(pulse_count < pulse_count_low_speed){
				pulse = 65535 - pulse_count_low_speed + pulse_count;
				//fprintf(USART1_STREAM,"pulse_count=%d < pulse_count_low_speed=%d\r\n",pulse_count,pulse_count_low_speed);
				pulse_count_low_speed = pulse_count;
				time_count = SystemtimeClock - clock_old_forcal_low_speed;
				clock_old_forcal_low_speed = SystemtimeClock;
				Calc_Wheel_Speed(pulse,clock_old_forcal_low_speed);

			}
		}
		return pulse;
	}
}
/*
 * 获取大车编码器的脉冲数
 */
uint16_t Calc_Bus_Encoder_Velocity()
{

	if(wheelSpeed < 0){
		return Calc_Bus_Encoder_Velocity_LowSpeed();
	}
	else{
		if(SystemtimeClock - clock_old >= loop_time_gap){
			min_plus = wheelSpeed/100 +1;
			if(min_plus > 5)
				min_plus = 5;
			clock_old = SystemtimeClock;
			uint16_t pulse_count= (uint16_t) (T2_SFR->CNT);

			if(pulse_count - oldpulse_count >= 0)
				pulse = pulse_count - oldpulse_count;
			else if(pulse_count - oldpulse_count < 0)
				pulse = 65535 - oldpulse_count + pulse_count;
			else{
				oldpulse_count 	= pulse_count;
				pulse_count		= 0;
				return pulse;
			}
			if(wheel_or_eigen == 1){
				return pulse;
			}


			if(cir_num < min_time_count){
				cir_num += 1;
			}else if(cir_num >= min_time_count && cir_num < max_time_count){
				cir_num += 1;
				if(pulse > min_plus){
					time_count = (SystemtimeClock - clock_old_forcal);
					clock_old_forcal = SystemtimeClock;
					Calc_Wheel_Speed(pulse,clock_old_forcal);
					//fprintf(USART1_STREAM,"low_:p=%d,n=%d,wv=%0.2f\r\n",pulse,cir_num,wheelSpeed*0.1);
					cir_num = 0;
					oldpulse_count 	= pulse_count;
					pulse_count		= 0;
				}
			}
			else if(cir_num >= max_time_count){
				time_count = (SystemtimeClock - clock_old_forcal);
				clock_old_forcal = SystemtimeClock;
				Calc_Wheel_Speed(pulse,clock_old_forcal);
				//fprintf(USART1_STREAM,"low_:p=%d,n=%d,wv=%0.2f\r\n",pulse,cir_num,wheelSpeed*0.1);
				cir_num = 0;
				oldpulse_count 	= pulse_count;
				pulse_count		= 0;
			}
		/*lop:
			oldpulse_count 	= pulse_count;
			pulse_count		= 0;*/
			}

			//fprintf(USART1_STREAM,"low_:p=%d,n=%d,wv=%0.2f\r\n",pulse,cir_num,wheelSpeed*0.1);
			return pulse;
	}

}
/*
 * 获取大车编码器的脉冲数
 */
uint16_t Calc_Bus_Encoder_Velocity_old()
{
	if(SystemtimeClock - clock_old >= loop_time_gap){
		clock_old = SystemtimeClock;

		uint16_t pulse_count= (uint16_t) (T2_SFR->CNT);

		if(pulse_count - oldpulse_count >= 0)
			pulse = pulse_count - oldpulse_count;
		else if(pulse_count - oldpulse_count < 0)
			pulse = 65535 - oldpulse_count + pulse_count;
		else{
			oldpulse_count 	= pulse_count;
			pulse_count		= 0;
			return pulse;
		}
		if(wheel_or_eigen == 1){
			return pulse;
		}

		if(((pulse < min_plus)&(pulse >= 0))||(cir_num < min_time_count)){
			cir_num += 1;
			//Calc_Wheel_Speed(save_plus_num);
			if(cir_num >= max_time_count){
				cir_num -=1;
				Calc_Wheel_Speed(pulse,clock_old);
				//fprintf(USART1_STREAM,"low_:p=%d,n=%d,wv=%0.2f\r\n",pulse,cir_num,wheelSpeed*0.1);
				cir_num = 0;
				oldpulse_count 	= pulse_count;
				pulse_count		= 0;
			}
		}
		else{
			//stVehicleParas.fVehicleSpeed = pulse*1.0;
			Calc_Wheel_Speed(pulse,clock_old);
			//fprintf(USART1_STREAM,"p=%d,n=%d,wv=%0.2f\r\n",pulse,cir_num,wheelSpeed*0.1);
			cir_num = 0;
			oldpulse_count 	= pulse_count;
			pulse_count		= 0;

			// 利用编码器脉冲个数，拟合速度曲线方程
			//fprintf(USART1_STREAM,"%d,%0.2f,%d\r\n",pulse,stVehicleParas.fVehicleSpeed*10,wheelSpeed);
		}

/*lop:
	oldpulse_count 	= pulse_count;
	pulse_count		= 0;*/
	}

	//fprintf(USART1_STREAM,"low_:p=%d,n=%d,wv=%0.2f\r\n",pulse,cir_num,wheelSpeed*0.1);
	return pulse;
}
/***
 * 轮速滤波,初次滤波
 */
float Filter_Average_first(float data_input, uint8_t filter_size){
	if(pointer_first < filter_size){
		cache_first[pointer_first] = data_input;
		pointer_first += 1;
		float output = 0.0;
		for(uint8_t i = 0; i < pointer_first; i++){
			output += cache_first[i];
		}
		return output/(float)(pointer_first);
	}
	else{
		cache_first[pointer_first - filter_size] = data_input;
		pointer_first += 1;
		if(pointer_first > double_size)
			pointer_first = filter_size;
		float output;
		output = 0.0;
		for(uint8_t i = 0; i < filter_size; i++){
			output += cache_first[i];
		}
		return output/(float)(filter_size);
	}
}
/***
 * 轮速滤波,二次滤波
 */
float Filter_Average_second(float data_input, uint8_t filter_size){
	if(pointer_second < filter_size){
		cache_second[pointer_second] = data_input;
		pointer_second += 1;
		float output = 0.0;
		for(uint8_t i = 0; i < pointer_second; i++){
			output += cache_second[i];
		}
		return output/(float)(pointer_second);
	}
	else{
		cache_second[pointer_second - filter_size] = data_input;
		pointer_second += 1;
		if(pointer_second > double_size)
			pointer_second = filter_size;
		float output;
		output = 0.0;
		for(uint8_t i = 0; i < filter_size; i++){
			output += cache_second[i];
		}
		return output/(float)(filter_size);
	}
}

//-------------------------gcz 2022-06-04------增加轮速判断过滤及异常值预测-----------------------------------------
#define SPEED_CACHE		5
#define MAX_SPEED		150		//250km/h，单位km/h
#define DELTA_SPEED		20			//与前一缓存速度差20km/h ，单位km/h
typedef struct __speed_cache
{
	uint32_t i_speed;					//车速	单位0.1km/h
	int64_t sys_timer_record;		//记录获取到的车速时间
}SP_CACHE;
typedef struct __speed_opt
{
	SP_CACHE st_sp_cache[SPEED_CACHE];					//车速 缓存
	uint16_t CurIndex;
	uint16_t NextIndex;
	uint16_t TotalNbr;
}SP_OPT;

SP_OPT g_st_sp_opt;
//计算加速度
//参数：被减数位置	减数位置		系数
float calc_accelerated_speed(uint32_t minuend_pos,uint32_t subtrahend_pos,SP_CACHE p_st_sp_cache[],uint32_t time_para)
{
	float accelerated = 0.0;
	int64_t time1,time2;
	if ((p_st_sp_cache[minuend_pos].sys_timer_record - p_st_sp_cache[subtrahend_pos].sys_timer_record) != 0)
	{
//		fprintf(USART1_STREAM,"    *********AAAAAAAAA %d - %d %ld %ld \r\n",minuend_pos,subtrahend_pos,(uint32_t)p_st_sp_cache[minuend_pos].sys_timer_record,(uint32_t)p_st_sp_cache[subtrahend_pos].sys_timer_record);

//		fprintf(USART1_STREAM,"[%X] [%X] [%X] [%X]\r\n",&s_st_sp_opt.st_sp_cache[minuend_pos],&s_st_sp_opt.st_sp_cache[subtrahend_pos]
//		                                  ,&p_st_sp_cache[minuend_pos],&p_st_sp_cache[subtrahend_pos]);
//		fprintf(USART1_STREAM,"(%d-%d)sp:[%d] [%d] [%d] [%d]\r\n",minuend_pos,subtrahend_pos,s_st_sp_opt.st_sp_cache[minuend_pos].i_speed,s_st_sp_opt.st_sp_cache[subtrahend_pos].i_speed
//				                                  ,p_st_sp_cache[minuend_pos].i_speed,p_st_sp_cache[subtrahend_pos].i_speed);
//		fprintf(USART1_STREAM,"(%d-%d)tm:[%d] [%d] [%d] [%d]\r\n",minuend_pos,subtrahend_pos,s_st_sp_opt.st_sp_cache[minuend_pos].sys_timer_record,s_st_sp_opt.st_sp_cache[subtrahend_pos].sys_timer_record
//						                                  ,p_st_sp_cache[minuend_pos].sys_timer_record,p_st_sp_cache[subtrahend_pos].sys_timer_record);
//		fprintf(USART1_STREAM,"tm:[%d] [%d]\r\n",p_st_sp_cache[minuend_pos].sys_timer_record,p_st_sp_cache[subtrahend_pos].sys_timer_record);
		time1 = p_st_sp_cache[minuend_pos].sys_timer_record;
		time2 = p_st_sp_cache[subtrahend_pos].sys_timer_record;
		accelerated = ((float)(p_st_sp_cache[minuend_pos].i_speed - p_st_sp_cache[subtrahend_pos].i_speed)) / (float)(time1 - time2) * time_para;//3600000;
		fprintf(USART1_STREAM,"   accelerated =  (%d - %d) / ((%ld - %ld) /1000 /3600)\r\n",p_st_sp_cache[minuend_pos].i_speed,p_st_sp_cache[subtrahend_pos].i_speed,
																							time1, time2);
	}
	fprintf(USART1_STREAM,"    *********BBBBBBBBB a=%f\r\n",accelerated);
	return accelerated;
}
//速度限制判断及预测算法
//参数：	当前速度		速度缓存		缓存数量		当前缓存的开始位置  	算法结束的缓存结束位置
//返回：判断计算后的速度
uint16_t speed_prediction_alg(uint16_t cur_int_speed,
								int64_t cur_speed_clk,
								SP_CACHE p_st_sp_cache[],
								uint16_t nbr,
								uint16_t start_pos,
								uint16_t end_pos,
								float coe,
								uint32_t time_para)
{
	int32_t i1;
	SP_CACHE last_cache;
	float accelerated = 0.0;
	float avg_accelerated = 0.0;
	float f_res = 0.0;
	bool b_flag = true;
	uint32_t res = 0,start_nbr = 0;
	if (end_pos != 0)
	{
		fprintf(USART1_STREAM,"----222222.\r\n");
		if (start_pos == 0)//用0和最后一个去算
		{
			last_cache.i_speed = p_st_sp_cache[nbr - 1].i_speed;
			last_cache.sys_timer_record = p_st_sp_cache[nbr - 1].sys_timer_record;
		}
		else
		{
			last_cache.i_speed = p_st_sp_cache[start_pos - 1].i_speed;
			last_cache.sys_timer_record = p_st_sp_cache[start_pos - 1].sys_timer_record;
		}
		//接缝0到nbr-1
		if (start_pos == 1)
		{
			accelerated += calc_accelerated_speed(start_pos - 1,nbr - 1,p_st_sp_cache,time_para);//calc_accelerated_speed(p_st_sp_cache[start_pos - 1],p_st_sp_cache[nbr - 1]);//calc_accelerated_speed(start_pos - 1,nbr - 1,p_st_sp_cache);
			fprintf(USART1_STREAM,"1.[%d]-[%d %d]\r\n",start_pos - 1,p_st_sp_cache[start_pos - 1].i_speed,p_st_sp_cache[start_pos - 1].sys_timer_record);
			fprintf(USART1_STREAM,"1.[%d]-[%d %d]\r\n",nbr - 1,p_st_sp_cache[nbr - 1].i_speed,p_st_sp_cache[nbr - 1].sys_timer_record);
		}
		else
		{
			//cur-1到0
			for (i1 = start_pos - 1;i1 >= 0;i1--)
			{
				accelerated += calc_accelerated_speed(i1,i1 - 1,p_st_sp_cache,time_para);//calc_accelerated_speed(p_st_sp_cache[i1],p_st_sp_cache[i1 - 1]);;//calc_accelerated_speed(i1,i1 - 1,p_st_sp_cache);
				fprintf(USART1_STREAM,"2.[%d]-[%d %d]\r\n",i1,p_st_sp_cache[i1].i_speed,p_st_sp_cache[0].sys_timer_record);
				fprintf(USART1_STREAM,"2.[%d]-[%d %d]\r\n",i1 - 1,p_st_sp_cache[i1 - 1].i_speed,p_st_sp_cache[i1 - 1].sys_timer_record);
			}
		}
		//nbr - 1到 cur
		for (i1 = nbr - 1;i1 >= start_pos;i1--)
		{
			accelerated += calc_accelerated_speed(i1,i1 - 1,p_st_sp_cache,time_para);//calc_accelerated_speed(p_st_sp_cache[i1],p_st_sp_cache[i1 - 1]);//calc_accelerated_speed(i1,i1 - 1,p_st_sp_cache);
			fprintf(USART1_STREAM,"4.[%d]-[%d %d]\r\n",i1,p_st_sp_cache[i1].i_speed,p_st_sp_cache[0].sys_timer_record);
			fprintf(USART1_STREAM,"4.[%d]-[%d %d]\r\n",i1 - 1,p_st_sp_cache[i1 - 1].i_speed,p_st_sp_cache[i1 - 1].sys_timer_record);
		}
		avg_accelerated = accelerated / (nbr - 1);
		fprintf(USART1_STREAM,"=========avg_nbr:%d\r\n",nbr - 1);
	}
	else
	{
	//	fprintf(USART1_STREAM,"----11111111.\r\n");
		if (start_pos >= 2)
		{
//			if (start_pos == 0)//用0和最后一个去算
//			{
//				last_cache.i_speed = p_st_sp_cache[nbr - 2].i_speed;
//				last_cache.sys_timer_record = p_st_sp_cache[nbr - 2].sys_timer_record;
//			}
//			else
			{
				last_cache.i_speed = p_st_sp_cache[start_pos - 1].i_speed;
				last_cache.sys_timer_record = p_st_sp_cache[start_pos - 1].sys_timer_record;
			}
			//nbr - 2 到0
			for (i1 = start_pos - 1;i1 >= 1;i1--)
			{
				accelerated += calc_accelerated_speed(i1,i1 - 1,p_st_sp_cache,time_para);//calc_accelerated_speed(p_st_sp_cache[i1],p_st_sp_cache[i1 - 1]);//calc_accelerated_speed(i1,i1 - 1,p_st_sp_cache);
			}
			avg_accelerated = accelerated / (start_pos - 1);
	//		fprintf(USART1_STREAM,"=========avg_nbr:%d\r\n",start_pos - 1);
		}
		else
			b_flag = false;
	}
//	fprintf(USART1_STREAM,"++++++++++++cal avg_accelerated: %f\r\n",avg_accelerated);
	//超最大，超差值，都认为当前速度异常，使用平均加速度进行计算

	if ((b_flag == true)
		&& ((cur_int_speed > (MAX_SPEED * coe)) || (cur_int_speed - last_cache.i_speed) >= (DELTA_SPEED * coe)))
	{
		f_res = last_cache.i_speed + avg_accelerated * ((cur_speed_clk - last_cache.sys_timer_record) * time_para) + 0.5;
		res = (uint32_t) f_res;
	}
	else
	{
		res = cur_int_speed;
	}
	fprintf(USART1_STREAM,"----last  final speed:%d\r\n",res);
	if (g_st_detection_cal_sp_para.flag == 0)
		res = cur_int_speed;

	fprintf(USART1_STREAM,"----final sp:%d\r\n",res);
	return res;
}
//过滤异常数据，本文件使用
//参数：当前计算的整型速度值
//返回：过滤后的速度值
uint16_t filter_abnormal_speed_with_local(uint16_t cur_int_speed,int64_t sys_clk,SP_OPT *p_st_sp_opt,uint16_t cache_nbr,uint32_t time_para)
{
	uint16_t res = 0;
	int64_t time_clk = sys_clk;
	if (p_st_sp_opt->TotalNbr == 0)
	{
		res = cur_int_speed;
		p_st_sp_opt->st_sp_cache[p_st_sp_opt->CurIndex].i_speed = cur_int_speed;
		p_st_sp_opt->st_sp_cache[p_st_sp_opt->CurIndex].sys_timer_record = time_clk;
		p_st_sp_opt->TotalNbr++;
	}
	else if (p_st_sp_opt->TotalNbr < SPEED_CACHE)
	{
		res = speed_prediction_alg(cur_int_speed,sys_clk,p_st_sp_opt->st_sp_cache,cache_nbr,p_st_sp_opt->CurIndex,0,10,time_para);
		p_st_sp_opt->st_sp_cache[p_st_sp_opt->CurIndex].i_speed = res;
		p_st_sp_opt->st_sp_cache[p_st_sp_opt->CurIndex].sys_timer_record = time_clk;
		p_st_sp_opt->TotalNbr++;
	}
	else
	{
		res = speed_prediction_alg(cur_int_speed,sys_clk,p_st_sp_opt->st_sp_cache,cache_nbr,p_st_sp_opt->CurIndex,p_st_sp_opt->CurIndex,10,time_para);
		p_st_sp_opt->st_sp_cache[p_st_sp_opt->CurIndex].i_speed = res;
		p_st_sp_opt->st_sp_cache[p_st_sp_opt->CurIndex].sys_timer_record = time_clk;
		p_st_sp_opt->TotalNbr = SPEED_CACHE + 1;
	}
	p_st_sp_opt->CurIndex++;
	if (p_st_sp_opt->CurIndex >= cache_nbr)
		p_st_sp_opt->CurIndex = 0;

	return res;
}
//过滤异常数据，外部接口使用
//参数：当前计算的整型速度值
//返回：过滤后的速度值

uint16_t filter_abnormal_speed(uint16_t cur_int_speed,int64_t sys_clk)
{
#ifdef TEST
	return filter_abnormal_speed_with_local(wheelSpeed,sys_clk,&s_st_sp_opt,SPEED_CACHE,3600000);
#else
	return cur_int_speed;//filter_abnormal_speed_with_local(wheelSpeed,sys_clk,&s_st_sp_opt,SPEED_CACHE);
#endif
}
#define SP_BUFFER_NBR						100
#define SP_SEC_BUFFER_NBR					2
uint32_t g_speed_cache[SP_BUFFER_NBR];
uint32_t g_sp_ptr = 0;
uint32_t g_sec_speed_cache[SP_SEC_BUFFER_NBR];
uint32_t g_sec_sp_ptr = 0;
//应用层过滤车速
//扩大后的计算速度整型值		扩大系数
uint32_t SpeedApplicationLayerFilter(uint32_t VehicleSpeed,uint32_t coe)
{
	static int64_t cur_time_clk = 0;
	static uint8_t timer_cnt = 0,sec_timer_cnt = 0;
	uint32_t res = VehicleSpeed;
	if ((SystemtimeClock - cur_time_clk) >= 10)
	{
		cur_time_clk = SystemtimeClock;
		g_speed_cache[g_sp_ptr] = VehicleSpeed;
		g_sp_ptr++;
		if (g_sp_ptr >= SP_BUFFER_NBR)
			g_sp_ptr = 0;

		timer_cnt++;
	}
	if (timer_cnt >= 100)
	{
		timer_cnt = 0;
		sec_timer_cnt++;
		if (sec_timer_cnt >= 2)
			sec_timer_cnt = 2;

		int32_t i1;
		uint64_t sp_tol = 0;
		uint32_t sp_avg = 0;
		uint32_t sp_temp = 0;
		for (i1 = 0;i1 < SP_BUFFER_NBR;i1++)
		{
			sp_tol += g_speed_cache[i1];
		}
		sp_avg = (uint64_t)((float)sp_tol /  (float)SP_BUFFER_NBR + 0.5);

		//左移队列空出最后一个
		for (i1 = 0;i1 < SP_SEC_BUFFER_NBR;i1++)
		{
			g_sec_speed_cache[i1] = g_sec_speed_cache[i1 + 1];
		}
		//最后一个插入值
		g_sec_speed_cache[SP_SEC_BUFFER_NBR - 1] = sp_avg;

	//	fprintf(USART1_STREAM,"----sp_avg :%d\r\n",sp_avg);
	}

	if ((VehicleSpeed >= MAX_SPEED * coe) && (sec_timer_cnt >= 2))
	{
		uint64_t a = (uint32_t)((float)(g_sec_speed_cache[SP_SEC_BUFFER_NBR - 1] - g_sec_speed_cache[SP_SEC_BUFFER_NBR - 2]) / 2.0 * 3600 + 0.5);
		res = g_sec_speed_cache[SP_SEC_BUFFER_NBR - 1] + a /3600;
	//	fprintf(USART1_STREAM,"----v:%d  a(%d) = %d / 2 *3600 \r\n",res,a,g_sec_speed_cache[SP_SEC_BUFFER_NBR - 1]-g_sec_speed_cache[SP_SEC_BUFFER_NBR - 2]);
		if (res > MAX_SPEED * coe)
			res = MAX_SPEED * coe;
	//	fprintf(USART1_STREAM,"----final:v:%d\r\n",res);
	}
	return res;
}
//初始化速度缓存
void InitSpeedOptCache()
{
	memset(&g_st_sp_opt,0,sizeof(g_st_sp_opt));
	memset(g_speed_cache,0,sizeof(g_speed_cache));
	memset(g_sec_speed_cache,0,sizeof(g_sec_speed_cache));
}
//------------------------------------------------------------------------------------------------------------
/*
 * 计算轮速
 */
void Calc_Wheel_Speed(uint16_t pulse,int64_t cur_speed_clk)//gcz 2022-06-05  增加系统时间戳
{
//	float speed = 0.4847 * pulse + 5.549;	// 下取值 偏大
//	float speed = 0.4841 * pulse + 5.2912;	// 上取值
//	float speed = 0.4844 * pulse + 5.4188;	// 平均值
	//float speed = 1.0674 * pulse - 1.255;	// 金旅
	float speed = rParms.WheelSpeed_Coefficient * (float)(pulse) / (float)(time_count);
	//wheelSpeed = (uint16_t)(speed * 10);
	float speed_filter_result=0.0;
	float speed_filter_result_2=0.0;
	speed_filter_result = Filter_Average_first(speed , filter_size);
	speed_filter_result_2 = Filter_Average_second(speed_filter_result , filter_size);
	if(speed > 10){
		wheelSpeed = (uint16_t)(speed_filter_result_2 * 10);
	}
	else if(speed > 5){
		wheelSpeed = (uint16_t)(speed_filter_result * 10);
	}
	else if(speed > 0){
		wheelSpeed = (uint16_t)(speed * 10);
	}
	else{
		wheelSpeed = 0;
	}
	//gcz 2022-06-05 增加车速判断过滤及预测功能
	wheelSpeed = filter_abnormal_speed(wheelSpeed,cur_speed_clk);
	//fprintf(USART1_STREAM,"speed=%0.2f,f_speed=%0.2f\r\n",speed,speed_filter_result);
}

/*
 * 获取轮速接口
 * 参数：无
 * 返回：轮速数据
 * 说明：1、轮速放大10倍；2、单位km/h
 */
uint16_t Get_Wheel_Speed()
{
	return wheelSpeed;
}
//gcz 2022-05-12
//获取当前车速
float get_cur_vehicle_speed()
{
	uint16_t wheel_speed;
	Calc_Bus_Encoder_Velocity();// 轮速
	wheel_speed = Get_Wheel_Speed();
	return (float)(wheel_speed) /10.0;
}
