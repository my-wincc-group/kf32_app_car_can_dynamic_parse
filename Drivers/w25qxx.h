#ifndef __W25QXX_H
#define __W25QXX_H
#include "system_init.h"
#include <stdlib.h>
#include "stdio.h"
#include <stdbool.h>
#include "cqueue.h"
#include "dma.h"
//#define _RW_CHK_D
#define W25X40B  				0XEF12
#define W25Q80 					0XEF13
#define W25Q16 					0XEF14
#define W25Q32 					0XEF15
#define W25Q64 					0XEF16
#define W25Q128					0XEF17
#define W25X_WriteEnable		0x06
#define W25X_WriteDisable		0x04
#define W25X_ReadStatusReg		0x05
#define W25X_WriteStatusReg		0x01
#define W25X_ReadData			0x03
#define W25X_FastReadData		0x0B
#define W25X_FastReadDual		0x3B
#define W25X_PageProgram		0x02
#define W25X_BlockErase			0xD8
#define W25X_SectorErase		0x20
#define W25X_ChipErase			0xC7		// 0x60
#define W25X_PowerDown			0xB9
#define W25X_ReleasePowerDown	0xAB
#define W25X_DeviceID			0xAB
#define W25X_ManufactDeviceID	0x90
#define W25X_JedecDeviceID		0x9F

// 为了增加读写文件的速度，仅操作每个扇区的前 W25_RW_DATA_SIZE 个字节数据，若后期不够用再扩展
#define W25_RW_DATA_SIZE		800
#define DATA_LEN_4B				4
#define DATA_LEN_8B				8
#define DATA_LEN_16B			16
#define DATA_LEN_32B			32
#define DATA_LEN_48B			48
#define DATA_LEN_128B			128
#define DATA_LEN_256B			256
#define DATA_LEN_512B			512
#define DATA_LEN_1024B			1024
#define DATA_LEN_4096B			4096

#define SPI_FLASH_CS_LOW()		GPIO_Set_Output_Data_Bits(GPIOC_SFR,GPIO_PIN_MASK_12,Bit_RESET)
#define SPI_FLASH_CS_HIGH()		GPIO_Set_Output_Data_Bits(GPIOC_SFR,GPIO_PIN_MASK_12,Bit_SET)

/*************************** 外部FLASH分区地址及大小，共2048KB *********************************/
#define OUT_FLASH_APP_START				0x00			// 应用程序 Bin存储的起始地址，大小为448KB
#define OUT_FLASH_BOOT_START			0x70000			// bootloader Bin存储的起始地址，大小为64KB
#define OUT_FLASH_PARAM_START			0x80000			// 升级版本信息的起始地址，版本+bin长度，大小为4KB
#define OUT_FLASH_SN_START				0x81000			// SN起始地址，大小为4KB，保证唯一性
#define OUT_FLASH_ALGPPARMGROUP_START 	0x82000			// AEB ALG 参数配置 版本号，或者参数数组ID
#define OUT_FLASH_ALGPPARM_START		0x83000			// 算法端起始地址，大小为4KB，算法配置参数,随后会分配100*4KB进行存储配置参数
#define OUT_FLASH_UPGRADE_INFO_START	0xE6000			// 存储4B的升级包的和校验码，大小为4KB
#define OUT_FLASH_SCREEN_START			0xE9000			// 小屏幕 Bin包存储的起始地址，大小为32KB
#define OUT_FLASH_LOSS_DATA_ID_START	0xF1000			// 存储数据防丢失frontid和endid,大小为4KB
#define OUT_FLASH_LOSS_OF_DATA_START	0xF2000			// 存储数据防丢失起始地址，大小为1024KB
#define OUT_FLASH_MILEAGE_START			0x1F2000		// 存储设备的里程数
#define OUT_FLASH_CAR_CAN_ANALY_CFG_START	0x1F3000	// 车CAN协议解析，大小为4KB
#define OUT_FLASH_SRR_START				0x1F4000		// 角雷达起始地址，大小4k
#define OUT_FLASH_CARDVR_CFG_START		0x1F5000		// 记录仪起始地址 ,大小为4KB
#define OUT_FLASH_TEST_LOGIC_START   	0x1F6000		// flash test logic 地址	update 20221014 lmz
#define OUT_FLASH_AEB_DEMO_CFG_START	0x1F7000		// AEB配置Demo起始地址 ,大小为4KB
// 剩余的起始地址，大小为40KB , 使用地址时，需要重新命名地址宏

#define EX_FLASH_SECTOR_SIZE			DATA_LEN_4096B
#define EX_FLASH_PAGE_SIZE				DATA_LEN_256B

#define APP_SECTION_SIZE				96				// 96 * 4 KB = 384 KB，对外部FLASH的操作(占用的bin文件的大小)
#define BOOT_SECTION_SIZE				DATA_LEN_16B
#define SCREEN_SECTION_SIZE				DATA_LEN_8B
/******************************* 0x80000 ***************************/
#define VERSION_SIZE		  		DATA_LEN_32B

#define PKG_SIZE_SIZE				DATA_LEN_8B				// storage upgrade package size
#define PKG_SIZE_OFFSET				VERSION_SIZE

#define RUN_VERSION_OFFSET			(VERSION_SIZE + PKG_SIZE_SIZE)
#define GPS_INTERVAL_OFFSET			(VERSION_SIZE + PKG_SIZE_SIZE + VERSION_SIZE)

/************************* 0x81000 ******************************/
#define DEV_SN_SIZE					DATA_LEN_32B				// storage device sn
//#define DEV_SN_OFFSET				0

#define DEV_PN_SIZE					DATA_LEN_32B				// storage device PN
#define DEV_PN_OFFSET				(DEV_SN_SIZE)

#define SECRET_SIZE					DATA_LEN_16B				// platform secret
#define SECRET_OFFSET				(DEV_PN_OFFSET + DEV_PN_SIZE)

#define EC_BAUD_SIZE				DATA_LEN_8B				// EC200U-CN baud
#define EC_BAUD_OFFSET				(SECRET_OFFSET + SECRET_SIZE)

#define HARDWARE_INFO_SIZE			DATA_LEN_48B				// 硬件信息
#define HARDWARE_INFO_OFFSET		(EC_BAUD_OFFSET + EC_BAUD_SIZE)

#define BOARD_PN_SIZE				DATA_LEN_32B				// 板卡PN信息
#define BOARD_PN_OFFSET				(HARDWARE_INFO_OFFSET + HARDWARE_INFO_SIZE)

#define BOARD_SN_SIZE				DATA_LEN_32B				// 板卡SN信息
#define BOARD_SN_OFFSET				(BOARD_PN_OFFSET + BOARD_PN_SIZE)


/************************* 0x83000 ******************************/
#define ALG_PARA_GROUPID_SIZE		DATA_LEN_8B

/************************* 0xE6000 ******************************/
#define UPGRADE_BINCHK_SIZE			DATA_LEN_8B

#define BOOT_RUN_VER_SIZE			DATA_LEN_32B
#define BOOT_RUN_VER_OFFSET			UPGRADE_BINCHK_SIZE

#define BOOT_UP_VER_SIZE			DATA_LEN_32B
#define BOOT_UP_VER_OFFSET			(BOOT_RUN_VER_OFFSET + BOOT_RUN_VER_SIZE)

#define BOOT_BIN_PKG_SIZE			DATA_LEN_8B
#define BOOT_BIN_PKG_OFFSET			(BOOT_UP_VER_OFFSET + BOOT_UP_VER_SIZE)

#define BOOT_PKG_CHK_SIZE			DATA_LEN_8B
#define BOOT_PKG_CHK_OFFSET			(BOOT_BIN_PKG_OFFSET + BOOT_BIN_PKG_SIZE)

#define SCREEN_RUN_VER_SIZE			DATA_LEN_32B
#define SCREEN_RUN_VER_OFFSET		(BOOT_PKG_CHK_OFFSET + BOOT_PKG_CHK_SIZE)

#define SCREEN_UP_VER_SIZE			DATA_LEN_32B
#define SCREEN_UP_VER_OFFSET		(SCREEN_RUN_VER_OFFSET + SCREEN_RUN_VER_SIZE)

#define SCREEN_BIN_PKG_SIZE			DATA_LEN_8B
#define SCREEN_BIN_PKG_OFFSET		(SCREEN_UP_VER_OFFSET + SCREEN_UP_VER_SIZE)

#define SCREEN_PKG_CHK_SIZE			DATA_LEN_8B
#define SCREEN_PKG_CHK_OFFSET		(SCREEN_BIN_PKG_OFFSET + SCREEN_BIN_PKG_SIZE)

#define SCREEN_IS_UPGRADE			DATA_LEN_4B
#define SCREEN_IS_UPGRADE_OFFSET	(SCREEN_PKG_CHK_OFFSET + SCREEN_PKG_CHK_SIZE)

#define APP_IS_UPGRADE				DATA_LEN_4B
#define APP_IS_UPGRADE_OFFSET		(SCREEN_IS_UPGRADE_OFFSET + SCREEN_IS_UPGRADE)

/************************* 0xF1000 ******************************/
#define DLP_ID_SIZE					DATA_LEN_16B				// 数据防丢失存在外部FLASH的ID号
/************************* 0x1F2000 ******************************/
#define MILEAGE_SIZE				DATA_LEN_8B				// 车辆里程计

/* ----------------------- 函数声明 ------------------------------- */
void 		W25QXX_Init(void);
void 		W25QXX_Erase_Sector(uint32_t Dst_Addr);
void 		W25QXX_Write_NoCheck(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite);
void 		W25QXX_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite);
void 		W25QXX_Read(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t NumByteToRead);
uint8_t		W25QXX_ReadSR(void);        			//读取状态寄存器
void 		W25QXX_WAKEUP(void);					// 唤醒
void 		W25QXX_PowerDown(void);					// 进入掉电模式
void 		W25QXX_Wait_Busy(void);           		//等待空闲
void 		W25QXX_Write_Enable(void);  			//写使能
void 		W25QXX_Write_Disable(void);				//写保护

void 		W25QXX_Read_No_Chk(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);

bool 		W25QXX_Write_Chk(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);	// 写+校验
bool 		W25QXX_Read_Chk(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);		// 读+校验

bool    	W25QXX_Write_Chk2(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);  	// 写+校验
bool    	W25QXX_Read_Chk2(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);   	// 读+校验

// 针对写结构体体设置
bool 		W25QXX_Write_One_Sector_Chk(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
bool 		W25QXX_Read_One_Sector_Chk(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToWrite);

uint8_t		Version_Compare(uint8_t *current_v, uint8_t *upgrade_v);
//void 		Print_Flash_bin_Content(uint32_t bin_size);			// debug print flash content

/*******************************   0x80000    ***************************/
bool 		Get_APP_UP_Version(uint8_t *p_version);			// 32B
uint32_t 	Get_APP_UP_PKG_size(void);						// 8B,疏忽项，正常应该存放到0x32000地址处
bool 		Get_APP_Run_Version(uint8_t *p_version);		// 32B
uint8_t		Get_GPS_Interval();								// 4B

bool 		Set_APP_UP_Version(uint8_t *p_version);
bool  		Set_APP_UP_PKG_size(uint32_t len);
bool 		Set_App_Run_Version(uint8_t *p_version);
bool 		Set_GPS_Interval(uint8_t time);

/*******************************   0x81000    ***************************/
bool		Get_Device_SN(char * p_sn);				// 32B
bool		Get_Device_PN(char * p_pn);				// 32B
bool 		Get_Secret(uint8_t *p_secret);			// 16B
//uint32_t 	Get_EC200U_CN_Baud();					// 8B
bool 		Get_Hardware_Info(uint8_t *p_info);		// 48B
bool 		Get_Board_SN(char * p_board_sn);		//16B
bool 		Get_Board_PN(char * p_board_pn);		//16B

bool 		Set_Device_SN(uint8_t * p_sn);
bool 		Set_Device_PN(uint8_t * p_pn);
bool 		Set_Secret(uint8_t *p_secret);			// platform secret
bool 		Set_EC200U_CN_Baud(uint8_t *p_baud);
bool		Set_Hardware_Info(uint8_t *p_info);
bool 		Set_Board_SN(uint8_t *p_sn);
bool 		Set_Board_PN(uint8_t *p_pn);

/*******************************   0x82000    ***************************/
uint8_t 	Get_Vehicle_Type_ExFlh();						// 8B
bool 		Set_Vehicle_Type_ExFlh(uint8_t id);

/*******************************   0x83000 ~ 0xE7FFF   ***************************/
bool		Get_AEB_Alg_Parameter(uint8_t *data,  uint16_t dataLen, uint8_t id);	// 默认大小为结构体大小
bool		Set_AEB_Alg_Parameter(uint8_t *data,  uint16_t dataLen,  uint8_t id);
/*******************************   0xE6000    ***************************/
uint32_t 	Get_APP_UP_Bin_Chk_Value_Compatible();				// 8B，为了兼容
bool 		Get_BOOT_Run_Version(uint8_t *p_version);			// 32B
bool 		Get_BOOT_UP_Version(uint8_t *p_version);			// 32B
uint32_t 	Get_BOOT_UP_PKG_size(void);							// 8B,疏忽项，正常应该存放到0x32000地址处
uint32_t 	Get_BOOT_UP_Bin_Chk_Value();						// 8B
bool 		Get_SCREEN_Run_Version(uint8_t *p_version);			// 32B
bool 		Get_SCREEN_UP_Version(uint8_t *p_version);			// 32B
uint32_t 	Get_SCREEN_UP_PKG_size(void);						// 8B,疏忽项，正常应该存放到0x32000地址处
uint32_t 	Get_SCREEN_UP_Bin_Chk_Value();						// 8B
uint8_t 	Get_SCREEN_IS_Upgrade_Flag();						// 4B
uint8_t 	Get_APP_IS_Upgrade_Flag();							// 4B	【0：不回复平台】【1：回复平台OTA升级成功】【2：回复平台OTA升级失败】

bool 		Set_APP_UP_Bin_Chk_Value_Compatible(uint32_t chk_value);
bool 		Set_BOOT_Run_Version(uint8_t *p_version);			// 32B
bool 		Set_BOOT_UP_Version(uint8_t *p_version);			// 32B
bool 		Set_BOOT_UP_PKG_size(uint32_t len);					// 8B,疏忽项，正常应该存放到0x32000地址处
bool 		Set_BOOT_UP_Bin_Chk_Value(uint32_t chk_value);		// 8B
bool 		Set_SCREEN_Run_Version(uint8_t *p_version);			// 32B
bool 		Set_SCREEN_UP_Version(uint8_t *p_version);			// 32B
bool 		Set_SCREEN_UP_PKG_size(uint32_t len);				// 8B,疏忽项，正常应该存放到0x32000地址处
bool 		Set_SCREEN_UP_Bin_Chk_Value(uint32_t chk_value);	// 8B
bool 		Set_SCREEN_IS_Upgrade_Flag(uint8_t flg);			// 4B
uint8_t 	Set_APP_IS_Upgrade_Flag(uint8_t flg);				// 4B	【0：不回复平台】【1：回复平台OTA升级成功】【2：回复平台OTA升级失败】

/*******************************   0xF1000 ***************************/
bool 		Get_Data_Loss_Prevention_ID(uint8_t *p_id_s);						//16B
bool 		Set_Data_Loss_Prevention_ID(uint16_t front_id, uint16_t end_id);	//16B

/*******************************   0xF2000~0x1F1FFF  数据防丢失***************************/


/*******************************   0x1F2000 里程数***************************/
float 		Get_Vehicle_Mileage();				//8B
bool 		Set_Vehicle_Mileage(float mileage);	//8B

/*******************************   0x1F3000 车CAN解析配置区 ***************************/
bool		Read_Car_Can_Analysis_Info(uint8_t *data, uint16_t data_len);	// 512B
bool 		Write_Car_Can_Analysis_Info(uint8_t *data, uint16_t data_len);	// 512B

#endif
