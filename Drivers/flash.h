/**
  ********************************************************************
  * 文件名  flash.h
  * 作  者   ChipON_AE/FAE_Group
  * 版  本  V2.1
  * 日  期  2019-11-16
  * 描  述  该文件提供了flash功能的相关读写函数功能定义
  *
  *********************************************************************
*/
#ifndef FLASH_H_
#define FLASH_H_
#include "system_init.h"
//#define	FLASH_BUFFER_MAX	    		256         	//整页的数据个数
//#define	FLASH_BUFFER_Halfpage			128         	//半页的数据个数

//*****************内部FLASH划分区****共512KB*******************
/* 内部FLASH分区地址及大小，共512KB */
#define IN_FLASH_BOOTLOADER_START			0x00000			// 存储Bootloader，大小为64KB
#define IN_FLASH_APP_START					0x10000			// 存储app，大小为300KB
#define IN_FLASH_RESERVED_START				0x5B000			// 预留大小为84KB
#define IN_FLASH_BOOT_TMP_START				0x70000			// 留有60KB,作为Bootloader BIn包的缓冲区
#define IN_FLASH_PARAM_UPGRADE_FLAG_START	0x7F000			// 1KB作为升级标志位
#define IN_FLASH_PARAM_SENSOR_MGT_START		0x7F400			// 1KB作为传感器参数配置区
// 内部FLASH还剩余2KB空间 0x7F800 0x7FC00

#define IN_FLASH_SIZE_8						8
#define IN_FLASH_SIZE_128					128
#define IN_FLASH_SIZE_256					256
#define IN_FLASH_SIZE_512					512
#define IN_FLASH_SIZE_1024					1024

#define IN_FLASH_SECTOR_SIZE				IN_FLASH_SIZE_1024
#define SENSOR_CFG_SIZE						IN_FLASH_SIZE_512
#define	FLASH_WAY_SIZE						4

#if 0
//void FLASH_Read_NByte(uint32_t page_address,uint8_t *p_DataBuffer,uint32_t offset,uint32_t leng);
//void FLASH_Modify_NByte(uint32_t page_address,uint8_t *p_DataBuffer,uint32_t offset,uint32_t leng);
//void print_page_data(uint32_t *buff, uint16_t length);

//void FLASH_HALFPAGE_WRITECODE_fun(uint32_t address,uint32_t *p_FlashBuffer,uint32_t length);
//void FLASH_PageWrite_fun(uint32_t address,uint32_t *p_FlashBuffer,uint8_t length);
//void FLASH_WriteCODE_ONE(uint32_t address,uint32_t *p_FlashBuffer);
//void FLASH_READCODE_fun(uint32_t address,uint32_t *p_FlashBuffer,uint32_t length);

//void FLASH_WriteCFG_ONE(uint32_t address,uint32_t *p_FlashBuffer);
//void FLASH_HALFPAGE_WRITECFG_fun(uint32_t address,uint32_t *p_FlashBuffer,uint32_t length);
//void FLASH_PageWrite_CFG_fun(uint32_t address,uint32_t *p_FlashBuffer,uint8_t length);


//地址必须为被8整除
//void FLASH_WriteByte(uint32_t address,uint8_t p_FlashBuffer);     //写byte
//void FLASH_WriteByte_4(uint32_t address,uint8_t buffer1,uint8_t buffer2,uint8_t buffer3,uint8_t buffer4);	// add lmz
//void FLASH_WriteHalfWord(uint32_t address,uint16_t p_FlashBuffer);//写HalWord
//void FLASH_WriteWord(uint32_t address,uint32_t p_FlashBuffer);    //写word
//void FLASH_WriteNByte(uint32_t address,uint8_t *p_FlashBuffer,uint32_t leng);//写多Byte

//uint32_t FLASH_ReadByte(uint32_t address,uint8_t *p_FlashBuffer);  //读byte
//uint32_t FLASH_ReadHalWord(uint32_t address,uint16_t *p_FlashBuffer);//读HalWord
//uint32_t FLASH_ReadWord(uint32_t address,uint32_t *p_FlashBuffer);//读Word
//void 	FLASH_ReadNByte(uint32_t address,uint8_t *p_FlashBuffer,uint32_t leng);//读多Byte
#endif

void 		FLASH_WriteWord_8_BYTE(uint32_t address,uint32_t p_FlashBuffer1,uint32_t p_FlashBuffer2);
//void 		Interal_Flash_8_Byte_to_Write_one_Sector(uint32_t mcu_address, uint8_t *write_buff, uint8_t jjj);
void 		Interal_Flash_8_Byte_to_Write_one_Sector(uint32_t write_addr, uint8_t *write_buff);
void 		Interal_Flash_8_Byte_To_Read_One_Sector(uint32_t read_addr, uint8_t *read_buff);

/************************* 0x7F000 ************************************/
uint8_t		Get_Flash_Way_InFlash();
uint8_t		Set_Flahs_Way_InFlash(uint8_t way);
/************************* 0x7F400 ************************************/
uint8_t 	Set_Sensor_MGT_Param_InFlash(uint8_t *mgt_param);
uint8_t 	Get_Sensor_MGT_Param_InFlash(uint8_t *mgt_param);
#endif /* FLASH_H_ */
