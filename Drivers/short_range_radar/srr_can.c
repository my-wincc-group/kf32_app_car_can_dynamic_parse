/*
 * srr_can.c
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#include "srr_can.h"
#include "c_queue.h"
#include "usart.h"
#include "tool.h"

#define SRR_CAN_BUFFER_LENGTH 36

struct CQueue srr_can_queue_[1] = { 0 };
struct can_frame srr_can_buf_[SRR_CAN_BUFFER_LENGTH] = { 0 };

static const uint32_t g_valid_can_frame[] = {
    0x750, 0x751, 0x752, 0x753, 0x754, 0x755, 0x756, 0x757, 0x758
};

static const uint32_t g_can_frame_size = sizeof(g_valid_can_frame);

bool initSrrCan() {
  CQueue_Create(srr_can_queue_, srr_can_buf_, SRR_CAN_BUFFER_LENGTH, sizeof(struct can_frame),
      C_QUEUE_UPDATE_NEW);
  return true;
}

int getSizeOfSrrCanInfo() {
  return CQueue_GetSize(srr_can_queue_);
}

bool findBeginSrrCanInfo(struct can_frame *frame, uint32_t frame_id) {
	while (1)
	{
    if (!CQueue_GetData(srr_can_queue_, frame)) {
      //USART1_Bebug_Print_Num("[findBeginSrrCanInfo]",0, 1);
   //     gcz_serial_v1_dma_printf(SERIAL_UART1,"1.frame_id %02x\r\n" ,frame->TargetID);
      return false;
    }
 //   gcz_serial_v1_dma_printf(SERIAL_UART1,"2.frame_id %02x\r\n" ,frame->TargetID);
    //USART1_Bebug_Print_Num("[findBeginSrrCanInfo]",1, 1);
    if (frame_id == frame->TargetID) {
      //USART1_Bebug_Print_Num("[findBeginSrrCanInfo]",1, 1);
      return true;
    } else {
      //USART1_Bebug_Print_Num("[findBeginSrrCanInfo]",2, 1);
      CQueue_Pop(srr_can_queue_);
    }
  }
}

bool receiveSrrCanInfo(struct can_frame * rx_frame) {
//  for (int i = 0; i < g_can_frame_size; i++) {
//    if (g_valid_can_frame[i] == rx_frame->TargetID) {
//      CQueue_Push(srr_can_queue_, rx_frame);
//      break;
//    }
//  }
  //Push the srr data to queue
  CQueue_Push(srr_can_queue_, rx_frame);
}

bool tranmitSrrCanInfo(struct can_frame * frame) {
  return CQueue_GetData(srr_can_queue_, frame);
}

bool popSrrCanInfo() {
  CQueue_Pop(srr_can_queue_);
}

