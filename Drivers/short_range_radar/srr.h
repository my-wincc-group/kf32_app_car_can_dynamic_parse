/*
 * SRR.h
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#ifndef SRR_H_
#define SRR_H_
#include "srr_type.h"

bool initSrr();
bool processSrr();

struct SRRObstacle * GetSrrObsIns();

bool Srr_IsBraking();
bool Srr_FilterSafetyDist(const float *input_data, float *filted_data);
bool Srr_FilterDeceleration(const float *input_data, float *filted_data);

bool Srr_BrakingState(float ego_speed, int64_t sys_tm, int64_t *pre_sys_tm, int8_t *state, float *acc);
bool TTC_BrakingState(float ego_speed, int64_t sys_tm, int64_t *pre_sys_tm,
    int8_t *state, float *acc);
#endif /* SRR_H_ */
