/*
 * srr_data.h
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#ifndef SRR_DATA_H_
#define SRR_DATA_H_
#include "srr_type.h"

#pragma pack(1)
union SrrCanFrame{
  uint64_t data;
  struct  {
    uint64_t frame_order :8;
    uint64_t main_object_id :8;
    uint64_t warning_grade :8;
    uint64_t alive_frame_no :8;
    uint64_t long_ttc :8;
    uint64_t lateral_ttc :8;
    uint64_t warning_zone :4;
    uint64_t motion_state :4;
    uint64_t malfunction :4;
    uint64_t reserve :4;
  }frame0;

  struct {
    uint64_t frame_order :8;
    uint64_t object_x :16;
    uint64_t object_y :12;
    uint64_t object_vel_x :12;
    uint64_t object_vel_y :10;
    uint64_t : 0;
  }frame1;

  struct {
    uint64_t frame_order :8;
    uint64_t object_acc_x :8;
    uint64_t object_acc_y :8;
    uint64_t reserve :40;
  }frame2;
};

#pragma pack()

#define NUM_ZONE 3
#define NUM_OBS_FRAME 3

bool initSrrData(int num_zone);

bool GetSrrCanFrame(struct can_frame * tx_can, void * frame, int data_size);

bool GetSRRInfo(const union SrrCanFrame frame[NUM_OBS_FRAME], struct SRRObstacle * info);

bool GetZoneObs(struct SRRObstacle srr_obstacle_[NUM_ZONE]);
#endif /* SRR_DATA_H_ */
