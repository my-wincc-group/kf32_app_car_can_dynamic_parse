/*
 * srr_info_type.h
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#ifndef SRR_INFO_TYPE_H_
#define SRR_INFO_TYPE_H_

#include <stdint.h>
#include <stdbool.h>
#include "../canhl.h"
#include "c_data_type.h"

#define G_SRR_NUM_OBS 64

enum SRRCarZone {
  UNKNOWN_CAR_ZONE = 0, RIGHT_MIDDLE_CAR_ZONE, RIGHT_BACK_CAR_ZONE, RIGHT_FRONT_CAR_ZONE,
};

enum SRRMOtionState {
  UNKNOWN_MOTION_STATE = 0, MOVING_MOTION_STATE, STOPED_MOTION_STATE, STANDING_MOTION_STATE,
};

enum SRRMalfunction {
  SENSOR_OK = 0, SENSOR_FAILURE, SENSOR_PERFORM_DECREASED, SENSOR_COMMUNICATION_FAILURE,
};

struct SRRObstacle {
  bool is_cipv;

  uint8_t id;
  uint8_t tracking_num;
  uint8_t warning_grade;

  int64_t system_time;

  struct Point2f ttc;
  struct Point2f dist;
  struct Point2f vel;
  struct Point2f acc;

  enum SRRCarZone zone;
  enum SRRMOtionState motion_state;
  enum SRRMalfunction malfunction;
};

struct SRRInfo {
  struct SRRObstacle obstacle[G_SRR_NUM_OBS];
};

#endif /* SRR_INFO_TYPE_H_ */
