/*
 * c_data_type.h
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#ifndef C_DATA_TYPE_H_
#define C_DATA_TYPE_H_

struct Point2f {
  float x;
  float y;
};

struct Point3f {
  float x;
  float y;
  float z;
};

#endif /* C_DATA_TYPE_H_ */
