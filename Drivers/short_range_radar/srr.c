/*
 * SRR.c
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#include "srr.h"
#include <stdbool.h>
#include <float.h>
#include "aeb_cms_alg.h"

#include "srr_can.h"
#include "srr_data.h"
#include "ego/ego_info.h"
#include "usart.h"
#include "c_queue.h"
#include "c_filter.h"
#include "tool.h"
#define BUFFER_LENGTH 10

struct SRRObjReappearCount {
  volatile uint8_t id;
  volatile uint8_t reappear_Count; //重复出现帧计数
  volatile uint8_t reappear;
};

static float dece_buffer_[BUFFER_LENGTH];
static float dist_buffer_[BUFFER_LENGTH];

static struct CQueue dece_queue_[1] = { 0 };
static struct CQueue dist_queue_[1] = { 0 };
static struct CFilter dece_filter_[1] = { 0 };
static struct CFilter dist_filter_[1] = { 0 };

static struct SRRObstacle srr_obstacle_[NUM_ZONE];
struct SRRObjReappearCount srr_obj_reappear_data[3] = { 0 };

static int Compare(void *max_value, void *min_value);

volatile uint8_t get_srr_data_flag_ = 0; //获取角雷达障碍物数据标志 失败为0，成功计数 +1

bool initSrr() {
  CQueue_Create(dece_queue_, dece_buffer_, BUFFER_LENGTH, sizeof(float), C_QUEUE_UPDATE_NEW);
  CQueue_Create(dist_queue_, dist_buffer_, BUFFER_LENGTH, sizeof(float), C_QUEUE_UPDATE_NEW);

  CFilter_Init(dece_filter_, dece_queue_, Compare);
  CFilter_Init(dist_filter_, dist_queue_, Compare);

  initSrrData(NUM_ZONE);
  initSrrCanFrame();
  initSrrCan();
  return true;
}
//extern void GetElemData_test(struct CQueue * self, int index, void *data);
bool processSrr() {

  if (!GetZoneObs(srr_obstacle_)) {
//    get_srr_data_flag_ = 0;
//	  USART1_Bebug_Print_Num("get_srr_data_flag_", 0,1,1);

    return false;
  }
//  USART1_Bebug_Print_Num("get_srr_data_flag_", 1,1,1);
  get_srr_data_flag_++;
  if (get_srr_data_flag_ >= 3) {
    get_srr_data_flag_ = 3;
  }
//  static uint32_t GetSrr_clk = 0;
//  USART1_Bebug_Print_Num("processSrr", SystemtimeClock - GetSrr_clk,1,0);
//  USART1_Bebug_Print_Num("getSizeOfSrrCanInfo", getSizeOfSrrCanInfo(),1,0);
//  GetSrr_clk = SystemtimeClock;
  return true;
}

struct SRRObstacle * GetSrrObsIns() {
  struct SRRObjReappearCount srr_obj_reappear_tmp[3] = { 0 };
  static uint16_t noData_count = 0;

//  static uint32_t AngleRadar_clk = 0;
  static uint32_t GetSrr_clk = 0;
//  USART1_Bebug_Print_Num("syslockLoopTime", SystemtimeClock - AngleRadar_clk,1,1);
//  AngleRadar_clk = SystemtimeClock;
  if (get_srr_data_flag_ >= 1) {
//	  USART1_Bebug_Print_Num("GetSrr_clk", SystemtimeClock - GetSrr_clk,1,0);
//	  USART1_Bebug_Print_Num("Remaining", get_srr_data_flag_,1,0);
    GetSrr_clk = SystemtimeClock;
//	  USART1_Bebug_Print_Num("get_srr_data_flag_", get_srr_data_flag_, 1, 1);
    get_srr_data_flag_--;
    if (get_srr_data_flag_ <= 0) {
      get_srr_data_flag_ = 0;
    }
    noData_count = 0;
    for (int i = 0; i < 3; i++) {
      srr_obstacle_[i].malfunction = SENSOR_OK;
    }
    for (int i = 0; i < 3; i++) {
      srr_obj_reappear_tmp[i].id = 255; // 默认id 255
      srr_obj_reappear_tmp[i].id = srr_obstacle_[i].id;
      srr_obj_reappear_data[i].reappear = 0;
      srr_obj_reappear_tmp[i].reappear = 0;

    }
    for (int j = 0; j < 3; j++) {
      for (int i = 0; i < 3; i++) {
        if (srr_obj_reappear_data[i].id == srr_obj_reappear_tmp[j].id) {
          if (srr_obstacle_[i].id != 255) {
            srr_obj_reappear_data[i].reappear_Count++;
          }
          srr_obj_reappear_data[i].reappear = 1;
          srr_obj_reappear_tmp[j].reappear = 1;
          srr_obj_reappear_tmp[j].reappear_Count = srr_obj_reappear_data[i].reappear_Count;

          if (srr_obj_reappear_data[i].reappear_Count >= 250) {
            srr_obj_reappear_data[i].reappear_Count = 250;
          }

        }
      }
    }
    for (int i = 0; i < 3; i++) {
      if (!(srr_obj_reappear_tmp[i].reappear)) {
        for (int j = 0; j < 3; j++) {
          if (!(srr_obj_reappear_data[j].reappear)) {
            srr_obj_reappear_data[j].id = srr_obj_reappear_tmp[i].id;
            srr_obj_reappear_data[j].reappear = 1;
            srr_obj_reappear_tmp[i].reappear = 1;
            srr_obj_reappear_data[j].reappear_Count = 1;
            srr_obj_reappear_tmp[j].reappear_Count = srr_obj_reappear_data[i].reappear_Count;
            break;
          }
        }
      }
    }
    for (int i = 0; i < 3; i++) {
    	if (srr_obj_reappear_tmp[i].reappear_Count >= 2) {
        if (srr_obstacle_[i].id != 255) {
          srr_obstacle_[i].is_cipv = TRUE;
        }
        //			USART1_Bebug_Print_Num("SRROb_id>5", srr_obstacle_[i].id,1,1);
//        			USART1_Bebug_Print_Num("srr_obj_ID>5", srr_obj_reappear_data[i].id,1,1);
//        			USART1_Bebug_Print_Num("srr_obj_alive_no", srr_obstacle_[i].tracking_num,1,1);
//        			USART1_Bebug_Print_Num("zone", srr_obstacle_[i].zone,1,1);
//        			USART1_Bebug_Print_Num("warning_grade", srr_obstacle_[i].warning_grade,1,1);
        //			USART1_Bebug_Print_Num("SRROb_count>5", srr_obj_reappear_data[i].reappear_Count,1,1);
        //			USART1_Bebug_Print_Num("is_cipv>", srr_obstacle_[i].is_cipv,1,1);
      } else {
        srr_obstacle_[i].is_cipv = FALSE;
        //			USART1_Bebug_Print_Num("SRROb_id", srr_obstacle_[i].id,1,1);
        //			USART1_Bebug_Print_Num("srr_obj_ID", srr_obj_reappear_data[i].id,1,1);
        //			USART1_Bebug_Print_Num("SRROb_count", srr_obj_reappear_data[i].reappear_Count,1,1);
        //			USART1_Bebug_Print_Num("is_cipv", srr_obstacle_[i].is_cipv,1,1);
      }
    }
//		for(int i = 0; i < 3; i++)
//		{
//			USART1_Bebug_Print_Num("srrObstacle_ID", srr_obstacle_[i].id,1,0);
//			USART1_Bebug_Print_Num("srrOb_is_cipv", srr_obstacle_[i].is_cipv,1,0);
//		}
  } else {
//		USART1_Bebug_Print_Num("get_srr_data_flag_", get_srr_data_flag_,1,1);
    noData_count++;
    if (noData_count >= 90000) {
      for (int i = 0; i < 3; i++) {
        srr_obstacle_[i].malfunction = SENSOR_COMMUNICATION_FAILURE;
      }
    }
//    srr_obstacle_[0].is_cipv = false;
//    srr_obstacle_[1].is_cipv = false;
//    srr_obstacle_[2].is_cipv = false;
  }

  return srr_obstacle_;
}

bool Srr_IsBraking() {
  float min_data = 0.f;
  if (CQueue_IsFull(dece_filter_->queue)) {
    CFilter_GetMinValue(dece_filter_, &min_data);
  }
  if (min_data > FLT_EPSILON) {
    return true;
  }
  return false;
}

bool Srr_FilterSafetyDist(const float *input_data, float *filted_data) {
  CFilter_InputData(dist_filter_, input_data);
  CFilter_GetMaxValue(dist_filter_, filted_data);
}

bool Srr_FilterDeceleration(const float *input_data, float *filted_data) {
  CFilter_InputData(dece_filter_, input_data);
  CFilter_GetMaxValue(dece_filter_, filted_data);
}

static int Compare(void *max_value, void *min_value) {
  if (*((float *) max_value) > *((float *) min_value)) {
    return 1;
  } else if (*((float *) max_value) == *((float *) min_value)) {
    return 0;
  } else {
    return -1;
  }
}

// 减速度按时间平分分成四个阶段a1 = 3/2 a2 = 9/4 a3 = 27/9 a4， t = ttc / 4 计算a1约等于 2 *(s - v*ttc) / 3
static float CalcBrakingAcc(float response_tm, float dist, float relative_speed) {
  const float ttc = 2.f;
  const float safe_dist = dist - relative_speed * response_tm; // ego_speed (m/s), response time 0.7s;

  if (relative_speed > 0) {
    return 0.f;
  }

  relative_speed = -relative_speed;
  relative_speed += 1.5f; // speed buffer 1.5m/s
  return (2.f * (safe_dist - relative_speed * ttc) / 3.f);
}

bool Srr_BrakingState(float ego_speed, int64_t sys_tm, int64_t *pre_sys_tm, int8_t *state, float *acc) {
  const int64_t segment_tm = 500; // ms
  const int64_t response_tm = 200; //假定系统响应时间为 200 * 4 = 800ms

  static float step_acc = 0.f;
  static float response_acc = 0.f;
  const float response_segment_num = 4.f;

  const float min_acc = 2.5f;

  if (ego_speed < 0.5f) {
    *state = 0;
    return true;
  }

  switch (*state) {
  case 0:
    *state += 1;
    *pre_sys_tm = sys_tm;

    response_acc = *acc / response_segment_num;
    step_acc = response_acc;
    *acc = step_acc > min_acc ? step_acc : min_acc;
    break;
  case 1:
  case 2:
  case 3:
    if (sys_tm - *pre_sys_tm > response_tm) {
      *state += 1;
      *pre_sys_tm = sys_tm;

      step_acc += response_acc;
      *acc = step_acc > min_acc ? step_acc : min_acc;
    }
    break;
  case 4:
    if (sys_tm - *pre_sys_tm > response_tm) {
      *state += 1;
      *pre_sys_tm = sys_tm;
    }
    break;
  case 5:
  case 6:
  case 7:
  case 8:
    if (sys_tm - *pre_sys_tm > segment_tm) {
      *state += 1;
      *pre_sys_tm = sys_tm;
      step_acc *= 0.666666f; // 2/3 = 0.6666666
      *acc = step_acc > min_acc ? step_acc : min_acc;
    }
    break;
  case 9:
    *acc = 0.f;
    *state = 0;
    break;
  default:
    *acc = 0.f;
    *state = 0;
    return false;
  }

  return true;
}


bool TTC_BrakingState(float ego_speed, int64_t sys_tm, int64_t *pre_sys_tm,
    int8_t *state, float *acc) {
  const int64_t segment_tm = 600; // ms
  const int64_t response_tm = 10; //假定系统响应时间为 200 * 4 = 800ms

  static float step_acc = 0.f;
  static float response_acc = 0.f;
  const float response_segment_num = 4.f;

  const float min_acc = 1.5f;

//  if (ego_speed < 0.1f) {
//    *state = 0;
//    return true;
//  }

  switch (*state) {
  case 0:
    *state += 1;
    *pre_sys_tm = sys_tm;

    response_acc = *acc / response_segment_num;
    step_acc = response_acc;
    *acc = step_acc > min_acc ? step_acc : min_acc;
    break;
  case 1:
  case 2:
  case 3:
    if (sys_tm - *pre_sys_tm > response_tm) {
      *state += 1;
      *pre_sys_tm = sys_tm;

      step_acc += response_acc;
      *acc = step_acc > min_acc ? step_acc : min_acc;
    }
    break;
  case 4:
    if (sys_tm - *pre_sys_tm > response_tm) {
      *state += 1;
      *pre_sys_tm = sys_tm;
    }
    break;
  case 5:
  case 6:
  case 7:
  case 8:
    if (sys_tm - *pre_sys_tm > segment_tm) {
      *state += 1;
      *pre_sys_tm = sys_tm;
      step_acc *= 0.666666f; // 2/3 = 0.6666666
      *acc = step_acc > min_acc ? step_acc : min_acc;
    }
    break;
  case 9:
    if(ego_speed < 0.1f){
      *state += 1;
      *acc += 1.f;
    }
    break;
  case 10:
    if (sys_tm - *pre_sys_tm > segment_tm) {
      *state += 1;
    }
    break;
  case 11:
    *acc = 0.f;
    *state = 0;
    break;
  default:
    *acc = 0.f;
    *state = 0;
    return false;
  }

  return true;
}












