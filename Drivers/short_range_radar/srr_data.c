/*
 * srr_data.c
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#include "srr_data.h"
#include <string.h>
#include "usart.h"
#include "srr_can.h"
#include "Tasks/common.h"

static int g_num_frame = 0;

static struct can_frame tx_can_;
static union SrrCanFrame can_frame_[9];

static const int k_begin_frame_id = 0x750;

bool initSrrData(int num_zone) {
  g_num_frame = num_zone * NUM_OBS_FRAME;
}

bool GetSrrCanFrame(struct can_frame * tx_can, void * frame, int data_size) {
  if (NULL == memcpy(frame, tx_can->data, data_size)) {
    return false;
  } else {
    return true;
  }
}

bool GetSRRInfo(const union SrrCanFrame frame[], struct SRRObstacle * info) {

  if (255 == frame[0].frame0.main_object_id || frame[0].frame0.alive_frame_no < 3) {
    //USART1_Bebug_Print_Num("[START Info id false]", frame[0].frame0.main_object_id, 2); // 勿删除
    info->is_cipv = false;
    info->id = frame[0].frame0.main_object_id;
    info->motion_state = UNKNOWN_MOTION_STATE;
    return false;
  }
  //USART1_Bebug_Print_Num("[GetSRRInfo]",frame[0].frame0.main_object_id, 1);
//  info->alive_no = frame[0].frame0.alive_frame_no;
  info->is_cipv = true;
  info->id = frame[0].frame0.main_object_id;
  info->zone = frame[0].frame0.warning_zone;
  info->tracking_num = frame[0].frame0.alive_frame_no;
  info->warning_grade = frame[0].frame0.warning_grade;
  info->ttc.x = ((float) frame[0].frame0.long_ttc) * 0.05f;
  info->ttc.y = ((float) frame[0].frame0.lateral_ttc) * 0.05f;
  info->motion_state = frame[0].frame0.motion_state;
  info->dist.x = ((float) frame[1].frame1.object_x) * 0.02f - 655.36f;
  info->dist.y = ((float) frame[1].frame1.object_y) * 0.02f - 40.96f;
  info->vel.x = ((float) frame[1].frame1.object_vel_x) * 0.1f - 204.8f;
  info->vel.y = ((float) frame[1].frame1.object_vel_y) * 0.1f - 51.2f;
  info->acc.x = ((float) frame[2].frame2.object_acc_x) * 0.2f - 25.6f;
  info->acc.y = ((float) frame[2].frame2.object_acc_y) * 0.2f - 25.6f;
  info->malfunction = frame[0].frame0.malfunction;
  info->system_time = SystemtimeClock;

//  USART1_Bebug_Print_Num("[START Info id]", info->id, 1, 1); // 勿删除
//  USART1_Bebug_Print_Num("[START Info tracking_num]", info->tracking_num, 1, 1); // 勿删除
//  USART1_Bebug_Print_Num("[START Info warning_grade]", info->warning_grade, 1, 1); // 勿删除
//  USART1_Bebug_Print_Num("[START Info motion_state]", info->motion_state, 1, 1); // 勿删除
//  USART1_Bebug_Print_Num("[START Info zone]", info->zone, 1, 1); // 勿删除
//  USART1_Bebug_Print_Num("[START Info system_time]", info->system_time, 1, 1); // 勿删除
//
//  USART1_Bebug_Print_Flt("[START Info vel  x]", info->vel.x, 1, 1); // 勿删除
//  USART1_Bebug_Print_Flt("[START Info vel  y]", info->vel.y, 1, 1); // 勿删除
//  USART1_Bebug_Print_Flt("[START Info dist  x]", info->dist.x, 1, 1); // 勿删除
//  USART1_Bebug_Print_Flt("[START Info dist  y]", info->dist.y, 1, 1); // 勿删除
//
//  USART1_Bebug_Print_Flt("[START Info acc  x]", info->acc.x, 1, 1); // 勿删除
//  USART1_Bebug_Print_Flt("[START Info acc  y]", info->acc.y, 1, 1); // 勿删除

  return true;
}

static bool canToSrr(int begin_id, int index) {

  if (!tranmitSrrCanInfo(&tx_can_)) {
    return false;
  }

  if (begin_id == tx_can_.TargetID && 0 != index) {
    return false;
  }

  popSrrCanInfo();

  if (begin_id + index != tx_can_.TargetID) {
    return false;
  }

  if (!GetSrrCanFrame(&tx_can_, can_frame_ + index, sizeof(union SrrCanFrame))) {
    return false;
  }

  return true;
}
//获取CAN缓存数据
bool get_can_data_form_cahce(struct can_frame *frame)
{
	if (!CQueue_GetData(srr_can_queue_, frame))
	{
		return false;
	}

	return true;
}

#define WHOLE_SRR_CAN_PKG_RX_NBR		9
#define CAN_DATA_NBR					3
typedef struct __srr_can_process
{
	struct can_frame can_data;
	uint8_t put_cnt;
	uint8_t rx_tab;
}SRR_CAN_PROCESS;

typedef struct __srr_can_opt
{
	SRR_CAN_PROCESS  srr_head_right[CAN_DATA_NBR];		//右前
	SRR_CAN_PROCESS  srr_middle_right[CAN_DATA_NBR];	//右侧
	SRR_CAN_PROCESS  srr_back_right[CAN_DATA_NBR];		//右后
	uint8_t total_srr_can_pkg_cnt;						//所有SRR接收数据表计数
	bool is_full[WHOLE_SRR_CAN_PKG_RX_NBR];				//理想状态9帧数据插入标志位置
}SRR_CAN_DATA_OPT;
SRR_CAN_DATA_OPT g_st_srr_can_opt;
//获取焦雷达CAN数据包的有效个数
uint8_t get_srr_rx_pkg_tab_nbr(SRR_CAN_PROCESS *p_st_srr_can_process,uint8_t max_nbr)
{
	uint8_t res = 0;
	for (uint8_t i1 = 0;i1 < max_nbr;i1++)
	{
		if (p_st_srr_can_process[i1].rx_tab == true)
			res++;
	}
	return res;
}
//处理角雷达CAN数据
void process_srr_can_data(struct can_frame *frame,SRR_CAN_DATA_OPT *p_st_srr_can_opt)
{
	uint32_t can_id;
	if ((frame->TargetID == 0X750) || (frame->TargetID == 0X751) || (frame->TargetID == 0X752))
	{
		can_id = frame->TargetID - 0X750;
		memcpy(&p_st_srr_can_opt->srr_head_right[can_id].can_data,frame,sizeof(struct can_frame));
		p_st_srr_can_opt->srr_head_right[can_id].rx_tab = true;
		//p_st_srr_can_opt->srr_head_right[can_id].put_cnt++;
	}
	else if ((frame->TargetID == 0X753) || (frame->TargetID == 0X754) || (frame->TargetID == 0X755))
	{
		can_id = frame->TargetID - 0X753;
		memcpy(&p_st_srr_can_opt->srr_middle_right[can_id].can_data,frame,sizeof(struct can_frame));
		p_st_srr_can_opt->srr_middle_right[can_id].rx_tab = true;
		//p_st_srr_can_opt->srr_middle_right[can_id].put_cnt++;
	}
	else if ((frame->TargetID == 0X756) || (frame->TargetID == 0X757) || (frame->TargetID == 0X758))
	{
		can_id = frame->TargetID - 0X756;
		memcpy(&p_st_srr_can_opt->srr_back_right[can_id].can_data,frame,sizeof(struct can_frame));
		p_st_srr_can_opt->srr_back_right[can_id].rx_tab = true;
		//p_st_srr_can_opt->srr_back_right[can_id].put_cnt++;
	}
}
#include "tool.h"
//检查是否关闭POP
bool check_is_need_close_pop(struct can_frame *p_can_frame,SRR_CAN_DATA_OPT *p_st_srr_can_opt)
{
	bool res = false;
	uint32_t can_id;
	can_id = p_can_frame->TargetID - 0X750;
	if ((p_can_frame->TargetID >= 0x750) && (p_can_frame->TargetID <= 0x758))
	{
		if (p_st_srr_can_opt->is_full[can_id] == true)
			res = true;
		else
		{
			for (uint8_t i1 = 0;i1 <= can_id;i1++)
			{
				p_st_srr_can_opt->is_full[i1] = true;
			//	gcz_serial_v1_dma_printf(SERIAL_UART1,"(%d)%d ",i1,p_st_srr_can_opt->is_full[i1]);
			}
			//gcz_serial_v1_dma_printf(SERIAL_UART1,"\r\n");
		}
	}
	return res;
}

//处理角雷达CAN数据包的操作
bool process_srr_can_pkg_opt(struct SRRObstacle srr_obstacle[])
{
	if(getSizeOfSrrCanInfo() < 9){
		return false;
	}
	struct can_frame srr_can_frame;
	g_st_srr_can_opt.total_srr_can_pkg_cnt = 0;
	bool flag = true;
	for (int i1 = 0;i1 < WHOLE_SRR_CAN_PKG_RX_NBR;i1++)
	{
		//如果获取到数据成功
		if (get_can_data_form_cahce(&srr_can_frame))//先从缓存拿CAN数据
		{
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"%X ",srr_can_frame.TargetID);
			//每次处理，第一次获取数据成功时，清空缓存，避免历史数据干扰
			if (flag)
			{
				flag = false;
				memset(&g_st_srr_can_opt,0,sizeof(g_st_srr_can_opt));
			}
			process_srr_can_data(&srr_can_frame,&g_st_srr_can_opt);//分包，标记
			g_st_srr_can_opt.total_srr_can_pkg_cnt++;
			if (check_is_need_close_pop(&srr_can_frame,&g_st_srr_can_opt) == false)
				CQueue_Pop(srr_can_queue_);//pop调整环形队列指针位置
			else
				break;
		}
		else
			break;
	}
//	if (flag == false)
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"\r\n");
	//大于0说明有CAN包接收到
	if (g_st_srr_can_opt.total_srr_can_pkg_cnt > 0)
	{
		uint8_t head_right_can_pkg_nbr = get_srr_rx_pkg_tab_nbr(g_st_srr_can_opt.srr_head_right,CAN_DATA_NBR);
		uint8_t middle_right_can_pkg_nbr = get_srr_rx_pkg_tab_nbr(g_st_srr_can_opt.srr_middle_right,CAN_DATA_NBR);
		uint8_t back_right_can_pkg_nbr = get_srr_rx_pkg_tab_nbr(g_st_srr_can_opt.srr_back_right,CAN_DATA_NBR);
		//开始判断数据包合理性
		uint8_t i1;
				//每次赋值前先给定无效值，避免上次数据的历史遗留导致误处理
		for (i1 = 0;i1 < 9;i1++)
			memset(&can_frame_[i1],255,sizeof(union SrrCanFrame));

		if (head_right_can_pkg_nbr == 3)//导入右前
		{
			for (i1 = 0;i1 < 3;i1++)
			{
				GetSrrCanFrame(&g_st_srr_can_opt.srr_head_right[i1].can_data,
								can_frame_ + i1,
								sizeof(union SrrCanFrame));
			}
		}
		if (middle_right_can_pkg_nbr == 3)//导入右测
		{
			for (i1 = 3;i1 < 6;i1++)
			{
				GetSrrCanFrame(&g_st_srr_can_opt.srr_middle_right[i1-3].can_data, can_frame_ + i1, sizeof(union SrrCanFrame));
//				gcz_serial_v1_dma_printf(SERIAL_UART1,"can_frame_%d \r\n",g_st_srr_can_opt.srr_middle_right[0].can_data.TargetID);
			}
		}
		if (back_right_can_pkg_nbr == 3)//导入右后
		{
			for (i1 = 6;i1 < 9;i1++)
			{
				GetSrrCanFrame(&g_st_srr_can_opt.srr_back_right[i1-6].can_data, can_frame_ + i1, sizeof(union SrrCanFrame));
			}
		}
		//赋值CAN包结果
		for (int j = 0; j < NUM_ZONE; j++)
		{
			GetSRRInfo(can_frame_ + j * 3, srr_obstacle + j);
		}
		if(head_right_can_pkg_nbr == 3 || middle_right_can_pkg_nbr == 3 ||  back_right_can_pkg_nbr == 3)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
}


bool GetZoneObs(struct SRRObstacle srr_obstacle[]) {

	return(process_srr_can_pkg_opt(srr_obstacle));
#if 0
  if (!findBeginSrrCanInfo(&tx_can_, k_begin_frame_id) || getSizeOfSrrCanInfo() < 9) {
  //  USART1_Bebug_Print_Num("[<<<<<<<<<<<<<<<<<<<<<]",0, 1,0);
    return 0;
  }


//  USART1_Bebug_Print_Num("[>>>>>>>>>>>>>>>>>>>>]",1, 1,0);
  for (int i = 0; i < g_num_frame; i++) {
    if (!canToSrr(k_begin_frame_id, i)) {
      return 0;
    }
  }

  for (int j = 0; j < NUM_ZONE; j++) {
    GetSRRInfo(can_frame_ + j * 3, srr_obstacle + j);
  }
  return 1;
#endif

}

