/*
 * SRR_CAN.h
 *
 *  Created on: 2022-7-11
 *      Author: acai
 */

#ifndef SRR_CAN_H_
#define SRR_CAN_H_
#include "../canhl.h"
#include "../Tasks/common.h"
#include "c_queue.h"

bool initSrrCan();
bool destorySrrCan();

int getSizeOfSrrCanInfo();

bool findBeginSrrCanInfo(struct can_frame *frame, uint32_t frame_id);
bool receiveSrrCanInfo(struct can_frame * rx_frame);
bool tranmitSrrCanInfo(struct can_frame * tx_frame);
bool popSrrCanInfo();

extern struct CQueue srr_can_queue_[1];
#endif /* SRR_CAN_H_ */
