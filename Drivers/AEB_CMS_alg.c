/*
 * AEB_CMS_alg.c
 *
 *  Created on: 2021-11-25
 *  Author: xujie
 */
#include "aeb_cms_alg.h"
#include <float.h>
#include "stereo_camera.h"
#include "wheelSpeed.h"
#include "at_parse_alg_para.h"
#include "kunlun_fcw_ldw.h"
#include "AEB_upload_info.h"
#include "usart.h"
#include "srr.h"
#include "math.h"
#include "imu.h"
#include "aeb_sensor_management.h"
#include "params.h"
#include "wt_jy901_imu.h"
#include "tool.h"
#include "movement_trail_lib.h"
#include "application/subsystem_application/imu_status_detection_application/imu_app.h"
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#ifndef fabs
#define fabs(a)				((a > 0) ? a : (-a))
#endif

#ifndef speed_meter_p_s
#define speed_meter_p_s(v)            ((v) / (3.6))
#endif

#define TURN_ON   0x01
#define TURN_OFF  0x00

#define MIN_TIME_FORCE_FEEL  0.2


#define delay_count     5
#define keep_count      2

#define approacing_disppear_keep_count      2

#define break_calculate_cancel      0x00
#define break_calculate_continue    0x01
#define break_cancel_NONE       0x00
#define break_cancel_TTC     	0x02
#define break_cancel_HMW     	0x03
#define break_cancel_TTCHMW     0x04

#define effictive_distance_hmw    3

#define LOGLEVEL    LOG_NONE

#define cal_ttc_history_n 4
#define approaching_history_n 5
#define time_gap_cam_to_radar 5

#define USE_CAM_Warning_info 1

#define ReCal_HMW_TTC 1

#define USE_ReCal_HMW_TTC 0

#define MIN_BREAK_COOLING_TIME 0.5

#define TIME_GAPE_OF_TURN_LIGHT 1.0

#define SOUND_OF_AEB_SYSTEM_TURN_ON  0x08
#define SOUND_OF_AEB_SYSTEM_TURN_OFF 0x29

#define SOUND_OF_LDW_WARNING_TURN_ON  0x14
#define SOUND_OF_LDW_WARNING_TURN_OFF 0x2A

#define CROSSWISE_CACHE_SIZE 5
#define CROSSWISE_STATUS_JUDGE_TIMES 5
#define CROSSWISE_STATUS_MIN_DISTANCE_CHANGE 0.1

#define MAX_DECELERATE_OUTPUT 10.0

#define STRATEGY 2

/**********************************************************
 * Public:
 * Share with Outside Function
 * ********************************************************
 */
/////////////////////////////////////////////////////////
//System Parameters Status Check
/////////////////////////////////////////////////////////
uint8_t Status_System_Param = 0x01;// not ready


/////////////////////////////////////////////////////////
//System Config Set From GPIO
/////////////////////////////////////////////////////////
uint8_t g_AEB_ON_OFF_GPIO = 0x01;


/////////////////////////////////////////////////////////
//System Config Set From Displayer
/////////////////////////////////////////////////////////
uint8_t g_AEB_ON_OFF_Displayer = 0x01;
uint8_t g_LDW_ON_OFF_Displayer = 0x01;// Just use in Displayer
uint8_t g_AEB_ON_OFF_Displayer_Feedback = 0x01;
uint8_t g_LDW_ON_OFF_Displayer_Feedback = 0x01;// Just use in Displayer

/////////////////////////////////////////////////////////
//System Config Parameters, Set by FAE
/////////////////////////////////////////////////////////
volatile KunLun_AEB_Para_Cfg old_rParms 			= {0};  	// read Copy
uint8_t g_AEB_ON_OFF_BT = 1;
uint8_t g_LDW_ON_OFF_BT = 1;       // Just use in Displayer
uint8_t g_SSS_ON_OFF_BT = 1;

/////////////////////////////////////////////////////////
//System Config Parameters, Set by User by Cam Displayer
/////////////////////////////////////////////////////////
float g_HMW_Warning_Time_From_Cam_Displayer;

/////////////////////////////////////////////////////////
//Parameters for outside function reading(just reading)
/////////////////////////////////////////////////////////
//-----Brake Flag
float g_AEB_CMS_outside_dec_output       = 0.0;
uint8_t g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x00;
uint8_t g_AEB_CMS_outside_BrakeBy_CMSTTC = 0x00;
uint8_t g_AEB_CMS_outside_BrakeBy_AEBTTC = 0x00;
//-----Middle Status
uint8_t g_AEB_CMS_approcahing 			= 0x00;
uint8_t g_CMS_hmw_warning 				= 0x00;
uint8_t g_CMS_ttc_warning 				= 0x00;
uint8_t g_AEB_TTC_Brake_Dynamic_time 	= 0x00;
uint8_t g_AEB_ttc_warning_L1 			= 0x00;
uint8_t g_AEB_ttc_warning_L2			= 0x00;
uint8_t g_AEB_CMS_cipv_age    			= 0x00;
//-----Cam info
uint8_t g_is_hmw_warning      			= 0x00;
//-----Main_control share outside
uint8_t g_AEB_CMS_Enable 				= TURN_ON;
uint8_t g_CMS_HMW_Enable 				= TURN_ON;
uint8_t g_CMS_TTC_Enable 				= TURN_ON;
uint8_t g_AEB_TTC_Enable 				= TURN_OFF;
uint8_t g_SSS_Enable 					= TURN_ON;
uint8_t g_HMW_Dynamic_Thr_Enable 		= TURN_ON;
uint8_t g_Chassis_Safe_Strategy_Enable  = TURN_ON;
uint8_t g_LDW_Enable 					= TURN_ON;
//Vehicle Parameter
static struct VehicleParams *g_veh_params = NULL;
//System Brake flag
bool    g_srr_brake = FALSE;
int64_t g_last_driver_brake_time = 0;

/////////////////////////////////////////////////////////
//Log use ChipOn Debug Oscilloscop(just int)
/////////////////////////////////////////////////////////
uint8_t OSC_Cam_d_z;
uint8_t OSC_V_speed;

uint8_t OSC_Appro_to_Lead_Car;
uint8_t OSC_Cam_TTC;
uint8_t OSC_Cam_HMW;

uint8_t OSC_Dec_out;
uint8_t OSC_Dec_out_cms_hmw;
uint8_t OSC_Dec_out_cms_ttc;
uint8_t OSC_Dec_out_aeb_ttc;
uint8_t OSC_Dec_out_uradar;
/////////////////////////////////////////////////////////
// Brake Status Share to 4G upload
////////////////////////////////////////////////////////
uint8_t Brake_state_share = 0;
//////////////////////////////////////////////////////////
// Angle Radar malfunction
/////////////////////////////////////////////////////////
uint8_t srrMalfunction_ = 0;

//SRR_Obstacle_Paras srrOutputParas;
/***********************************************************
 * Private:
 * Just use inside
 * *********************************************************
 */
// Recycle Brake coefficient
static float retract_brake_coe = 2.0;
static uint8_t retract_brake_flag = 0x00;
//Circual Queue
static Circular_Queue Crosswise_Status_queue;
static uint16_t Crosswise_Status_Change_Lanes_Times_Positive;
static uint16_t Crosswise_Status_Change_Lanes_Times_Negative;
static float Crosswise_Status_Change_Lanes_Sum_DisatanceX;

// FCW warning
extern Circular_Queue hmw_warning_cahe;
extern Circular_Queue ttc_warning_cahe;
//LDW warning
extern Circular_Queue LDW_queue;

//Approaching judge
Circular_Queue Approaching_cipv_distance;
//Approaching Keep
Circular_Queue Approaching_keep;
//DriverBraking delay
Circular_Queue DriverBraking_delay;

static CMS_Break_State cms_break_state;
static AEB_Break_State aeb_break_state;
static AEB_CMS_Control_Parameters AEB_CMS_parameters;
static volatile float dec_output = 0.0;
static volatile float ego_speed = 0.0;

static Obstacle_Information  cipv_history;
static Camera_Essential_Data cam_essiential_history;
static Obstacle_Basic_Data obs_basic_data_history;
static uint8_t got_cipv_history = 0;

static uint32_t turn_light_keep_time_stamp 	= 0;
static uint32_t driver_control_gap_time_stamp 	= 0;
static uint32_t aeb_break_cooling_time_stamp    = 0;
static uint32_t aeb_break_keep_time_stamp   	= 0;
static uint32_t cal_ttc_history_time_stamp[4];
float cal_ttc_history_distance_z[4];
static uint8_t cal_ttc_count = 0;
static uint8_t cal_ttc_history_cipv_id = 0;
static uint8_t cal_ttc_history_ID = 0;
static float ttc_r_cal;
static float hmw_r_cal;
static uint8_t warning_name;  //0: 无 1:
static uint8_t warning_function;
static float Uradar_distance;
static float ttc_aeb_dec;
static float ttc_cms_dec;
static float hmw_cms_dec;
static float bsd_aeb_dec;
static float uradar_cms_dec;
static float dynamic_hmw_thr = 0.0;
static float dynamic_ttc_thr = 0.0;


static float history_speed;
static float history_distance_z;
static float history_distance_x;


static uint32_t judge_approaching_uradar_timeatamp[5];
static float judge_approaching_uradar_distance[5];
static uint8_t judge_approaching_uradar_distance_count = 0;


static uint8_t cipv_count = 0;
static uint8_t have_processed = 0;
static uint8_t have_save_to_history = 0;
static uint8_t have_processed_nocipv = 0;

static uint8_t approaching_ornot = 0;
static uint8_t approaching_disppear_count = 0x00;

static uint8_t  Singal_Stop_Sta;
static uint8_t  Singal_TurnLeft_Sta;
static uint8_t  Singal_TurnRight_Sta;
static uint8_t  Singal_Back_Sta;
static uint8_t  AEB_SWITCH_Sta;
static uint16_t wheel_speed;
//

static uint8_t s_Old_AEB_ON_OFF_GPIO        = 0x02;// init_status
static uint8_t s_Old_AEB_ON_OFF_Displayer   = 0x02;// init_status
static uint8_t s_Old_LDW_ON_OFF_Displayer   = 0x02;// init_status
static uint8_t s_Old_AEB_ON_OFF_BT		    = 0x02;// init_status
static uint8_t s_Old_LDW_ON_OFF_BT			= 0x02;// init_status
static uint8_t s_Old_SSS_ON_OFF_BT			= 0x02;// init_status
//
static uint8_t g_old_AEB_CMS_Enable 				= 0x02;
static uint8_t g_old_CMS_HMW_Enable 				= 0x02;
static uint8_t g_old_CMS_TTC_Enable 				= 0x02;
static uint8_t g_old_AEB_TTC_Enable 				= 0x02;
static uint8_t g_old_SSS_Enable 					= 0x02;
static uint8_t g_old_HMW_Dynamic_Thr_Enable 		= 0x02;
static uint8_t g_old_Chassis_Safe_Strategy_Enable   = 0x02;
static uint8_t g_old_LDW_Enable 					= 0x02;
//
static uint16_t pwm_plus_time;
static uint8_t  pwm_Hz;
static uint32_t pwm_time_stamp;
static uint8_t Min_keep_Time_Level = 50;
static uint8_t Min_keep_Time_Level_1 = 50;
static uint8_t Min_keep_Time_Level_2 = 40;
static uint8_t Min_keep_Time_Level_3 = 30;

///////////////////////////////////////////////////////////////////////
//Paramters Confirm:
//if no correct parameters ,then use the default parameters
//////////////////////////////////////////////////////////////////////
static uint8_t ttc_cms_set_hmw_thr  = 0;
static uint8_t ttc_cms_set_hmw_thr_count = 0;

static float hmw_brake_ttc_feel = 6.0;

///////////////////////////////////////////////////////////////////////
//Paramters Flag :
//////////////////////////////////////////////////////////////////////
bool Vechile_TurnRight_Form_GPIO = false;
bool Vechile_TurnLeft_Form_GPIO = false;
bool Vechile_DriverBrake_Form_GPIO = false;
bool Vechile_ReverseGear_Form_GPIO = false;
///////////////////////////////////////////////////////////////////////
//Test Brake:
//////////////////////////////////////////////////////////////////////
static uint32_t test_brake_keep_time_stamp;
uint8_t g_AEBS_outside_Test_Brake_Warning = 0x00;

bool g_b_srr_log_key = false;
bool srr_demo_sw = false;//行人横穿演示demo开关 0：关闭 ，1：打开
///////////////////////////////////////////////////////////////////////
//Global Switch Control
//////////////////////////////////////////////////////////////////////
uint8_t Dynamic_Control_Megnetic_Valve_in_alg(uint8_t swt, uint16_t enableTime, uint8_t Hz)
{
	if(swt){

		if((SystemtimeClock - pwm_time_stamp) < enableTime){
			Set_Magnetic_Valve(1);
		}else{
			if(((SystemtimeClock - pwm_time_stamp) >= enableTime)&&((SystemtimeClock - pwm_time_stamp) < (1000/Hz))){
				Set_Magnetic_Valve(0);
			}
			else{
				pwm_time_stamp = SystemtimeClock;
			}
		}
	}
	else{
		Set_Magnetic_Valve(0);
	}
}

void PWM_Control_Dec_to_pwm(float deceleration){
	if(rParms.PWM_magnetic_valve_Switch && (deceleration > 0.0)){
		if(rParms.PWM_magnetic_valve_N <= 0){
			return;
		}
		float ratio_per_second = deceleration*0.01 * rParms.PWM_magnetic_valve_Scale;
		float total_time_ms = 1000*ratio_per_second;
		uint8_t flag = 0;
		if(total_time_ms >= 1000){
			total_time_ms *= 0.8;
		}
		if(total_time_ms < Min_keep_Time_Level){
			Min_keep_Time_Level = Min_keep_Time_Level_1;
			if(total_time_ms < Min_keep_Time_Level_1){
				Min_keep_Time_Level = Min_keep_Time_Level_2;
				if(total_time_ms < Min_keep_Time_Level_2){
					Min_keep_Time_Level = Min_keep_Time_Level_3;
				}
			}
		}
		if((total_time_ms)> 900){
			total_time_ms = 900;
		}
		for(uint8_t i = rParms.PWM_magnetic_valve_N; (i>=1 && i<=rParms.PWM_magnetic_valve_N); i--){
			if((total_time_ms / i)> Min_keep_Time_Level){

				pwm_Hz = i;
				pwm_plus_time = (uint16_t)(total_time_ms / i);
				if(pwm_plus_time <= (0.95*1000/i)){
					flag = 1;
					//fprintf(USART1_STREAM,"--------------------------\r\n");
					break;
				}

			}

		}
		//fprintf(USART1_STREAM,"Hz=%d T=%d Total=%0.2f  ,dec=%0.2f\r\n",pwm_Hz,pwm_plus_time,total_time_ms,deceleration);
		if(flag){
			Dynamic_Control_Megnetic_Valve_in_alg(1, pwm_plus_time, pwm_Hz);
			//PWM_Magnetic_Valve_set(pwm_plus_time,pwm_Hz,1);
		}
	}
	else{
		Dynamic_Control_Megnetic_Valve_in_alg(0, pwm_plus_time, pwm_Hz);
		//PWM_Magnetic_Valve_set(10,0,0);
	}

}

////////////////////////////////
void Load_switch_status_BT(void){
	if(rParms.switch_g.AEB_CMS_Enable != g_AEB_ON_OFF_BT){
		g_AEB_ON_OFF_BT = rParms.switch_g.AEB_CMS_Enable;
	}
	if(rParms.switch_g.SSS_Enable != g_SSS_ON_OFF_BT){
		g_SSS_ON_OFF_BT = rParms.switch_g.SSS_Enable;
	}
	if(rParms.switch_g.LDW_Enable != g_LDW_ON_OFF_BT){
		g_LDW_ON_OFF_BT = rParms.switch_g.LDW_Enable;
	}
//	if(rParms.switch_g.AEB_CMS_Enable != old_rParms.switch_g.AEB_CMS_Enable){
//		if(rParms.switch_g.AEB_CMS_Enable){
//			g_AEB_ON_OFF_BT = TURN_ON;
//		}else{
//			g_AEB_ON_OFF_BT = TURN_OFF;
//		}
//		old_rParms.switch_g.AEB_CMS_Enable = rParms.switch_g.AEB_CMS_Enable;
//	}
//	if(rParms.switch_g.SSS_Enable != old_rParms.switch_g.SSS_Enable){
//		if(rParms.switch_g.SSS_Enable){
//			g_SSS_ON_OFF_BT = TURN_ON;
//		}else{
//			g_SSS_ON_OFF_BT = TURN_OFF;
//		}
//		old_rParms.switch_g.SSS_Enable = rParms.switch_g.SSS_Enable;
//	}
//	if(rParms.switch_g.LDW_Enable != old_rParms.switch_g.LDW_Enable){
//		if(rParms.switch_g.LDW_Enable){
//			g_LDW_ON_OFF_BT = TURN_ON;
//		}else{
//			g_LDW_ON_OFF_BT = TURN_OFF;
//		}
//		old_rParms.switch_g.LDW_Enable = rParms.switch_g.LDW_Enable;
//	}

}

void Load_switch_status_GPIO(void){
	g_AEB_ON_OFF_GPIO = GPIO_Read_Input_Data_Bit(GPIOH_SFR,GPIO_PIN_MASK_12);//AEB
	//g_LDW_ON_OFF_GPIO = GPIO_Read_Input_Data_Bit(GPIOH_SFR,GPIO_PIN_MASK_13);//LDW
	//fprintf(USART1_STREAM,"@@@@@@@@@@@@@@@@g_AEB_ON_OFF_GPIO = %d\r\n",g_AEB_ON_OFF_GPIO);
}

KunLun_AEB_Para_Cfg rParms_old = {0};  	// read Copy
void Check_System_Param(void){
	Status_System_Param = 0x01;

	if(rParms.Brake_Cooling_Time > 60.0 || rParms.Brake_Cooling_Time < 0.0){
		Status_System_Param = 0x00;
		//fprintf(USART1_STREAM,"1\r\n");
	}
	if(rParms.Driver_Brake_Cooling_Time > 60.0 || rParms.Driver_Brake_Cooling_Time < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"2\r\n");
		}
	if(rParms.Max_Brake_Keep_Time > 30.0 || rParms.Max_Brake_Keep_Time < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"3\r\n");
		}
	if(rParms.Air_Brake_Delay_Time > 1.0 || rParms.Air_Brake_Delay_Time < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"4\r\n");
		}
	if(rParms.Max_Percent_Decelerate > 1.0 || rParms.Max_Percent_Decelerate < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"5\r\n");
		}
	if(rParms.Min_Enable_Speed > 120.0 || rParms.Min_Enable_Speed < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"6\r\n");
		}
	if(rParms.Ratio_Force_To_Deceleration > 10.0 || rParms.Ratio_Force_To_Deceleration < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"7\r\n");
		}
	if(rParms.CMS_HMW_Brake_Time_Thr > 2.0 || rParms.CMS_HMW_Brake_Time_Thr < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"8\r\n");
		}
	if(rParms.CMS_HMW_Brake_Force_Feel_Para > 10.0 || rParms.CMS_HMW_Brake_Force_Feel_Para < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"9\r\n");
		}
	if(rParms.CMS_HMW_Warning_Time_Thr > 3.0 || rParms.CMS_HMW_Warning_Time_Thr < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"10\r\n");
		}
	if(rParms.CMS_HMW_Dynamic_Offset_Speed_L > 60.0 || rParms.CMS_HMW_Dynamic_Offset_Speed_L < 10.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"11\r\n");
		}
	if(rParms.CMS_HMW_Dynamic_Offset_Value_L > 2.0 || rParms.CMS_HMW_Dynamic_Offset_Value_L < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"12\r\n");
		}
	if(rParms.CMS_HMW_Dynamic_Offset_Speed_H > 120.0 || rParms.CMS_HMW_Dynamic_Offset_Speed_H < 50.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"13\r\n");
		}
	if(rParms.CMS_HMW_Dynamic_Offset_Value_H > 2.0 || rParms.CMS_HMW_Dynamic_Offset_Value_H < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"14\r\n");
		}
	if(rParms.CMS_TTC_Brake_Time_Thr > 5.0 || rParms.CMS_TTC_Brake_Time_Thr < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"15\r\n");
		}
	if(rParms.CMS_TTC_Brake_Force_Feel_Para > 10.0 || rParms.CMS_TTC_Brake_Force_Feel_Para < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"16\r\n");
		}
	if(rParms.CMS_TTC_Warning_Time_Level_First > 6.0 || rParms.CMS_TTC_Warning_Time_Level_First < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"17\r\n");
		}
	if(rParms.CMS_TTC_Warning_Time_Level_First > 6.0 || rParms.CMS_TTC_Warning_Time_Level_First < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"18\r\n");
		}
	if(rParms.CMS_TTC_Warning_Time_Level_Second > 6.0 || rParms.CMS_TTC_Warning_Time_Level_Second < 0.0){
		Status_System_Param = 0x00;
	}
	if(rParms.AEB_Decelerate_Set > 10.0 || rParms.AEB_Decelerate_Set < 0.0){
		Status_System_Param = 0x00;
		//fprintf(USART1_STREAM,"19\r\n");
	}
	if(rParms.AEB_Stop_Distance > 10.0 || rParms.AEB_Stop_Distance < 0.0){
		Status_System_Param = 0x00;
		//fprintf(USART1_STREAM,"1\r\n");
	}
	if(rParms.AEB_TTC_Warning_Time_Level_First > 5.0 || rParms.AEB_TTC_Warning_Time_Level_First < 0.0){
		Status_System_Param = 0x00;
		//fprintf(USART1_STREAM,"20\r\n");
	}
	if(rParms.AEB_TTC_Warning_Time_Level_Second > 5.0 || rParms.AEB_TTC_Warning_Time_Level_Second < 0.0){
		Status_System_Param = 0x00;
		//fprintf(USART1_STREAM,"21\r\n");
	}
	if(rParms.SSS_Brake_Force > 10.0 || rParms.SSS_Brake_Force < 0.0){
		Status_System_Param = 0x00;
		//fprintf(USART1_STREAM,"22\r\n");
	}
	if(rParms.SSS_Break_Enable_Distance > 2.5 || rParms.SSS_Break_Enable_Distance < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"23\r\n");
		}
	if(rParms.SSS_Warning_Enable_Distance > 5.0 || rParms.SSS_Warning_Enable_Distance < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"24\r\n");
		}
	if(rParms.SSS_Max_Enable_Speed > 40.0 || rParms.SSS_Max_Enable_Speed < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"25\r\n");
		}
	if(rParms.SSS_FR_FL_Install_Distance_To_Side > 1.0 || rParms.SSS_FR_FL_Install_Distance_To_Side < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"1\r\n");
		}
	if(rParms.SSS_Stop_Distance > 1.0 || rParms.SSS_Stop_Distance < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"26\r\n");
		}
	if(rParms.SSS_Default_Turn_Angle > 60.0 || rParms.SSS_Default_Turn_Angle < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"27\r\n");
		}
	if(rParms.WheelSpeed_Coefficient > 10000.0 || rParms.WheelSpeed_Coefficient < 0.0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"28\r\n");
		}
	if(rParms.PWM_magnetic_valve_T > 70 || rParms.PWM_magnetic_valve_T < 0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"29\r\n");
		}
	if(rParms.PWM_magnetic_valve_N > 5 || rParms.PWM_magnetic_valve_N < 0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"30\r\n");
		}
	if(rParms.PWM_magnetic_valve_Switch > 1 || rParms.Brake_Cooling_Time < 0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"31\r\n");
		}
	if(rParms.PWM_magnetic_valve_Scale > 100 || rParms.PWM_magnetic_valve_Scale < 0){
			Status_System_Param = 0x00;
			//fprintf(USART1_STREAM,"32\r\n");
		}


	if(server_info.vehicle_type == 0x00 || server_info.vehicle_type > 0x65){// not set param

		//if(Status_System_Param == 0x01)
		//	server_info.vehicle_type = 1;
		//fprintf(USART1_STREAM,"33\r\n");
	}

}

uint32_t sound_time_stamp_3_second = 0;
uint8_t old_system_param_check_result = 0;
uint8_t voice_prompt_count = 0;
void AEB_System_Switch(void){
	// AEB: ON/OFF
	//  priority level : Status_System_Param > on_off_GPIO > on_off_dispalyer > on_off_BT
	//20220928 yuhong cancel the GPIO to close the AEB on/off
	//Load_switch_status_GPIO();

	Load_switch_status_BT();

	if(g_AEB_ON_OFF_GPIO == TURN_OFF){
		if(s_Old_AEB_ON_OFF_GPIO == 0x02){
			g_AEB_ON_OFF_GPIO = TURN_ON;
		}
		else if(s_Old_AEB_ON_OFF_GPIO == TURN_ON){
			s_Old_AEB_ON_OFF_GPIO = TURN_OFF;
			g_AEB_CMS_Enable = TURN_OFF;
			g_SSS_Enable	 = TURN_OFF;
			//gcz 2022-05-08
			set_AEB_key_source(AEB_SOURCE_GPIO);
			// send voice to displayer
			Send_Display_Message_VoiveNum(SOUND_OF_AEB_SYSTEM_TURN_OFF);// turn off
		}
		//fprintf(USART1_STREAM,"SSS_driver_control_GPIO_OFF\r\n");
	}else if(g_AEB_ON_OFF_GPIO == TURN_ON){
		//fprintf(USART1_STREAM,"SSS_driver_control_GPIO_ON\r\n");
		if(s_Old_AEB_ON_OFF_GPIO == 0x02){
			// init = off, but it should be turn on at first time
			s_Old_AEB_ON_OFF_GPIO = TURN_ON;
		}else if(s_Old_AEB_ON_OFF_GPIO == TURN_OFF){
			g_AEB_ON_OFF_GPIO = TURN_OFF;
		}
	}
	//
	if(g_AEB_ON_OFF_GPIO == TURN_ON){
		//
		if(s_Old_SSS_ON_OFF_BT != g_SSS_ON_OFF_BT)
		{
			if(s_Old_SSS_ON_OFF_BT != 0x02)
			{
				g_old_SSS_Enable	 = g_SSS_Enable;
				g_SSS_Enable	 = g_SSS_ON_OFF_BT;
				//fprintf(USART1_STREAM,"g_AEB_ON_OFF_BT\r\n");	// 勿删除
				// send voice to displayer
				if(g_SSS_Enable == TURN_OFF)
				{
					Send_Display_Message_VoiveNum(SOUND_OF_AEB_SYSTEM_TURN_OFF);// turn off
				}
				else{
					Send_Display_Message_VoiveNum(SOUND_OF_AEB_SYSTEM_TURN_ON);// turn on
				}
			}
			s_Old_SSS_ON_OFF_BT = g_SSS_ON_OFF_BT;
		}
		if(s_Old_AEB_ON_OFF_BT != g_AEB_ON_OFF_BT)
		{
			if(s_Old_AEB_ON_OFF_BT != 0x02)
			{
				s_Old_AEB_ON_OFF_BT = g_AEB_ON_OFF_BT;
				g_old_AEB_CMS_Enable = g_AEB_CMS_Enable;
				g_old_SSS_Enable	 = g_SSS_Enable;
				g_AEB_CMS_Enable = g_AEB_ON_OFF_BT;
				g_SSS_Enable	 = g_AEB_ON_OFF_BT;
				//gcz 2022-05-08
				if (g_st_uart_opt.usart_used_id == UART_1_ID)
					set_AEB_key_source(AEB_SOURCE_PC_AT);
				else if (g_st_uart_opt.usart_used_id == UART_4_ID)
					set_AEB_key_source(AEB_SOURCE_BT);
				// send voice to displayer
				if(g_AEB_CMS_Enable == TURN_OFF)
				{
					Send_Display_Message_VoiveNum(SOUND_OF_AEB_SYSTEM_TURN_OFF);// turn off
				}
				else{
					Send_Display_Message_VoiveNum(SOUND_OF_AEB_SYSTEM_TURN_ON);// turn on
				}
			}
			s_Old_AEB_ON_OFF_BT = g_AEB_ON_OFF_BT;
		}

		if(s_Old_AEB_ON_OFF_Displayer != g_AEB_ON_OFF_Displayer){
		 	//gcz 2022-05-08
			if (s_Old_AEB_ON_OFF_Displayer != 0x02)
			{
				if((rParms.switch_g.Displayer_Switch_Enable == 0x01 || rParms.switch_g.Displayer_Switch_Enable == 0x02)){
					s_Old_AEB_ON_OFF_Displayer = g_AEB_ON_OFF_Displayer;
					g_old_AEB_CMS_Enable = g_AEB_CMS_Enable;
					g_old_SSS_Enable	 = g_SSS_Enable;
					g_AEB_CMS_Enable = g_AEB_ON_OFF_Displayer;
					g_SSS_Enable	 = g_AEB_ON_OFF_Displayer;
					set_AEB_key_source(AEB_SOURCE_DISPLAYER);
					if(g_AEB_CMS_Enable == TURN_OFF)
					{
						Send_Display_Message_VoiveNum(SOUND_OF_AEB_SYSTEM_TURN_OFF);// turn off
					}
					else{
						Send_Display_Message_VoiveNum(SOUND_OF_AEB_SYSTEM_TURN_ON);// turn on
					}
				}else{
					s_Old_AEB_ON_OFF_Displayer = g_AEB_ON_OFF_Displayer;
					if(g_AEB_CMS_Enable == TURN_OFF)
					{
						Send_Display_Message_VoiveNum(SOUND_OF_AEB_SYSTEM_TURN_OFF);// turn off
					}
					else{
						Send_Display_Message_VoiveNum(SOUND_OF_AEB_SYSTEM_TURN_ON);// turn on
					}
				}
			}
			s_Old_AEB_ON_OFF_Displayer = g_AEB_ON_OFF_Displayer;
		}


		/////////
		//---Status_System_Param
		/////////
		old_system_param_check_result = Status_System_Param;
		Check_System_Param();
		if(Status_System_Param == 0x00){
			// systemParam:Second  priorit
			g_old_AEB_CMS_Enable = g_AEB_CMS_Enable;
			g_old_SSS_Enable	 = g_SSS_Enable;
			g_AEB_CMS_Enable = TURN_OFF;
			g_SSS_Enable	 = TURN_OFF;

			if(((SystemtimeClock - sound_time_stamp_3_second) / 1000) > 3 && voice_prompt_count < 20){
				voice_prompt_count ++;
//				Send_Display_Message_VoiveNum(45);// set param failed
				sound_time_stamp_3_second = SystemtimeClock;
				//fprintf(USART1_STREAM,"Status_System_Param = %d\r\n",Status_System_Param);
				//fprintf(USART1_STREAM,"Status_System_Param = %d  id=%d\r\n",Status_System_Param,rParms.id);
			}
		}
		else if(old_system_param_check_result != Status_System_Param){
			g_old_AEB_CMS_Enable = g_AEB_CMS_Enable;
			g_old_SSS_Enable	 = g_SSS_Enable;
			g_AEB_CMS_Enable = TURN_ON;
			g_SSS_Enable	 = TURN_ON;
			Send_Display_Message_VoiveNum(44);// set param sucess
			voice_prompt_count = 0;
		}
	}
	else{
		// GPIO:top priorit
		g_old_AEB_CMS_Enable = g_AEB_CMS_Enable;
		g_old_SSS_Enable	 = g_SSS_Enable;
		g_AEB_CMS_Enable = TURN_OFF;
		g_SSS_Enable	 = TURN_OFF;
	}

	// sync to BT and Displayer
	// BT sync
	if(g_AEB_ON_OFF_BT != g_AEB_CMS_Enable){
		if(g_AEB_CMS_Enable == TURN_ON){
			s_Old_AEB_ON_OFF_BT = TURN_ON;
			g_AEB_ON_OFF_BT = s_Old_AEB_ON_OFF_BT;
//			rParms.switch_g.AEB_CMS_Enable = 1;
		}
		else{
			s_Old_AEB_ON_OFF_BT = TURN_OFF;
			g_AEB_ON_OFF_BT = s_Old_AEB_ON_OFF_BT;
			rParms.switch_g.AEB_CMS_Enable = 0;
		}
		Write_AEB_Config_Param(rParms);
	}
	// Displayer sync
	// do it in Displayer_Pull();
}


void LDW_Switch(void){
	// LDW: ON/OFF
	Load_switch_status_BT();
	if(s_Old_LDW_ON_OFF_BT != g_LDW_ON_OFF_BT){
		if(s_Old_LDW_ON_OFF_BT != 0x02){
			//s_Old_LDW_ON_OFF_BT = g_LDW_ON_OFF_BT; // sync
			g_old_LDW_Enable = g_LDW_Enable;
			g_LDW_Enable = g_LDW_ON_OFF_BT;
			//gcz 2022-05-08
			if (g_st_uart_opt.usart_used_id == UART_1_ID)
				set_LDW_key_source(AEB_SOURCE_PC_AT);
			else if (g_st_uart_opt.usart_used_id == UART_4_ID)
				set_LDW_key_source(AEB_SOURCE_BT);
			if(g_LDW_Enable == TURN_OFF){
				Send_Display_Message_VoiveNum(SOUND_OF_LDW_WARNING_TURN_OFF);// turn off
			}
			else{
				Send_Display_Message_VoiveNum(SOUND_OF_LDW_WARNING_TURN_ON);// turn on
			}
		}
		s_Old_LDW_ON_OFF_BT = g_LDW_ON_OFF_BT; // sync
	}
	if(s_Old_LDW_ON_OFF_Displayer != g_LDW_ON_OFF_Displayer){
		//gcz 2022-05-08
		if (s_Old_LDW_ON_OFF_Displayer != 0x02)
		{
			if((rParms.switch_g.Displayer_Switch_Enable == 0x01 || rParms.switch_g.Displayer_Switch_Enable == 0x03)){
				s_Old_LDW_ON_OFF_Displayer = g_LDW_ON_OFF_Displayer; // sync
				g_old_LDW_Enable = g_LDW_Enable;
				g_LDW_Enable = g_LDW_ON_OFF_Displayer;
				set_LDW_key_source(AEB_SOURCE_DISPLAYER);
		//		fprintf(USART1_STREAM,"g_LDW_Enable = %d\r\n",g_LDW_Enable);
				if(g_LDW_Enable == TURN_OFF){
					Send_Display_Message_VoiveNum(SOUND_OF_LDW_WARNING_TURN_OFF);// turn off
				}
				else{
					Send_Display_Message_VoiveNum(SOUND_OF_LDW_WARNING_TURN_ON);// turn on
				}
			}else{
				s_Old_LDW_ON_OFF_Displayer = g_LDW_ON_OFF_Displayer;
				if(g_LDW_Enable == TURN_OFF){
					Send_Display_Message_VoiveNum(SOUND_OF_LDW_WARNING_TURN_OFF);// turn off
				}
				else{
					Send_Display_Message_VoiveNum(SOUND_OF_LDW_WARNING_TURN_ON);// turn on
				}
			}
		}
		s_Old_LDW_ON_OFF_Displayer = g_LDW_ON_OFF_Displayer;
	}
	// sync to BT and Displayer
	// BT sync
	if(g_LDW_ON_OFF_BT != g_LDW_Enable){
		if(g_LDW_Enable == TURN_ON){
			s_Old_LDW_ON_OFF_BT = TURN_ON;
			g_LDW_ON_OFF_BT = s_Old_LDW_ON_OFF_BT;
			rParms.switch_g.LDW_Enable = 1;
		}
		else{
			s_Old_LDW_ON_OFF_BT = TURN_OFF;
			g_LDW_ON_OFF_BT = s_Old_LDW_ON_OFF_BT;
			rParms.switch_g.LDW_Enable = 0;
		}
		if(Write_AEB_Config_Param(rParms)){
			//Send_Display_Message_VoiveNum(PARAM_CONFIG_OK_S);	// 语音：系统参数配置成功
		}
	}
	//rParms.Global_Switch |= 0x01;
	//wParms.Global_Switch |= 0x01;
	// Displayer sync
		//// do it in Displayer_Pull();
}

void Dynamic_HMW_Switch(void){
	if(rParms.switch_g.HMW_Dynamic_Thr_Enable){
		g_HMW_Dynamic_Thr_Enable = TURN_ON;
	}else{
		g_HMW_Dynamic_Thr_Enable = TURN_OFF;
	}
}
void Dynamic_hmw_ttc_thr(float speed, float *dynamic_hmw){
	// cal dynamic HMWT/TTC brake thr
	if((rParms.CMS_HMW_Dynamic_Offset_Speed_L < rParms.Min_Enable_Speed)||(rParms.CMS_HMW_Dynamic_Offset_Speed_L > 50.0)||(rParms.CMS_HMW_Dynamic_Offset_Speed_H > 90.0)||(rParms.CMS_HMW_Dynamic_Offset_Speed_H > AEB_CMS_parameters.max_enable_speed)){
		(*dynamic_hmw) = rParms.CMS_HMW_Brake_Time_Thr;
		return ;
	}
	if((speed > 0)&(speed < rParms.CMS_HMW_Dynamic_Offset_Speed_L)){
		(*dynamic_hmw) = rParms.CMS_HMW_Dynamic_Offset_Value_L * (rParms.CMS_HMW_Dynamic_Offset_Speed_L - speed)/ rParms.CMS_HMW_Dynamic_Offset_Speed_L;
		(*dynamic_hmw) += rParms.CMS_HMW_Brake_Time_Thr;
		if((*dynamic_hmw) > 1.5){
			(*dynamic_hmw) = 1.5;
		}
	}
	else if((speed > rParms.CMS_HMW_Dynamic_Offset_Speed_H )){
		(*dynamic_hmw) = rParms.CMS_HMW_Dynamic_Offset_Value_H * (speed - rParms.CMS_HMW_Dynamic_Offset_Speed_H )/ (AEB_CMS_parameters.max_Dynamic_HMW_speed - rParms.CMS_HMW_Dynamic_Offset_Speed_H );
		//
		(*dynamic_hmw) = rParms.CMS_HMW_Brake_Time_Thr - (*dynamic_hmw);
		if((*dynamic_hmw) < 0.6){
			(*dynamic_hmw) = 0.6;
		}
	}
	// ttc: next time
}
float Dynamic_hmw_thr_2D(float speed_ego, float d_speed, float target_dec, float base_hmw_thr){

	if(d_speed > (speed_ego*0.5)){
		d_speed = (speed_ego*0.5);
	}
	if(d_speed > 5.555){
		d_speed = 5.555;//m/s
	}
	if(d_speed <=0){
		return base_hmw_thr;
	}
	float a_average = d_speed*0.1 + target_dec;
	//float a_average = d_speed*0.1 + target_dec;
	return (0.5*d_speed*d_speed/(speed_ego*a_average) + base_hmw_thr + 0.5/speed_ego);
}


void Update_OSC_Data(void){
	// use for debug in ChipON and show the
	OSC_Cam_d_z = (uint8_t)cipv_history.DistanceZ;
	OSC_Cam_TTC = (uint8_t)cipv_history.TTC;
	OSC_Cam_HMW = (uint8_t)cipv_history.HMW;
	OSC_V_speed = (uint8_t)stVehicleParas.fVehicleSpeed;
	OSC_Appro_to_Lead_Car = approaching_ornot;

	OSC_Dec_out = (uint8_t)dec_output;
	OSC_Dec_out_cms_hmw = (uint8_t)hmw_cms_dec;
	OSC_Dec_out_cms_ttc = (uint8_t)ttc_cms_dec;
	OSC_Dec_out_aeb_ttc = (uint8_t)ttc_aeb_dec;
	OSC_Dec_out_uradar = (uint8_t)uradar_cms_dec;
}

//gcz 2022-05-08
int g_iShow_RT_speed[2] = {FALSE,FALSE};
bool g_b_pwm_sp_show_flag = false;
extern float g_f_srr_fVehicleSpeed;
void Got_Vehicle_Para_form_GPIO(_VEHICLE_PARA* stVehicleParas){
	AEB_SWITCH_Sta = GPIO_Read_Input_Data_Bit(GPIOH_SFR,GPIO_PIN_MASK_12);//AEB
	if(rParms.Vehicle_State_Obtain_Method == 1){
		Singal_Stop_Sta					=GPIO_Read_Input_Data_Bit(GPIOG_SFR,GPIO_PIN_MASK_6);//SingalStop
		Singal_TurnLeft_Sta 			= GPIO_Read_Input_Data_Bit(GPIOG_SFR,GPIO_PIN_MASK_7);//Singal_TurnLeft
		Singal_TurnRight_Sta 			= GPIO_Read_Input_Data_Bit(GPIOH_SFR,GPIO_PIN_MASK_5);//Singal_TurnRight
		Singal_Back_Sta 				= GPIO_Read_Input_Data_Bit(GPIOH_SFR,GPIO_PIN_MASK_6);//BACK
		stVehicleParas->BrakeFlag 		= Singal_Stop_Sta;
		stVehicleParas->ReverseGear 	= Singal_Back_Sta;
		stVehicleParas->LeftFlagTemp 	= Singal_TurnLeft_Sta;
		stVehicleParas->RightFlagTemp 	= Singal_TurnRight_Sta;
		//fprintf(USART1_STREAM,"LeftFlagTemp=%d,RightFlagTemp=%d,Singal_TurnLeft_Sta=%d,Singal_TurnRight_Sta=%d\r\n",stVehicleParas->LeftFlagTemp,stVehicleParas->RightFlagTemp,Singal_TurnLeft_Sta,Singal_TurnRight_Sta);
		//fprintf(USART1_STREAM,"#############AEB_SWITCH_Sta=%d\r\n",AEB_SWITCH_Sta);

	}

	if(rParms.Vehicle_Speed_Obtain_Method == 1){
		Calc_Bus_Encoder_Velocity();// 轮速
		wheel_speed 					= Get_Wheel_Speed();
	//	stVehicleParas->fVehicleSpeed 	= (float)(wheel_speed) /10.0;
		stVehicleParas->fVehicleSpeed 	= SpeedApplicationLayerFilter(wheel_speed,10) /10.0;
		g_f_srr_fVehicleSpeed  = stVehicleParas->fVehicleSpeed;
		if (g_b_pwm_sp_show_flag)
		{
		  static uint32_t currentTime   = 0;
		  if ((SystemtimeClock - currentTime) >= 100)
		  {
		    currentTime = SystemtimeClock;
		    USART1_Bebug_Print_Flt("PWM_SP:",stVehicleParas->fVehicleSpeed,1,1);
		  }
		}
	//	fprintf(USART1_STREAM,"111---stVehicleParas->fVehicleSpeed =%.2f\r\n",stVehicleParas->fVehicleSpeed);
		//stVehicleParas->fVehicleSpeed = 10.0;
		//------------------------gcz 2022-05-08--------------------------
		if (g_st_detection_cal_sp_para.flag == 1)
		{
			//y由于uint32_t interval = 1; 并且if(SystemtimeClock - previousMillis >= interval)，因此本函数理论上期望1ms执行一次
			//我们目的是500ms输出一次
			static uint32_t currentTime		= 0;
			if (g_st_detection_cal_sp_para.cnt == 0)
				currentTime = SystemtimeClock;

			if ((SystemtimeClock - currentTime) < 500)
			{
				g_st_detection_cal_sp_para.cnt++;
			}
			else
			{
				g_st_detection_cal_sp_para.cnt = 0;
				if (g_u8_instlltion_detection_cmd_src == CMD_SRC_DEBUG)
				{
					g_iShow_RT_speed[0] = TRUE;
					g_u8_instlltion_detection_cmd_src = CMD_END;
				}
				else if (g_u8_instlltion_detection_cmd_src == CMD_SRC_BT)
				{
					g_iShow_RT_speed[1] = TRUE;
					g_u8_instlltion_detection_cmd_src = CMD_END;
				}

				if (g_iShow_RT_speed[0] == TRUE)
					fprintf(USART1_STREAM,"RT_SPEED:%.1f\r\n",stVehicleParas->fVehicleSpeed);
				if (g_iShow_RT_speed[1] == TRUE)
					fprintf(USART4_STREAM,"RT_SPEED:%.1f\r\n",stVehicleParas->fVehicleSpeed);
			}
		}
		//-----------------------------------------------------
	}
	if(LOG_DEBUG == LOGLEVEL){
		if(Singal_Stop_Sta == 1){
			fprintf(USART1_STREAM,"Singal_Stop:%d\r\n",Singal_Stop_Sta);
		}
		if(Singal_TurnLeft_Sta == 1){
			fprintf(USART1_STREAM,"Singal_TurnLeft_Sta:%d\r\n",Singal_TurnLeft_Sta);
		}
		if(Singal_TurnRight_Sta == 1){
			fprintf(USART1_STREAM,"Singal_TurnRight_Sta:%d\r\n",Singal_TurnRight_Sta);
		}
		if(Singal_Back_Sta == 1){
			fprintf(USART1_STREAM,"Singal_Back_Sta:%d\r\n",Singal_Back_Sta);
		}
	}

}
uint32_t jump_signal_keep_timestamp_left 	= 0;
uint32_t jump_signal_keep_timestamp_right  	= 0;
void Vehicle_Turn_Signal_Keep(_VEHICLE_PARA* stVehicleParas){
	if(stVehicleParas->LeftFlagTemp == 0x01){
		jump_signal_keep_timestamp_left = SystemtimeClock;
		stVehicleParas->LeftFlag = 0x01;
	}else{
		if((SystemtimeClock - jump_signal_keep_timestamp_left)/1000 < 1.0){
			//stVehicleParas->LeftFlagTemp = 0x01;
			stVehicleParas->LeftFlag = 0x01;
		}else{
			stVehicleParas->LeftFlag = stVehicleParas->LeftFlagTemp;
		}
	}
	if(stVehicleParas->RightFlagTemp == 0x01){
		jump_signal_keep_timestamp_right = SystemtimeClock;
		stVehicleParas->RightFlag = 0x01;
	}else{
		if((SystemtimeClock - jump_signal_keep_timestamp_right)/1000 < 1.0){
			//stVehicleParas->RightFlagTemp = 0x01;
			stVehicleParas->RightFlag = 0x01;
		}else{
			stVehicleParas->RightFlag = stVehicleParas->RightFlagTemp;
		}
	}
	//fprintf(USART1_STREAM,"LeftFlagTemp=%d,RightFlagTemp=%d,Singal_TurnLeft_Sta=%d,Singal_TurnRight_Sta=%d\r\n",stVehicleParas->LeftFlagTemp,stVehicleParas->RightFlagTemp,Singal_TurnLeft_Sta,Singal_TurnRight_Sta);

}
void Chassis_Safe_Strategy_Enable_Switch(void){
	if(rParms.switch_g.Chassis_Safe_Strategy_Enable){
		g_Chassis_Safe_Strategy_Enable = TURN_ON;
	}else{
		g_Chassis_Safe_Strategy_Enable = TURN_OFF;
	}
}
uint8_t AEB_System_Safe_Tactics(void){
	Chassis_Safe_Strategy_Enable_Switch();
	// keep enough time and then make sure that the system have finish an effective break
	uint8_t gono = 0x01;
	if((g_Chassis_Safe_Strategy_Enable == TURN_ON)&&(((SystemtimeClock - aeb_break_cooling_time_stamp)/1000.0) < rParms.Brake_Cooling_Time)&&(((SystemtimeClock - aeb_break_cooling_time_stamp)/1000.0) >= MIN_BREAK_COOLING_TIME)){
		gono = 0x00;
	}
	if((g_Chassis_Safe_Strategy_Enable == TURN_ON)&&(((SystemtimeClock - aeb_break_keep_time_stamp)/1000.0) > rParms.Max_Brake_Keep_Time)){
		gono = 0x00;
	}
	if(dec_output > 0.000001){
		aeb_break_cooling_time_stamp = SystemtimeClock;
	}
	if(dec_output < 0.000001){
		aeb_break_keep_time_stamp = SystemtimeClock;
	}
	if(gono == 0x00)
		return break_calculate_cancel;
	else
		return break_calculate_continue;
}

// crosswise status judge
static float average_speed_x = 0.0;
static float leave_distance = 0.0;
static float leave_time = 0.0;
static float leave_distance_persent = 1.0;
uint8_t Lead_car_crosswise_status_monitor(Obstacle_Information cipv, float vehicle_width, uint8_t judge_times, float min_distance_change){
	// judge the lead car change lanes or ours car turning around the lead car
	// return 0: None
	// return 1: status 1;
	// return 2: status 2;
	// return 3: status 3;
	// return 4: status 3 and status 1;
	// return 5: status 3 and status 2;
	// status 1.normal: lead car on the axle wire, that is the target car's
	//
	//  ego car axle wire
	//         ^
	//      ___|___
	//     |   |   |
	//     |leadcar|
	//     |   |   |
	//     |___|___|
	//         |
	//         |
	//

	//
	// status 2.biased: that is the lead car not on the center of the axle wire
	//         ^
	//  _____  |
	// |     | |
	// |     | |
	// |     | |
	// |     | |
	// |_____| |
	//         |
	//         |
    // or
	//         ^
	//         |
	//         |  _____
	//         | |     |
	//         | |     |
	//         | |     |
	//         | |     |
	//         | |_____|
	//         |
	//
	//
	//

	// status 3.the lead car change lanes or ego car turn around:【no need brake】
	//
	//
	//         ^                  ^
	// 	______ |                  | ______
	//  \     \|                  |/     /
	//   \     \                  /     /
	//    \    |\                /|    /
	//     \___|_\              /_|___/
	//   <---— |                  | ---->
	//         |                  |
	//
	// OR
	//
	//
	//       ||  	  	    ||                       ||  	  	    ||
	//       ||   ______    ||                       ||   ______    ||
	//       ||  |	  	|   ||                       ||  |	  	|   ||
	//       ||  |	 	|   ||                       ||  |	    |   ||
	//       ||  |	 	|   ||                       ||  | 	    |   ||
	//       ||  | 		|   ||                       ||  |      |	||
	//       ||  |______|   ||                       ||  |______|   ||
	//       ||     	    ||                       ||  	  	    ||
	//       ||   \ 	    ||                       ||  	  	/   ||
	//       || ___\___     ||                       ||     ___/___ ||
	//       || \<--\-  \   ||                       ||    / -/-->/ ||
	//       ||  \   \   \  ||                       ||   /  /	 /  ||
    //		 ||   \   \   \ ||                       ||  /	/  	/   ||
    //		 ||    \___\ __\||                       || /__/___/    ||
    //		 ||   		\   ||                       ||   / 	    ||
    //		 ||    		 \  ||                       ||  /	  	    ||
    //		 ||    		    ||                       ||  	  	    ||
    //		 ||    		    ||                       ||  	  	    ||

	// Step 1: data-caching
	if(Crosswise_Status_queue.is_empty == 1){
		Crosswise_Status_queue.cipv_ID = cipv.ObstacleID;
		In_Queue(&Crosswise_Status_queue, cipv.DistanceX);
	}else{
		if(Crosswise_Status_queue.cipv_ID == cipv.ObstacleID){
			In_Queue(&Crosswise_Status_queue, cipv.DistanceX);
		}
		else{
			Clean_Queue(&Crosswise_Status_queue);
		}
	}

	if(Crosswise_Status_queue.is_full == 0){
		Crosswise_Status_Change_Lanes_Times_Positive = 0;
		Crosswise_Status_Change_Lanes_Times_Negative = 0;
		return 0;// can not judge the status
	}
	// Step 2: Status judge
	uint8_t status;
	// status 3:change lanes or turn around: return 3
	if(Crosswise_Status_queue.is_full == 1){
		float distance_chage = Get_data(&Crosswise_Status_queue, Crosswise_Status_queue.right_now_head) - Get_data(&Crosswise_Status_queue, Crosswise_Status_queue.next_head_index);
		//fprintf(USART1_STREAM,"head_index=%d,value=%0.2f,tail_index=%d,value=%0.2f,distance_chage = %0.2f\r\n",Crosswise_Status_queue.right_now_head,Get_data(&Crosswise_Status_queue, Crosswise_Status_queue.right_now_head),Crosswise_Status_queue.next_head_index,Get_data(&Crosswise_Status_queue, Crosswise_Status_queue.next_head_index),distance_chage);
		if(distance_chage >= min_distance_change){
			Crosswise_Status_Change_Lanes_Times_Negative = 0;
			Crosswise_Status_Change_Lanes_Times_Positive += 1;
			Crosswise_Status_Change_Lanes_Sum_DisatanceX += distance_chage;
			//fprintf(USART1_STREAM,"count+=%d, sumdistance=%0.2f\r\n",Crosswise_Status_Change_Lanes_Times_Positive,Crosswise_Status_Change_Lanes_Sum_DisatanceX);
		}else if(((-distance_chage) >= min_distance_change)){
			Crosswise_Status_Change_Lanes_Times_Positive = 0;
			Crosswise_Status_Change_Lanes_Times_Negative += 1;
			Crosswise_Status_Change_Lanes_Sum_DisatanceX += distance_chage;
			//fprintf(USART1_STREAM,"count-=%d, sumdistance=%0.2f\r\n",Crosswise_Status_Change_Lanes_Times_Negative,Crosswise_Status_Change_Lanes_Sum_DisatanceX);
		}
		else{
			Crosswise_Status_Change_Lanes_Times_Positive = 0;
			Crosswise_Status_Change_Lanes_Times_Negative = 0;
			Crosswise_Status_Change_Lanes_Sum_DisatanceX = 0.0;
		}

		if(Crosswise_Status_Change_Lanes_Times_Positive >= judge_times || Crosswise_Status_Change_Lanes_Times_Negative >= judge_times){
			//return 3;
			// judge if will be collision
			if(Crosswise_Status_Change_Lanes_Times_Positive > 0){
				average_speed_x = Crosswise_Status_Change_Lanes_Sum_DisatanceX / (Crosswise_Status_Change_Lanes_Times_Positive*0.1);//0.1s=100ms=10Hz
			}
			else if(Crosswise_Status_Change_Lanes_Times_Negative > 0){
				average_speed_x = Crosswise_Status_Change_Lanes_Sum_DisatanceX / (Crosswise_Status_Change_Lanes_Times_Negative*0.1);
			}
			else{
				average_speed_x = 0.0;
			}

			if(average_speed_x > 0.01 || average_speed_x < -0.01){
				if(cipv.DistanceX > 0){
					leave_distance = (cipv.ObstacleWidth * 0.5 - cipv.DistanceX) + vehicle_width*0.5;
				}else if(cipv.DistanceX <= 0){
					leave_distance = (cipv.ObstacleWidth * 0.5 + cipv.DistanceX) + vehicle_width*0.5;
				}

				if(leave_distance >= 0){
					if(average_speed_x > 0)
						leave_time = leave_distance / average_speed_x;
					else
						leave_time = leave_distance / (-average_speed_x);

					if( (leave_time) < (cipv.TTC * 0.8)){//2022-02-17:brake sensitivity = 0.8 Or 1.25 :(1/0.8=1.25)
						status = 3;
					}else{
						status = 0;
					}
					//fprintf(USART1_STREAM,"cipv.DistanceX=%0.2f,leave_distance = %0.2f，average_speed_x = %0.2f，disprea_time = %0.2f\r\n",cipv.DistanceX,leave_distance,average_speed_x,leave_time);
					if(vehicle_width > 1.0){
						leave_distance_persent = leave_distance/(vehicle_width*0.5);
						if(leave_distance_persent < 1.0){
							if(cipv.HMW > (rParms.CMS_HMW_Brake_Time_Thr * 0.5 *(leave_distance_persent + 1.0))){
								status = 3;
							}
						}
						//fprintf(USART1_STREAM,"cipv.HMW=%0.2f,leave_hmw=%0.2f\r\n",cipv.HMW,rParms.CMS_HMW_Brake_Time_Thr * 0.4 + rParms.CMS_HMW_Brake_Time_Thr * 0.6*leave_distance_persent);
					}
					else{
						status = 3;
					}
				}
				else if(leave_distance < 0){
					status = 3;
				}
				///if(((vehicle_width*0.5 - (cipv.DistanceX - cipv.ObstacleWidth * 0.5))/average_speed_x) > (rParms.CMS_HMW_Brake_Time_Thr)){//wrong: need calculate hmw(t)
				///	// no need brake
				///	status = 3;
				///}

			}
			else{
				status = 0;
			}
		}
		else{
			status = 0;
		}
	}
	else{
		status = 0;
	}

	// status 1.normal:return 1
	float normal_or_not = (cipv.DistanceX + cipv.ObstacleWidth * 0.5 ) * ( + cipv.DistanceX - cipv.ObstacleWidth * 0.5);
	if(normal_or_not < 0){
		return (status + 1);
	}
	// status 2.bias
	if(normal_or_not >= 0){
		return (status + 2);
	}
}

static float target_cms_dec = 2.0;
bool hmw_come_in;
TimeType time_beijing;
uint8_t is_night_flag;
int32_t time_stamp_CIPV_TimeID = 0;			//add xuejie 2022-05-26
static uint8_t fcw_warning = 0x00;
float AEB_CMS_Break_control(uint8_t got_cipv,
							Obstacle_Basic_Data *obs_basic_data,
							Obstacle_Information *cipv,
							_VEHICLE_PARA* stVehicleParas,
							Camera_Essential_Data* cam_essiential,
							URADER_MESSAGE	Urader_Company_Message[]){
	///////////////
	//Switch Check
	///////////////

	AEB_System_Switch();
	LDW_Switch();
  //fprintf(USART1_STREAM,"test the BSD\r\n");

	///////////////////////
	//Switch Status judge
	///////////////////////

	if(0x00 == g_AEB_CMS_Enable
		||(stVehicleParas->LeftFlag ==1)&&(stVehicleParas->RightFlag == 1))//yuhong 20220819 双闪取消超声波制动
	{
		dec_output = 0.0;
		g_AEB_CMS_outside_dec_output = dec_output;
		AEB_State_reset();
		CMS_State_reset();

		g_CMS_hmw_warning = 0x00;
		g_CMS_ttc_warning = 0x00;
		g_AEB_ttc_warning_L1 = 0x00;
		g_AEB_ttc_warning_L2 = 0x00;


		g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x00;
		g_AEB_CMS_outside_BrakeBy_CMSTTC = 0x00;
		g_AEB_CMS_outside_BrakeBy_AEBTTC = 0x00;
		if(LOG_Brake_Cancel == LOGLEVEL)
			fprintf(USART1_STREAM,"g_AEB_CMS_Enable=0\r\n");
		return dec_output;
	}

	////////////////////////////////////////////////
	// calculate the effictive distance every time
	////////////////////////////////////////////////

	Uradar_distance = Get_Uradar_distance(Urader_Company_Message, stVehicleParas);
	g_is_hmw_warning = cam_essiential->HMWWarning;


	////////////////////////////////////////////////
	// filter the frame,one frame just process once
	////////////////////////////////////////////////

	if((*obs_basic_data).TimeID == obs_basic_data_history.TimeID){
	  // if the frame target no cipv, then keep the last one dec
		//-----------------------add xuejie 2022-05-26----
		if((SystemtimeClock - time_stamp_CIPV_TimeID)> 500){
			AEB_State_reset();
			CMS_State_reset();

			g_CMS_hmw_warning = 0x00;
			g_CMS_ttc_warning = 0x00;
			g_AEB_ttc_warning_L1 = 0x00;
			g_AEB_ttc_warning_L2 = 0x00;

			g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x00;
			g_AEB_CMS_outside_BrakeBy_CMSTTC = 0x00;
			g_AEB_CMS_outside_BrakeBy_AEBTTC = 0x00;

			dec_output = 0.0;
			//fprintf(USART1_STREAM,"test the BSD22222222222222\r\n");
		}

		//fprintf(USART1_STREAM,"test the BSD %f\r\n", dec_output);
		//---------------------------------------------
		return dec_output;
	}else{
		//
		cipv_history = (*cipv);
		obs_basic_data_history = (*obs_basic_data);
		cam_essiential_history = (*cam_essiential);
		got_cipv_history = (*cipv).CIPV;
		//-----------------add xuejie 2022-05-26-------------
		time_stamp_CIPV_TimeID = SystemtimeClock;
		//----------------------------------------------------
	}
	/////////////////////////////////////////////////////
	// Function: Caculate decout
	// IF HAVE CIPV, THEN CALCULATE DEC OUTPUT:warng
	// IF Got One fram data,then process once
	////////////////////////////////////////////////////

	//////////////////////////////
	//Driver Braking Signal Cache
	//////////////////////////////
	In_Queue(&DriverBraking_delay,(*stVehicleParas).BrakeFlag);

	//////////////////////////////
	// Dynamic_HMW
	//////////////////////////////

	if(ttc_cms_set_hmw_thr == 0 && dec_output == 0){
		time_beijing = Get_CurrentTime_Stamp();
		float hour_f = time_beijing.tm_hour + (float)time_beijing.tm_min/60.0;
		if(rParms.CMS_HMW_Time_Offset_Night_StartT > rParms.CMS_HMW_Time_Offset_Night_EndT){
			if(hour_f >= rParms.CMS_HMW_Time_Offset_Night_StartT || hour_f < rParms.CMS_HMW_Time_Offset_Night_EndT){
				is_night_flag = 0x01;
				//fprintf(USART1_STREAM,"is_night_flag=1,data=%d-%d-%d\r\n",time_beijing.tm_hour,time_beijing.tm_min,time_beijing.tm_sec);
			}
			else{
				is_night_flag = 0x00;
			}
		}else{
			if(hour_f >= rParms.CMS_HMW_Time_Offset_Night_StartT && hour_f < rParms.CMS_HMW_Time_Offset_Night_EndT){
				is_night_flag = 0x01;
				//fprintf(USART1_STREAM,"is_night_flag=1,data=%d-%d-%d\r\n",time_beijing.tm_hour,time_beijing.tm_min,time_beijing.tm_sec);
			}
			else{
				is_night_flag = 0x00;
			}
		}

		Dynamic_HMW_Switch();
		if(g_HMW_Dynamic_Thr_Enable){
			//Dynamic_hmw_ttc_thr(stVehicleParas->fVehicleSpeed,&dynamic_hmw_thr);
			hmw_brake_ttc_feel = cipv->TTC;
			dynamic_hmw_thr = Dynamic_hmw_thr_2D((stVehicleParas->fVehicleSpeed/3.6),cipv->RelativeSpeedZ,target_cms_dec,rParms.CMS_HMW_Brake_Time_Thr);
		}else{
			dynamic_hmw_thr = rParms.CMS_HMW_Brake_Time_Thr;
		}
		if(is_night_flag == 0x01){
			dynamic_hmw_thr += rParms.CMS_HMW_Time_Offset_Night;
		}
	}
	//fprintf(USART1_STREAM,"dynamic_hmw_thr=%0.2f,hmw=%0.2f,dec_output=%0.2f\r\n",dynamic_hmw_thr,cipv->HMW,dec_output);

	///////////////////////////////
    // Cal break
	///////////////////////////////

#ifdef ReCal_HMW_TTC
	if(ReCal_HMW_TTC == 1){
		// recalculate hmw
		hmw_r_cal = cipv_history.DistanceZ / speed_meter_p_s((*stVehicleParas).fVehicleSpeed);
		if((hmw_r_cal > 10.0))
			hmw_r_cal = 10.0;
		// recalculate ttc
		ttc_r_cal = Cal_TTC(&cipv_history);
		history_speed = (*stVehicleParas).fVehicleSpeed;
		history_distance_z = cipv_history.DistanceZ;
		history_distance_x = cipv_history.DistanceX;
		if((ttc_r_cal > 10.0)|(ttc_r_cal < 0.0))
			ttc_r_cal = 10.0;
		// log
		if(LOGLEVEL == LOG_IN_OUT){
			//Log_print(LOG_IN_OUT, 0x00,0x00," ","main", cipv_history,stVehicleParas, cam_essiential_history,obs_basic_data_history);
		}
	}
#endif
#ifdef USE_ReCal_HMW_TTC
	if(USE_ReCal_HMW_TTC == 1){
		cipv_history.HMW = hmw_r_cal;
		cipv_history.TTC = ttc_r_cal;
		AEB_CMS_parameters.Cam_HMW_warning_time = obs_basic_data_history.HMWAlarmThreshold;
		if(cipv_history.HMW > 6.0)
			cam_essiential_history.HMWGrade = 0;
		if(cipv_history.HMW > 2.7)
			cam_essiential_history.HMWGrade = 3;
		else if(cipv_history.HMW > obs_basic_data_history.HMWAlarmThreshold)
			cam_essiential_history.HMWGrade = 2;
		else if(cipv_history.HMW < obs_basic_data_history.HMWAlarmThreshold)
			cam_essiential_history.HMWGrade = 1;
	}
#endif
	ego_speed = speed_meter_p_s((*stVehicleParas).fVehicleSpeed);
	//AEB_CMS_parameters.cipv_max_distance = ego_speed * effictive_distance_hmw;

	////////////////////////////////////
	//Basic Conditons Judge:No logic inside, just judge the basic conditon;
	////////////////////////////////////
	uint8_t return_back_Break_Trigger = Break_Trigger(&cipv_history, stVehicleParas, &cam_essiential_history,&obs_basic_data_history);
	////////////////////////////////////
	// AEBS Cooling time and max keep time
	////////////////////////////////////
	uint8_t return_back_Safe_Tactics = AEB_System_Safe_Tactics(); // Safe tactics
	//////////////////////////////////
	//Last BrakeCancel Logic:-- Driver Take over the BUS
	//////////////////////////////////
	uint8_t return_back_Driver_control = Driver_control(stVehicleParas);

	if(!rParms.switch_g.AEB_TTC_Enable){
    uint8_t return_back = 1;
    if(return_back_Break_Trigger == break_calculate_cancel){
      //不满足设置的阈值条件，直接返回，不进行计算
      //Log_print(LOG_DEBUG,0x00,0x00, "debug","debug", cipv,stVehicleParas, cam_essiential);
      dec_output = 0.0;
      if((*stVehicleParas).fVehicleSpeed > rParms.Min_Enable_Speed){
        if(LOGLEVEL == LOG_IN_OUT){
          ttc_r_cal = Cal_TTC(&cipv_history);
          hmw_r_cal = cipv_history.DistanceZ / ego_speed;
          Log_print(LOG_IN_OUT, 0x00,0x00," ","main", &cipv_history, stVehicleParas, &cam_essiential_history, &obs_basic_data_history);
        }
      }
      g_AEB_CMS_outside_dec_output = dec_output;
      //---------------add xuejie 2022-05-26----
      AEB_State_reset();
      CMS_State_reset();
      //--------------------------------

      g_CMS_hmw_warning = 0x00;
      //--------------add xuejie 2022-05-26----
      g_CMS_ttc_warning = 0x00;
      g_AEB_ttc_warning_L1 = 0x00;
      g_AEB_ttc_warning_L2 = 0x00;
      //-----------------------------------

      g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x00;
      g_AEB_CMS_outside_BrakeBy_CMSTTC = 0x00;
      g_AEB_CMS_outside_BrakeBy_AEBTTC = 0x00;
      if(LOG_Brake_Cancel == LOGLEVEL)
        fprintf(USART1_STREAM,"Break_Trigger=0\r\n");
      return dec_output;
    }else if(return_back_Safe_Tactics == break_calculate_cancel || return_back_Driver_control == break_calculate_cancel){
      dec_output = 0.0;
      g_AEB_CMS_outside_dec_output = dec_output;
      g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x00;
      g_AEB_CMS_outside_BrakeBy_CMSTTC = 0x00;
      g_AEB_CMS_outside_BrakeBy_AEBTTC = 0x00;
      //if(LOG_Brake_Cancel == LOGLEVEL){
        fprintf(USART1_STREAM,"Cooling_Time:return_back_Safe_Tactics=%d,return_back_Driver_control=%d\r\n",return_back_Safe_Tactics,return_back_Driver_control);
      //}

      return dec_output;
    }
	}
	//////////////////////////////////////////////////////////
	//Status of approaching
	//////////////////////////////////////////////////////////
	approaching_ornot = Approaching_to_lead_car(&cipv_history, approaching_history_n);
	if(LOG_Brake_Cancel == LOGLEVEL)
		fprintf(USART1_STREAM,"approaching_ornot=%d\r\n",approaching_ornot);
	//////////////////////////////
	// FCW + HMW
	//////////////////////////////
	//20221008 为了西安渣土车项目，临时删除，后续需要开发出来，否则报警有问题，导致不兼容之前的设定。
//	fcw_warning = 0x00;
//	AEB_TTC_warning(&cipv_history,
//			&obs_basic_data_history,
//					rParms.AEB_TTC_Warning_Time_Level_First,
//					rParms.AEB_TTC_Warning_Time_Level_Second,
//					rParms.AEB_Decelerate_Set,
//					rParms.AEB_Stop_Distance,
//					rParms.Air_Brake_Delay_Time,
//					&fcw_warning);
//	(fcw_warning == 0x01) ? (g_AEB_ttc_warning_L1 = 0x01) : (g_AEB_ttc_warning_L1 = 0x00);
//	(fcw_warning == 0x02) ? (g_AEB_ttc_warning_L2 = 0x01) : (g_AEB_ttc_warning_L2 = 0x00);

	CMS_TTC_warning(stVehicleParas,cipv_history.TrackNumber,AEB_CMS_parameters.min_track_number,rParms.CMS_TTC_Brake_Time_Thr + rParms.CMS_TTC_Warning_Time_Level_First, rParms.AEB_TTC_Warning_Time_Level_First,rParms.AEB_TTC_Warning_Time_Level_Second,cipv_history.TTC);
	CMS_HMW_warning(stVehicleParas,cipv_history.TrackNumber,AEB_CMS_parameters.min_track_number,(rParms.CMS_HMW_Warning_Time_Thr +(dynamic_hmw_thr - rParms.CMS_HMW_Brake_Time_Thr)), cipv_history.HMW, cam_essiential->HMWGrade, approaching_ornot);

	///////////////////////////////////////////////////////////
	//calculate decelerate
	///////////////////////////////////////////////////////////
	ttc_aeb_dec = AEB_TTC_Break(&cipv_history, &obs_basic_data_history, stVehicleParas);

	if(!rParms.switch_g.AEB_TTC_Enable){
	  ttc_cms_dec = CMS_TTC_Break(&cipv_history, &obs_basic_data_history, stVehicleParas, &cam_essiential_history, Urader_Company_Message);
	  hmw_cms_dec = CMS_HMW_Break(&cipv_history, &obs_basic_data_history, stVehicleParas, &cam_essiential_history, Urader_Company_Message,approaching_ornot);
	}
	if(LOG_Brake_Cancel == LOGLEVEL)
		fprintf(USART1_STREAM,"ttc_aeb_dec=%0.2f,ttc_cms_dec=%0.2f,hmw_cms_dec=%0.2f\r\n",ttc_aeb_dec,ttc_cms_dec,hmw_cms_dec);

	uint8_t return_back = Break_Cancel(&cipv_history, stVehicleParas,&cam_essiential_history,&obs_basic_data_history,dec_output);

	if(!rParms.switch_g.AEB_TTC_Enable){
    if(return_back == break_cancel_TTC){
      ttc_aeb_dec = 0.0;
      ttc_cms_dec = 0.0;
      if(LOG_Brake_Cancel == LOGLEVEL)
        fprintf(USART1_STREAM,"break_cancel_TTC\r\n");
    }
    if(return_back == break_cancel_HMW){
      hmw_cms_dec = 0.0;
      if(LOG_Brake_Cancel == LOGLEVEL)
        fprintf(USART1_STREAM,"break_cancel_HMW\r\n");
    }
    if(break_cancel_TTCHMW == return_back){
      ttc_aeb_dec = 0.0;
      ttc_cms_dec = 0.0;
      hmw_cms_dec = 0.0;
      if(LOG_Brake_Cancel == LOGLEVEL)
        fprintf(USART1_STREAM,"break_cancel_TTCHMW\r\n");
    }
	}
	dec_output = max(max(ttc_aeb_dec, ttc_cms_dec), hmw_cms_dec);

#if 1
	uradar_cms_dec = CMS_URadar_Break(stVehicleParas, Urader_Company_Message);
	if(uradar_cms_dec > 0.0){
		dec_output = max(dec_output,uradar_cms_dec);
	}
#else
	//Angle Radar TTC brake value
	bsd_aeb_dec = AngleRadar_TTC_Break(&cipv_history, &obs_basic_data_history, stVehicleParas);
	if(bsd_aeb_dec > 0.0)
	{
		{
			fprintf(USART1_STREAM,"bsd_aeb_dec=%0.2f,dec_output=%0.2f\r\n",bsd_aeb_dec,dec_output);
		}
		dec_output = max(dec_output,bsd_aeb_dec);
	}
#endif

	//-- Complex Conditons Judge:
	//-- Such as: complex scene + Driver control + ...

	////////////////////////////////
	//-- Monitor the crosswise status
	///////////////////////////////
	uint8_t Crosswise_Status = Lead_car_crosswise_status_monitor(cipv_history, rParms.Vechile_Width, CROSSWISE_STATUS_JUDGE_TIMES, CROSSWISE_STATUS_MIN_DISTANCE_CHANGE);
	if(Crosswise_Status >= 3){
		//fprintf(USART1_STREAM,"dec_output = %0.2f,Crosswise_Status=%d\r\n",dec_output,Crosswise_Status);
		//dec_output = 0.0;
		//if(LOG_Brake_Cancel == LOGLEVEL)
			fprintf(USART1_STREAM,"Crosswise_Status>3\r\n");
	}

	///////////////////////////////
	// Debug
	///////////////////////////////
	//Update_OSC_Data();

	///////////////////////////////
	//Log
	///////////////////////////////
	if(LOGLEVEL == LOG_IN_OUT){
		ttc_r_cal = Cal_TTC(&cipv_history);
		hmw_r_cal = cipv_history.DistanceZ / ego_speed;

		Log_print(LOG_IN_OUT, 0x00,0x00," ","main", &cipv_history, stVehicleParas, &cam_essiential_history, &obs_basic_data_history);
	}
	///////////////////////////////
	// sync the break status
	///////////////////////////////
	if((dec_output > 0.0)){
		if(dec_output == hmw_cms_dec){
			g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x01;
			g_AEB_CMS_outside_BrakeBy_CMSTTC = 0x00;
			g_AEB_CMS_outside_BrakeBy_AEBTTC = 0x00;
		}
		else if(dec_output == ttc_cms_dec){
			g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x00;
			g_AEB_CMS_outside_BrakeBy_CMSTTC = 0x01;
			g_AEB_CMS_outside_BrakeBy_AEBTTC = 0x00;
		}else if(dec_output == ttc_aeb_dec){
			g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x00;
			g_AEB_CMS_outside_BrakeBy_CMSTTC = 0x00;
			g_AEB_CMS_outside_BrakeBy_AEBTTC = 0x01;
		}
	}
	else{
		g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x00;
		g_AEB_CMS_outside_BrakeBy_CMSTTC = 0x00;
		g_AEB_CMS_outside_BrakeBy_AEBTTC = 0x00;
	}
	/////////////////////////////////////////
	// output dec limited
	/////////////////////////////////////////
	if(dec_output > rParms.Max_Output_dec){
		dec_output = rParms.Max_Output_dec;
		//fprintf(USART1_STREAM, "Max_Output_dec=%0.2f\r\n",rParms.Max_Output_dec);
	}else if(dec_output > MAX_DECELERATE_OUTPUT){
		dec_output = MAX_DECELERATE_OUTPUT;
	}

	if(dec_output > 4.f){
	  dec_output = 4.f;
	}
	/////////////////////////////////////////
	// sync the break deceleration
	/////////////////////////////////////////
	g_AEB_CMS_outside_dec_output = dec_output;
	if(dec_output == 0){
		ttc_cms_set_hmw_thr_count++;
		if(ttc_cms_set_hmw_thr_count > 5){
			ttc_cms_set_hmw_thr_count = 0;
			ttc_cms_set_hmw_thr = 0;
			hmw_brake_ttc_feel = 6.0;
			hmw_come_in = false;
		}

	}
	if(LOG_Brake_Cancel == LOGLEVEL)
		fprintf(USART1_STREAM,"final_dec_output = %0.2f\r\n",dec_output);
	return dec_output;
}

float AEB_TTC_Break_Time_Point(Obstacle_Information* cipv,
		Obstacle_Basic_Data *obs_basic_data,
		float aeb_decelerate_set,
		float aeb_stop_distance,
		float air_break_sys_delay_time)
{
	// Is time to Brake ?
	float brake_time_point = 0.5 * cipv->RelativeSpeedZ / 4.0;
	if(rParms.AEB_Decelerate_Set > 0.0)
		brake_time_point = 0.5 * cipv->RelativeSpeedZ / rParms.AEB_Decelerate_Set +
		  (rParms.AEB_Stop_Distance + rParms.Air_Brake_Delay_Time * cipv->RelativeSpeedZ)/cipv->RelativeSpeedZ;
	brake_time_point = max(0.8,min(3.0,brake_time_point));// JT/T-1242 brake time point the TTC/ETTC must small than 3.0s;
	return brake_time_point;
}
static uint8_t aeb_ttc_break_effective = 0x00;
static uint32_t aeb_ttc_break_effective_timestamp = 0;
static uint8_t aeb_ttc_break_effective_Ob_ID = 0;
float AEB_TTC_Break(Obstacle_Information* cipv,
					Obstacle_Basic_Data* obs_basic_data,
					_VEHICLE_PARA* stVehicleParas){
  static int8_t ttc_state = 0;
  static int8_t pre_obs_id = 255;
  static int64_t pre_sys_tm = 0;
  static float new_acc = 0.f;
	float dec_out = 0.f;

	struct ImuData s_imuData;
	s_imuData.x_acc 				  = 0.0;
	s_imuData.y_deg 				  = 0.0;
	s_imuData.z_deg_sp 				  = 0.0;
	//get Imu data for the yaw rate
	GetImuData(&s_imuData);

	// AEB_TTC Switch judge
	if((!g_AEB_CMS_Enable)||(!rParms.switch_g.AEB_TTC_Enable) ||
	    1 == stVehicleParas->ReverseGear){
		ttc_state = 0;
		aeb_ttc_break_effective = 0x00;
		return 0.f;
	}

	//yaw rate大于4.0情况下，也就是拐弯场景下，不进行TTC制动
	if(fabs(s_imuData.z_deg_sp)>=4.0f){
		dec_out = 0.f;
		ttc_state = 0;
		//gcz_serial_v1_dma_printf(SERIAL_UART1,"s_imuData.z_deg_sp = %0.2f",s_imuData.z_deg_sp);
		return 0;
	}

//fprintf(USART1_STREAM,"AEB_TTC_Break=%.2f\r\n",rParms.CMS_TTC_Brake_Time_Thr);
//	float brake_time_point = AEB_TTC_Break_Time_Point(cipv,
//			obs_basic_data,
//			rParms.AEB_Decelerate_Set,
//			rParms.AEB_Stop_Distance,
//			rParms.Air_Brake_Delay_Time);

	float brake_time_point = rParms.AEB_Decelerate_Set;

	if(cipv->TTC > 0.f && cipv->TTC < brake_time_point){
	  const float speed = stVehicleParas->fVehicleSpeed / 3.6f;
	  //dec_out = 0.6f * speed * speed / (cipv->DistanceZ - 1.f); // 保留1米安全距离
	  dec_out = 1.5f * stVehicleParas->fVehicleSpeed / 3.6f / brake_time_point;
	  dec_out = min(4.f, dec_out);
//	  fprintf(USART1_STREAM,"cipv->TTC =%.2f, %.2f, %.2f\r\n",
//      cipv->TTC, brake_time_point, stVehicleParas->fVehicleSpeed);
	}

//  if(1 == ttc_state || 2 == ttc_state){
//    if(pre_obs_id != cipv->ObstacleID){
//      ttc_state = 0;
//      pre_obs_id = 255;
//      dec_out = 0.f;
//      USART1_Bebug_Print("srr", "break off braking", 1);
//    }
//  }

  if(cipv->TTC > 0 && dec_out > 0.f && 0 == ttc_state){
    fprintf(USART1_STREAM,"cipv->RelativeSpeedZ=%.2f\r\n",cipv->RelativeSpeedZ);
    new_acc = dec_out;
    pre_obs_id = cipv->ObstacleID;
  }

  if((dec_out > 0.f && 0 == ttc_state) || 0 != ttc_state){
    TTC_BrakingState(stVehicleParas->fVehicleSpeed, SystemtimeClock,
        &pre_sys_tm, &ttc_state, &new_acc);
    dec_out = new_acc;
  }

//  fprintf(USART1_STREAM,"AEB_TTC_Break2= %d, %.2f\r\n",ttc_state, dec_out);
	return dec_out;
}

//TTC大于1.5秒后的，横向的距离检测
//速度小于30km/h时，min(abs(1+(speed-b)/b),0.6)
//速度大于30km/h时，0.6+(speed-b)/speed;

float lateralMinDistance(float min_lateral, float vehicle_speed, float vehicle_width)
{
	const float piecewise_speed = 20.f;
	float dist = min_lateral;  //min lateral distance is 1.0m

	if(vehicle_speed > piecewise_speed){
	  dist = min_lateral + 1.f - piecewise_speed  / vehicle_speed;
	}
	return dist + vehicle_width;
}

bool InSafeLateralDistance(float front_to_axel, float dist, float obs_x,
    float obs_y, float deg_sp){

	//gcz 2022-09-28 添加日志打印
	if (g_b_srr_log_key)
	{
		gcz_serial_v1_dma_printf(SERIAL_UART1,"[ISD-1]obs_x(%0.4f) - front_to_axel(%0.4f) = %.4f obs_y:%0.4f dist:%0.4f\r\n",obs_x,front_to_axel,obs_x - front_to_axel,obs_y,dist);
	}
  //直行或左转不做横向扩充，纵向距离大于车头到后轴距离不做扩充
  //根据实验结果，完成右转刹停需要的长度是3.0
  //20220929 yuhong 经过讨论及实际测试，此段代码在Demo模式下起作用
  if(false == srr_demo_sw || deg_sp > 0.f || obs_x - front_to_axel >= 3.0){
    return -obs_y < dist;
  }

  //重新设定角度的精度为1度
  float rough_deg_sp = (float)((int)(-deg_sp + 0.5f)) * 3.1415926f / 180.f;
  float safe_dist = obs_x * tanf(rough_deg_sp * 3.f) + dist;//3s后的度数。

	//gcz 2022-09-28 添加日志打印
	if (g_b_srr_log_key)
	{
		gcz_serial_v1_dma_printf(SERIAL_UART1,"[ISD-2]deg_sp:%0.4f rough_deg_sp:%.4f dist:0.4f safe_dist:%0.4f\r\n",deg_sp,rough_deg_sp,dist,safe_dist);
	}
  return -obs_y < min(safe_dist, 3.75f);//3.75车道宽度

}

/// 因为角雷达制动过程中，检测不稳定，故采用固定制动时长策略，减速度按时间平分分成四个阶段a1 = 3/2 a2 = 9/4 a3 = 27/8 a4， t = T / 4
/**
 *  v = (a1 + a2 + a3 + a4) * t = (a1 + 2/3 a1 + 4/9 a1 + 8/27 a1) *t = 65/27 *a1* t
 *    = 65 / 108 * a1 * T
 *  brake_s = s1 + s2 + s3 + s4
 *    = 4 * v * t - (3 * a1 * t + 2 * a2 * t + a3 * t) * t - 1/2*(a1 + a2 + a3 + a4)*t^2
 *    = 4 * v * t - 1/2 * (7 * a1 + 5 * a2  + 3 * a3  + a4) * t^2
 *    = 4 * v * t - 1/2 * (7 + 5 * 2/3 + 3 * 4/9  + 8/27 )* a1 * t^2
 *    = 4 * v * t - 1/54 * (189 + 90 + 36 + 8) * a1 * t^2
 *    = 4 * v * t - 323/54 * a1 * t^2
 *    ≈ v * T - 3/8 * a1 * T^2
 *
 *  计算制动力度
 *  T = v * 108 / 65 / a1
 *  brake_s =  108 / 65 * v^2 / a1 - 3/8 * (108 /65)^2 * v^2 / a1
 *          =  0.626* v^2/a1
 *
 *  计算安全距离 T = ttc = 2s
 *  a1 = v * 108 / 65 / T
 *  brake_s = v * T - 3 / 8 * v * 108 / 65 * T = (1 - 3 / 8 * 108 / 65) * v * T ≈ 0.377 * v * T
 */

static inline float CalcSafeAcc(float d, float v, float response_time){
  float brake_s = d - response_time * v;
  if(brake_s < 0.1){
    return 10.f;
  }

  return 0.626f * v * v / brake_s;
}

// 动态加速度，固定碰撞时间。对于tt来说，是简化的TTC模型
static inline float  CalcSafeDist(float v, float t, float response_time){
  float safe_dist = v * t * 0.377f + v * response_time;
  if(v > 15.f * 3.6f && v < 30.f * 3.6f){
    safe_dist -= 2.f;
  }else{
    safe_dist -= 1.5f;
  }
  return safe_dist;

}

static void LongitudinalMinDistanceAndAcc(float vehicle_speed, float relative_speed,
    float relative_dist, float dist_to_front, float *dist , float *acc){
  const float piecewise_speed = 10.f /3.6f;
  const float ttc = 2.0f, response_time = 1.f;
  const float min_acc = 1.5f;
  const float max_brake_stop_acc = 4.15f; // 18公里刹停 :30.f / (3.6f * ttc) * 108.f / 65.f;
  const float first_safe_dist = 2.f;
  const float second_safe_dist = CalcSafeDist(piecewise_speed, ttc, response_time);
  float veh_speed_ms =  vehicle_speed / 3.6f; // km/h to m/s
  float virtual_bumper = dist_to_front + 1.f; //  Virtual bumper
  float speed = veh_speed_ms;
  float min_safe_dist = first_safe_dist;

  if(relative_speed < 0.f){
    relative_speed = -relative_speed; // minus sign
    if(veh_speed_ms > piecewise_speed){
      //Calculate the min safe distance when vehicle bigger than 10km/h
      min_safe_dist = second_safe_dist;
      //get the speed for the safe distance calculate
      //speed = (relative_speed < piecewise_speed) ? piecewise_speed : relative_speed;

      if(relative_speed > piecewise_speed){
       // relative_speed += 1.5f;
        speed = relative_speed + 1.f;
      }else{
        speed = piecewise_speed;
      }
    }

    *dist = CalcSafeDist(speed, ttc, response_time);
    *dist = max(min_safe_dist, *dist);

    //calculate the deceleration
    if(veh_speed_ms * 3.6f > 40.f){
      *acc = 0.f;
    }else{
      //*acc = speed / ttc;
      // 车头到障碍物的距离
      *acc = CalcSafeAcc(relative_dist - virtual_bumper, speed, response_time);
      *acc = max(min_acc, *acc);
      *acc = min(max_brake_stop_acc, *acc);
    }
  }else {
    *acc = 0.f;
    *dist = first_safe_dist;
    // 如果相对速度大于0，并且自车车速大于分段速度，最小安全距离设置为第二安全距离
    if(veh_speed_ms > piecewise_speed){
      *dist = second_safe_dist;
    }
  }

  *dist += virtual_bumper;
}

uint8_t valueRangeCheck(float a, float value1, float value2)
{
	uint8_t result = 0;
	if(a>=value1&&a<=value2)
	{
		result = 1;
	}
	return result;
}

uint8_t obstacleDirectionMatch(uint8_t obstacleDirection)
{
	//小屏幕定义的方向：0无效 1前方 2右侧 3后方 4左侧
	uint8_t brakeUreaderDirection = 0;
	switch(obstacleDirection)
	{
		case RIGHT_MIDDLE_CAR_ZONE:
			brakeUreaderDirection = 2;
		  break;
		case RIGHT_BACK_CAR_ZONE:
			brakeUreaderDirection = 3;
		  break;
		case RIGHT_FRONT_CAR_ZONE:
			brakeUreaderDirection = 1;
		  break;
		default:
			brakeUreaderDirection = 0;
		  break;
	}
	return brakeUreaderDirection;
}

//Brake value calculate by speed
static float CalcLatDeceleration(float vehicle_speed)
{
  float acc = 0.f;
  if(valueRangeCheck(vehicle_speed, 0, 10.0))
  {
	  acc = 3.0f;
  }
  else if(valueRangeCheck(vehicle_speed, 10, 20.0))
  {
	  acc = 3.5f;
  }
  else if(valueRangeCheck(vehicle_speed, 20, 30.0))
  {
    acc = 3.0f;
  }
  else if(valueRangeCheck(vehicle_speed, 30, 40.0))
  {
	  acc = 2.0f;
  }
//  else if(valueRangeCheck(vehicle_speed, 40, 50.0))
//  {
//	  acc = 2.0f;
//  }
//  else if(valueRangeCheck(vehicle_speed, 50, 60.0))
//  {
//	  acc = 1.5f;
//  }
  else
  {
	  acc = 0.0f;
  }
  return acc;
}

//Safety distance calculate
float bsdSafeDistanceCalculate(float constant, float speed)
{
	float result = constant;
	if(speed<10.0)
	{
		result = constant;
	}
	else if(speed>10.0&&speed<20.0)
	{
		result = constant + 0.5;
	}
	else if(speed>20.0&&speed<30.0)
	{
		result = constant + 1.0;
	}
	else
	{
		result = constant + 1.5;
	}
	return result;
}

//RIGHT_FRONT_CAR_ZONE brake strategy
static bool srr_break_status_ = false;

static uint32_t stopped_object_timestamp[2] = { 0 };
static enum SRRMOtionState last_motion_state[2] = { UNKNOWN_MOTION_STATE,UNKNOWN_MOTION_STATE};
static uint8_t last_obstacle_id[2] = {255, 255};
static uint8_t standing_switch[2] = {0}; //如果障碍物之前变化了，则下面如果id和motion状态为stopped，则一直变为static

void obstacleStopedMotionSwitch(struct SRRObstacle *srrObstacle,uint8_t last_obstacle_id,
								uint8_t last_motion_state, uint8_t *standing_switch,
								uint32_t stopped_object_timestamp)
{
	//Judge that moving target has stopped for 3 seconds then change it to static state
	if(last_obstacle_id!=255
		&&last_obstacle_id==srrObstacle->id
		&&srrObstacle->motion_state==STOPED_MOTION_STATE)
	{
		//之前的保留的障碍物信息的运动状态为MOVING，则更新stopped的状态时间戳
		if(last_motion_state==MOVING_MOTION_STATE)
		{
			stopped_object_timestamp = SystemtimeClock;
			*standing_switch = 0;
		}
		//如果角雷达检测到障碍物停止的时间超过3秒，则将运动状态赋值为静止状态，并初始化之前的记录
		else if((last_motion_state==STOPED_MOTION_STATE)
				&&(SystemtimeClock - stopped_object_timestamp > 2000))
		{
			*standing_switch = 1;
			srrObstacle->motion_state = STANDING_MOTION_STATE;
			//USART1_Bebug_Print_Num("############[srrObstacle.motion_state]: ", srrObstacle->motion_state, 2, 1); // 勿删除
		}
		else
		{

		}
	}
}


bool rightFrontCarZoneBrake(struct SRRObstacle *srrObstacle, SRR_Obstacle_Paras *obstacleParas, _VEHICLE_PARA* stVehicleParas,JUDGE_RESULT scene)
{
	bool result = false;
	struct ImuData s_imuData;
	//Init the Vehicle Width and vehicle front to vehicle rear axle length
	float vehicle_width = g_veh_params->width / 2; //1.1
	float front_to_rear_axle = g_veh_params->front_to_rear_axle;//4.5f;

	//gcz_serial_v1_dma_printf(SERIAL_UART1,"////  %0.2f %0.2f\r\n",g_params.front_to_rear_axle,g_params.width);
	//initialize the obstacle Paras
	obstacleParas->id = srrObstacle->id;
	obstacleParas->decelerationOutput = 0.0;
	obstacleParas->warningZone		  = UNKNOWN_CAR_ZONE;
	obstacleParas->warningLevel 	  = 0;
	obstacleParas->valid       		  = 0;
	obstacleParas->motion_state = srrObstacle->motion_state;
	obstacleParas->vel_x = srrObstacle->vel.x;
	obstacleParas->vel_y = srrObstacle->vel.y;
	obstacleParas->ttc_x = srrObstacle->ttc.x;
	obstacleParas->ttc_y = srrObstacle->ttc.y;
	obstacleParas->dist_x = srrObstacle->dist.x;
	obstacleParas->dist_y = srrObstacle->dist.y;

	s_imuData.x_acc 				  = 0.0;
	s_imuData.y_deg 				  = 0.0;
	s_imuData.z_deg_sp 				  = 0.0;
	//get Imu data for the yaw rate
	GetImuData(&s_imuData);
	//parameter check

	//gcz 2022-09-21 增加超车完成冷却期的判断触发
	enum
	{
		COOLING_TIME_OVER = 0,
		JUDEGE_COOLING_TIME,
	};
	static uint32_t turn_cooling_time;
	static int64_t cur_clk;
	static uint8_t overtake_cooling_ctrl = COOLING_TIME_OVER;

	if (scene == LEFT_SIDE_OVERTAKE && srr_demo_sw != true)
	{
		//相对速度较大时，用相对速度算
		if ((srrObstacle->vel.x >= 1.0) && (srrObstacle->vel.x < -1.0))
		{
			turn_cooling_time = (3 / fabs(srrObstacle->vel.x) + 0.5) * 1000;
		}
		else//相对速度小，用自车车速算，避免计算的冷却期过大
		{
			turn_cooling_time = (3 / (stVehicleParas->fVehicleSpeed * 3.6) + 0.5) * 1000;
		}
		//冷却器限制
		turn_cooling_time = max(turn_cooling_time,500);//大于500ms
		turn_cooling_time = min(turn_cooling_time,3000);//小于3000ms
		overtake_cooling_ctrl = JUDEGE_COOLING_TIME;
		cur_clk = SystemtimeClock;

		if (g_b_srr_log_key)
			gcz_serial_v1_dma_printf(SERIAL_UART1,"************\r\[ncooling come] %d(ms) %.4f(s) deltaVx:%0.4f\r\n",turn_cooling_time,turn_cooling_time /1000.0,srrObstacle->vel.x);
	}

	if (overtake_cooling_ctrl == JUDEGE_COOLING_TIME)
	{
		if ((SystemtimeClock - cur_clk) >= turn_cooling_time)
			overtake_cooling_ctrl = COOLING_TIME_OVER;
	}

  //fprintf(USART1_STREAM, "Imu deg_sp : %.2f, acc : %2.f\n", s_imuData.z_deg_sp, s_imuData.x_acc);
	if(((obstacleParas==NULL)||(srrObstacle->malfunction!=0) || ((scene == LEFT_SIDE_OVERTAKE) ||
	  (scene == LEFT_SIDE_OVERTAKE_JUDGEING) || (scene == TURN_LEFT))))
	{
		//gcz 2022-09-28修复原有于日志开关在关闭时导致无法进入本控制内容的情况
		if (g_b_srr_log_key)
			gcz_serial_v1_dma_printf(SERIAL_UART1,"obstacleParas : %x, malfunction : %d scene : %d \n", obstacleParas, srrObstacle->malfunction, scene);
	return false;
	}

//	gcz_serial_v1_dma_printf(SERIAL_UART1, "RightFronnt: motion_state =%d, "
//		  "VehicleSpeed = %.2f, "
//		  " yaw rate = %0.2f, vel.x = %.2f, vel.y = %.2f, "
//		  " dist.x = %0.2f, dist.y = %0.2f "
//		  "ttx = %.2f, tty = %.2f\r\n",
//		  srrObstacle->motion_state,
//		  stVehicleParas->fVehicleSpeed, s_imuData.z_deg_sp,
//		  srrObstacle->vel.x *3.6f, srrObstacle->vel.y * 3.6f,
//		  srrObstacle->dist.x, srrObstacle->dist.y,
//		  srrObstacle->ttc.x, srrObstacle->ttc.y);

	// 右前侧区域障碍物超本车速度大于1m/s不报警不制动
	if((srrObstacle->is_cipv==TRUE&&srrObstacle->zone==RIGHT_FRONT_CAR_ZONE)
		&&(srrObstacle->motion_state!=STANDING_MOTION_STATE && srrObstacle->motion_state!=UNKNOWN_MOTION_STATE)
		&&stVehicleParas->fVehicleSpeed>5.f && srrObstacle->vel.x < 1.f)
	{
		result = true;
		obstacleParas->valid       = 1;
		obstacleParas->warningZone = obstacleDirectionMatch(srrObstacle->zone);

		//gcz_serial_v1_dma_printf(SERIAL_UART1, "RightFront Entry...............\r\n");

		float lon_dist = 0.f, lon_acc = 0.f, filted_lon_dist = 0.f;
		LongitudinalMinDistanceAndAcc(stVehicleParas->fVehicleSpeed, srrObstacle->vel.x,
		    srrObstacle->dist.x, front_to_rear_axle, &lon_dist, &lon_acc);

		float lat_dist = lateralMinDistance(DISTANCE_MIN,stVehicleParas->fVehicleSpeed,vehicle_width);

		//Srr_FilterSafetyDist(&lon_dist, &filted_lon_dist);

		if(srr_break_status_){
		  lon_dist = lon_dist * 1.1f;
		  //lon_dist = filted_lon_dist;
		}

		srr_break_status_ = false;

//		fprintf(USART1_STREAM, "Imu deg_sp : %.2f, acc : %.2f, srrObstacle->dist.x : %.2f, "
//		    "srrObstacle->dist.y : %.2f\n",
//		          s_imuData.z_deg_sp, s_imuData.x_acc, srrObstacle->dist.x, srrObstacle->dist.y);  // 测试用
		if(srrObstacle->dist.x <= lon_dist &&
		    InSafeLateralDistance(front_to_rear_axle, lat_dist, srrObstacle->dist.x, srrObstacle->dist.y, s_imuData.z_deg_sp))
		{
		  if(true == srr_demo_sw){
        if((1 == stVehicleParas->RightFlag || -s_imuData.z_deg_sp > 3.0f)&&(1!= stVehicleParas->LeftFlag))   //左转向时取消刹车
        {
          obstacleParas->decelerationOutput = lon_acc;
          //有刹车，则标记已经刹车
          srr_break_status_ = true;
        }
		  }else{
        //gcz 2022-09-28 依照朱玉洪转述刘永才提出方法修改判断条件"||"为"&&"
        if((1 == stVehicleParas->RightFlag && -s_imuData.z_deg_sp > 3.0f)&&(1!= stVehicleParas->LeftFlag))   //左转向时取消刹车
        {
          obstacleParas->decelerationOutput = lon_acc;
          //有刹车，则标记已经刹车
          srr_break_status_ = true;
        }
		  }
			if (g_b_srr_log_key)
			{
				gcz_serial_v1_dma_printf(SERIAL_UART1, "[2]Imu deg_sp : %.4f, acc : %0.2f brake_flag:%d\r\n",
					s_imuData.z_deg_sp, s_imuData.x_acc,srr_break_status_);  // 测试用
			}
			obstacleParas->warningLevel = 1;
		}
		else if(srrObstacle->dist.x <= lon_dist && fabs(srrObstacle->dist.y) < vehicle_width + DISTANCE_MAX
		    && srrObstacle->vel.x < 1.f)
		{
			obstacleParas->warningLevel = 2;
		}
		else
		{
			obstacleParas->warningLevel = 0;
		}
		//gcz 2022-09-21 增加TTCx的动态刹车阈值计算

		float ssr_ttc_limt = 100;

		if(srr_demo_sw != true){
			if (srrObstacle->vel.x < 0.0)
				ssr_ttc_limt = fabs(srrObstacle->vel.x) / (4.0 * 2)
							 + (2.5 + rParms.Air_Brake_Delay_Time * fabs(srrObstacle->vel.x))/fabs(srrObstacle->vel.x);
			else
				ssr_ttc_limt = 0.1;
			ssr_ttc_limt = min(ssr_ttc_limt,rParms.f_rigth_font_x_max_ttc);
		}
		if (g_b_srr_log_key)
			gcz_serial_v1_dma_printf(SERIAL_UART1,"[TTC_X limit]---TTC:%0.4f ttc_x:%0.4f\r\n",ssr_ttc_limt,srrObstacle->ttc.x);
		//if(valueRangeCheck(srrObstacle->ttc.x, 0, 2.5f) && valueRangeCheck(srrObstacle->ttc.y,0,2.f)) {
		//yuhong 20220908 添加行人横穿或者两车交汇时，横向速度应该大于1m/s
		float ttc_y = 2.5;
		if (srr_demo_sw != true)
			ttc_y = 1.5;

		if(srrObstacle->dist.x <= lon_dist && valueRangeCheck(srrObstacle->ttc.y,0,ttc_y)
		   && valueRangeCheck(srrObstacle->ttc.x,0,ssr_ttc_limt)	//TTC x紧急情况下制动
		   && (srrObstacle->vel.y >= 1.f) && obstacleParas->decelerationOutput < FLT_EPSILON) {

			bool scene_cancel_brake_flag = false;

			//gcz 2022-09-16 增加线性轨迹预测点坐标
			if (scene == GO_STAIGHT && srr_demo_sw != true)
			{
				PREDICT_POS predict_pos;
				//算法计算需转换Y轴坐标，由负变正，便于计算，所以传递Y距离时，加入fabs取绝对值
				linear_predict_future_postion(srrObstacle->ttc.y,
												srrObstacle->dist.x - front_to_rear_axle,
												fabs(srrObstacle->dist.y - vehicle_width),//取绝对值计算
												srrObstacle->vel.x,srrObstacle->vel.y,
												stVehicleParas->fVehicleSpeed,
												&predict_pos);
				uint8_t target_movement_type = check_movement_direction(stVehicleParas->fVehicleSpeed /3.6,srrObstacle->vel.x);
				uint8_t temp;
				if (target_movement_type == HOMODROMOUS)
					temp = 'S';
				else if (target_movement_type == ANTIDROMIC)
					temp = 'N';
				else
					temp = 'H';
				if (g_b_srr_log_key)
				{
					gcz_serial_v1_dma_printf(SERIAL_UART1,"++++++++++\r\n    [11111]predict %0.4f %0.4f deltaVx:%.4f type:%c ",predict_pos.x,predict_pos.y,srrObstacle->vel.x,temp);
					gcz_serial_v1_dma_printf(SERIAL_UART1,"%0.2f %0.2f\r\n",front_to_rear_axle,vehicle_width);
				}

				if (/*(predict_pos.x > 0) && */(predict_pos.y >= rParms.f_rigth_font_y_dis))
				{
					scene_cancel_brake_flag = true;
					if (g_b_srr_log_key)
						gcz_serial_v1_dma_printf(SERIAL_UART1,"    [222222]NO BRAKE-------\r\n");
				}
				else
				{
					if (g_b_srr_log_key)
					{
						if (overtake_cooling_ctrl == JUDEGE_COOLING_TIME)
							gcz_serial_v1_dma_printf(SERIAL_UART1,"    [333333]CooLing stop brake-------\r\n");
						else
							gcz_serial_v1_dma_printf(SERIAL_UART1,"    [222222]need     brake-------\r\n");
					}
				}
			}
			else if (scene == TURN_RIGHT && srr_demo_sw != true)
			{
				//if (srrObstacle->ttc.y <= 0.05)
				float f_cal_x;
				CIRCL_AREA cirle_area = judgement_circular_type(stVehicleParas->fVehicleSpeed,s_imuData.z_deg_sp,srrObstacle->dist.y - vehicle_width,srrObstacle->dist.x - front_to_rear_axle,&f_cal_x);
				if (cirle_area == EXTERNAL)
				{
					scene_cancel_brake_flag = true;
					if (g_b_srr_log_key)
						gcz_serial_v1_dma_printf(SERIAL_UART1,"    [333333]RIGHT circle cancle brake---circle:x:%d y:%d----\r\n",f_cal_x,srrObstacle->dist.y - vehicle_width);
				}
				else
				{
					if (g_b_srr_log_key)
						gcz_serial_v1_dma_printf(SERIAL_UART1,"    [333333]RIGHT CIRCLE Need BRAKE---circle:x:%d y:%d----\r\n",f_cal_x,srrObstacle->dist.y - vehicle_width);
				}
			}

		  if((1!= stVehicleParas->LeftFlag) && (!scene_cancel_brake_flag)
		  	  && (overtake_cooling_ctrl != JUDEGE_COOLING_TIME))   //左转向时取消刹车     gcz 2022-09-19 增加： 取消直行时刻，预测的不应刹车的制动情况
		  {
			  obstacleParas->decelerationOutput = lon_acc;
			  //srr_break_status_ = true;
		  }

		  obstacleParas->warningLevel = 1;
		  if (g_b_srr_log_key)
		  {
			  gcz_serial_v1_dma_printf(SERIAL_UART1, "Imu deg_sp : %.2f, acc : %0.2f\n", s_imuData.z_deg_sp, s_imuData.x_acc*9.8);
			  gcz_serial_v1_dma_printf(SERIAL_UART1,"[info]---v_ego:%0.4f km/h %0.4f ms/s dist.x:%0.6f dist.y:%0.6f ",
					  	  	  	  	  stVehicleParas->fVehicleSpeed,stVehicleParas->fVehicleSpeed/3.6,srrObstacle->dist.x,srrObstacle->dist.y);
			  gcz_serial_v1_dma_printf(SERIAL_UART1,"ttc.x=%0.6f ttc.y=%0.6f vel.x = %0.6f vel.y = %0.6f\r\n-----------\r\n",
					  	  	  	  	  srrObstacle->ttc.x,srrObstacle->ttc.y,srrObstacle->vel.x,srrObstacle->vel.y);
			}
		}

//		if(obstacleParas->warningLevel >0){
//		  gcz_serial_v1_dma_printf(SERIAL_UART1, "Zone =%d, dec =%0.2f, "
//				  "Level =%d, VehicleSpeed = %.2f, "
//				  " yaw rate = %0.2f, vel.x = %.2f, vel.y = %.2f, "
//				  "lon_dist = %.2f, lat_dist = %.2f, "
//				  "lat_acc = %.2f, dist.x = %0.2f, dist.y = %0.2f "
//				  "ttx = %.2f, tty = %.2f\r\n",
//				  obstacleParas->warningZone, obstacleParas->decelerationOutput,
//				  obstacleParas->warningLevel, stVehicleParas->fVehicleSpeed, s_imuData.z_deg_sp,
//				  srrObstacle->vel.x *3.6f, srrObstacle->vel.y * 3.6f,
//				  lon_dist, lat_dist, lon_acc, srrObstacle->dist.x, srrObstacle->dist.y,
//				  srrObstacle->ttc.x, srrObstacle->ttc.y);
//		}
	}

	return result;
}
//RIGHT_MIDDLE_CAR_ZONE brake strategy
bool rightSideCarZoneBrake(struct SRRObstacle *srrObstacle, SRR_Obstacle_Paras *obstacleParas, _VEHICLE_PARA* stVehicleParas,JUDGE_RESULT scene)
{
	bool result = false;
	//Init the Vehicle Width
	float vehicle_width = g_veh_params->width/2; //1.1
	float front_to_rear_axle = g_veh_params->front_to_rear_axle;//4.5f;
  //fprintf(USART1_STREAM, "vehicle_width: %.2f\n", vehicle_width);
	float lat_dist = 1.0f;
	struct ImuData s_imuData;
	//initialize the obstacle Paras
	obstacleParas->id = srrObstacle->id;
	obstacleParas->decelerationOutput = 0.0;
	obstacleParas->warningZone		  = UNKNOWN_CAR_ZONE;
	obstacleParas->warningLevel 	  = 0;
	obstacleParas->valid       		  = 0;
	obstacleParas->motion_state = srrObstacle->motion_state;
	obstacleParas->vel_x = srrObstacle->vel.x;
	obstacleParas->vel_y = srrObstacle->vel.y;
	obstacleParas->ttc_x = srrObstacle->ttc.x;
	obstacleParas->ttc_y = srrObstacle->ttc.y;
	obstacleParas->dist_x = srrObstacle->dist.x;
	obstacleParas->dist_y = srrObstacle->dist.y;

	s_imuData.x_acc 				  = 0.0;
	s_imuData.y_deg 				  = 0.0;
	s_imuData.z_deg_sp 				  = 0.0;

    //get Imu data for the yaw rate
	GetImuData(&s_imuData);
	//parameter check
	if((obstacleParas==NULL)||(srrObstacle->malfunction!=0) || ((scene == LEFT_SIDE_OVERTAKE) || (scene == LEFT_SIDE_OVERTAKE_JUDGEING) || (scene == TURN_LEFT)))
	{
		return false;
	}

//	gcz_serial_v1_dma_printf(SERIAL_UART1, "RightSide: motion_state =%d, "
//				  "VehicleSpeed = %.2f, "
//				  " yaw rate = %0.2f, vel.x = %.2f, vel.y = %.2f.\r\n",
//				  srrObstacle->motion_state, stVehicleParas->fVehicleSpeed, s_imuData.z_deg_sp,
//				  srrObstacle->vel.x, srrObstacle->vel.y);

	//右侧区域障碍物超车速度大于3m/s不报警不制动，本车超右侧障碍物速度大于3m/s不报警不制动
	if((srrObstacle->is_cipv==TRUE&&srrObstacle->zone==RIGHT_MIDDLE_CAR_ZONE)
		&&(srrObstacle->motion_state!=STANDING_MOTION_STATE && srrObstacle->motion_state!=UNKNOWN_MOTION_STATE)
		&&stVehicleParas->fVehicleSpeed>5.f && srrObstacle->vel.x < 3.f && srrObstacle->vel.x > -3.f)
	{
//		gcz_serial_v1_dma_printf(SERIAL_UART1, "RightSide: motion_state =%d, "
//			  "VehicleSpeed = %.2f, "
//			  " yaw rate = %0.2f, vel.x = %.2f, vel.y = %.2f, "
//			  " dist.x = %0.2f, dist.y = %0.2f "
//			  "ttx = %.2f, tty = %.2f\r\n",
//			  srrObstacle->motion_state,
//			  stVehicleParas->fVehicleSpeed, s_imuData.z_deg_sp,
//			  srrObstacle->vel.x *3.6f, srrObstacle->vel.y * 3.6f,
//			  srrObstacle->dist.x, srrObstacle->dist.y,
//			  srrObstacle->ttc.x, srrObstacle->ttc.y);

	  lat_dist = lateralMinDistance( DISTANCE_MIN,stVehicleParas->fVehicleSpeed,vehicle_width);
		obstacleParas->warningZone = obstacleDirectionMatch(srrObstacle->zone);
		obstacleParas->valid       = 1;
		result = true;
		//gcz 2022-09-29 note
		//原始if(valueRangeCheck(fabs(srrObstacle->dist.y),vehicle_width+0.2, lateralDistance))该条件判断1.3~19(最大2.2)米
		//下面新判断，判断的是小于1.9(最大2.2)米以内，需要刹车
		//对比来看，新判断对于靠近车辆的判断条件严苛增加，更容易制动或误刹
		if(InSafeLateralDistance(front_to_rear_axle, lat_dist, srrObstacle->dist.x, srrObstacle->dist.y, s_imuData.z_deg_sp)||
		    srrObstacle->ttc.y < 2.f)
		//if(valueRangeCheck(fabs(srrObstacle->dist.y),1.1,2.6))
		{

		  if(true == srr_demo_sw){
        if((stVehicleParas->RightFlag==1 || -s_imuData.z_deg_sp > 2.5f)
          &&(1!= stVehicleParas->LeftFlag))   //左转向时取消刹车
        {
          obstacleParas->decelerationOutput = CalcLatDeceleration(stVehicleParas->fVehicleSpeed);
        }
		  }else{
		    if((stVehicleParas->RightFlag==1 && -s_imuData.z_deg_sp > 2.5f)
		            &&(1!= stVehicleParas->LeftFlag))   //左转向时取消刹车
        {
          obstacleParas->decelerationOutput = CalcLatDeceleration(stVehicleParas->fVehicleSpeed);
        }
		  }
			if (g_b_srr_log_key)
			{
				gcz_serial_v1_dma_printf(SERIAL_UART1, "[1]Imu deg_sp : %.2f, acc : %0.4f\n", s_imuData.z_deg_sp, s_imuData.x_acc);
			}
			obstacleParas->warningLevel = 1;

//			gcz_serial_v1_dma_printf(SERIAL_UART1, "RightFlag : %d, LeftFlag :%d \n", stVehicleParas->RightFlag,
//			    stVehicleParas->LeftFlag);
		}
		else if(valueRangeCheck(fabs(srrObstacle->dist.y),lat_dist,vehicle_width+DISTANCE_MAX))
		{
			obstacleParas->warningLevel = 2;
		}
		else
		{
			obstacleParas->warningLevel = 0;
		}
	}
//	if(obstacleParas->valid==1)
//	{
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"RightSideCarZone: warningZone =%d, decelerationOutput =%0.2f, "
//				"warningLevel =%d, vehicalWidth = %0.2f, yaw rate = %0.2f.\r\n",
//				obstacleParas->warningZone, obstacleParas->decelerationOutput,
//				obstacleParas->warningLevel, rParms.Vechile_Width, s_imuData.z_deg_sp);
//		gcz_serial_v1_dma_printf(SERIAL_UART1, "RightSideCarZone dec =%0.2f, "
//						  "Level =%d, VehicleSpeed = %.2f, "
//						  "yaw rate = %0.2f, vel.x = %.2f, vel.y = %.2f, "
//						  "lateralDistance = %.2f, dist.x = %0.2f, dist.y = %0.2f "
//						  "ttx = %.2f, tty = %.2f\r\n", obstacleParas->decelerationOutput,
//						  obstacleParas->warningLevel, stVehicleParas->fVehicleSpeed, s_imuData.z_deg_sp,
//						  srrObstacle->vel.x *3.6f, srrObstacle->vel.y * 3.6f,
//						  lateralDistance, srrObstacle->dist.x, srrObstacle->dist.y,
//						  srrObstacle->ttc.x, srrObstacle->ttc.y);
//	}
	return result;
}

//RIGHT_BACK_CAR_ZONE brake strategy
bool rightRearCarZoneBrake(struct SRRObstacle *srrObstacle, SRR_Obstacle_Paras *obstacleParas, _VEHICLE_PARA* stVehicleParas)
{
	bool result = false;

	//initialize the obstacle Paras
	obstacleParas->id = srrObstacle->id;
	obstacleParas->decelerationOutput = 0.0;
	obstacleParas->warningZone		  = UNKNOWN_CAR_ZONE;
	obstacleParas->warningLevel 	  = 0;
	obstacleParas->valid       		  = 0;
	obstacleParas->motion_state = srrObstacle->motion_state;
	obstacleParas->vel_x = srrObstacle->vel.x;
	obstacleParas->vel_y = srrObstacle->vel.y;
	obstacleParas->ttc_x = srrObstacle->ttc.x;
	obstacleParas->ttc_y = srrObstacle->ttc.y;
	obstacleParas->dist_x = srrObstacle->dist.x;
	obstacleParas->dist_y = srrObstacle->dist.y;
	//parameter check
	if((obstacleParas==NULL)||(srrObstacle->malfunction!=0))
	{
		return false;
	}

	if((srrObstacle->is_cipv==TRUE&&srrObstacle->zone==RIGHT_BACK_CAR_ZONE)
		&&(srrObstacle->motion_state!=STANDING_MOTION_STATE && srrObstacle->motion_state!=UNKNOWN_MOTION_STATE)
		&&stVehicleParas->fVehicleSpeed>5.f)
	{
		result = true;
		obstacleParas->warningZone = obstacleDirectionMatch(srrObstacle->zone);
		obstacleParas->valid       = 1;

		if(valueRangeCheck(srrObstacle->ttc.x,0,1.5))
		{
			obstacleParas->warningLevel = 2;
		}
		else if(valueRangeCheck(srrObstacle->ttc.x,1.5,3.0))
		{
			obstacleParas->warningLevel = 2;
		}
		else if(valueRangeCheck(srrObstacle->ttc.x,3.0,10.0)
				&&valueRangeCheck(fabs(srrObstacle->dist.x),1.7,50.0)) //如果车在车身后到50m之内，则需要显示障碍物;后轴为原点，需要加上-1.7米
		{
			obstacleParas->warningLevel = 3;
		}
		else
		{
			obstacleParas->warningLevel = 0;
		}
	}
//	if(obstacleParas->valid==1)
//	{
//		gcz_serial_v1_dma_printf(SERIAL_UART1, "RightRearCarZone: warningZone =%d, decelerationOutput =%0.2f, warningLevel =%d\r\n",
//				obstacleParas->warningZone, obstacleParas->decelerationOutput, obstacleParas->warningLevel);
//	}
	return result;
}
enum SRRMalfunction calculateAngleRadarMalfunction(uint8_t frontMalf, uint8_t sideMalf,uint8_t rearMalf)
{
  enum SRRMalfunction result = SENSOR_OK;

	if(frontMalf==SENSOR_FAILURE
		||sideMalf==SENSOR_FAILURE
		||rearMalf==SENSOR_FAILURE)
	{
		result = SENSOR_FAILURE;
	}
	else if(frontMalf==SENSOR_COMMUNICATION_FAILURE
			||sideMalf==SENSOR_COMMUNICATION_FAILURE
			||rearMalf==SENSOR_COMMUNICATION_FAILURE)
	{
		result = SENSOR_COMMUNICATION_FAILURE;
	}
	else
	{
		result = SENSOR_PERFORM_DECREASED;
	}
	return result;
}

uint8_t getAngleRadarMalfunction( void )
{
  return srrMalfunction_;
}

SRR_Obstacle_Paras srrOutputParas;

SRR_Obstacle_Paras getSrrWarningAndZoneParas( void )
{
	if(srrOutputParas.valid!=1)
	{
		srrOutputParas.levelOneValid = false;
		srrOutputParas.levelTwoValid = false;
		srrOutputParas.levelThreeValid = false;
	}
	else
	{
		switch(srrOutputParas.warningLevel)
		{
			case 0:
				srrOutputParas.levelOneValid = false;
				srrOutputParas.levelTwoValid = false;
				srrOutputParas.levelThreeValid = false;
				break;
			case 1:
				srrOutputParas.levelOneValid = true;
				srrOutputParas.levelTwoValid = false;
				srrOutputParas.levelThreeValid = false;

				break;
			case 2:
				srrOutputParas.levelOneValid = false;
				srrOutputParas.levelTwoValid = true;
				srrOutputParas.levelThreeValid = false;
				break;
			case 3:
				srrOutputParas.levelOneValid = false;
				srrOutputParas.levelTwoValid = false;
				srrOutputParas.levelThreeValid = true;
				break;
			default:
				srrOutputParas.levelOneValid = false;
				srrOutputParas.levelTwoValid = false;
				srrOutputParas.levelThreeValid = false;
				break;
		}
	}
	return srrOutputParas;
}
void setObstacleParasToDefault(SRR_Obstacle_Paras *paras)
{
	paras->decelerationOutput 	= 0.0;
	paras->levelOneValid 		= false;
	paras->levelTwoValid 		= false;
	paras->levelThreeValid 		= false;
	paras->valid 				= false;
	paras->warningLevel 		= 0;
	paras->warningZone 			= 0;
	paras->motion_state = UNKNOWN_MOTION_STATE;
}

#if 0
typedef enum trun_def
{
	OVERTAKE_GO_STAIGHT_STATE =0,
	OVERTAKE_LEFT_STATE,
	OVERTAKE_RIGHT_STATE,
}TRUE_DEF;

typedef enum ctrl_def
{
	NO_CHECK_CTRL_STATE = 0,
	START_CHECK_IS_TRUN_RIGHT_STATE,
	START_CHECK_IS_STAIGHT_STATE,
	LEFT_OVERTAKE_YES_STATE,
}CTRL_DEF;
//typedef enum _is_need_brake
//{
//	DONOT_BRAKE = 0,					//不刹车
//	NEED_JUDGE_BRAKE,					//需要判断刹车
//	U_DECIDE_ISNEED_BRAKE				//你自己决定是否需要刹车
//}BRAKE_CTRL_RESULT;
//超车场景过滤
//返回：判断场景
JUDGE_RESULT overtake_conditional_filter_v1()
{
	JUDGE_RESULT res = OTHER;
//	gcz_serial_v1_dma_printf(SERIAL_UART1,"**\r\n");
	static TRUE_DEF trun_around_state = OVERTAKE_GO_STAIGHT_STATE;
	static TRUE_DEF trun_around_state_zero = OVERTAKE_GO_STAIGHT_STATE;
	static CTRL_DEF ctrl_state = NO_CHECK_CTRL_STATE;
	uint8_t trun_around_state_temp;
	IMU_FILTER_DATA *p = get_imu_original_data();
	//1.触发条件识别
	//左转为正
	if (p->angular_v[Z_P] >= 2)
	{
		trun_around_state_temp  = OVERTAKE_LEFT_STATE;
	//	gcz_serial_v1_dma_printf(SERIAL_UART1,"1++A:  <<===\r\n");
	}
	else if (p->angular_v[Z_P] <= -2)
	{
		trun_around_state_temp  = OVERTAKE_RIGHT_STATE;
	//	gcz_serial_v1_dma_printf(SERIAL_UART1,"2++B:  ===>>\r\n");
	}
	else
	{
		trun_around_state_temp = OVERTAKE_GO_STAIGHT_STATE;
	//	gcz_serial_v1_dma_printf(SERIAL_UART1,"3++x:  |||||\r\n");
	}
	gcz_serial_v1_dma_printf(SERIAL_UART1,"\r\n**sys_clk: %d\r\n",SystemtimeClock);
	gcz_serial_v1_dma_printf(SERIAL_UART1,"4++C: st %d %0.6f\r\n",trun_around_state_temp,p->angular_v[Z_P]);
	//1.触发源过滤保持，起到稳定左右，避免跳动带来的干扰，稳定250ms内没有跳动的才认为是有效值
	static int64_t action_cur_time_zero = 0;
	static int64_t action_cur_time = 0;
	static bool zero_flag = false;
	if (trun_around_state_temp != OVERTAKE_GO_STAIGHT_STATE)
	{
		trun_around_state_zero = trun_around_state_temp;
		action_cur_time_zero = SystemtimeClock;
		zero_flag = false;
	//	gcz_serial_v1_dma_printf(SERIAL_UART1,"3.111111 ===========\r\n");
	}
	else if ((trun_around_state_temp == OVERTAKE_GO_STAIGHT_STATE)
		&& (trun_around_state_zero != trun_around_state_temp))
	{
		if ((SystemtimeClock - action_cur_time_zero) >= 800)
		{
			trun_around_state_zero = trun_around_state_temp;
			zero_flag = true;
		//	gcz_serial_v1_dma_printf(SERIAL_UART1,"3.222222 ===========\r\n");
		}
		else
		{
			zero_flag = false;
		//	gcz_serial_v1_dma_printf(SERIAL_UART1,"3.33333333333 ===========\r\n");
		}
	}
	else
	{
		action_cur_time_zero = SystemtimeClock;
		zero_flag = false;
	//	gcz_serial_v1_dma_printf(SERIAL_UART1,"3.44444444444 ===========\r\n");
	}


	if (trun_around_state != trun_around_state_temp)
	{
		if ((SystemtimeClock - action_cur_time) >= 200)
		{
			if ((trun_around_state_temp == OVERTAKE_GO_STAIGHT_STATE) && (zero_flag))
			{
				trun_around_state = OVERTAKE_GO_STAIGHT_STATE;
			//	gcz_serial_v1_dma_printf(SERIAL_UART1,"3.55555555555 ===========\r\n");
			}
			else if (trun_around_state_temp != OVERTAKE_GO_STAIGHT_STATE)
			{
				trun_around_state = trun_around_state_temp;
			//	gcz_serial_v1_dma_printf(SERIAL_UART1,"3.66666666666 ===========\r\n");
			}
#if 0
			uint8_t buffer[10];
			if (trun_around_state_temp  == OVERTAKE_LEFT_STATE)
				strcpy(buffer,"5++<<<<<-----");
			else if (trun_around_state_temp  == OVERTAKE_RIGHT_STATE)
				strcpy(buffer,"6++----->>>>>");
			else
			{
				strcpy(buffer,"7++||||||||||");
			}
			gcz_serial_v1_dma_printf(SERIAL_UART1,"1:TURN_TRIGGER:%s %0.6f\r\n",buffer,p->angular_v[Z_P]);
#endif
		}
#if 0
		else
			gcz_serial_v1_dma_printf(SERIAL_UART1,"8++1111111111111111\r\n");
#endif
	}
	else
	{
		action_cur_time = SystemtimeClock;
	//	gcz_serial_v1_dma_printf(SERIAL_UART1,"9++22222222222222222\r\n");
	}

	//2.场景判断触发
	if (trun_around_state == OVERTAKE_LEFT_STATE)//发现左转向    a?=↖
	{
		ctrl_state = START_CHECK_IS_TRUN_RIGHT_STATE;//开始检测是否右转向 a = ↖
#if 1
		gcz_serial_v1_dma_printf(SERIAL_UART1,"10++2:cur <<-- start judge ?= -->> ????");
#endif
	}

	gcz_serial_v1_dma_printf(SERIAL_UART1,"11++ctrl_st: %d  turn_st:%d\r\n",ctrl_state,trun_around_state);
	//3.开始场景判断及结果识别的状态机控制
	//判断是否左转向后右转向，进行变道超车
	if (ctrl_state == START_CHECK_IS_TRUN_RIGHT_STATE)
	{
		//左转过程，持续检测是否右转，判断是否转向动作
		if (trun_around_state == OVERTAKE_RIGHT_STATE)//发现右转向了，开始角速度判断是否变负 b ?= ↗
		{
			ctrl_state = START_CHECK_IS_STAIGHT_STATE;//b = ↗
			res = LEFT_SIDE_OVERTAKE;
			gcz_serial_v1_dma_printf(SERIAL_UART1,"12++3:cur == -->>");
		}
		else if (trun_around_state == OVERTAKE_GO_STAIGHT_STATE)//左转后直行250ms，认为是做转向  a ?= ↑，这里持续判断R可区分左转与掉头
		{
			res = TURN_LEFT;//a = ↑
			ctrl_state = NO_CHECK_CTRL_STATE;
#if 1
			gcz_serial_v1_dma_printf(SERIAL_UART1,"13++4:cur == ||||");
#endif
		}
		else
		{
			res = LEFT_SIDE_OVERTAKE_JUDGEING;
		}
	}
	else if (ctrl_state == START_CHECK_IS_STAIGHT_STATE)//持续判断是否变直行 b ?= ↑
	{
		if (trun_around_state == OVERTAKE_GO_STAIGHT_STATE)//发现角速度已经变负，可以正式判断是超车场景
		{
			ctrl_state = LEFT_OVERTAKE_YES_STATE;// b = ↑
			res = LEFT_SIDE_OVERTAKE;
#if 1
			gcz_serial_v1_dma_printf(SERIAL_UART1,"14++5:cur == ||| == overtake");
#endif
		}
		else
		{
			res = LEFT_SIDE_OVERTAKE_JUDGEING;
		}
	}
	else if (ctrl_state == LEFT_OVERTAKE_YES_STATE)//判断出左侧超车
	{
		ctrl_state = NO_CHECK_CTRL_STATE;
		res = LEFT_SIDE_OVERTAKE;
#if 1
			gcz_serial_v1_dma_printf(SERIAL_UART1,"15++6:overtake is close");
#endif
	}
	else//右转不关注
	{
		if (trun_around_state == OVERTAKE_GO_STAIGHT_STATE)
			res = GO_STAIGHT;
		else if(trun_around_state == OVERTAKE_RIGHT_STATE)//这里持续判断R可区分左转与掉头
		{
			res = TURN_RIGHT;
#if 1
			gcz_serial_v1_dma_printf(SERIAL_UART1,"16++7:cur running == --->>>>");
#endif
		}
	}
	return res;
}
#endif
#define FLOAT_INVALID 							9999.0
#define GO_STRAIGHT_DETECTION_KEEP_TIME			500				//直行检测保持时间 ms
#define TRUN_LEFT_DECTION_TIME					4000			//左转检测保持时间 ms
#define TRUN_RIGHT_DECTION_TIME					3000			//右转转检测保持时间 ms

//V1参数
//#define TRUN_LEFT_TRIGGER_DEG_SP_1			2.0
//#define L_2_M_TRIGGER_LEFT_SIDE_DEG_SP_2		1.0
//#define L_2_R_TRIGGER_LEFT_SIDE_DEG_SP_3		0.7
//#define L_2_M_TRIGGER_RIGHT_SIDE_DEG_SP_4		-0.7
//#define L_2_R_TRIGGER_RIGHT_SIDE_DEG_SP_5		-1.0
//#define TRUN_RIGHT_TRIGGER_DEG_SP_6			-2.0
//V2参数
#define TRUN_LEFT_TRIGGER_DEG_SP_1				2.0
#define L_2_M_TRIGGER_LEFT_SIDE_DEG_SP_2		1.0
#define L_2_R_TRIGGER_LEFT_SIDE_DEG_SP_3		1.0
#define L_2_M_TRIGGER_RIGHT_SIDE_DEG_SP_4		-1.0
#define L_2_R_TRIGGER_RIGHT_SIDE_DEG_SP_5		-1.0
#define TRUN_RIGHT_TRIGGER_DEG_SP_6				-2.0
JUDGE_RESULT overtake_conditional_filter()
{
	JUDGE_RESULT res = GO_STAIGHT;

	IMU_FILTER_DATA *p = get_imu_original_data();
	enum
	{
		IDLE_STATE = 0,
		START_L_STATE,			//开始左
		L_2_M_STATE,			//左到中
		L_2_R_2_M_OVER_STATE,	//左到右再到中
		L_2_R_STATE,			//到右
		M_STATE,				//稳定中
		TURN_R_STATE,			//右转
		TURN_L_STATE,			//左转
	};
	static uint8_t turn_direction_detection = IDLE_STATE;
	float trun_dep_sp_temp = FLOAT_INVALID;
	static int64_t action_l_2_r_time = 0;
#if 0
	gcz_serial_v1_dma_printf(SERIAL_UART1,"*********sys_clk: %d\r\n",SystemtimeClock);
	gcz_serial_v1_dma_printf(SERIAL_UART1,"1.[dep_sp]:  <%0.6f>\r\n",p->angular_v[Z_P]);
#endif
	//1.动作源触发
	if (turn_direction_detection == IDLE_STATE)
	{
		if (p->angular_v[Z_P] >= TRUN_LEFT_TRIGGER_DEG_SP_1)
		{
			trun_dep_sp_temp = p->angular_v[Z_P];
			turn_direction_detection = START_L_STATE;
			action_l_2_r_time = SystemtimeClock;
#if 0
			gcz_serial_v1_dma_printf(SERIAL_UART1,"1.1 [trigger]:%s\r\n","START_L_STATE");
#endif
		}
		else if (p->angular_v[Z_P] < TRUN_RIGHT_TRIGGER_DEG_SP_6)
		{
			trun_dep_sp_temp = p->angular_v[Z_P];
			turn_direction_detection = TURN_R_STATE;
			action_l_2_r_time = SystemtimeClock;
#if 0
			gcz_serial_v1_dma_printf(SERIAL_UART1,"1.2 [trigger]:%s\r\n","TURN_R_STATE");
#endif
		}
	}
	//2.转向类型判断
	if (turn_direction_detection == START_L_STATE)//开始左转
	{
		if (p->angular_v[Z_P] < L_2_M_TRIGGER_LEFT_SIDE_DEG_SP_2)
		{
			turn_direction_detection = L_2_M_STATE;
			action_l_2_r_time = SystemtimeClock;
#if 0
			gcz_serial_v1_dma_printf(SERIAL_UART1,"2.1 [START_L_STATE]:trun to [L_2_M_STATE]\r\n");
#endif
		}
		else if (p->angular_v[Z_P] >= TRUN_LEFT_TRIGGER_DEG_SP_1)
		{
			if ((SystemtimeClock - action_l_2_r_time) >= TRUN_LEFT_DECTION_TIME)
			{
				turn_direction_detection = TURN_L_STATE;
#if 0
				gcz_serial_v1_dma_printf(SERIAL_UART1,"2.2 [START_L_STATE]:trun to [TURN_L_STATE]\r\n");
#endif
			}
		}
		else
		{
			action_l_2_r_time = SystemtimeClock;
		}
	}
	else if (turn_direction_detection == L_2_M_STATE)//左到中了
	{
		if ((p->angular_v[Z_P] >= L_2_M_TRIGGER_RIGHT_SIDE_DEG_SP_4) && (p->angular_v[Z_P] <= L_2_M_TRIGGER_LEFT_SIDE_DEG_SP_2))
		{
			if ((SystemtimeClock - action_l_2_r_time) >= GO_STRAIGHT_DETECTION_KEEP_TIME)
			{
				turn_direction_detection = M_STATE;
#if 0
				gcz_serial_v1_dma_printf(SERIAL_UART1,"3.1 [L_2_M_STATE]:trun to [M_STATE]\r\n");
#endif
			}
		}
		else if (p->angular_v[Z_P] < L_2_R_TRIGGER_RIGHT_SIDE_DEG_SP_5)
		{
			turn_direction_detection = L_2_R_STATE;
			action_l_2_r_time = SystemtimeClock;
#if 0
			gcz_serial_v1_dma_printf(SERIAL_UART1,"3.2 [L_2_M_STATE]:trun to [L_2_R_STATE]\r\n");
#endif
		}
		else if (p->angular_v[Z_P] > TRUN_LEFT_TRIGGER_DEG_SP_1)
		{
			turn_direction_detection = START_L_STATE;
			action_l_2_r_time = SystemtimeClock;
#if 0
			gcz_serial_v1_dma_printf(SERIAL_UART1,"3.3 [L_2_M_STATE]:trun to [START_L_STATE]\r\n");
#endif
		}
		else
			action_l_2_r_time = SystemtimeClock;
	}
	else if (turn_direction_detection == L_2_R_STATE)//左到右了
	{
		if ((p->angular_v[Z_P] >= L_2_R_TRIGGER_RIGHT_SIDE_DEG_SP_5) && (p->angular_v[Z_P] <= L_2_R_TRIGGER_LEFT_SIDE_DEG_SP_3))
		{
			if ((SystemtimeClock - action_l_2_r_time) >= GO_STRAIGHT_DETECTION_KEEP_TIME)
			{
				turn_direction_detection = L_2_R_2_M_OVER_STATE;
#if 0
				gcz_serial_v1_dma_printf(SERIAL_UART1,"4.1 [L_2_R_STATE]:trun to [L_2_R_2_M_OVER_STATE]\r\n");
#endif
			}
		}
		else if (p->angular_v[Z_P] > TRUN_LEFT_TRIGGER_DEG_SP_1)
		{
			turn_direction_detection = START_L_STATE;
			action_l_2_r_time = SystemtimeClock;
#if 0
			gcz_serial_v1_dma_printf(SERIAL_UART1,"4.2 [L_2_R_STATE]:trun to [START_L_STATE]\r\n");
#endif
		}
		else if (p->angular_v[Z_P] < TRUN_RIGHT_TRIGGER_DEG_SP_6)
		{
			if ((SystemtimeClock - action_l_2_r_time) >= TRUN_RIGHT_DECTION_TIME)
			{
				turn_direction_detection = TURN_R_STATE;
				action_l_2_r_time = SystemtimeClock;
#if 0
				gcz_serial_v1_dma_printf(SERIAL_UART1,"4.3 [L_2_R_STATE]:trun to [TURN_R_STATE]\r\n");
#endif
			}
		}
		else
		{
			turn_direction_detection = L_2_R_STATE;
			action_l_2_r_time = SystemtimeClock;
		}
	}
	else if (turn_direction_detection == TURN_R_STATE)//右转
	{
		if ((p->angular_v[Z_P] >= L_2_R_TRIGGER_RIGHT_SIDE_DEG_SP_5) && (p->angular_v[Z_P] <= L_2_R_TRIGGER_LEFT_SIDE_DEG_SP_3))
		{
			if ((SystemtimeClock - action_l_2_r_time) >= GO_STRAIGHT_DETECTION_KEEP_TIME)
			{
				turn_direction_detection = M_STATE;
#if 0
				gcz_serial_v1_dma_printf(SERIAL_UART1,"5.1 [TURN_R_STATE]:trun to [M_STATE]\r\n");
#endif
			}
		}
		else if (p->angular_v[Z_P] > TRUN_LEFT_TRIGGER_DEG_SP_1)
		{
			turn_direction_detection = START_L_STATE;
			action_l_2_r_time = SystemtimeClock;
#if 0
			gcz_serial_v1_dma_printf(SERIAL_UART1,"5.2 [TURN_R_STATE]:trun to [START_L_STATE]\r\n");
#endif
		}
		else
		{
			action_l_2_r_time = SystemtimeClock;
		}
	}
	else if (turn_direction_detection == TURN_L_STATE)
	{
		if (p->angular_v[Z_P] < L_2_M_TRIGGER_LEFT_SIDE_DEG_SP_2)
		{
			turn_direction_detection = L_2_M_STATE;
			action_l_2_r_time = SystemtimeClock;
#if 0
			gcz_serial_v1_dma_printf(SERIAL_UART1,"6.1 [TURN_L_STATE]:trun to [L_2_M_STATE]\r\n");
#endif
		}
		else if (p->angular_v[Z_P] >= TRUN_LEFT_TRIGGER_DEG_SP_1)
		{
			if ((SystemtimeClock - action_l_2_r_time) >= GO_STRAIGHT_DETECTION_KEEP_TIME)
			{
				turn_direction_detection = TURN_L_STATE;
				action_l_2_r_time = SystemtimeClock;
#if 0
				gcz_serial_v1_dma_printf(SERIAL_UART1,"6.2 [TURN_L_STATE]:keep [TURN_L_STATE]\r\n");
#endif
			}
		}
		else
		{
			action_l_2_r_time = SystemtimeClock;
		}
	}
	else
	{
		turn_direction_detection = IDLE_STATE;
	}

	static uint8_t result_judge = IDLE_STATE;
	if ((turn_direction_detection == M_STATE) || (turn_direction_detection == IDLE_STATE))
		res = GO_STAIGHT;
	else if (turn_direction_detection == TURN_R_STATE)
		res = TURN_RIGHT;
	else if (turn_direction_detection == TURN_L_STATE)
		res = TURN_LEFT;
	else
	{
		if (result_judge == IDLE_STATE)
			result_judge = turn_direction_detection;
		if ((result_judge == L_2_R_STATE) && (turn_direction_detection == L_2_R_2_M_OVER_STATE))
		{
			res = LEFT_SIDE_OVERTAKE;
			result_judge = IDLE_STATE;
		}
		else
			res = LEFT_SIDE_OVERTAKE_JUDGEING;
		if (result_judge != IDLE_STATE)
			result_judge = turn_direction_detection;
	}

	return res;
}

enum DRIVER_BRAKE_STATE {
  SRR_ENTRY_STATE = 0, DRIVER_BRAKE_STATE, DRIVER_BRAKE_KEEP, DRIVER_BRAKE_CANCEL
};

float AngleRadar_TTC_Break(_VEHICLE_PARA* stVehicleParas)
{
	float bsd_ttc_dec_out = 0.0;
	struct SRRObstacle* srrObstacle;
	static uint8_t DriverBrakeState = 0;
	static uint8_t brakeFlagCount = 0;

	float filted_dec_out = 0.f;
	SRR_Obstacle_Paras frontParas;
	SRR_Obstacle_Paras sideParas;
	SRR_Obstacle_Paras rearParas;

	//assign the srr output parameter
	srrOutputParas.id = 255;
	srrOutputParas.decelerationOutput = 0.0;
	srrOutputParas.warningZone = 0;
	srrOutputParas.warningLevel = 0;
	srrOutputParas.valid = 0;
	srrOutputParas.levelOneValid = false;
	srrOutputParas.levelTwoValid = false;
	srrOutputParas.levelThreeValid = false;
	srrOutputParas.motion_state = UNKNOWN_MOTION_STATE;
	srrOutputParas.ttc_x  = 0.f;
	srrOutputParas.ttc_y  = 0.f;
	srrOutputParas.dist_x = 0.f;
	srrOutputParas.dist_y = 0.f;
	srrOutputParas.vel_x  = 0.f;
	srrOutputParas.vel_y  = 0.f;

	srrMalfunction_ = 0;

	JUDGE_RESULT res = overtake_conditional_filter();//获取当前转弯的状态
	//20220924 yuhong 如果司机打右转向，立即判定为右转；否则还是依据与上面的函数进行转弯的判断
	if(stVehicleParas->RightFlag==1){
		res = TURN_RIGHT;
	}
//	if ((res == LEFT_SIDE_OVERTAKE) || (res == LEFT_SIDE_OVERTAKE_JUDGEING) || (res == TURN_LEFT))
//		return 0.0;
//	if (res == GO_STAIGHT)
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"------OVERERTAKE_state:%s\r\n","GO_STAIGHT");
//	else if (res == LEFT_SIDE_OVERTAKE_JUDGEING)
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"------OVERERTAKE_state:%s\r\n","L_OT_JUDGEING");
//	else if (res == LEFT_SIDE_OVERTAKE)
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"------OVERERTAKE_state:%s\r\n","LEFT_SIDE_OVERTAKE");
//	else if (res == TURN_LEFT)
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"------OVERERTAKE_state:%s\r\n","TURN_LEFT");
//	else if (res == TURN_RIGHT)
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"------OVERERTAKE_state:%s\r\n","TURN_RIGHT");

	//fprintf(USART1_STREAM,"Malfunction=%d\r\n",1);
	//AEB制动开关关闭,打开双闪以及司机刹车，则角雷达不起作用
	if(0x00 == g_AEB_CMS_Enable ||((stVehicleParas->LeftFlag==0x01) &&
	    (stVehicleParas->RightFlag==0x01)))
//		||(stVehicleParas->BrakeFlag==0x01))
	{
		if (g_b_srr_log_key)
		{
			gcz_serial_v1_dma_printf(SERIAL_UART1,"g_AEB_CMS_Enable=%d\r\n",g_AEB_CMS_Enable);
		}
		return bsd_ttc_dec_out;
	}

	if(true != srr_demo_sw && FUC_NORMAL != g_imu_detection_result){
	  if (g_b_srr_log_key)
    {
      gcz_serial_v1_dma_printf(SERIAL_UART1,"g_imu_detection_result=%d\r\n",g_imu_detection_result);
    }
	  return bsd_ttc_dec_out;
	}

	//fprintf(USART1_STREAM,"g_srr_brake=%d, and stVehicleParas->BrakeFlag = %d\r\n",g_srr_brake, stVehicleParas->BrakeFlag);
	//yuhong 20220909 司机刹车时，角雷达盲区监测不进行制动及报警
	if(g_srr_brake==FALSE
		&&stVehicleParas->BrakeFlag==TRUE
		&&DriverBrakeState==SRR_ENTRY_STATE)//只在角雷达没有制动输出，而此时司机踩刹车(也就是刹车灯亮了)，此时才判定司机在制动
	{
		g_last_driver_brake_time = SystemtimeClock;
		DriverBrakeState = DRIVER_BRAKE_STATE; //设置不进入角雷达策略
		if (g_b_srr_log_key)
			gcz_serial_v1_dma_printf(SERIAL_UART1,"AngleRadar_TTC_Break=%d\r\n",2);
	}
	else if(DriverBrakeState!=DRIVER_BRAKE_STATE)
	{
		DriverBrakeState = SRR_ENTRY_STATE; //设置进入角雷达策略
	}
	else
	{
	}

  //fprintf(USART1_STREAM,"Malfunction=%d\r\n",2);
//	DriverBrakeState = 0;
	if(DriverBrakeState==SRR_ENTRY_STATE)
	{
		//获取障碍物信息
		srrObstacle = GetSrrObsIns();

		if(srrObstacle[0].is_cipv==false
			&&srrObstacle[1].is_cipv==false
			&&srrObstacle[2].is_cipv==false)
		{
		  //fprintf(USART1_STREAM,"AngleRadar_TTC_Break=%d\r\n",3);
			return bsd_ttc_dec_out;
		}

		//USART1_Bebug_Print_Num("srrObstacle ID =",1,1);

		if(srrObstacle[0].malfunction!=0
			||srrObstacle[1].malfunction!=0
			||srrObstacle[2].malfunction!=0)
		{
			//malfunction assign value
			srrMalfunction_ = calculateAngleRadarMalfunction(srrObstacle[0].malfunction,srrObstacle[1].malfunction,srrObstacle[2].malfunction);
			USART1_Bebug_Print_Num("Malfunction=",srrMalfunction_,1,1);
			return bsd_ttc_dec_out;
		}
//	    for (int i = 0; i < 3; i++) {
//	    	gcz_serial_v1_dma_printf(SERIAL_UART1,"srr_obj_ID : %d ", srrObstacle[i].id);
//	    	gcz_serial_v1_dma_printf(SERIAL_UART1,"srr_obj_alive_no : %d ", srrObstacle[i].tracking_num);
//	    	gcz_serial_v1_dma_printf(SERIAL_UART1,"warningZone :%d ", obstacleDirectionMatch(srrObstacle[i].zone));
//	    	gcz_serial_v1_dma_printf(SERIAL_UART1,"motion_state :%d ", srrObstacle[i].motion_state);
//	    	gcz_serial_v1_dma_printf(SERIAL_UART1," dist x : %.2f, dist y : %.2f ttc x : %.2f, ttc y : %.2f  v x : %.2f, v y : %.2f\n",srrObstacle[i].dist.x,srrObstacle[i].dist.y,srrObstacle[i].ttc.x,srrObstacle[i].ttc.y,srrObstacle[i].vel.x,srrObstacle[i].vel.y);
//	    }


		//静止物体的判定

		for(int i=0;i<2;i++)
		{
			//save&update the motion state and the obstacle ID
			if(srrObstacle[i].motion_state==UNKNOWN_MOTION_STATE||srrObstacle[i].motion_state==STANDING_MOTION_STATE)
			{
				//状态不是运动或者停止状态，则循环继续
				standing_switch[i]=0;
				continue;
			}

			if(i==1
				&&srrObstacle[i].zone==RIGHT_MIDDLE_CAR_ZONE
				&&srrObstacle[i].id==last_obstacle_id[0]
				&&srrObstacle[i].motion_state==STOPED_MOTION_STATE
				&&standing_switch[0] == 1)
			{

				//gcz_serial_v1_dma_printf(SERIAL_UART1,"SrrObstacle[%d].id is %d, and the front last_obstacle_id is %d.", i, srrObstacle[i].id, last_obstacle_id[0]);
				//右前的障碍物已经被判定为静止状态之后，自车前行，障碍物出现在右侧，而且其ID未变化且运动状态为停止
				srrObstacle[i].motion_state = STANDING_MOTION_STATE;
				standing_switch[i] = 1;
				continue;
			}


			if(standing_switch[i]==1
				&&last_obstacle_id[i]==srrObstacle[i].id
				&&srrObstacle[i].motion_state==STOPED_MOTION_STATE)
			{
				//gcz_serial_v1_dma_printf(SERIAL_UART1,"Need to assign srrObstacle[%d].motion state to standing.", i);
				srrObstacle->motion_state = STANDING_MOTION_STATE;
			}
			else
			{
				last_motion_state[i] = srrObstacle[i].motion_state;
				last_obstacle_id[i]  = srrObstacle[i].id;
				standing_switch[i]   = 0;
				obstacleStopedMotionSwitch(&srrObstacle[i],last_obstacle_id[i],last_motion_state[i],&standing_switch[i],stopped_object_timestamp[i]);
			}
		}

	  //fprintf(USART1_STREAM,"Malfunction=%d\r\n",2);
		//for the anther strategy
		//############################################################################################################################
		/*  -------------------------------------------------------------------------------------------------------------------------|
			|                                          RIGHT_FRONT_CAR_ZONE                                                          |
			|------------------------------------------------------------------------------------------------------------------------|
			|                                          RIGHT_MIDDLE_CAR_ZONE                                                         |
			|------------------------------------------------------------------------------------------------------------------------|
			|                                          RIGHT_BACK_CAR_ZONE                                                           |
			--------------------------------------------------------------------------------------------------------------------------
		*/
		//############################################################################################################################
		if(srrObstacle[0].id!=255)
		{
			rightFrontCarZoneBrake(&srrObstacle[0], &frontParas, stVehicleParas,res);
		}
		else
		{
			setObstacleParasToDefault(&frontParas);
		}

		if(srrObstacle[1].id!=255)
		{
			rightSideCarZoneBrake(&srrObstacle[1], &sideParas, stVehicleParas,res);
		}
		else
		{
			setObstacleParasToDefault(&sideParas);
		}

		if(srrObstacle[2].id!=255)
		{
			rightRearCarZoneBrake(&srrObstacle[2], &rearParas, stVehicleParas);
		}
		else
		{
			setObstacleParasToDefault(&rearParas);
		}

	  //first we need judge the deceleration value
	  if(frontParas.decelerationOutput > 0.f &&
		 frontParas.decelerationOutput > sideParas.decelerationOutput)
	  {
		bsd_ttc_dec_out = frontParas.decelerationOutput;
		//assign the srr output parameter
		srrOutputParas.id = frontParas.id;
		srrOutputParas.decelerationOutput = frontParas.decelerationOutput;
		srrOutputParas.warningZone = frontParas.warningZone;
		srrOutputParas.warningLevel = frontParas.warningLevel;
		srrOutputParas.valid = 1;
		srrOutputParas.motion_state = frontParas.motion_state;
		srrOutputParas.vel_x = frontParas.vel_x;
		srrOutputParas.vel_y = frontParas.vel_y;
		srrOutputParas.ttc_x = frontParas.ttc_x;
		srrOutputParas.ttc_y = frontParas.ttc_y;
		srrOutputParas.dist_x = frontParas.dist_x;
		srrOutputParas.dist_y = frontParas.dist_y;
	  }
	  else if(sideParas.decelerationOutput > 0.0)
	  {
		bsd_ttc_dec_out = sideParas.decelerationOutput;
		//assign the srr output parameter
		srrOutputParas.id = sideParas.id;
		srrOutputParas.decelerationOutput = sideParas.decelerationOutput;
		srrOutputParas.warningZone = sideParas.warningZone;
		srrOutputParas.warningLevel = sideParas.warningLevel;
		srrOutputParas.valid = 1;
		srrOutputParas.motion_state = sideParas.motion_state;
		srrOutputParas.vel_x = sideParas.vel_x;
		srrOutputParas.vel_y = sideParas.vel_y;
		srrOutputParas.ttc_x = sideParas.ttc_x;
		srrOutputParas.ttc_y = sideParas.ttc_y;
		srrOutputParas.dist_x = sideParas.dist_x;
		srrOutputParas.dist_y = sideParas.dist_y;
	  }
	  else if(sideParas.warningLevel==1)  //warning level judgement
	  {
		bsd_ttc_dec_out = 0.0;
		//assign the srr output parameter
		srrOutputParas.id = sideParas.id;
		srrOutputParas.decelerationOutput = sideParas.decelerationOutput;
		srrOutputParas.warningZone = sideParas.warningZone;
		srrOutputParas.warningLevel = sideParas.warningLevel;
		srrOutputParas.valid = 1;
		srrOutputParas.motion_state = sideParas.motion_state;
		srrOutputParas.vel_x = sideParas.vel_x;
		srrOutputParas.vel_y = sideParas.vel_y;
		srrOutputParas.ttc_x = sideParas.ttc_x;
		srrOutputParas.ttc_y = sideParas.ttc_y;
		srrOutputParas.dist_x = sideParas.dist_x;
		srrOutputParas.dist_y = sideParas.dist_y;
	  }
	  else if(frontParas.warningLevel==1)  //warning level judgement
	  {
		bsd_ttc_dec_out = 0.0;
		//assign the srr output parameter
		srrOutputParas.id = frontParas.id;
		srrOutputParas.decelerationOutput = frontParas.decelerationOutput;
		srrOutputParas.warningZone = frontParas.warningZone;
		srrOutputParas.warningLevel = frontParas.warningLevel;
		srrOutputParas.valid = 1;
		srrOutputParas.motion_state = frontParas.motion_state;
		srrOutputParas.vel_x = frontParas.vel_x;
		srrOutputParas.vel_y = frontParas.vel_y;
		srrOutputParas.ttc_x = frontParas.ttc_x;
		srrOutputParas.ttc_y = frontParas.ttc_y;
		srrOutputParas.dist_x = frontParas.dist_x;
		srrOutputParas.dist_y = frontParas.dist_y;
	  }
	  else if(sideParas.warningLevel==2)  //warning level judgement
	  {
		bsd_ttc_dec_out = 0.0;
		//assign the srr output parameter
		srrOutputParas.id = sideParas.id;
		srrOutputParas.decelerationOutput = sideParas.decelerationOutput;
		srrOutputParas.warningZone = sideParas.warningZone;
		srrOutputParas.warningLevel = sideParas.warningLevel;
		srrOutputParas.valid = 1;
		srrOutputParas.motion_state = sideParas.motion_state;
		srrOutputParas.vel_x = sideParas.vel_x;
		srrOutputParas.vel_y = sideParas.vel_y;
		srrOutputParas.ttc_x = sideParas.ttc_x;
		srrOutputParas.ttc_y = sideParas.ttc_y;
		srrOutputParas.dist_x = sideParas.dist_x;
		srrOutputParas.dist_y = sideParas.dist_y;
	  }
	  else if(frontParas.warningLevel==2)  //warning level judgement
	  {
		bsd_ttc_dec_out = 0.0;
		//assign the srr output parameter
		srrOutputParas.id = frontParas.id;
		srrOutputParas.decelerationOutput = frontParas.decelerationOutput;
		srrOutputParas.warningZone = frontParas.warningZone;
		srrOutputParas.warningLevel = frontParas.warningLevel;
		srrOutputParas.valid = 1;
		srrOutputParas.motion_state = frontParas.motion_state;
		srrOutputParas.vel_x = frontParas.vel_x;
		srrOutputParas.vel_y = frontParas.vel_y;
		srrOutputParas.ttc_x = frontParas.ttc_x;
		srrOutputParas.ttc_y = frontParas.ttc_y;
		srrOutputParas.dist_x = frontParas.dist_x;
		srrOutputParas.dist_y = frontParas.dist_y;
	  }
	  else if(rearParas.warningLevel==2)  //warning level judgement
	  {
		bsd_ttc_dec_out = 0.0;
		//assign the srr output parameter
		srrOutputParas.id = rearParas.id;
		srrOutputParas.decelerationOutput = rearParas.decelerationOutput;
		srrOutputParas.warningZone = rearParas.warningZone;
		srrOutputParas.warningLevel = rearParas.warningLevel;
		srrOutputParas.valid = 1;
		srrOutputParas.motion_state = rearParas.motion_state;
		srrOutputParas.vel_x = sideParas.vel_x;
		srrOutputParas.vel_y = sideParas.vel_y;
		srrOutputParas.ttc_x = rearParas.ttc_x;
		srrOutputParas.ttc_y = rearParas.ttc_y;
		srrOutputParas.dist_x = rearParas.dist_x;
		srrOutputParas.dist_y = rearParas.dist_y;
	  }
	  else if(sideParas.warningLevel==3)  //warning level judgement
	  {
		bsd_ttc_dec_out = 0.0;
		//assign the srr output parameter
		srrOutputParas.id = sideParas.id;
		srrOutputParas.decelerationOutput = sideParas.decelerationOutput;
		srrOutputParas.warningZone = sideParas.warningZone;
		srrOutputParas.warningLevel = sideParas.warningLevel;
		srrOutputParas.valid = 1;
		srrOutputParas.motion_state = sideParas.motion_state;
		srrOutputParas.vel_x = sideParas.vel_x;
		srrOutputParas.vel_y = sideParas.vel_y;
		srrOutputParas.ttc_x = sideParas.ttc_x;
		srrOutputParas.ttc_y = sideParas.ttc_y;
		srrOutputParas.dist_x = sideParas.dist_x;
		srrOutputParas.dist_y = sideParas.dist_y;
	  }
	  else if(frontParas.warningLevel==3)  //warning level judgement
	  {
		bsd_ttc_dec_out = 0.0;
		//assign the srr output parameter
		srrOutputParas.id = frontParas.id;
		srrOutputParas.decelerationOutput = frontParas.decelerationOutput;
		srrOutputParas.warningZone = frontParas.warningZone;
		srrOutputParas.warningLevel = frontParas.warningLevel;
		srrOutputParas.motion_state = frontParas.motion_state;
		srrOutputParas.valid = 1;
		srrOutputParas.vel_x = frontParas.vel_x;
		srrOutputParas.vel_y = frontParas.vel_y;
		srrOutputParas.ttc_x = frontParas.ttc_x;
		srrOutputParas.ttc_y = frontParas.ttc_y;
		srrOutputParas.dist_x = frontParas.dist_x;
		srrOutputParas.dist_y = frontParas.dist_y;
	  }
	  else if(rearParas.warningLevel==3)  //warning level judgement
	  {
		bsd_ttc_dec_out = 0.0;
		//assign the srr output parameter
		srrOutputParas.id = rearParas.id;
		srrOutputParas.decelerationOutput = rearParas.decelerationOutput;
		srrOutputParas.warningZone = rearParas.warningZone;
		srrOutputParas.warningLevel = rearParas.warningLevel;
		srrOutputParas.valid = 1;
		srrOutputParas.motion_state = rearParas.motion_state;
		srrOutputParas.vel_x = rearParas.vel_x;
		srrOutputParas.vel_y = rearParas.vel_y;
		srrOutputParas.ttc_x = rearParas.ttc_x;
		srrOutputParas.ttc_y = rearParas.ttc_y;
		srrOutputParas.dist_x = rearParas.dist_x;
		srrOutputParas.dist_y = rearParas.dist_y;
	  }
	  else
	  {
		bsd_ttc_dec_out = 0.0;
		//assign the srr output parameter
		srrOutputParas.id = 255;
		srrOutputParas.decelerationOutput = 0.0;
		srrOutputParas.warningZone = 0;
		srrOutputParas.warningLevel = 0;
		srrOutputParas.valid = 0;
		srrOutputParas.motion_state = UNKNOWN_MOTION_STATE;
		srrOutputParas.vel_x = 0.f;
		srrOutputParas.vel_y = 0.f;
		srrOutputParas.ttc_x = 0.f;
		srrOutputParas.ttc_y = 0.f;
		srrOutputParas.dist_x = 0.f;
		srrOutputParas.dist_y = 0.f;
	  }

	  static int8_t srr_state = 0;
    static int8_t pre_obs_id = 255;
    static int64_t pre_sys_tm = 0;
    static float new_acc = 0.f;
	  float lat_dist = lateralMinDistance( DISTANCE_MIN,stVehicleParas->fVehicleSpeed,
	      g_veh_params->width / 2);

//    gcz_serial_v1_dma_printf(SERIAL_UART1,"srr_state: %d, srrObstacle[0].id : %d, decelerationOutput : %.2f，"
//        "stVehicleParas.rightFlag :%d\n",
//        srr_state, srrObstacle[0].id, frontParas.decelerationOutput, stVehicleParas->RightFlag);

    if(srrOutputParas.decelerationOutput > 0.f && 0 == srr_state){
      new_acc = bsd_ttc_dec_out;
      pre_obs_id = srrOutputParas.id;
    }

    if(1 == srr_state){
        if(pre_obs_id != srrOutputParas.id || bsd_ttc_dec_out < FLT_EPSILON){
          srr_state = 0;
          pre_obs_id = 255;
          bsd_ttc_dec_out = 0.f;
          //USART1_Bebug_Print("srr", "break off braking", 1);

        }
    }

    struct ImuData s_imuData;
    GetImuData(&s_imuData);

//    if (g_b_srr_log_key){
//      gcz_serial_v1_dma_printf(SERIAL_UART1,"x_acc : %.2f, z_deg_sp : %2f \r\n",
//          s_imuData.x_acc, s_imuData.z_deg_sp);
//    }

    if((1 == srrOutputParas.warningZone && srrOutputParas.vel_x > 1.f) ||
        (2 == srrOutputParas.warningZone && !InSafeLateralDistance(
            g_veh_params->front_to_rear_axle, lat_dist, srrOutputParas.dist_x,
            srrOutputParas.dist_y, s_imuData.z_deg_sp))){
      if(pre_obs_id == srrOutputParas.id){
         srr_state = 0;
         pre_obs_id = 255;
         bsd_ttc_dec_out = 0.f;
               //USART1_Bebug_Print("srr", "break off braking", 1);

      }
    }

    if((srrOutputParas.decelerationOutput > 0.f && 0 == srr_state) || 0 != srr_state){
      Srr_BrakingState(stVehicleParas->fVehicleSpeed, SystemtimeClock, &pre_sys_tm, &srr_state, &new_acc);
      bsd_ttc_dec_out = new_acc;
    }

	  Srr_FilterDeceleration(&bsd_ttc_dec_out, &filted_dec_out);

	  //srrOutputParas.decelerationOutput = filted_dec_out;
	  if(filted_dec_out>0.f)
	  {
		  g_srr_brake = TRUE;
	  }
	  else
	  {
		  g_srr_brake = FALSE;
	  }
//	  if(srrOutputParas.warningLevel >0){
//	  	gcz_serial_v1_dma_printf(SERIAL_UART1,"Vehicle speed : %.2f, warningZone : %d, warningLevel : %d, motion_state : %d, "
//	  		"bsd_ttc_dec_out : %.2f, filted_dec_out : %.2f , ttc x : %.2f, ttc y : %.2f, "
//	  		"dist x : %.2f, dist y : %.2f\n",
//	  		stVehicleParas->fVehicleSpeed ,srrOutputParas.warningZone,
//	  		srrOutputParas.warningLevel, srrOutputParas.motion_state,
//	  		bsd_ttc_dec_out, filted_dec_out, srrOutputParas.ttc_x, srrOutputParas.ttc_y,
//	  		srrOutputParas.dist_x, srrOutputParas.dist_y);
//	  	}
	}//DriverBrakeState==0
	else if(DriverBrakeState==DRIVER_BRAKE_STATE)//司机刹车的状态下, 不进入到角雷达刹车策略,大于5000ms之后将刹车状态赋值为SRR_ENTRY_STATE
	{
		if(SystemtimeClock - g_last_driver_brake_time >5000)
		{
			//gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR","########Cancel DRIVER_BRAKE_STATE after 5 seconds.",1);
			DriverBrakeState = SRR_ENTRY_STATE;
		}
	}
	else
	{
		//DriverBrakeState状态为未知状态，则将DriverBrakeState初始化状态为SRR_ENTRY_STATE
		DriverBrakeState = SRR_ENTRY_STATE;
	}

	//yuhong 20220905 添加声光报警器需求 -报警等级为1或者右转灯打开
	if(srrOutputParas.warningLevel==1||stVehicleParas->RightFlag==1)
	{
	  Set_Brake_Negative_Control_Value(Bit_SET);
	}
	else
	{
	  Set_Brake_Negative_Control_Value(Bit_RESET);
  }

	if (g_b_srr_log_key)
	{
		if(srrOutputParas.warningLevel >0 || filted_dec_out >0)
		{
			gcz_serial_v1_dma_printf(SERIAL_UART1,"Veh v : %.2f, Zone : %d, Level : %d, state : %d, ",
										 stVehicleParas->fVehicleSpeed ,srrOutputParas.warningZone,srrOutputParas.warningLevel, srrOutputParas.motion_state);
			gcz_serial_v1_dma_printf(SERIAL_UART1,"dec : %.2f ,",filted_dec_out);
			gcz_serial_v1_dma_printf(SERIAL_UART1,"vx : %.2f, vy : %.2f, ",srrOutputParas.vel_x, srrOutputParas.vel_y);
			gcz_serial_v1_dma_printf(SERIAL_UART1,"tx : %.2f, ty : %.2f, dx : %.2f, dy : %.2f\n",srrOutputParas.ttc_x, srrOutputParas.ttc_y,srrOutputParas.dist_x, srrOutputParas.dist_y);

		  	struct ImuData s_imuData;
		  	s_imuData.x_acc 				  = 0.0;
		  	s_imuData.y_deg 				  = 0.0;
		  	s_imuData.z_deg_sp 				  = 0.0;
		  	//get Imu data for the yaw rate
		  	GetImuData(&s_imuData);
			gcz_serial_v1_dma_printf(SERIAL_UART1, "Imu deg_sp : %.2f, acc : %0.2f\r\n", s_imuData.z_deg_sp, s_imuData.x_acc*9.8);
		}
	}

	return filted_dec_out;
}

#define CMS_FILTER_LEN   5
float ttc_dd_dec;
float ttc_dec;
float ttc_feel;
float filter_cms_ttc[CMS_FILTER_LEN] = {0};
float CMS_TTC_Break(Obstacle_Information* cipv,
					Obstacle_Basic_Data* obs_basic_data,
					_VEHICLE_PARA* stVehicleParas,
					Camera_Essential_Data* cam_essiential,
					URADER_MESSAGE Urader_Company_Message[]){
	//TTC_CMS
	//float speed_ego = speed_meter_p_s(stVehicleParas.fVehicleSpeed);
	cms_break_state.cam_distance = (*cipv).DistanceZ;
	// TTC_CMS Switch judge
	if((!g_AEB_CMS_Enable)||(!g_CMS_TTC_Enable)){
		return 0.0;
	}


	//fprintf(USART1_STREAM,"CMS_TTC_Brake_Force_Feel_Para=%f\r\n",rParms.CMS_TTC_Brake_Force_Feel_Para);
	if(((rParms.CMS_TTC_Brake_Time_Thr) >= (*cipv).TTC)&&((*cipv).TTC > 0)&&((*cipv).RelativeSpeedZ > 0)){
		ttc_feel = rParms.CMS_TTC_Brake_Force_Feel_Para;
		ttc_feel = ttc_feel*(*cipv).DistanceZ/(*cipv).RelativeSpeedZ;
		if((rParms.CMS_TTC_Brake_Force_Feel_Para > MIN_TIME_FORCE_FEEL)&&(ttc_feel > 0)){
       ttc_dec = (1.0/ttc_feel) * ((*cipv).RelativeSpeedZ);
       cms_break_state.cms_ttc_dec_out = ttc_dec;

		  //max(ttc_dd_dec,ttc_dec); // Very Emergency
			/*********************************************
			 if(ttc_cms_set_hmw_thr == 0 && dec_output < 0.0001 && cms_break_state.cms_hmw_dec_out < 0.0001){
				if(dynamic_hmw_thr < (*cipv).HMW){
					fprintf(USART1_STREAM,"cms_break_state.cms_hmw_dec_out=%0.2f,dec_output=%0.2f,dynamic_hmw_thr=%0.2f,HMW=%0.2f\r\n",cms_break_state.cms_hmw_dec_out,dec_output,dynamic_hmw_thr,(*cipv).HMW);
					dynamic_hmw_thr = (*cipv).HMW;
					ttc_cms_set_hmw_thr = 1;
					ttc_cms_set_hmw_thr_count = 0;
				}
				//fprintf(USART1_STREAM,"dynamic_hmw_thr=%0.2f\r\n",dynamic_hmw_thr);
			}
			***********************************************/

			//cms_break_state.cms_ttc_dec_out = (1.0/rParms.CMS_TTC_Brake_Force_Feel_Para) * ((*cipv).RelativeSpeedZ - (cms_break_state.cam_distance / rParms.CMS_TTC_Brake_Time_Thr));
		}
		else{
			cms_break_state.cms_ttc_dec_out = 0.0;
			//ttc_cms_set_hmw_thr = 0;
		}
		//cms_break_state.cms_ttc_dec_out = cms_break_state.cms_ttc_dec_out / AEB_CMS_parameters.ttc_thr_set;
		if(cms_break_state.cms_ttc_dec_out > AEB_CMS_parameters.ttc_max_dec_output){
			cms_break_state.cms_ttc_dec_out = AEB_CMS_parameters.ttc_max_dec_output;
			if(LOGLEVEL == LOG_WARNING){
				Log_print(LOG_WARNING, 0x08,0x02,"TTC_CMS too big dec", "CMS_TTC_Break", cipv, stVehicleParas, cam_essiential,obs_basic_data);
			}
		}else if(cms_break_state.cms_ttc_dec_out < 0.0){
			cms_break_state.cms_ttc_dec_out = 0.0;
		}
		Filter_noise(&cms_break_state.cms_ttc_dec_out,cms_break_state.cms_ttc_dec_out_prev, &cms_break_state.cms_is_break_flag,&cms_break_state.cms_cam_delay_count, &cms_break_state.cms_cam_keep_count);
	}
	else{//相机有输出，但是，无需刹车
			cms_break_state.cms_cam_delay_count = 0;
			cms_break_state.cms_ttc_dec_out = 0.0;
		}

	if(cms_break_state.cms_ttc_dec_out > 0.0){
		cms_break_state.cms_is_break_flag = 0x01;
		cms_break_state.cms_ttc_dec_out_prev = cms_break_state.cms_ttc_dec_out;
	}else{
		cms_break_state.cms_is_break_flag = 0x00;
	}

	const int max_filter_len = CMS_FILTER_LEN -1;
	for(int i = 0; i < max_filter_len; i++){
	  filter_cms_ttc[i] = filter_cms_ttc[i + 1];
	}
	filter_cms_ttc[max_filter_len] = cms_break_state.cms_ttc_dec_out;

	float max_dec = filter_cms_ttc[0];
	for(int i = 1; i < CMS_FILTER_LEN; i++){
	  if(filter_cms_ttc[i] > max_dec){
	    max_dec = filter_cms_ttc[i];
	  }
	}

	cms_break_state.cms_ttc_dec_out = max_dec;
	return cms_break_state.cms_ttc_dec_out;
}
float ttc;

static float hmw_start_dv = 0;
float dec_cal_for_cms = 2.0;
float CMS_HMW_Break(Obstacle_Information* cipv,
					Obstacle_Basic_Data* obs_basic_data,
					_VEHICLE_PARA* stVehicleParas,
					Camera_Essential_Data* cam_essiential,
					URADER_MESSAGE Urader_Company_Message[],
					uint8_t Approaching){
	//HMW_CMS
	cms_break_state.cam_distance = (*cipv).DistanceZ;
	// HMW_CMS Switch judge
	if((!g_AEB_CMS_Enable)||(!g_CMS_HMW_Enable)){
		return 0.0;
	}

	//if((((*cipv).HMW < 6.0))&((*cipv).HMW > 0)&((*cipv).HMW < (AEB_CMS_parameters.hmw_thr_set + AEB_CMS_parameters.Air_Break_delay_time))&(AEB_CMS_parameters.hmw_thr_set > 0)){
	if((((*cipv).HMW < 6.0))&((*cipv).HMW > 0)&((*cipv).HMW < (dynamic_hmw_thr + rParms.Air_Brake_Delay_Time))&(dynamic_hmw_thr > 0)){
		if(cipv->RelativeSpeedZ > 0)
			ttc = cipv->DistanceZ/cipv->RelativeSpeedZ;
		//////////////////////////////////////////////
		// Processing the start braking hmw < Hmw_set
		//////////////////////////////////////////////
		//Approaching用于判断是否有接近目标车辆，判断方法，3帧图像信息计算相对距离是否减小，则认为靠近，参数赋值为0x01
		//hmw_come_in应对突然出现在车前方面的车辆时，如果提前判断了已靠近的状态，且又是第一次触发该状态，则进入后面的HMW篡改判断
		//HMW篡改原则：HMW动态计算若未1.0时，此时前方车辆突然出现，相机给出的HMW为0.8，则使用相机的HMW作为触发条件。
		if(Approaching == 0x01 && hmw_come_in == false){
			if(hmw_come_in == false && cms_break_state.cms_hmw_dec_out < 0.00001){
				if(dynamic_hmw_thr > (*cipv).HMW && cms_break_state.cms_hmw_dec_out < 0.00001 && dec_output < 0.0000001){
					if(ttc_cms_set_hmw_thr == 0 ){
						dynamic_hmw_thr = (*cipv).HMW;// if not,the start dec very big;
					}
				}
			}
			hmw_start_dv = (*cipv).RelativeSpeedZ;
			dec_cal_for_cms = hmw_start_dv*0.1 + target_cms_dec;
			hmw_come_in = true;
		}

		/////////////////////////////////////////////end
		//cms_break_state.cms_hmw_dec_out = AEB_CMS_parameters.hmw_force_set * (ego_speed - (cms_break_state.cam_distance / dynamic_hmw_thr));// NO effective about break delay time
		if( rParms.CMS_HMW_Brake_Force_Feel_Para > MIN_TIME_FORCE_FEEL){
			//cms_break_state.cms_hmw_dec_out = (1.0/(rParms.CMS_HMW_Brake_Force_Feel_Para*ttc)) * (ego_speed - (cms_break_state.cam_distance / (dynamic_hmw_thr)));// pre cal dec ,make sure that the driver can fell the break first time;
			//cms_break_state.cms_hmw_dec_out = (1.0/rParms.CMS_HMW_Brake_Force_Feel_Para) * (ego_speed - (cms_break_state.cam_distance / (dynamic_hmw_thr + rParms.Air_Brake_Delay_Time)));// pre cal dec ,make sure that the driver can fell the break first time;
			float ddv = (ego_speed - (cms_break_state.cam_distance / (dynamic_hmw_thr)));
			float new_feel = (rParms.CMS_HMW_Brake_Force_Feel_Para)/dynamic_hmw_thr;//gcz 2022-08-03  取消/dynamic_hmw_thr，从而使得体感调节与实际一致
			if(new_feel > hmw_brake_ttc_feel){
				new_feel = hmw_brake_ttc_feel;
			}
			cms_break_state.cms_hmw_dec_out = (1.0/new_feel) * ddv;// pre cal dec ,make sure that the driver can fell the break first time;

			if((rParms.switch_g.Retract_Brake_Enable == 0x01) && ((*cipv).RelativeSpeedZ*retract_brake_coe <= ddv)){
				cms_break_state.cms_hmw_dec_out = (1.0/new_feel) * ((*cipv).RelativeSpeedZ *retract_brake_coe);// pre cal dec ,make sure that the driver can fell the break first time;
				retract_brake_flag = 0x01;
			}
			else
				retract_brake_flag = 0x00;
			//fprintf(USART1_STREAM,"R_v=%0.2f,d_z=%0.2f,v=%0.2f,cms_hmw_dec_out=%0.2f,dynamic_hmw_thr=%0.2f,ttc=%0.2f,TTC=%0.2f\r\n",(*cipv).RelativeSpeedZ,cms_break_state.cam_distance,ego_speed,cms_break_state.cms_hmw_dec_out,dynamic_hmw_thr,ttc,cipv->TTC);
			//cms_break_state.cms_hmw_dec_out = cms_break_state.cms_hmw_dec_out / AEB_CMS_parameters.hmw_thr_set;
		}
		else {
			cms_break_state.cms_hmw_dec_out = 0.0;
		}

		if(cms_break_state.cms_hmw_dec_out > AEB_CMS_parameters.Vehicle_max_react_dec){
			cms_break_state.cms_hmw_dec_out = AEB_CMS_parameters.Vehicle_max_react_dec;
			if(LOGLEVEL == LOG_WARNING){
				Log_print(LOG_WARNING,0x06,0x01, "hmw_CMS too big dec", "CMS_HMW_Break", cipv,stVehicleParas,cam_essiential,obs_basic_data);
			}
		}else if(cms_break_state.cms_hmw_dec_out < 0.0){
			cms_break_state.cms_hmw_dec_out = 0.0;
			//cms_break_state.cms_break_flag = 0;
		}else if((*cipv).HMW > dynamic_hmw_thr){
			cms_break_state.cms_hmw_dec_out = 0.0;
		}
		Filter_noise(&cms_break_state.cms_hmw_dec_out,cms_break_state.cms_hmw_dec_out_prev,&cms_break_state.cms_is_break_flag,&cms_break_state.cms_cam_delay_count, &cms_break_state.cms_cam_keep_count);
		cms_break_state.cms_cam_over_timestamp = SystemtimeClock;// if the hmw is ok(samll than thr and small than 6.0, improve that the target was detected, if lost,this time stamp will recored the last timestamp)
	}else if((*cipv).HMW > 6.0){//相机输出无效0x3f 6.3
		cms_break_state.cms_cam_delay_count = 0;
		//先判断超声波有没有数据
		cms_break_state.uradar_distance = Get_Uradar_distance(Urader_Company_Message, stVehicleParas);
		//有数据,使用雷达数据
		// before use the uradar data , must confirmed that the camera lost the target just now
		if(((SystemtimeClock - cms_break_state.cms_cam_over_timestamp)/1000) < time_gap_cam_to_radar){
			if((cms_break_state.uradar_distance < 4.5)&((cms_break_state.uradar_distance > 0.0))){
				if( rParms.CMS_HMW_Brake_Force_Feel_Para > MIN_TIME_FORCE_FEEL){
					cms_break_state.cms_hmw_dec_out = (1.0/rParms.CMS_HMW_Brake_Force_Feel_Para) * (ego_speed - (cms_break_state.uradar_distance / dynamic_hmw_thr));
					//cms_break_state.cms_hmw_dec_out = cms_break_state.cms_hmw_dec_out / dynamic_hmw_thr;
				}
				else {
					cms_break_state.cms_hmw_dec_out = 0.0;
				}

				if(cms_break_state.cms_hmw_dec_out > AEB_CMS_parameters.Vehicle_max_react_dec){
					cms_break_state.cms_hmw_dec_out = AEB_CMS_parameters.Vehicle_max_react_dec;
					if(LOGLEVEL == LOG_WARNING){
						Log_print(LOG_WARNING, 0x07,0x01,"hmw_CMS use Uradar ", "CMS_HMW_Break", cipv, stVehicleParas, cam_essiential,obs_basic_data);
					}
				}else if(cms_break_state.cms_hmw_dec_out < 0.0){
					cms_break_state.cms_hmw_dec_out = 0.0;
					//cms_break_state.cms_break_flag = 0;
				}
				Filter_noise(&cms_break_state.cms_hmw_dec_out,cms_break_state.cms_hmw_dec_out_prev, &cms_break_state.cms_is_break_flag,&cms_break_state.cms_uradar_delay_count, &cms_break_state.cms_uradar_keep_count);

			}else{//雷达无数据，则判断是否需要保持
				cms_break_state.cms_uradar_delay_count = 0;
				cms_break_state.cms_hmw_dec_out = 0.0;
			}
		}else{
			cms_break_state.cms_hmw_dec_out = 0.0;
			Filter_noise(&cms_break_state.cms_hmw_dec_out,cms_break_state.cms_hmw_dec_out_prev, &cms_break_state.cms_is_break_flag,&cms_break_state.cms_uradar_delay_count, &cms_break_state.cms_uradar_keep_count);
		}

	}else{//相机有输出，但是，无需刹车
		cms_break_state.cms_hmw_dec_out = 0.0;
		Filter_noise(&cms_break_state.cms_hmw_dec_out,cms_break_state.cms_hmw_dec_out_prev, &cms_break_state.cms_is_break_flag,&cms_break_state.cms_cam_delay_count, &cms_break_state.cms_cam_keep_count);
		cms_break_state.cms_cam_over_timestamp = SystemtimeClock;// if the hmw is ok(samll than thr and small than 6.0, improve that the target was detected, if lost,this time stamp will recored the last timestamp)
	}

	if(cms_break_state.cms_hmw_dec_out > 0.0){
		cms_break_state.cms_is_break_flag = 0x01;
		cms_break_state.cms_hmw_dec_out_prev = cms_break_state.cms_hmw_dec_out;
	}else{
		cms_break_state.cms_is_break_flag = 0x00;
	}

	return cms_break_state.cms_hmw_dec_out;
}

float CMS_URadar_Break(_VEHICLE_PARA* stVehicleParas,
					URADER_MESSAGE Urader_Company_Message[]){
	// when the camera lost the target
	//先判断超声波有没有数据
	cms_break_state.uradar_distance = Get_Uradar_distance(Urader_Company_Message, stVehicleParas);
	//有数据,使用雷达数据
	// before use the uradar data , must confirmed that the camera lost the target just now
	if(((SystemtimeClock - cms_break_state.cms_cam_over_timestamp)/1000) < time_gap_cam_to_radar){
		if((cms_break_state.uradar_distance < 4.5)&((cms_break_state.uradar_distance > 0.0))){
			uint8_t approaching_ornot = Approaching_to_lead_car_uradar(cms_break_state.uradar_distance, approaching_history_n);
			if(approaching_ornot == 1){
				if( rParms.CMS_HMW_Brake_Force_Feel_Para > MIN_TIME_FORCE_FEEL){
					cms_break_state.cms_uradar_dec_out = (1.0/rParms.CMS_HMW_Brake_Force_Feel_Para) * (ego_speed - (cms_break_state.uradar_distance / dynamic_hmw_thr));
					cms_break_state.cms_uradar_dec_out = cms_break_state.cms_uradar_dec_out / dynamic_hmw_thr;
				}
				else {
					cms_break_state.cms_uradar_dec_out = 0.0;
				}

				if(cms_break_state.cms_uradar_dec_out > AEB_CMS_parameters.Vehicle_max_react_dec){
					cms_break_state.cms_uradar_dec_out = AEB_CMS_parameters.Vehicle_max_react_dec;

				}else if(cms_break_state.cms_uradar_dec_out < 0.0){
					cms_break_state.cms_uradar_dec_out = 0.0;
					//cms_break_state.cms_break_flag = 0;
				}
			}
			else{
				cms_break_state.cms_uradar_delay_count = 0;
				cms_break_state.cms_uradar_dec_out = 0.0;
			}

		}else{//雷达无数据，则判断是否需要保持
			cms_break_state.cms_uradar_delay_count = 0;
			cms_break_state.cms_uradar_dec_out = 0.0;
		}
	}else{
		cms_break_state.cms_uradar_dec_out = 0.0;
	}

	if(cms_break_state.cms_uradar_dec_out > 0.0){
		cms_break_state.cms_is_break_flag = 0x01;
		cms_break_state.cms_uradar_dec_out_prev = cms_break_state.cms_uradar_dec_out;
	}else{
		cms_break_state.cms_is_break_flag = 0x00;
	}

	return cms_break_state.cms_uradar_dec_out;
}

float Cal_TTC(Obstacle_Information* cipv){
	if(cal_ttc_history_cipv_id == (*cipv).ObstacleID){

		if((cal_ttc_count <cal_ttc_history_n)&(cal_ttc_count > 0)){
			cal_ttc_history_time_stamp[cal_ttc_count -1] = SystemtimeClock;
			cal_ttc_history_distance_z[cal_ttc_count -1] = (*cipv).DistanceZ;
			cal_ttc_count = cal_ttc_count + 1;
		}
		if(cal_ttc_count >= cal_ttc_history_n){
			cal_ttc_history_time_stamp[cal_ttc_count -cal_ttc_history_n] = SystemtimeClock;
			cal_ttc_history_distance_z[cal_ttc_count -cal_ttc_history_n] = (*cipv).DistanceZ;
			cal_ttc_count = cal_ttc_count + 1;
			if(cal_ttc_count > (cal_ttc_history_n*2 - 1)){
				cal_ttc_count = cal_ttc_history_n;
			}
			//
			uint8_t pointer_now = cal_ttc_count - 1;
			if(pointer_now < cal_ttc_history_n){
				pointer_now = (cal_ttc_history_n*2 - 1);
			}

			uint8_t pointer_old = cal_ttc_count;

			float d_d = cal_ttc_history_distance_z[pointer_old] - (*cipv).DistanceZ;
			if(d_d <= 0){
				//return -1.0;
				return 6.3;
			}
			float d_t = (SystemtimeClock - cal_ttc_history_time_stamp[pointer_old])/1000.0;
			if(d_t > 0){
				float d_v = d_d/d_t;
				float ttc = (*cipv).DistanceZ / d_v;
				if(ttc > 6.0){
					ttc = 6.3;
				}
				return ttc;
			}
		}
	}else{
		cal_ttc_history_cipv_id = (*cipv).ObstacleID;
		cal_ttc_count = 0;
	}
	return 6.3;//invalied
}



static uint8_t cam_distance_cipvID_approaching_judge = 0x00;
static float average_diatance = 0.0;
static uint32_t time_stamp_cipv_diatance;
uint8_t Approaching_to_target(Circular_Queue* cipv_diantance_queue,Obstacle_Information* cipv){

	if(cam_distance_cipvID_approaching_judge != (*cipv).ObstacleID){
		cam_distance_cipvID_approaching_judge = (*cipv).ObstacleID;
		Clean_Queue(cipv_diantance_queue);
	}else if((*cipv).DistanceZ > 50.0 && cipv_diantance_queue->is_empty == 0x00){
		Clean_Queue(cipv_diantance_queue);
		return 0x00;
	}else if((SystemtimeClock - time_stamp_cipv_diatance) > 1000){
		Clean_Queue(cipv_diantance_queue);
	}
	time_stamp_cipv_diatance = SystemtimeClock;

	In_Queue(cipv_diantance_queue,cipv->DistanceZ);

	if(cipv_diantance_queue->is_full){
		average_diatance = 0.0;
		for(int i = 0;i<cipv_diantance_queue->size_of_queue;i++){
			average_diatance += cipv_diantance_queue->data[i];
		}
		average_diatance /= cipv_diantance_queue->size_of_queue;
		if(average_diatance < cipv_diantance_queue->data[cipv_diantance_queue->next_head_index - cipv_diantance_queue->size_of_queue] && average_diatance > cipv_diantance_queue->data[cipv_diantance_queue->right_now_head - cipv_diantance_queue->size_of_queue]){
			return 0x01;// approaching
		}else{
			return 0x00;
		}
	}else{
		return 0x00;
	}
}

static uint8_t judge_approaching_cam_distance_ID = 0x00;
static uint8_t judge_approaching_cam_distance_count = 0x00;
uint8_t Approaching_to_lead_car(Obstacle_Information* cipv, uint8_t size_of_history){

	// use cam distance,to judge the ego car approaching the lead car(1), or not(0);
	// 1: approching
	//    hmw working
	// 0: go away
	//    hmw not working
	// when ttc > 6.0 ,just use for hmw breaking
	// loop recored the history data
	if(judge_approaching_cam_distance_ID != (*cipv).ObstacleID){
		judge_approaching_cam_distance_count = 0;

		judge_approaching_cam_distance_ID = (*cipv).ObstacleID;
		Clean_Queue(&Approaching_keep);
		//return 2;
	}
	//if((*cipv).DistanceZ > 50.0){
	//	// cover:when the image no target
	//	return 0;
	//}
	uint8_t approaching_result_k = Approaching_to_target(&Approaching_cipv_distance,cipv);
	if(LOG_Brake_Cancel == LOGLEVEL)
		fprintf(USART1_STREAM,"approaching_result_k = %d   %0.2f %0.2f %0.2f %0.2f %0.2f \r\n",approaching_result_k,Approaching_cipv_distance.data[0],Approaching_cipv_distance.data[1],Approaching_cipv_distance.data[2],Approaching_cipv_distance.data[3],Approaching_cipv_distance.data[4]);
	//////////////////////////////////////////
	// reconfirm
	//////////////////////////////////////////
	if(cipv->RelativeSpeedZ < -0.1){
		approaching_result_k = 0x00;
	}

	//////////////////////////////////////////
	// keep 2 frames
	//////////////////////////////////////////
	In_Queue(&Approaching_keep,approaching_result_k);
	if(approaching_result_k == 0x01){
		float sum=0.0;
		for(uint8_t i_n = 0;i_n < Approaching_keep.size_of_queue;i_n++){
			sum += Approaching_keep.data[i_n];
		}
		if(sum > Approaching_keep.size_of_queue/2.0){
			approaching_result_k = 0x01;
		}else{
			approaching_result_k = 0x00;
		}
	}
	else{
		float sum=0.0;
		for(uint8_t i_n = 0;i_n < Approaching_keep.size_of_queue;i_n++){
			sum += Approaching_keep.data[i_n];
		}
		if(sum < Approaching_keep.size_of_queue/2.0){
			approaching_result_k = 0x00;
		}else{
			approaching_result_k = 0x01;
		}
	}


	return approaching_result_k;
}
uint8_t Approaching_to_lead_car_uradar(float diatance, uint8_t size_of_history){
	//uint8_t double_size = (size_of_history*2 - 1);
	//if(diatance == 5.04){
	//	judge_approaching_uradar_distance_count = 0;
	//	return 2;
	//}
	//return Approaching_to_target(&Approaching_cipv_distance,cipv);
	return 0x00;
}
uint8_t Break_Trigger(Obstacle_Information* cipv, _VEHICLE_PARA* stVehicleParas, Camera_Essential_Data* cam_essiential,Obstacle_Basic_Data* obs_basic_data){

	if(((*stVehicleParas)).fVehicleSpeed < rParms.Min_Enable_Speed){
		// some times speed become to 0.0, and then come back  immediately;so we should use filter to iden
		AEB_State_reset();
		CMS_State_reset();
		return break_calculate_cancel;
	}
	// calculate max_distance every time outside the function
	if((*cipv).DistanceZ > AEB_CMS_parameters.cipv_max_distance){/*target too far :link with ego car speed*/
		AEB_State_reset();
		CMS_State_reset();
		if(LOGLEVEL == LOG_WARNING){
			Log_print(LOG_WARNING,0x01,0x00, "cipv_distance","Break_Trigger", cipv, stVehicleParas, cam_essiential, obs_basic_data);
		}
		return break_calculate_cancel;
	}
	if((*stVehicleParas).ReverseGear == 1){/*  reverse gear*/
		if(LOGLEVEL == LOG_WARNING){
			Log_print(LOG_WARNING,0x03,0x00,"ReverseGear","Break_Trigger", cipv, stVehicleParas, cam_essiential, obs_basic_data);
		}
		AEB_State_reset();
		CMS_State_reset();
		return break_calculate_cancel;
	}
	if((*cipv).RelativeSpeedX > 4.0 || (*cipv).RelativeSpeedX < -4.0){ /* process turning and fast crossing car*/
		if(LOGLEVEL == LOG_WARNING){
			Log_print(LOG_WARNING,0x05,0x00, "crossing target ","Break_Trigger", cipv, stVehicleParas, cam_essiential, obs_basic_data);
		}
		//AEB_State_reset();
		//CMS_State_reset();
		//return break_calculate_cancel;
	}

	return break_calculate_continue;
}

float leave_diatance = 0.0;
uint8_t Break_Cancel(Obstacle_Information* cipv, _VEHICLE_PARA* stVehicleParas, Camera_Essential_Data* cam_essiential, Obstacle_Basic_Data* obs_basic_data,float old_decout){
	uint8_t return_value = break_cancel_NONE;
#ifdef USE_CAM_Warning_info
	if(USE_CAM_Warning_info == 1){
		if(approaching_ornot != 1){
			CMS_HMW_State_reset();// when the lead car go away ,shouldnt break HMW too;
			//return break_cancel_TTCHMW;
			return_value = break_cancel_TTCHMW;
			if(LOG_Brake_Cancel == LOGLEVEL)
				fprintf(USART1_STREAM,"break_cancel_TTCHMW  approaching_ornot\r\n");
		}
		if(((*cipv).TTC < 0.0)|((*cipv).TTC > 6.0)){//|((cipv.RelativeSpeedZ < 0)&((abs(cipv.RelativeSpeedZ + stVehicleParas.fVehicleSpeed / 3.6) > 0.5)))){ /* lead car go away*/
			AEB_State_reset();
			CMS_TTC_State_reset();

			if(LOGLEVEL == LOG_WARNING){
				//Log_print(LOG_WARNING, 0x02,0x00,"TTC < 0","Break_Trigger", cipv);
			}
			//return break_cancel_TTC;
			if(return_value == break_cancel_NONE){
				return_value = break_cancel_TTC;
				if(LOG_Brake_Cancel == LOGLEVEL)
					fprintf(USART1_STREAM,"break_cancel_TTC  TTC\r\n");
			}

			if(return_value == break_cancel_HMW){
				return_value = break_cancel_TTCHMW;
				if(LOG_Brake_Cancel == LOGLEVEL)
					fprintf(USART1_STREAM,"break_cancel_TTCHMW   TTC\r\n");
			}

		}
		if((*cipv).TrackNumber <= AEB_CMS_parameters.min_track_number){//|((cipv.RelativeSpeedZ < 0)&((abs(cipv.RelativeSpeedZ + stVehicleParas.fVehicleSpeed / 3.6) > 0.5)))){ /* lead car go away*/
			AEB_State_reset();
			CMS_TTC_State_reset();
			CMS_HMW_State_reset();// when the lead car go away ,shouldnt break HMW too;
			if(LOGLEVEL == LOG_WARNING){
				//Log_print(LOG_WARNING, 0x02,0x00,"TTC < 0","Break_Trigger", cipv);
			}
			//return break_cancel_TTCHMW;
			return_value = break_cancel_TTCHMW;
			if(LOG_Brake_Cancel == LOGLEVEL)
				fprintf(USART1_STREAM,"break_cancel_TTCHMW  TrackNumber\r\n");

		}
		if((*cipv).RelativeSpeedX > 0){
			leave_diatance = (rParms.Vechile_Width/2 - (*cipv).DistanceX + (*cipv).ObstacleWidth*0.5);
		}else{
			leave_diatance = (rParms.Vechile_Width/2 + (*cipv).DistanceX + (*cipv).ObstacleWidth*0.5);
		}
		if(fabs((*cipv).RelativeSpeedX) > 1.0 && (*cipv).TTC > fabs(leave_diatance/(*cipv).RelativeSpeedX)){
			AEB_State_reset();
			CMS_TTC_State_reset();
			CMS_HMW_State_reset();// when the lead car go away ,shouldnt break HMW too;
			if(LOGLEVEL == LOG_WARNING){
				//Log_print(LOG_WARNING, 0x02,0x00,"TTC < 0","Break_Trigger", cipv);
			}
			//return break_cancel_TTCHMW;
			return_value = break_cancel_TTCHMW;
			if(LOG_Brake_Cancel == LOGLEVEL)
				fprintf(USART1_STREAM,"break_cancel_TTCHMW  RelativeSpeedX big\r\n");

		}
	}
	else{
		if((hmw_r_cal > AEB_CMS_parameters.Cam_HMW_warning_time)|(hmw_r_cal < 0)){
			if(LOGLEVEL == LOG_WARNING){
				Log_print(LOG_WARNING,0x05,0x00, "hmw ","Break_Cancel", cipv, stVehicleParas, cam_essiential,obs_basic_data);
			}
			CMS_HMW_State_reset();
			//return break_cancel_HMW;
			if(return_value == break_cancel_NONE){
				return_value = break_cancel_HMW;
				if(LOG_Brake_Cancel == LOGLEVEL)
					fprintf(USART1_STREAM,"break_cancel_HMW   hmw_r_cal\r\n");
			}
			else if(return_value == break_cancel_TTC){
				return_value = break_cancel_TTCHMW;
				if(LOG_Brake_Cancel == LOGLEVEL)
					fprintf(USART1_STREAM,"break_cancel_TTCHMW   hmw_r_cal\r\n");
			}
		}
	}
#endif

	return return_value;
}
uint8_t Driver_control(_VEHICLE_PARA* stVehicleParas){
	if((*stVehicleParas).LeftFlag ^ (*stVehicleParas).RightFlag){/* turning left or right*/
		AEB_State_reset();
		CMS_State_reset();
		driver_control_gap_time_stamp = SystemtimeClock;
		turn_light_keep_time_stamp = SystemtimeClock;
		if(LOG_Brake_Cancel == LOGLEVEL)
			fprintf(USART1_STREAM,"Driver_control_turn\r\n");
		return break_calculate_cancel;
	}
	if(((SystemtimeClock - turn_light_keep_time_stamp)/1000.0) <= TIME_GAPE_OF_TURN_LIGHT){
		if(LOG_Brake_Cancel == LOGLEVEL)
			fprintf(USART1_STREAM,"Driver_control_turn_keep\r\n");
		return break_calculate_cancel;
	}

	if((*stVehicleParas).BrakeFlag == 1){
		//Judge if cancel the AEBS‘s Brake
		if(dec_output == 0.0){
			//////////////////////////////////////////
			// delay 3 frames
			//////////////////////////////////////////
			if(DriverBraking_delay.is_full == 1){
				if(DriverBraking_delay.data[0] == 0x01 && DriverBraking_delay.data[1] == 0x01 && DriverBraking_delay.data[2] == 0x01){
					AEB_State_reset();
					CMS_State_reset();
					driver_control_gap_time_stamp = SystemtimeClock;
					if(LOG_Brake_Cancel == LOGLEVEL)
						fprintf(USART1_STREAM,"Driver_Brake\r\n");
					return break_calculate_cancel;
				}
				else{
					//do nothing,the AEBS priority level > Driver Brake priority level
				}
			}
			else{
				//do nothing,the AEBS priority level > Driver Brake priority level
			}
		}else{
			//do nothing,the AEBS priority level > Driver Brake priority level
		}
	}else if((*stVehicleParas).BrakeFlag == 0){

	}
	if((((SystemtimeClock - driver_control_gap_time_stamp)/1000.0) < rParms.Driver_Brake_Cooling_Time) && (rParms.switch_g.Chassis_Safe_Strategy_Enable == true)){
		AEB_State_reset();
		CMS_State_reset();
		if(LOG_Brake_Cancel == LOGLEVEL)
			fprintf(USART1_STREAM,"Driver_Brake_cooling_time\r\n");
		return break_calculate_cancel;
	}
	return break_calculate_continue;
}

void AEB_State_reset(void){
	aeb_break_state.TTC_AEB = 0.0;
	aeb_break_state.aeb_TTC_is_break_flag = 0x00;
}
void CMS_State_reset(void){
	cms_break_state.cms_uradar_dec_out_prev = 0.0;
	cms_break_state.cms_uradar_dec_out = 0.0;
	cms_break_state.cms_hmw_dec_out_prev = 0.0;
	cms_break_state.cms_hmw_dec_out = 0.0;
	cms_break_state.cms_ttc_dec_out_prev = 0.0;
	cms_break_state.cms_ttc_dec_out = 0.0;
	cms_break_state.cam_distance = 0.0;
	cms_break_state.uradar_distance = 0.0;


	cms_break_state.cms_is_warning_flag = 0x00;
	cms_break_state.cms_is_break_flag = 0x00;
	cms_break_state.cms_cam_keep_count = 0x00;
	cms_break_state.cms_cam_delay_count = 0x00;
	cms_break_state.cms_cam_over_timestamp = 0x00000000;
	cms_break_state.cms_uradar_keep_count = 0x00;
	cms_break_state.cms_uradar_delay_count = 0x00;

	cms_break_state.cms_ttc_is_warning_flag = 0x00;
	cms_break_state.cms_ttc_is_break_flag = 0x00;
	cms_break_state.cms_ttc_cam_keep_count = 0x00;
	cms_break_state.cms_ttc_cam_delay_count = 0x00;
	cms_break_state.cms_ttc_uradar_keep_count = 0x00;
	cms_break_state.cms_ttc_uradar_delay_count = 0x00;
}
void CMS_HMW_State_reset(void){
	cms_break_state.cms_uradar_dec_out_prev = 0.0;
	cms_break_state.cms_uradar_dec_out = 0.0;
	cms_break_state.cms_hmw_dec_out_prev = 0.0;
	cms_break_state.cms_hmw_dec_out = 0.0;

	cms_break_state.cms_is_warning_flag = 0x00;
	cms_break_state.cms_is_break_flag = 0x00;
	cms_break_state.cms_cam_keep_count = 0x00;
	cms_break_state.cms_cam_delay_count = 0x00;
	cms_break_state.cms_cam_over_timestamp = 0x00000000;
	cms_break_state.cms_uradar_keep_count = 0x00;
	cms_break_state.cms_uradar_delay_count = 0x00;
}
void CMS_TTC_State_reset(void){
	cms_break_state.cms_ttc_dec_out_prev = 0.0;
	cms_break_state.cms_ttc_dec_out = 0.0;
	cms_break_state.cam_distance = 0.0;
	cms_break_state.uradar_distance = 0.0;

	cms_break_state.cms_ttc_is_warning_flag = 0x00;
	cms_break_state.cms_ttc_is_break_flag = 0x00;
	cms_break_state.cms_ttc_cam_keep_count = 0x00;
	cms_break_state.cms_ttc_cam_delay_count = 0x00;
	cms_break_state.cms_ttc_uradar_keep_count = 0x00;
	cms_break_state.cms_ttc_uradar_delay_count = 0x00;
	cms_break_state.cms_uradar_delay_count = 0x00;
}

void AEB_CMS_alg_init(void){
	////////////////////////////////////////////////////
	// TOthers
	AEB_CMS_parameters.max_Dynamic_HMW_speed   = 90.0;     //km/h
	AEB_CMS_parameters.max_enable_speed        = 120.0;    //km/h
	// recal HWM and TTC warning Level
	AEB_CMS_parameters.Cam_HMW_warning_time    = 1.0; //too close to lead car
	AEB_CMS_parameters.Cam_TTC_warning_1_time  = 1.5; //
	AEB_CMS_parameters.Cam_TTC_warning_2_time  = 1.0; //
	// Vehicle parameters
	AEB_CMS_parameters.ttc_max_dec_output	   = 5.0;
	AEB_CMS_parameters.Vehicle_max_react_dec   = 10.0;
	AEB_CMS_parameters.Vehicle_min_react_dec   = 0.5;
	// parameters of CIPV
	AEB_CMS_parameters.cipv_max_distance       = 100.0;
	AEB_CMS_parameters.min_track_number        = 10.0;
	// Parameters of Uradar
	AEB_CMS_parameters.Uradar_max_enable_speed = 20.0;
// Queue init
	Circular_Queue_Init(&Crosswise_Status_queue,CROSSWISE_CACHE_SIZE);
	Circular_Queue_Init(&Approaching_cipv_distance,5);
	Circular_Queue_Init(&Approaching_keep,3);
	Circular_Queue_Init(&ttc_warning_cahe,3);
	Circular_Queue_Init(&hmw_warning_cahe,3);
	Circular_Queue_Init(&DriverBraking_delay,3);
	Circular_Queue_Init(&LDW_queue,5);
	//init the Vehicle Params
	g_veh_params = Param_GetVehicleParams();
}

float Get_Uradar_distance(URADER_MESSAGE urader_company_message[], _VEHICLE_PARA* stVehicleParas)
{
	//fprintf(USART1_STREAM,"Get_Uradar_distance \r\n");
	if(((*stVehicleParas).fVehicleSpeed > AEB_CMS_parameters.Uradar_max_enable_speed)|((*stVehicleParas).fVehicleSpeed <= 0)){
		return 5.04;
	}
	float position = 0;
	for(uint8_t i = 0;i < 2;i ++)
	{
		for(uint8_t j = 0;j < 12;j ++)
		{
			if(urader_company_message[i].Urader_work_stata[j] == URWORK)
			{

				if((urader_company_message[i].Uposition[j] == FRONT )|
				   (urader_company_message[i].Uposition[j] == FRMIDE)|
				   (urader_company_message[i].Uposition[j] == FLMIDE))
				{
					position = min(position, urader_company_message[i].distance[j]);
				}
			}
		}
	}
	if((position >0)&(position < 5.0)){
		return position;
	}
	return 5.04;
}
void Filter_noise(float *dec_out, float dec_out_k,uint8_t *is_break_flag, uint8_t *count_delay, uint8_t *count_keep){
	return;
	if((*dec_out > 0.0)&(*is_break_flag == 0x00)){
		*count_delay = *count_delay + 1;
		if(*count_delay < delay_count){
			*dec_out = 0.0;
		}
		else{
			*count_delay = 0;//delay_count + 1;
			*is_break_flag = 0x01;
		}
	}
	else if((*dec_out == 0.0)&(*is_break_flag == 0x00)){
		*count_delay = 0x00;
	}

	// 过滤高频消失噪声
	if((*dec_out == 0.0)&(*is_break_flag == 0x01)){
		*count_keep = *count_keep + 1;
		if(*count_keep < keep_count){
			*dec_out = dec_out_k;
			//cms_break_state.cms_break_flag = 0;
		}else{
			*count_keep = 0;//keep_count + 1;
			*is_break_flag = 0x00;
			//cms_break_state.cms_break_flag = 1;
		}
	}else if((*dec_out > 0.0)&(*is_break_flag == 0x01)){
		*count_keep = 0;
	}
}

void Log_print(enum LOG_LEVEL level, uint8_t cancel_n, uint8_t func_n, const char* description, const char* function_name, Obstacle_Information* cipv,_VEHICLE_PARA* stVehicleParas,Camera_Essential_Data* Cam_Essi_data,Obstacle_Basic_Data* obs_basic_data){
	//fprintf(USART1_STREAM,"DEBUG:");
	switch (level){
		case LOG_NONE:
			break;
		case LOG_ERROR:
			fprintf(USART1_STREAM,"ERROR: ego_speed: ,%.6f,  decp_out: ,%6.6f,  function: %s \r\n",ego_speed, dec_output,function_name);
			break;
		case LOG_WARNING:
			//fprintf(USART1_STREAM,"WARNING:ego_speed: %.6f,  decp_out: ,%6.6f, description: %s  function: %s \r\n",ego_speed, dec_output,description,function_name);
			fprintf(USART1_STREAM,"WARNING_speed_dec_cancleNum_functionNum:%.6f,%6.6f,%d,%d,%d,%s,%s \r\n",ego_speed, dec_output,cancel_n,func_n,0,description,function_name);
			break;
		case LOG_DEBUG:
			/*fprintf(USART1_STREAM,"DEBUG: ,%.6f, function: ,%s ",dec_output,function_name);
			fprintf(USART1_STREAM,"Ego_Speed: ,%6.6f ,Dec_out: ,%6.6f, TTC: ,%6.6f HMW: ,%6.6f CAM_distance: %6.6f CMS_HMW_FLAG: %d cms_hmw_decout: %6.6f  CMS_TTC_FLAG: %d cms_ttc_decout: %6.6f AEB_TTC_FLAG: %d aeb_ttc_decout: %6.6f", \
									ego_speed, dec_output,cipv.TTC,cipv.HMW,cipv.DistanceZ, cms_break_state.cms_is_break_flag, cms_break_state.cms_hmw_dec_out, cms_break_state.cms_ttc_is_break_flag, cms_break_state.cms_ttc_dec_out, aeb_break_state.aeb_TTC_is_break_flag, aeb_break_state.TTC_AEB_dec_out);
			fprintf(USART1_STREAM,"Relative_Speed_Z: ,%6.6f,  Relative_Speed_X: ,%6.6f \r\n",cipv.RelativeSpeedZ, cipv.RelativeSpeedX);
			fprintf(USART1_STREAM,"Urada_distance: %6.6f \r\n",cms_break_state.uradar_distance);*/
			fprintf(USART1_STREAM,"speed_dec_TTC_HMW_life:,%6.6f,%6.6f,%6.6f,%6.6f,%d \r\n",ego_speed, dec_output,(*cipv).TTC,(*cipv).HMW,(*cipv).TrackNumber);
			break;
		case LOG_IN_OUT:
			//speed+decout+CIPV+TTC+FCWStatus+HMW+HMW_Grade+
			/*fprintf(USART1_STREAM,"speed=%6.6f,dec_out=%6.6f,CIPV=%d,TTC=%6.6f,FCW_Status=%d,HMW=%6.6f,HMW_Grade=%d,Break_flag=%d,R_X_speed=%6.6f,R_Z_speed=%6.6f,Cam_distance=%6.6f,TrackNumber=%d,Urard_distance=%6.6f,ttc_aeb_dec=%6.6f,ttc_cms_dec=%6.6f,hmw_cms_dec=%6.6f\r\n",\
					ego_speed, dec_output,cipv.CIPV,cipv.TTC,Cam_Essi_data.FCWStatus,cipv.HMW,Cam_Essi_data.HMWGrade,stVehicleParas.BrakeFlag,cipv.RelativeSpeedX,cipv.RelativeSpeedZ,cipv.DistanceZ,cipv.TrackNumber,Uradar_distance,ttc_aeb_dec,ttc_cms_dec,hmw_cms_dec);*/
			//fprintf(USART1_STREAM,"t=%6.2f,v=%6.2f,dec=%6.2f,cipvn=%d,cipv=%d,TTC=%6.2f,ttc=%6.2f,FCW=%d,HMWn=%6.2f,HMW=%6.2f,HMWG=%d,Bf=%d,RXv=%6.2f,RZv=%6.2f,Camd=%6.2f,age=%d,Ud=%6.2f,ttcaeb=%6.2f,ttccms=%6.2f,hmwcms=%6.2f,TL=%d,TR=%d,R=%d\r\n",\
			//		SystemtimeClock/6000,ego_speed, dec_output,cipv_now.CIPV,cipv.CIPV,cipv.TTC,ttc_r_cal,Cam_Essi_data.FCWStatus,cipv_now.HMW,cipv.HMW,Cam_Essi_data.HMWGrade,stVehicleParas.BrakeFlag,cipv.RelativeSpeedX,cipv.RelativeSpeedZ,cipv.DistanceZ,cipv.TrackNumber,Uradar_distance,ttc_aeb_dec,ttc_cms_dec,hmw_cms_dec,stVehicleParas.LeftFlagTemp,stVehicleParas.RightFlagTemp,stVehicleParas.ReverseGear);
			fprintf(USART1_STREAM,"\r\n %ld,Tid=%d,Tid=%d,wv=%0.2f,v=%0.2f,dec=%0.2f,appro=%d,ID=%d,type=%d,cipvgot=%d,cipv=%d,TTC=%0.2f,ttc=%0.2f,FCW=%d,HMW=%0.2f,hmw=%0.2f",\
								SystemtimeClock, obs_basic_data_history.TimeID,(*obs_basic_data).TimeID,Get_Wheel_Speed()/10.0,ego_speed*3.6,dec_output,approaching_ornot,cipv_history.ObstacleID,cipv_history.ObstacleType,got_cipv_history,cipv_history.CIPV,cipv_history.TTC,ttc_r_cal,cam_essiential_history.FCWStatus,cipv_history.HMW,hmw_r_cal);
			fprintf(USART1_STREAM,",HMWG=%d,Bf=%d,W=%0.2f,H=%0.2f,RXv=%0.2f,RZv=%0.2f,Camdz=%0.2f,Camdx=%0.2f,age=%d,Ud=%0.2f,ttcaeb=%0.2f,ttccms=%0.2f,hmwcms=%0.2f,TL=%d,TR=%d,R=%d\r\n",\
					cam_essiential_history.HMWGrade,(*stVehicleParas).BrakeFlag,cipv_history.ObstacleWidth,cipv_history.ObstacleHeight,cipv_history.RelativeSpeedZ,cipv_history.RelativeSpeedX,cipv_history.DistanceZ,cipv_history.DistanceX,cipv_history.TrackNumber,Uradar_distance,ttc_aeb_dec,ttc_cms_dec,hmw_cms_dec,(*stVehicleParas).LeftFlag,(*stVehicleParas).RightFlag,(*stVehicleParas).ReverseGear);
			/*fprintf(USART1_STREAM,"\r\n wv=%0.2f,v=%0.2f",\
											Get_Wheel_Speed()/10.0,ego_speed*3.6);
											*/
			//fprintf(USART1_STREAM,"dec=%0.2f,Camdz_can=%0.2f Camdz_history=%0.2f\r\n",dec_output, obstacle_cipv_data.DistanceZ,cipv_history.DistanceZ);

			break;
		default:
			break;
	}
	return;
}

//for test
uint16_t m_pressure_xj;
static uint32_t Ret_test_time_tamp = 0;
float  Ret_test(void){
	if((SystemtimeClock - Ret_test_time_tamp) > 30){
		if(m_pressure_xj <= 50){
			m_pressure_xj = 500;//m_pressure_xj + 50;//ret_pressure + 3;
		}
		else if(m_pressure_xj > 50){
			m_pressure_xj = m_pressure_xj - 10;
		}
		else{
			m_pressure_xj = 0;
		}
		Ret_test_time_tamp = SystemtimeClock;
	}
	//m_pressure_xj = 0;

	//m_pressure_xj  = (uint16_t)(ret_pressure * 100);
	/*tx_valve_control.data[0] = (uint8_t)(m_pressure_xj & 0x00FF);
	tx_valve_control.data[1] = (uint8_t)((m_pressure_xj & 0xFF00) >> 8);
	*tx_frame = tx_valve_control;*/
	//fprintf(USART1_STREAM,"SystemtimeClock: %d Set_Pressure: %d  tx_valve_control: %d ",SystemtimeClock,m_pressure_xj,tx_valve_control);

	return (float)m_pressure_xj;
}

static float aebs_test_brake_max_speed = 40.0;
float AEBS_Test_Brake(uint8_t set_brake_perssure,_VEHICLE_PARA stVehicleParas){
	if(stVehicleParas.fVehicleSpeed > aebs_test_brake_max_speed){
		test_brake_keep_time_stamp = SystemtimeClock;
		g_AEBS_outside_Test_Brake_Warning = 0x00;
		return 0.0;
	}
	if(set_brake_perssure < 6 && set_brake_perssure > 0){
		if((SystemtimeClock - test_brake_keep_time_stamp)/1000 < 40.0){
			g_AEBS_outside_Test_Brake_Warning = 0x01;
			return (float)(set_brake_perssure);
		}else{
			g_AEBS_outside_Test_Brake_Warning = 0x00;
			Brake_Test_Pressure_Bar = 0x00;
			return 0.0;
		}

	}else{
		test_brake_keep_time_stamp = SystemtimeClock;
		g_AEBS_outside_Test_Brake_Warning = 0x00;
		return 0.0;
	}
}


//Dynamic Ratio Of Deceleration
uint32_t dec_self_adaption_timestamp = 0;
uint32_t dec_self_adaption_timestamp_for_integral = 0;
float dec_self_adaption_dec_integral = 0.0;
float dec_self_adaption_start_speed = 0.0;
float Dec_Self_Adaption(
		float relative_speed,				//m/s
		float target_dec,					//m/s2
		float vehicle_speed,				//m/s
		float brake_sys_delay,				//s
		float add_offset_upper_limit,		//m/s2
		float add_offset_lower_limit,		//m/s2
		float multiply_offset_upper_limit,	//--
		float multiply_offset_lower_limit,	//--
		float div_time_upper_limit,			//s
		float div_time_lower_limit,			//s
		float coefficient_of_time_flow		//--
		)
{
	if(target_dec <= 0.0)
	{
		dec_self_adaption_timestamp = SystemtimeClock;
		dec_self_adaption_timestamp_for_integral = SystemtimeClock;
		dec_self_adaption_dec_integral = 0.0;
		dec_self_adaption_start_speed = vehicle_speed;
		return 1.0;
	}else{
		/////////////////Add offset////////////////////
		// dec integral
		//1.对每次减速度与时间间隔做乘法，对预计的速度减少量的进行累加求和，算出从刹车时刻开始到当前时刻，总预计速度减少量是多少
		//实际就是当前时刻的预计速度减少量。
		dec_self_adaption_dec_integral += target_dec * (SystemtimeClock -dec_self_adaption_timestamp_for_integral)/1000;//m/s2
		dec_self_adaption_timestamp_for_integral = SystemtimeClock;
		// speed change
		//2.计算刹车时刻车速与当前车速的实际速度减少量。
		float speed_changec = dec_self_adaption_start_speed - vehicle_speed;// m/s
//******************************
		//以下部分想做时间的补偿，实际上输出时与SIMULINK仿真设置的0.5固定值一个值
		// div_time for calcualte dec offset(add)
		float time_flow = (SystemtimeClock - dec_self_adaption_timestamp)/1000 + brake_sys_delay;//s
		time_flow = min(5.0,time_flow);
		time_flow *= coefficient_of_time_flow;//0.3
		time_flow = max(0.01,min(0.5,time_flow));
//**********************************
		// calculate add offset
		//3.计算预期速度减少量与实际速度减少量的差值，并除以时间，算出加性补偿的△a+
		float add_offset = (dec_self_adaption_dec_integral - speed_changec)/time_flow;
		add_offset = max(-10,min(15.0,add_offset));
		//////////////////multiply_offset///////////////
		// calculate multiply offset coefficient(dynamic ratio)
		//4.算法乘性补偿
		float dynamic_ratio = (target_dec + add_offset)/target_dec;
		dynamic_ratio = max(0.1,min(10.0,dynamic_ratio));
		//////////////////Result Dec cal////////////////
		//5.算出最终的自适应后的刹车力度
		float Self_Adaption_result_dec = dynamic_ratio * (target_dec + add_offset);
		Self_Adaption_result_dec = max(0.0,min(rParms.Max_Output_dec,Self_Adaption_result_dec));
//		if(retract_brake_flag){
//			if(Self_Adaption_result_dec < target_dec)
//				Self_Adaption_result_dec = target_dec;
//		}
		return Self_Adaption_result_dec;
	}

}

