/*
 * i2c.c
 *
 *  Created on: 2022-03-30
 *      Author: yuhong
 */
#include "gpio.h"
#include "stdio.h"
#include "i2c.h"

void i2c_delay_us( uint32_t nCount);
void scl_out();
void sda_out();
void sda_in();


/**
  * 描述   I2C GPIO重映射初始化
  * 输入   无
  * 返回   无
*/
void I2C_GPIO_init(void)
{
	/*I2Cx*/
	/*引脚配置*/
	sda_out();//I2C3 SDA
	scl_out();//I2C3 SCL
}


/**
 * 设置I2C3 GPIO为输出模式
 */
void sda_out()
{
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_12, GPIO_MODE_OUT);//I2C3 SDA
}
/**
 * 设置I2C3 GPIO为输入模式
 */
void sda_in()
{
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_12, GPIO_MODE_IN);//I2C3 SDA
}
/**
 * 设置I2C3 SDA GPIO的值
 */
void set_sda(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_12, BitsValue);
}

/**
 * 设置I2C3 GPIO SCL为输出模式
 */
void scl_out()
{
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_11, GPIO_MODE_OUT);//I2C3 SCL
}

/**
 * 设置I2C3 SCL GPIO的值
 */
void set_scl(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_11, BitsValue);
}

void i2c_delay_us( uint32_t nCount)
{
	nCount=24*nCount;
	while(nCount--)
	{
	}
}

/**
  * 描述   I2C3 的启动
  * Bit_SET为 0x1
  * Bit_RESET为0x0
  *
  * 返回   无
*/
void I2C_Start( void )
{
	sda_out();
	set_sda(Bit_SET);  //空闲态
	set_scl(Bit_SET);
	i2c_delay_us(6);
	set_scl(Bit_SET);
	set_sda(Bit_RESET);//档SCL线为高电平时，SDA由高到低电平跳变，为起始信号
	i2c_delay_us(6);
	set_scl(Bit_RESET);
}

/**
  * 描述   I2C3 的停止
  * Bit_SET为 0x1
  * Bit_RESET为0x0
  *
  * 返回   无
*/
void I2C_Stop( void )
{
	sda_out();
	set_scl(Bit_RESET);
	set_sda(Bit_RESET);  //空闲态
	i2c_delay_us(6);
	set_scl(Bit_SET);
	set_sda(Bit_SET);    //当SCL线为高电平时，SDA由低到高电平跳变，为停止信号
	i2c_delay_us(6);
	set_scl(Bit_RESET);
}

BitAction read_sda( void )
{
	return GPIO_Read_Input_Data_Bit(GPIOB_SFR, GPIO_PIN_MASK_12);
}
/**
  * 等待ACK
  * 返回   0： ACK
  *  	 1：NACK
  * Bit_SET为 0x1
  * Bit_RESET为0x0
*/
uint8_t I2C_Wait_Ack( void )
{
	uint16_t timeCnt = 0;
	//fprintf(USART1_STREAM,">> Entry the %s\r\n",__FUNCTION__);
	set_sda(Bit_SET);
	//i2c_delay_us(2);
	sda_in();             //SDA管脚设为输入模式
	i2c_delay_us(2);
	set_scl(Bit_SET);
	i2c_delay_us(2);

	while(read_sda())
	{
		//fprintf(USART1_STREAM,">> In read_sda() loop value is %d, and the time cnt is %d\r\n",read_sda(),timeCnt);
		timeCnt++;
		if(timeCnt>500)
		{
			I2C_Stop();
			fprintf(USART1_STREAM,"i2c I2C_Wait_Ack()>>No ACK Get. read_sda() is %d \r\n",read_sda());
			return 1;
		}
	}
	set_scl(Bit_RESET);
	//fprintf(USART1_STREAM,"i2c I2C_Wait_Ack()>>ACK Get. read_sda() is %d. The timeCnt is %d\r\n",read_sda(),timeCnt);
	return 0;
}

/**
  * 发送一个字节地址数据或其他数据
  * 返回   无
  *
  * Bit_SET为 0x1
  * Bit_RESET为0x0
*/
void I2C_Send_Byte( uint8_t byte )
{
	uint8_t index = 0;
	uint8_t bit;
	//fprintf(USART1_STREAM,"i2c I2C_Send_Byte()>>The Byte is 0x%x and byte&0x80 is 0x%x.\r\n", byte ,(byte&0x80));
	sda_out();
	set_scl(Bit_RESET);   //只有在SCL线为低电平时，SDA线才可以改变
	//fprintf(USART1_STREAM,"i2c I2C_Send_Byte()>>The data:{");
	for(index=0;index<8;index++)
	{
		bit = (byte&0x80)>>7; //取最高位
		byte = byte<<1;       //更新最高位
		//fprintf(USART1_STREAM,"%d\r",bit);
		set_sda(bit);
		i2c_delay_us(2);
		set_scl(Bit_SET);
		i2c_delay_us(4);
		set_scl(Bit_RESET);
		//i2c_delay_us(2);
	}
	i2c_delay_us(2);
	//set_scl(Bit_SET);
	//i2c_delay_us(2);
	//fprintf(USART1_STREAM,"}\n",bit);
}

/**
  * 读取一个字节，并发送ACK或NACK
  * 输入    ack：收发发送ACK
  *      TRUE发送，FALSE不发送
  * 返回   返回接收到的值
*/
uint8_t I2C_Read_Byte(uint8_t ack)
{
	uint8_t i;
	uint8_t recvVal = 0;
	sda_in();
	for(i=0;i<8;i++)
	{
		set_scl(Bit_RESET);
		i2c_delay_us(2);      //等待输出
		set_scl(Bit_SET);     //可读取
		recvVal = recvVal<<1; //将最低位空出
		if(read_sda())
		{
			recvVal++;        //高电平，则最低位为1
		}
		//fprintf(USART1_STREAM,"I2C_Read_Byte>> Each recvVal is 0x%x\r\n",recvVal);
		i2c_delay_us(2);
	}

	if(ack)
	{
		I2C_Ack();            //发送ACK
	}
	else
	{
		I2C_Nack();           //发送NACK
	}
	//fprintf(USART1_STREAM,"i2c>> The Read recvVal is 0x%x\r\n",recvVal);
	return recvVal;
}

/**
  * 读取一个字（两个字节），并发送ACK或NACK
  * 输入    ack：收发发送ACK
  *      TRUE发送，FALSE不发送
  * 返回   返回接收到的值
*/
uint16_t I2C_Read_Word()
{
	uint8_t i;
	uint16_t recvVal = 0;
	sda_in();
	for(i=0;i<16;i++)
	{
		set_scl(Bit_RESET);
		i2c_delay_us(2);      //等待输出
		set_scl(Bit_SET);     //可读取
		recvVal = recvVal<<1; //将最低位空出
		if(read_sda())
		{
			recvVal++;        //高电平，则最低位为1
		}
		if(i==7)
		{
			I2C_Ack();            //收到8个bit之后，发送ACK
		}
		//fprintf(USART1_STREAM,"i2c I2C_Read_Byte>> Each recvVal is 0x%x\r\n",recvVal);
		i2c_delay_us(2);
	}
	return recvVal;
}

/**
  * 发送ACK应答
  * 输入    无
  * 返回    无
*/
void I2C_Ack(void)
{
	sda_out();
	set_scl(Bit_RESET);
	set_sda(Bit_RESET);
	i2c_delay_us(4);
	set_scl(Bit_SET);
	i2c_delay_us(4);
	set_scl(Bit_RESET);
}
/**
  * 发送NACK应答
  * 输入    无
  * 返回    无
*/
void I2C_Nack(void)
{
	sda_out();
	set_scl(Bit_RESET);
	set_sda(Bit_SET);
	i2c_delay_us(2);
	set_scl(Bit_SET);
	i2c_delay_us(2);
	set_scl(Bit_RESET);
}

/**
  * 描述   I2C 写一个字节数据
  * 输入   write_addr：写入数据的AT24C128C的存储地址
  *      data: 待写入的值
  * 返回   无
*/
void AT24CXX_WriteOneByte(uint16_t write_addr,uint32_t data)
{
	I2C_Start();	                   //发出起始信号
	I2C_Send_Byte(EEPROM_WRITE_ADDR);  //写器件的地址+写操作
	I2C_Wait_Ack();
	//fprintf(USART1_STREAM,"i2c>> The Read High Addr is 0x%x\r\n",write_addr>>8);
	I2C_Send_Byte(write_addr>>8);      //发送高地址
	I2C_Wait_Ack();
	//fprintf(USART1_STREAM,"i2c>> The Read Low Addr is 0x%x\r\n",write_addr%256);
	I2C_Send_Byte(write_addr%256);     //发送低地址
	I2C_Wait_Ack();
	I2C_Send_Byte(data);
	I2C_Wait_Ack();
	I2C_Stop();
	i2c_delay_us(6);             //等待AT24C128写数据
	//fprintf(USART1_STREAM,"i2c>> Exit the AT24CXX_WriteOneByte\r\n");
}

/**
  * 描述   I2C 读取一个字节数据
  * 输入   read_addr：
  * 返回   uint8_t：读取到的数据
*/
uint8_t AT24CXX_ReadOneByte(uint16_t read_addr)
{
	uint8_t result;
	//fprintf(USART1_STREAM,"Entry the %s\r\n",__FUNCTION__);
	I2C_Start();                       //发出起始信号
	//fprintf(USART1_STREAM,"AT24CXX_ReadOneByte>> I2C_Start Finish\r\n");
	I2C_Send_Byte(EEPROM_WRITE_ADDR);  //写器件地址+写操作
	//fprintf(USART1_STREAM,"AT24CXX_ReadOneByte>> I2C_Send_Byte 0x%x\r\n",EEPROM_WRITE_ADDR);
	I2C_Wait_Ack();
	//fprintf(USART1_STREAM,"AT24CXX_ReadOneByte>> The Read High Addr is 0x%x\r\n",read_addr>>8);
	I2C_Send_Byte(read_addr>>8);       //发送高地址
	I2C_Wait_Ack();
	//fprintf(USART1_STREAM,"AT24CXX_ReadOneByte>> The Read Low Addr is 0x%x\r\n",read_addr%256);
	I2C_Send_Byte(read_addr%256);      //发送低地址
	I2C_Wait_Ack();
	I2C_Start();                       //重新发出起始信息
	I2C_Send_Byte(EEPROM_READ_ADDR);   //读操作
	I2C_Wait_Ack();
	result = I2C_Read_Byte(0);         //读取一个字节，发出NACK
	I2C_Stop();
	//fprintf(USART1_STREAM,"Exit the %s\r\n",__FUNCTION__);
	return result;
}

/**
  * 描述   I2C 读多个字节数据
  * 输入   Read_I2C_Addr：指定要读的I2C地址
  *          p_buffer: 读入数据地址指针
  *          number_of_byte：读取数据个数
  * 返回   无
  *
*/
uint8_t AT24CXX_ReadBytes(uint16_t read_addr,uint8_t *p_buffer,uint16_t num_of_read)
{
	//parameter check
	if((read_addr<0)||((read_addr+num_of_read)>AT24C128C_SIZE)||(read_addr>AT24C128C_SIZE))
	{
		fprintf(USART1_STREAM,"i2c AT24CXX_WriteBytes()>> The Write Address less than zero or larger than max size\r\n");
		return 1;
	}
	while((num_of_read--)&&(read_addr<=AT24C128C_SIZE))
	{
		*p_buffer++ = AT24CXX_ReadOneByte(read_addr++);
	}
	return 0;
}

/**
  * 描述   I2C 写多个字节数据
  * 输入   Write_i2c_Addr：要写入指定的I2C地址
  *          p_buffer: 写入数据地址指针
  *          number_of_byte：写入数据个数
  * 返回   无
*/
uint8_t AT24CXX_WriteBytes(uint16_t write_addr,uint8_t *p_buffer,uint16_t num_of_write)
{
	//parameter check
	if((write_addr<0)||(write_addr>AT24C128C_SIZE))
	{
		fprintf(USART1_STREAM,"i2c AT24CXX_WriteBytes()>> The Write Address less than zero or larger than max size\r\n");
		return 1;
	}
	while((num_of_write--)&&(write_addr<=AT24C128C_SIZE))
	{
		//fprintf(USART1_STREAM,"i2c AT24CXX_WriteBytes()>> num_of_write is %d, write_addr is 0x%x and *p_buffer is 0x%x\r\n",num_of_write,write_addr,*p_buffer);
		AT24CXX_WriteOneByte(write_addr,*p_buffer);
		write_addr++; //地址加1
		p_buffer++;  //数组地址加1
	}
	return 0;
}

/**
  * 描述   检查AT24CXX设备是否存在，首次在末尾写入0x55，然后读取末尾字节是否是0x55
  * 输入  无
  * 返回   0: 写入成功；1：写入失败
*/
uint8_t AT24CXX_Check( void )
{
	uint8_t tmp;
	tmp = AT24CXX_ReadOneByte(AT24C128C_SIZE);
	if(tmp==0x55)
	{
		return 0;   //之前已经写入，直接返回
	}
	else
	{
		AT24CXX_WriteOneByte(AT24C128C_SIZE,0xFF);
		tmp = AT24CXX_ReadOneByte(AT24C128C_SIZE);
		if(tmp==0x55)
		{
			return 0;
		}
	}
	return 1;
}

/**
  * 描述   获取温度
  * 输入  无
  * 返回  环境温度
*/
float TMP102A_Get( void )
{
	uint8_t first;
	uint8_t second;
	int16_t temperature;
	float temp;
	//fprintf(USART1_STREAM,"Entry the %s\r\n",__FUNCTION__);
	I2C_Start();                            //发出起始信号
	//fprintf(USART1_STREAM,"I2C_Start().\r\n");
	I2C_Send_Byte(TEMP_SENSOR_WRITE_ADDR);  //写器件地址+写操作
	//fprintf(USART1_STREAM,"I2C_Send_Byte().\r\n");
	I2C_Wait_Ack();
	I2C_Send_Byte(POINTER_ADDR);            //发送指针地址
	//fprintf(USART1_STREAM,"I2C_Send_Byte().\r\n",__FUNCTION__);
	I2C_Wait_Ack();
	I2C_Stop();                             //停止I2C
	i2c_delay_us(10);

	//fprintf(USART1_STREAM,">> I2C_Start again to read temperature.\r\n");
	I2C_Start();  						    //开始读取温度
	//fprintf(USART1_STREAM,">> The Send the Read Addr is 0x%x\r\n",TEMP_SENSOR_READ_ADDR);
	I2C_Send_Byte(TEMP_SENSOR_READ_ADDR);   //读操作
	I2C_Wait_Ack();
	first = I2C_Read_Byte(1);               //读取一个字节，发出ACK
	//I2C_Wait_Ack();
	second = I2C_Read_Byte(0);              //读取一个字节，发出NACK
	I2C_Stop();
	//fprintf(USART1_STREAM,"the first byte is 0x%x, the second byte is 0x%x \r\n", first, second);

	//ignore the lower 4 bits of byte 2
	second = second >> 4;
	//combine to make one 12 bit binary number
	temperature = (first << 4) | second;
	temp = temperature*0.0625;
	//fprintf(USART1_STREAM,"the result is %0.2f \r\n", temp);
	//fprintf(USART1_STREAM,"the first byte is 0x%x, the second byte is 0x%x \r\n", first, second);
	//fprintf(USART1_STREAM,"Exit the %s\r\n",__FUNCTION__);
	return (first >>7) ? -temp:temp;
}



