/*
 * kunlun_fcw_ldw.c
 *
 *  Created on: 2022-4-16
 *      Author: xujie
 */
#include "kunlun_fcw_ldw.h"
#include "circular_queue.h"
#include "AEB_CMS_alg.h"

Circular_Queue hmw_warning_cahe;
static float hmw_queue[10];
static uint8_t hmw_queue_size		= 3;
static uint8_t hmw_queue_count 		= 0;
static uint8_t hmw_queue_pointer 	= 0;

Circular_Queue ttc_warning_cahe;

static float ttc_queue[10];
static uint8_t ttc_queue_size 		= 3;
static uint8_t ttc_queue_count 		= 0;
static uint8_t ttc_queue_pointer 	= 0;

uint32_t Left_LDW_Reset_time_stamp 	= 0;
uint32_t Right_LDW_Reset_time_stamp = 0;
uint32_t Left_LDW_End_time_stamp 	= 0;
uint32_t Right_LDW_End_time_stamp 	= 0;
Circular_Queue LDW_queue;

uint8_t history_id = 0x00;
uint32_t history_cipv_time_stamp = 0x00;
uint8_t history_cipv_timeID = 0x00;
void AEB_TTC_warning(Obstacle_Information* cipv,
		Obstacle_Basic_Data *obs_basic_data,
					float level1_time_gap,
					float level2_time_gap,
					float aeb_break_dec_set,
					float aeb_break_stop_distance_set,
					float air_break_delay_time,
					uint8_t* fcw_warning)
{
	float brake_time_point = AEB_TTC_Break_Time_Point(cipv,
			obs_basic_data,
			aeb_break_dec_set,
			aeb_break_stop_distance_set,
			air_break_delay_time);
	if(history_id != cipv->ObstacleID){
		fcw_warning = 0x00;
		history_id = cipv->ObstacleID;
		return;
	}
	if(history_cipv_timeID != obs_basic_data->TimeID){
		history_cipv_time_stamp = SystemtimeClock;
	}
	history_cipv_timeID = obs_basic_data->TimeID;

	if((SystemtimeClock - history_cipv_time_stamp)/1000 > 2.0){
		fcw_warning = 0x00;
		return;
	}

	if((brake_time_point + level2_time_gap) > cipv->TTC)// || (brake_time_point + level2_time_gap) > cipv.ETTC)
	{
		(*fcw_warning) = 0x02;
	}
	else if((brake_time_point + level1_time_gap) > cipv->TTC)// || (brake_time_point + level1_time_gap) > cipv.ETTC)
	{
		(*fcw_warning) = 0x01;
	}else{
		(*fcw_warning) = 0x00;
	}
	return;
}
void CMS_HMW_warning(_VEHICLE_PARA* stVehicleParas,uint8_t TrackNumber,uint8_t min_tracknumber,float hmw_thr, float hmw, uint8_t warning_grade, uint8_t appr_ornot){
	if(hmw < 6.3){
		In_Queue(&hmw_warning_cahe,hmw);
	}
	else{
		Clean_Queue(&hmw_warning_cahe);
	}

	if(hmw_warning_cahe.is_full == 0x01){
		g_CMS_hmw_warning = 1;
		for(uint8_t l_i = 0;l_i < hmw_warning_cahe.size_of_queue; l_i++){
			if(hmw_warning_cahe.data[l_i] > hmw_thr || appr_ornot == 0x00){
				g_CMS_hmw_warning = 0;
				return;
			}
		}
	}
	else{
		g_CMS_hmw_warning = 0;
	}
	if(TrackNumber < min_tracknumber || appr_ornot == 0x00){
		g_CMS_hmw_warning = 0;
	}
	return;

}
void CMS_TTC_warning(_VEHICLE_PARA* stVehicleParas,uint8_t TrackNumber,uint8_t min_tracknumber,float cms_ttc_warning_thr, float aeb_ttc_thr_L1,float aeb_ttc_thr_L2,float ttc){
	// judge if TTC warning(CMS/AEB) or not
	if(ttc < 6.3){
		In_Queue(&ttc_warning_cahe,ttc);
	}
	else{
		Clean_Queue(&ttc_warning_cahe);
	}

	if(ttc_warning_cahe.is_empty == 0){
		g_CMS_ttc_warning = 1;
		for(uint8_t l_i = 0;l_i < ttc_warning_cahe.size_of_queue; l_i++){
			if(ttc_warning_cahe.data[l_i] > cms_ttc_warning_thr){
				g_CMS_ttc_warning = 0;
				//return ;
			}
		}
	}
	else{
		g_CMS_ttc_warning = 0;
	}
	if(TrackNumber < min_tracknumber){
		g_CMS_ttc_warning = 0;
	}
	//fprintf(USART1_STREAM, "ttc_queue:%0.2f\r\n",ttc_queue[1]);
	return;
}

// LDW: return warning level:follow Displayer CAN protocol V0.8
#define cooling_time_LDW 5.0
#define max_keep_time_LDW 0.1
#define Left_LDW 1
#define Right_LDW 2
uint8_t flag_LDW = 0x00;
uint8_t flag_LDW_Reset = 0x01;
#include "tool.h"
uint8_t LDW_Lane_Departure_Warning(_VEHICLE_PARA* stVehicleParas,Camera_Info camera_share,Camera_LDW_data camera_ldw_data)
{
//	gcz_serial_v1_dma_printf(SERIAL_UART1,"1.%d %d %d %d\r\n",camera_ldw_data.LeftLaneStyle,
//			camera_share.CammeraEssentialData.LeftLDW,camera_ldw_data.RightLaneStyle,camera_share.CammeraEssentialData.RightLDW);
	if((camera_ldw_data.LeftLaneStyle == 0 && camera_ldw_data.RightLaneStyle == 0)){
		if(LDW_queue.is_empty == 0x00)
			Clean_Queue(&LDW_queue);
		return 0x00;
	}
	if(camera_share.CammeraEssentialData.LeftLDW == 0x00 && camera_share.CammeraEssentialData.RightLDW == 0x00){
//gcz 2022-06-06	注释掉原有过滤方案，该方案会过度过滤，比如先左转再右转，这时右转压线会被该方法过滤掉而丢失
#if 1
		flag_LDW = 0x00;
		In_Queue(&LDW_queue,(float)flag_LDW);
		flag_LDW_Reset = 0x01;
		for(uint8_t i=0;i<LDW_queue.size_of_queue;i++){
			if(LDW_queue.data[i] != 0){
				flag_LDW_Reset = 0x00;
			}
		}
		if((flag_LDW_Reset == 0x01) && ((SystemtimeClock - Right_LDW_End_time_stamp)/1000 > cooling_time_LDW) && ((SystemtimeClock - Left_LDW_End_time_stamp)/1000 > cooling_time_LDW)){
			Left_LDW_Reset_time_stamp = SystemtimeClock;
			Right_LDW_Reset_time_stamp = SystemtimeClock;
		}
#endif
		return 0x03;// No Warning
	}else{
		if((camera_share.CammeraEssentialData.LeftLDW == 0x01) && (camera_share.CammeraEssentialData.RightLDW == 0x00)){
//gcz 2022-06-06	注释掉原有过滤方案，该方案会过度过滤，比如先左转再右转，这时右转压线会被该方法过滤掉而丢失
#if 1
			In_Queue(&LDW_queue,(float)Left_LDW);
			Right_LDW_Reset_time_stamp = SystemtimeClock;
			Left_LDW_End_time_stamp = SystemtimeClock;

			if(((SystemtimeClock - Left_LDW_Reset_time_stamp)/1000 > max_keep_time_LDW) || ((SystemtimeClock - Right_LDW_End_time_stamp)/1000 < cooling_time_LDW) || (stVehicleParas->LeftFlag == 0x01))
				return 0x03;
			else
#endif
				return Left_LDW;
		}else if((camera_share.CammeraEssentialData.LeftLDW == 0x00) && (camera_share.CammeraEssentialData.RightLDW == 0x01) ){
//gcz 2022-06-06	注释掉原有过滤方案，该方案会过度过滤，比如先左转再右转，这时右转压线会被该方法过滤掉而丢失
#if 1
			In_Queue(&LDW_queue,(float)Right_LDW);
			Left_LDW_Reset_time_stamp = SystemtimeClock;
			Right_LDW_End_time_stamp = SystemtimeClock;
			if(((SystemtimeClock - Right_LDW_Reset_time_stamp)/1000 > max_keep_time_LDW) || ((SystemtimeClock - Left_LDW_End_time_stamp)/1000 < cooling_time_LDW) || (stVehicleParas->RightFlag == 0x01))
				return 0x03;
			else
#endif
				return Right_LDW;
		}
	}
}

