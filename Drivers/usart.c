/**
  ******************************************************************************
  * 文件名  Usart.c
  * 作  者  ChipON_AE/FAE_Group
  * 日  期  2019-10-19
  * 描  述  该文件提供了串口例程相关配置函数，包括
  *          + 串口发送
  *          + 串口异步配置
  *          + 串口同步配置
  *          + 串口接收中断使能
  ******************************************************************************/
#include "usart.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <get_parameter.h>
#include <set_parameter.h>
#include "dma.h"
/* ------------------------全局变量------------------------------- */
uint8_t 			User_Rxbuffer[RXBUFFER_SIZE]  =  {0};
volatile uint16_t 	User_Rxcount  =  0;

//gcz 2022-05-27
uint8_t 			User_Rxbuffer_BT[256]  =  {0};
volatile uint16_t 	User_Rxcount_BT  =  0;

/* ------------------------全局函数------------------------------- */
void 	USART2_Init();
void 	USART4_Init();

void USART_Init()
{
	/*************************** USART0/EC200U-CN 4G模块/暂定/230400bps **********************************/
	USART0_Init();
	INT_Interrupt_Priority_Config(INT_USART0, 0, 0);
	USART_Async_config(USART0_SFR, EC200U_BAUD);
	Enable_Usart_Interrupt(USART0_SFR, INT_USART0);
	// 增加空闲中断
	USART_Receive_Idle_Frame_Config(USART0_SFR, TRUE);
	USART_IDLE_INT_Enable(USART0_SFR, TRUE);

	DMA_USART0_Init();					// USART0_DMA初始化
	Enable_DMA_USART0(TRUE);

	/*************************** USART1/串口1 用户调试口/RS232/230400 bps**********************************/
	USART1_Init();
	INT_Interrupt_Priority_Config(INT_USART1, 1, 0);
	USART_Async_config(USART1_SFR, USART1_BAUD);
	Enable_Usart_Interrupt(USART1_SFR, INT_USART1);

	DMA_USART1_Init();					// USART1_DMA初始化
	Enable_DMA_USART1(TRUE);
	USART_SendData(USART1_SFR ,0x00);	// 启动  usart1-dma 发送
	// 增加空闲中断
	USART_Receive_Idle_Frame_Config(USART1_SFR,TRUE);
	USART_IDLE_INT_Enable(USART1_SFR,TRUE);

	/*************************** USART2/备用超声波雷达串口 /RS232/115200bps**********************************/
	USART2_Init();
	INT_Interrupt_Priority_Config(INT_USART2,3,3);
	USART_Async_config(USART2_SFR, USART2_BAUD);
	Enable_Usart_Interrupt(USART2_SFR, INT_USART2);
	USART_Receive_Idle_Frame_Config(USART2_SFR,TRUE);
	USART_IDLE_INT_Enable(USART2_SFR,TRUE);

	/*************************** USART3/RS485/预留 **********************************/


	/*************************** USART4/外接BT/TTL/9600bps**********************************/
	USART4_Init();
	INT_Interrupt_Priority_Config(INT_USART4, 3, 3);
	USART_Async_config(USART4_SFR, USART4_BAUD);
	USART_RDR_INT_Enable(USART4_SFR, TRUE);
	INT_Interrupt_Enable(INT_USART4, TRUE);
	USART_ReceiveData(USART4_SFR);		//清接收标志位
}

/**
  * 描述  串口初始化, 默认使用外设高速时钟16M。
  * 输入  串口波特率, 当前波特小于等于1M
  * 返回  无。
*/
void USART0_Init()
{
	GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_0, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_1, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOA_SFR, GPIO_Pin_Num_0, GPIO_RMP_AF5_USART0);	  // 重映射为USART1
	GPIO_Pin_RMP_Config (GPIOA_SFR, GPIO_Pin_Num_1, GPIO_RMP_AF5_USART0);     // 重映射为USART1
	GPIO_Pin_Lock_Config (GPIOA_SFR, GPIO_PIN_MASK_0, TRUE);                  // 配置锁存
	GPIO_Pin_Lock_Config (GPIOA_SFR, GPIO_PIN_MASK_1, TRUE);                  // 配置锁存
}

void USART1_Init()
{
	GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_15, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOE_SFR, GPIO_PIN_MASK_0, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOA_SFR, GPIO_Pin_Num_15, GPIO_RMP_AF5_USART1);	  // 重映射为USART1
	GPIO_Pin_RMP_Config (GPIOE_SFR, GPIO_Pin_Num_0, GPIO_RMP_AF5_USART1);     // 重映射为USART1
	GPIO_Pin_Lock_Config (GPIOA_SFR, GPIO_PIN_MASK_15, TRUE);                  // 配置锁存
	GPIO_Pin_Lock_Config (GPIOE_SFR, GPIO_PIN_MASK_0, TRUE);                  // 配置锁存
}

void USART2_Init()
{
	GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_8, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_9, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOA_SFR, GPIO_Pin_Num_8, GPIO_RMP_AF5_USART2);	  // 重映射为USART2
	GPIO_Pin_RMP_Config (GPIOA_SFR, GPIO_Pin_Num_9, GPIO_RMP_AF5_USART2);     // 重映射为USART2
	GPIO_Pin_Lock_Config (GPIOA_SFR, GPIO_PIN_MASK_8, TRUE);                  // 配置锁存
	GPIO_Pin_Lock_Config (GPIOA_SFR, GPIO_PIN_MASK_9, TRUE);                  // 配置锁存
}

void USART4_Init()
{
	GPIO_Write_Mode_Bits(GPIOD_SFR, GPIO_PIN_MASK_3, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOD_SFR, GPIO_PIN_MASK_4, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOD_SFR, GPIO_Pin_Num_3, GPIO_RMP_AF6_USART4);	  // 重映射为USART4
	GPIO_Pin_RMP_Config (GPIOD_SFR, GPIO_Pin_Num_4, GPIO_RMP_AF6_USART4);     // 重映射为USART4
	GPIO_Pin_Lock_Config (GPIOD_SFR, GPIO_PIN_MASK_3, TRUE);                  // 配置锁存
	GPIO_Pin_Lock_Config (GPIOD_SFR, GPIO_PIN_MASK_4, TRUE);                  // 配置锁存
}

/**
  * 描述   串口发送
  * 输入   USARTx:   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  *      Databuf：   指向发送数据的指针
  *      length：      发送的长度
  * 返回   无
  */
void USART_Send(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length)
{
	uint32_t i;
	for(i = 0; i<length; i++)
	{
		USART_SendData(USARTx, Databuf[i]);					// 串口发送
		while(!USART_Get_Transmitter_Empty_Flag(USARTx));	// 发送完成标志
	}
}

/**
  * 描述  串口异步全双工配置(默认8bit收发使能  全双工 9600)
  * 输入   指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  * 返回   无
  */
void USART_Async_config(USART_SFRmap *USARTx, uint32_t baud)
{
	USART_InitTypeDef USART_InitStructure;

	USART_Struct_Init(&USART_InitStructure);
    USART_InitStructure.m_Mode 			= USART_MODE_FULLDUPLEXASY;    	// 全双工
    USART_InitStructure.m_TransferDir 	= USART_DIRECTION_FULL_DUPLEX; 	// 传输方向
    USART_InitStructure.m_WordLength 	= USART_WORDLENGTH_8B;      	// 8位数据
    USART_InitStructure.m_StopBits 		= USART_STOPBITS_1;        		// 1位停止位
    USART_InitStructure.m_BaudRateBRCKS = USART_CLK_HFCLK;        		// 内部高频时钟作为 USART波特率发生器时钟

    /* 波特率  = Fck/(16*z（1+x/y)) 外设时钟内部高频16M*/
    // 4800    z:208    x:0    y:0
    // 9600    z:104    x:0    y:0
    // 19200   z:52     x:0    y:0
    // 38400   z:10     x:8    y:5
    // 57600   z:5      x:5    y:2
    // 115200  z:8      x:1    y:13
    // 230400  z:4      x:1    y:13
    // 波特率115200
    switch(baud)
    {
    case 4800:
        USART_InitStructure.m_BaudRateInteger 		= 208;	// USART波特率整数部分z，取值为0~65535
        USART_InitStructure.m_BaudRateNumerator		= 0;   	// USART波特率小数分子部分x，取值为0~0xF
        USART_InitStructure.m_BaudRateDenominator 	= 0;   	// USART波特率小数分母部分y，取值为0~0xF
        break;
    case 9600:
        USART_InitStructure.m_BaudRateInteger 		= 104;	// USART波特率整数部分z，取值为0~65535
        USART_InitStructure.m_BaudRateNumerator 	= 0;    // USART波特率小数分子部分x，取值为0~0xF
        USART_InitStructure.m_BaudRateDenominator 	= 0; 	// USART波特率小数分母部分y，取值为0~0xF
        break;
    case 19200:
        USART_InitStructure.m_BaudRateInteger 		= 52;  	// USART波特率整数部分z，取值为0~65535
        USART_InitStructure.m_BaudRateNumerator 	= 0;   	// USART波特率小数分子部分x，取值为0~0xF
        USART_InitStructure.m_BaudRateDenominator 	= 0;   	// USART波特率小数分母部分y，取值为0~0xF
        break;
    case 38400:
        USART_InitStructure.m_BaudRateInteger 		= 10; 	// USART波特率整数部分z，取值为0~65535
        USART_InitStructure.m_BaudRateNumerator 	= 8;  	// USART波特率小数分子部分x，取值为0~0xF
        USART_InitStructure.m_BaudRateDenominator 	= 5;  	// USART波特率小数分母部分y，取值为0~0xF
    	break;
    case 57600:
        USART_InitStructure.m_BaudRateInteger 		= 5;   	// USART波特率整数部分z，取值为0~65535
        USART_InitStructure.m_BaudRateNumerator 	= 5; 	// USART波特率小数分子部分x，取值为0~0xF
        USART_InitStructure.m_BaudRateDenominator 	= 2;  	// USART波特率小数分母部分y，取值为0~0xF
        break;
    case 115200:
    	USART_InitStructure.m_BaudRateInteger 		= 8;   	// USART波特率整数部分z，取值为0~65535
    	USART_InitStructure.m_BaudRateNumerator 	= 1;   	// USART波特率小数分子部分x，取值为0~0xF
    	USART_InitStructure.m_BaudRateDenominator 	= 13; 	// USART波特率小数分母部分y，取值为0~0xF
    	break;
    case 230400:
    	USART_InitStructure.m_BaudRateInteger 		= 4;   	// USART波特率整数部分z，取值为0~65535
    	USART_InitStructure.m_BaudRateNumerator 	= 1;   	// USART波特率小数分子部分x，取值为0~0xF
    	USART_InitStructure.m_BaudRateDenominator 	= 13;	// USART波特率小数分母部分y，取值为0~0xF
    	break;
    default:
    	USART_InitStructure.m_BaudRateInteger 		= 8;  	// USART波特率整数部分z，取值为0~65535
    	USART_InitStructure.m_BaudRateNumerator 	= 1;   	// USART波特率小数分子部分x，取值为0~0xF
    	USART_InitStructure.m_BaudRateDenominator 	= 13; 	// USART波特率小数分母部分y，取值为0~0xF
    	break;
    }
	USART_Reset(USARTx);                                   	// USARTx复位
	USART_Configuration(USARTx, &USART_InitStructure);  	// USARTx配置
    USART_Passageway_Select_Config(USARTx, USART_U7816R_PASSAGEWAY_TX0);	// UASRTx选择TX0通道
	USART_Clear_Transmit_BUFR_INT_Flag(USARTx);         	// USARTx发送BUF清零
	USART_RESHD_Enable (USARTx,  TRUE);						// 使能RESHD位
	USART_Cmd(USARTx, TRUE);                            	// USARTx使能
}

/**
  * 描述   串口接收中断配置
  * 输入   USARTx:指向USART内存结构的指针，取值为USART0_SFR~USART8_SFR
  *      Peripheral:外设或内核中断向量编号，取值范围为：
  *                 枚举类型InterruptIndex中的外设中断向量编号
  * 返回   无
  */
void Enable_Usart_Interrupt(USART_SFRmap *USARTx, InterruptIndex Peripheral)
{
	USART_RDR_INT_Enable(USARTx, TRUE);
	INT_Interrupt_Enable(Peripheral, TRUE);
	USART_ReceiveData(USARTx);//清接收标志位

	//USART_Clear_Transmit_BUFR_INT_Flag(USARTx);
	USART_Receive_Idle_Frame_Config(USARTx, TRUE);
	USART_IDLE_INT_Enable(USARTx, TRUE);

//	INT_All_Enable(TRUE);
//	fprintf(USART1_STREAM, "Enable_Usart_Interrupt()\r\n");
}

/*
 * 关闭串口中断
 */
void Disable_Usart_Interrupt(USART_SFRmap *USARTx, InterruptIndex Peripheral)
{
	USART_RDR_INT_Enable(USARTx, FALSE);
	INT_Interrupt_Enable(Peripheral, FALSE);
	USART_ReceiveData(USARTx);//清接收标志位
//	INT_All_Enable(FALSE);
//	fprintf(USART1_STREAM, "Disable_Usart_Interrupt()\r\n");
}


/*
 * 清空USART1缓冲区
 * 返回：无
 */
void Clear_UART1_Buffer()
{
	memset(User_Rxbuffer, 0, sizeof(User_Rxbuffer));
	User_Rxcount = 0;
}
void Clear_UART4_Buffer()
{
	memset(User_Rxbuffer_BT, 0, sizeof(User_Rxbuffer_BT));
	User_Rxcount_BT = 0;
}

/*
 * 打印数字接口
 * 参数1：关键提示语
 * 参数2：打印数字
 * 参数3：控制打印格式
 * 参数4：是否使用dma打印
 */
void USART1_Bebug_Print_Num(uint8_t *mark_info, uint32_t int_data, uint8_t print_type, uint8_t use_dma)
{
	if(strlen(mark_info) >= 90) return ;

	uint8_t print_info[100] = {0};

	switch(print_type){
	case 0: sprintf(print_info, "%s:%d\t", mark_info, int_data); break;
	case 1: sprintf(print_info, "%s:%d\r\n", mark_info, int_data); break;
	case 2: sprintf(print_info, "%s:%02X\r\n", mark_info, int_data);break;
	case 3: sprintf(print_info, "%s:%08X\r\n", mark_info, int_data);break;
	}

	if(use_dma) Usart1_DMA_Transmit(print_info, strlen(print_info));
	else USART_Send(USART1_SFR, print_info, strlen(print_info));
}
/*
 * 打印字符串
 * 参数1：关键提示语
 * 参数2：打印字符串
 * 参数3：是否使用dma打印
 */
void USART1_Bebug_Print(uint8_t *mark_info, uint8_t *string, uint8_t use_dma)
{
	if(strlen(mark_info) + strlen(string) >= 136) return ;

	uint8_t print_info[140] = {0};

	sprintf(print_info, "[%s]%s\r\n", mark_info, string);

	if(use_dma) Usart1_DMA_Transmit(print_info, strlen(print_info));
	else USART_Send(USART1_SFR, print_info, strlen(print_info));


}
/*
 * 打印float类型接口
 * 参数1：关键提示语
 * 参数2：打印float数据
 * 参数3：控制打印格式
 * 参数4：是否使用dma打印
 */
void USART1_Bebug_Print_Flt(uint8_t *mark_info, float float_data, uint8_t print_type, uint8_t use_dma)
{
	if(strlen(mark_info) >= 40) return ;

	uint8_t print_info[50] = {0};

	switch(print_type){
	case 0: sprintf(print_info, "%s: %0.2f\t", mark_info, float_data); break;
	case 1: sprintf(print_info, "%s: %0.2f\r\n", mark_info, float_data); break;
	case 2: sprintf(print_info, "%s: %0.8f\t", mark_info, float_data); break;
	case 3: sprintf(print_info, "%s: %0.8f\r\n", mark_info, float_data); break;
	}

	if(use_dma) Usart1_DMA_Transmit(print_info, strlen(print_info));
	else USART_Send(USART1_SFR, print_info, strlen(print_info));
}
