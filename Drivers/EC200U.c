/*
 * 4G_model.h
 *
 *  Created on: 2021-7-10
 *      Author: chenq
 */

#include "usart.h"
#include "EC200U.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "stdlib.h"
#include "string.h"
#include "common.h"
#include "utc_time.h"
#include "flash.h"
#include "dma.h"
#include "usart.h"
#include "_4g_para_config.h"
//gcz   2022-06-04  自检所需状态机状态
enum
{
	STATE_SELF_CHK_SEND = 0,
	STATE_SELF_CHK_WAIT_RCV,
	STATE_SELF_CHK_RCV_FINISH,
};

/* -----------------------局部函数声明------------------------------- */
static bool Self_Check_EC200U_Baud();
bool Try_To_Connect_EC200U();
void Module_4G_Start();
/* ------------------------全局变量定义区------------------------------- */

//int 	baud_a[7] = {4800, 9600, 19200, 38400, 57600, 115200, 230400};
uint32_t baud_a[4] = {19200, 38400, 115200, 230400};
bool 	find_ec200u_baud = false;
extern UART_RCV_OPT g_st_uart0_rcv_obj;
#include "board_config.h"
#include "tool.h"

//gcz   2022-06-04  4G接收处理函数
bool dispose_4g_rcv_msg()
{
	bool res = false;
	Analysis_DMA_USART0_Data();

	if ((g_st_uart0_rcv_obj.handle_index_before != g_st_uart0_rcv_obj.handle_index_after))
	{
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"++++(%d)[%s]\r\n",g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len,
//																g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf);
		res = _4G_Message_Handle_Function((uint8_t *)g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf,
									g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len);
		memset(g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf,0,g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len);
		g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len = 0;
		g_st_uart0_rcv_obj.handle_index_after++;

		if(g_st_uart0_rcv_obj.handle_index_after >= BUFFER_NBR)
			g_st_uart0_rcv_obj.handle_index_after = 0;
	}
	return res;
}
//--------------------------------------------------------------------------------------
//延时函数 局部变量用volatile声明，否则可能会被优化
void delay_ms(uint32_t nms)
{
	volatile uint32_t i, j;
	for(i=0;i<nms;i++)
	{
		j=7000;
		while(j--);
	}
}

/*
 * 4G模块使能
 */
void Module_4G_Software_Start()
{
	// 软件重启上电
	EC200U_RESET(Bit_RESET);
	delay_ms(30);
	EC200U_RESET(Bit_SET);
	delay_ms(200);
	EC200U_RESET(Bit_RESET);
	delay_ms(30);

	USART1_Bebug_Print("4G Module", "Software Start.", 0);
}

/*
 * 4G模块使能
 */
void Module_4G_Enable()
{
	// 重新使能
	EC200U_POWERKEY(Bit_RESET);
	delay_ms(100);
	EC200U_POWERKEY(Bit_SET);
	delay_ms(50);
	USART1_Bebug_Print("4G Module", "Enable.", 0);
}

static bool Find_EC200U_Buad()
{
	volatile uint8_t index = 0;
	uint8_t times = 0;
	uint8_t cmd[20] = {0};
	uint8_t state = STATE_SELF_CHK_SEND;
	uint32_t cursystick = 0;
	bool flag = false,res = false;
	strcpy(cmd, "AT+IPR?\r\n");
	while(1)
	{
		if(index > 5) break ;

		// 切换波特率
		if(times == 0)
		{
			times = 1;
			switch(index){
			case 0:USART1_Bebug_Print("EXCHANGE BAUD", "19200", 0);break;
			case 1:USART1_Bebug_Print("EXCHANGE BAUD", "38400", 0);break;
			case 2:USART1_Bebug_Print("EXCHANGE BAUD", "115200", 0);break;
			case 3:USART1_Bebug_Print("EXCHANGE BAUD", "230400", 0);break;
			default:USART1_Bebug_Print("EXCHANGE BAUD", "other baud", 0);break;
			}

			USART0_Init();
			USART_Async_config(USART0_SFR, baud_a[index]);
			Enable_Usart_Interrupt(USART0_SFR, INT_USART0);
			USART_Receive_Idle_Frame_Config(USART0_SFR, TRUE);
			USART_IDLE_INT_Enable(USART0_SFR, TRUE);

			// 因为公用DMA0，所以要一起初始化USART1
			DMA_USART0_Init();
			Enable_DMA_USART0(FALSE);
			Enable_DMA_USART0(TRUE);
			delay_ms(1);
//			USART1_Init();
//			DMA_USART1_Init();
//			Enable_DMA_USART1(FALSE);
//			Enable_DMA_USART1(TRUE);
		}
		//gcz   2022-06-04  修改中断接收处理的机制
		switch(state)
		{
		case STATE_SELF_CHK_SEND:		EC200U_SendData(cmd, strlen(cmd), 1);// 发送数据
										cursystick = SystemtimeClock;
										state = STATE_SELF_CHK_WAIT_RCV;
										break;
		case STATE_SELF_CHK_WAIT_RCV:	if (dispose_4g_rcv_msg())
										{
											state = STATE_SELF_CHK_RCV_FINISH;
										}
										else
										{
											if ((SystemtimeClock - cursystick) > 100)
											{
												state = STATE_SELF_CHK_RCV_FINISH;
											}
										}
										break;
		case STATE_SELF_CHK_RCV_FINISH: if(find_ec200u_baud)
										{


											find_ec200u_baud = false;
											USART1_Bebug_Print_Num("[4G Module]Find Baud", baud_a[index], 1, 0);
											return true;
										}
										else
										{
											// 1.2 若三次没有接收到+IPR就切换baud到115200bps,发送AT+IPR?\r\n，返回+IPR:;就发送AT+IPR=38400;&W\r\n,修改波特率到38400bps;进入下一阶段
											if(times >= 5)
											{
												index++;
												times = 0;
											}else times++;
										}
										state = STATE_SELF_CHK_SEND;
										break;
		}

	}
	return res;
}
/*
 * EC200U-CN 4G模块初始化
 */
void EC200U_INIT()
{
	// PB6:WAKEUP | PB7:RESET | PB8:POWERKEY | PB9:DISABLE | PD1:POWER_EN
	GPIO_Write_Mode_Bits(GPIOD_SFR, GPIO_PIN_MASK_1, GPIO_MODE_OUT);//POWER_EN
	GPIO_Set_Output_Data_Bits(GPIOD_SFR, GPIO_PIN_MASK_1, 1);
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_8, GPIO_MODE_OUT);//POWERKEY
	GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_8,  0);
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_7, GPIO_MODE_OUT);//RESET
	GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_7, 1);
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_6, GPIO_MODE_OUT);//WAKEUP
	GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_6, 1);
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_9, GPIO_MODE_OUT);//DISABLE
	GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_9,  0);

	Module_4G_Enable();	// 3S
	Module_4G_Software_Start();

	server_info.step = SET_BAUD;

	// 用默认115200bps发送AT指令，有回复就直接跳过；若没有返回就开始自检
	if(Try_To_Connect_EC200U()) return ;

	USART1_Bebug_Print("4G Module", "Start Self-Check.", 0);
	if(Self_Check_EC200U_Baud()){
		USART1_Bebug_Print("4G Module", "Self-Check Success.", 0);
	}else{
		USART1_Bebug_Print("4G Module", "Self-Check Failed.", 0);
	}

	USART0_Init();
	INT_Interrupt_Priority_Config(INT_USART0, 0, 0);
	USART_Async_config(USART0_SFR, EC200U_BAUD);	// 115200
	Enable_Usart_Interrupt(USART0_SFR, INT_USART0);

	// 因为公用DMA0，所以要一起初始化USART1
	DMA_USART0_Init();
	Enable_DMA_USART0(FALSE);
	Enable_DMA_USART0(TRUE);

	USART1_Init();
	INT_Interrupt_Priority_Config(INT_USART1, 1, 0);
	USART_Async_config(USART1_SFR, USART1_BAUD);
	Enable_Usart_Interrupt(USART1_SFR, INT_USART1);

	DMA_USART1_Init();
	Enable_DMA_USART1(FALSE);
	Enable_DMA_USART1(TRUE);
}
/*
 * 4G模块发送数据
 * 参数1：Databuf：
 * 参数2：数据长度
 * 参数3：1使用dma发送；0不使用dma
 * 返回：无
 */
void EC200U_SendData(uint8_t* Databuf,  uint16_t length, uint8_t use_dma)
{
	if(length == 0) return ;

//	Usart1_DMA_Transmit(Databuf, length);

	if(use_dma)	Usart0_DMA_Transmit(Databuf, length);
	else USART_Send(USART0_SFR, Databuf, length);
}

/*
 * 校验与更新波特率
 * 参数：
 * 返回：0；1沿用115200
 * 说明：EC200U-CN 默认波特率为115200bps，实际应用波特率为38400bps；
 */
static bool Self_Check_EC200U_Baud()
{
	uint8_t state = STATE_SELF_CHK_SEND;
	uint32_t cursystick = 0;
	if(Find_EC200U_Buad()){
		uint8_t cmd[20] = {0};
		sprintf(cmd , "AT+IPR=%d;&W\r\n", EC200U_BAUD);
		//gcz 2022-06-04   修改原有中断接收处理的方式
		bool flag = false;
		while(1){
			switch(state)
			{
			case STATE_SELF_CHK_SEND:		EC200U_SendData(cmd, strlen(cmd), 1);	// BST code high
											state = STATE_SELF_CHK_WAIT_RCV;
											cursystick = SystemtimeClock;
											break;
			case STATE_SELF_CHK_WAIT_RCV:	dispose_4g_rcv_msg();
											if(_4g_message_state == _4G_OK)
											{
												_4g_message_state = _4G_WAIT;
												USART1_Bebug_Print_Num("[4G Module] Set EC200U Baud", EC200U_BAUD, 1, 0);
												state = STATE_SELF_CHK_RCV_FINISH;
												return true;
											}
											if ((SystemtimeClock - cursystick) > 300)
											{
												return false;
											}
											break;
			}
		}
	}

	return false;
}

bool Try_To_Connect_EC200U()
{
	uint8_t cmd[5] = "AT\r\n";
	uint8_t send_times = 0;
	uint32_t cursystick = 0;
	uint8_t state = STATE_SELF_CHK_SEND;
	while(1){

		if(send_times > 20) break;
		//gcz   2022-06-04  修改中断接收处理的机制
		switch(state)
		{
		case STATE_SELF_CHK_SEND:		EC200U_SendData(cmd, strlen(cmd), 1);
										state = STATE_SELF_CHK_WAIT_RCV;
										cursystick = SystemtimeClock;
										break;
		case STATE_SELF_CHK_WAIT_RCV:	dispose_4g_rcv_msg();
										if(_4g_message_state == _4G_OK)
										{
											_4g_message_state = _4G_WAIT;
											USART1_Bebug_Print("4G Module", "Start Run...", 0);
											return true;
										}
										else
										{
											if ((SystemtimeClock - cursystick) >= 200)
											{
												state = STATE_SELF_CHK_SEND;
												send_times++;
											}
										}
										break;
		}
	}

	return false;
}
