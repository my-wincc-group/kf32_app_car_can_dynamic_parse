/*
 * dma.c
 *
 *  Created on: 2022-7-13
 *      Author: Administrator
 */
#include "dma.h"
#include "_4g_upgrade.h"
#include "at_parse_alg_para.h"
#include "_4g_para_config.h"
#include "usart_upgrade.h"
#include "usart.h"
#include "parse.h"
#include "tool.h"

uint8_t USART1_Dma_Buf_Rev[DMA_BUFF_SIZE] = {0};
uint8_t USART0_Dma_Buf_Rev[DMA_BUFF_SIZE] = {0};
USARTX_DMA usart0_dma = {0};
USARTX_DMA usart1_dma = {0};

#ifdef _SPI_DMA_W25Q16
	//uint8_t SPI_Send_Buf[SPI_BUF_SIZE] = {0};
	uint8_t SPI_Recv_Buf[SPI_BUF_SIZE] = {0}; //读数据缓存
	USARTX_DMA spi_dma_w25q16 = {0};
#endif
/******************************** USART0  DMA ********************************************/
/**
  * 描述  串口0的DMA初始化, 包括DMA发送和接收。
  * 输入  DMA0
  * 返回  无。
*/
void DMA_USART0_Init()
{
	DMA_InitTypeDef DMA_TX_INIT,DMA_RX_INIT;
	DMA_Reset (DMA0_SFR);
	DMA_Struct_Init( &DMA_TX_INIT );
	DMA_TX_INIT.m_Number = 1;
	DMA_TX_INIT.m_Channel = USART0_TXDMA_CHAN;
	DMA_TX_INIT.m_Direction = DMA_MEMORY_TO_PERIPHERAL;
	DMA_TX_INIT.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;
	DMA_TX_INIT.m_MemoryDataSize = DMA_DATA_WIDTH_8_BITS;
	DMA_TX_INIT.m_Priority = DMA_CHANNEL_HIGHER;//DMA_CHANNEL_LOWER;
	DMA_TX_INIT.m_PeripheralInc = 0;//TRUE;FALSE;
	DMA_TX_INIT.m_MemoryInc =TRUE; //TRUE;FALSE;
	DMA_TX_INIT.m_LoopMode = FALSE;//TRUE;FALSE;
	DMA_TX_INIT.m_BlockMode = DMA_TRANSFER_BYTE;//DMA_TRANSFER_BYTE;DMA_TRANSFER_BLOCK;
	DMA_TX_INIT.m_MemoryAddr = (uint32_t)0;
	DMA_TX_INIT.m_PeriphAddr = (uint32_t)&(USART0_SFR->TBUFR);

	DMA_Configuration (DMA0_SFR, &DMA_TX_INIT);

	DMA_Struct_Init( &DMA_RX_INIT );

	DMA_RX_INIT.m_Number = DMA_BUFF_SIZE;
	DMA_RX_INIT.m_Direction = DMA_PERIPHERAL_TO_MEMORY;//DMA_MEMORY_TO_PERIPHERAL;//
	DMA_RX_INIT.m_Priority = DMA_CHANNEL_LOWER; // DMA_CHANNEL_LOWER;
	DMA_RX_INIT.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;
	DMA_RX_INIT.m_MemoryDataSize = DMA_DATA_WIDTH_8_BITS;
	DMA_RX_INIT.m_PeripheralInc = 0;
	DMA_RX_INIT.m_MemoryInc = 1;
	DMA_RX_INIT.m_Channel = USART0_RXDMA_CHAN;
	DMA_RX_INIT.m_BlockMode = DMA_TRANSFER_BYTE;//DMA_TRANSFER_BYTE; //DMA_TRANSFER_BLOCK;//
	DMA_RX_INIT.m_LoopMode = 1;
	DMA_RX_INIT.m_PeriphAddr = (uint32_t)&USART0_SFR->RBUFR ; //T1
	DMA_RX_INIT.m_MemoryAddr = (uint32_t)USART0_Dma_Buf_Rev;

	DMA_Configuration (DMA0_SFR, &DMA_RX_INIT); //将参数写入DMAx寄存器
//	DMA_Memory_To_Memory_Enable (DMAx, USART1_RXDMA_CHAN, FALSE);
//	DMA_Finish_Transfer_INT_Enable(DMAx,USART1_RXDMA_CHAN,TRUE);
//	DMA_Memory_To_Memory_Enable (DMAx, USART1_TXDMA_CHAN, FALSE);
//	DMA_Finish_Transfer_INT_Enable(DMAx,USART1_TXDMA_CHAN,TRUE);
	usart0_dma.dma_recv_end_addr = (uint32_t)USART0_Dma_Buf_Rev;
	usart0_dma.dma_recv_front_addr = usart0_dma.dma_recv_end_addr;
}

void Enable_DMA_USART0(FunctionalState state)
{
	USART_Receive_DMA_INT_Enable(USART0_SFR, state);
	DMA_Channel_Enable(DMA0_SFR, USART0_RXDMA_CHAN, state);

	USART_Transmit_DMA_INT_Enable(USART0_SFR, state);
	DMA_Channel_Enable(DMA0_SFR, USART0_TXDMA_CHAN, state);

	if(state)
		USART_SendData(USART0_SFR, 0x00);	// 启动  usart0-dma 发送
}
/*
 * USART0利用DMA发送数据
 */
void Usart0_DMA_Transmit(volatile uint8_t *pData, uint16_t nSize)
{
	DMA_Memory_Start_Address_Config(DMA0_SFR, USART0_TXDMA_CHAN, (uint32_t)pData);
	DMA_Transfer_Number_Config(DMA0_SFR, USART0_TXDMA_CHAN, nSize);
//	DMA_Transfer_Mode_Config(DMA0_SFR, USART1_TXDMA_CHAN,DMA_TRANSFER_BLOCK);

	USART_Transmit_Data_Enable(USART0_SFR, TRUE);
	DMA_Channel_Enable(DMA0_SFR, USART0_TXDMA_CHAN, TRUE);
	USART_Transmit_DMA_INT_Enable(USART0_SFR, TRUE);

	//USART_SendData(USART0_SFR,0x00);
//	while(!DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, USART0_TXDMA_CHAN));

	while(!USART_Get_Transmitter_Empty_Flag(USART0_SFR));	//发送完成标志
}

void DMA_Print_ASCII(uint8_t *data, uint16_t data_len)
{
	USART1_Bebug_Print_Num("data_len", data_len, 1, 1);

	uint8_t print_info[12600] = {0};
	for(uint16_t i=0; i<data_len; i++){
		sprintf(&print_info[i*3], "%02X ", data[i]);
	}

	Usart1_DMA_Transmit(print_info, data_len);
}
void Analysis_DMA_USART0_Data()
{
	char *btStrx = NULL;

	if(usart0_dma.recv_idle_flag){
		usart0_dma.recv_idle_flag = 0;

		usart0_dma.dma_recv_front_addr = DMA_Get_Memory_Current_Address(DMA0_SFR, USART0_RXDMA_CHAN);
		usart0_dma.recv_data_len = usart0_dma.dma_recv_front_addr - usart0_dma.dma_recv_end_addr;

		if(usart0_dma.recv_data_len == 0) return ;
		else if(usart0_dma.recv_data_len < 0){
			usart0_dma.recv_data_len += DMA_BUFF_SIZE;
		}
//		USART1_Bebug_Print_Num("0recv_data_len", usart0_dma.recv_data_len, 1, 1);

		if(upgrade_info.rx_mode == RECV_BIN){
			if(usart0_dma.dma_recv_front_addr > usart0_dma.dma_recv_end_addr){
				memcpy((uint8_t *)down_pkg.data + down_pkg.cnt, (uint32_t*)usart0_dma.dma_recv_end_addr, usart0_dma.recv_data_len);
				down_pkg.cnt += usart0_dma.recv_data_len;
			}else{	// 分段取
				uint32_t remain_len = (uint32_t)USART0_Dma_Buf_Rev + DMA_BUFF_SIZE - usart0_dma.dma_recv_end_addr;
				memcpy((uint8_t *)down_pkg.data + down_pkg.cnt, (uint32_t*)usart0_dma.dma_recv_end_addr, remain_len);

				memcpy(((uint8_t *)down_pkg.data + remain_len + down_pkg.cnt), USART0_Dma_Buf_Rev, usart0_dma.recv_data_len - remain_len);
				down_pkg.cnt += usart0_dma.recv_data_len;
			}

			usart0_dma.dma_recv_end_addr = usart0_dma.dma_recv_front_addr;

//			USART1_Bebug_Print_Num("down_pkg.cnt", down_pkg.cnt, 1, 1);
		}else{
			while (usart0_dma.dma_recv_end_addr != usart0_dma.dma_recv_front_addr){
				if(upgrade_info.step != UPGRADE_SUB_DMN){ // 避免OTA时造成干扰
					uart_rcv_to_cache(USART0_Dma_Buf_Rev[usart0_dma.dma_recv_end_addr -  (uint32_t) (&USART0_Dma_Buf_Rev[0])]);
				}
				g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf[g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len] = USART0_Dma_Buf_Rev[usart0_dma.dma_recv_end_addr -  (uint32_t) (&USART0_Dma_Buf_Rev[0])];
				g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len++;

				//gcz  2022-06-02	合并lyj的缓存处理机制，优化封包机制
				// 接收到结束符(\r\n)后，就判断状态 usart0_buf[usart0_buf_len - 2] == '\r' &&
				if ((g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len == 2)
					&& (g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf[0] == '\r')
					&& (g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf[1] == '\n'))
				{
					g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len = 0;
					g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf[g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len - 1] = 0;
				}
				else if (((g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len > 2)
						&& (g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf[g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len - 2] == '\r')
						&& (g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf[g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len - 1] == '\n'))
						|| (g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len == (BUFFER_LEN - 1)))
				{
					g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf[g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len] = 0;
					g_st_uart0_rcv_obj.handle_index_before++;
					if (g_st_uart0_rcv_obj.handle_index_before >= BUFFER_NBR)
						g_st_uart0_rcv_obj.handle_index_before = 0;
				}
				usart0_dma.dma_recv_end_addr++;
				if (usart0_dma.dma_recv_end_addr - (uint32_t) (&USART0_Dma_Buf_Rev[0]) >= DMA_BUFF_SIZE){
					usart0_dma.dma_recv_end_addr = (uint32_t)&USART0_Dma_Buf_Rev[0] ;
				}
			}
		}

		if (g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len > 0)
		{
			g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf[g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_before].handle_buf_len] = 0;
			g_st_uart0_rcv_obj.handle_index_before++;
			if (g_st_uart0_rcv_obj.handle_index_before >= BUFFER_NBR)
				g_st_uart0_rcv_obj.handle_index_before = 0;
		}
	}
}
/******************************** USART1 DMA********************************************/
/**
  * 描述  串口1的DMA初始化, 包括DMA发送和接收。
  * 输入  DMA0
  * 返回  无。
*/
void DMA_USART1_Init()
{
	DMA_InitTypeDef DMA_TX_INIT,DMA_RX_INIT;
//	DMA_Reset (DMA0_SFR);
	DMA_Struct_Init( &DMA_TX_INIT );
	DMA_TX_INIT.m_Number = 1;
	DMA_TX_INIT.m_Channel = USART1_TXDMA_CHAN;
	DMA_TX_INIT.m_Direction = DMA_MEMORY_TO_PERIPHERAL;
	DMA_TX_INIT.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;
	DMA_TX_INIT.m_MemoryDataSize = DMA_DATA_WIDTH_8_BITS;
	DMA_TX_INIT.m_Priority = DMA_CHANNEL_HIGHER;//DMA_CHANNEL_LOWER;
	DMA_TX_INIT.m_PeripheralInc = 0;//TRUE;FALSE;
	DMA_TX_INIT.m_MemoryInc =TRUE; //TRUE;FALSE;
	DMA_TX_INIT.m_LoopMode = FALSE;//TRUE;FALSE;
	DMA_TX_INIT.m_BlockMode = DMA_TRANSFER_BYTE;//DMA_TRANSFER_BYTE;DMA_TRANSFER_BLOCK;
	DMA_TX_INIT.m_MemoryAddr = (uint32_t)0;
	DMA_TX_INIT.m_PeriphAddr = (uint32_t)&(USART1_SFR->TBUFR);

	DMA_Configuration (DMA0_SFR, &DMA_TX_INIT);

	DMA_Struct_Init( &DMA_RX_INIT );

	DMA_RX_INIT.m_Number = DMA_BUFF_SIZE;
	DMA_RX_INIT.m_Direction = DMA_PERIPHERAL_TO_MEMORY;//DMA_MEMORY_TO_PERIPHERAL;//
	DMA_RX_INIT.m_Priority = DMA_CHANNEL_LOWER; // DMA_CHANNEL_LOWER;
	DMA_RX_INIT.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;
	DMA_RX_INIT.m_MemoryDataSize = DMA_DATA_WIDTH_8_BITS;
	DMA_RX_INIT.m_PeripheralInc = 0;
	DMA_RX_INIT.m_MemoryInc = 1;
	DMA_RX_INIT.m_Channel = USART1_RXDMA_CHAN;
	DMA_RX_INIT.m_BlockMode = DMA_TRANSFER_BYTE;//DMA_TRANSFER_BYTE; //DMA_TRANSFER_BLOCK;//
	DMA_RX_INIT.m_LoopMode = 1;
	DMA_RX_INIT.m_PeriphAddr = (uint32_t)&USART1_SFR->RBUFR ; //T1
	DMA_RX_INIT.m_MemoryAddr = (uint32_t)USART1_Dma_Buf_Rev;

	DMA_Configuration (DMA0_SFR, &DMA_RX_INIT); //将参数写入DMAx寄存器
//	DMA_Memory_To_Memory_Enable (DMAx, USART1_RXDMA_CHAN, FALSE);
//	DMA_Finish_Transfer_INT_Enable(DMAx,USART1_RXDMA_CHAN,TRUE);
//	DMA_Memory_To_Memory_Enable (DMAx, USART1_TXDMA_CHAN, FALSE);
//	DMA_Finish_Transfer_INT_Enable(DMAx,USART1_TXDMA_CHAN,TRUE);
	usart1_dma.dma_recv_end_addr = (uint32_t)USART1_Dma_Buf_Rev;
	usart1_dma.dma_recv_front_addr = usart1_dma.dma_recv_end_addr;
}

void Enable_DMA_USART1(FunctionalState state)
{
	USART_Receive_DMA_INT_Enable(USART1_SFR, state);
	DMA_Channel_Enable(DMA0_SFR, USART1_RXDMA_CHAN, state);

	USART_Transmit_DMA_INT_Enable(USART1_SFR, state);
	DMA_Channel_Enable(DMA0_SFR, USART1_TXDMA_CHAN, state);

	USART_SendData(USART1_SFR ,0x00);	// 启动  usart1-dma 发送
}

/*
 * USART1利用DMA发送数据
 */
void Usart1_DMA_Transmit(volatile uint8_t *pData, uint16_t nSize)
{
	DMA_Memory_Start_Address_Config(DMA0_SFR, USART1_TXDMA_CHAN, (uint32_t)pData);
	DMA_Transfer_Number_Config(DMA0_SFR, USART1_TXDMA_CHAN, nSize);
//	DMA_Transfer_Mode_Config(DMA0_SFR, USART1_TXDMA_CHAN,DMA_TRANSFER_BLOCK);

	USART_Transmit_Data_Enable(USART1_SFR, TRUE);
	DMA_Channel_Enable(DMA0_SFR, USART1_TXDMA_CHAN, TRUE);
//	USART_Transmit_DMA_INT_Enable(USART1_SFR,TRUE);

	//USART_SendData(USART1_SFR,0x00);
//	while(!DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, USART1_TXDMA_CHAN));

	while(!USART_Get_Transmitter_Empty_Flag(USART1_SFR));//发送完成标志
}

void Analysis_DMA_USART1_Data()
{

	if(usart1_dma.recv_idle_flag){
		usart1_dma.recv_idle_flag = 0;

		usart1_dma.dma_recv_front_addr = DMA_Get_Memory_Current_Address(DMA0_SFR, USART1_RXDMA_CHAN);
		usart1_dma.recv_data_len = usart1_dma.dma_recv_front_addr - usart1_dma.dma_recv_end_addr;

		if(usart1_dma.recv_data_len == 0) return ;
		else if(usart1_dma.recv_data_len < 0){
			usart1_dma.recv_data_len += DMA_BUFF_SIZE;
		}

		if(upgrade_info.rx_mode == RECV_BIN){	// 线刷升级包（bin包）
#if 1
			if(usart1_dma.dma_recv_front_addr > usart1_dma.dma_recv_end_addr){
				memcpy((uint8_t *)down_pkg.data + down_pkg.cnt, (uint32_t*)usart1_dma.dma_recv_end_addr, usart1_dma.recv_data_len);
				down_pkg.cnt += usart1_dma.recv_data_len;
			}else{	// 分段取
				uint32_t remain_len = (uint32_t)USART1_Dma_Buf_Rev + DMA_BUFF_SIZE - usart1_dma.dma_recv_end_addr;
				memcpy((uint8_t *)down_pkg.data + down_pkg.cnt, (uint32_t*)usart1_dma.dma_recv_end_addr, remain_len);

				memcpy(((uint8_t *)down_pkg.data + remain_len + down_pkg.cnt), USART1_Dma_Buf_Rev, usart1_dma.recv_data_len - remain_len);
				down_pkg.cnt += usart1_dma.recv_data_len;
			}

			usart1_dma.dma_recv_end_addr = usart1_dma.dma_recv_front_addr;

//			USART1_Bebug_Print_Num("down_pkg.cnt", down_pkg.cnt, 1, 1);
#endif
		}else{
			// 异常处理：防止栈溢出
			if(usart1_dma.recv_data_len > RXBUFFER_SIZE) usart1_dma.recv_data_len = RXBUFFER_SIZE;

			if(usart1_dma.dma_recv_front_addr > usart1_dma.dma_recv_end_addr){
				memcpy(User_Rxbuffer, (uint32_t*)usart1_dma.dma_recv_end_addr, usart1_dma.recv_data_len);
				User_Rxcount = usart1_dma.recv_data_len;
			}else{	// 分段取
				uint32_t remain_len = (uint32_t)USART1_Dma_Buf_Rev + DMA_BUFF_SIZE - usart1_dma.dma_recv_end_addr;
				memcpy(User_Rxbuffer, (uint32_t*)usart1_dma.dma_recv_end_addr, remain_len);

				memcpy(User_Rxbuffer + remain_len, USART1_Dma_Buf_Rev, usart1_dma.recv_data_len - remain_len);
				User_Rxcount = usart1_dma.recv_data_len;
			}
			// 测试打印
//			USART1_Bebug_Print_Num("User_Rxcount", User_Rxcount, 1, 1);

			usart1_dma.dma_recv_end_addr = usart1_dma.dma_recv_front_addr;

			// 字符串处理
			if(strstr(User_Rxbuffer, "ZKHYGET*4GINFO:")){
				return ;
			}

			int buf_index = 0;
			while(User_Rxbuffer[buf_index] != '\0') {
			  Parse_ReceiveData(User_Rxbuffer[buf_index], USART1_STREAM);
			  buf_index++;
			}
			Parse_ReceiveData('\0', USART1_STREAM);

			char *btStrx = NULL;
			if((btStrx = strstr(User_Rxbuffer, "AT+")) || (btStrx = strstr(User_Rxbuffer, "ZKHYCHK*RALGPARA")) \
					|| (btStrx = strstr(User_Rxbuffer, "ZKHYCHK*BTAPP"))|| (btStrx = strstr(User_Rxbuffer,"ZKHYCHK*SENSORPARA"))){	// 分出AT指令
				g_st_uart_opt.usart_used_id = UART_1_ID;
				sprintf(g_st_uart_opt.usart1_At_Buf,"%s",btStrx);
//				fprintf(USART1_STREAM,"[%s]\r\n",g_st_uart_opt.usart1_At_Buf);
//				USART1_Bebug_Print("DMA AT", g_st_uart_opt.usart1_At_Buf, 1);
				Clear_UART1_Buffer();

				//gcz 2022-08-12 勿删！！！
				g_DetectionCfgWay = 0;
				if (g_st_detection_cal_sp_para.flag == 1)
					g_u8_instlltion_detection_cmd_src = CMD_SRC_DEBUG;

				if (g_st_detection_ult_radra_para.flag == 1)
					g_u8_check_ult_radra_cmd_src = CMD_SRC_DEBUG;
			}
		}
	}
	return ;
}

/******************************** SPI DMA ********************************************/
#ifdef _SPI_DMA_W25Q16
/**
  * 描述    SPI_DMA配置
  * 输入   SPIx：指向SPI内存结构的指针，取值为SPI0_SFR~SPI3_SFR
  * 返回   无
  */
void SPI2_DMA_Configuration(SPI_SFRmap* SPIx)
{
	SPI_InitTypeDef newStruct_SPI;
	/*SPI配置*/
//#if SPI_MASTER
	newStruct_SPI.m_Mode = SPI_MODE_MASTER_CLKDIV4;                   	//主模式主时钟4分频 18M
//#else
//	newStruct_SPI.m_Mode = SPI_MODE_SLAVE;                            	//从模式
//#endif
 	newStruct_SPI.m_Clock 		= SPI_CLK_SCLK;							//SPI主频时钟
	newStruct_SPI.m_FirstBit 	= SPI_FIRSTBIT_MSB;					  	//MSB
	newStruct_SPI.m_CKP 		= SPI_CKP_HIGH;                    		//SCK空闲为高
	newStruct_SPI.m_CKE 		= SPI_CKE_1EDGE;              			//第一个时钟开始发送数据
	newStruct_SPI.m_DataSize 	= SPI_DATASIZE_8BITS;              		//8bit
	newStruct_SPI.m_BaudRate 	= 0x12B;                                //Fck_spi=Fck/2(m_BaudRate+1)=10us
    SPI_Reset(SPIx);											      	//复位模块
    SPI_Configuration(SPIx, &newStruct_SPI);					      	//写入结构体配置

    /*DMA配置*/
    /*接收通道*/
	DMA_InitTypeDef dmaNewStruct;
	DMA_Reset(DMA0_SFR);
    dmaNewStruct.m_Number 			= SPI_BUF_SIZE;                     //传输数据个数: SPI_BUF_SIZE
    dmaNewStruct.m_Direction 		= DMA_PERIPHERAL_TO_MEMORY;       	//配置 DMA传输方向：外设到内存
    dmaNewStruct.m_Priority 		= DMA_CHANNEL_HIGHER;				//配置 DMA通道优先级：高优先级
    dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;        	//配置 外设数据位宽： 8位宽
    dmaNewStruct.m_MemoryDataSize 	= DMA_DATA_WIDTH_8_BITS;            //配置存储器数据位宽:8位宽
    dmaNewStruct.m_PeripheralInc 	= FALSE; // FALSE; 					//配置 外设地址增量模式使能: 禁止
    dmaNewStruct.m_MemoryInc 		= TRUE;								//配置 存储器地址增量模式使能: 允许
    dmaNewStruct.m_Channel 			= SPI2_RXDMA_CHAN;						//配置 DMA通道选择:通道  RX
    dmaNewStruct.m_BlockMode 		= DMA_TRANSFER_BYTE;	         	//配置 数据块传输模式：字节传输
    dmaNewStruct.m_LoopMode 		= TRUE;//FALSE;TRUE		       		//配置 循环模式使能: 允许
    dmaNewStruct.m_PeriphAddr 		= (uint32_t)&(SPIx->BUFR);     		//配置 外设起始地址
    dmaNewStruct.m_MemoryAddr 		= (uint32_t)&SPI_Recv_Buf;    		//配置 内存起始地址

    DMA_Configuration (DMA0_SFR, &dmaNewStruct);                       	//写入配置

//    SPI_Receive_DMA_INT_Enable (SPIx, TRUE);                        	//接收DMA
//	DMA_Channel_Enable (DMA0_SFR, SPI2_RXDMA_CHAN, TRUE);					  	//DMA通道使能
//	DMA_Finish_Transfer_INT_Enable(DMA0_SFR, SPI2_RXDMA_CHAN, TRUE);		    //接收完整数据DMA中断

	DMA_Struct_Init(&dmaNewStruct);
	/*发送通道*/
	dmaNewStruct.m_Number 			= SPI_BUF_SIZE; 					//传输数据个数: SPI_BUF_SIZE
    dmaNewStruct.m_Direction 		= DMA_MEMORY_TO_PERIPHERAL;      	//配置 DMA传输方向：内存到外设
    dmaNewStruct.m_Priority 		= DMA_CHANNEL_HIGHER;
    dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;        	//配置 外设数据位宽： 8位宽
	dmaNewStruct.m_MemoryDataSize 	= DMA_DATA_WIDTH_8_BITS;            //配置存储器数据位宽:8位宽
	dmaNewStruct.m_PeripheralInc 	= FALSE; // FALSE; 					//配置 外设地址增量模式使能: 禁止
	dmaNewStruct.m_MemoryInc 		= TRUE;								//配置 存储器地址增量模式使能: 允许
    dmaNewStruct.m_Channel 			= SPI2_TXDMA_CHAN;						//配置 DMA通道选择:通道  TX
    dmaNewStruct.m_BlockMode 		= DMA_TRANSFER_BYTE;	         	//配置 数据块传输模式：字节传输
    dmaNewStruct.m_LoopMode 		= FALSE;			          		//配置 循环模式使能: 禁止
    dmaNewStruct.m_PeriphAddr 		= (uint32_t)&(SPIx->BUFR);   		//配置 外设起始地址
//#if SPI_MASTER
//    dmaNewStruct.m_MemoryAddr 		= (uint32_t)&SPI_Send_Buf;          	//配置 内存起始地址
    dmaNewStruct.m_MemoryAddr 		= (uint32_t)0;          	//配置 内存起始地址
//#else
//    dmaNewStruct.m_MemoryAddr	 	= (uint32_t)&Tx_slave;           	//配置 内存起始地址
//#endif
    DMA_Configuration (DMA0_SFR, &dmaNewStruct);				        //写入配置

//	SPI_Transmit_DMA_INT_Enable(SPIx, TRUE);							//发送DMA
//    DMA_Channel_Enable (DMA0_SFR, SPI2_TXDMA_CHAN, TRUE);				    	//DMA通道使能
//	DMA_Finish_Transfer_INT_Enable(DMA0_SFR, SPI2_TXDMA_CHAN, TRUE);		    //接收完整数据DMA中断

	SPI_Cmd(SPIx, TRUE);											    //使能SPIx

	Enable_SPI2_DMA(TRUE);

	spi_dma_w25q16.dma_recv_end_addr = (uint32_t)SPI_Recv_Buf;
	spi_dma_w25q16.dma_recv_front_addr = spi_dma_w25q16.dma_recv_end_addr;
	spi_dma_w25q16.recv_data_len = 0;
	spi_dma_w25q16.recv_idle_flag = 0;
	//第一次配置DMA，需填充数据启动DMA发送
//	SPI_I2S_SendData8(SPI_W25q16, 0xFF);
//	SPI_I2S_SendData32(SPI_W25q16, 0xFF);
}

void Enable_SPI2_DMA(FunctionalState state)
{
	SPI_Receive_DMA_INT_Enable(SPI_W25q16, state);
	DMA_Channel_Enable(DMA0_SFR, SPI2_RXDMA_CHAN, state);

	SPI_Transmit_DMA_INT_Enable(SPI_W25q16, state);
	DMA_Channel_Enable(DMA0_SFR, SPI2_TXDMA_CHAN, state);
}

/*
 * 参数1：发送数据
 * 参数2：数据长度
 * 参数3：标志位：0写数据（指令）；1直接填充0xFF
 */
void SPI2_DMA_Transmit(uint8_t *data, uint16_t data_len, uint8_t flag)
{
#if 1
	// 发送
	switch(flag){
	case 0:{	// 写内容(数据、指令和地址)，发送指令和地址时（写数据时除外）W25q16模块仅是解析不会反馈数据，也就是spi接收buf数据不会增长；
		for(uint16_t i=0; i<data_len; i++){
			SPI_I2S_SendData8(SPI_W25q16, data[i]);
		}
	}break;
	case 1:{	// 读数据(填充)，发送多少数据，spi的接收buf就会有多少数据的增长；
		for(uint16_t i=0; i<data_len; i++){
			SPI_I2S_SendData8(SPI_W25q16, 0xFF);
		}
	}break;
	}
	// 等待发送完成
	while(SPI_Get_BUSY_Flag(SPI_W25q16) == SET);
#else
	for(uint16_t i=0; i<nSize; i++){
		SPI_Send_Buf[i] = pData[i];
	}
//	DMA_Memory_Start_Address_Config(DMA0_SFR, SPI2_TXDMA_CHAN, (uint32_t)pData);
//	DMA_Transfer_Number_Config(DMA0_SFR, SPI2_TXDMA_CHAN, nSize);
	DMA_Transfer_Mode_Config(DMA0_SFR, SPI2_TXDMA_CHAN, DMA_TRANSFER_BYTE);

//	DMA_Channel_Enable(DMA0_SFR, SPI2_TXDMA_CHAN, TRUE);
//	SPI_Transmit_DMA_INT_Enable(SPI_W25q16, TRUE);

	SPI_Transmit_DMA_INT_Enable(SPI_W25q16, TRUE);							//发送DMA
	DMA_Channel_Enable (DMA0_SFR, SPI2_TXDMA_CHAN, TRUE);				    	//DMA通道使能
	DMA_Finish_Transfer_INT_Enable(DMA0_SFR, SPI2_TXDMA_CHAN, TRUE);		    //接收完整数据DMA中断

	while(SPI_Get_BUSY_Flag(SPI_W25q16) == SET);
#endif
}

/*
 * 参数1：需要获取的数据
 * 参数2：获取数据的长度
 * 参数3：发送数据总长度-获取数据长度
 * 说明：接收缓冲区是环形BUF
 * 注：若实际中参数2和参数3值相等，根据实际调试必须满足：参数2=参数3-1
 */
void From_SPI2_DMA_Buf_Get_Data(uint8_t *get_data, uint16_t get_data_len, uint16_t diff)
{
	spi_dma_w25q16.dma_recv_front_addr = DMA_Get_Memory_Current_Address(DMA0_SFR, SPI2_RXDMA_CHAN);
	// [分段拷贝]接收长度超出了接收buf大小，就需从头开始拷贝数据；否则，直接拷贝
	spi_dma_w25q16.recv_data_len = spi_dma_w25q16.dma_recv_front_addr - spi_dma_w25q16.dma_recv_end_addr;

	if(spi_dma_w25q16.recv_data_len > 0){	// 不分段
		USART1_Bebug_Print_Num("SPI2 Len", spi_dma_w25q16.recv_data_len, 1, 1);

		// 测试数据长度
		if(spi_dma_w25q16.recv_data_len == (diff+get_data_len)) USART1_Bebug_Print("1GET SPI2 LEN", "==", 1);
		else{
			USART1_Bebug_Print("1GET SPI2 LEN", "!=", 1);
			USART1_Bebug_Print_Num("recv_data_len", spi_dma_w25q16.recv_data_len, 1, 1);
			for(uint16_t i=0; i<spi_dma_w25q16.recv_data_len; i++){
				fprintf(USART1_STREAM, "%02X ", SPI_Recv_Buf[i]);
			}
			fprintf(USART1_STREAM, "\r\n");
//			return ;
		}

		USART1_Bebug_Print_Num("end length", spi_dma_w25q16.dma_recv_end_addr - (uint32_t)SPI_Recv_Buf, 1, 1);

		// 数据拷贝
		memcpy(get_data, SPI_Recv_Buf + (spi_dma_w25q16.dma_recv_end_addr - (uint32_t)SPI_Recv_Buf) \
				+ diff, get_data_len);

		spi_dma_w25q16.dma_recv_end_addr = spi_dma_w25q16.dma_recv_front_addr;
	}else{									// 分段
		spi_dma_w25q16.recv_data_len += DMA_BUFF_SIZE;

		// 测试数据长度
		if(spi_dma_w25q16.recv_data_len == get_data_len) USART1_Bebug_Print("2GET SPI2 LEN", "==", 1);
		else  USART1_Bebug_Print("2GET SPI2 LEN", "!=", 1);

		// 这个地方也需要做优化：指令长度超不超过剩余字节
		// 数据拷贝
		uint32_t remain_length = (uint32_t) (&SPI_Recv_Buf[0]) + DMA_BUFF_SIZE - spi_dma_w25q16.dma_recv_end_addr;

		memcpy(get_data, SPI_Recv_Buf + spi_dma_w25q16.dma_recv_end_addr, remain_length);
//		spi_dma_w25q16.dma_recv_end_addr = (uint32_t) (&SPI_Recv_Buf[0]);

		memcpy(get_data + remain_length, SPI_Recv_Buf, spi_dma_w25q16.dma_recv_front_addr);
		spi_dma_w25q16.dma_recv_end_addr = spi_dma_w25q16.dma_recv_front_addr;
	}
}
#endif
