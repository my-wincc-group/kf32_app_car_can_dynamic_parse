/*
 * at_parse_alg_para.c
 *
 *  Created on: 2021-12-30
 *      Author: Administrator
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "cat.h"
#include "usart.h"
#include "upgrade_common.h"
#include "at_parse_alg_para.h"
#include "common.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "gpio.h"
#include "_4g_para_config.h"
#include "_4g_data_upload.h"
#include "aebs_version_management.h"
#include "wheelSpeed.h"
#include "AEB_upload_info.h"
#include "aeb_sensor_management.h"
#include "car_dvr.h"
#include "serial_app.h"
#include "tool.h"
/**************************** 宏/枚举 ****************************************************/

/**************************** 结构体 ****************************************************/
typedef struct _MegneticValve {
  uint32_t clk; // 时间控制
  uint8_t swt; // 使能开关
  uint16_t enableTime; // 使能时间
  uint16_t disableTime; // 不使能时间
  uint8_t ctrlCnt; // 控制次数
  uint8_t enFlag; // 使能不使能切换标志
  uint16_t clk_1; // 判断中间值
  uint32_t clk_2; // 判断中间值
} MegneticValve;

MegneticValve megneticVal = { 0, false, 20, 230, 4, false, 0, 0 };

/**************************** 全局变量 ****************************************************/
KunLun_AEB_Para_Cfg rParms = { 0 }; // read Copy
KunLun_AEB_Para_Cfg wParms = { 0 }; // write Copy
UART_OPT g_st_uart_opt; // AT cmd buff
volatile uint8_t Brake_Test_Pressure_Bar = 0;
Device_Version_Struct device_version_str;

static char ack_results[DATA_LEN_128B] 	= { 0 };
static char const *input_text 		= NULL;
static size_t input_index 			= 0;
uint8_t algConfigway				= 0;		// alg 参数配置方式：0：USART1串口配置；1：通过USART4蓝牙配置
static struct cat_command cmds[];

uint8_t valid_range[DATA_LEN_32B] 	= {0};		// valid range warning string
uint8_t at_str1[DATA_LEN_128B]   	= {0};
static uint8_t g_stf_para_arr[10] 	= {0};		// 接收string并将其转换成float类型进行处理，原因AT SDK库中没有接收float类型的接口
static float g_flt_para_1, g_flt_para_2, g_flt_para_3;	// g_flt_para_1 = atof(g_stf_para_arr);
static int g_int_para_1, g_int_para_2, g_int_para_3, g_int_para_4;

//gcz 2022-05-07
DETEC_PARA g_st_detection_cal_sp_para;
DETEC_PARA g_st_detection_ult_radra_para;
uint8_t g_DetectionCfgWay 					= 0; 		// alg 参数配置方式：0：USART1串口配置；1：通过USART4蓝牙配置
uint8_t g_u8_instlltion_detection_cmd_src 	= CMD_INIT; //安装检测命令的来源的状态控制
uint8_t g_u8_check_ult_radra_cmd_src 		= CMD_INIT; //超声波雷达检测命令的来源的状态控制
int g_iShow_RT_ULT_RADAR[2] 				= { FALSE, FALSE };
extern URADER_MESSAGE detection_Urader_Company_Message[2];
/**************************** 全局函数 ****************************************************/
void Init_AEB_Alg_Para();

/*
 * 设置AT报错信息
 * 参数：错误信息输出
 * 返回：-1，就会调用error_ack函数（对应回调函数为get_error_ack()）打印错误信息
 */
static inline int Set_AT_Error_Info(uint8_t *err_info)		// 20220905 lmz add
{
	memset(valid_range, 0, sizeof(valid_range));
	memcpy(valid_range, err_info, strlen(err_info));
	return -1;
}
/*
 * 写AEB算法参数
 */
bool Write_AEB_Config_Param(KunLun_AEB_Para_Cfg aebAlgPara) {
  uint8_t data[DATA_LEN_512B] = { 0 };		// 20220906 lmz update 长度由800改成了512，向下兼容
  memcpy(data, &aebAlgPara, sizeof(aebAlgPara));

  if (Set_AEB_Alg_Parameter(data, sizeof(KunLun_AEB_Para_Cfg), server_info.vehicle_type)) { // 写数据
//		USART1_Bebug_Print("Param OK", "Set Configure Parameter Success.");
    return true;
  }

  USART1_Bebug_Print("Param FAILED", "Set Configure Parameter Failed.", 1);
  return false;
}

bool Read_AEB_Config_Param(KunLun_AEB_Para_Cfg *algPara) {
  uint8_t data[DATA_LEN_512B] = { 0 };		// 20220906 lmz update 长度由800改成了512，向下兼容
  memset(data, 0, DATA_LEN_512B);

  if (Get_AEB_Alg_Parameter(data, sizeof(KunLun_AEB_Para_Cfg), server_info.vehicle_type)) { // 获取数据
//		USART1_Bebug_Print("Param OK", "Get Configure Parameter Success.");
    memcpy(algPara, data, sizeof(KunLun_AEB_Para_Cfg)); // copy
    return true;
  }

  USART1_Bebug_Print("Param FAILED", "Get Configure Parameter Failed.", 1);
  return false;
}

/*
 * 打印All parameters
 */
void Print_AEB_ALG_Para(STREAM* USART_Print) {
  KunLun_AEB_Para_Cfg algP = { 0 };

  if (Read_AEB_Config_Param(&algP)) {
    uint8_t AEB_CMS_Enable_keep = rParms.switch_g.AEB_CMS_Enable;
    uint8_t LDW_Enable_keep = rParms.switch_g.LDW_Enable;
    rParms = algP;
    rParms.switch_g.AEB_CMS_Enable = AEB_CMS_Enable_keep;
    rParms.switch_g.LDW_Enable = LDW_Enable_keep;

    wParms = algP;
  } else {
    USART1_Bebug_Print("READ FAILED", "Load Default Parameters.", 1);
    Load_And_Write_AEB_Default_Para();
    algP = wParms;
  }

  fprintf(USART_Print, "-------------------------------------\r\n");
  fprintf(USART_Print, "Vehicle_Type:                         %d\r\n", server_info.vehicle_type); // add lmz 20220224
  fprintf(USART_Print, "AEB_CMS_Enable:                       %d\r\n",
      algP.switch_g.AEB_CMS_Enable);
  fprintf(USART_Print, "CMS_HMW_Enable:                       %d\r\n",
      algP.switch_g.CMS_HMW_Enable);
  fprintf(USART_Print, "CMS_TTC_Enable:                       %d\r\n",
      algP.switch_g.CMS_TTC_Enable);
  fprintf(USART_Print, "AEB_TTC_Enable:                       %d\r\n",
      algP.switch_g.AEB_TTC_Enable);
  fprintf(USART_Print, "SSS_Enable:                           %d\r\n", algP.switch_g.SSS_Enable);
  fprintf(USART_Print, "HMW_Dynamic_Thr_Enable:               %d\r\n",
      algP.switch_g.HMW_Dynamic_Thr_Enable);
  fprintf(USART_Print, "Chassis_Safe_Strategy_Enable:         %d\r\n",
      algP.switch_g.Chassis_Safe_Strategy_Enable);
  fprintf(USART_Print, "LDW_Enable:                           %d\r\n", algP.switch_g.LDW_Enable);
  fprintf(USART_Print, "Displayer_Switch_Enable:              %d\r\n",
      algP.switch_g.Displayer_Switch_Enable);
  fprintf(USART_Print, "Retract_Brake_Enable:                 %d\r\n",
      algP.switch_g.Retract_Brake_Enable);

  fprintf(USART_Print, "Brake_Cooling_Time:                   %4.2f\r\n", algP.Brake_Cooling_Time);
  fprintf(USART_Print, "Driver_Brake_Cooling_Time:            %4.2f\r\n",
      algP.Driver_Brake_Cooling_Time);
  fprintf(USART_Print, "Max_Brake_Keep_Time:                  %4.2f\r\n", algP.Max_Brake_Keep_Time);
  fprintf(USART_Print, "Air_Brake_Delay_Time:                 %4.2f\r\n",
      algP.Air_Brake_Delay_Time);
  fprintf(USART_Print, "Max_Percent_Decelerate:               %4.2f\r\n",
      algP.Max_Percent_Decelerate);
  fprintf(USART_Print, "Min_Enable_Speed:                     %4.2f\r\n", algP.Min_Enable_Speed);
  fprintf(USART_Print, "Max_Output_dec:                       %4.2f\r\n", algP.Max_Output_dec);
  fprintf(USART_Print, "Ratio_Force_To_Deceleration:          %4.2f\r\n",
      algP.Ratio_Force_To_Deceleration);
  fprintf(USART_Print, "CMS_HMW_Brake_Time_Thr:               %4.2f\r\n",
      algP.CMS_HMW_Brake_Time_Thr);
  fprintf(USART_Print, "CMS_HMW_Brake_Force_Feel_Para:        %4.2f\r\n",
      algP.CMS_HMW_Brake_Force_Feel_Para);
  fprintf(USART_Print, "CMS_HMW_Warning_Time_Thr:             %4.2f\r\n",
      algP.CMS_HMW_Warning_Time_Thr);
  fprintf(USART_Print, "CMS_HMW_Dynamic_Offset_Speed_L:       %4.2f\r\n",
      algP.CMS_HMW_Dynamic_Offset_Speed_L);
  fprintf(USART_Print, "CMS_HMW_Dynamic_Offset_Value_L:       %4.2f\r\n",
      algP.CMS_HMW_Dynamic_Offset_Value_L);
  fprintf(USART_Print, "CMS_HMW_Dynamic_Offset_Speed_H:       %4.2f\r\n",
      algP.CMS_HMW_Dynamic_Offset_Speed_H);
  fprintf(USART_Print, "CMS_HMW_Dynamic_Offset_Value_H:       %4.2f\r\n",
      algP.CMS_HMW_Dynamic_Offset_Value_H);
  fprintf(USART_Print, "CMS_HMW_Time_Offset_Night:            %4.2f\r\n",
      algP.CMS_HMW_Time_Offset_Night);
  fprintf(USART_Print, "CMS_HMW_Time_Offset_Night_StartT:     %4.2f\r\n",
      algP.CMS_HMW_Time_Offset_Night_StartT);
  fprintf(USART_Print, "CMS_HMW_Time_Offset_Night_EndT:       %4.2f\r\n",
      algP.CMS_HMW_Time_Offset_Night_EndT);
  fprintf(USART_Print, "CMS_TTC_Brake_Time_Thr:               %4.2f\r\n",
      algP.CMS_TTC_Brake_Time_Thr);
  fprintf(USART_Print, "CMS_TTC_Brake_Force_Feel_Para:        %4.2f\r\n",
      algP.CMS_TTC_Brake_Force_Feel_Para);
  fprintf(USART_Print, "CMS_TTC_Warning_Time_Level_First:     %4.2f\r\n",
      algP.CMS_TTC_Warning_Time_Level_First);
  fprintf(USART_Print, "CMS_TTC_Warning_Time_Level_Second:    %4.2f\r\n",
      algP.CMS_TTC_Warning_Time_Level_Second);
  fprintf(USART_Print, "AEB_Decelerate_Set:                   %4.2f\r\n", algP.AEB_Decelerate_Set);
  fprintf(USART_Print, "AEB_Stop_Distance:                    %4.2f\r\n", algP.AEB_Stop_Distance);
  fprintf(USART_Print, "AEB_TTC_Warning_Time_Level_First:     %4.2f\r\n",
      algP.AEB_TTC_Warning_Time_Level_First);
  fprintf(USART_Print, "AEB_TTC_Warning_Time_Level_Second:    %4.2f\r\n",
      algP.AEB_TTC_Warning_Time_Level_Second);
  fprintf(USART_Print, "SSS_Brake_Force:                      %4.2f\r\n", algP.SSS_Brake_Force);
  fprintf(USART_Print, "SSS_Break_Enable_Distance:            %4.2f\r\n",
      algP.SSS_Break_Enable_Distance);
  fprintf(USART_Print, "SSS_Warning_Enable_Distance:          %4.2f\r\n",
      algP.SSS_Warning_Enable_Distance);
  fprintf(USART_Print, "SSS_Max_Enable_Speed:                 %4.2f\r\n",
      algP.SSS_Max_Enable_Speed);
  fprintf(USART_Print, "SSS_FR_FL_Install_Distance_To_Side:   %4.2f\r\n",
      algP.SSS_FR_FL_Install_Distance_To_Side);
  fprintf(USART_Print, "SSS_Stop_Distance:                    %4.2f\r\n", algP.SSS_Stop_Distance);
  fprintf(USART_Print, "SSS_Default_Turn_Angle:               %5.2f\r\n",
      algP.SSS_Default_Turn_Angle);
  fprintf(USART_Print, "Vehicle_Speed_Obtain_Method:          %d\r\n",
      algP.Vehicle_Speed_Obtain_Method);
  fprintf(USART_Print, "WheelSpeed_Coefficient:               %5.2f\r\n",
      algP.WheelSpeed_Coefficient);
  fprintf(USART_Print, "Vehicle_State_Obtain_Method:          %d\r\n",
      algP.Vehicle_State_Obtain_Method);

  fprintf(USART_Print, "PWM_magnetic_valve_T:                 %d\r\n", algP.PWM_magnetic_valve_T);
  fprintf(USART_Print, "PWM_magnetic_valve_N:                 %d\r\n", algP.PWM_magnetic_valve_N);
  fprintf(USART_Print, "PWM_magnetic_valve_Switch:            %d\r\n",
      algP.PWM_magnetic_valve_Switch);
  fprintf(USART_Print, "PWM_magnetic_valve_Scale:             %d\r\n",
      algP.PWM_magnetic_valve_Scale);

  fprintf(USART_Print, "Vechile_Width:                        %0.2f\r\n", algP.Vechile_Width);
//  fprintf(USART_Print, "CAN_0_Baud_Rate:                      %d\r\n", algP.CAN_0_Baud_Rate);
//  fprintf(USART_Print, "CAN_1_Baud_Rate:                      %d\r\n", algP.CAN_1_Baud_Rate);
//  fprintf(USART_Print, "CAN_2_Baud_Rate:                      %d\r\n", algP.CAN_2_Baud_Rate);
//  fprintf(USART_Print, "CAN_3_Baud_Rate:                      %d\r\n", algP.CAN_3_Baud_Rate);
//  fprintf(USART_Print, "CAN_4_Baud_Rate:                      %d\r\n", algP.CAN_4_Baud_Rate);
//  fprintf(USART_Print, "CAN_5_Baud_Rate:                      %d\r\n", algP.CAN_5_Baud_Rate);
  fprintf(USART_Print, "-------------------------------------\r\n");
}
/*
 * 打印 Blue Tooth Need  parameters
 */
void Print_AEB_ALG_Para_For_BT_Show(STREAM* USART_Print) {
  KunLun_AEB_Para_Cfg algP = { 0 };

  bool ret = Read_AEB_Config_Param(&algP);
  if (ret) {
    uint8_t AEB_CMS_Enable_keep 	= rParms.switch_g.AEB_CMS_Enable;
    uint8_t LDW_Enable_keep 		= rParms.switch_g.LDW_Enable;
    rParms 							= algP;
    rParms.switch_g.AEB_CMS_Enable 	= AEB_CMS_Enable_keep;
    rParms.switch_g.LDW_Enable 		= LDW_Enable_keep;

    wParms 							= algP;
  } else { // 采用默认参数
    USART1_Bebug_Print("READ FAILED", "Load Default Parameters.", 1);
    Load_And_Write_AEB_Default_Para();
    algP = wParms;
  }
  Sensor_MGT m_sensor_mgt ;
  Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt);
//	KunLun_AEB_Para_Cfg algP = Read_AEB_ALG_Para();
  fprintf(USART_Print, "-------------------------------------\r\n");

  fprintf(USART_Print, "Vehicle_Type:                      %d\r\n", server_info.vehicle_type); // add lmz 20220224

  fprintf(USART_Print, "AEB_CMS_Enable:                    %d\r\n", algP.switch_g.AEB_CMS_Enable);
  fprintf(USART_Print, "SSS_Enable:                        %d\r\n", algP.switch_g.SSS_Enable);
  fprintf(USART_Print, "HMW_Dynamic_Thr_Enable:            %d\r\n",
      algP.switch_g.HMW_Dynamic_Thr_Enable);
  fprintf(USART_Print, "Chassis_Safe_Strategy_Enable:      %d\r\n",
      algP.switch_g.Chassis_Safe_Strategy_Enable);
  fprintf(USART_Print, "LDW_Enable:                        %d\r\n", algP.switch_g.LDW_Enable);
  fprintf(USART_Print, "Displayer_Switch_Enable:          %d\r\n",
      algP.switch_g.Displayer_Switch_Enable);
  fprintf(USART_Print, "Retract_Brake_Enable:             %d\r\n",
      algP.switch_g.Retract_Brake_Enable);

  fprintf(USART_Print, "Driver_Brake_Cooling_Time:         %4.2f\r\n",
      algP.Driver_Brake_Cooling_Time);
  fprintf(USART_Print, "Brake_Cooling_Time:                %4.2f\r\n", algP.Brake_Cooling_Time);

  fprintf(USART_Print, "Min_Enable_Speed:                  %4.2f\r\n", algP.Min_Enable_Speed);
  fprintf(USART_Print, "CMS_HMW_Time_Offset_Night:         %4.2f\r\n",
      algP.CMS_HMW_Time_Offset_Night);
  fprintf(USART_Print, "CMS_HMW_Time_Offset_Night_StartT:  %4.2f\r\n",
      algP.CMS_HMW_Time_Offset_Night_StartT);
  fprintf(USART_Print, "CMS_HMW_Time_Offset_Night_EndT:    %4.2f\r\n",
      algP.CMS_HMW_Time_Offset_Night_EndT);

  fprintf(USART_Print, "Ratio_Force_To_Deceleration:       %4.2f\r\n",
      algP.Ratio_Force_To_Deceleration);
  fprintf(USART_Print, "CMS_HMW_Brake_Time_Thr:            %4.2f\r\n", algP.CMS_HMW_Brake_Time_Thr);
  fprintf(USART_Print, "CMS_HMW_Brake_Force_Feel_Para:     %4.2f\r\n",
      algP.CMS_HMW_Brake_Force_Feel_Para);
  fprintf(USART_Print, "CMS_HMW_Warning_Time_Thr:          %4.2f\r\n",
      algP.CMS_HMW_Warning_Time_Thr);

  fprintf(USART_Print, "CMS_TTC_Brake_Time_Thr:            %4.2f\r\n", algP.CMS_TTC_Brake_Time_Thr);
  fprintf(USART_Print, "CMS_TTC_Brake_Force_Feel_Para:     %4.2f\r\n",
      algP.CMS_TTC_Brake_Force_Feel_Para);
  fprintf(USART_Print, "CMS_TTC_Warning_Time_Level_First:  %4.2f\r\n",
      algP.CMS_TTC_Warning_Time_Level_First);
  fprintf(USART_Print, "CMS_TTC_Warning_Time_Level_Second: %4.2f\r\n",
      algP.CMS_TTC_Warning_Time_Level_Second);

  fprintf(USART_Print, "SSS_Brake_Force:                   %4.2f\r\n", algP.SSS_Brake_Force);
  fprintf(USART_Print, "SSS_Max_Enable_Speed:              %4.2f\r\n", algP.SSS_Max_Enable_Speed);

  //fprintf(USART_Print,"WheelSpeed_Coefficient:%4.2f\r\n",algP.WheelSpeed_Coefficient);
  fprintf(USART_Print, "Vehicle_Speed_Obtain_Method:       %d\r\n",
      algP.Vehicle_Speed_Obtain_Method);
  fprintf(USART_Print, "WheelSpeed_Coefficient:            %5.2f\r\n", algP.WheelSpeed_Coefficient);
  fprintf(USART_Print, "Vehicle_State_Obtain_Method:       %d\r\n",
      algP.Vehicle_State_Obtain_Method);
  fprintf(USART_Print, "Vehicle_Srr_Demo_Sw:       %d\r\n",
		  m_sensor_mgt.veh_srr_demo_sw);
  fprintf(USART_Print, "-------------------------------------\r\n");
}

/*
 * 打印 Blue Tooth Need  VersionInfo
 */
void Print_AEBS_Versoin_Info_For_BT_Show(STREAM* USART_Print) {
  KunLun_AEB_Para_Cfg algP = { 0 };
  if (Read_AEB_Config_Param(&algP)) {
    rParms = algP;
    wParms = rParms;
  } else { // 采用默认参数
    USART1_Bebug_Print("READ FAILED", "Load Default Parameters.", 1);
    Load_And_Write_AEB_Default_Para();
  }

  fprintf(USART_Print, "-------------------------------------\r\n");
  fprintf(USART_Print, "params_version:                    %d\r\n", algP.params_version);

  fprintf(USART_Print, "AEBS_SW_Version:                   %s\r\n", sw_hw_version.AEBS_SW_Version);
  fprintf(USART_Print,"Bootloader_Version:                %s\r\n", upgrade_info.boot_v);//sw_hw_version.Bootloader_Version
  fprintf(USART_Print, "AEBS_SN:                           %s\r\n", sw_hw_version.AEBS_SN);
  fprintf(USART_Print, "AEBS_PN:                           %s\r\n", sw_hw_version.AEBS_PN);

  uint8_t buffer[15];
  uint8_t cMon = 0;
  memset(buffer, 0, sizeof(buffer));
  if (stCanCommSta.stCamera.status == ONLINE) {
    fprintf(USART_Print, "Camera_SW_Version:                 A%d.%d.%d.%d\r\n",
        sw_hw_version.Camera_SW_Version.v0, sw_hw_version.Camera_SW_Version.v1,
        sw_hw_version.Camera_SW_Version.v2, sw_hw_version.Camera_SW_Version.v3);
    fprintf(USART_Print, "Camera_HW_Version:                 V%d.%d.%d\r\n",
        sw_hw_version.Camera_HW_Version.v0, sw_hw_version.Camera_HW_Version.v1,
        sw_hw_version.Camera_HW_Version.v2);

    if (camera_sn_info.mon > 9) {
      cMon = 'C' - (12 - camera_sn_info.mon);
    } else {
      cMon = camera_sn_info.mon + '0';
    }
    sprintf(buffer, "%d%02d%c%02d%04d", camera_sn_info.factory_code, camera_sn_info.year, cMon,
        camera_sn_info.day, camera_sn_info.stream_code);
  } else {
    fprintf(USART_Print, "Camera_SW_Version:                 A-.-.-.-\r\n");
    fprintf(USART_Print, "Camera_HW_Version:                 V-.-.-\r\n");

  }
  fprintf(USART_Print, "Camera_SN:                         %s\r\n", buffer);

  if (stCanCommSta.stDisplayer.status == ONLINE) {
    fprintf(USART_Print, "Displayer_SW_Version:              V%d.%d.%d\r\n",
        sw_hw_version.Displayer_SW_Version.v0, sw_hw_version.Displayer_SW_Version.v1,
        sw_hw_version.Displayer_SW_Version.v2);
  } else {
    fprintf(USART_Print, "Displayer_SW_Version:              V-.-.-\r\n");

  }
  fprintf(USART_Print, "Displayer_SN:                      %s\r\n", sw_hw_version.Displayer_SN);
  fprintf(USART_Print, "Displayer_PN:                      %s\r\n", sw_hw_version.Displayer_PN);

  if (stCanCommSta.stHRadar.status == ONLINE)
    fprintf(USART_Print, "URadar_Version:                    V%d.%d\r\n",
        sw_hw_version.URadar_Version.v0, sw_hw_version.URadar_Version.v1);
  else
    fprintf(USART_Print, "URadar_Version:                    V-.-\r\n");

  if (stCanCommSta.stRadar.status == ONLINE)
    fprintf(USART_Print, "MRadar_Version:                    V%d.%d.%d\r\n",
        sw_hw_version.MRadar_Version.v0, sw_hw_version.MRadar_Version.v1,
        sw_hw_version.MRadar_Version.v2);
  else
    fprintf(USART_Print, "MRadar_Version:                    V-.-.-\r\n");

  if (stCanCommSta.stCamera.status == ONLINE)
    fprintf(USART_Print, "Camera_CAN_ProtocoI_Version:       V%d.%d.%d\r\n",
        comm_protocol_version.Camera_CAN_ProtocoI_Version.v0,
        comm_protocol_version.Camera_CAN_ProtocoI_Version.v1,
        comm_protocol_version.Camera_CAN_ProtocoI_Version.v2);
  else
    fprintf(USART_Print, "Camera_CAN_ProtocoI_Version:       V-.-.-\r\n");
  fprintf(USART_Print, "AEBS_AT_PC_ProtocoI_Version:       V%d.%d.%d\r\n",
      comm_protocol_version.AEBS_AT_PC_ProtocoI_Version.v0,
      comm_protocol_version.AEBS_AT_PC_ProtocoI_Version.v1,
      comm_protocol_version.AEBS_AT_PC_ProtocoI_Version.v2);
  fprintf(USART_Print, "AEBS_AT_BT_ProtocoI_Version:       V%d.%d.%d\r\n",
      comm_protocol_version.AEBS_AT_BT_ProtocoI_Version.v0,
      comm_protocol_version.AEBS_AT_BT_ProtocoI_Version.v1,
      comm_protocol_version.AEBS_AT_BT_ProtocoI_Version.v2);
  fprintf(USART_Print, "AEBS_CAN_ProtocoI_Version:         V%d.%d\r\n",
      comm_protocol_version.AEBS_CAN_ProtocoI_Version.v0,
      comm_protocol_version.AEBS_CAN_ProtocoI_Version.v1);
  fprintf(USART_Print, "Displayer_CAN_ProtocoI_Version:    V%d.%d\r\n",
      comm_protocol_version.Displayer_CAN_ProtocoI_Version.v0,
      comm_protocol_version.Displayer_CAN_ProtocoI_Version.v1);
  fprintf(USART_Print, "Cloud_Platform_ProtocoI_Version:   V%d.%d\r\n",
      comm_protocol_version.Cloud_Platform_ProtocoI_Version.v0,
      comm_protocol_version.Cloud_Platform_ProtocoI_Version.v1);

  fprintf(USART_Print, "-------------------------------------\r\n");
}
static int Set_Params_Version(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  wParms.params_version = g_int_para_2;

  return 0;
}
/*
 * 设置车辆ID，也是存参时编号
 */
static int Set_Vehicle_Type(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 >= 1 && g_int_para_2 <= 101) {

    // 车类型切换之前，清空之前配置参数
    if (server_info.vehicle_type != g_int_para_2) {
      server_info.vehicle_type = g_int_para_2;
      // 写
      if (!Set_Vehicle_Type_ExFlh(server_info.vehicle_type)) {
        Set_Vehicle_Type_ExFlh(server_info.vehicle_type);
      }

      // 获取参数，失败就加载默认参数
      KunLun_AEB_Para_Cfg algP = { 0 };
      if (Read_AEB_Config_Param(&algP)) {
        wParms = algP;
        rParms = wParms;
      } else {
        Load_And_Write_AEB_Default_Para();
      }
    }
  } else {
    return Set_AT_Error_Info("[1~101]");
  }

  return 0;
}
/*
 * 一键测试：测试压力Bar
 */
static int Set_Test_Brake(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 >= 0 && g_int_para_2 <= 5) {
    Brake_Test_Pressure_Bar = g_int_para_2;
  } else {
    return Set_AT_Error_Info("[0~5]");
  }

  return 0;
}


/*
 * AEB_CMS全系统使能【1：开】 【0：关】
 */
static int AEB_CMS_Enable(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 == 0x00 || g_int_para_2 == 0x01) {
    wParms.switch_g.AEB_CMS_Enable = g_int_para_2 & 0x01;
  } else {
    return Set_AT_Error_Info("0 OR 1");
  }

  return 0;
}
/*
 * CMS_HMW使能【1：开】 【0：关】
 */
static int CMS_HMW_Enable(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 == 0x00 || g_int_para_2 == 0x01) {
    wParms.switch_g.CMS_HMW_Enable = g_int_para_2 & 0x01;
  } else {
    return Set_AT_Error_Info("0 OR 1");
  }

  return 0;
}
/*
 * CMS_TTC使能【1：开】 【0：关】
 */
static int CMS_TTC_Enable(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 == 0x00 || g_int_para_2 == 0x01) {
    wParms.switch_g.CMS_TTC_Enable = g_int_para_2 & 0x01;
  } else {
	  return Set_AT_Error_Info("0 OR 1");
  }
  return 0;
}
/*
 * AEB_TTC使能【1：开】 【0：关】
 */
static int AEB_TTC_Enable(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 == 0x00 || g_int_para_2 == 0x01) {
    wParms.switch_g.AEB_TTC_Enable = g_int_para_2 & 0x01;
  } else {
	  return Set_AT_Error_Info("0 OR 1");
  }
  return 0;
}
/*
 * SSS使能【1：开】 【0：关】
 */
static int SSS_Enable(const struct cat_command *cmd, const uint8_t *data, const size_t data_size,
    const size_t args_num) {

  if (g_int_para_2 == 0x00 || g_int_para_2 == 0x01) {
    wParms.switch_g.SSS_Enable = g_int_para_2 & 0x01;
  } else {
	  return Set_AT_Error_Info("0 OR 1");
  }
  return 0;
}
/*
 * HMW动态阈值使能【1：开】 【0：关】
 */
static int HMW_Dynamic_Thr_Enable(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 == 0x00 || g_int_para_2 == 0x01) {
    wParms.switch_g.HMW_Dynamic_Thr_Enable = g_int_para_2 & 0x01;
  } else {
	  return Set_AT_Error_Info("0 OR 1");
  }
  return 0;
}
/*
 * 	底盘安全策略使能【1：开】 【0：关】
 */
static int Chassis_Safe_Strategy_Enable(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 == 0x00 || g_int_para_2 == 0x01) {
    wParms.switch_g.Chassis_Safe_Strategy_Enable = g_int_para_2 & 0x01;
  } else {
	  return Set_AT_Error_Info("0 OR 1");
  }
  return 0;
}
/*
 * 车道偏离预警使能【1：开】 【0：关】
 */
static int LDW_Enable(const struct cat_command *cmd, const uint8_t *data, const size_t data_size,
    const size_t args_num) {
  if (g_int_para_2 == 0x00 || g_int_para_2 == 0x01) {
    wParms.switch_g.LDW_Enable = g_int_para_2 & 0x01;
  } else {
	  return Set_AT_Error_Info("0 OR 1");
  }
  return 0;
}

/*
 * 小屏幕开关（AEB+LDW）使能【0：关】【1：开AEB+LDW】 【2：开AEB,关LDW】【3：开LDW,关AEB】
 */
static int Displayer_Switch_Enable(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 >= 0x00 && g_int_para_2 <= 0x03) {
    wParms.switch_g.Displayer_Switch_Enable = g_int_para_2 & 0x03;
  } else {
	  return Set_AT_Error_Info("[0, 3]");
  }
  return 0;
}
/*
 * HMW slow retract brake 【1：开】 【0：关】
 */
static int Retract_Brake_Enable(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if (g_int_para_2 == 0x00 || g_int_para_2 == 0x01) {
    wParms.switch_g.Retract_Brake_Enable = g_int_para_2 & 0x01;
  } else {
	  return Set_AT_Error_Info("0 OR 1");
  }
  return 0;
}
/*
 * 车身宽度参数
 */
static int Vechile_Width(const struct cat_command *cmd, const uint8_t *data, const size_t data_size,
    const size_t args_num) {
	g_flt_para_1 =  atof(g_stf_para_arr);
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 10.0) {
    wParms.Vechile_Width = g_flt_para_1;
//    USART1_Bebug_Print_Flt("DEBUG", wParms.Vechile_Width, 1, 1);
  } else {
    return Set_AT_Error_Info("[0.0~10.0]");
  }
  return 0;
}
/*
 * CAN_0_Baud_Rate:CAN口波特率
 */
static int CAN_0_Baud_Rate(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	// CAN波特率为125、250、500、800、1000KB
	switch(g_int_para_2){
	case 125:
	case 250:
	case 500:
	case 800:
	case 1000:{
		wParms.CAN_0_Baud_Rate = g_int_para_2;
	}break;
	default:{
		sprintf(valid_range, "Can0 baud %dKB is illegal.", g_int_para_2);
		return -1;
	}
	}
	return 0;
}
/*
 * CAN_1_Baud_Rate:CAN口波特率
 */
static int CAN_1_Baud_Rate(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	// CAN波特率为125、250、500、800、1000KB
	switch(g_int_para_2){
	case 125:
	case 250:
	case 500:
	case 800:
	case 1000:{
		wParms.CAN_1_Baud_Rate = g_int_para_2;
	}break;
	default:{
		sprintf(valid_range, "Can1 baud %dKB is illegal.", g_int_para_2);
		return -1;
	}
	}
	return 0;
}
/*
 * CAN_2_Baud_Rate:CAN口波特率
 */
static int CAN_2_Baud_Rate(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	// CAN波特率为125、250、500、800、1000KB
	switch(g_int_para_2){
	case 125:
	case 250:
	case 500:
	case 800:
	case 1000:{
		wParms.CAN_2_Baud_Rate = g_int_para_2;
	}break;
	default:{
		sprintf(valid_range, "Can2 baud %dKB is illegal.", g_int_para_2);
		return -1;
	}
	}
	return 0;
}
/*
 * CAN_3_Baud_Rate:CAN口波特率
 */
static int CAN_3_Baud_Rate(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	// CAN波特率为125、250、500、800、1000KB
	switch(g_int_para_2){
	case 125:
	case 250:
	case 500:
	case 800:
	case 1000:{
		wParms.CAN_3_Baud_Rate = g_int_para_2;
	}break;
	default:{
		sprintf(valid_range, "Can3 baud %dKB is illegal.", g_int_para_2);
		return -1;
	}
	}
	return 0;
}
/*
 * CAN_4_Baud_Rate:CAN口波特率
 */
static int CAN_4_Baud_Rate(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	// CAN波特率为125、250、500、800、1000KB
	switch(g_int_para_2){
	case 125:
	case 250:
	case 500:
	case 800:
	case 1000:{
		wParms.CAN_4_Baud_Rate = g_int_para_2;
	}break;
	default:{
		sprintf(valid_range, "Can4 baud %dKB is illegal.", g_int_para_2);
		return -1;
	}
	}
	return 0;
}
/*
 * CAN_5_Baud_Rate:CAN口波特率
 */
static int CAN_5_Baud_Rate(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	// CAN波特率为125、250、500、800、1000KB
	switch(g_int_para_2){
	case 125:
	case 250:
	case 500:
	case 800:
	case 1000:{
		wParms.CAN_5_Baud_Rate = g_int_para_2;
	}break;
	default:{
		sprintf(valid_range, "Can5 baud %dKB is illegal.", g_int_para_2);
		return -1;
	}
	}
	return 0;
}
/*
 * 申工电磁阀配置参数
 */
static int PWM_Magnetic_Valve_Params(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  //T
  if (g_int_para_1 >= 0 && g_int_para_1 <= 50) {
    wParms.PWM_magnetic_valve_T = g_int_para_1;
  } else {
    return Set_AT_Error_Info("[0~50]");
  }
  //N
  if (g_int_para_2 >= 0 && g_int_para_2 <= 5) {
    if (g_int_para_2 != 0 && g_int_para_1 < 1000 / g_int_para_2) {
      wParms.PWM_magnetic_valve_N = g_int_para_2;
    } else {
      return Set_AT_Error_Info("[0~5]");
    }
  } else {
	  return Set_AT_Error_Info("[0~5]");
  }
  //Switch
  if (g_int_para_3 == 0 || g_int_para_3 == 1) {
    wParms.PWM_magnetic_valve_Switch = g_int_para_3;
  } else {
    return Set_AT_Error_Info("[0~1]");
  }

  //scale
  if (g_int_para_4 >= 0 && g_int_para_4 <= 100) {
    wParms.PWM_magnetic_valve_Scale = g_int_para_4;
  } else {
    return Set_AT_Error_Info("[0~100]");
  }
  ///wParms.PWM_magnetic_valve_N		 	= g_int_para_2;	// 次数
  ///wParms.PWM_magnetic_valve_Switch 	= g_int_para_3;	// 开关
  ///wParms.PWM_magnetic_valve_Scale		= g_int_para_4;	// 比例系数

    megneticVal.enableTime = g_int_para_1;
    megneticVal.ctrlCnt = g_int_para_2;
    megneticVal.swt = g_int_para_3;
    megneticVal.disableTime = 1000 / megneticVal.ctrlCnt - megneticVal.enableTime;

  return 0;
}
//int PWM_Magnetic_Valve_set(uint16_t p1_time, uint16_t p2_num, uint8_t swt) {
//  megneticVal.enableTime = p1_time;
//  megneticVal.ctrlCnt = p2_num;
//  megneticVal.swt = swt;
//
//  megneticVal.disableTime = 1000 / megneticVal.ctrlCnt - megneticVal.enableTime;
//
//  return 0;
//}
/*
 * AEB/CMS 配置参数
 * 刹车冷却时间
 */
static int Brake_Cooling_Time(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
//	USART1_Bebug_Print("DEBUG", g_stf_para_arr, 1);
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 60.0) {
    wParms.Brake_Cooling_Time = atof(g_stf_para_arr);
  } else {
    return Set_AT_Error_Info("[0.0~60.0]");
  }
  return 0;
}
/*
 * AEB/CMS 配置参数
 * 司机介入后冷却时间
 */
static int Driver_Brake_Cooling_Time(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 60.0) {
    wParms.Driver_Brake_Cooling_Time = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~60.0]");
  }
  return 0;
}
/*
 * AEB/CMS 配置参数
 * 最长刹车时长
 */
static int Max_Brake_Keep_Time(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 30.0) {
    wParms.Max_Brake_Keep_Time = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~30.0]");
  }
  return 0;
}
/*
 * AEB/CMS 配置参数
 * 气刹延迟时间
 */
static int Air_Brake_Delay_Time(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 1.0) {
    wParms.Air_Brake_Delay_Time = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~1.0]");
  }
  return 0;
}
/*
 * AEB/CMS 配置参数
 * 最大减速量
 */
static int Max_Percent_Decelerate(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 1.0) {
    wParms.Max_Percent_Decelerate = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~1.0]");
  }
  return 0;
}
/*
 * AEB/CMS 配置参数
 * AEB_CMS最低启用车速
 */
static int Min_Enable_Speed(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 120.0) {
    wParms.Min_Enable_Speed = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~120.0]");
  }
  return 0;
}
/*
 * AEB/CMS 配置参数
 * AEB_CMS最大减速度
 */
static int Max_Output_dec(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 12.0) {
    wParms.Max_Output_dec = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[4.0~120.0]");
  }
  return 0;
}
/*
 * AEB/CMS 配置参数
 * 刹车力度系数
 */
static int Ratio_Force_To_Deceleration(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 100.0) {
    wParms.Ratio_Force_To_Deceleration = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~100.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW刹车时间阈值
 */
static int CMS_HMW_Brake_Time_Thr(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 2.0) {
    wParms.CMS_HMW_Brake_Time_Thr = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~2.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW刹车力度参数（体感）
 */
static int CMS_HMW_Brake_Force_Feel_Para(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 10.0) {
    wParms.CMS_HMW_Brake_Force_Feel_Para = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~10.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW预警时间阈值
 */
static int CMS_HMW_Warning_Time_Thr(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 3.0) {
    wParms.CMS_HMW_Warning_Time_Thr = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~3.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW刹车时间动态阈值参数1(低速车速)
 */
static int CMS_HMW_Dynamic_Offset_Speed_L(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 10.0 && g_flt_para_1 <= 60.0) {
    wParms.CMS_HMW_Dynamic_Offset_Speed_L = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[10.0~60.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW刹车时间动态阈值参数2（时间开度）
 */
static int CMS_HMW_Dynamic_Offset_Value_L(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 2.0) {
    wParms.CMS_HMW_Dynamic_Offset_Value_L = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~2.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW刹车时间动态阈值参数3（高速车速）
 */
static int CMS_HMW_Dynamic_Offset_Speed_H(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 50.0 && g_flt_para_1 <= 120.0) {
    wParms.CMS_HMW_Dynamic_Offset_Speed_H = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[50.0~120.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW刹车时间动态阈值参数4（时间开度）
 */
static int CMS_HMW_Dynamic_Offset_Value_H(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 2.0) {
    wParms.CMS_HMW_Dynamic_Offset_Value_H = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~2.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW刹车时间夜间调整
 */
static int CMS_HMW_Time_Offset_Night(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 2.0) {
    wParms.CMS_HMW_Time_Offset_Night = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~2.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW刹车时间夜间动态阈值（夜间时间范围：start time）
 */
static int CMS_HMW_Time_Offset_Night_StartT(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 < 24.0) {
    wParms.CMS_HMW_Time_Offset_Night_StartT = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~24.0]");
  }
  return 0;
}
/*
 * CMS/HMW
 * HMW刹车时间夜间动态阈值（夜间时间范围：end time）
 */
static int CMS_HMW_Time_Offset_Night_EndT(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 < 24.0) {
    wParms.CMS_HMW_Time_Offset_Night_EndT = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~24.0]");
  }
  return 0;
}
/*
 * CMS/TTC
 * CMS_TTC缓刹刹车时间阈值
 */
static int CMS_TTC_Brake_Time_Thr(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 5.0) {
    wParms.CMS_TTC_Brake_Time_Thr = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~5.0]");
  }
  return 0;
}
/*
 * CMS/TTC
 * CMS_TTC缓刹固定刹车力度值（体感）
 */
static int CMS_TTC_Brake_Force_Feel_Para(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 10.0) {
    wParms.CMS_TTC_Brake_Force_Feel_Para = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~10.0]");
  }
  return 0;
}
/*
 * CMS/TTC
 * CMS_TTC缓刹一级预警时间
 */
static int CMS_TTC_Warning_Time_Level_First(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 6.0) {
    wParms.CMS_TTC_Warning_Time_Level_First = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~6.0]");
  }
  return 0;
}
/*
 * CMS/TTC
 * CMS_TTC缓刹二级预警时间
 */
static int CMS_TTC_Warning_Time_Level_Second(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 6.0) {
    wParms.CMS_TTC_Warning_Time_Level_Second = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~6.0]");
  }
  return 0;
}
/*
 * AEB
 * 预设刹车减速度
 */
static int AEB_Decelerate_Set(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 10.0) {
    wParms.AEB_Decelerate_Set = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~10.0]");
  }
  return 0;
}
/*
 * AEB
 * 刹停距离
 */
static int AEB_Stop_Distance(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 10.0) {
    wParms.AEB_Stop_Distance = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~10.0]");
  }
  return 0;
}
/*
 * AEB
 * 紧急制动TTC一级预警时间
 */
static int AEB_TTC_Warning_Time_Level_First(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 5.0) {
    wParms.AEB_TTC_Warning_Time_Level_First = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~5.0]");
  }
  return 0;
}
/*
 * AEB
 * 紧急制动TTC二级预警时间
 */
static int AEB_TTC_Warning_Time_Level_Second(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 5.0) {
    wParms.AEB_TTC_Warning_Time_Level_Second = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~5.0]");
  }
  return 0;
}
/*
 * SSS
 * 刹车力度
 */
static int SSS_Brake_Force(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 10.0) {
    wParms.SSS_Brake_Force = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~10.0]");
  }
  return 0;
}
/*
 * SSS
 * 刹车使能距离阈值
 */
static int SSS_Break_Enable_Distance(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 2.5) {
    wParms.SSS_Break_Enable_Distance = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~2.50]");
  }
  return 0;
}
/*
 * SSS
 * 预警使能距离阈值
 */
static int SSS_Warning_Enable_Distance(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 5.0) {
    wParms.SSS_Warning_Enable_Distance = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~5.0]");
  }
  return 0;
}
/*
 * SSS
 * 最大有效车速
 */
static int SSS_Max_Enable_Speed(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 40.0) {
    wParms.SSS_Max_Enable_Speed = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~40.0]");
  }
  return 0;
}
/*
 * SSS
 * 前向雷达距离侧面安装距离
 */
static int SSS_FR_FL_Install_Distance_To_Side(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr); 	// add lmz 20220905
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 1.0) {
    wParms.SSS_FR_FL_Install_Distance_To_Side = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~1.0]");
  }
  return 0;
}
/*
 * SSS
 * 刹停后距离障碍物距离
 */
static int SSS_Stop_Distance(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr);
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 1.0) {
    wParms.SSS_Stop_Distance = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~1.0]");
  }

  return 0;
}
/*
 * SSS
 * 默认转向角度
 */
static int SSS_Default_Turn_Angle(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr);
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 60.0) {
    wParms.SSS_Default_Turn_Angle = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.0~60.0]");
  }
  return 0;
}
/*
 * Vehicle Speed
 * 轮速调整系数
 */
static int WheelSpeed_Coefficient(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
	g_flt_para_1 = atof(g_stf_para_arr);
  if (g_flt_para_1 >= 0.0 && g_flt_para_1 <= 10000.0) {
    wParms.WheelSpeed_Coefficient = g_flt_para_1;
  } else {
    return Set_AT_Error_Info("[0.00~10000.00]");
  }
  return 0;
}
/*
 * 车速获取方式配置【1：轮速】 【0：CAN】
 */
static int Vehicle_Speed_Obtain_Method(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
//	wParms.Vehicle_Speed_Obtain_Method = g_int_para_2 & 0x0001;
  if (g_int_para_2 == 0 || g_int_para_2 == 1) {
    wParms.Vehicle_Speed_Obtain_Method = g_int_para_2;
  } else {
    return Set_AT_Error_Info("0:CAN; 1:GPIO");
  }
  return 0;
}
/*
 * 车身信息获取方式配置【1：GPIO】 【0：CAN】
 */
static int Vehicle_State_Obtain_Method(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
//	wParms.Vehicle_State_Obtain_Method = g_int_para_2 & 0x0001;
  if (g_int_para_2 == 0 || g_int_para_2 == 1) {
    wParms.Vehicle_State_Obtain_Method = g_int_para_2;
  } else {
    return Set_AT_Error_Info("0:CAN; 1:GPIO");
  }
  return 0;
}
//gcz 2022-05-07
/*
 * 开启/关闭标定车速流程
 */
static int APP_START_CALIBRATION_SPEED_FLOW(const struct cat_command *cmd, const uint8_t *data,const size_t data_size, const size_t args_num)
{
	if (g_int_para_2 == 0 || g_int_para_2 == 1)
	{
		g_st_detection_cal_sp_para.flag = g_int_para_2;
		g_st_detection_cal_sp_para.cnt = 0;

		if (g_st_detection_cal_sp_para.flag == 1) //开启标定流程
		{
			if (g_DetectionCfgWay == 0) //串口1来源
				g_u8_instlltion_detection_cmd_src = CMD_SRC_DEBUG;
			else
				g_u8_instlltion_detection_cmd_src = CMD_SRC_BT;

		}
		else
		{
			g_u8_instlltion_detection_cmd_src = CMD_END;
			g_iShow_RT_speed[0] = FALSE;
			g_iShow_RT_speed[1] = FALSE;
		}
	}
	else
	{
		return Set_AT_Error_Info("0:CLOSE 1:OPEN");
	}
	return 0;
}
/*
 * 标定车速流程
 */
static int APP_SET_CALIBRATION_SPEED(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if ((g_int_para_2 >= 30) && (g_int_para_2 <= 70)) {

    if (g_st_detection_cal_sp_para.flag == 1) {
      if (rParms.Vehicle_Speed_Obtain_Method == 1) {
        float f_cur_sp = get_cur_vehicle_speed();
        if (f_cur_sp > 0) {
          //获取当前车速
          wParms.WheelSpeed_Coefficient = g_int_para_2 / f_cur_sp * rParms.WheelSpeed_Coefficient;
        } else {
          return Set_AT_Error_Info("[cur speed < 30]");
        }
      } else {
        return Set_AT_Error_Info("[Obtain_Method:0]");
      }
    } else {
      return Set_AT_Error_Info("[start_flow:close]");
    }
  } else {
    return Set_AT_Error_Info("[1~70]");
  }
  return 0;
}
/*
 * 正控刹车灯输出
 */
//static int APP_CHECK_OUTPUT_BRAKE(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
//{
////	fprintf(USART1_STREAM, "111111111111\r\n");
//	if((g_int_para_2 == 0) || (g_int_para_2 == 1)){
////		fprintf(USART1_STREAM, "2222222---%d\r\n",g_int_para_2);
//
//		if (g_int_para_2 == 1)
//		{
//			GPIO_Set_Output_Data_Bits(GPIOH_SFR,GPIO_PIN_MASK_14, 1);
//			fprintf(USART1_STREAM, "1111111111\r\n");
//		}
//		else
//		{
//			GPIO_Set_Output_Data_Bits(GPIOH_SFR,GPIO_PIN_MASK_14, 0);
//			fprintf(USART1_STREAM, "22222222222\r\n");
//		}
////		fprintf(USART1_STREAM, "3333333333333---%d\r\n",g_int_para_2);
//	}else{
//
//		return Set_AT_Error_Info("[0:CLOSE 1:OPEN]");
//	}
//	return 0;
//}
/*
 * 设置所有参数
 */
static int Sensor_GetInfo_CAR_DVR(const struct cat_command *cmd)	// 20220905 lmz update
{
	USART1_Bebug_Print_Num("car_dvr_param.g_sensor_isOpen", car_dvr_param.g_sensor_isOpen, 1, 1);
	USART1_Bebug_Print_Num("car_dvr_param.g_sensor_level", car_dvr_param.g_sensor_level, 1, 1);
	USART1_Bebug_Print_Num("car_dvr_param.record_seconds", car_dvr_param.record_seconds, 1, 1);
	USART1_Bebug_Print_Num("car_dvr_param.warning_fcw", car_dvr_param.warning_fcw, 1, 1);
	USART1_Bebug_Print_Num("car_dvr_param.warning_hmw", car_dvr_param.warning_hmw, 1, 1);
	USART1_Bebug_Print_Num("car_dvr_param.warning_ldw", car_dvr_param.warning_ldw, 1, 1);
	USART1_Bebug_Print_Num("car_dvr_param.warning_aeb", car_dvr_param.warning_aeb, 1, 1);

	return 0;
}
/*
 * 开启/关闭超声波雷达检测数据
 */
static int APP_CHECK_ULT_DIS(const struct cat_command *cmd, const uint8_t *data,const size_t data_size, const size_t args_num)
{
	if (g_int_para_2 == 0 || g_int_para_2 == 1)
	{
		g_st_detection_ult_radra_para.flag = g_int_para_2;
		g_st_detection_ult_radra_para.cnt = 0;
		if (g_st_detection_ult_radra_para.flag == 1) //开启标定流程
		{
			if (g_DetectionCfgWay == 0) //串口1来源
				g_u8_check_ult_radra_cmd_src = CMD_SRC_DEBUG;
			else
				g_u8_check_ult_radra_cmd_src = CMD_SRC_BT;
		}
		else
		{
			g_u8_check_ult_radra_cmd_src = CMD_END;
			g_iShow_RT_ULT_RADAR[0] = FALSE;
			g_iShow_RT_ULT_RADAR[1] = FALSE;
		}
	}
	else
	{
		return Set_AT_Error_Info("0:CLOSE 1:OPEN");
	}
	return 0;
}
/* gcz
 * ULT_RADAR_MONITOR_ENSBLE  使能【1：开】 【0：关】
 */
static int ULT_RADAR_MONITOR_Enable(const struct cat_command *cmd, const uint8_t *data,
    const size_t data_size, const size_t args_num) {
  if ((g_int_para_1 >= 1) && (g_int_para_1 <= MAX_ULT_RADAR_NBR) && (g_int_para_2 == 0x00 || g_int_para_2 == 0x01)) {
    wParms.switch_g.ultrasonic_radar_monitor_ensble[g_int_para_1 - 1] = g_int_para_2 & 0x01;
    int i1;
    char buffer[10], outbuf[20];
    memset(outbuf, 0, sizeof(outbuf));
    for (i1 = 0; i1 < MAX_ULT_RADAR_NBR - 1; i1++) {
      memset(buffer, 0, 10);
      sprintf(buffer, "%d)%d ", i1 + 1, wParms.switch_g.ultrasonic_radar_monitor_ensble[i1]);
      strcat(outbuf, buffer);
    }
    memset(buffer, 0, 10);
    sprintf(buffer, "%d)%d\r\n", i1 + 1,
        wParms.switch_g.ultrasonic_radar_monitor_ensble[MAX_ULT_RADAR_NBR - 1]);
    strcat(outbuf, buffer);
    USART1_Bebug_Print("", outbuf, 1);		// 20220906 lmz update
  } else {
    return Set_AT_Error_Info("[ch:1~12,key:0 ro 1]");
  }
  return 0;
}

static int Sensor_Mgt_Project_Type(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
//	USART1_Bebug_Print_Num("g_int_para_1", g_int_para_1, 0, 1);
//	USART1_Bebug_Print_Num("g_int_para_2", g_int_para_2, 0, 1);
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}

	if(g_int_para_2>=0 && g_int_para_2<=255){
		m_sensor_mgt.m_project = g_int_para_2;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("project_type[0~255]");
	}
	return 0;
}
static int Sensor_Mgt_Product_Type(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}

	if(g_int_para_2>=0 && g_int_para_2<=255){
		m_sensor_mgt.m_product = g_int_para_2;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("product_type[0~255]");
	}
	return 0;
}
static int Sensor_Mgt_4G(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}

	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_4g.m_isOpen = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=15){
		m_sensor_mgt.m_4g.m_producer = g_int_para_2;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~15]");
	}
	return 0;
}
static int Sensor_Mgt_GPS(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}

	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_gps.m_isOpen = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=15){
		m_sensor_mgt.m_gps.m_producer = g_int_para_2;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~15]");
	}
	return 0;
}
static int Sensor_Mgt_DBL_Cam(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}

	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_dbl_cam.m_isOpen = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=15){
		m_sensor_mgt.m_dbl_cam.m_producer = g_int_para_2;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{

		return Set_AT_Error_Info("producer[0~15]");
	}
	return 0;
}
static int Sensor_Mgt_SGL_Cam(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}

	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_sgl_cam.m_isOpen = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=15){
		m_sensor_mgt.m_sgl_cam.m_producer = g_int_para_2;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~15]");
	}
	return 0;
}

static int Sensor_Mgt_Ultra_radar_Front(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}
	// 前超声波雷达总开关
	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_ultra_radar.m_front.m_main_sw = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=0x3F){	// 最多6个1
		m_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw = g_int_para_2;
		m_sensor_mgt.m_ultra_radar.m_front.m_sensor_num = 0;
		if((g_int_para_2 & 0x01) == 1) m_sensor_mgt.m_ultra_radar.m_front.m_sensor_num++;
		if((g_int_para_2 & 0x02)>>1 == 1) m_sensor_mgt.m_ultra_radar.m_front.m_sensor_num++;
		if((g_int_para_2 & 0x04)>>2 == 1) m_sensor_mgt.m_ultra_radar.m_front.m_sensor_num++;
		if((g_int_para_2 & 0x08)>>3 == 1) m_sensor_mgt.m_ultra_radar.m_front.m_sensor_num++;
		if((g_int_para_2 & 0x10)>>4 == 1) m_sensor_mgt.m_ultra_radar.m_front.m_sensor_num++;
		if((g_int_para_2 & 0x20)>>5 == 1) m_sensor_mgt.m_ultra_radar.m_front.m_sensor_num++;
	}else{
		return Set_AT_Error_Info("sgl_sw[0~63] 6个1");
	}

	// 厂家
	if(g_int_para_3>=0 && g_int_para_3<=7){
		m_sensor_mgt.m_ultra_radar.m_front.m_producer = g_int_para_3;
	}else{
		return Set_AT_Error_Info("producer[0~7]");
	}

	// 控制器编号
	if(g_int_para_4>=0 && g_int_para_4<=3){
		m_sensor_mgt.m_ultra_radar.m_front.m_controler = g_int_para_4;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("control_id[0~3]");
	}
	return 0;
}
static int Sensor_Mgt_Ultra_radar_End(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}
	// 后超声波雷达总开关
	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_ultra_radar.m_end.m_main_sw = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=0x3F){	// 最多6个1
		m_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw = g_int_para_2;
		m_sensor_mgt.m_ultra_radar.m_end.m_sensor_num = 0;
		if((g_int_para_2 & 0x01) == 1) m_sensor_mgt.m_ultra_radar.m_end.m_sensor_num++;
		if((g_int_para_2 & 0x02)>>1 == 1) m_sensor_mgt.m_ultra_radar.m_end.m_sensor_num++;
		if((g_int_para_2 & 0x04)>>2 == 1) m_sensor_mgt.m_ultra_radar.m_end.m_sensor_num++;
		if((g_int_para_2 & 0x08)>>3 == 1) m_sensor_mgt.m_ultra_radar.m_end.m_sensor_num++;
		if((g_int_para_2 & 0x10)>>4 == 1) m_sensor_mgt.m_ultra_radar.m_end.m_sensor_num++;
		if((g_int_para_2 & 0x20)>>5 == 1) m_sensor_mgt.m_ultra_radar.m_end.m_sensor_num++;
	}else{
		return Set_AT_Error_Info("sgl_sw[0~63] 6个1");
	}

	// 厂家
	if(g_int_para_3>=0 && g_int_para_3<=7){
		m_sensor_mgt.m_ultra_radar.m_end.m_producer = g_int_para_3;
	}else{
		return Set_AT_Error_Info("producer[0~7]");
	}
	// 控制器编号
	if(g_int_para_4>=0 && g_int_para_4<=3){
		m_sensor_mgt.m_ultra_radar.m_end.m_controler = g_int_para_4;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("control_id[0~3]");
	}
	return 0;
}
static int Sensor_Mgt_Ultra_radar_Right(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}
	// 右超声波雷达总开关
	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_ultra_radar.m_right.m_main_sw = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=0x3F){	// 最多6个1
		m_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw = g_int_para_2;
		m_sensor_mgt.m_ultra_radar.m_right.m_sensor_num = 0;
		if((g_int_para_2 & 0x01) == 1) m_sensor_mgt.m_ultra_radar.m_right.m_sensor_num++;
		if((g_int_para_2 & 0x02)>>1 == 1) m_sensor_mgt.m_ultra_radar.m_right.m_sensor_num++;
		if((g_int_para_2 & 0x04)>>2 == 1) m_sensor_mgt.m_ultra_radar.m_right.m_sensor_num++;
		if((g_int_para_2 & 0x08)>>3 == 1) m_sensor_mgt.m_ultra_radar.m_right.m_sensor_num++;
		if((g_int_para_2 & 0x10)>>4 == 1) m_sensor_mgt.m_ultra_radar.m_right.m_sensor_num++;
		if((g_int_para_2 & 0x20)>>5 == 1) m_sensor_mgt.m_ultra_radar.m_right.m_sensor_num++;
	}else{
		return Set_AT_Error_Info("sgl_sw[0~63] 6个1");
	}

	// 厂家
	if(g_int_para_3>=0 && g_int_para_3<=7){
		m_sensor_mgt.m_ultra_radar.m_right.m_producer = g_int_para_3;
	}else{
		return Set_AT_Error_Info("producer[0~7]");
	}
	// 控制器编号
	if(g_int_para_4>=0 && g_int_para_4<=3){
		m_sensor_mgt.m_ultra_radar.m_right.m_controler = g_int_para_4;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("control_id[0~3]");
	}
	return 0;
}
static int Sensor_Mgt_Ultra_radar_Left(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
//	USART1_Bebug_Print_Num("g_int_para_1", g_int_para_1, 0, 1);
//	USART1_Bebug_Print_Num("g_int_para_2", g_int_para_2, 0, 1);
//	USART1_Bebug_Print_Num("g_int_para_3", g_int_para_3, 0, 1);
//	USART1_Bebug_Print_Num("g_int_para_4", g_int_para_4, 0, 1);

	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}
	// 右超声波雷达总开关
	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_ultra_radar.m_left.m_main_sw = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=0x3F){	// 最多6个1
		m_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw = g_int_para_2;
		m_sensor_mgt.m_ultra_radar.m_left.m_sensor_num = 0;
		if((g_int_para_2 & 0x01) == 1) m_sensor_mgt.m_ultra_radar.m_left.m_sensor_num++;
		if((g_int_para_2 & 0x02)>>1 == 1) m_sensor_mgt.m_ultra_radar.m_left.m_sensor_num++;
		if((g_int_para_2 & 0x04)>>2 == 1) m_sensor_mgt.m_ultra_radar.m_left.m_sensor_num++;
		if((g_int_para_2 & 0x08)>>3 == 1) m_sensor_mgt.m_ultra_radar.m_left.m_sensor_num++;
		if((g_int_para_2 & 0x10)>>4 == 1) m_sensor_mgt.m_ultra_radar.m_left.m_sensor_num++;
		if((g_int_para_2 & 0x20)>>5 == 1) m_sensor_mgt.m_ultra_radar.m_left.m_sensor_num++;
	}else{
		return Set_AT_Error_Info("sgl_sw[0~63] 6个1");
	}

	// 厂家
	if(g_int_para_3>=0 && g_int_para_3<=7){
		m_sensor_mgt.m_ultra_radar.m_left.m_producer = g_int_para_3;
	}else{
		return Set_AT_Error_Info("producer[0~7]");
	}
	// 控制器编号
	if(g_int_para_4>=0 && g_int_para_4<=3){
		m_sensor_mgt.m_ultra_radar.m_left.m_controler = g_int_para_4;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("control_id[0~3]");
	}
	return 0;
}

static int Sensor_Mgt_MMW_radar_Front(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}
	// 前毫米波雷达总开关
	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_mmw_radar.m_front.m_main_sw = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=1){	// 最多1个1
		m_sensor_mgt.m_mmw_radar.m_front.m_rdr_sw = g_int_para_2;
		m_sensor_mgt.m_mmw_radar.m_front.m_sensor_num = 0;
		if((g_int_para_2 & 0x01) == 1) m_sensor_mgt.m_mmw_radar.m_front.m_sensor_num++;
	}else{
		return Set_AT_Error_Info("sgl_sw[0~1]");
	}

	// 厂家
	if(g_int_para_3>=0 && g_int_para_3<=7){
		m_sensor_mgt.m_mmw_radar.m_front.m_producer = g_int_para_3;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~7]");
	}

	return 0;
}
static int Sensor_Mgt_MMW_radar_End(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}
	// 后毫米波雷达总开关
	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_mmw_radar.m_end.m_main_sw = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=1){	// 最多1个1
		m_sensor_mgt.m_mmw_radar.m_end.m_rdr_sw = g_int_para_2;
		m_sensor_mgt.m_mmw_radar.m_end.m_sensor_num = 0;
		if((g_int_para_2 & 0x01) == 1) m_sensor_mgt.m_mmw_radar.m_end.m_sensor_num++;
	}else{
		return Set_AT_Error_Info("sgl_sw[0~1]");
	}

	// 厂家
	if(g_int_para_3>=0 && g_int_para_3<=7){
		m_sensor_mgt.m_mmw_radar.m_end.m_producer = g_int_para_3;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~7]");
	}
	return 0;
}
static int Sensor_Mgt_MMW_radar_Left(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}
	// 左毫米波雷达总开关
	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_mmw_radar.m_left.m_main_sw = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=3){	// 最多2个1
		m_sensor_mgt.m_mmw_radar.m_left.m_rdr_sw = g_int_para_2;
		m_sensor_mgt.m_mmw_radar.m_left.m_sensor_num = 0;
		if((g_int_para_2 & 0x01) == 1) m_sensor_mgt.m_mmw_radar.m_left.m_sensor_num++;
		if((g_int_para_2 & 0x02)>>1 == 1) m_sensor_mgt.m_mmw_radar.m_left.m_sensor_num++;
	}else{
		return Set_AT_Error_Info("sgl_sw[0~3]");
	}

	// 厂家
	if(g_int_para_3>=0 && g_int_para_3<=7){
		m_sensor_mgt.m_mmw_radar.m_left.m_producer = g_int_para_3;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~7]");
	}
	return 0;
}
static int Sensor_Mgt_MMW_radar_Right(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}
	// 右毫米波波雷达总开关
	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_mmw_radar.m_right.m_main_sw = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=3){	// 最多2个1
		m_sensor_mgt.m_mmw_radar.m_right.m_rdr_sw = g_int_para_2;
		m_sensor_mgt.m_mmw_radar.m_right.m_sensor_num = 0;
		if((g_int_para_2 & 0x01) == 1) m_sensor_mgt.m_mmw_radar.m_right.m_sensor_num++;
		if((g_int_para_2 & 0x02)>>1 == 1) m_sensor_mgt.m_mmw_radar.m_right.m_sensor_num++;
	}else{
		return Set_AT_Error_Info("sgl_sw[0~3]");
	}

	// 厂家
	if(g_int_para_3>=0 && g_int_para_3<=7){
		m_sensor_mgt.m_mmw_radar.m_right.m_producer = g_int_para_3;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~7]");
	}
	return 0;
}

static int Sensor_Mgt_IMU(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}

	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_imu.m_isOpen = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=15){
		m_sensor_mgt.m_imu.m_producer = g_int_para_2;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~15]");
	}
	return 0;
}

static int Sensor_Mgt_CAR_DVR(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}

	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_car_dvr.m_isOpen = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=15){
		m_sensor_mgt.m_car_dvr.m_producer = g_int_para_2;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~15]");
	}

	return 0;
}

static int Sensor_Mgt_Actuator(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	Sensor_MGT m_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
		return Set_AT_Error_Info("Get Sensor Mgt Failed.");
	}

	if(g_int_para_1>=0 && g_int_para_1<=1){
		m_sensor_mgt.m_actuator.m_isOpen = g_int_para_1;
	}else{
		return Set_AT_Error_Info("main_sw[0~1]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=15){
		m_sensor_mgt.m_actuator.m_producer = g_int_para_2;
		// 写配置参数
		if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
			return Set_AT_Error_Info("Set Sensor MGT Failed.");
		}
	}else{
		return Set_AT_Error_Info("producer[0~15]");
	}
	return 0;
}

static int Sensor_Config_CAR_DVR(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	CarDvr_Cfg car_dvr_tmp;

	car_dvr_tmp = Get_Car_Dvr_Param();

	if(g_int_para_1>=0 && g_int_para_1<=60)
	{
		car_dvr_tmp.record_seconds = g_int_para_1;
	}
	else
	{
		return Set_AT_Error_Info("[0~60]");
	}

	if(g_int_para_2>=0 && g_int_para_2<=3)
	{
		if(g_int_para_2 != 0)//记录仪的Gsensor 等级为1-3，如果该值为0则关闭G-sensor
		{
		    car_dvr_tmp.g_sensor_isOpen = 1;
			car_dvr_tmp.g_sensor_level = g_int_para_2;
		}
		else
		{
		    car_dvr_tmp.g_sensor_isOpen = 0;
		}
	}
	else
	{
		return Set_AT_Error_Info("[0~3]");
	}

	if(g_int_para_3 >= 0 && g_int_para_3 <= 0xf)    //AEB、FCW、HMW、LDW各占一bit
	{
		car_dvr_tmp.warning_aeb = g_int_para_3 & 0x01;
		car_dvr_tmp.warning_fcw = (g_int_para_3>>1) & 0x01;
		car_dvr_tmp.warning_hmw = (g_int_para_3>>2) & 0x01;
		car_dvr_tmp.warning_ldw = (g_int_para_3>>3) & 0x01;
	}
	else
	{
		return Set_AT_Error_Info("[0~15]");
	}
	car_dvr_param = car_dvr_tmp;
	// 先做备份，若写失败就恢复
	Read_CarDvr_Cfg_Param(&car_dvr_tmp);

	// 写操作
	if(Write_CarDvr_Cfg_Param(car_dvr_param)){
//		USART1_Bebug_Print("Write OK", "Write CarDvr Config Parameter OK.", 1);
		// 20220906 lmz update fprintf()-->USART1_Bebug_Print_Num()
		USART1_Bebug_Print_Num("car_dvr_param.g_sensor_isOpen", car_dvr_param.g_sensor_isOpen, 1, 1);
		USART1_Bebug_Print_Num("car_dvr_param.g_sensor_level", car_dvr_param.g_sensor_level, 1, 1);
		USART1_Bebug_Print_Num("car_dvr_param.record_seconds", car_dvr_param.record_seconds, 1, 1);
		USART1_Bebug_Print_Num("car_dvr_param.warning_fcw", car_dvr_param.warning_fcw, 1, 1);
		USART1_Bebug_Print_Num("car_dvr_param.warning_hmw", car_dvr_param.warning_hmw, 1, 1);
		USART1_Bebug_Print_Num("car_dvr_param.warning_ldw", car_dvr_param.warning_ldw, 1, 1);
		USART1_Bebug_Print_Num("car_dvr_param.warning_aeb", car_dvr_param.warning_aeb, 1, 1);

		return 0;
	}else{
		USART1_Bebug_Print("Write FAILED", "Write CarDvr Config Parameter Failed.", 1);
		// 恢复
		car_dvr_param = car_dvr_tmp;
		Write_CarDvr_Cfg_Param(car_dvr_param);
	}
	//	fprintf(USART1_STREAM, "from %d \r\n", param.from);


//	if(!Set_Car_Dvr_Param(car_dvr_tmp))
//	{
//
//		return Set_AT_Error_Info("Set Sensor MGT Failed.");
//	}
	return -1;
}
//param1 开关，param2 灵敏度

static int Sensor_Config_CAR_DVR_Gsensor(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	USART1_Bebug_Print_Flt("g_flt_para_1", g_flt_para_1, 1, 1);		// 20220906 lmz update fprintf()-->USART1_Bebug_Print_Num()
	return 0;
}

static int Config_Agps_Server(const struct cat_command * cmd,const uint8_t * data,const size_t data_size,const size_t args_num)
{
	// at_str1接收的字符串设置为AGPS服务器域名
	USART1_Bebug_Print("at_str1", at_str1, 1);					// 20220906 lmz update fprintf()-->USART1_Bebug_Print()
	return 0;
}

static int Config_Agps_UserName(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	USART1_Bebug_Print("at_str1", at_str1, 1);					// 20220906 lmz update fprintf()-->USART1_Bebug_Print()
	return 0;
}
static int Config_Agps_Password(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	USART1_Bebug_Print("at_str1", at_str1, 1);					// 20220906 lmz update fprintf()-->USART1_Bebug_Print()
	return 0;
}
static int Config_Agps_UserID(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	USART1_Bebug_Print("at_str1", at_str1, 1);					// 20220906 lmz update fprintf()-->USART1_Bebug_Print()
	return 0;
}
extern bool srr_demo_sw;
static int Config_Srr_Demo_Sw(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
//	USART1_Bebug_Print_Num("Config_Srr_Demo_Sw", g_int_para_1, 1,1);
	Sensor_MGT m_sensor_mgt = {0};
	  if (g_int_para_2 >= 0 && g_int_para_2 <= 1) {
		  srr_demo_sw = g_int_para_2;
		  if(srr_demo_sw){
			srr_demo_sw = true;

			if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
				Set_AT_Error_Info("Get srr_demo_sw Failed.");

			}
			m_sensor_mgt.veh_srr_demo_sw = srr_demo_sw;

			// 写配置参数
			if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
				Set_AT_Error_Info("Set srr_demo_sw Failed.");

			}

//			gcz_serial_v1_dma_printf(SERIAL_UART1,"SET SRR_DEMO_ON OK\r\n");
		}
		else{

			srr_demo_sw = false;

			if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
				Set_AT_Error_Info("Get srr_demo_sw Failed.");

			}
			m_sensor_mgt.veh_srr_demo_sw = srr_demo_sw;

			// 写配置参数
			if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
				Set_AT_Error_Info("Set srr_demo_sw Failed.");

			}

//			gcz_serial_v1_dma_printf(SERIAL_UART1,"SET SRR_DEMO_OFF OK\r\n");
		}
		return 0;
	  }
	  else {
	  	    return Set_AT_Error_Info("[0~1]");
	  }

}
static int SETALL(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	// write all parameter
	if(g_int_para_2 >= 1){
//		Print_ALG_Test(wParms);
		wParms.params_version++;
		if (Write_AEB_Config_Param(wParms)) {
		  rParms = wParms;
		  Send_Display_Message_VoiveNum(PARAM_CONFIG_OK_S); // 语音：系统参数配置成功
		  return 0;
		}

		Send_Display_Message_VoiveNum(PARAM_CONFIG_ERR_S); // 语音：系统参数配置失败
		return Set_AT_Error_Info("Parameter Configure Failed.\r\n");
	}
  return 0;
}




//static int Param_Float_Write(const struct cat_variable *var, size_t write_size) {
//  g_flt_para_1 = atof(var->data);
//  return 0;
//}

//static struct cat_variable vars_string[] = { { .type = CAT_VAR_UINT_DEC, .data = &Id_t, .data_size =
//    sizeof(Id_t), .write = Id_write }, { .type = CAT_VAR_BUF_STRING, .data = g_stf_para_arr, .data_size =
//    sizeof(g_stf_para_arr), .write = Param_Float_Write }, { .type = CAT_VAR_UINT_DEC, .data = &rw_t,
//    .data_size = sizeof(rw_t), .write = Rw_Write },{.type = CAT_VAR_BUF_STRING,	.data = &at_str1,
//	.data_size = sizeof(at_str1),.write = NULL} };
/*
 * 字符串转float类型变量集  API
 * 说明：第一个是uint类型输入，第二个是字符串的输入（具有float类型的的string）
 * 为了兼容旧版本，string to float
 */
static struct cat_variable vars_string2Flt[] = {
		// 20220906 lmz add
		{ .type = CAT_VAR_UINT_DEC, 	.data = &g_int_para_1, 	.data_size = sizeof(g_int_para_1),	.name="g_int_para_1",},
		{ .type = CAT_VAR_BUF_STRING, 	.data = &g_stf_para_arr, .data_size = sizeof(g_stf_para_arr), .name="g_stf_para_arr",},
		{ .type = CAT_VAR_UINT_DEC, 	.data = &g_int_para_3, 	.data_size = sizeof(g_int_para_3),	.name="g_int_para_3",},
};
/*
 * int类型变量集 API
 * 说明：支持4个uint类型的数据输入
 */
static struct cat_variable vars_int[] = {
	// 20220905 lmz update
	{ .type = CAT_VAR_INT_DEC,	.data = &g_int_para_1, 	.data_size = sizeof(g_int_para_1),	.name="g_int_para_1",},
	{ .type = CAT_VAR_INT_DEC,	.data = &g_int_para_2, 	.data_size = sizeof(g_int_para_2),	.name="g_int_para_2",},
	{ .type = CAT_VAR_INT_DEC, 	.data = &g_int_para_3, 	.data_size = sizeof(g_int_para_3),	.name="g_int_para_3",},
	{ .type = CAT_VAR_INT_DEC, 	.data = &g_int_para_4, 	.data_size = sizeof(g_int_para_4),	.name="g_int_para_4",}
};
/*
 * string类型变量集 API
 * 说明：第一个是uint类型输入，第二个是字符串的输入
 */
static struct cat_variable vars_string[] = {
	// 20220906 lmz update
	{ .type = CAT_VAR_UINT_DEC,		.data = &g_int_para_1, 	.data_size = sizeof(g_int_para_1),	.name="g_int_para_1", },
	{ .type = CAT_VAR_BUF_STRING, 	.data = &at_str1, 		.data_size = sizeof(at_str1),		.name="at_str1"}
};
/*
 * float类型变量集 API
 * 说明：支持3个float类型的输入（若以后再有float类型的输入，推荐用这个变量集）
 * 暂时未启用
 */
static struct cat_variable vars_float[] = {
	// 20220906 lmz add
	{ .type = CAT_VAR_FLOAT_DEC, 	.data = &g_flt_para_1, 	.data_size = sizeof(g_flt_para_1),	.decimal_places = 3},
	{ .type = CAT_VAR_FLOAT_DEC, 	.data = &g_flt_para_2, 	.data_size = sizeof(g_flt_para_2), 	.decimal_places = 3},
	{ .type = CAT_VAR_FLOAT_DEC, 	.data = &g_flt_para_3, 	.data_size = sizeof(g_flt_para_3), 	.decimal_places = 3},
};
/*
 * 错误信息回调函数
 * 调用函数cat_cmd_error_ack_handler
 */
uint8_t * get_error_ack()
{
	if(strlen(valid_range) > 1){
		sprintf(ack_results, "[ERROR]Valid Range:%s", valid_range);
	}else{	// 20220906 lmz add
		// 当 返回OK时，将上次异常提示信息清空
		memset(ack_results, 0, sizeof(ack_results));
	}

	return ack_results;
}

/******************************** 超卓添加AT指令移植到此处 *****************start************************/
// add lmz 20220905
extern bool g_b_can_sp_show_flag;
extern bool g_b_pwm_sp_show_flag;
extern bool g_b_cal_sp_show;
static int OPN_SHW_CAN_SP(const struct cat_command *cmd)
{
	g_b_can_sp_show_flag = true;
	USART1_Bebug_Print("OPEN SHOW","CAN SPEED OK",1);

	return 0;
}

static int CLS_SHW_CAN_SP(const struct cat_command *cmd)
{
	g_b_can_sp_show_flag = false;
	USART1_Bebug_Print("CLOSE SHOW","CAN SPEED OK",1);

	return 0;
}

static int OPN_SHW_PWM_SP(const struct cat_command *cmd)
{
	g_b_pwm_sp_show_flag = true;
	USART1_Bebug_Print("OPEN SHOW","PWM SPEED OK",1);

	return 0;
}

static int CLS_SHW_PWM_SP(const struct cat_command *cmd)
{
	g_b_pwm_sp_show_flag = false;
	USART1_Bebug_Print("CLOSE SHOW","PWM SPEED OK",1);

	return 0;
}


static int WT_UART_IMU_OPEN(const struct cat_command *cmd)
{
	g_b_imu_show = true;
	gcz_serial_v1_dma_printf(SERIAL_UART1,"WT UART IMU OPEN OK\r\n");

	return 0;
}

static int WT_UART_IMU_CLOSE(const struct cat_command *cmd)
{
	g_b_imu_show = false;
	gcz_serial_v1_dma_printf(SERIAL_UART1,"WT UART IMU CLOSE OK\r\n");

	return 0;
}

static int CAL_SP_SHOW_OPEN(const struct cat_command *cmd)
{
	g_b_cal_sp_show = true;
	gcz_serial_v1_dma_printf(SERIAL_UART1,"CAL SP SHOW OPEN OK\r\n");

	return 0;
}

static int CAL_SP_SHOW_CLOSE(const struct cat_command *cmd)
{
	g_b_cal_sp_show = false;
	gcz_serial_v1_dma_printf(SERIAL_UART1,"CAL SP SHOW OPEN OK\r\n");

	return 0;
}

static int SRR_RF_Y_WARN_DIS(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	uint8_t buffer[20];
	g_flt_para_1 = atof(g_stf_para_arr);
	if (g_flt_para_1 <= -100)
	{
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR_RF_Y_WARN_DIS:%0.4f\r\n",g_f_rigth_font_y_dis);

		sprintf(buffer,"R:%0.4f",rParms.f_rigth_font_y_dis);
	}
	else
	{
		wParms.f_rigth_font_y_dis = g_flt_para_1;
		sprintf(buffer,"W:%0.4f",wParms.f_rigth_font_y_dis);
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR_RF_Y_WARN_DIS:%0.4f\r\n",g_f_rigth_font_y_dis);
	}
	gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR_RF_Y_WARN_DIS:%0.4f\r\n",g_flt_para_1);
	return Set_AT_Error_Info(buffer);
}
static int SRR_RF_X_WARN_ACC(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	uint8_t buffer[20];
	g_flt_para_1 = atof(g_stf_para_arr);
	if (g_flt_para_1 <= -100)
	{
		sprintf(buffer,"R:%0.4f",rParms.f_rigth_font_x_acc);
		//gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR_RF_Y_WARN_ACC:%0.4f\r\n",g_f_rigth_font_x_acc);
	}
	else
	{
		wParms.f_rigth_font_x_acc = g_flt_para_1;
		sprintf(buffer,"W:%0.4f",wParms.f_rigth_font_x_acc);
		//gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR_RF_Y_WARN_ACC:%0.4f\r\n",g_f_rigth_font_x_acc);
	}
	return Set_AT_Error_Info(buffer);
}
static int SRR_RF_X_MAX_TTC(const struct cat_command *cmd,  const uint8_t *data,  const size_t data_size,  const size_t args_num)
{
	uint8_t buffer[20];
	g_flt_para_1 = atof(g_stf_para_arr);
	if (g_flt_para_1 <= -100)
	{
		sprintf(buffer,"R:%0.4f",rParms.f_rigth_font_x_max_ttc);
		//gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR_RF_Y_WARN_ACC:%0.4f\r\n",g_f_rigth_font_x_acc);
	}
	else
	{
		wParms.f_rigth_font_x_max_ttc = g_flt_para_1;
		sprintf(buffer,"W:%0.4f",wParms.f_rigth_font_x_max_ttc);
		//gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR_RF_Y_WARN_ACC:%0.4f\r\n",g_f_rigth_font_x_acc);
	}
	return Set_AT_Error_Info(buffer);
}

/*
 * 输出所有指令及语法格式
 */
static int Print_CMD_List(const struct cat_command *cmd)
{
	return CAT_RETURN_STATE_PRINT_CMD_LIST_OK;
}
/******************************** 超卓添加AT指令移植到此处 *****************end************************/
/*
 * AT指令集 API
 * .name:接口名字，AT连接.name就是触发的AT指令；举例，AT++Set_Vehicle_Type=1,1
 * .write:写函数接口（后跟回调函数）（目前所有逻辑判断都在此），会在.var参数回调函数执行完毕后被执行
 * .read:读函数接口（后跟回调函数）
 * .run：运行函数接口（后跟回调函数），直接执行不带参数指令的查询指令（例如"AT+Sensor_GetInfo_CAR_DVR"），执行优先级在.write和.read之后。
 * .var:参数集判断函数接口（后跟回调函数），目前该回调函数有vars_int(可接收4个参数的整数类型)和vars_string2Flt（可接收整数和字符串类型混搭）
 * .var_name:参数个数
 * .error_ack:打印错误信息的接口（后跟回调函数），自研添加项。
 * .ok_ack:打印正确（OK）后的信息提示接口（后跟回调函数），自研添加项，暂时未用。
 * 说明：
 * 1、因原始AT SDK不支持float类型输入，所以就有了现在vars_string2Flt参数集，将float类型数据以string类型数据输入，并执行强转float = atof(string)
 * 2、所有回调函数都必须有返回值(return 0/-1);没有回调函数就被持续调用，最终会触发看门狗导致设备重启。☆☆☆☆☆
 */
static struct cat_command cmds[] = {
		{
			.name = "+Set_Vehicle_Type",
			.write = Set_Vehicle_Type,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Set_Params_Version",
			.write = Set_Params_Version,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Set_Test_Brake",
			.write = Set_Test_Brake,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+AEB_CMS_Enable",
			.write = AEB_CMS_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Enable",
			.write = CMS_HMW_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_TTC_Enable",
			.write = CMS_TTC_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+AEB_TTC_Enable",
			.write = AEB_TTC_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SSS_Enable",
			.write = SSS_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+HMW_Dynamic_Thr_Enable",
			.write = HMW_Dynamic_Thr_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Chassis_Safe_Strategy_Enable",
			.write = Chassis_Safe_Strategy_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+LDW_Enable",
			.write = LDW_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Displayer_Switch_Enable",
			.write = Displayer_Switch_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Retract_Brake_Enable",
			.write = Retract_Brake_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Vechile_Width",
			.write = Vechile_Width,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
#if 1		// CAN 波特率配置, 20220906 lmz note
		{
			.name = "+CAN_0_Baud_Rate",
			.write = CAN_0_Baud_Rate,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CAN_1_Baud_Rate",
			.write = CAN_1_Baud_Rate,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CAN_2_Baud_Rate",
			.write = CAN_2_Baud_Rate,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CAN_3_Baud_Rate",
			.write = CAN_3_Baud_Rate,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CAN_4_Baud_Rate",
			.write = CAN_4_Baud_Rate,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CAN_5_Baud_Rate",
			.write = CAN_5_Baud_Rate,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
#endif
		{
			.name = "+PWM_Magnetic_Valve_Params",
			.write = PWM_Magnetic_Valve_Params,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Brake_Cooling_Time",
			.write = Brake_Cooling_Time,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Driver_Brake_Cooling_Time",
			.write = Driver_Brake_Cooling_Time,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Max_Brake_Keep_Time",
			.write = Max_Brake_Keep_Time,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Air_Brake_Delay_Time",
			.write = Air_Brake_Delay_Time,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Max_Percent_Decelerate",
			.write = Max_Percent_Decelerate,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Min_Enable_Speed",
			.write = Min_Enable_Speed,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Max_Output_dec",
			.write = Max_Output_dec,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Ratio_Force_To_Deceleration",
			.write = Ratio_Force_To_Deceleration,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Brake_Time_Thr",
			.write = CMS_HMW_Brake_Time_Thr,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Brake_Force_Feel_Para",
			.write = CMS_HMW_Brake_Force_Feel_Para,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Warning_Time_Thr",
			.write = CMS_HMW_Warning_Time_Thr,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Dynamic_Offset_Speed_L",
			.write = CMS_HMW_Dynamic_Offset_Speed_L,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Dynamic_Offset_Value_L",
			.write = CMS_HMW_Dynamic_Offset_Value_L,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Dynamic_Offset_Speed_H",
			.write = CMS_HMW_Dynamic_Offset_Speed_H,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Dynamic_Offset_Value_H",
			.write = CMS_HMW_Dynamic_Offset_Value_H,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Time_Offset_Night",
			.write = CMS_HMW_Time_Offset_Night,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Time_Offset_Night_StartT",
			.write = CMS_HMW_Time_Offset_Night_StartT,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_HMW_Time_Offset_Night_EndT",
			.write = CMS_HMW_Time_Offset_Night_EndT,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_TTC_Brake_Time_Thr",
			.write = CMS_TTC_Brake_Time_Thr,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_TTC_Brake_Force_Feel_Para",
			.write = CMS_TTC_Brake_Force_Feel_Para,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_TTC_Warning_Time_Level_First",
			.write = CMS_TTC_Warning_Time_Level_First,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+CMS_TTC_Warning_Time_Level_Second",
			.write = CMS_TTC_Warning_Time_Level_Second,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+AEB_Decelerate_Set",
			.write = AEB_Decelerate_Set,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+AEB_Stop_Distance",
			.write = AEB_Stop_Distance,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+AEB_TTC_Warning_Time_Level_First",
			.write = AEB_TTC_Warning_Time_Level_First,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+AEB_TTC_Warning_Time_Level_Second",
			.write = AEB_TTC_Warning_Time_Level_Second,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SSS_Brake_Force",
			.write = SSS_Brake_Force,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SSS_Break_Enable_Distance",
			.write = SSS_Break_Enable_Distance,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SSS_Warning_Enable_Distance",
			.write = SSS_Warning_Enable_Distance,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SSS_Max_Enable_Speed",
			.write = SSS_Max_Enable_Speed,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SSS_FR_FL_Install_Distance_To_Side",
			.write = SSS_FR_FL_Install_Distance_To_Side,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SSS_Stop_Distance",
			.write = SSS_Stop_Distance,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SSS_Default_Turn_Angle",
			.write = SSS_Default_Turn_Angle,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+WheelSpeed_Coefficient",
			.write = WheelSpeed_Coefficient,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Vehicle_Speed_Obtain_Method",
			.write = Vehicle_Speed_Obtain_Method,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Vehicle_State_Obtain_Method",
			.write = Vehicle_State_Obtain_Method,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SETALL",
			.write = SETALL,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		//gcz 2022-05-07
		{
			.name = "+APP_START_CALIBRATION_SPEED_FLOW",
			.write = APP_START_CALIBRATION_SPEED_FLOW,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+APP_SET_CALIBRATION_SPEED",
			.write = APP_SET_CALIBRATION_SPEED,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		//gcz 2022-05-13
		{
			.name = "+APP_CHECK_ULT_DIS",
			.write = APP_CHECK_ULT_DIS,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		//gcz
		{
			.name = "+ULT_RADAR_MONITOR_Enable",
			.write = ULT_RADAR_MONITOR_Enable,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
#if 1			// 20220906 lmz note
		/****************** 适配不同传感器，参数配置  ******************/
		{
			.name = "+Sensor_Mgt_Project_Type",
			.write = Sensor_Mgt_Project_Type,		// 1，项目类型
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_Product_Type",
			.write = Sensor_Mgt_Product_Type,		// 1，产品类型
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_4G",
			.write = Sensor_Mgt_4G,					// 功能开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_GPS",
			.write = Sensor_Mgt_GPS,				// 功能开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_DBL_Cam",
			.write = Sensor_Mgt_DBL_Cam,			// 功能开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_SGL_Cam",
			.write = Sensor_Mgt_SGL_Cam,			// 功能开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_Ultra_radar_Front",
			.write = Sensor_Mgt_Ultra_radar_Front,	// 单侧开关，单个探头开关，厂家，控制器编号
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_Ultra_radar_End",
			.write = Sensor_Mgt_Ultra_radar_End,	// 单侧开关，单个探头开关，厂家，控制器编号
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_Ultra_radar_Left",
			.write = Sensor_Mgt_Ultra_radar_Left,	// 单侧开关，单个探头开关，厂家，控制器编号
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_Ultra_radar_Right",
			.write = Sensor_Mgt_Ultra_radar_Right,	// 单侧开关，单个探头开关，厂家，控制器编号
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_MMW_radar_Front",
			.write = Sensor_Mgt_MMW_radar_Front,	// 单侧开关，单个雷达开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_MMW_radar_End",
			.write = Sensor_Mgt_MMW_radar_End,		// 单侧开关，单个雷达开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_MMW_radar_Left",
			.write = Sensor_Mgt_MMW_radar_Left,		// 单侧开关，单个雷达开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_MMW_radar_Right",
			.write = Sensor_Mgt_MMW_radar_Right,	// 单侧开关，单个雷达开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_IMU",
			.write = Sensor_Mgt_IMU,				// 功能开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_CAR_DVR",
			.write = Sensor_Mgt_CAR_DVR,			// 功能开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_Mgt_Actuator",
			.write = Sensor_Mgt_Actuator,			// 功能开关，厂家
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
        {
			.name = "+HELP",
			.run = Print_CMD_List,					// 打印指令列表
        },
#endif
		/******************lyj 参数配置  ******************/
        {
            .name = "+Sensor_Config_CAR_DVR",
            .write = Sensor_Config_CAR_DVR,
            .var = vars_int,
            .var_num = sizeof(vars_int) / sizeof(vars_int[0]),
            .error_ack = get_error_ack,
        },
		{
			.name = "+Config_Agps_Server",
			.write = Config_Agps_Server,		// AT+Config_Agps_Server=
			.var = vars_string,
			.var_num = sizeof(vars_string) / sizeof(vars_string[0]),
		},
		{
			.name = "+Config_Agps_UserName",
			.write = Config_Agps_UserName,
			.var = vars_string,
			.var_num = sizeof(vars_string) / sizeof(vars_string[0]),
		},
		{
			.name = "+Config_Agps_Password",
			.write = Config_Agps_Password,
			.var = vars_string,
			.var_num = sizeof(vars_string) / sizeof(vars_string[0]),
		},
		{
			.name = "+Config_Agps_UserID",
			.write = Config_Agps_UserID,
			.var = vars_string,
			.var_num = sizeof(vars_string) / sizeof(vars_string[0]),
		},
		{
			.name = "+Config_Srr_Demo_Sw",
			.write = Config_Srr_Demo_Sw,
			.var = vars_int,
			.var_num = sizeof(vars_int) / sizeof(vars_int[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+Sensor_GetInfo_CAR_DVR",	// 没有参数触发返回OK; 有参数(多一个空格也是有参数)就报错ERROR
			.run = Sensor_GetInfo_CAR_DVR,		// 20220905 lmz update
		},
#if 1		// add lmz 20220905 , 为了调试用
		/******************************** 超卓添加AT指令移植到此处 *****************start************************/
		// add lmz 20220905
		{
			.name = "+OPN_SHW_CAN_SP",		// 开启MM显示
			.run = OPN_SHW_CAN_SP,			// 没有参数触发返回OK; 有参数(多一个空格也是有参数)就报错ERROR
		},
		{
			.name = "+CLS_SHW_CAN_SP",		// 关闭MM显示
			.run = CLS_SHW_CAN_SP,
		},
		{
			.name = "+OPN_SHW_PWM_SP",		// 开启MM显示
			.run = OPN_SHW_PWM_SP,
		},
		{
			.name = "+CLS_SHW_PWM_SP",		// 关闭MM显示
			.run = CLS_SHW_PWM_SP,
		},
		{
			.name = "+WT_UART_IMU_OPEN",	// 状态数据测试
			.run = WT_UART_IMU_OPEN,
		},
		{
			.name = "+WT_UART_IMU_CLOSE",	// 状态数据测试
			.run = WT_UART_IMU_CLOSE,
		},
		{
			.name = "+CAL_SP_SHOW_OPEN",	// 状态数据测试
			.run = CAL_SP_SHOW_OPEN,
		},
		{
			.name = "+CAL_SP_SHOW_CLOSE",	// 状态数据测试
			.run = CAL_SP_SHOW_CLOSE,
		},
		/******************************** 超卓添加AT指令移植到此处 *****************end************************/
#endif
		{
			.name = "+SRR_RF_Y_WARN_DIS",	// 角雷达右前Y方向预警距离设置
			.write = SRR_RF_Y_WARN_DIS,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SRR_RF_X_WARN_ACC",	// 角雷达右前预警X方向TTC计算时间所用加速度设置
			.write = SRR_RF_X_WARN_ACC,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
		{
			.name = "+SRR_RF_X_MAX_TTC",	// 角雷达右前预警X方向最大TTC阈值
			.write = SRR_RF_X_MAX_TTC,
			.var = vars_string2Flt,
			.var_num = sizeof(vars_string2Flt) / sizeof(vars_string2Flt[0]),
			.error_ack = get_error_ack,
		},
};
/*
 * AT指令库回调函数的指令组 API
 * .cmd:参数集，后面是个指令集的回调函数
 * .cmd_num:指令数
 */
static struct cat_command_group cmd_group =
    { .cmd = cmds, .cmd_num = sizeof(cmds) / sizeof(cmds[0]), };

//static void prepare_input(const char *text) {
//  input_text = text;
//  input_index = 0;
//  // 20220905 lmz update
//  g_int_para_1 = g_int_para_2 = g_int_para_3 = g_int_para_4 = 0;
//  g_flt_para_1 = 0.0f;
//  memset(g_stf_para_arr, 0, sizeof(g_stf_para_arr));
//  memset(at_str1, 0, sizeof(at_str1));
//  memset(ack_results, 0, sizeof(ack_results));
//  memset(valid_range, 0, sizeof(valid_range));
//}

/*
 * 对外获取AT指令组 API
 * 参数：AT指令组指针，会在cat_init(self, &desc_, &iface_, NULL);中desc初始赋值时被赋值（其实就是回调函数）
 * 说明：AT指令组可以有多个。
 */
void  AlgPrase_GetAtCmd(void **cmd){
  *cmd = &cmd_group;
}

void Init_AEB_Alg_Para() {
  rParms.params_version = 1;
  rParms.Brake_Cooling_Time = 0.0;
  rParms.Driver_Brake_Cooling_Time = 0.0;
  rParms.Max_Brake_Keep_Time = 0.0;
  rParms.Air_Brake_Delay_Time = 0.0;
  rParms.Max_Percent_Decelerate = 0.0;
  rParms.Min_Enable_Speed = 0.0;
  rParms.Max_Output_dec = 0.0;
  rParms.Ratio_Force_To_Deceleration = 0.0;
  rParms.CMS_HMW_Brake_Time_Thr = 0.0;
  rParms.CMS_HMW_Brake_Force_Feel_Para = 0.0;
  rParms.CMS_HMW_Warning_Time_Thr = 0.0;
  rParms.CMS_HMW_Dynamic_Offset_Speed_L = 0.0;
  rParms.CMS_HMW_Dynamic_Offset_Value_L = 0.0;
  rParms.CMS_HMW_Dynamic_Offset_Speed_H = 0.0;
  rParms.CMS_HMW_Dynamic_Offset_Value_H = 0.0;
  rParms.CMS_HMW_Time_Offset_Night = 0.0; //HMW
  rParms.CMS_HMW_Time_Offset_Night_StartT = 0.0; //Hour
  rParms.CMS_HMW_Time_Offset_Night_EndT = 0.0; //Hour

  rParms.CMS_TTC_Brake_Time_Thr = 0.0;
  rParms.CMS_TTC_Brake_Force_Feel_Para = 0.0;
  rParms.CMS_TTC_Warning_Time_Level_First = 0.0;
  rParms.CMS_TTC_Warning_Time_Level_Second = 0.0;

  rParms.AEB_Decelerate_Set = 0.0;
  rParms.AEB_Stop_Distance = 0.0;
  rParms.AEB_TTC_Warning_Time_Level_First = 0.0;
  rParms.AEB_TTC_Warning_Time_Level_Second = 0.0;

  rParms.SSS_Brake_Force = 0.0;
  rParms.SSS_Break_Enable_Distance = 0.0;
  rParms.SSS_Warning_Enable_Distance = 0.0;
  rParms.SSS_Max_Enable_Speed = 0.0;
  rParms.SSS_FR_FL_Install_Distance_To_Side = 0.0;
  rParms.SSS_Stop_Distance = 0.0;
  rParms.SSS_Default_Turn_Angle = 0.0;

  rParms.WheelSpeed_Coefficient = 0.0;
  rParms.PWM_magnetic_valve_N = 0;
  rParms.PWM_magnetic_valve_Scale = 0;
  rParms.PWM_magnetic_valve_Switch = 0;
  rParms.PWM_magnetic_valve_T = 0;

  rParms.switch_g.AEB_CMS_Enable = 0;
  rParms.switch_g.CMS_HMW_Enable = 0;
  rParms.switch_g.CMS_TTC_Enable = 0;
  rParms.switch_g.AEB_TTC_Enable = 0;
  rParms.switch_g.SSS_Enable = 0;
  rParms.switch_g.HMW_Dynamic_Thr_Enable = 0;
  rParms.switch_g.Chassis_Safe_Strategy_Enable = 0;
  rParms.switch_g.LDW_Enable = 0;
  rParms.switch_g.Displayer_Switch_Enable = 0;
  rParms.switch_g.Retract_Brake_Enable = 0;

  rParms.Vehicle_Speed_Obtain_Method = 0;
  rParms.Vehicle_State_Obtain_Method = 0;
  rParms.Vehicle_Brake_Control_Mode = 0;

  rParms.Vechile_Width = 0.0;

  rParms.CAN_0_Baud_Rate = 500;
  rParms.CAN_1_Baud_Rate = 500;
  rParms.CAN_2_Baud_Rate = 250;
  rParms.CAN_3_Baud_Rate = 250;
  rParms.CAN_4_Baud_Rate = 500;
  rParms.CAN_5_Baud_Rate = 500;

  rParms.params_version = 0;
  int i1;
  for (i1 = 0; i1 <= 1; i1++)
    rParms.switch_g.ultrasonic_radar_monitor_ensble[i1] = 0;
  for (i1 = 2; i1 <= 5; i1++)
    rParms.switch_g.ultrasonic_radar_monitor_ensble[i1] = 1;
  for (i1 = 6; i1 < MAX_ULT_RADAR_NBR; i1++)
    rParms.switch_g.ultrasonic_radar_monitor_ensble[i1] = 0;

  rParms.f_rigth_font_y_dis = 0.0;
  rParms.f_rigth_font_x_acc = 0.0;
  rParms.f_rigth_font_x_max_ttc = 0.0;
  wParms = rParms;
}
/*
 * 加载默认参数，只有setall操作才有rParm = wParm;
 */
void Load_And_Write_AEB_Default_Para() {
  // 测试使用
  rParms.switch_g.AEB_CMS_Enable = 1;
  rParms.switch_g.CMS_HMW_Enable = 1;
  rParms.switch_g.CMS_TTC_Enable = 1;
  rParms.switch_g.AEB_TTC_Enable = 1;
  rParms.switch_g.SSS_Enable = 1;
  rParms.switch_g.HMW_Dynamic_Thr_Enable = 1;
  rParms.switch_g.Chassis_Safe_Strategy_Enable = 1;
  rParms.switch_g.LDW_Enable = 1;
  rParms.switch_g.Displayer_Switch_Enable = 1;
  rParms.switch_g.Retract_Brake_Enable = 1;

  rParms.params_version = 1;
  rParms.Brake_Cooling_Time = 5.0;
  rParms.Driver_Brake_Cooling_Time = 5.0;
  rParms.Max_Brake_Keep_Time = 10.0;
  rParms.Air_Brake_Delay_Time = 0.2;
  rParms.Max_Percent_Decelerate = 0.5;
  rParms.Min_Enable_Speed = 10.0;
  rParms.Max_Output_dec = 10.0;
  rParms.Ratio_Force_To_Deceleration = 1.0;
  rParms.CMS_HMW_Brake_Time_Thr = 1.0;
  rParms.CMS_HMW_Brake_Force_Feel_Para = 4.0;
  rParms.CMS_HMW_Warning_Time_Thr = 1.1;
  rParms.CMS_HMW_Dynamic_Offset_Speed_L = 30.0;
  rParms.CMS_HMW_Dynamic_Offset_Value_L = 0.5;
  rParms.CMS_HMW_Dynamic_Offset_Speed_H = 70.0;
  rParms.CMS_HMW_Dynamic_Offset_Value_H = 0.5;
  rParms.CMS_HMW_Time_Offset_Night = 0.3; //HMW
  rParms.CMS_HMW_Time_Offset_Night_StartT = 23.5; //Hour
  rParms.CMS_HMW_Time_Offset_Night_EndT = 5.0; //Hour

  rParms.CMS_TTC_Brake_Time_Thr = 1.0;
  rParms.CMS_TTC_Brake_Force_Feel_Para = 0.7;
  rParms.CMS_TTC_Warning_Time_Level_First = 0.1;
  rParms.CMS_TTC_Warning_Time_Level_Second = 0.0;

  rParms.AEB_Decelerate_Set = 5.0;
  rParms.AEB_Stop_Distance = 0.5;
  rParms.AEB_TTC_Warning_Time_Level_First = 1.4;
  rParms.AEB_TTC_Warning_Time_Level_Second = 0.8;

  rParms.SSS_Brake_Force = 2.0;
  rParms.SSS_Break_Enable_Distance = 0.8;
  rParms.SSS_Warning_Enable_Distance = 2.0;
  rParms.SSS_Max_Enable_Speed = 20.0;
  rParms.SSS_FR_FL_Install_Distance_To_Side = 0.3;
  rParms.SSS_Stop_Distance = 0.3;
  rParms.SSS_Default_Turn_Angle = 30.0;

  rParms.WheelSpeed_Coefficient = 100.0;
  rParms.PWM_magnetic_valve_N = 3;
  rParms.PWM_magnetic_valve_Scale = 25;
  rParms.PWM_magnetic_valve_Switch = 0;
  rParms.PWM_magnetic_valve_T = 30;

  rParms.switch_g.AEB_CMS_Enable = 1;
  rParms.switch_g.CMS_HMW_Enable = 1;
  rParms.switch_g.CMS_TTC_Enable = 1;
  rParms.switch_g.AEB_TTC_Enable = 0;
  rParms.switch_g.SSS_Enable = 1;
  rParms.switch_g.HMW_Dynamic_Thr_Enable = 1;
  rParms.switch_g.Chassis_Safe_Strategy_Enable = 1;
  rParms.switch_g.LDW_Enable = 1;
  rParms.switch_g.Displayer_Switch_Enable = 1;
  rParms.switch_g.Retract_Brake_Enable = 1;

  rParms.Vehicle_Speed_Obtain_Method = 1;
  rParms.Vehicle_State_Obtain_Method = 1;
  rParms.Vehicle_Brake_Control_Mode = 0;

  rParms.Vechile_Width = 2.5;
  rParms.CAN_0_Baud_Rate = 500;
  rParms.CAN_1_Baud_Rate = 500;
  rParms.CAN_2_Baud_Rate = 250;
  rParms.CAN_3_Baud_Rate = 250;
  rParms.CAN_4_Baud_Rate = 500;
  rParms.CAN_5_Baud_Rate = 500;

  rParms.params_version = 1;
  uint8_t i1;
  for (i1 = 0; i1 <= 1; i1++)
    rParms.switch_g.ultrasonic_radar_monitor_ensble[i1] = 0;
  for (i1 = 2; i1 <= 5; i1++)
    rParms.switch_g.ultrasonic_radar_monitor_ensble[i1] = 1;
  for (i1 = 6; i1 < MAX_ULT_RADAR_NBR; i1++)
    rParms.switch_g.ultrasonic_radar_monitor_ensble[i1] = 0;

  rParms.f_rigth_font_y_dis = 0.3;
  rParms.f_rigth_font_x_acc = 3.0;
  rParms.f_rigth_font_x_max_ttc = 2.5;
  wParms = rParms;

  USART1_Bebug_Print("Param Init", "Load Default Parameters.", 1);
}

extern uint8_t Status_System_Param;
/*
 * AT参数配置参数化
 */
void AlgParse_Init() {
  Init_AEB_Alg_Para(); // 获取默认参数，杰哥更新

  // 获取车辆类型
  server_info.vehicle_type = Get_Vehicle_Type_ExFlh(); // 获取参数ID
  Brake_Test_Pressure_Bar = 0; //初始测试压力0

  USART1_Bebug_Print_Num("Vehicle_Type", server_info.vehicle_type, 1, 1);
  // 车辆ID为空采用默认参数，非空就获取参数
  if (server_info.vehicle_type == 0) {
    USART1_Bebug_Print("WARNING", "Current Parameter ID is 0, Please Set ID", 1);

    // 加载默认参数
    Load_And_Write_AEB_Default_Para();
  } else { // 采用默认参数
    KunLun_AEB_Para_Cfg algPara = { 0 };
    if (Read_AEB_Config_Param(&algPara)) {
      rParms = algPara;
      wParms = algPara;
    } else { // 采用默认参数
      USART1_Bebug_Print("ERROR", "Read Configure Parameter failed.", 1);
      Load_And_Write_AEB_Default_Para();
    }
  }
}

//gcz 2022-05-07 -----------------------------------------------------------------------------------------------
enum {
  GPIO_IN_LEFT = 0, GPIO_IN_RIGHT, GPIO_IN_BRAKE, GPIO_IN_BACK
};
extern float GPS_Format_To_Degree(uint8_t *dms);
CHECK_OPT g_st_4g_net_detection;
CHECK_OPT g_st_4g_sim_card_detection;
CHECK_OPT g_st_gps_detection;
UART_OBJ g_st_uart0_cache_opt_obj;
//查找指定位置，指定字符的返回目标字符所在位置
//参数：输入字符串    目标字符  需要查找的索引个数
//返回：目标字符所在位置
int FindIndexPosStrPos(char *pInBuffer, char cTargetChar, unsigned int IndexNbr) {
  unsigned int len = strlen(pInBuffer), i1, cnt = 0, res = FALSE;
  for (i1 = 0; i1 < len; i1++) {
    if (pInBuffer[i1] == cTargetChar) {
      cnt++;
    }
    if (cnt == IndexNbr) {
      res = TRUE;
      break;
    }
  }
  if (res == TRUE) {
    return i1;
  } else {
    return -1;
  }

}

//CAN 外设检测：检测CAN状态
//参数：UARTx类型
static void detection_can_status(STREAM* USART_Print) {
  uint8_t buffer[100];
  _STRUCT_VALVE_PARA *p_st_valve = get_valveparas();
  int8_t VehicleStatus;
  int8_t Proportional_valve_status, Proportional_valve_output_code;
  //整车CAN状态检测判定
//  if (rParms.Vehicle_State_Obtain_Method == 1) {
  if (rParms.Vehicle_Speed_Obtain_Method == 1) {
    VehicleStatus = -2;
  } else {
    VehicleStatus = (stCanCommSta.stVehicle.status == OFFLINE) ? -1 : 0;
  }

  //比例阀CAN状态检测判定
  if (server_info.vehicle_type == 1) {
    Proportional_valve_status = (stCanCommSta.Proportional_valve.status == OFFLINE) ? -1 : 0;
  } else
    Proportional_valve_status = -2;

  if (Proportional_valve_status >= 0)
    Proportional_valve_output_code = p_st_valve->Fault_Code;
  else
    Proportional_valve_output_code = Proportional_valve_status;
  sprintf(buffer,
      "CAM:%d\r\nMM:%d\r\nVHCL:%d\r\nPV:%d\r\nDISPR:%d\r\nULT:%d\r\n",
      (stCanCommSta.stCamera.status == OFFLINE) ? -1 : camera_data.ErrorCode,
     /* -2*/((stCanCommSta.stRadar.status| stCanCommSta.stSrr.status)== OFFLINE) ? -1 : 0, //stCanCommSta.stRadar.status
      VehicleStatus, Proportional_valve_output_code,
      (stCanCommSta.stDisplayer.status == OFFLINE) ? -1 : 0,
      (stCanCommSta.stHRadar.status == OFFLINE) ? -1 : 0);
  fprintf(USART_Print, buffer);
}
extern void delay_ms(uint32_t nms);
//对GPIO输入检测，左、右转向灯，刹车灯，倒车灯
//参数：UARTx类型 	GPIO需要检测的类型
static void detection_gpio_input(STREAM* USART_Print, uint8_t gpio_in_type) {
  uint8_t res = -1;
  uint8_t buffer[30], strhead[6];
  memset(strhead, 0, sizeof(strhead));
  int cnt = 3;
  switch (gpio_in_type) {
  case GPIO_IN_LEFT:
    while (((res = Read_turnleft_signal()) == Bit_RESET) && (cnt > 0)) {
      cnt--;
      IWDT_Feed_The_Dog();
      delay_ms(1000);
    }
    //res = stVehicleParas.LeftFlag;
    strcpy(strhead, "LEFT");
    break;
  case GPIO_IN_RIGHT:
    while (((res = Read_turnright_signal()) == Bit_RESET) && (cnt > 0)) {
      cnt--;
      IWDT_Feed_The_Dog();
      delay_ms(1000);
    }
    //res = stVehicleParas.RightFlag;
    strcpy(strhead, "RIGHT");
    break;
  case GPIO_IN_BRAKE:
    while (((res = Read_stop_signal()) == Bit_RESET) && (cnt > 0)) {
      cnt--;
      IWDT_Feed_The_Dog();
      delay_ms(1000);
    }
    //res = stVehicleParas.BrakeFlag;
    strcpy(strhead, "BRAKE");
    break;
  case GPIO_IN_BACK:
    while (((res = Read_turnback_signal()) == Bit_RESET) && (cnt > 0)) {
      cnt--;
      IWDT_Feed_The_Dog();
      delay_ms(1000);
    }
    //res = stVehicleParas.ReverseGear;
    strcpy(strhead, "BACK");
    break;
  default:
    break;
  }
  if (res != -1) {
    sprintf(buffer, "%s_SIG:%d\r\n", strhead, res);
    fprintf(USART_Print, buffer);
  } else {
    fprintf(USART_Print, "ERROR:invalid input type\r\n");
  }
}

//输出GPS信息
//参数：UARTx类型
static void get_gps_info(STREAM* USART_Print, uint8_t *plon, uint8_t *plat, uint8_t sateNum) {
  uint8_t buffer[60];
  sprintf(buffer, "GPS_LONG:%0.3f\r\nGPS_LAT:%0.3f\r\nGPS_NBR:%d\r\n",
      GPS_Format_To_Degree(plon/*gpsInfo.lon*/), GPS_Format_To_Degree(plat/*gpsInfo.lat*/),
      sateNum/*gpsInfo.sateNum*/);
  fprintf(USART_Print, buffer);
}
//检测4G
static void ping_web_for_detection_4g(STREAM* USART_Print, uint8_t *pweb_addr) //www.baidu.com
{
  uint8_t buffer[100];
  if (strlen(pweb_addr) > sizeof(buffer)) {
    fprintf(USART_Print, "4G_PING:-1\r\n");
    return;
  }
  memset(&g_st_4g_net_detection, 0, sizeof(g_st_4g_net_detection));
  memset(&g_st_uart0_cache_opt_obj, 0, sizeof(g_st_uart0_cache_opt_obj));
  g_st_4g_net_detection.check_state_manchine.state = WAIT_RCV_PING_RSPS;
  sprintf(buffer, "AT+QPING=1,\"%s\",1,1\r\n", pweb_addr);

//	fprintf(USART1_STREAM,"****AT_4g_p:%s",buffer);
  fprintf(USART0_STREAM, buffer);
  g_st_4g_net_detection.check_flag = TRUE;
}
//检测SIM卡
static void detection_sim_card() {
  uint8_t buffer[100];
  memset(&g_st_4g_sim_card_detection, 0, sizeof(g_st_4g_sim_card_detection));
  memset(&g_st_uart0_cache_opt_obj, 0, sizeof(g_st_uart0_cache_opt_obj));
  g_st_4g_sim_card_detection.check_state_manchine.state = WAIT_RCV_SET_SIMSTAT_RSPS;
  sprintf(buffer, "AT+QSIMSTAT=1\r\n");
  fprintf(USART0_STREAM, buffer);
  g_st_4g_sim_card_detection.check_flag = TRUE;

}
//检测GPS
static void detection_gps_card() {
  uint8_t buffer[100];
  memset(&g_st_gps_detection, 0, sizeof(g_st_gps_detection));
  memset(&g_st_uart0_cache_opt_obj, 0, sizeof(g_st_uart0_cache_opt_obj));
  g_st_gps_detection.check_state_manchine.state = WAIT_RCV_GPS_OPEN;
  sprintf(buffer, "AT+QGPS=1\r\n");
  fprintf(USART0_STREAM, buffer);
  g_st_gps_detection.check_flag = TRUE;
}
//接收串口字符到缓存
//说明：由于现有的串口接收机制，为1个255字节的单纬数组，接收数据多处调用，且其机制很不友好，将所有信息放到一个数组，而无拆分包处理，因此，本功能独立使用自身的串口缓存机制
//缓存3组，每组50字节，遇到\n与超长，进行封包处理
static void UartRcvDataToBuffer(uint8_t data, UART_OBJ *pStUartOptObj) {
  int8_t res = FALSE;
  if ((g_st_4g_net_detection.check_state_manchine.state == WAIT_RCV_PING_RSPS) //只有需要工作的时候才功能，平时不工作
  || (g_st_4g_sim_card_detection.check_state_manchine.state == WAIT_RCV_SET_SIMSTAT_RSPS)
      || (g_st_4g_sim_card_detection.check_state_manchine.state == WAIT_RCV_GET_SIMSTAT_REPORT_RSPS)
      || (g_st_gps_detection.check_state_manchine.state == WAIT_RCV_GPS_OPEN)
      || (g_st_gps_detection.check_state_manchine.state == WAIT_RCV_SET_QGPSLOC_RSPS)) {
    //	USART_SendData(USART1_SFR,data);
    pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].UartRcvBuffer[pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].DataLen] =
        data;
    pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].DataLen++;
    if (pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].DataLen >= (NEW_UART0_DATA_LEN - 1)) {
      pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].used_flag = TRUE;
      res = TRUE;
    } else {
      if (pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].UartRcvBuffer[pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].DataLen
          - 1] == '\n') //封包处理
          {
        pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].used_flag = TRUE;
        res = TRUE;
      }
    }
		if (res == TRUE)
		{
			if(pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].DataLen < 4){
				pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].DataLen = 0;
				return ;
			}
			//	fprintf(USART1_STREAM,"**PutPt:%d\r\n",pStUartOptObj->PutPrt);
			pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].UartRcvBuffer[pStUartOptObj->st_uart_opt[pStUartOptObj->PutPrt].DataLen] = 0;
			pStUartOptObj->PutPrt++;
			if (pStUartOptObj->PutPrt >= NEW_UART0_CACHE_NBR)
				pStUartOptObj->PutPrt = 0;
		}
	}
}

//4G ping的目标字符串监控
//参数：输入的目标字符串
static void check_4g_ping_rsps_string(CHECK_OPT *pStCheckOpt, uint8_t *p_buffer) {
  if (pStCheckOpt->check_state_manchine.state == WAIT_RCV_PING_RSPS) {
    //	fprintf(USART1_STREAM,"****4g_p:%s\r\n",g_st_4g_net_detection.UartRcvBuffer);
    if (strstr(p_buffer, "+QPING:") != NULL) {
      strcpy(pStCheckOpt->check_state_manchine.rsps_buffer, p_buffer);
      pStCheckOpt->check_state_manchine.state = RCV_RSPS_PING_SUCCEED;
      fprintf(USART1_STREAM, "---rcv_rsps:%s\r\n", pStCheckOpt->check_state_manchine.rsps_buffer);
    } else if (strstr(p_buffer, "ERROR") != NULL) {
      strcpy(pStCheckOpt->check_state_manchine.rsps_buffer, p_buffer);
      pStCheckOpt->check_state_manchine.state = RCV_RSPS_PING_SUCCEED;
    }
  }
}
//SIM卡检测的目标字符串监控
//参数：输入的目标字符串
static void check_sim_card_rsps_string(CHECK_OPT *pStCheckOpt, uint8_t *p_buffer) {
  if (pStCheckOpt->check_state_manchine.state == WAIT_RCV_SET_SIMSTAT_RSPS) {
    if (strstr(p_buffer, "OK") != NULL) {
      strcpy(pStCheckOpt->check_state_manchine.rsps_buffer, p_buffer);
      pStCheckOpt->check_state_manchine.state = RCV_RSPS_SET_SIMSTAT_SUCCEED;
      fprintf(USART1_STREAM, "---rcv_rsps:%s\r\n", pStCheckOpt->check_state_manchine.rsps_buffer);
    }
  }

  if (pStCheckOpt->check_state_manchine.state == WAIT_RCV_GET_SIMSTAT_REPORT_RSPS) {
    if (strstr(p_buffer, "+QSIMSTAT:") != NULL) {
      strcpy(g_st_4g_sim_card_detection.check_state_manchine.rsps_buffer, p_buffer);
      pStCheckOpt->check_state_manchine.state = RCV_RSPS_GET_SIMSTAT_REPORT_SUCCEED;
      fprintf(USART1_STREAM, "---rcv_rsps:%s\r\n", pStCheckOpt->check_state_manchine.rsps_buffer);
    }
  }
}
#define TIME_OUT_CNT		2000
//4g的ping检测状态机
//参数：串口     main循环中调用的interval
static void check_4g_ping_state_machine(STREAM* USART_Print, uint32_t time_interval) {
  if (g_st_4g_net_detection.check_flag == FALSE)
    return;
  g_st_4g_net_detection.check_state_manchine.timer += time_interval;
  if (g_st_4g_net_detection.check_state_manchine.state == WAIT_RCV_PING_RSPS) {
    if ((g_st_4g_net_detection.check_state_manchine.timer / time_interval) >= TIME_OUT_CNT) {
      fprintf(USART_Print, "4G_PING:-2\r\n");
      g_st_4g_net_detection.check_state_manchine.state = RCV_RSPS_PING_FINISH;
    }
  } else if (g_st_4g_net_detection.check_state_manchine.state == RCV_RSPS_PING_SUCCEED) {
    if (strstr(g_st_4g_net_detection.check_state_manchine.rsps_buffer, "+QPING:")) {
      char *str1, *str2, cPingTime[10];
      int res = FALSE;
      if ((str1 = FindIndexPosStrng(g_st_4g_net_detection.check_state_manchine.rsps_buffer, ',', 3))
          != NULL) {
        str1 += 1;
        if ((str2 = FindIndexPosStrng(g_st_4g_net_detection.check_state_manchine.rsps_buffer, ',',
            4)) != NULL) {
          memset(cPingTime, 0, sizeof(cPingTime));
          memcpy(cPingTime, str1, str2 - str1);
          if (strlen(cPingTime) > 0) {
            res = TRUE;
            fprintf(USART_Print, "4G_PING:%s\r\n", cPingTime);
            fprintf(USART1_STREAM, "####################4G_PING:%s\r\n", cPingTime);

          }
        }
      }
      if (res == FALSE)
        fprintf(USART_Print, "4G_PING:-1\r\n");
      g_st_4g_net_detection.check_state_manchine.state = RCV_RSPS_PING_FINISH;
    } else if (strstr(g_st_4g_net_detection.check_state_manchine.rsps_buffer, "ERROR")) {
      g_st_4g_net_detection.check_state_manchine.state = RCV_RSPS_PING_FINISH;
      fprintf(USART_Print, "4G_PING:-1\r\n");
    } else {
      if ((g_st_4g_net_detection.check_state_manchine.timer / time_interval) >= TIME_OUT_CNT) {
        g_st_4g_net_detection.check_state_manchine.state = RCV_RSPS_PING_FINISH;
        fprintf(USART_Print, "4G_PING:-2\r\n");
      }
    }
  }
  if (g_st_4g_net_detection.check_state_manchine.state == RCV_RSPS_PING_FINISH) {
    memset(&g_st_4g_net_detection, 0, sizeof(g_st_4g_net_detection));
    g_st_4g_net_detection.check_flag = FALSE;
  }
}
//4g的sim卡检测状态机
//参数：串口     main循环中调用的interval
static void check_4g_sim_card_state_machine(STREAM* USART_Print, uint32_t time_interval) {
  if (g_st_4g_sim_card_detection.check_flag == FALSE)
    return;
  g_st_4g_sim_card_detection.check_state_manchine.timer += time_interval;
  if (g_st_4g_sim_card_detection.check_state_manchine.state == WAIT_RCV_SET_SIMSTAT_RSPS) {
    if ((g_st_4g_sim_card_detection.check_state_manchine.timer / time_interval) >= TIME_OUT_CNT) {
      fprintf(USART_Print, "SIMSTAT:-2\r\n");
      g_st_4g_sim_card_detection.check_state_manchine.state = RCV_RSPS_GET_SIMSTAT_REPORT_FINISH;
    }
  } else if (g_st_4g_sim_card_detection.check_state_manchine.state
      == RCV_RSPS_SET_SIMSTAT_SUCCEED) {
    uint8_t buffer[100];

    memset(&g_st_4g_sim_card_detection.check_state_manchine, 0,
        sizeof(g_st_4g_sim_card_detection.check_state_manchine));
    g_st_4g_sim_card_detection.check_state_manchine.state = WAIT_RCV_GET_SIMSTAT_REPORT_RSPS;
    sprintf(buffer, "AT+QSIMSTAT?\r\n");
    fprintf(USART0_STREAM, buffer);
  } else if (g_st_4g_sim_card_detection.check_state_manchine.state
      == WAIT_RCV_GET_SIMSTAT_REPORT_RSPS) {
    if ((g_st_4g_sim_card_detection.check_state_manchine.timer / time_interval) >= TIME_OUT_CNT) {
      fprintf(USART_Print, "SIMSTAT:-2\r\n");
      g_st_4g_sim_card_detection.check_state_manchine.state = RCV_RSPS_GET_SIMSTAT_REPORT_FINISH;
    }
  } else if (g_st_4g_sim_card_detection.check_state_manchine.state
      == RCV_RSPS_GET_SIMSTAT_REPORT_SUCCEED) {
    if (strstr(g_st_4g_sim_card_detection.check_state_manchine.rsps_buffer, "+QSIMSTAT:") != NULL) {
      char *str1, *str2, cSimCardStat[10];
      int res = FALSE;
      if ((str1 = FindIndexPosStrng(g_st_4g_sim_card_detection.check_state_manchine.rsps_buffer,
          ',', 1)) != NULL) {
        str1 += 1;
        if ((str2 = FindIndexPosStrng(g_st_4g_sim_card_detection.check_state_manchine.rsps_buffer,
            '\r', 1)) != NULL) {
          memset(cSimCardStat, 0, sizeof(cSimCardStat));
          memcpy(cSimCardStat, str1, str2 - str1);
          if (strlen(cSimCardStat) > 0) {
            res = TRUE;
            fprintf(USART_Print, "SIMSTAT:%s\r\n", cSimCardStat);
          }
        }
      }
      if (res == FALSE)
        fprintf(USART_Print, "SIMSTAT:-2\r\n");
      g_st_4g_sim_card_detection.check_state_manchine.state = RCV_RSPS_GET_SIMSTAT_REPORT_FINISH;
    } else {
      if ((g_st_4g_sim_card_detection.check_state_manchine.timer / time_interval) >= TIME_OUT_CNT) {
        g_st_4g_sim_card_detection.check_state_manchine.state = RCV_RSPS_GET_SIMSTAT_REPORT_FINISH;
        fprintf(USART_Print, "SIMSTAT:-2\r\n");
      }
    }
  }
  if (g_st_4g_sim_card_detection.check_state_manchine.state == RCV_RSPS_GET_SIMSTAT_REPORT_FINISH) {
    memset(&g_st_4g_sim_card_detection, 0, sizeof(g_st_4g_sim_card_detection));
    g_st_4g_sim_card_detection.check_flag = FALSE;
  }
}
//GPS检测的目标字符串监控
//参数：输入的目标字符串
static void check_gps_info_rsps_string(CHECK_OPT *pStCheckOpt, uint8_t *p_buffer) {
  if (pStCheckOpt->check_state_manchine.state == WAIT_RCV_GPS_OPEN) {
    if ((strstr(p_buffer, "OK") != NULL) || (strstr(p_buffer, "+CMS ERROR: 504") != NULL)) {
      strcpy(pStCheckOpt->check_state_manchine.rsps_buffer, p_buffer);
      pStCheckOpt->check_state_manchine.state = RCV_GPS_OPEN_SUCCEED;
      fprintf(USART1_STREAM, "---rcv_rsps:%s\r\n", pStCheckOpt->check_state_manchine.rsps_buffer);
    }
  }

  if (pStCheckOpt->check_state_manchine.state == WAIT_RCV_SET_QGPSLOC_RSPS) {
    if (strstr(p_buffer, "+QGPSLOC:") != NULL) {
      strcpy(g_st_gps_detection.check_state_manchine.rsps_buffer, p_buffer);
      pStCheckOpt->check_state_manchine.state = RCV_SET_QGPSLOC_SUCCEED;
      fprintf(USART1_STREAM, "---rcv_rsps:%s\r\n", pStCheckOpt->check_state_manchine.rsps_buffer);
    }
  }
}
//GPS检测状态机
//参数：串口     main循环中调用的interval
static void check_gps_state_machine(STREAM* USART_Print, uint32_t time_interval) {
  if (g_st_gps_detection.check_flag == FALSE)
    return;
  g_st_gps_detection.check_state_manchine.timer += time_interval;
  if (g_st_gps_detection.check_state_manchine.state == WAIT_RCV_GPS_OPEN) {
    if ((g_st_gps_detection.check_state_manchine.timer / time_interval) >= TIME_OUT_CNT) {
      get_gps_info(USART_Print, "0", "0", 0);
      g_st_gps_detection.check_state_manchine.state = GET_GPS_DATA_FINISH;
    }
  } else if (g_st_gps_detection.check_state_manchine.state == RCV_GPS_OPEN_SUCCEED) {
    uint8_t buffer[20];
    memset(buffer, 0, sizeof(buffer));

    memset(&g_st_gps_detection.check_state_manchine, 0,
        sizeof(g_st_gps_detection.check_state_manchine));
    g_st_gps_detection.check_state_manchine.state = WAIT_RCV_SET_QGPSLOC_RSPS;
    strcpy(buffer, "AT+QGPSLOC=0\r\n");
    fprintf(USART0_STREAM, buffer);
  } else if (g_st_gps_detection.check_state_manchine.state == WAIT_RCV_SET_QGPSLOC_RSPS) {
    if ((g_st_gps_detection.check_state_manchine.timer / time_interval) >= TIME_OUT_CNT) {
      get_gps_info(USART_Print, "0", "0", 0);
      g_st_gps_detection.check_state_manchine.state = GET_GPS_DATA_FINISH;
    }
  } else if (g_st_gps_detection.check_state_manchine.state == RCV_SET_QGPSLOC_SUCCEED) {
    //	strcpy(g_st_gps_detection.check_state_manchine.rsps_buffer,"+QGPSLOC: 112800.000,3909.9300N,11710.9163E,2.7,-17.4,3,000.00,1.7,0.9,280522,10\r\n");//+QGPSLOC: 070807.000, 3858.1866N, 11713.5805E, 2.0, 2.4, 3, 000.00, 0.7, 0.4, 301021, 07
    if (strstr(g_st_gps_detection.check_state_manchine.rsps_buffer, "+QGPSLOC:") != NULL) {
      char *str1, *str2, clat[12], clon[12], cNum[12], iNum;
      int res = FALSE;
      if (((str1 = FindIndexPosStrng(g_st_gps_detection.check_state_manchine.rsps_buffer, ',', 1))
          != NULL)
          && ((str2 = FindIndexPosStrng(g_st_gps_detection.check_state_manchine.rsps_buffer, ',', 2))
              != NULL)) {
        str1 += 1;
        memset(clat, 0, sizeof(clat));
        memcpy(clat, str1, str2 - str1);
        if (strlen(clat) > 0) {
          str1 = str2 + 1;
          if ((str2 = FindIndexPosStrng(g_st_gps_detection.check_state_manchine.rsps_buffer, ',', 3))
              != NULL) {
            memset(clon, 0, sizeof(clon));
            memcpy(clon, str1, str2 - str1);
            if (strlen(clon) > 0) {
//
              if (((str1 = FindIndexPosStrng(g_st_gps_detection.check_state_manchine.rsps_buffer,
                  ',', 10)) != NULL)
                  && ((str2 = FindIndexPosStrng(g_st_gps_detection.check_state_manchine.rsps_buffer,
                      '\r', 1)) != NULL)) {
                str1 += 1;
                memset(cNum, 0, sizeof(cNum));
                memcpy(cNum, str1, str2 - str1);
                iNum = atoi(cNum);
                res = TRUE;
                get_gps_info(USART_Print, clon, clat, iNum);
              }
            }
          }
        }
      }
      if (res == FALSE)
        get_gps_info(USART_Print, "0", "0", 0);
      g_st_gps_detection.check_state_manchine.state = GET_GPS_DATA_FINISH;
    } else {
      if ((g_st_gps_detection.check_state_manchine.timer / time_interval) >= TIME_OUT_CNT) {
        g_st_gps_detection.check_state_manchine.state = GET_GPS_DATA_FINISH;
        get_gps_info(USART_Print, "0", "0", 0);
      }
    }
  }
  if (g_st_gps_detection.check_state_manchine.state == GET_GPS_DATA_FINISH) {
    memset(&g_st_gps_detection, 0, sizeof(g_st_gps_detection));
    g_st_gps_detection.check_flag = FALSE;
  }
}
static void check_aebs_4g_detection_rsps_string(UART_OBJ *pStUartOptObj) {
  if (pStUartOptObj->st_uart_opt[pStUartOptObj->GetPrt].used_flag == TRUE) {
    //fprintf(USART1_STREAM,"---GutPt:%d---%s\r\n",pStUartOptObj->GetPrt,pStUartOptObj->st_uart_opt[pStUartOptObj->GetPrt].UartRcvBuffer);
    check_4g_ping_rsps_string(&g_st_4g_net_detection,
        pStUartOptObj->st_uart_opt[pStUartOptObj->GetPrt].UartRcvBuffer);
    check_sim_card_rsps_string(&g_st_4g_sim_card_detection,
        pStUartOptObj->st_uart_opt[pStUartOptObj->GetPrt].UartRcvBuffer);
    check_gps_info_rsps_string(&g_st_gps_detection,
        pStUartOptObj->st_uart_opt[pStUartOptObj->GetPrt].UartRcvBuffer);
    pStUartOptObj->st_uart_opt[pStUartOptObj->GetPrt].used_flag = FALSE;
    memset(pStUartOptObj->st_uart_opt[pStUartOptObj->GetPrt].UartRcvBuffer, 0, NEW_UART0_DATA_LEN);
    pStUartOptObj->st_uart_opt[pStUartOptObj->GetPrt].DataLen = 0;
    pStUartOptObj->GetPrt++;
    if (pStUartOptObj->GetPrt >= NEW_UART0_CACHE_NBR)
      pStUartOptObj->GetPrt = 0;
  }
}
//串口0接收缓存队列
void uart_rcv_to_cache(uint8_t data) {
  UartRcvDataToBuffer(data, &g_st_uart0_cache_opt_obj);
}
//检测AEBS在4G检测时需要判断的AT回应
void check_aebs_4g_all_detection_rsps_string() {
  check_aebs_4g_detection_rsps_string(&g_st_uart0_cache_opt_obj);
}
//AEBS安装检测命令处理
uint8_t aebs_detection_in_installation(STREAM* USART_Print, uint8_t *buffer) {
  uint8_t res = 1;
  //gcz 2022-0507
  if (strstr((const char*) buffer, (const char*) "ZKHY*APP_CHECK_CAN>")) {
    detection_can_status(USART_Print);
  } else if (strstr((const char*) buffer, (const char*) "ZKHYCHK*APP_CHECK_INPUT_LEFT>")) {
    detection_gpio_input(USART_Print, GPIO_IN_LEFT);
  } else if (strstr((const char*) buffer, (const char*) "ZKHYCHK*APP_CHECK_INPUT_RIGHT>")) {
    detection_gpio_input(USART_Print, GPIO_IN_RIGHT);
  } else if (strstr((const char*) buffer, (const char*) "ZKHYCHK*APP_CHECK_INPUT_BRAKE>")) {
    detection_gpio_input(USART_Print, GPIO_IN_BRAKE);
  } else if (strstr((const char*) buffer, (const char*) "ZKHYCHK*APP_CHECK_INPUT_BACK>")) {
    detection_gpio_input(USART_Print, GPIO_IN_BACK);
  } else if (strstr((const char*) buffer, (const char*) "ZKHYCHK*APP_CHECK_GPS>")) {
    GPS_INFO gpsInfo = Get_GPS_Info();
    if (gpsInfo.isConnect == TRUE) {
      get_gps_info(USART_Print, gpsInfo.lon, gpsInfo.lat, gpsInfo.sateNum);
    } else
      detection_gps_card();
  } else if (strstr((const char*) buffer, (const char*) "ZKHYCHK*APP_CHECK_4G_PING>")) {
    ping_web_for_detection_4g(USART_Print, "www.baidu.com");
  } else if (strstr((const char*) buffer, (const char*) "ZKHYCHK*APP_CHECK_SIMSTAT>")) {
    detection_sim_card();
  } else
    res = 0;
  return res;
}
//安装检测4G的状态机
void install_detection_4G_ctrl_machine() {
  STREAM *USART_Print = NULL;
  static uint32_t previousMillis = 0;
  switch (g_u8UartFlag) {
  case UART_4_ID:
    USART_Print = USART4_STREAM;
    break; // BT
  default:
    USART_Print = USART1_STREAM;
    break;
  }
  if ((SystemtimeClock - previousMillis) >= 1) {
    previousMillis = SystemtimeClock;
    //gcz  2022-05-08-------------------------------------
    check_4g_ping_state_machine(USART_Print, 1); // 安装时 4g ping检测状态机
    check_4g_sim_card_state_machine(USART_Print, 1); // 安装时 4g SIM卡检测状态机
    check_gps_state_machine(USART_Print, 1); // 安装时 GPS检测状态机
    //---------------------------------------------------------------------------
  }
}
//获取超声波雷达状态信息
static int get_ult_radar_info(uint8_t index) {
  int res;
  if (detection_Urader_Company_Message[0].Urader_work_stata[index] == ACCESSIBILITY) //未检测到障碍物
    res = -3;
  else if (detection_Urader_Company_Message[0].Urader_work_stata[index] == NOURADER) //未插入雷达
    res = -2;
  else if (detection_Urader_Company_Message[0].Urader_work_stata[index] == NOWORK) //雷达不需要工作
    res = -1;
  else
    res = 0;
  return res;
}
static uint8_t g_ult_frame_comm_status[2] = { OFFLINE, OFFLINE };
//超声波雷达通讯帧700和701检测
static uint8_t check_ult_comm_single_frame_status(uint8_t frame_id, uint32_t ctrl_times) {
  uint8_t res = OFFLINE;
  if (g_check_ult_radar_exist_hearbeat_cnt[frame_id] >= 1) {
    g_check_ult_radar_exist_hearbeat_cnt[frame_id]++; //每次CAN中断接收到会复位1，当通讯异常断帧，此处每次都会自增，自增到目标次数，即可标记异常状态
    if (g_check_ult_radar_exist_hearbeat_cnt[frame_id] >= ctrl_times) {
      g_check_ult_radar_exist_hearbeat_cnt[frame_id] = 0;
    } else
      res = ONLINE;
  }

  return res;
}
//本函数需要放到ms,s等时间可控处，该函数需要定时计数
//参数：需要计数的总次数
void check_ult_comm_frame_status(uint32_t ctrl_times) {
  g_ult_frame_comm_status[ID_700] = check_ult_comm_single_frame_status(ID_700, ctrl_times);
  g_ult_frame_comm_status[ID_701] = check_ult_comm_single_frame_status(ID_701, ctrl_times);
}
//复位检测结构数据
static void reset_detection_struct_data() {
  if (g_ult_frame_comm_status[ID_700] == OFFLINE) {
    int i1;
    for (i1 = 0; i1 < 4; i1++) {
      detection_Urader_Company_Message[0].Urader_work_stata[i1] = NOURADER;
      detection_Urader_Company_Message[0].distance[i1] = 0;
    }
  }

  if (g_ult_frame_comm_status[ID_701] == OFFLINE) {
    int i1;
    for (i1 = 4; i1 < 12; i1++) {
      detection_Urader_Company_Message[0].Urader_work_stata[i1] = NOURADER;
      detection_Urader_Company_Message[0].distance[i1] = 0;
    }
  }
}
//gcz 2022-05-13----------------------------------------
#if 1
//该方式使用需要定义超过200字节，若定义局部变量可能超过栈设置的默认大小，若使用需使用全局变量，比较浪费，因此，暂不使用该方式
//生成全部超声波雷达检测结果数据流
static void generate_ult_radra_detection_result(uint8_t *p_out_buffer, uint16_t buffer_length) {
  int i1, res;
  uint8_t buffer[30];
  memset(p_out_buffer, 0, buffer_length);
  for (i1 = 0; i1 < 12; i1++) {
    if ((res = get_ult_radar_info(i1)) != 0) {
      sprintf(buffer, "RT_UR_DIS_%d:%d\r\n", i1 + 1, res);
    } else {
      sprintf(buffer, "RT_UR_DIS_%d:%0.2f\r\n", i1 + 1,
          detection_Urader_Company_Message[0].distance[i1]);
    }
    strcat(p_out_buffer, buffer);
  }
}
#endif
//生成单路超声波雷达检测结果数据流
static void generate_single_ult_radra_detection_result(uint8_t index, uint8_t *p_out_buffer,
    uint16_t buffer_length) {
  int res;
  memset(p_out_buffer, 0, buffer_length);
  if ((res = get_ult_radar_info(index)) != 0) {
    sprintf(p_out_buffer, "RT_UR_DIS_%d:%d\r\n", index + 1, res);
  } else {
    sprintf(p_out_buffer, "RT_UR_DIS_%d:%0.2f\r\n", index + 1,
        detection_Urader_Company_Message[0].distance[index]);
  }
}
//发送超声波雷达信息
//参数：串口类型
static void send_ult_radar_info(STREAM* USART_Print)
{
	uint8_t i1;
	uint8_t arry_out_buffer[30];
	reset_detection_struct_data();
	for (i1 = 0; i1 < 12; i1++)
	{
		generate_single_ult_radra_detection_result(i1, arry_out_buffer, 30);
		fprintf(USART_Print, arry_out_buffer);
	}
}
//gcz 2022-05-13
//实时显示超声波雷达数据
void show_rt_ult_radar_info()
{
	if (g_st_detection_ult_radra_para.flag == 1)
	{
		check_ult_comm_frame_status(1000);
		//y由于uint32_t interval = 1; 并且if(SystemtimeClock - previousMillis >= interval)，因此本函数理论上期望1ms执行一次
		//我们目的是500ms输出一次
		static uint32_t currentTime = 0;
		if (g_st_detection_ult_radra_para.cnt == 0)
			currentTime = SystemtimeClock;

		if ((SystemtimeClock - currentTime) < 500)
		{
			g_st_detection_ult_radra_para.cnt++;
		}
		else
		{
			g_st_detection_ult_radra_para.cnt = 0;
			if (g_u8_check_ult_radra_cmd_src == CMD_SRC_DEBUG)
			{
				g_iShow_RT_ULT_RADAR[0] = TRUE;
				g_u8_check_ult_radra_cmd_src = CMD_END;
			}
			else if (g_u8_check_ult_radra_cmd_src == CMD_SRC_BT)
			{
				g_iShow_RT_ULT_RADAR[1] = TRUE;
				g_u8_check_ult_radra_cmd_src = CMD_END;
			}
			if (g_iShow_RT_ULT_RADAR[0] == TRUE)
				send_ult_radar_info(USART1_STREAM);
			if (g_iShow_RT_ULT_RADAR[1] == TRUE)
				send_ult_radar_info(USART4_STREAM);
		}
	}
}
/*
 * AT参数配置，在Main函数处理逻辑
 * 扫描间隔：120ms
 */
extern bool g_b_srr_log_key;
extern bool srr_demo_sw;
uint32_t at_clk = 0;
void Analysis_AT_CMD_InMainLoop() {
  if (SystemtimeClock - at_clk < 120)
    return;

	at_clk = SystemtimeClock;
	if(strlen(g_st_uart_opt.usart1_At_Buf) > 10)
    {	// valid data
    	// use USART1/BT Print info
    	STREAM *USART_Print = NULL;
    	switch(algConfigway)
    	{
    	case 1:USART_Print = USART4_STREAM;break;	// BT
    	default:USART_Print = USART1_STREAM;break;
    	}
		algConfigway = 0;
    	if(strstr((const char*)g_st_uart_opt.usart1_At_Buf, (const char*)"ZKHYCHK*RALGPARA>")){
    		Print_AEB_ALG_Para(USART_Print);
    	}
    	else if(strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"ZKHYCHK*BTAPPGETP>")){
    		Print_AEB_ALG_Para_For_BT_Show(USART_Print);
    	}
    	else if(strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"ZKHYCHK*BTAPPGETV>")){
    		Print_AEBS_Versoin_Info_For_BT_Show(USART_Print);
    	}
    	else if(strstr((const char*)g_st_uart_opt.usart1_At_Buf, (const char*)"ZKHYCHK*SENSORPARA>")){

    	//	fprintf(USART1_STREAM, "1111111111111\r\n");
    		Print_Sensor_Cfg_Param();
    	}
    	else if(strstr((const char*)g_st_uart_opt.usart1_At_Buf, (const char*)"Gsensor_C6_Cmd")){//lyj Gsensor cmd测试
    		Request_CARDVR_Set_Gsensor_C6_ASCII();
    	}
    	else if(strstr((const char*)g_st_uart_opt.usart1_At_Buf, (const char*)"SRR_DEMO_ON")){//lyj 角雷达行人横穿demo开
    		Sensor_MGT m_sensor_mgt = {0};
    		srr_demo_sw = true;

			if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
				Set_AT_Error_Info("Get srr_demo_sw Failed.");
				return;
			}
			m_sensor_mgt.veh_srr_demo_sw = srr_demo_sw;

			// 写配置参数
			if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
				Set_AT_Error_Info("Set srr_demo_sw Failed.");
				return;
			}

			gcz_serial_v1_dma_printf(SERIAL_UART1,"SET SRR_DEMO_ON OK\r\n");

    	}
    	else if(strstr((const char*)g_st_uart_opt.usart1_At_Buf, (const char*)"SRR_DEMO_OFF")){//lyj 角雷达行人横穿demo关
    		Sensor_MGT m_sensor_mgt = {0};
    		srr_demo_sw = false;

			if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
				Set_AT_Error_Info("Get srr_demo_sw Failed.");
				return;
			}
			m_sensor_mgt.veh_srr_demo_sw = srr_demo_sw;

			// 写配置参数
			if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
				Set_AT_Error_Info("Set srr_demo_sw Failed.");
				return;
			}

			gcz_serial_v1_dma_printf(SERIAL_UART1,"SET SRR_DEMO_OFF OK\r\n");

    	}
		else if (strstr((const char*) g_st_uart_opt.usart1_At_Buf, (const char*) "TEST_FUNC")) //gcz 状态数据测试
		{
			test_func();
		}
		else if (strstr((const char*) g_st_uart_opt.usart1_At_Buf, (const char*) "SRR_LOG_OPEN")) //gcz 角雷达日志开
		{
			g_b_srr_log_key = true;
			gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR LOG OPEN OK\r\n");
		}
		else if (strstr((const char*) g_st_uart_opt.usart1_At_Buf, (const char*) "SRR_LOG_CLOSE")) //gcz 角雷达日志关
		{
			g_b_srr_log_key = false;
			gcz_serial_v1_dma_printf(SERIAL_UART1,"SRR LOG CLOSE OK\r\n");
		}
#if 0		// 20220905 lmz note
//		else if (strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"OPN_SHW_CAN_SP"))//gcz 开启MM显示
//		{
//			g_b_can_sp_show_flag = true;
//			USART1_Bebug_Print("OPEN SHOW","CAN SPEED OK",1);
//		}
//		else if (strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"CLS_SHW_CAN_SP"))//gcz 关闭MM显示
//		{
//			g_b_can_sp_show_flag = false;
//			USART1_Bebug_Print("CLOSE SHOW","CAN SPEED OK",1);
//		}
//		else if (strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"OPN_SHW_PWM_SP"))//gcz 开启MM显示
//		{
//			g_b_pwm_sp_show_flag = true;
//			USART1_Bebug_Print("OPEN SHOW","PWM SPEED OK",1);
//		}
//		else if (strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"CLS_SHW_PWM_SP"))//gcz 关闭MM显示
//		{
//			g_b_pwm_sp_show_flag = false;
//			USART1_Bebug_Print("CLOSE SHOW","PWM SPEED OK",1);
//		}
//		else if (strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"WT_UART_IMU_OPEN"))//gcz 状态数据测试
//		{
//			g_b_imu_show = true;
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"WT UART IMU OPEN OK\r\n");
//		}
//		else if (strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"WT_UART_IMU_CLOSE"))//gcz 状态数据测试
//		{
//			g_b_imu_show = false;
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"WT UART IMU CLOSE OK\r\n");
//		}
//		else if (strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"CAL_SP_SHOW_OPEN"))//gcz 状态数据测试
//		{
//			g_b_cal_sp_show = true;
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"CAL SP SHOW OPEN OK\r\n");
//		}
//		else if (strstr((const char*)g_st_uart_opt.usart1_At_Buf,(const char*)"CAL_SP_SHOW_CLOSE"))//gcz 状态数据测试
//		{
//			g_b_cal_sp_show = false;
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"CAL SP SHOW CLOSE OK\r\n");
//		}
#endif
        // clear buffer
    	memset(g_st_uart_opt.usart1_At_Buf, 0, ALG_PARSE_BUFFER_SIZE);
	}
//	is_current_service_ = false;

}

/*
 * 外接蓝牙数据解析
 */
static uint8_t btBuf[ALG_PARSE_BUFFER_SIZE] = { 0 };
static uint8_t btCnt = 0;

void HC02_BT_Analysis_Alg_Para(uint8_t data) {

  char *strx = NULL;

  if (btCnt >= ALG_PARSE_BUFFER_SIZE)
    btCnt = 0;

  btBuf[btCnt] = data;
	//---------------------gcz 2022-05-27------------------		勿动！！！！！-
	User_Rxbuffer_BT[User_Rxcount_BT] = data;
	User_Rxcount_BT++;
	if(User_Rxcount_BT >= 256) User_Rxcount_BT = 0;
  //fprintf(USART1_STREAM,"%c",User_Rxbuffer_BT[User_Rxcount_BT]);
  //fprintf(USART1_STREAM,"[%d]%s",User_Rxcount_BT,User_Rxbuffer_BT);

//  fprintf(USART1_STREAM,"[%c]",data);
  //-------------------------------------------------------

	if(btBuf[btCnt-1]==',' && (btBuf[btCnt]=='0'|| btBuf[btCnt]=='1') || btBuf[btCnt]=='>')
	{//
//		fprintf(USART1_STREAM,"+");
		if((strx = strstr(btBuf,"AT+"))//gcz 2022-08-12  勿删！！！会影响蓝牙通讯
			||(strx = strstr(btBuf,"ZKHYCHK*RALGPARA"))
			|| (strx = strstr(btBuf,"ZKHYCHK*BTAPPGETP"))|| (strx = strstr(btBuf,"ZKHYCHK*BTAPPGETV")))
		{	// 分出AT指令

//			is_current_service_ = true;
			algConfigway = 1;
			g_st_uart_opt.usart_used_id = UART_4_ID;
			sprintf(g_st_uart_opt.usart1_At_Buf, "%s\r\n", strx);
//			USART1_Bebug_Print("AT", g_st_uart_opt.usart1_At_Buf, 1);

			memset(btBuf, 0, sizeof(btBuf));
			btCnt = 0;
			//gcz 2022-08-12		勿动！！！！！
			g_DetectionCfgWay = 1;
			if (g_st_detection_cal_sp_para.flag == 1)
				g_u8_instlltion_detection_cmd_src = CMD_SRC_BT;

			if (g_st_detection_ult_radra_para.flag == 1)
				g_u8_check_ult_radra_cmd_src = CMD_SRC_BT;

	//		fprintf(USART1_STREAM, "-----------src:%d\r\n", g_u8_instlltion_detection_cmd_src);
		}
	}
	else
	{
		btCnt++;
	}
}

/*
 * 控制电磁阀，来控制申工3V310-10
 * 控制时间最长为50ms;
 * 控制次数最多5次/秒
 * 该函数在1秒内对应动作
 */
uint8_t valveCnt = 0;
uint8_t Dynamic_Control_Megnetic_Valve() {
  if (megneticVal.swt) {
    if ((SystemtimeClock - megneticVal.clk) < megneticVal.enableTime) { // 控制1秒
      Set_Magnetic_Valve(Bit_SET); // 使能
    } else {
      if (((SystemtimeClock - megneticVal.clk) >= megneticVal.enableTime)
          && ((SystemtimeClock - megneticVal.clk)
              < (megneticVal.enableTime + megneticVal.disableTime))) {
        Set_Magnetic_Valve(Bit_RESET);
      } else {
        megneticVal.clk = SystemtimeClock;
      }
    }
  } else {
    Set_Magnetic_Valve(Bit_RESET);
  }
}

