/******************** (C) COPYRIGHT  源地工作室 ********************************
 * 文件名  ：w25qxx.c
 * 描述    ：完成对W25XX的flash完成读写操作
 * 作者    ：lmz
 * 版本更新: 2021-08-30
 * 硬件连接  :PC12-/CS   PC9--CLK   PC10--MISO  PC11--MOSI
 * 调试方式：KungFu KF32DP2
**********************************************************************************/
#include "w25qxx.h"
#include "upgrade_common.h"
#include "spi.h"
#include "usart.h"
#include "stdlib.h"
// #include "malloc.h"

uint16_t W25QXX_TYPE 					= W25Q16;	// 默认是W25X40B
uint8_t w25_rw_data[W25_RW_DATA_SIZE] 	= {0};		// 读写ALG配置参数时，使用固定大小

/* -----------------------局部函数声明------------------------------- */
uint16_t 	W25QXX_ReadID(void);  	    			//读取FLASH ID
AppVersion 	Split_APP_Version(uint8_t *version);

void delay_us( uint32_t nCount);
void delay_us( uint32_t nCount)
{
	nCount=24*nCount;
	while(nCount--)
	{
	}
}

//=============================================================================
//函数名称:W25QXX_Init
//功能概要:初始化SPI FLASH的IO(/CS)口
//参数名称:无
//函数返回:无
//=============================================================================
void W25QXX_Init(void)
{
	GPIO_SPI2();
	SPI_FLASH_CS_HIGH(); 						//SPI FLASH不选中
	SPI2_Init();		   						//初始化SPI
//	W25QXX_TYPE = W25QXX_ReadID();				//读取FLASH ID.
//
//	switch(W25QXX_TYPE)							// print debug
//	{
//	case W25X40B:fprintf(USART1_STREAM,"W25X40B\n");break;
//	case W25Q80:fprintf(USART1_STREAM,"W25Q80\n");break;
//	case W25Q16:fprintf(USART1_STREAM,"W25Q16\n");break;
//	case W25Q32:fprintf(USART1_STREAM,"W25Q32\n");break;
//	case W25Q64:fprintf(USART1_STREAM,"W25Q64\n");break;
//	case W25Q128:fprintf(USART1_STREAM,"W25Q128\n");break;
//	default:fprintf(USART1_STREAM,"external flash not find type.\n");break;
//	}
}

//=============================================================================
//函数名称:W25QXX_ReadSR
//功能概要:读取W25QXX的状态寄存器
//参数名称:无
//函数返回:无
//说明：
//BIT7  6   5   4   3   2   1   0
//SPR   RV  TB BP2 BP1 BP0 WEL BUSY
//SPR:默认0,状态寄存器保护位,配合WP使用
//TB,BP2,BP1,BP0:FLASH区域写保护设置
//WEL:写使能锁定
//BUSY:忙标记位(1,忙;0,空闲)
//默认:0x00
//=============================================================================
uint8_t W25QXX_ReadSR(void)
{
	uint8_t byte=0;
	SPI_FLASH_CS_LOW();							//使能器件

	SPI2_ReadWriteByte(W25X_ReadStatusReg);    	//发送读取状态寄存器命令0x05
	byte=SPI2_ReadWriteByte(0Xff);             	//读取一个字节

	SPI_FLASH_CS_HIGH();						//取消片选
	return byte;
}
//=============================================================================
//函数名称:W25QXX_Write_SR
//功能概要:写W25QXX状态寄存器
//参数名称:sr 状态寄存器
//函数返回:无
//说明：     只有SPR,TB,BP2,BP1,BP0(bit 7,5,4,3,2)可以写!!!
//=============================================================================
//void W25QXX_Write_SR(uint8_t sr)
//{
//	SPI_FLASH_CS_LOW();                         //使能器件
//	SPI2_ReadWriteByte(W25X_WriteStatusReg);   	//发送写取状态寄存器命令0x01
//	SPI2_ReadWriteByte(sr);               		//写入一个字节
//	SPI_FLASH_CS_HIGH();                        //取消片选
//}
//=============================================================================
//函数名称:W25QXX_Write_Enable
//功能概要:W25QXX写使能
//参数名称:无
//函数返回:无
//说明：     将WEL置位
//=============================================================================
void W25QXX_Write_Enable(void)
{
	SPI_FLASH_CS_LOW();                        	//使能器件
    SPI2_ReadWriteByte(W25X_WriteEnable);      	//发送写使能0x06
	SPI_FLASH_CS_HIGH();                     	//取消片选
}
//=============================================================================
//函数名称:W25QXX_Write_Disable
//功能概要:W25QXX写禁止
//参数名称:无
//函数返回:无
//说明：     将WEL清零
//=============================================================================
void W25QXX_Write_Disable(void)
{
	SPI_FLASH_CS_LOW();                     	//使能器件
    SPI2_ReadWriteByte(W25X_WriteDisable);     	//发送写禁止指令0x04
	SPI_FLASH_CS_HIGH();                   		//取消片选
}
//=============================================================================
//函数名称:W25QXX_ReadID
//功能概要:读取芯片ID
//参数名称:无
//函数返回:无
//=============================================================================
uint16_t W25QXX_ReadID(void)
{
	uint16_t Temp = 0;
	SPI_FLASH_CS_LOW();

	SPI2_ReadWriteByte(W25X_ManufactDeviceID);	//发送读取ID命令
	SPI2_ReadWriteByte(0xFF);					//Dummy
	SPI2_ReadWriteByte(0xFF);
	SPI2_ReadWriteByte(0x00);
	Temp|=SPI2_ReadWriteByte(0xFF)<<8;
	Temp|=SPI2_ReadWriteByte(0xFF);

	SPI_FLASH_CS_HIGH();
	return Temp;
}
//=============================================================================
//函数名称:W25QXX_Read
//功能概要:读取SPI FLASH，在指定地址开始读取指定长度的数据
//参数名称:
//pBuffer:数据存储区
//ReadAddr:开始读取的地址(24bit)
//NumByteToRead:要读取的字节数(最大65535)
//函数返回:无
//=============================================================================
void W25QXX_Read(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
 	uint16_t i;
	SPI_FLASH_CS_LOW();                    		//使能器件

    SPI2_ReadWriteByte(W25X_ReadData);         	//发送读取命令0x03
    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF0000)>>16));	//发送24bit地址
    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF00)>>8));
    SPI2_ReadWriteByte((uint8_t)(ReadAddr&0xFF));
    for(i=0;i<NumByteToRead;i++)
	{
        pBuffer[i] = SPI2_ReadWriteByte(0XFF);   	//循环读数
    }
	SPI_FLASH_CS_HIGH();

    // 去掉无用的
	uint16_t j = 0;
	for(j=0; j<NumByteToRead; j++){
		if(pBuffer[j] == 0xFF){	// 去除0XFF空字符
			break;
		}else{
			j++;
		}
	}
	pBuffer[j] = '\0';
//	fprintf(USART1_STREAM, "2read:%s\r\n", pBuffer);
}

void W25QXX_Read_No_Chk(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
 	uint16_t i;
	SPI_FLASH_CS_LOW();                    		//使能器件

    SPI2_ReadWriteByte(W25X_ReadData);         	//发送读取命令0x03
    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF0000)>>16));	//发送24bit地址
    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF00)>>8));
    SPI2_ReadWriteByte((uint8_t)(ReadAddr&0xFF));
    for(i=0;i<NumByteToRead;i++)
	{
        pBuffer[i] = SPI2_ReadWriteByte(0XFF);   	//循环读数
    }
	SPI_FLASH_CS_HIGH();
}
/*
 * 加了校验的读取FLASH数据
 * 仅是针对普通的参数配置
 */
bool W25QXX_Read_Chk(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
	SPI_FLASH_CS_LOW();                    		//使能器件

    SPI2_ReadWriteByte(W25X_ReadData);         	//发送读取命令0x03
    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF0000)>>16));	//发送24bit地址
    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF00)>>8));
    SPI2_ReadWriteByte((uint8_t)(ReadAddr&0xFF));
    uint16_t i=0;
    for(; i<NumByteToRead; i++)
	{
        pBuffer[i]=SPI2_ReadWriteByte(0XFF);   	//循环读数
    }
	SPI_FLASH_CS_HIGH();

	// 测试打印
//	fprintf(USART1_STREAM, "\r\n------------read----1-------\r\n");
//	for(uint16_t j=0; j<NumByteToRead; j++){
//		fprintf(USART1_STREAM, "%02X ", pBuffer[j]);
//	}
//	fprintf(USART1_STREAM, "\r\n------------read----1-------\r\n");
    // 去掉无用的,计算结束符号
	uint16_t end = 0;
	for(end=0; end<NumByteToRead; end++){
		if(pBuffer[end]==0xFF && pBuffer[end+1]==0xFF && pBuffer[end+2]==0xFF && pBuffer[end+3]==0xFF){	// 去除0XFF空字符
			break;
		}
	}
	// 若被清空扇区后
	if(end == 0) {
//		fprintf(USART1_STREAM, "\r\nEase Flash:%s\r\n", pBuffer);
		memset(pBuffer, 0, NumByteToRead);
		return false;
	}

	// 测试打印
//	fprintf(USART1_STREAM, "\r\n------------read----2-------\r\n");
//	for(uint16_t j=0; j<end; j++){
//		fprintf(USART1_STREAM, "%02X ", pBuffer[j]);
//	}
//	fprintf(USART1_STREAM, "\r\n------------read----2-------\r\n");

	// 获取校验字节
	uint8_t src_Chk = pBuffer[end-2];

	// 获取校验值
	uint8_t chk_sum = 0;
	for(uint16_t j=0; j<end-2; j++){
		chk_sum += pBuffer[j];
	}
	if(chk_sum == 0xFF) chk_sum = 0xFE;

//	fprintf(USART1_STREAM, "\r\nchk:%02X, src:%02X\r\n", chk_sum, src_Chk);
	// 校验
	if(chk_sum == src_Chk){
		pBuffer[end-2] = '\0';
//		fprintf(USART1_STREAM, "read chk success\r\n\r\n");
		return true;
	}

	fprintf(USART1_STREAM, "\r\n------------read----E-------\r\n");
	for(uint16_t j=0; j<NumByteToRead; j++){
		fprintf(USART1_STREAM, "%02X ", pBuffer[j]);
	}
	fprintf(USART1_STREAM, "\r\n------------read----E-------\r\n");

	memset(pBuffer, 0, NumByteToRead);
//	fprintf(USART1_STREAM, "read chk faild.\r\n\r\n");
	return false;
}

bool W25QXX_Read_Chk2(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
  SPI_FLASH_CS_LOW();                       //使能器件

  SPI2_ReadWriteByte(W25X_ReadData);          //发送读取命令0x03
  SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF0000)>>16)); //发送24bit地址
  SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF00)>>8));
  SPI2_ReadWriteByte((uint8_t)(ReadAddr&0xFF));
  uint16_t i=0;
  for(; i<NumByteToRead; i++)
  {
      pBuffer[i]=SPI2_ReadWriteByte(0XFF);    //循环读数
  }

  int src_Chk = SPI2_ReadWriteByte(0XFF);

  SPI_FLASH_CS_HIGH();

  // 获取校验值
  uint8_t chk_sum = 0;
  for(uint16_t j=0; j < NumByteToRead; j++){
    chk_sum += pBuffer[j];
  }
  if(chk_sum == 0xFF) chk_sum = 0xFE;

//  fprintf(USART1_STREAM, "\r\nchk:%02X, src:%02X\r\n", chk_sum, src_Chk);
  // 校验
  if(chk_sum == src_Chk){
    return true;
  }

//  fprintf(USART1_STREAM, "\r\n------------read----E-------\r\n");
//  for(uint16_t j=0; j<NumByteToRead; j++){
//    fprintf(USART1_STREAM, "%02X ", pBuffer[j]);
//  }
//
//  fprintf(USART1_STREAM, "chk_sum : %02X src_Chk :%02X", chk_sum, src_Chk);
//  fprintf(USART1_STREAM, "\r\n------------read----E-------\r\n");

  memset(pBuffer, 0, NumByteToRead);
//  fprintf(USART1_STREAM, "read chk faild.\r\n\r\n");
  return false;
}


//void W25QXX_FAST_Read(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t NumByteToRead)
//{
// 	uint16_t i;
//	SPI_FLASH_CS_LOW();                         //使能器件
//
//    SPI2_ReadWriteByte(W25X_FastReadData);    	//发送读取命令0x0B
//    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF0000)>>16));	//发送24bit地址
//    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF00)>>8));
//    SPI2_ReadWriteByte((uint8_t)(ReadAddr&0xFF));
//    SPI2_ReadWriteByte(0xFF);
//    for(i=0;i<NumByteToRead;i++)
//	{
//        pBuffer[i]=SPI2_ReadWriteByte(0XFF);   //循环读数
//    }
//    pBuffer[i]='\0';
//
//	SPI_FLASH_CS_HIGH();
//}
//=============================================================================
//函数名称:W25QXX_Write_Page
//功能概要:在指定地址开始写入最大256字节的数据，SPI在一页(0~65535)内写入
//         少于256个字节的数据
//参数名称:
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大256),该数不应该超过该页的剩余字节数!!!
//函数返回:无
//=============================================================================
void W25QXX_Write_Page(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite)
{
 	uint16_t i;
    W25QXX_Write_Enable();                  	//SET WEL
	SPI_FLASH_CS_LOW();//                 		//使能器件

    SPI2_ReadWriteByte(W25X_PageProgram);      	//发送写页命令0x02
    SPI2_ReadWriteByte((uint8_t)((WriteAddr)>>16)); //发送24bit地址
    SPI2_ReadWriteByte((uint8_t)((WriteAddr)>>8));
    SPI2_ReadWriteByte((uint8_t)WriteAddr);

    for(i=0;i<NumByteToWrite;i++)
    	SPI2_ReadWriteByte(pBuffer[i]);			//循环写数

	SPI_FLASH_CS_HIGH();                		//取消片选
	W25QXX_Wait_Busy();					   		//等待写入结束
//	delay_us(1);
}
//=============================================================================
//函数名称:W25QXX_Write_NoCheck
//功能概要:无检验写SPI FLASH ,在指定地址开始写入指定长度的数据,但是要确保地址不越界!
//参数名称:
//					pBuffer:数据存储区
//					WriteAddr:开始写入的地址(24bit)
//					NumByteToWrite:要写入的字节数(最大65535)
//函数返回:无
//说明：  必须确保所写的地址范围内的数据全部为0XFF,否则在非0XFF处写入的数据将失败!
//        具有自动换页功能
//=============================================================================
void W25QXX_Write_NoCheck(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
	if(NumByteToWrite == 0) return ;

	uint16_t pageremain = EX_FLASH_PAGE_SIZE - WriteAddr % EX_FLASH_PAGE_SIZE; //单页剩余的字节数

	if(NumByteToWrite <= pageremain)
		pageremain = NumByteToWrite;			//不大于256个字节

	while(1)
	{
		W25QXX_Write_Page(pBuffer, WriteAddr, pageremain);
		if(NumByteToWrite == pageremain)break;	//写入结束了
	 	else //NumByteToWrite>pageremain
		{
			pBuffer			+= pageremain;
			WriteAddr		+= pageremain;

			NumByteToWrite	-= pageremain;		//减去已经写入了的字节数
			if(NumByteToWrite > EX_FLASH_PAGE_SIZE)
				pageremain 	= EX_FLASH_PAGE_SIZE; 		//一次可以写入256个字节
			else pageremain = NumByteToWrite; 	//不够256个字节了
		}
	}
}
//=============================================================================
//函数名称:W25QXX_Write
//功能概要:写SPI FLASH  在指定地址开始写入指定长度的数据
//参数名称:
// 最多能写写4096个字节，一个扇区的大小
//					pBuffer:数据存储区
//					WriteAddr:开始写入的地址(24bit)
//					NumByteToWrite:要写入的字节数(最大65535)
//函数返回:无
//=============================================================================
uint8_t W25QXX_BUF[EX_FLASH_SECTOR_SIZE] = {0};
void W25QXX_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite)
{
 	uint16_t i 		= 0;
	uint32_t pos	= WriteAddr / EX_FLASH_SECTOR_SIZE;		//扇区地址
	uint16_t off	= WriteAddr % EX_FLASH_SECTOR_SIZE;		//在扇区内的偏移
//	fprintf(USART1_STREAM,"pos:%d,off:%d,num:%d.\r\n",pos,off,NumByteToWrite);
	memset(W25QXX_BUF,0,sizeof(W25QXX_BUF));

	W25QXX_Read(W25QXX_BUF, pos*EX_FLASH_SECTOR_SIZE, EX_FLASH_SECTOR_SIZE);//读出整个扇区的内容
	for(i=0; i<NumByteToWrite; i++)	   		//复制
	{
		W25QXX_BUF[off+i] = pBuffer[i];
	}
//	fprintf(USART1_STREAM,"-->%s<--\r\n",W25QXX_BUF);
	/* 写入整个扇区 */
	W25QXX_Erase_Sector(pos);			//擦除这个扇区
	W25QXX_Write_NoCheck(W25QXX_BUF, pos*EX_FLASH_SECTOR_SIZE, EX_FLASH_SECTOR_SIZE);	// EX_FLASH_SECTOR_SIZE  strlen(W25QXX_BUF)
}
/*
 * 添加了校验的写FLASH
 */
bool W25QXX_Write_Chk(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
	// 去除字符串中多余的字符
//	uint8_t *buf_w = (uint8_t *)malloc(NumByteToWrite);
	uint8_t buf_w[48] = {0};
	memset(buf_w, 0, 48);

	bool flag = false;
	for(uint16_t i=0; i<NumByteToWrite; i++){
		if(pBuffer[i] != 0 && !flag){
			buf_w[i] = pBuffer[i];
		}else{
			flag = true;
			buf_w[i] = '\0';		// 多余字符为0
		}
	}

	// 将所有写数据相加求和，取最后一个字节作为校验
	uint8_t chk_sum = 0;
	// 去掉1B校验和1B的\0
	for(uint16_t j=0; j<NumByteToWrite-2; j++){
		chk_sum += buf_w[j];
	}
//	fprintf(USART1_STREAM, "write:%02X\r\n", chk_sum);
	// 存储时将校验存放到最后1字节
	if(chk_sum == 0xFF) chk_sum = 0xFE;	// 为了区分格式化的FLASH的默认数据时0xFF
	buf_w[NumByteToWrite-2] = chk_sum;
	buf_w[NumByteToWrite-1] = '\0';

	// 测试打印
//	fprintf(USART1_STREAM, "\r\n------------write-----------\r\n");
//	for(uint16_t i=0; i< NumByteToWrite; i++){
//		fprintf(USART1_STREAM,"%02X ",buf_w[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n------------write-----------\r\n");

	uint32_t pos	= WriteAddr / EX_FLASH_SECTOR_SIZE;		//扇区地址
	uint16_t off	= WriteAddr % EX_FLASH_SECTOR_SIZE;		//在扇区内的偏移
	memset(W25QXX_BUF, 0, sizeof(W25QXX_BUF));

	W25QXX_Read(W25QXX_BUF, pos*EX_FLASH_SECTOR_SIZE, W25_RW_DATA_SIZE);//读出整个扇区的内容 EX_FLASH_SECTOR_SIZE

	for(uint16_t i=0; i< NumByteToWrite; i++)	   		//复制
	{
		W25QXX_BUF[off+i] = buf_w[i];
	}

//	free(buf_w);
	/* 写入整个扇区 */
	W25QXX_Erase_Sector(pos);			//擦除这个扇区
	W25QXX_Write_NoCheck(W25QXX_BUF, pos*EX_FLASH_SECTOR_SIZE, W25_RW_DATA_SIZE);	// EX_FLASH_SECTOR_SIZE  strlen(W25QXX_BUF)

	return true;
}

bool W25QXX_Write_Chk2(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
  // 将所有写数据相加求和，取最后一个字节作为校验
  uint8_t chk_sum = 0;
  // 去掉1B校验和1B的\0
  for(uint16_t j=0; j<NumByteToWrite; j++){
    chk_sum += pBuffer[j];
  }

  if(chk_sum == 0xFF) {
    chk_sum = 0xFE;
  } // 为了区分格式化的FLASH的默认数据时0xFF

  // 测试打印
//  fprintf(USART1_STREAM, "\r\n------------write-----------\r\n");
//  for(uint16_t i=0; i< NumByteToWrite; i++){
//    fprintf(USART1_STREAM,"%02X ",buf_w[i]);
//  }
  //fprintf(USART1_STREAM, "\r\n------------write1------%x-----\r\n", chk_sum);

  uint32_t pos  = WriteAddr / EX_FLASH_SECTOR_SIZE;   //扇区地址
  uint16_t off  = WriteAddr % EX_FLASH_SECTOR_SIZE;   //在扇区内的偏移
  memset(W25QXX_BUF, 0, sizeof(W25QXX_BUF));

  W25QXX_Read(W25QXX_BUF, pos*EX_FLASH_SECTOR_SIZE, W25_RW_DATA_SIZE);//读出整个扇区的内容 EX_FLASH_SECTOR_SIZE

  for(uint16_t i=0; i< NumByteToWrite; i++)       //复制
  {
    W25QXX_BUF[off+i] = pBuffer[i];
  }

  W25QXX_BUF[ off+ NumByteToWrite] = chk_sum;
  W25QXX_BUF[ off+ NumByteToWrite + 1] = '\0';

  //fprintf(USART1_STREAM, "\r\n------------write2------%x-----\r\n", W25QXX_BUF[ off+ NumByteToWrite]);

//  free(buf_w);
  /* 写入整个扇区 */
  W25QXX_Erase_Sector(pos);     //擦除这个扇区
  W25QXX_Write_NoCheck(W25QXX_BUF, pos*EX_FLASH_SECTOR_SIZE, W25_RW_DATA_SIZE); // EX_FLASH_SECTOR_SIZE  strlen(W25QXX_BUF)

  return true;
}

//=============================================================================
//函数名称:W25QXX_Erase_Chip
//功能概要:擦除整个芯片
//参数名称:
//函数返回:无
//说明     等待时间超长...
//=============================================================================
//void W25QXX_Erase_Chip(void)
//{
//	W25QXX_Write_Enable();                  	//SET WEL
//	W25QXX_Wait_Busy();
//	SPI_FLASH_CS_LOW();                     	//使能器件
//
//	SPI2_ReadWriteByte(W25X_ChipErase);        	//发送片擦除命令0xC7
//
//	SPI_FLASH_CS_HIGH();                    	//取消片选
//	W25QXX_Wait_Busy();   				   		//等待芯片擦除结束
//}
//=============================================================================
//函数名称:W25QXX_Erase_Sector
//功能概要:擦除一个扇区
//参数名称:Dst_Addr:扇区地址 根据实际容量设置
//函数返回:无
//说明    擦除一个山区的最少时间:150ms
//=============================================================================
void W25QXX_Erase_Sector(uint32_t Dst_Addr)
{
	//监视falsh擦除情况,测试用
	Dst_Addr *= EX_FLASH_SECTOR_SIZE;
	W25QXX_Write_Enable();                  	//SET WEL
	W25QXX_Wait_Busy();
	SPI_FLASH_CS_LOW();                         //使能器件

	SPI2_ReadWriteByte(W25X_SectorErase);      	//发送扇区擦除指令 0x20
	SPI2_ReadWriteByte((uint8_t)((Dst_Addr)>>16));  //发送24bit地址
	SPI2_ReadWriteByte((uint8_t)((Dst_Addr)>>8));
	SPI2_ReadWriteByte((uint8_t)Dst_Addr);

	SPI_FLASH_CS_HIGH();                        //取消片选
	W25QXX_Wait_Busy();   				   		//等待擦除完成
}
//=============================================================================
//函数名称:W25QXX_Wait_Busy
//功能概要:等待空闲
//参数名称:无
//函数返回:无
//=============================================================================
void W25QXX_Wait_Busy(void)
{
	while((W25QXX_ReadSR()&0x01)==0x01);   		//等待BUSY位清空
}
//=============================================================================
//函数名称:W25QXX_PowerDown
//功能概要:进入掉电模式
//参数名称:无
//函数返回:无
//=============================================================================
void W25QXX_PowerDown(void)
{
	SPI_FLASH_CS_LOW();                     	//使能器件
	SPI2_ReadWriteByte(W25X_PowerDown);        	//发送掉电命令 0xB9
	SPI_FLASH_CS_HIGH();                      	//取消片选
//	delay_us(3);                               	//等待TPD
}
//=============================================================================
//函数名称:W25QXX_WAKEUP
//功能概要:唤醒
//参数名称:无
//函数返回:无
//=============================================================================
void W25QXX_WAKEUP(void)
{
	SPI_FLASH_CS_LOW();                     	//使能器件
	SPI2_ReadWriteByte(W25X_ReleasePowerDown);  //send W25X_PowerDown command 0xAB
	SPI_FLASH_CS_HIGH();                        //取消片选
//	delay_us(3);                              	//等待TRES1
}

/*
 * 外FLASH中获取程序运行版本号
 * 参数：版本号指针
 * 返回：true成功；false失败
 */
bool Get_APP_Run_Version(uint8_t *p_version)
{
	return W25QXX_Read_Chk(p_version, OUT_FLASH_PARAM_START+RUN_VERSION_OFFSET, VERSION_SIZE);
}
/*
 * 外部FLASH中获取GPS的时间间隔
 * 返回：GPS上报的时间间隔
 * 说明：取值范围为[0,60]。获取失败，反馈
 */
uint8_t Get_GPS_Interval()
{
	uint8_t data[DATA_LEN_4B] = {0};

	if(W25QXX_Read_Chk(data, OUT_FLASH_PARAM_START + GPS_INTERVAL_OFFSET, DATA_LEN_4B)){
		uint8_t time = atoi(data);

		if(time>60 || time<5){
			fprintf(USART1_STREAM, "[ERROR] GPS Interval Use Default Value:30.\r\n");
			goto RESULT;
		}

		return time;
	}

RESULT:
	return 30;	// 默认30
}
/*
 * 获取版本信息
 * 参数：获取版本信息
 * 返回：无
 */
bool Get_APP_UP_Version(uint8_t *p_version)
{
	return W25QXX_Read_Chk(p_version, OUT_FLASH_PARAM_START, VERSION_SIZE);
}

/*
 * 获取升级包的大小
 * 参数：无
 * 返回：升级包大小
 */
uint32_t Get_APP_UP_PKG_size(void)
{
	uint8_t binSize[PKG_SIZE_SIZE] = {0};

	if(W25QXX_Read_Chk(binSize, OUT_FLASH_PARAM_START+PKG_SIZE_OFFSET, PKG_SIZE_SIZE)){
		return atoi(binSize);
	}

	return 0;
}

/*
 * 解析出版本号中的对应版本数字
 * 参数：版本号v1.0.0.1
 * 返回：AppVersion结构体
 */
AppVersion Split_APP_Version(uint8_t *p_version)
{
	uint8_t version[VERSION_SIZE] 	= {0};
	uint8_t pos 					= 0;
	AppVersion ver 					= {0};

	memset(&ver, 0, sizeof(AppVersion));
	strcpy(version, p_version);

	uint8_t *p = strtok(version + 1, ".");	// delete V

	while(p)
	{
		switch(pos){
		case 0:ver.main_v = atoi(p);break;
		case 1:ver.sub_v = atoi(p);break;
		case 2:ver.rev_v = atoi(p);break;
//		case 3:ver.bld_v = atoi(p);break;
		case 3:
			strcpy(ver.sensor_v,p);
			ver.sensor_v[strlen(ver.sensor_v)] = '\0';
			break;
		default:break;
		}
		p = strtok(NULL, ".");
		pos++;
	}
	pos = 0;

	return ver;
}

/*
 * 外部FLASH中设置应用程序的版本号
 * 参数：版本号指针
 * 返回：true成功，false失败
 */
bool Set_APP_UP_Version(uint8_t *p_version)
{
	uint8_t up_version[VERSION_SIZE] = {0};

	memcpy(up_version, p_version, strlen(p_version));
	W25QXX_Write_Chk(up_version, OUT_FLASH_PARAM_START, VERSION_SIZE);

	memset(up_version, 0, VERSION_SIZE);
	return W25QXX_Read_Chk(up_version, OUT_FLASH_PARAM_START, VERSION_SIZE);
}
/*
 * 外部FLASH中设置升级包的长度
 * 参数：升级包的大小
 * 返回：true成功，false失败
 */
bool Set_APP_UP_PKG_size(uint32_t len)
{
	uint8_t len_s[PKG_SIZE_SIZE] = {0};
	sprintf(len_s, "%d", len);

	W25QXX_Write_Chk(len_s, OUT_FLASH_PARAM_START+PKG_SIZE_OFFSET, PKG_SIZE_SIZE);

	memset(len_s, 0, PKG_SIZE_SIZE);
	return W25QXX_Read_Chk(len_s, OUT_FLASH_PARAM_START+PKG_SIZE_OFFSET, PKG_SIZE_SIZE);
}
/*
 * 外部FLASH中设置AEB算法端参数
 * 参数1：数据指针；
 * 参数2：数据长度
 * 参数3：参数ID
 * 返回：true成功，false失败
 */
bool Set_AEB_Alg_Parameter(uint8_t *data, uint16_t dataLen, uint8_t id)
{
	if(!W25QXX_Write_One_Sector_Chk(data, OUT_FLASH_ALGPPARM_START+(id-1)*EX_FLASH_SECTOR_SIZE+10, dataLen)){
		return false;
	}

//	fprintf(USART1_STREAM, "id:%d, read dataLen:%d\r\n", id, dataLen);
	uint8_t data_r[512] = {0};
	if(W25QXX_Read_One_Sector_Chk(data_r, OUT_FLASH_ALGPPARM_START+(id-1)*EX_FLASH_SECTOR_SIZE+10, dataLen)){
		return true;
	}else{
		// 失败就就擦除扇区
		W25QXX_Erase_Sector(OUT_FLASH_ALGPPARM_START/EX_FLASH_SECTOR_SIZE + id - 1);
		return false;
	}
}
/*
 * 设置AEB算法组ID
 * 参数：ID号
 * 返回：true成功，false失败
 */
bool Set_Vehicle_Type_ExFlh(uint8_t id)
{
	if(id>101 || id==0) return false;

	uint8_t data[ALG_PARA_GROUPID_SIZE] = {0};
	sprintf(data, "%d", id);

	W25QXX_Write_Chk(data, OUT_FLASH_ALGPPARMGROUP_START, ALG_PARA_GROUPID_SIZE);

	memset(data, 0, ALG_PARA_GROUPID_SIZE);

	return W25QXX_Read_Chk(data, OUT_FLASH_ALGPPARMGROUP_START, ALG_PARA_GROUPID_SIZE);
}

/*
 * 外FLASH中获取AEB算法组ID
 * 返回：0失败；非0成功
 */
uint8_t Get_Vehicle_Type_ExFlh()
{
	uint8_t data[ALG_PARA_GROUPID_SIZE] = {0};
	uint8_t id = 1;//默认车辆类型为1
	if(W25QXX_Read_Chk(data, OUT_FLASH_ALGPPARMGROUP_START, ALG_PARA_GROUPID_SIZE))
	{
		id = atoi(data);
		if(id==0 || id>101){	// 出厂
			id = 1;
		}
	}

	return id;
}
/*
 * 获取AEB 算法的参数
 * 参数1：数据指针；
 * 参数2：数据长度
 * 参数3：参数ID
 * 返回：true成功，false失败
 */
bool Get_AEB_Alg_Parameter(uint8_t *data, uint16_t dataLen, uint8_t id)
{
	if(id < 1 && id > 101){
		fprintf(USART1_STREAM, "[ERROR]vehicle_type effecrive range is [1,100].\r\n");
		return false;
	}
//	fprintf(USART1_STREAM, "id:%d, read dataLen:%d\r\n", id, dataLen);

	return W25QXX_Read_One_Sector_Chk(data, OUT_FLASH_ALGPPARM_START + (id-1) * EX_FLASH_SECTOR_SIZE+10, dataLen);
}

/*
 * 设置程序运行版本号
 * 参数：版本号
 * 返回：无
 * 返回：true成功，false失败
 * 说明：在bootloader模式下使用
 */
bool Set_App_Run_Version(uint8_t *p_version)
{
	uint8_t run_version[VERSION_SIZE] = {0};

	memcpy(run_version, p_version, strlen(p_version));
	W25QXX_Write_Chk(run_version, OUT_FLASH_PARAM_START+RUN_VERSION_OFFSET, VERSION_SIZE);

	memset(run_version, 0, VERSION_SIZE);
	return W25QXX_Read_Chk(run_version, OUT_FLASH_PARAM_START+RUN_VERSION_OFFSET, VERSION_SIZE);
}

/*
 * 设置GPS的时间间隔
 * 参数：时间间隔（单位秒）
 * 返回：true成功，false失败
 * 取值范围[5, 60]
 */
bool Set_GPS_Interval(uint8_t time)
{
	if(time>60 || time<5){
		fprintf(USART1_STREAM, "Valid GPS Interval is [5, 60].\r\n");
		return false;
	}

	uint8_t data[DATA_LEN_4B] = {0};
	sprintf(data, "%d", time);

	W25QXX_Write_Chk(data, OUT_FLASH_PARAM_START + GPS_INTERVAL_OFFSET, DATA_LEN_4B);

	memset(data, 0, DATA_LEN_4B);
	return W25QXX_Read_Chk(data, OUT_FLASH_PARAM_START + GPS_INTERVAL_OFFSET, DATA_LEN_4B);
}
/*
 * 判断APP是否需要下载平台bin文件
 * 参数：平台版本号
 * 返回：1需要升级；0无需升级
 */
uint8_t Version_Compare(uint8_t *p_run_ver, uint8_t *p_up_ver)
{
//	fprintf(USART1_STREAM,"%s,%s\r\n", p_run_ver, p_up_ver);

	AppVersion run_v 	= Split_APP_Version(p_run_ver);
	AppVersion up_v 	= Split_APP_Version(p_up_ver);

	// 0.9.1.L1C5,0.8.9.L1C5
	if(run_v.main_v < up_v.main_v){							// 升级版本号主版本高，就直接升级
//		fprintf(USART1_STREAM, "Need Upgrade, run_v.main_v:%d, up_v.main_v:%d\r\n", run_v.main_v, up_v.main_v);
		return 1;
	}else if(run_v.main_v > up_v.main_v){					// 升级版本号主版本低，不升级
//		fprintf(USART1_STREAM, "No Upgrade, run_v.main_v:%d, up_v.main_v:%d\r\n", run_v.main_v, up_v.main_v);
		return 0;
	}else{
		if(run_v.sub_v < up_v.sub_v){						// 升级版本号子版本高，就直接升级
//			fprintf(USART1_STREAM, "Need Upgrade, run_v.sub_v:%d, up_v.sub_v:%d\r\n", run_v.sub_v, up_v.sub_v);
			return 1;
		}else if(run_v.sub_v > up_v.sub_v){					// 升级版本号子版本低，不升级
//			fprintf(USART1_STREAM, "No Upgrade, run_v.sub_v:%d, up_v.sub_v:%d\r\n", run_v.sub_v, up_v.sub_v);
			return 0;
		}else{
			if(run_v.rev_v < up_v.rev_v){					// 升级版本号修订版本高，就直接升级
//				fprintf(USART1_STREAM, "Need Upgrade, run_v.rev_v:%d, up_v.rev_v:%d\r\n", run_v.rev_v, up_v.rev_v);
				return 1;
			}else if(run_v.rev_v > up_v.rev_v){				// 升级版本号修订版本低，不升级
//				fprintf(USART1_STREAM, "No Upgrade, run_v.rev_v:%d, up_v.rev_v:%d\r\n", run_v.rev_v, up_v.rev_v);
				return 0;
			}else{
//				fprintf(USART1_STREAM, "No Upgrade, run_v.rev_v:%d, up_v.rev_v:%d\r\n", run_v.rev_v, up_v.rev_v);
				return 0;
			}
		}
	}
	return 0;
}

/*
 * 设置设备SN号
 * 参数：SN号
 * 返回：true成功，false失败
 */
bool Set_Device_SN(uint8_t *p_sn)
{
	if(strlen(p_sn) > DEV_SN_SIZE) return false;

	uint8_t sn[DEV_SN_SIZE] = {0};
	memcpy(sn, p_sn, strlen(p_sn));

	W25QXX_Write_Chk(sn, OUT_FLASH_SN_START, DEV_SN_SIZE);

	memset(sn, 0, DEV_SN_SIZE);
	return W25QXX_Read_Chk(sn, OUT_FLASH_SN_START, DEV_SN_SIZE);
}

/*
 * 设置设备PN号
 * 参数：PN号
 * 返回：true成功，false失败
 */
bool Set_Device_PN(uint8_t *p_pn)
{
	if(strlen(p_pn) > DEV_PN_SIZE) return false;

	uint8_t pn[DEV_PN_SIZE] = {0};
	memcpy(pn, p_pn, strlen(p_pn));

	W25QXX_Write_Chk(pn, OUT_FLASH_SN_START+DEV_PN_OFFSET, DEV_PN_SIZE);

	memset(pn, 0, DEV_PN_SIZE);
	return W25QXX_Read_Chk(pn, OUT_FLASH_SN_START+DEV_PN_OFFSET, DEV_PN_SIZE);
}
/*
 * 外部FLASH中设置从平台获取的秘钥
 * 参数：秘钥指针
 * 返回：true成功，false失败
 */
bool Set_Secret(uint8_t *p_secret)
{
	if(strlen(p_secret) > SECRET_SIZE){
		memset(p_secret, 0, SECRET_SIZE);
		return false;
	}
	uint8_t secret[SECRET_SIZE] = {0};
	memcpy(secret, p_secret, strlen(p_secret));

	W25QXX_Write_Chk(secret, OUT_FLASH_SN_START+SECRET_OFFSET, SECRET_SIZE);

	memset(secret, 0, SECRET_SIZE);
	return W25QXX_Read_Chk(secret, OUT_FLASH_SN_START+SECRET_OFFSET, SECRET_SIZE);
}
/*
 * 外部FLASH中设置USART0与EC200U-CN通讯的波特率
 * 参数：波特率指针
 * 返回：true成功；false失败
 */
bool Set_EC200U_CN_Baud(uint8_t *p_baud)
{
	if(strlen(p_baud) > EC_BAUD_SIZE) return false;

	uint8_t baud[EC_BAUD_SIZE] = {0};
	memcpy(baud, p_baud, strlen(p_baud));

	W25QXX_Write_Chk(baud, OUT_FLASH_SN_START+EC_BAUD_OFFSET, EC_BAUD_SIZE);

	memset(baud, 0, EC_BAUD_SIZE);
	return W25QXX_Read_Chk(baud, OUT_FLASH_SN_START+EC_BAUD_OFFSET, EC_BAUD_SIZE);
}
/*
 * 外部FLASH中获取设备SN号
 * 参数：设备SN指针
 * 返回：false失败；true成功
 */
bool Get_Device_SN(char * p_device_sn)
{
	return W25QXX_Read_Chk(p_device_sn, OUT_FLASH_SN_START, DEV_SN_SIZE);
}

/*
 * 外部FLASH中获取设备PN号
 * 参数：设备PN号指针
 * 返回：false失败；true成功
 */
bool Get_Device_PN(char * p_device_pn)
{
	return W25QXX_Read_Chk(p_device_pn, OUT_FLASH_SN_START+DEV_PN_OFFSET, DEV_PN_SIZE);
}
/*
 * 外部FLASH中获取从平台获取的秘钥
 * 参数：秘钥指针
 * 返回：true成功；false失败
 */
bool Get_Secret(uint8_t *p_secret)
{
	return W25QXX_Read_Chk(p_secret, OUT_FLASH_SN_START+SECRET_OFFSET, SECRET_SIZE);
}

/*
 * 外部FLASH中获取升级包bin校验值（兼容版主要为了兼容包含V0.9.2.L1C5之前版本）
 * 参数：升级包和校验值
 * 返回：0获取失败；非0获取成功
 */
uint32_t Get_APP_UP_Bin_Chk_Value_Compatible()
{
	uint8_t chk[UPGRADE_BINCHK_SIZE] = {0};

	if(W25QXX_Read_Chk(chk, OUT_FLASH_UPGRADE_INFO_START, UPGRADE_BINCHK_SIZE)){
		uint32_t chk_sum = chk[3]<<24 | chk[2]<<16 | chk[1]<<8 + chk[0];
		return chk_sum;
	}

	return 0;
}
/*
 * 获取硬件信息（硬件版本信息）
 * 参数：硬件信息
 * 返回：true成功；false失败
 */
bool Get_Hardware_Info(uint8_t *p_info)
{
	return W25QXX_Read_Chk(p_info, OUT_FLASH_SN_START + HARDWARE_INFO_OFFSET, HARDWARE_INFO_SIZE);
}

/*
 * 外部FLASH中设置升级包bin校验值（兼容版主要为了兼容包含V0.9.2.L1C5之前版本）
 * 参数：升级包和校验值
 * 返回：true成功；false失败
 */
bool Set_APP_UP_Bin_Chk_Value_Compatible(uint32_t chk_value)
{
	uint8_t chk[UPGRADE_BINCHK_SIZE] = {0};
	chk[0] = chk_value;
	chk[1] = chk_value>>8;
	chk[2] = chk_value>>16;
	chk[3] = chk_value>>24;

	W25QXX_Write_Chk(chk, OUT_FLASH_UPGRADE_INFO_START, UPGRADE_BINCHK_SIZE);
	memset(chk, 0, UPGRADE_BINCHK_SIZE);
	return W25QXX_Read_Chk(chk, OUT_FLASH_UPGRADE_INFO_START, UPGRADE_BINCHK_SIZE);
}
/*
 * 设置硬件版本信息（硬件版本号）
 * 参数：硬件版本信息‘
 * 返回：true成功；false失败
 */
bool Set_Hardware_Info(uint8_t *p_info)
{
	uint8_t info[HARDWARE_INFO_SIZE] = {0};
	memcpy(info, p_info, strlen(p_info));

	W25QXX_Write_Chk(info, OUT_FLASH_SN_START + HARDWARE_INFO_OFFSET, HARDWARE_INFO_SIZE);

	memset(info, 0, UPGRADE_BINCHK_SIZE);
	return W25QXX_Read_Chk(info, OUT_FLASH_SN_START + HARDWARE_INFO_OFFSET, HARDWARE_INFO_SIZE);
}
/*
 * 外部FLASH中写一个扇区的数据（加密操作）
 * 参数1：数据指针
 * 参数2：地址
 * 参数3：写数据长度
 * 返回：true成功；false失败
 * 写一个扇区操作，最后两位为校验位
 */
bool W25QXX_Write_One_Sector_Chk(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
	uint8_t chk_sum = 0;

	// copy, get checkout value
	memset(w25_rw_data, 0, W25_RW_DATA_SIZE);
	for(uint16_t j=0; j<NumByteToWrite; j++){
		w25_rw_data[j] = pBuffer[j];
		chk_sum += pBuffer[j];
	}
//	fprintf(USART1_STREAM, "write:%02X\r\n", chk_sum);
	// 存储时将校验存放到最后1字节
	w25_rw_data[NumByteToWrite] = chk_sum;

	// 若写入数据全为0，且校验和也为0，就不写
	if(w25_rw_data[0]==0 && w25_rw_data[1]==0 && w25_rw_data[2]==0 && chk_sum==0){
		return false;
	}

//	fprintf(USART1_STREAM,"\r\n------------w------------------\r\n");
//	for(uint16_t i=0; i< NumByteToWrite+1; i++){
//		fprintf(USART1_STREAM,"%02X ",w25_rw_data[i]);
//	}
//	fprintf(USART1_STREAM,"\r\n------------w------------------\r\n");

	/* 写入整个扇区 */
	W25QXX_Erase_Sector(WriteAddr / EX_FLASH_SECTOR_SIZE);					//擦除这个扇区
	W25QXX_Write_NoCheck(w25_rw_data, WriteAddr, NumByteToWrite+1);

	return true;
}
/*
 * 外部FLASH中读一个扇区的数据（加密操作）
 * 参数1：数据指针
 * 参数2：地址
 * 参数3：读取数据长度
 * 返回：true成功；false失败
 * 写一个扇区操作，最后两位为校验位
 */
bool W25QXX_Read_One_Sector_Chk(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToWrite)
{
	SPI_FLASH_CS_LOW();                    		//使能器件

    SPI2_ReadWriteByte(W25X_ReadData);         	//发送读取命令0x03
    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF0000)>>16));	//发送24bit地址
    SPI2_ReadWriteByte((uint8_t)((ReadAddr&0xFF00)>>8));
    SPI2_ReadWriteByte((uint8_t)(ReadAddr&0xFF));

	memset(w25_rw_data, 0, W25_RW_DATA_SIZE);

    uint16_t i=0;
    for(; i<NumByteToWrite+1; i++)
	{
    	w25_rw_data[i] = SPI2_ReadWriteByte(0XFF);   	//循环读数
    }
	SPI_FLASH_CS_HIGH();

	// 测试打印
//	fprintf(USART1_STREAM,"\r\n------------r------------------\r\n");
//	for(uint16_t j=0; j<NumByteToWrite + 1; j++){
//		fprintf(USART1_STREAM, "%02X ", w25_rw_data[j]);
//	}
//	fprintf(USART1_STREAM,"\r\n------------r------------------\r\n");

	// 获取校验字节
	uint8_t src_Chk = w25_rw_data[NumByteToWrite];

	// 若写入数据全为0，且校验和也为0，就不写
	if(w25_rw_data[0]==0 && w25_rw_data[1]==0 && w25_rw_data[2]==0 && src_Chk==0){
		memset(pBuffer, 0, NumByteToWrite);
		return false;
	}

//	 获取校验值
	uint8_t chk_sum = 0;
	for(uint16_t j=0; j<NumByteToWrite; j++){
		chk_sum += w25_rw_data[j];
	}
//	fprintf(USART1_STREAM, "\r\nchk:%02X, src:%02X\r\n", chk_sum, src_Chk);
	// 校验
	if(chk_sum == src_Chk){
		memcpy(pBuffer, w25_rw_data, NumByteToWrite);
		memset(w25_rw_data, 0, W25_RW_DATA_SIZE);

		return true;
	}
	memset(pBuffer, 0, NumByteToWrite);
	return false;
}



/*
 * 获取板卡SN号
 * 参数：传入参数
 * 返回：false失败；true成功
 */
bool Get_Board_SN(char * p_board_sn)
{
	return W25QXX_Read_Chk(p_board_sn, OUT_FLASH_SN_START + BOARD_SN_OFFSET, BOARD_SN_SIZE);
}

/*
 * 获取板卡PN号
 * 参数：传入参数
 * 返回：false失败；true成功
 */
bool Get_Board_PN(char * p_board_pn)
{
	return W25QXX_Read_Chk(p_board_pn, OUT_FLASH_SN_START + BOARD_PN_OFFSET, BOARD_PN_SIZE);
}

/*
 * 设置板卡PN号
 * 参数：PN号
 * 返回：true成功；false失败
 */
bool Set_Board_PN(uint8_t *p_pn)
{
	if(strlen(p_pn) > BOARD_PN_SIZE) return false;

	uint8_t pn[VERSION_SIZE] = {0};
	memcpy(pn, p_pn, strlen(p_pn));

	W25QXX_Write_Chk(pn, OUT_FLASH_SN_START + BOARD_PN_OFFSET, BOARD_PN_SIZE);

	memset(pn, 0, BOARD_PN_SIZE);
	return W25QXX_Read_Chk(pn, OUT_FLASH_SN_START+BOARD_PN_OFFSET, BOARD_PN_SIZE);
}

/*
 * 设置板卡SN号
 * 参数：SN号
 * 返回：ture成功；false失败
 */
bool Set_Board_SN(uint8_t *p_sn)
{
	if(strlen(p_sn) > BOARD_SN_SIZE) return false;

	uint8_t sn[BOARD_SN_SIZE] = {0};
	memcpy(sn, p_sn, strlen(p_sn));

	W25QXX_Write_Chk(sn, OUT_FLASH_SN_START + BOARD_SN_OFFSET, BOARD_SN_SIZE);

	memset(sn, 0, BOARD_SN_SIZE);
	return W25QXX_Read_Chk(sn, OUT_FLASH_SN_START+BOARD_SN_OFFSET, BOARD_SN_SIZE);
}

/*
 * 获取bootloader运行版本号
 * 参数：版本号指针
 * 返回：true成功；false失败
 */
bool Get_BOOT_Run_Version(uint8_t *p_read_version)
{
	return W25QXX_Read_Chk(p_read_version, OUT_FLASH_UPGRADE_INFO_START + BOOT_RUN_VER_OFFSET, BOOT_RUN_VER_SIZE);
}
/*
 * 获取bootloader升级包的升级版本号
 * 参数：版本号指针
 * 返回：true成功；false失败
 */
bool Get_BOOT_UP_Version(uint8_t *p_read_version)
{
	return W25QXX_Read_Chk(p_read_version, OUT_FLASH_UPGRADE_INFO_START + BOOT_UP_VER_OFFSET, BOOT_UP_VER_SIZE);
}
/*
 * 获取bootloader升级包大小
 * 返回：升级包大小（0失败，非0成功）
 */
uint32_t Get_BOOT_UP_PKG_size(void)
{
	uint8_t binSize[BOOT_BIN_PKG_SIZE] = {0};

	if(W25QXX_Read_Chk(binSize, OUT_FLASH_UPGRADE_INFO_START + BOOT_BIN_PKG_OFFSET, BOOT_BIN_PKG_SIZE)){
		return atoi(binSize);
	}
	return 0;
}
/*
 * 获取bootloader升级包Bin包的和校验值
 * 返回：和校验值（0失败，非0成功）
 */
uint32_t Get_BOOT_UP_Bin_Chk_Value()
{
	uint8_t binChk[BOOT_PKG_CHK_SIZE] = {0};

	if(W25QXX_Read_Chk(binChk, OUT_FLASH_UPGRADE_INFO_START + BOOT_PKG_CHK_OFFSET, BOOT_PKG_CHK_SIZE)){
		uint32_t chk_sum = binChk[3]<<24 | binChk[2]<<16 | binChk[1]<<8 | binChk[0];
		return chk_sum;
	}
	return 0;
}

/*
 * 设置bootloader运行版本号
 * 参数：版本号指针
 * 返回：true成功；false失败
 */
bool Set_BOOT_Run_Version(uint8_t *p_read_version)
{
	uint8_t run_version[BOOT_RUN_VER_SIZE] = {0};
	memcpy(run_version, p_read_version, strlen(p_read_version));

	W25QXX_Write_Chk(run_version, OUT_FLASH_UPGRADE_INFO_START + BOOT_RUN_VER_OFFSET, BOOT_RUN_VER_SIZE);

	memset(run_version, 0, BOOT_RUN_VER_SIZE);
	return W25QXX_Read_Chk(run_version, OUT_FLASH_UPGRADE_INFO_START + BOOT_RUN_VER_OFFSET, BOOT_RUN_VER_SIZE);
}
/*
 * 设置bootloader升级包版本号
 * 参数：版本号指针
 * 返回：true成功；false失败
 */
bool Set_BOOT_UP_Version(uint8_t *p_read_version)
{
	uint8_t upgrade_version[BOOT_UP_VER_SIZE] = {0};
	memcpy(upgrade_version, p_read_version, strlen(p_read_version));

	W25QXX_Write_Chk(upgrade_version, OUT_FLASH_UPGRADE_INFO_START + BOOT_UP_VER_OFFSET, BOOT_UP_VER_SIZE);

	memset(upgrade_version, 0, BOOT_UP_VER_SIZE);
	return W25QXX_Read_Chk(upgrade_version, OUT_FLASH_UPGRADE_INFO_START + BOOT_UP_VER_OFFSET, BOOT_UP_VER_SIZE);
}
/*
 * 设置bootloader升级包的大小
 * 参数：升级包bin包的大小，单位B
 */
bool Set_BOOT_UP_PKG_size(uint32_t len)
{
	uint8_t len_s[BOOT_BIN_PKG_SIZE] = {0};
	sprintf(len_s, "%d", len);

	W25QXX_Write_Chk(len_s, OUT_FLASH_UPGRADE_INFO_START + BOOT_BIN_PKG_OFFSET, BOOT_BIN_PKG_SIZE);

	memset(len_s, 0, BOOT_BIN_PKG_SIZE);
	return W25QXX_Read_Chk(len_s, OUT_FLASH_UPGRADE_INFO_START + BOOT_BIN_PKG_OFFSET, BOOT_BIN_PKG_SIZE);
}
/*
 * 设置bootloader升级包的bin包校验值
 * 参数：和校验值
 * 返回：true成功；false失败
 */
bool Set_BOOT_UP_Bin_Chk_Value(uint32_t chk_value)
{
	uint8_t chk[BOOT_PKG_CHK_SIZE] = {0};
	chk[0] = chk_value;
	chk[1] = chk_value>>8;
	chk[2] = chk_value>>16;
	chk[3] = chk_value>>24;

	W25QXX_Write_Chk(chk, OUT_FLASH_UPGRADE_INFO_START + BOOT_PKG_CHK_OFFSET, BOOT_PKG_CHK_SIZE);
	memset(chk, 0, BOOT_PKG_CHK_SIZE);
	return W25QXX_Read_Chk(chk, OUT_FLASH_UPGRADE_INFO_START + BOOT_PKG_CHK_OFFSET, BOOT_PKG_CHK_SIZE);
}

// screen
/*
 * 获取小屏幕运行版本号
 * 参数：版本号指针
 */
bool Get_SCREEN_Run_Version(uint8_t *p_read_version)
{
	return W25QXX_Read_Chk(p_read_version, OUT_FLASH_UPGRADE_INFO_START + SCREEN_RUN_VER_OFFSET, SCREEN_RUN_VER_SIZE);
}
/*
 * 获取小屏幕升级版本号
 * 参数：版本号指针
 */
bool Get_SCREEN_UP_Version(uint8_t *p_read_version)
{
	return W25QXX_Read_Chk(p_read_version, OUT_FLASH_UPGRADE_INFO_START + SCREEN_UP_VER_OFFSET, SCREEN_UP_VER_SIZE);
}
/*
 * 获取小屏幕升级包的大小
 * 返回：升级包的大小
 */
uint32_t Get_SCREEN_UP_PKG_size(void)
{
	uint8_t binSize[SCREEN_BIN_PKG_SIZE] = {0};

	if(W25QXX_Read_Chk(binSize, OUT_FLASH_UPGRADE_INFO_START + SCREEN_BIN_PKG_OFFSET, SCREEN_BIN_PKG_SIZE)){
		return atoi(binSize);
	}
	return 0;
}
/*
 * 获取小屏幕升级bin包的和校验值
 * 返回：升级包的和校验值
 */
uint32_t Get_SCREEN_UP_Bin_Chk_Value()
{
	uint8_t binChk[SCREEN_PKG_CHK_SIZE] = {0};

	if(W25QXX_Read_Chk(binChk, OUT_FLASH_UPGRADE_INFO_START + SCREEN_PKG_CHK_OFFSET, SCREEN_PKG_CHK_SIZE)){
		uint32_t chk_sum = binChk[3]<<24 | binChk[2]<<16 | binChk[1]<<8 | binChk[0];
		return chk_sum;
	}
	return 0;
}
/*
 * 获取小屏幕升级标记位
 */
uint8_t Get_SCREEN_IS_Upgrade_Flag()
{
	uint8_t flag[SCREEN_IS_UPGRADE] = {0};

	if(W25QXX_Read_Chk(flag, OUT_FLASH_UPGRADE_INFO_START + SCREEN_IS_UPGRADE_OFFSET, SCREEN_IS_UPGRADE)){
		return atoi(flag);
	}
	return 0;
}
/*
 * 获取APP升级标志位
 * 返回：1设备上电后需要回复平台（0xB8）；0设备上电后无需回复平台
 */
uint8_t Get_APP_IS_Upgrade_Flag()
{
	uint8_t flag[APP_IS_UPGRADE] = {0};

	if(W25QXX_Read_Chk(flag, OUT_FLASH_UPGRADE_INFO_START + APP_IS_UPGRADE_OFFSET, APP_IS_UPGRADE)){
		return atoi(flag);
	}
	return 0;
}
/*
 * 设置小屏幕升级标记位
 */
bool Set_SCREEN_IS_Upgrade_Flag(uint8_t flg)
{
	uint8_t flag[SCREEN_IS_UPGRADE] = {0};
	sprintf(flag, "%d", flg);
	W25QXX_Write_Chk(flag, OUT_FLASH_UPGRADE_INFO_START + SCREEN_IS_UPGRADE_OFFSET, SCREEN_IS_UPGRADE);

	memset(flag, 0, SCREEN_IS_UPGRADE);
	return W25QXX_Read_Chk(flag, OUT_FLASH_UPGRADE_INFO_START + SCREEN_IS_UPGRADE_OFFSET, SCREEN_IS_UPGRADE);
}
/*
 * 设置应用程序升级标志位
 * 参数：标志位；1设备上电后需要回复平台（0xB8）；0设备上电后无需回复平台
 */
uint8_t Set_APP_IS_Upgrade_Flag(uint8_t flg)
{
	uint8_t flag[APP_IS_UPGRADE] = {0};
	sprintf(flag, "%d", flg);
	W25QXX_Write_Chk(flag, OUT_FLASH_UPGRADE_INFO_START + APP_IS_UPGRADE_OFFSET, APP_IS_UPGRADE);

	memset(flag, 0, APP_IS_UPGRADE);
	return W25QXX_Read_Chk(flag, OUT_FLASH_UPGRADE_INFO_START + APP_IS_UPGRADE_OFFSET, APP_IS_UPGRADE);
}
/*
 * 设置小屏幕运行版本号
 * 参数：版本号指针
 */
bool Set_SCREEN_Run_Version(uint8_t *p_write_version)
{
	uint8_t run_version[VERSION_SIZE] = {0};
	memcpy(run_version, p_write_version, strlen(p_write_version));

	W25QXX_Write_Chk(run_version, OUT_FLASH_UPGRADE_INFO_START + SCREEN_RUN_VER_OFFSET, SCREEN_RUN_VER_SIZE);

	memset(run_version, 0, SCREEN_RUN_VER_SIZE);
	return W25QXX_Read_Chk(run_version, OUT_FLASH_UPGRADE_INFO_START + SCREEN_RUN_VER_OFFSET, SCREEN_RUN_VER_SIZE);
}

/*
 * 设置小屏幕升级版本号
 * 参数：版本号指针
 */
bool Set_SCREEN_UP_Version(uint8_t *p_write_version)
{
	uint8_t upgrade_version[VERSION_SIZE] = {0};
	memcpy(upgrade_version, p_write_version, strlen(p_write_version));

	W25QXX_Write_Chk(upgrade_version, OUT_FLASH_UPGRADE_INFO_START + SCREEN_UP_VER_OFFSET, SCREEN_UP_VER_SIZE);

	memset(upgrade_version, 0, SCREEN_UP_VER_SIZE);
	return W25QXX_Read_Chk(upgrade_version, OUT_FLASH_UPGRADE_INFO_START + SCREEN_UP_VER_OFFSET, SCREEN_UP_VER_SIZE);
}

/*
 * 设置小屏幕升级包的大小
 * 参数：升级包的大小，单位B
 */
bool Set_SCREEN_UP_PKG_size(uint32_t len)
{
	uint8_t len_s[SCREEN_BIN_PKG_SIZE] = {0};
	sprintf(len_s, "%d", len);

	W25QXX_Write_Chk(len_s, OUT_FLASH_UPGRADE_INFO_START + SCREEN_BIN_PKG_OFFSET, SCREEN_BIN_PKG_SIZE);

	memset(len_s, 0, SCREEN_BIN_PKG_SIZE);
	return W25QXX_Read_Chk(len_s, OUT_FLASH_UPGRADE_INFO_START + SCREEN_BIN_PKG_OFFSET, SCREEN_BIN_PKG_SIZE);
}

/*
 * 设置小屏幕升级包校验值
 * 参数：升级包的校验值
 */
bool Set_SCREEN_UP_Bin_Chk_Value(uint32_t chk_value)
{
	uint8_t chk[SCREEN_PKG_CHK_SIZE] = {0};
	chk[0] = chk_value;
	chk[1] = chk_value>>8;
	chk[2] = chk_value>>16;
	chk[3] = chk_value>>24;

//	fprintf(USART1_STREAM,"write:%02X %02X %02X %02X %02X\r\n", chk[0],chk[1],chk[2],chk[3],chk[4]);

	W25QXX_Write_Chk(chk, OUT_FLASH_UPGRADE_INFO_START + SCREEN_PKG_CHK_OFFSET, SCREEN_PKG_CHK_SIZE);
	memset(chk, 0, SCREEN_PKG_CHK_SIZE);
	return W25QXX_Read_Chk(chk, OUT_FLASH_UPGRADE_INFO_START + SCREEN_PKG_CHK_OFFSET, SCREEN_PKG_CHK_SIZE);
}

/*
 * 获取数据防丢失下标（frontId_endId）
 * 参数：字符串(frontId_endId)
 */
bool Get_Data_Loss_Prevention_ID(uint8_t *p_id_s)
{
	return W25QXX_Read_One_Sector_Chk(p_id_s, OUT_FLASH_LOSS_DATA_ID_START, DLP_ID_SIZE);
}

/*
 * 设置数据防丢失下标
 * 参数1：前下标
 * 参数2：后下标
 * 说明：记录设备的front和end地址。便于设备重启后也能知道位置；
 * end_id追赶front_id，追上了就说明没有发送数据；否则就有(front_id - end_id)条需要发送的数据。
 */
bool Set_Data_Loss_Prevention_ID(uint16_t front_id, uint16_t end_id)
{
	uint8_t id_s[DLP_ID_SIZE] = {0};
	sprintf(id_s, "%d_%d", front_id, end_id);

	if(!W25QXX_Write_One_Sector_Chk(id_s, OUT_FLASH_LOSS_DATA_ID_START, DLP_ID_SIZE)){
		return false;
	}

	memset(id_s, 0, DLP_ID_SIZE);
	if(W25QXX_Read_One_Sector_Chk(id_s, OUT_FLASH_LOSS_DATA_ID_START, DLP_ID_SIZE)){
		return true;
	}else{
		// 失败就就擦除扇区
		W25QXX_Erase_Sector(OUT_FLASH_LOSS_DATA_ID_START/EX_FLASH_SECTOR_SIZE);
		return false;
	}
}

/*
 * 获取车辆里程计
 */
float Get_Vehicle_Mileage()
{
	float mileage = 0.0f;
	uint8_t mileage_s[MILEAGE_SIZE] = {0};
	if(W25QXX_Read_One_Sector_Chk(mileage_s, OUT_FLASH_MILEAGE_START, MILEAGE_SIZE)){
		mileage = *(float*)(mileage_s);
	}else{
		mileage = 0.0f;
	}
//	fprintf(USART1_STREAM, "\r\nMeliage: %f\r\n", mileage);

	return mileage;
}
/*
 * 设置车辆里程计
 * 参数：里程数
 */
bool Set_Vehicle_Mileage(float mileage)
{
	uint8_t data[MILEAGE_SIZE] = {0};
	memcpy(data, (char*)&mileage, 4);
	if(!W25QXX_Write_One_Sector_Chk(data, OUT_FLASH_MILEAGE_START, MILEAGE_SIZE)){
		return false;
	}

	memset(data, 0, MILEAGE_SIZE);
	if(W25QXX_Read_One_Sector_Chk(data, OUT_FLASH_MILEAGE_START, MILEAGE_SIZE)){
		return true;
	}else{
		// 失败就就擦除扇区
		W25QXX_Erase_Sector(OUT_FLASH_MILEAGE_START/EX_FLASH_SECTOR_SIZE);
		return false;
	}
}
/*******************************   0x1F3000 车CAN解析配置区 ***************************/
bool Read_Car_Can_Analysis_Info(uint8_t *data, uint16_t data_len)
{
	return W25QXX_Read_One_Sector_Chk(data, OUT_FLASH_CAR_CAN_ANALY_CFG_START, data_len);
}

bool Write_Car_Can_Analysis_Info(uint8_t *data, uint16_t data_len)
{
	if(!W25QXX_Write_One_Sector_Chk(data, OUT_FLASH_CAR_CAN_ANALY_CFG_START, data_len)){
		return false;
	}

//	fprintf(USART1_STREAM, "id:%d, read dataLen:%d\r\n", id, dataLen);
	memset(data, 0, strlen(data));
	if(W25QXX_Read_One_Sector_Chk(data, OUT_FLASH_CAR_CAN_ANALY_CFG_START, data_len)){
		return true;
	}else{
		// 失败就就擦除扇区
		W25QXX_Erase_Sector(OUT_FLASH_ALGPPARM_START/EX_FLASH_SECTOR_SIZE);
		return false;
	}
}
