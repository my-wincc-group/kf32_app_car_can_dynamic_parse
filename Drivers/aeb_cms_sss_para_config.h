/*
 * aeb_cms_sss_para_config.h
 *
 *  Created on: 2021-12-29
 *      Author: zkhy
 */

#ifndef AEB_CMS_SSS_PARA_CONFIG_H_
#define AEB_CMS_SSS_PARA_CONFIG_H_

#include "common.h"
#include <stdbool.h>
#include "upgrade_common.h"
// 适配车辆类型
enum Vehicle_Type
{
	// 1-10 用于参数配置
	JINLV_BUS = 1,
	BYD_E6 = 2,
	JIEFANG_TRUCK = 3,

	TEST_VEHICLE = 101

};
//gcz 2022-05-12
#define MAX_ULT_RADAR_NBR       12
typedef struct {
	uint8_t AEB_CMS_Enable;
	uint8_t CMS_HMW_Enable;
	uint8_t CMS_TTC_Enable;
	uint8_t AEB_TTC_Enable;
	uint8_t SSS_Enable;
	uint8_t HMW_Dynamic_Thr_Enable;
	uint8_t Chassis_Safe_Strategy_Enable;
	uint8_t LDW_Enable;
	uint8_t Displayer_Switch_Enable;	// 0:Disable; 1:enable(AEB+LDW); 2:AEB enable + LDW disable; 3.AEB disable + LDW enable
	uint8_t Retract_Brake_Enable;		// 0: Disable Retract Brake; 1: Enable Retract Brake(feeling good);
	//gcz 2022-05-12
	uint8_t ultrasonic_radar_monitor_ensble[MAX_ULT_RADAR_NBR];        //共12路，0:开启 1:关闭
}GlobalSwitch;

typedef struct {
	// Parameters Version
	uint32_t params_version;
	// Global Function Switch
	GlobalSwitch switch_g;				// add lmz 20220223	//12

//	uint8_t screen_Switch_Enable;		// 显示器开关使能 add lmz 20220518
//	uint8_t Retract_Brake_Enable;		// 收刹车使能

	// AEB、CMS common parameters
	float Brake_Cooling_Time;
	float Driver_Brake_Cooling_Time;
	float Max_Brake_Keep_Time;
	float Air_Brake_Delay_Time;
	float Max_Percent_Decelerate;
	float Min_Enable_Speed;
	float Max_Output_dec;
	float Ratio_Force_To_Deceleration;	// update lmz 20220223,from uint8_t to float	// 8
	// CMS parameters
	// CMS parameters - HMW
	float CMS_HMW_Brake_Time_Thr;
	float CMS_HMW_Brake_Force_Feel_Para;
	float CMS_HMW_Warning_Time_Thr;
	float CMS_HMW_Dynamic_Offset_Speed_L;
	float CMS_HMW_Dynamic_Offset_Value_L;
	float CMS_HMW_Dynamic_Offset_Speed_H;
	float CMS_HMW_Dynamic_Offset_Value_H;
	float CMS_HMW_Time_Offset_Night;//HMW
	float CMS_HMW_Time_Offset_Night_StartT;//Hour
	float CMS_HMW_Time_Offset_Night_EndT;  //Hour	// 10
	// CMS parameters - TTC
	float CMS_TTC_Brake_Time_Thr;
	float CMS_TTC_Brake_Force_Feel_Para;
	float CMS_TTC_Warning_Time_Level_First;
	float CMS_TTC_Warning_Time_Level_Second;
	// AEB
	float AEB_Decelerate_Set;
	float AEB_Stop_Distance;
	float AEB_TTC_Warning_Time_Level_First;
	float AEB_TTC_Warning_Time_Level_Second;	// 8
	// SSS
	float SSS_Brake_Force;
	float SSS_Break_Enable_Distance;
	float SSS_Warning_Enable_Distance;
	float SSS_Max_Enable_Speed;
	float SSS_FR_FL_Install_Distance_To_Side;
	float SSS_Stop_Distance;
	float SSS_Default_Turn_Angle;
	// wheel speed parameter
	float WheelSpeed_Coefficient;		// 8
	/////////////////////////////////////////////
	// System Parameters （is bound to Vehicle_Type）
	/////////////////////////////////////////////
	// Vehicle Width
	float Vechile_Width;	//35
	// Vehicle Speed
	uint8_t Vehicle_Speed_Obtain_Method; // CAN:0 GPIO:1
	// Vehicle state
	uint8_t Vehicle_State_Obtain_Method; // CAN:0 GPIO:1
	// Vehicle Brake Method
	uint8_t Vehicle_Brake_Control_Mode; // 0:Proportional valve(CAN) ; 1:electron magnetic valve(GPIO[PWM]) ; 2: Deceleration request(CAN)
	// Electron megnetic valve
	uint8_t PWM_magnetic_valve_T;
	uint8_t PWM_magnetic_valve_N;
	uint8_t PWM_magnetic_valve_Switch;
	uint8_t PWM_magnetic_valve_Scale;	//7
	// AEBS_CAN Baud Rate Set
	uint16_t CAN_0_Baud_Rate; // Camera
	uint16_t CAN_1_Baud_Rate; // mm Wave Radar
	uint16_t CAN_2_Baud_Rate; // Vechile
	uint16_t CAN_3_Baud_Rate; // Brake:Proportional valve
	uint16_t CAN_4_Baud_Rate; // Displayer
	uint16_t CAN_5_Baud_Rate; // Ultrasonic Radar	// 6

	//角雷达右前侧刹车条件控制参数  gcz 2022-09-23
	float f_rigth_font_y_dis;
	float f_rigth_font_x_acc;
	float f_rigth_font_x_max_ttc;
}KunLun_AEB_Para_Cfg;

typedef struct _Device_Version_Struct{
	uint8_t AEBS_SW_Version[SH_INFO_SIZE];
	uint8_t AEBS_SN[SH_INFO_SIZE];
	uint8_t AEBS_PN[SH_INFO_SIZE];
	uint8_t Camera_SW_Version[SH_INFO_SIZE];
	uint8_t Camera_HW_Version[SH_INFO_SIZE];
	uint8_t Camera_SN[SH_INFO_SIZE];
	uint8_t Camera_PN[SH_INFO_SIZE];
	uint8_t Displayer_SW_Version[SH_INFO_SIZE];
	uint8_t Displayer_SN[SH_INFO_SIZE];
	uint8_t Displayer_PN[SH_INFO_SIZE];
	uint8_t URadar_Version[SH_INFO_SIZE];
	uint8_t MRadar_Version[SH_INFO_SIZE];
	uint8_t Camera_CAN_ProtocoI_Version[SH_INFO_SIZE];
	uint8_t AEBS_AT_PC_ProtocoI_Version[SH_INFO_SIZE];
	uint8_t AEBS_AT_BT_ProtocoI_Version[SH_INFO_SIZE];
	uint8_t AEBS_CAN_ProtocoI_Version[SH_INFO_SIZE];
	uint8_t Displayer_CAN_ProtocoI_Version[SH_INFO_SIZE];
	uint8_t Cloud_Platform_ProtocoI_Version[SH_INFO_SIZE];
}Device_Version_Struct;

extern  KunLun_AEB_Para_Cfg wParms; // "write Copy"
extern  KunLun_AEB_Para_Cfg rParms;  // "read Copy"
extern 	Device_Version_Struct device_version_str;
// extern volatile uint8_t vehicle_Type;
extern volatile uint8_t Brake_Test_Pressure_Bar;
#endif /* AEB_CMS_SSS_PARA_CONFIG_H_ */
