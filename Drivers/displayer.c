/*
 * displayer.c
 *
 *  Created on: 2021-7-7
 *      Author: shuai
 */
#include "displayer.h"
#include "data_center_services.h"
#include "aeb_cms_sss_para_config.h"
#include "common.h"
#include "AEB_CMS_alg.h"
#include "gpio.h"
#include "AEB_upload_info.h"
#include "aeb_sensor_management.h"
Displayer_Show 	displayer_show_info = {0};
//BaseType_t		isAEBTaskClose 		= {0};
//BaseType_t 		isLDWTaskClose		= {0};
static uint8_t 	old_Time_ID 			= 0;
static uint8_t 	old_Time_ID_keep_num 	= 0;

struct can_frame displayer_tx_frame 			= {0};	// AEB_controler Tx to displayer
struct can_frame displayer_radar_tx_frame 		= {0};	// AEB_controler Tx to displayer
struct can_frame displayer_tx_write_snpn_frame 	= {0}; 	// AEB_controler Tx to displayer
struct can_frame displayer_tx_set_voice_frame	= {0};  // AEB_controler Tx to displayer
struct can_frame displayer_rx_read_snpn_frame;  		// AEB_controler Rx from displayer

void Displayer_Init()
{
	displayer_show_info.FrontCameraStatus 			= 0;
	displayer_show_info.MillimeterWaveRadarStatus 	= 0;
	displayer_show_info.UltrasonicRadarStatus		= 0;
	displayer_show_info.ValveStatus 				= 0;
	displayer_show_info.VehicleCANBusStatus 		= 0;
	displayer_show_info.VehicleSpeedStatus 			= 0;
	displayer_show_info.Module4GStatus 				= 0;
	displayer_show_info.GPSStatus 					= 0;
	displayer_show_info.AEBSTaskStatus 				= 2;
	displayer_show_info.FCWTaskStatus 				= 2;
	displayer_show_info.LDWTaskStatus 				= 2;
	displayer_show_info.SSSTaskStatus 				= 2;
	displayer_show_info.CMSTaskStatus 				= 2;
	displayer_show_info.FCWLevel 					= 0;
	displayer_show_info.LDW 						= 0;
	displayer_show_info.HMWGrade 					= 0;
	displayer_show_info.DigitalDisplay 				= 0;
	displayer_show_info.HMWTime 					= 6.3;
	displayer_show_info.LengthwaysRelativeSpeed 	= 128.0;
	displayer_show_info.TTC 						= 6.3;
	displayer_show_info.LengthwaysDistance 			= 204.0;
	displayer_show_info.ObstacleType 				= 0;

//	isAEBTaskClose 									= pdFALSE;	// need to optimized
//	isLDWTaskClose 									= pdFALSE;

	displayer_tx_frame.TargetID 					= DISPLAYER_SHOW_ID;
	displayer_tx_frame.lenth 						= 8;
	displayer_tx_frame.MsgType						= CAN_DATA_FRAME;
	displayer_tx_frame.RmtFrm 						= CAN_FRAME_FORMAT_EFF;
	displayer_tx_frame.RefreshRate 					= DISPLAYER_SHOW_REFRESH_RATE;

	displayer_radar_tx_frame.TargetID 				= DISPLAYER_RADAR_ID;
	displayer_radar_tx_frame.lenth 					= 8;
	displayer_radar_tx_frame.MsgType 				= CAN_DATA_FRAME;
	displayer_radar_tx_frame.RmtFrm 				= CAN_FRAME_FORMAT_EFF;
	displayer_radar_tx_frame.RefreshRate 			= DISPLAYER_RADAR_REFRESH_RATE;

	displayer_tx_write_snpn_frame.TargetID 			= DISPLAYER_WRITE_SNPN_ID;
	displayer_tx_write_snpn_frame.lenth 			= 8;
	displayer_tx_write_snpn_frame.MsgType 			= CAN_DATA_FRAME;
	displayer_tx_write_snpn_frame.RmtFrm 			= CAN_FRAME_FORMAT_EFF;
	displayer_tx_write_snpn_frame.RefreshRate 		= DISPLAYER_ERITE_SNPN_REFRESH_RATE;

	displayer_tx_set_voice_frame.TargetID 			= DISPLAYER_FEEDBACK_VOICE_ID;
	displayer_tx_set_voice_frame.lenth 				= 8;
	displayer_tx_set_voice_frame.MsgType 			= CAN_DATA_FRAME;
	displayer_tx_set_voice_frame.RmtFrm 			= CAN_FRAME_FORMAT_EFF;
	displayer_tx_set_voice_frame.RefreshRate 		= DISPLAYER_FEEDBACK_VOICE_REFRESH_RATE;

	for(int i=0; i<8; i++)
	{
		displayer_tx_frame.data[i] 					= 0xFF;
		displayer_radar_tx_frame.data[i] 			= 0xFF;
	}
	old_Time_ID 									= 0xff;//init
	old_Time_ID_keep_num 							= 0;//init
}

bool Displayer_CANTx_Get(struct can_frame *tx_frame)
{
	Camera_Info_Push(&camera_data, &obstacle_Basic_data, &obstacle_cipv_data);//解析后的数据提交数据中心
	Displayer_Pull(&displayer_show_info);			//更新显示数据。

	displayer_tx_frame.data[0] = 0x00;
	displayer_tx_frame.data[0] |= displayer_show_info.FrontCameraStatus;
	displayer_tx_frame.data[0] |= displayer_show_info.MillimeterWaveRadarStatus << 1;
//	fprintf(USART1_STREAM,"---MM2:%d %d\r\n",displayer_tx_frame.data[0],displayer_show_info.MillimeterWaveRadarStatus );

	displayer_tx_frame.data[0] |= displayer_show_info.UltrasonicRadarStatus << 2;
	displayer_tx_frame.data[0] |= displayer_show_info.ValveStatus << 3;
	displayer_tx_frame.data[0] |= displayer_show_info.VehicleCANBusStatus << 4;
	displayer_tx_frame.data[0] |= displayer_show_info.VehicleSpeedStatus << 5;
	displayer_tx_frame.data[0] |= displayer_show_info.Module4GStatus << 6;
	displayer_tx_frame.data[0] |= displayer_show_info.GPSStatus << 7;

	displayer_tx_frame.data[1] = 0x00;
	displayer_tx_frame.data[1] |= displayer_show_info.AEBSTaskStatus;//0x00;//
	displayer_tx_frame.data[1] |= displayer_show_info.FCWTaskStatus << 2;
	displayer_tx_frame.data[1] |= displayer_show_info.LDWTaskStatus << 4;
	displayer_tx_frame.data[1] |= displayer_show_info.SSSTaskStatus << 6;

	displayer_tx_frame.data[2] = 0x00;
	displayer_tx_frame.data[2] |= 0x03;//HLC:Height Limited
	///////////////////////////////
	//
	///////////////////////////////
	if(old_Time_ID == camera_share.ObsBasicData.TimeID){
		old_Time_ID_keep_num += 1;
		if(old_Time_ID_keep_num > 3){
			displayer_show_info.HMWTime = 6.3;
			displayer_show_info.ObstacleType = 0;
//			g_CMS_hmw_warning = 0x00;
//			g_AEB_CMS_outside_BrakeBy_CMSHMW = 0x00;
//			camera_share.ObsInormation.CIPV = 0x00;
		}
	}
	old_Time_ID = camera_share.ObsBasicData.TimeID;

	//前向碰撞报警
	if((camera_share.ObsInormation.CIPV != 0) && (stVehicleParas.fVehicleSpeed > 0) && (displayer_show_info.FCWTaskStatus == 1)){
		//displayer_tx_frame.data[2] |= displayer_show_info.FCWLevel << 2; //don't use Cam Warning
		//fprintf(USART1_STREAM, "FCWLevel:%d\r\n",displayer_show_info.FCWLevel);
	}
	if(g_AEB_CMS_outside_BrakeBy_AEBTTC == 0x01 || g_AEB_CMS_outside_BrakeBy_CMSTTC == 0x01 || g_AEBS_outside_Test_Brake_Warning == 0x01){
		displayer_tx_frame.data[2] |= 2 << 2;
		//fprintf(USART1_STREAM, "%d   %d   %d displayer_tx_frame.data[2]:%d\r\n",g_AEB_CMS_outside_BrakeBy_AEBTTC,g_AEB_CMS_outside_BrakeBy_CMSTTC,g_AEB_ttc_warning_L2,displayer_tx_frame.data[2]);
	}else if(g_CMS_ttc_warning == 0x01 || g_AEB_ttc_warning_L1 == 0x01 || g_AEB_ttc_warning_L2 == 0x01){
		displayer_tx_frame.data[2] |= 1 << 2;
		//fprintf(USART1_STREAM, "%d %d displayer_tx_frame.data[2]:%d\r\n",g_CMS_ttc_warning,g_AEB_ttc_warning_L1,displayer_tx_frame.data[2]);
	}


	//2022-03-04 test
	if(g_AEB_CMS_outside_dec_output > 0.0){
		//fprintf(USART1_STREAM, "g_AEB_CMS_outside_dec_output:%0.2f\r\n",g_AEB_CMS_outside_dec_output);
	}
	if(stVehicleParas.fVehicleSpeed > 0)
	{
//		if ((displayer_show_info.LDW == 1)//左侧LDW
//			&& (stVehicleParas.LeftFlag == 1))//打左转向，抑制左侧LDW
//		{
//			displayer_show_info.LDW = 3;
//		}
//		if ((displayer_show_info.LDW == 2)//右侧LDW
//			&& (stVehicleParas.RightFlag == 1))//打右转向，抑制右侧LDW
//		{
//			displayer_show_info.LDW = 3;
//		}
		if ((stVehicleParas.LeftFlag == 1) || (stVehicleParas.RightFlag == 1))
		{
			displayer_show_info.LDW = 3;
		}
		displayer_tx_frame.data[2] |= displayer_show_info.LDW << 4;
	}
	else
	{
		displayer_show_info.LDW = 3;
		displayer_tx_frame.data[2] |= displayer_show_info.LDW << 4;
	}

	if(g_LDW_Enable == 0x00 || stCanCommSta.stCamera.status == OFFLINE){
		displayer_tx_frame.data[2] &= 0xCF;
	}
	//gcz 2022-05-19
	uint8_t hmw_level = 0;
	//HMW
	//gcz 2022-05-29	stVehicleParas.Car_Gear的用法不对，目前GPIO或CAN检测到的倒挡状态都是使用stVehicleParas.ReverseGear
	if((stVehicleParas.fVehicleSpeed > 0) && (stVehicleParas.ReverseGear != 1/*stVehicleParas.Car_Gear != 3*/)){

		if(g_AEB_CMS_outside_BrakeBy_CMSHMW == 0x01){
			displayer_tx_frame.data[2] |= 2 << 6; // HMW_Warning Level 2 at the same time brakeing
			displayer_tx_frame.data[2] &= 0xF3; // the level 2 warning > level 1 warning  //add xujie 2022-05-27
			//gcz 2022-05-19
			hmw_level = 2;
			//fprintf(USART1_STREAM,"hmw_brake:1\r\n");
		}else if(g_CMS_hmw_warning == 0x01){
			displayer_tx_frame.data[2] |= 1 << 6; // HMW_Warning Level 1 just warning
			//gcz 2022-05-19
			hmw_level = 1;
			//fprintf(USART1_STREAM,"hmw_warning:1\r\n");
		}else if(displayer_show_info.HMWGrade != 0){
			displayer_tx_frame.data[2] |= 3 << 6; // HMW_Warning Level 1 just warning
		}
	}
	//gcz 2022-05-19
	set_hmw_level(hmw_level);
	displayer_tx_frame.data[3] = 0x00;
	displayer_tx_frame.data[3] |= displayer_show_info.DigitalDisplay;
	if(camera_share.ObsInormation.CIPV == 0)
		displayer_show_info.HMWTime = 6.3;
	else{
		old_Time_ID_keep_num = 0;
	}

	displayer_tx_frame.data[3] |= (uint8_t)(displayer_show_info.HMWTime * 10.0) << 2;

	uint16_t rel_speed = (uint16_t)((displayer_show_info.LengthwaysRelativeSpeed * 4.0) + 127.9375);
	displayer_tx_frame.data[4] = (uint8_t)(rel_speed & 0x00FF);

	displayer_tx_frame.data[5] = 0x00;
	displayer_tx_frame.data[5] |= (uint8_t)((rel_speed & 0x0300) >> 8);

	if(camera_share.ObsInormation.CIPV != 0)
		displayer_tx_frame.data[5] |= (uint8_t)(displayer_show_info.TTC * 10.0) << 2;
	else
		displayer_tx_frame.data[5] |= 0x3f << 2;

	uint16_t rel_distance = (uint16_t)(displayer_show_info.LengthwaysDistance * 10.0);
	displayer_tx_frame.data[6] = (uint8_t)(rel_distance & 0x00FF);

	displayer_tx_frame.data[7] = 0x00;
	displayer_tx_frame.data[7] |= (uint8_t)((rel_distance & 0x0F00) >> 8);

	if(camera_share.ObsInormation.CIPV != 0)
		displayer_tx_frame.data[7] |= displayer_show_info.ObstacleType << 4;

	*tx_frame = displayer_tx_frame;

	return true;
}

bool Displayer_CANTx_Radar_Get(struct can_frame *tx_frame)
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	displayer_radar_tx_frame.data[0] = 0x00;
//	if (m_sensor_mgt.m_ultra_radar.m_isOpen == FUNC_OPEN)
	if(m_sensor_mgt.m_ultra_radar.m_front.m_main_sw == FUNC_OPEN \
		|| m_sensor_mgt.m_ultra_radar.m_end.m_main_sw == FUNC_OPEN \
		|| m_sensor_mgt.m_ultra_radar.m_left.m_main_sw == FUNC_OPEN \
		|| m_sensor_mgt.m_ultra_radar.m_right.m_main_sw == FUNC_OPEN )
	{
		if (((stVehicleParas.BreakUreaderDirection == 1) && (m_sensor_mgt.m_ultra_radar.m_front.m_main_sw == SENSOR_OPEN))
			|| ((stVehicleParas.BreakUreaderDirection == 2) && (m_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
			|| ((stVehicleParas.BreakUreaderDirection == 3) && (m_sensor_mgt.m_ultra_radar.m_end.m_main_sw == SENSOR_OPEN))
			|| ((stVehicleParas.BreakUreaderDirection == 4) && (m_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN)))
		{
			displayer_radar_tx_frame.data[0] |= stVehicleParas.BreakUreaderLevel; //BLISLevel
			displayer_radar_tx_frame.data[0] |= stVehicleParas.BreakUreaderDirection << 2; //BLISType
		}
	}

	SRR_Obstacle_Paras m_srr_para = getSrrWarningAndZoneParas();
	if ((m_sensor_mgt.m_mmw_radar.m_right.m_main_sw == FUNC_OPEN) && (m_sensor_mgt.m_mmw_radar.m_right.m_main_sw == SENSOR_OPEN))
	{
		displayer_radar_tx_frame.data[0] |= m_srr_para.warningLevel;
		displayer_radar_tx_frame.data[0] |= m_srr_para.warningZone << 2;
	}

	displayer_radar_tx_frame.data[1] = 0x00;
	displayer_radar_tx_frame.data[1] |= 0x01;//FrontCameraInsert
	if ((m_sensor_mgt.m_mmw_radar.m_front.m_main_sw == FUNC_OPEN) || (m_sensor_mgt.m_mmw_radar.m_right.m_main_sw == FUNC_OPEN))
	{
		displayer_radar_tx_frame.data[1] |= 0x01 << 1;//MillimeterWaveRadarInsert
	}
	else
		displayer_radar_tx_frame.data[1] |= 0x00 << 1;//MillimeterWaveRadarInsert
	displayer_radar_tx_frame.data[1] |= 0x01 << 2;//UltrasonicRadarInsert
	displayer_radar_tx_frame.data[1] |= 0x01 << 3;//ValveInsert
	//gcz 2055-05-08 修改整车CAN参数配置与小屏显示异常问题
	if(rParms.Vehicle_State_Obtain_Method == 0){
		displayer_radar_tx_frame.data[1] |= 0x01 << 4;//VehicleCANBusInsert
	}else{
		displayer_radar_tx_frame.data[1] |= 0x00 << 4;//VehicleCANBusInsert
	}
	if(rParms.Vehicle_Speed_Obtain_Method == 0x01 && stVehicleParas.fVehicleSpeed <= 0.0){
		displayer_radar_tx_frame.data[1] |= 0x00 << 5;//VehicleSpeedInsert
	}else{
		displayer_radar_tx_frame.data[1] |= 0x01 << 5;//VehicleSpeedInsert
	}

	displayer_radar_tx_frame.data[1] |= 0x01 << 6;//Module4GInsert
	displayer_radar_tx_frame.data[1] |= 0x01 << 7;//GPSInsert

	*tx_frame = displayer_radar_tx_frame;

	return true;
}
bool Displayer_CANTx_WriteSNPN_Get(struct can_frame *tx_frame,uint32_t sn, uint32_t pn)
{
	displayer_tx_write_snpn_frame.data[0] = (uint8_t)(sn & 0xff);
	displayer_tx_write_snpn_frame.data[1] = (uint8_t)((sn >> 8 )& 0xff);
	displayer_tx_write_snpn_frame.data[2] = (uint8_t)((sn >> 16)& 0xff);
	displayer_tx_write_snpn_frame.data[3] = (uint8_t)((sn >> 24)& 0xff);

	displayer_tx_write_snpn_frame.data[4] = (uint8_t)(pn & 0xff);
	displayer_tx_write_snpn_frame.data[5] = (uint8_t)((pn >> 8 )& 0xff);
	displayer_tx_write_snpn_frame.data[6] = (uint8_t)((pn >> 16)& 0xff);
	displayer_tx_write_snpn_frame.data[7] = (uint8_t)((pn >> 24)& 0xff);

	*tx_frame = displayer_tx_write_snpn_frame;
	return true;
}

bool Displayer_CANTx_VoiceNum_Get(struct can_frame *tx_frame,uint8_t voice_num)
{
	displayer_tx_set_voice_frame.data[0] = voice_num;
	*tx_frame = displayer_tx_set_voice_frame;
	return true;
}


void Led_Control(void)
{
	static uint32_t led_clk = 0;
	if(SystemtimeClock - led_clk < 100) return ;
	led_clk = SystemtimeClock;

	Set_LED7(stCanCommSta.stRadar.status);				// 毫米波雷达
	Set_LED6(stCanCommSta.Proportional_valve.status);	// 执行-AEB刹车
	Set_LED5(stCanCommSta.stVehicle.status);			// 整车CAN
	Set_LED4(stCanCommSta.stCamera.status);				// 相机
	Set_LED3(1);										// 工作电源
	Set_LED2(stCanCommSta.stWireless.status);			// 4G指示灯
	Set_LED1(stCanCommSta.stHRadar.status);				// 超声波雷达
}


bool Displayer_CANTx_AngleRadar_Get(struct can_frame *tx_frame)
{
	displayer_radar_tx_frame.data[0] = 0x00;
	displayer_radar_tx_frame.data[0] |= stVehicleParas.BreakUreaderLevel; //BLISLevel
  //displayer_radar_tx_frame.data[0] |= 1; //BLISLevel
	displayer_radar_tx_frame.data[0] |= stVehicleParas.BreakUreaderDirection << 2; //BLISType

	displayer_radar_tx_frame.data[1] = 0x00;
	displayer_radar_tx_frame.data[1] |= 0x01;//FrontCameraInsert
	displayer_radar_tx_frame.data[1] |= 0x00 << 1;//MillimeterWaveRadarInsert
	displayer_radar_tx_frame.data[1] |= 0x01 << 2;//UltrasonicRadarInsert
	displayer_radar_tx_frame.data[1] |= 0x01 << 3;//ValveInsert
	//gcz 2055-05-08 修改整车CAN参数配置与小屏显示异常问题
	if(rParms.Vehicle_State_Obtain_Method == 0){
		displayer_radar_tx_frame.data[1] |= 0x01 << 4;//VehicleCANBusInsert
	}else{
		displayer_radar_tx_frame.data[1] |= 0x00 << 4;//VehicleCANBusInsert
	}
	if(rParms.Vehicle_Speed_Obtain_Method == 0x01 && stVehicleParas.fVehicleSpeed <= 0.0)
	{
		displayer_radar_tx_frame.data[1] |= 0x00 << 5;//VehicleSpeedInsert
	}else{
		displayer_radar_tx_frame.data[1] |= 0x01 << 5;//VehicleSpeedInsert
	}

	displayer_radar_tx_frame.data[1] |= 0x01 << 6;//Module4GInsert
	displayer_radar_tx_frame.data[1] |= 0x01 << 7;//GPSInsert

	*tx_frame = displayer_radar_tx_frame;

	return true;
}
