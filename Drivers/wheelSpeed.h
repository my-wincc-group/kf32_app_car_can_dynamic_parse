/*
 * wheelSpeed.h
 *
 *  Created on: 2021-11-30
 *      Author: Administrator
 */

#ifndef WHEELSPEED_H_
#define WHEELSPEED_H_
#include "system_init.h"

void 		WheelSpeed_Init();
uint16_t 	Calc_Bus_Encoder_Velocity();
uint16_t 	Get_Wheel_Speed();
//获取车速
float get_cur_vehicle_speed();
//初始化速度缓存
void InitSpeedOptCache();
//应用层过滤车速
uint32_t SpeedApplicationLayerFilter(uint32_t VehicleSpeed,uint32_t coe);
#endif /* WHEELSPEED_H_ */
