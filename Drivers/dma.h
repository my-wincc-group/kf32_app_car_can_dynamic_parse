/*
 * dma.h
 *
 *  Created on: 2022-7-13
 *      Author: Administrator
 */

#ifndef DMA_H_
#define DMA_H_
#include "system_init.h"
#include "spi.h"
/******************** 宏/结构体/枚举  ************************/
#define DMA_BUFF_SIZE	8202
//#define _SPI_DMA_W25Q16		// 仍需要调试

#ifdef _SPI_DMA_W25Q16
	#define SPI_BUF_SIZE	4096
#endif
// lyj 2022-6-10
#define USART0_TXDMA_CHAN 		DMA_CHANNEL_1
#define USART0_RXDMA_CHAN 		DMA_CHANNEL_2
#define USART1_TXDMA_CHAN 		DMA_CHANNEL_3
#define USART1_RXDMA_CHAN 		DMA_CHANNEL_4

#ifdef _SPI_DMA_W25Q16
	#define SPI2_TXDMA_CHAN      	DMA_CHANNEL_5
	#define SPI2_RXDMA_CHAN    	 	DMA_CHANNEL_6
#endif
//#define  SPI_MASTER 			1   //SPI模式选择，1=主模式

typedef struct _USARTX_DMA{
	volatile uint32_t dma_recv_front_addr;	// 最新地址
	volatile uint32_t dma_recv_end_addr;	// 旧地址，旧地址要追赶新地址
	volatile int32_t  recv_data_len;
	volatile uint8_t  recv_idle_flag;		// 1空闲；0非空闲
}USARTX_DMA;

/******************** 全局变量 ************************/
extern uint8_t USART1_Dma_Buf_Rev[DMA_BUFF_SIZE];
extern uint8_t USART0_Dma_Buf_Rev[DMA_BUFF_SIZE];
extern USARTX_DMA usart0_dma;
extern USARTX_DMA usart1_dma;

#ifdef _SPI_DMA_W25Q16
	//extern uint8_t SPI_Send_Buf[SPI_BUF_SIZE];
	extern uint8_t SPI_Recv_Buf[SPI_BUF_SIZE]; //读数据缓存
	extern USARTX_DMA spi_dma_w25q16;
#endif
/******************** 函数声明 ************************/
extern void		DMA_USART1_Init();
extern void		DMA_USART0_Init();
extern void 	Enable_DMA_USART0(FunctionalState state);
extern void 	Enable_DMA_USART1(FunctionalState state);
extern void 	Analysis_DMA_USART0_Data();
extern void 	Analysis_DMA_USART1_Data();
extern void 	Usart0_DMA_Transmit(volatile uint8_t *pData, uint16_t nSize);
extern void 	Usart1_DMA_Transmit(volatile uint8_t *pData, uint16_t nSize);

#ifdef _SPI_DMA_W25Q16
	extern void Enable_SPI2_DMA(FunctionalState state);
	extern void SPI2_DMA_Configuration(SPI_SFRmap* SPIx);
	extern void SPI2_DMA_Transmit(uint8_t *data, uint16_t data_len, uint8_t flag);
	extern void From_SPI2_DMA_Buf_Get_Data(uint8_t *get_data, uint16_t get_data_len, uint16_t diff);
#endif
#endif /* DMA_H_ */
