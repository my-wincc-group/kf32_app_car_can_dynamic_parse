/*
 * SSS_alg.c
 *
 *  Created on: 2021-11-26
 *      Author: zkhy
 */
#include "sss_alg.h"
#include "circular_queue.h"
#include "SSS_alg.h"

#define speed_meter_p_s(v)            	(v / 3.6)
#define sss_break_calculate_cancel      0x00
#define sss_break_calculate_continue    0x01
#define sss_history_size   				3
#define sss_warning   	 				0x01
#define sss_no_warning   				0x00

SSS_parameters sss_param;
SSS_state sss_state;
SSS_uradar_history uradar_history;

float 		dec_out_sss 			= 0.0f;
uint8_t	 	g_SSS_warning 			= 0x00;
uint8_t 	g_SSS_outside_isBrake 	= 0x00;
uint32_t 	starting_time_stamp 	= 0x00;

//extern URADER_MESSAGE	Urader_Company_Message[2];
//extern uint8_t g_SSS_Enable; // Update in "AEB_CMS_alg.c"



void sss_init(void){
	sss_param.break_ttc_thr = 1.5;
}

float calculate_cos(float angle){
	return -0.0001391*angle*angle - 0.0003124*angle + 1.001;
}

float calculate_sin(float angle){
	return angle > 0.0? 0.0159*angle : 0.0;
}

uint8_t Approaching_all_location(uint8_t i, uint8_t j, float distance){

	return SSS_Approaching_to_target(sss_history_size, distance, uradar_history.sss_uradar_history_distance[i][j], uradar_history.sss_uradar_history_distance_timestamp[i][j],&(uradar_history.sss_uradar_history_distance_count[i][j]));

}

uint8_t Starting_Or_Not(float speed){
	//fprintf(USART1_STREAM,"speed:%0.2f\r\n",speed);
	if(speed <= 0){
		starting_time_stamp = SystemtimeClock;
		return 0x00; // not starting
	}else{
		if(((SystemtimeClock - starting_time_stamp)/1000) < 5.0){
			//Send_Display_Message_VoiveNum(38);// di di di di di
			return 0x01; // starting
		}
		else
			return 0x00; // not starting
	}
}
static uint8_t braking_keep;
static uint8_t braking_keep_count;
static uint8_t braking_direction;
static uint8_t ult_id;
float SSS_Break_control(_VEHICLE_PARA* stVehicleParas, URADER_MESSAGE Urader_Company_Message[]){

	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
//	if (m_sensor_mgt.m_ultra_radar.m_isOpen == FUNC_OPEN)
	if(m_sensor_mgt.m_ultra_radar.m_front.m_main_sw == FUNC_OPEN \
		|| m_sensor_mgt.m_ultra_radar.m_end.m_main_sw == FUNC_OPEN \
		|| m_sensor_mgt.m_ultra_radar.m_left.m_main_sw == FUNC_OPEN \
		|| m_sensor_mgt.m_ultra_radar.m_right.m_main_sw == FUNC_OPEN )
	{
	//	stVehicleParas->fVehicleSpeed = 10;
		uint8_t break_or_not = SSS_Break_Trigger(*stVehicleParas, g_SSS_Enable);
		if(break_or_not == sss_break_calculate_cancel){
			//fprintf(USART1_STREAM,"SSS_Break_Trigger\r\n");
			stVehicleParas->BreakUreaderDirection = 0x00;
			stVehicleParas->BreakUreaderLevel = 0x00;
			braking_keep = 0;
			return 0.0;
		}
		// calculate decoutput
		if(!Starting_Or_Not((*stVehicleParas).fVehicleSpeed)){
			//dec_out_sss = Get_Uradar_break_pressure_notSSS(Urader_Company_Message, stVehicleParas);
		}
		else{
			//dec_out_sss = Get_Uradar_break_pressure_distance(Urader_Company_Message, stVehicleParas);
		}

		switch(server_info.vehicle_type){
			case JINLV_BUS:
				dec_out_sss = Get_Uradar_break_pressure_notSSS(Urader_Company_Message, (*stVehicleParas),&braking_direction,&ult_id,m_sensor_mgt,&stVehicleParas->Ultrasonicdistance);
				break;
			case BYD_E6:
				dec_out_sss = Get_Uradar_break_dec_BYD(Urader_Company_Message, (*stVehicleParas),&braking_direction,&ult_id,m_sensor_mgt,&stVehicleParas->Ultrasonicdistance);
				break;
			case JIEFANG_TRUCK:
				dec_out_sss = Get_Uradar_break_pressure_notSSS(Urader_Company_Message, (*stVehicleParas),&braking_direction,&ult_id,m_sensor_mgt,&stVehicleParas->Ultrasonicdistance);
				break;
			default:
				dec_out_sss = 0.0;
				break;
		}


		// break_cancel
//		if(dec_out_sss > 0){
//			fprintf(USART1_STREAM,"Brake_SSS\r\n");
//		}
		break_or_not = SSS_driver_control((*stVehicleParas));
		if(break_or_not == sss_break_calculate_cancel){
			//fprintf(USART1_STREAM,"SSS_driver_control\r\n");
			dec_out_sss = 0.0;
		}
		sss_state.dec_output = dec_out_sss;
		if(dec_out_sss > 0){
			//Send_Display_Message_VoiveNum(39);// di di di di di
			stVehicleParas->BreakUreaderDirection = braking_direction;
			stVehicleParas->BreakUreaderLevel = 0x01;

			stVehicleParas->ult_warning_id = ult_id + 1;
		//	fprintf(USART1_STREAM,"Brake_SSS\r\n");
		}
		else{
			stVehicleParas->Ultrasonicdistance = 0;
			stVehicleParas->BreakUreaderDirection = 0x00;
			stVehicleParas->BreakUreaderLevel = 0x00;
			Get_Uradar_Warning_Info(Urader_Company_Message, stVehicleParas, 0.4,0.6);
			ult_id = 0;
			stVehicleParas->ult_warning_id = 0;
		}
	//fprintf(USART1_STREAM,"Get_Uradar_Warning_Info\r\n");
#ifdef not_stop
	if(dec_out_sss > 0){
		braking_keep = 1;
		braking_keep_count = 0;
	}
	else if(dec_out_sss == 0.0 && stVehicleParas.fVehicleSpeed == 0){
		braking_keep = 0;
		braking_keep_count = 0;
	}else if(braking_keep_count > 5){
		braking_keep = 0;
		braking_keep_count = 0;
	}else if(braking_keep_count < 5){
		braking_keep = 1;
		braking_keep_count += 1;
	}

	if(braking_keep == 1){
		sss_state.dec_output = rParms.SSS_Brake_Force;
		//fprintf(USART1_STREAM,"Brake_SSS_keep\r\n");
	}
#endif
	}
	else
		sss_state.dec_output = 0.0;
	//yuhong 20220819 双闪取消超声波制动
    if((stVehicleParas->LeftFlag ==1)&&(stVehicleParas->RightFlag == 1))
    {
    	sss_state.dec_output = 0.0;
    }

	return sss_state.dec_output;
}
uint8_t SSS_Break_Trigger(_VEHICLE_PARA stVehicleParas, uint8_t switch_on_off){
	//fprintf(USART1_STREAM,"switch_off=%d\r\n",switch_on_off);
	// Judge if Starting
	if(!Starting_Or_Not(stVehicleParas.fVehicleSpeed)){
		//fprintf(USART1_STREAM,"Starting_Not\r\n");
		//return sss_break_calculate_cancel;
	}
	else{
		//fprintf(USART1_STREAM,"Starting_Yes\r\n");
	}
	if(switch_on_off == 0x00){
		//fprintf(USART1_STREAM,"switch_off\r\n");
		return sss_break_calculate_cancel;
	}
	if((stVehicleParas.fVehicleSpeed <= 0)||(stVehicleParas.fVehicleSpeed > rParms.SSS_Max_Enable_Speed)){
		//fprintf(USART1_STREAM,"fVehicleSpeed=%0.2f,SSS_Max_Enable_Speed = %0.2f\r\n",stVehicleParas.fVehicleSpeed,rParms.SSS_Max_Enable_Speed);
		return sss_break_calculate_cancel;
	}
	if(stVehicleParas.ReverseGear == 1 ){
		//fprintf(USART1_STREAM,"ReverseGear\r\n");
		return sss_break_calculate_cancel;
	}

	return sss_break_calculate_continue;
}
uint8_t SSS_driver_control(_VEHICLE_PARA stVehicleParas){
	if(stVehicleParas.BrakeFlag == 1 ){
		//return sss_break_calculate_cancel;
	}
	return sss_break_calculate_continue;
}
uint8_t SSS_Approaching_to_target(uint8_t size_of_history, float distance, float distance_resived[], uint32_t distance_timestamp[],uint8_t* num_count){
	uint8_t double_size = (size_of_history*2 - 1);
	// recored the history data
	if(*num_count < size_of_history){
		distance_resived[*num_count] = distance;
		if(distance_timestamp != NULL)
			distance_timestamp[*num_count] = SystemtimeClock;
		*num_count = *num_count + 1;
	}
	else{
		distance_resived[*num_count - size_of_history] = distance;
		if(distance_timestamp != NULL)
			distance_timestamp[*num_count] = SystemtimeClock;
		*num_count = *num_count + 1;
		if(*num_count > double_size){
			*num_count = size_of_history;
		}
	}
	// judge approaching or not
	if(*num_count >= size_of_history){
		uint8_t now_pointer = *num_count - 1;
		if(now_pointer < size_of_history)
			now_pointer = double_size;

		uint8_t middle_pointer = now_pointer - 1;
		if(middle_pointer < size_of_history)
			middle_pointer = double_size;
		middle_pointer = middle_pointer - 1;
		if(middle_pointer < size_of_history)
			middle_pointer = double_size;

		uint8_t old_pointer = *num_count;
		if((distance_resived[now_pointer - size_of_history] < distance_resived[old_pointer - size_of_history])){
			return 0x01;// approaching
		}
		else{
			return 0x01;//not approaching
		}

		/*if((distance_resived[now_pointer - size_of_history] < distance_resived[middle_pointer - size_of_history])&(distance_resived[middle_pointer - size_of_history] < distance_resived[old_pointer - size_of_history])){
			return 0x01;// approaching
		}
		else if((distance_resived[now_pointer - size_of_history] > distance_resived[old_pointer - size_of_history])&(distance_resived[middle_pointer - size_of_history] > distance_resived[old_pointer - size_of_history])){
			return 0x00;// not approaching
		}
		else {
			if((distance_resived[now_pointer - size_of_history] < distance_resived[middle_pointer - size_of_history])){
				return 0x01;// approaching
			}
			else if((distance_resived[now_pointer - size_of_history] > distance_resived[old_pointer - size_of_history])){
				return 0x00;// not approaching
			}
			else{
				return 0x02; // can not judge
			}
		}*/
	}
	else{
		return 0x02; // can not judge
	}
}

float sss_cal_ttc(uint8_t size_of_history, float distance_resived[], uint32_t distance_timestamp[], uint8_t num_count){
	uint8_t double_size = size_of_history * 2 - 1;
	float invalid_value = 10.0;
	if(num_count >= size_of_history){
		uint8_t now_pointer = num_count - 1;
		if(now_pointer < size_of_history)
			now_pointer = double_size;

		uint8_t old_pointer = num_count;
		float deta_d = distance_resived[old_pointer - size_of_history] - distance_resived[now_pointer - size_of_history];
		if(deta_d > 0.0){
			float deta_t = (distance_timestamp[now_pointer - size_of_history] - distance_timestamp[old_pointer - size_of_history])/1000;
			if(deta_t > 0.0){
				float deta_v = deta_d / deta_t;
				float ttc = distance_resived[now_pointer - size_of_history] / deta_v;
				if(ttc > invalid_value)
					ttc = invalid_value;
				return ttc;
			}
			else{
				return invalid_value;
			}
		}
		else{
			return invalid_value;
		}
	}
	else{
		return invalid_value; // can not judge
	}
}
float Get_Uradar_break_pressure_ttc(URADER_MESSAGE urader_company_message[], _VEHICLE_PARA stVehicleParas)
{
	float udistance = 0;
	uint8_t double_size = sss_history_size*2 - 1;
	for(uint8_t i = 0;i < 2;i ++)
	{
		for(uint8_t j = 0;j < 12;j ++)
		{
			if(urader_company_message[i].Urader_work_stata[j] == URWORK)
			{
				udistance = urader_company_message[i].distance[j];
				if(udistance > rParms.SSS_Warning_Enable_Distance){
					sss_state.warning_output = 0x00;
					uradar_history.sss_uradar_history_distance_count[i][j] = 0;
					continue;
				}
				else{
					if(udistance > rParms.SSS_Break_Enable_Distance){
						sss_state.warning_output = 0x00;
						continue;
					}
					//warning and jude if break
					sss_state.warning_output = 0x01;

					uint8_t p = uradar_history.sss_uradar_history_distance_count[i][j];
					if(p > sss_history_size){
						p = p - 1;
						if(p < sss_history_size)
							p = double_size;
						p = p - sss_history_size;
					}
					 if(uradar_history.sss_uradar_history_distance[i][j][p] == udistance){
						 continue;
					 }
					 else{
						 if(uradar_history.sss_uradar_location[i][j] != urader_company_message[i].Uposition[j]){
							 uradar_history.sss_uradar_location[i][j] = urader_company_message[i].Uposition[j];
						 }
						 uint8_t get_back = SSS_Approaching_to_target(sss_history_size, udistance, uradar_history.sss_uradar_history_distance[i][j], uradar_history.sss_uradar_history_distance_timestamp[i][j], &uradar_history.sss_uradar_history_distance_count[i][j]);
						 if(get_back == 1){
							 float ttc = sss_cal_ttc(sss_history_size, uradar_history.sss_uradar_history_distance[i][j], uradar_history.sss_uradar_history_distance_timestamp[i][j], uradar_history.sss_uradar_history_distance_count[i][j]);
							 if((ttc > 0.0)&(ttc <= sss_param.break_ttc_thr)){
								 float dec_out = udistance / ttc;
								 return  dec_out;
							 }
						 }
					 }
				}
			}
		}
	}

	return 0.0;
}

uint8_t approaching_or_not  = 0x00;
float distance_fixed 		= 0.0f;
float speed_ego      		= 0.0f;
float break_distance 		= 0.0f;
float Get_Uradar_break_pressure_distance(URADER_MESSAGE urader_company_message[], _VEHICLE_PARA stVehicleParas){
	distance_fixed;
	speed_ego = speed_meter_p_s(stVehicleParas.fVehicleSpeed);
	break_distance = 0.5*speed_ego*speed_ego/(rParms.SSS_Brake_Force*0.5);//defult:the air "pressure" = 0.5*deceleration
	break_distance += speed_ego * rParms.Air_Brake_Delay_Time;
	break_distance += rParms.SSS_Stop_Distance;
	dec_out_sss = 0.0;
	approaching_or_not = 0x01;
	//Fo rtest
	//stVehicleParas.RightFlagTemp = 1;*
	//fprintf(USART1_STREAM,"SSS \r\n");

	for(uint8_t i = 0;i < 2;i ++)
	{
		for(uint8_t j = 0;j < 12;j ++)
		{
			if(urader_company_message[i].Urader_work_stata[j] == URWORK)
			{
				//fprintf(USART1_STREAM,"stVehicleParas.LeftFlagTemp = %d,stVehicleParas.RightFlagTemp=%d \r\n",stVehicleParas.LeftFlagTemp,stVehicleParas.RightFlagTemp);
				//1. go stright
				if((stVehicleParas.LeftFlag == 0)&(stVehicleParas.RightFlag == 0)){
					if(FRONT == urader_company_message[i].Uposition[j] ||
					   FRMIDE == urader_company_message[i].Uposition[j] ||
					   FLMIDE == urader_company_message[i].Uposition[j]){
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,speed=%0.2f,dis_fixed=%0.2f,dis=%0.2f\r\n",approaching_or_not,speed_ego*3.6,distance_fixed,urader_company_message[i].distance[j]);
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if(FRSIDE == urader_company_message[i].Uposition[j]){
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,speed=%0.2f,dis_fixed=%0.2f,dis=%0.2f\r\n",approaching_or_not,speed_ego*3.6,distance_fixed,urader_company_message[i].distance[j]);
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance)||(distance_fixed < rParms.SSS_Stop_Distance)){
									//dec_out_sss = rParms.SSS_Brake_Force;
									//return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if(FLEFT == urader_company_message[i].Uposition[j] ||
					   FRIGHT == urader_company_message[i].Uposition[j])
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < rParms.SSS_FR_FL_Install_Distance_To_Side)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}
					}
				}//2. turn right
				else if((stVehicleParas.LeftFlag == 0)&(stVehicleParas.RightFlag == 1))
				{
					float component_cos_turn = calculate_cos(rParms.SSS_Default_Turn_Angle);
					if(FRONT == urader_company_message[i].Uposition[j] ||
					   FRIGHT == urader_company_message[i].Uposition[j] ||
					   FRMIDE == urader_company_message[i].Uposition[j] ||
					   FLMIDE == urader_company_message[i].Uposition[j] )
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance * component_cos_turn)||(distance_fixed <= rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}

					}
					if(FRSIDE == urader_company_message[i].Uposition[j])
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								//fprintf(USART1_STREAM,"speed=%0.2f,distance_fixed=%0.2f,distance=%0.2f,break_distance=%0.2f\r\n",speed_ego*3.6,distance_fixed,urader_company_message[i].distance[j],break_distance);
								if((distance_fixed < break_distance)||(distance_fixed <= rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if(RIGHT == urader_company_message[i].Uposition[j])
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if(FLEFT == urader_company_message[i].Uposition[j])
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < rParms.SSS_FR_FL_Install_Distance_To_Side)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}
					}
				}//3. turn left
				else if((stVehicleParas.LeftFlag == 1)&(stVehicleParas.RightFlag == 0)){
					float component_cos_turn = calculate_cos(rParms.SSS_Default_Turn_Angle);
					if(FRONT == urader_company_message[i].Uposition[j] ||
					   FLEFT == urader_company_message[i].Uposition[j] ||
					   FRMIDE == urader_company_message[i].Uposition[j] ||
					   FLMIDE == urader_company_message[i].Uposition[j] )
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance * component_cos_turn)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}

					}
					if(FLSIDE == urader_company_message[i].Uposition[j])
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if(LEFT == urader_company_message[i].Uposition[j])
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if(FRIGHT == urader_company_message[i].Uposition[j])
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < rParms.SSS_FR_FL_Install_Distance_To_Side)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									return rParms.SSS_Brake_Force;
								}
							}
						}
					}
				}
			}
			if(dec_out_sss > 0.0){
				return dec_out_sss;
			}
		}
	}
	return dec_out_sss;
}
//gcz 2022-08-02 添加函数的配置参数引入
float break_distance_stright		= 0.0f;
float break_distance_turn_right		= 0.5f;
float Get_Uradar_break_pressure_notSSS(URADER_MESSAGE urader_company_message[],
										_VEHICLE_PARA stVehicleParas,
										uint8_t *direction,
										uint8_t *p_ult_id,
										Sensor_MGT st_sensor_mgt,
										float *distance)
{
//	stVehicleParas.fVehicleSpeed = 10.0;
	distance_fixed;
	speed_ego = speed_meter_p_s(stVehicleParas.fVehicleSpeed);
	break_distance_stright = stVehicleParas.fVehicleSpeed*(2.0-0.8)/(rParms.SSS_Max_Enable_Speed - 1.0) + (0.5 - (2.0-0.8)/(rParms.SSS_Max_Enable_Speed - 1.0));//defult:the air "pressure" = 0.5*deceleration
	break_distance_turn_right =0.5;
	dec_out_sss = 0.0;
	approaching_or_not = 0x01;
	//Fo rtest
	//stVehicleParas.RightFlagTemp = 1;*
	//fprintf(USART1_STREAM,"SSS \r\n");
	(*direction) = 0x00;//init
	//fprintf(USART1_STREAM,"stVehicleParas.LeftFlagTemp=%d,stVehicleParas.RightFlagTemp =%d\r\n",stVehicleParas.LeftFlagTemp,stVehicleParas.RightFlagTemp);
	for(uint8_t i = 0;i < 2;i ++)
	{
		for(uint8_t j = 0;j < 12;j ++)
		{
			if(urader_company_message[i].Urader_work_stata[j] == URWORK)
			{
				//fprintf(USART1_STREAM,"stVehicleParas.LeftFlagTemp = %d,stVehicleParas.RightFlagTemp=%d \r\n",stVehicleParas.LeftFlagTemp,stVehicleParas.RightFlagTemp);
				//1. go stright
				if((stVehicleParas.LeftFlag == 0)&&(stVehicleParas.RightFlag == 0))
				{
			//		fprintf(USART1_STREAM,"---FrontK=%d\r\n",st_sensor_mgt.m_ultra_radar.m_front.m_isOpen);

			//		fprintf(USART1_STREAM,"--[%d-%d] %d\r\n",i,j,urader_company_message[i].Uposition[j]);
					if (((FRONT == urader_company_message[i].Uposition[j]) ||
					   (FRMIDE == urader_company_message[i].Uposition[j]) ||
					   (FLMIDE == urader_company_message[i].Uposition[j]))
					   && (st_sensor_mgt.m_ultra_radar.m_front.m_main_sw == SENSOR_OPEN))
					{
					//	fprintf(USART1_STREAM,"--11111-dec_out_sss=%d\r\n",dec_out_sss);
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,speed=%0.2f,dis_fixed=%0.2f,dis=%0.2f\r\n",approaching_or_not,speed_ego*3.6,distance_fixed,urader_company_message[i].distance[j]);
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
							//	fprintf(USART1_STREAM,"--222222222-dec_out_sss=%d\r\n",dec_out_sss);

//								if (distance_fixed < 1.0)
//									fprintf(USART1_STREAM,"--AAAA-DF=%0.6f,DF=%0.6f,PARA=%0.6f\r\n",distance_fixed,break_distance_stright,rParms.SSS_Stop_Distance);
								if((distance_fixed < break_distance_stright)||(distance_fixed < rParms.SSS_Stop_Distance)){
						//			fprintf(USART1_STREAM,"--333333333-dec_out_sss=%d\r\n",dec_out_sss);
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x01;//Front
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
					if((FRSIDE == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,speed=%0.2f,dis_fixed=%0.2f,dis=%0.2f\r\n",approaching_or_not,speed_ego*3.6,distance_fixed,urader_company_message[i].distance[j]);
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance)||(distance_fixed < rParms.SSS_Stop_Distance)){
									//(*direction) = 0x06;//FrontRight
									//dec_out_sss = rParms.SSS_Brake_Force;
									//return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if (((FLEFT == urader_company_message[i].Uposition[j] ||
					   FRIGHT == urader_company_message[i].Uposition[j]))
						&& (st_sensor_mgt.m_ultra_radar.m_front.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance_stright)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									if(FRIGHT == urader_company_message[i].Uposition[j])
										(*direction) = 0x01;//Right
									if(FLEFT == urader_company_message[i].Uposition[j])
										(*direction) = 0x01;//Left
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
					if ((RIGHT == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= break_distance_turn_right){
							if(approaching_or_not == 0x01){
								//dec_out_sss = rParms.SSS_Brake_Force;
								//break;
								//(*direction) = 0x02;//Right
								//return rParms.SSS_Brake_Force;
							}
						}
					}
					if ((LEFT == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= break_distance_turn_right){
							if(approaching_or_not == 0x01){
								//dec_out_sss = rParms.SSS_Brake_Force;
								//break;
								//(*direction) = 0x04;//Left
								//return rParms.SSS_Brake_Force;
							}
						}
					}
				}//2. turn right
				else if((stVehicleParas.LeftFlag == 0)&&(stVehicleParas.RightFlag == 1))
				{
					float component_cos_turn = calculate_cos(rParms.SSS_Default_Turn_Angle);
					if (((FRONT == urader_company_message[i].Uposition[j]) ||
					   (FRIGHT == urader_company_message[i].Uposition[j]) ||
					   (FRMIDE == urader_company_message[i].Uposition[j]) ||
					   (FLMIDE == urader_company_message[i].Uposition[j]) ||
					   (FLEFT == urader_company_message[i].Uposition[j]))
						&& (st_sensor_mgt.m_ultra_radar.m_front.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= break_distance_turn_right){
							if(approaching_or_not == 0x01){
								dec_out_sss = rParms.SSS_Brake_Force;
								//break;
								(*direction) = 0x01;//Front
								*p_ult_id = i * 2 + j;
								*distance = urader_company_message[i].distance[j];
								return rParms.SSS_Brake_Force;
							}
						}

					}
					if ((FRSIDE == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{

						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= break_distance_turn_right){
							if(approaching_or_not == 0x01){
								dec_out_sss = rParms.SSS_Brake_Force;
								//break;
								(*direction) = 0x06;//FrontRight
								*p_ult_id = i * 2 + j;
								*distance = urader_company_message[i].distance[j];
								return rParms.SSS_Brake_Force;
							}
						}
					}
					if ((RIGHT == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= break_distance_turn_right){
							if(approaching_or_not == 0x01){
								dec_out_sss = rParms.SSS_Brake_Force;
								//break;
								(*direction) = 0x02;//Right
								*p_ult_id = i * 2 + j;
								*distance = urader_company_message[i].distance[j];
								return rParms.SSS_Brake_Force;
							}
						}
					}
					if ((LEFT == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= break_distance_turn_right){
							if(approaching_or_not == 0x01){
								dec_out_sss = rParms.SSS_Brake_Force;
								//break;
								(*direction) = 0x04;//Left
								*p_ult_id = i * 2 + j;
								*distance = urader_company_message[i].distance[j];
								return rParms.SSS_Brake_Force;
							}
						}
					}

				}//3. turn left
				else if((stVehicleParas.LeftFlag == 1) && (stVehicleParas.RightFlag == 0)){
					float component_cos_turn = calculate_cos(rParms.SSS_Default_Turn_Angle);
					if(((FRONT == urader_company_message[i].Uposition[j]) ||
					   (FLEFT == urader_company_message[i].Uposition[j]) ||
					   (FRMIDE == urader_company_message[i].Uposition[j]) ||
					   (FLMIDE == urader_company_message[i].Uposition[j]))
					   && (st_sensor_mgt.m_ultra_radar.m_front.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance * component_cos_turn)||(distance_fixed < rParms.SSS_Stop_Distance)){
									dec_out_sss = rParms.SSS_Brake_Force;
						//			fprintf(USART1_STREAM,"--333333333-dec_out_sss=%f\r\n",dec_out_sss);
									//break;
									(*direction) = 0x01;//Front
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if ((FLSIDE == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance)||(distance_fixed < rParms.SSS_Stop_Distance)){
									//dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									//(*direction) = 0x06;//RightFront
									//return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if ((LEFT == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < break_distance)||(distance_fixed < rParms.SSS_Stop_Distance)){
									//dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									//(*direction) = 0x04;//Left
									//return rParms.SSS_Brake_Force;
								}
							}
						}
					}
					if ((FRIGHT == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_front.m_main_sw == SENSOR_OPEN))
					{
						distance_fixed = urader_company_message[i].distance[j] - speed_ego * rParms.Air_Brake_Delay_Time;
						//approaching_or_not = Approaching_all_location(i, j, urader_company_message[i].distance[j]);
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							if(approaching_or_not == 0x01){
								if((distance_fixed < rParms.SSS_FR_FL_Install_Distance_To_Side)||(distance_fixed < rParms.SSS_Stop_Distance)){
									//dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									//(*direction) = 0x05;//LeftFront
									//return rParms.SSS_Brake_Force;
								}
							}
						}
					}
				}
				//4.双闪，前向2颗预警
				else if((stVehicleParas.LeftFlag == 1)&(stVehicleParas.RightFlag == 1))
				{
					if(FRONT == urader_company_message[i].Uposition[j] ||
					   FRMIDE == urader_company_message[i].Uposition[j] ||
					   FLMIDE == urader_company_message[i].Uposition[j])
					{
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed < rParms.SSS_Warning_Enable_Distance){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
					}
				}
			}
			if(dec_out_sss > 0.0){
				return dec_out_sss;
			}
		}
	}
	return dec_out_sss;
}

static float Base_Distance_Brake_BYD = 0.4f;
float Get_Uradar_break_dec_BYD(URADER_MESSAGE urader_company_message[],
								_VEHICLE_PARA stVehicleParas,
								uint8_t *direction,uint8_t *p_ult_id,
								Sensor_MGT st_sensor_mgt,
								float *distance)
{
	(*direction) = 0x00;//init
	//fprintf(USART1_STREAM,"stVehicleParas.LeftFlagTemp=%d,stVehicleParas.RightFlagTemp =%d\r\n",stVehicleParas.LeftFlagTemp,stVehicleParas.RightFlagTemp);
	float brake_distance_dynamic = Base_Distance_Brake_BYD * 0.5;
	for(uint8_t i = 0;i < 2;i ++)
	{
		for(uint8_t j = 0;j < 12;j ++)
		{
			if(urader_company_message[i].Urader_work_stata[j] == URWORK)
			{
				//fprintf(USART1_STREAM,"stVehicleParas.LeftFlagTemp = %d,stVehicleParas.RightFlagTemp=%d \r\n",stVehicleParas.LeftFlagTemp,stVehicleParas.RightFlagTemp);
				//1. turn angle 0-60
				if((stVehicleParas.steer_wheel_angle < 60.0)&&(stVehicleParas.steer_wheel_angle > -60.0)){
				}//2. turn angle 60~200
				else if((stVehicleParas.steer_wheel_angle < 200.0) && (stVehicleParas.steer_wheel_angle >= 60.0)){
					if ((FRSIDE == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 0.8;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x01;//Front
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
					else if ((RIGHT == urader_company_message[i].Uposition[j])
							&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 0.5;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x02;//right
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
				}//3. turn angle 200~480
				else if((stVehicleParas.steer_wheel_angle >= 200.0) && (stVehicleParas.steer_wheel_angle < 480.0))
				{
					if ((FRSIDE == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 1.0;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x01;//Front
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
					else if ((RIGHT == urader_company_message[i].Uposition[j])
							&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 0.8;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x02;//right
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}

				}//4. turn angle 480~max
				else if((stVehicleParas.steer_wheel_angle >= 480.0)){
					if ((FRSIDE == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 1.0;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x01;//Front
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
					else if ((RIGHT == urader_company_message[i].Uposition[j])
							&& (st_sensor_mgt.m_ultra_radar.m_right.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 1.0;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x02;//right
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
				}//2. turn angle -60~-200
				else if((stVehicleParas.steer_wheel_angle > -200.0) && (stVehicleParas.steer_wheel_angle <= -60.0)){
					if ((FLSIDE == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 0.8;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x01;//Front
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
					else if ((LEFT == urader_company_message[i].Uposition[j])
							&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 0.5;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x04;//left
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
				}//3. turn angle -200~-480
				else if((stVehicleParas.steer_wheel_angle <= -200.0) && (stVehicleParas.steer_wheel_angle > -480.0))
				{
					if ((FLSIDE == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 1.0;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x01;//Front
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
					else if ((LEFT == urader_company_message[i].Uposition[j])
							&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 0.8;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x04;//Left
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}

				}//4. turn angle min~-480
				else if((stVehicleParas.steer_wheel_angle <= 480.0)){
					if ((FLSIDE == urader_company_message[i].Uposition[j])
						&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 1.0;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x01;//Front
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
					else if ((LEFT == urader_company_message[i].Uposition[j])
							&& (st_sensor_mgt.m_ultra_radar.m_left.m_main_sw == SENSOR_OPEN))
					{
						brake_distance_dynamic = Base_Distance_Brake_BYD * 1.0;
						distance_fixed = urader_company_message[i].distance[j];// - speed_ego * rParms.Air_Brake_Delay_Time;
						if(distance_fixed <= brake_distance_dynamic * 1.5){
							g_SSS_warning = sss_warning;
						}
						else{
							g_SSS_warning = sss_no_warning;
						}
						//if(distance_fixed <= rParms.SSS_Break_Enable_Distance){
							//fprintf(USART1_STREAM,"rParms.SSS_Break_Enable_Distance:%d,%0.2f\r\n",approaching_or_not,speed_ego*3.6);
							if(approaching_or_not == 0x01){
								if(distance_fixed < brake_distance_dynamic){
									dec_out_sss = rParms.SSS_Brake_Force;
									//break;
									(*direction) = 0x04;//Left
									*p_ult_id = i * 2 + j;
									*distance = urader_company_message[i].distance[j];
									return rParms.SSS_Brake_Force;
								}
							}
						//}
					}
				}
				if(dec_out_sss > 0.0){
					return dec_out_sss;
				}
			}
		}
	}
	return dec_out_sss;
}
void Get_Uradar_Warning_Info(URADER_MESSAGE urader_company_message[], _VEHICLE_PARA* stVehicleParas,float warning_L2,float warning_L3){
	stVehicleParas->BreakUreaderLevel = 0x00;//init
	stVehicleParas->BreakUreaderDirection = 0x00;//init

	for(uint8_t i = 0;i < 2;i ++)
	{
		for(uint8_t j = 0;j < 12;j ++)
		{
			if(urader_company_message[i].Urader_work_stata[j] == URWORK)
			{
				if( FRONT == urader_company_message[i].Uposition[j] ||
				   FRMIDE == urader_company_message[i].Uposition[j] ||
				   FLMIDE == urader_company_message[i].Uposition[j] ||
				   FRIGHT == urader_company_message[i].Uposition[j] ||
				    FLEFT == urader_company_message[i].Uposition[j]){
					float speed_ego = speed_meter_p_s(stVehicleParas->fVehicleSpeed);
					float break_distance_stright = stVehicleParas->fVehicleSpeed*(2.0-0.8)/(rParms.SSS_Max_Enable_Speed - 1.0) + (0.5 - (2.0-0.8)/(rParms.SSS_Max_Enable_Speed - 1.0));//defult:the air "pressure" = 0.5*deceleration

					if(urader_company_message[i].distance[j] < break_distance_stright + speed_ego*0.8 && urader_company_message[i].distance[j] < rParms.SSS_Warning_Enable_Distance){
						stVehicleParas->BreakUreaderDirection = 0x01;//front
						stVehicleParas->BreakUreaderLevel = 0x02;
					}
					else if(urader_company_message[i].distance[j] < break_distance_stright + speed_ego*1.4 && urader_company_message[i].distance[j] < rParms.SSS_Warning_Enable_Distance){
						stVehicleParas->BreakUreaderDirection = 0x01;//front
						stVehicleParas->BreakUreaderLevel = 0x03;
					}
				}
				if(RIGHT == urader_company_message[i].Uposition[j] && (stVehicleParas->LeftFlag == 0))
				{
					if(urader_company_message[i].distance[j] < warning_L2){
						if(stVehicleParas->BreakUreaderDirection == 0x00)
							stVehicleParas->BreakUreaderDirection = 0x02;//rightfront
						else if(stVehicleParas->BreakUreaderDirection == 0x01)
							stVehicleParas->BreakUreaderDirection = 0x06;//rightfront
						else if(stVehicleParas->BreakUreaderDirection == 0x03)
							stVehicleParas->BreakUreaderDirection = 0x08;//rightback
						stVehicleParas->BreakUreaderLevel = 0x02;
					}
					else if(urader_company_message[i].distance[j] < warning_L3){
						if(stVehicleParas->BreakUreaderDirection == 0x00)
							stVehicleParas->BreakUreaderDirection = 0x02;//right
						else if(stVehicleParas->BreakUreaderDirection == 0x01)
							stVehicleParas->BreakUreaderDirection = 0x06;//rightfront
						else if(stVehicleParas->BreakUreaderDirection == 0x03)
							stVehicleParas->BreakUreaderDirection = 0x08;//rightback
						stVehicleParas->BreakUreaderLevel = 0x03;
					}
				}
				if(LEFT == urader_company_message[i].Uposition[j] && (stVehicleParas->RightFlag == 0))
				{
					if(urader_company_message[i].distance[j] < warning_L2){
						if(stVehicleParas->BreakUreaderDirection == 0x00)
							stVehicleParas->BreakUreaderDirection = 0x04;//left
						else if(stVehicleParas->BreakUreaderDirection == 0x01)
							stVehicleParas->BreakUreaderDirection = 0x05;//leftfront
						else if(stVehicleParas->BreakUreaderDirection == 0x03)
							stVehicleParas->BreakUreaderDirection = 0x07;//leftback
						stVehicleParas->BreakUreaderLevel = 0x02;
					}
					else if(urader_company_message[i].distance[j] < warning_L3){
						if(stVehicleParas->BreakUreaderDirection == 0x00)
							stVehicleParas->BreakUreaderDirection = 0x04;//leftfront
						else if(stVehicleParas->BreakUreaderDirection == 0x01)
							stVehicleParas->BreakUreaderDirection = 0x05;//leftfront
						else if(stVehicleParas->BreakUreaderDirection == 0x03)
							stVehicleParas->BreakUreaderDirection = 0x07;//leftback
						stVehicleParas->BreakUreaderLevel = 0x03;
					}
				}
				if(FRSIDE == urader_company_message[i].Uposition[j]){
					if(urader_company_message[i].distance[j] < warning_L2){
						stVehicleParas->BreakUreaderDirection = 0x06;//rightfront
						stVehicleParas->BreakUreaderLevel = 0x02;
					}
					else if(urader_company_message[i].distance[j] < warning_L3){
						stVehicleParas->BreakUreaderDirection = 0x06;//rightfront
						stVehicleParas->BreakUreaderLevel = 0x03;
					}
				}
				if(FLSIDE == urader_company_message[i].Uposition[j]){
					if(urader_company_message[i].distance[j] < warning_L2){
						stVehicleParas->BreakUreaderDirection = 0x05;//rightfront
						stVehicleParas->BreakUreaderLevel = 0x02;
					}
					else if(urader_company_message[i].distance[j] < warning_L3){
						stVehicleParas->BreakUreaderDirection = 0x05;//rightfront
						stVehicleParas->BreakUreaderLevel = 0x03;
					}
				}
			}
		}
	}
}
//static float distance_FRONT;
//static float distance_FRIGHT;
//static float distance_FLEFT;
//static float distance_FRMIDE;
//static float distance_FLMIDE;
//static float distance_FRSIDE;
//static float distance_FLSIDE;
//static float distance_RIGHT;
//static float distance_LEFT;
//void Uradar_test(URADER_MESSAGE urader_company_message[]){
//	for(uint8_t i = 0;i < 2;i ++)
//	{
//		for(uint8_t j = 0;j < 12;j ++)
//		{
//			if(urader_company_message[i].Urader_work_stata[j] == URWORK)
//			{
//
//				enum _URADER_POSITION Now_position = urader_company_message[i].Uposition[j];
//				fprintf(USART1_STREAM,"URWORK=%d,i=%d,j=%d,distance=%0.2f\r\n",Now_position,i,j,urader_company_message[i].distance[j]);
//				switch(Now_position){
//				case FRONT:
//					distance_FRONT  = urader_company_message[i].distance[j];
//					break;
//				case FRIGHT:
//					distance_FRIGHT = urader_company_message[i].distance[j];
//					break;
//				case FLEFT:
//					distance_FLEFT  = urader_company_message[i].distance[j];
//					break;
//				case FRMIDE:
//					distance_FRMIDE = urader_company_message[i].distance[j];
//									break;
//				case FLMIDE:
//					distance_FLMIDE = urader_company_message[i].distance[j];
//									break;
//				case FRSIDE:
//					distance_FRSIDE = urader_company_message[i].distance[j];
//									break;
//				case FLSIDE:
//					distance_FLSIDE = urader_company_message[i].distance[j];
//									break;
//				case LEFT:
//					distance_LEFT   = urader_company_message[i].distance[j];
//									break;
//				case RIGHT:
//					distance_RIGHT  = urader_company_message[i].distance[j];
//									break;
//				}
//
//			}
//
//		}
//		fprintf(USART1_STREAM,"URADAR:FLSIDE=%0.2f,FRSIDE=%0.2f,L=%0.2f,R=%0.2f\r\n", \
//							distance_FLSIDE,distance_FRSIDE,distance_LEFT,distance_RIGHT);
//	}
//	//fprintf(USART1_STREAM,"URADAR:F=%0.2f,FR=%0.2f,FL=%0.2f,FRM=%0.2f,FLM=%0.2f,FRS=%0.2f,FLS=%0.2f,R=%0.2f,L=%0.2f \r\n", \
//	//			distance_FRONT,distance_FRIGHT,distance_FLEFT,distance_FRMIDE,distance_FLMIDE,distance_FRSIDE,distance_FLSIDE,distance_RIGHT,distance_LEFT);
//	//fprintf(USART1_STREAM,"F=%0.2f,FR=%0.2f,FL=%0.2f,FRM=%0.2f,FLM=%0.2f\r\n", \
//	//		distance_FRONT,distance_FRIGHT,distance_FLEFT,distance_FRMIDE,distance_FLMIDE);
//}
