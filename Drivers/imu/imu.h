/*
 * imu.h
 *
 *  Created on: 2022-7-19
 *      Author: acai
 */

#ifndef IMU_H_
#define IMU_H_
#include <stdbool.h>
#include "canhl.h"
struct ImuData{
  float x_acc;
  float y_deg;
  float z_deg;
  float z_deg_sp;
};
bool Imu_Init();
bool Imu_TranmitData(struct ImuData *data);
bool Imu_receiveData(struct can_frame * tx_can);
bool LinsTechPkgParse(struct ImuData *data);
bool IMU_WitTechPkgParse(struct ImuData *data);
void GetImuData(struct ImuData *data);
bool dingyiImuParse(struct ImuData *data);

#endif /* IMU_H_ */
