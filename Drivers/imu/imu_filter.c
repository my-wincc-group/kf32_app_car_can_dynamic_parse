#include "imu_filter.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "tool.h"
#include "usart.h"
#define IMU_YAW_VEC_LENGTH 15
#define IMU_THETA_VEC_LENGTH 100

struct CQueue yaw_vec_[1] = { 0 };
float yaw_buf_[IMU_YAW_VEC_LENGTH] = { 0 };
float yaw_sort_[IMU_YAW_VEC_LENGTH];

struct CQueue acc_vec_[1] = { 0 };
float acc_buf_[IMU_YAW_VEC_LENGTH] = { 0 };
float acc_sort_[IMU_YAW_VEC_LENGTH];

struct CQueue theta_vec_[1] = { 0 };
float theta_buf_[IMU_THETA_VEC_LENGTH] = { 0 };

bool reverse_gear = false;
bool stop_flag = false;
float speed_calc = 0;
int acc_count = 0;

bool ImuFilter_CopyData(struct CQueue *queue, float yaw_sort[]);
bool ImuFilter_SortData(float array[],const int size);
bool ImuFilter_AccumulateData(struct CQueue *queue, float *sum_result);
void ImuFilter_Print(struct CQueue *queue);

bool ImuFilter_Init() {
  CQueue_Create(yaw_vec_, yaw_buf_, IMU_YAW_VEC_LENGTH, sizeof(float), C_QUEUE_UPDATE_NEW);
  CQueue_Create(theta_vec_, theta_buf_, IMU_THETA_VEC_LENGTH, sizeof(float), C_QUEUE_UPDATE_NEW);
  CQueue_Create(acc_vec_, acc_buf_, IMU_YAW_VEC_LENGTH, sizeof(float), C_QUEUE_UPDATE_NEW);

  return true;
}

bool ImuFilter_Process(float yawrate, float *yaw_filter, float *yaw_theta) {

  float yaw_thre = 1.f;
  float delta_t = 1.f / 50.f;

  CQueue_Push(yaw_vec_, &yawrate);
  if (CQueue_GetSize(yaw_vec_) < IMU_YAW_VEC_LENGTH) {
    *yaw_filter = 0;
    *yaw_theta = 0;
    return false;
  }

  memset(yaw_sort_, 0.f, sizeof(yaw_sort_));

  ImuFilter_CopyData(yaw_vec_, yaw_sort_);
  ImuFilter_SortData(yaw_sort_, IMU_YAW_VEC_LENGTH);

  const int half_index = IMU_YAW_VEC_LENGTH >> 1;
  if (half_index > (sizeof(yaw_sort_) / sizeof(yaw_sort_[0]))) {
    *yaw_filter = 0;
  } else {
    *yaw_filter = yaw_sort_[half_index];
  }

  CQueue_Push(theta_vec_, yaw_filter);

  float theta_sum = 0;
  ImuFilter_AccumulateData(theta_vec_, &theta_sum);
  if (fabs(theta_sum) < yaw_thre) {
    *yaw_theta = 0;
  }

  *yaw_theta += (*yaw_filter) * delta_t;

  return true;
}

bool ImuSpeedFilter_Process(const float speed_input, float accele,float acc_offset,
                            float *acc_filter, int *reverse_gear_) {

  float delta_t = 1.f / 100.f;
  float acc_temp = accele + acc_offset;
  CQueue_Push(acc_vec_, &acc_temp);

  if (CQueue_GetSize(acc_vec_) < IMU_YAW_VEC_LENGTH - 1) {
    *acc_filter = 0;
    *reverse_gear_ = 0;
    return false;
  }

  memset(acc_sort_, 0.f, sizeof(acc_sort_));

  ImuFilter_CopyData(acc_vec_, acc_sort_);
  ImuFilter_SortData(acc_sort_, IMU_YAW_VEC_LENGTH);

  const int half_index = IMU_YAW_VEC_LENGTH >> 1;
  if (half_index > (sizeof(acc_sort_) / sizeof(acc_sort_[0]))) {
    *acc_filter = 0;
  } else {
    *acc_filter = acc_sort_[half_index];
  }

  if (speed_input < 0.5f) {
    stop_flag = true;
    speed_calc = 0.f;
    acc_count = 0;
  }

  if (stop_flag) {
    acc_count++;
    speed_calc += *acc_filter * delta_t;
 //   gcz_serial_v1_dma_printf(SERIAL_UART1,"cnt:%d sp:%0.6f af:%0.6f\r\n",acc_count,speed_calc,*acc_filter);
  }
enum{
	CAR_STATE_NO_REVERSE_STATE = 0,
	CAR_START_REVERSE_STATE,
}
  static reverse_gear_state = CAR_STATE_NO_REVERSE_STATE;
  if (acc_count > 9) {
	  acc_count = 10;
    if (speed_calc < 0)
    {
    	reverse_gear_state = CAR_START_REVERSE_STATE;
  //  	gcz_serial_v1_dma_printf(SERIAL_UART1,"11111111111\r\n");
    }
  }
	if (reverse_gear_state == CAR_START_REVERSE_STATE)
	{
		if (((speed_input >= 0.0) && (speed_input < 0.5)) || ((*acc_filter - acc_offset) >= 1.0))
		{
			reverse_gear_state = CAR_STATE_NO_REVERSE_STATE;
			reverse_gear = false;
			stop_flag = false;
			speed_calc = 0;
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"22222222222\r\n");
		}
	}
	if (reverse_gear_state == CAR_START_REVERSE_STATE)
	{
		reverse_gear = true;
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"3333333\r\n");
	}

  if (reverse_gear) {
    *reverse_gear_ = 1;
 //   gcz_serial_v1_dma_printf(SERIAL_UART1,"AAAAAAAAA\r\n");
  } else {
    *reverse_gear_ = 0;
  //  gcz_serial_v1_dma_printf(SERIAL_UART1,"BBBBBBBBBBBB\r\n");
  }

  return true;
}

bool ImuFilter_CopyData(struct CQueue *queue, float yaw_sort[]) {
  int size = CQueue_GetSize(queue);
  memcpy(yaw_sort, queue->element_buffer, queue->element_size * size);
  return true;
}

bool ImuFilter_SortData(float array[],const int size) {
  for (int i = 0; i < size; i++) {
    bool exchange = false;
    for (int j = 0; j < size - 1; j++) {
      if (array[j] > array[j + 1]) {
        exchange = true;

        float temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
      }
    }

    if(!exchange){
      break;
    }
  }

  return true;
}


bool ImuFilter_AccumulateData(struct CQueue *queue, float *sum_result) {

  float sum = 0;
  int size = CQueue_GetSize(queue);
  for (int i = 0; i < size; i++) {
    void *node = queue->element_buffer + queue->element_size * i;
    sum += *(float *)node;
  }
  *sum_result = sum;

  return true;
}

void ImuFilter_Print(struct CQueue *queue) {
  int size = CQueue_GetSize(queue);
  for (int i = 0; i < size; i++) {
    void *node = queue->element_buffer + queue->element_size * i;
    printf("%d data is %f\n", i, *(float *)node);
  }

}
