#ifndef IMU_FILTER_H
#define IMU_FILTER_H

#include "c_queue.h"


bool ImuFilter_Init();
bool ImuFilter_Process(float yawrate, float *yaw_filter, float *yaw_theta);
bool ImuSpeedFilter_Process(const float speed_input, float accele,float acc_offset,
                            float *acc_filter, int *speed_filter);


#endif // !IMU_FILTER_H
