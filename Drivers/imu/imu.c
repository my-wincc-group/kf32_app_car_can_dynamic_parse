/*
 * imu.c
 *
 *  Created on: 2022-7-19
 *      Author: acai
 */

#include "imu.h"
#include "c_queue.h"
#include "../canhl.h"
#include "common.h"
#include "usart.h"
#include "aeb_sensor_management.h"
#include "imu_filter.h"
#include "wt_jy901_imu.h"
#include "tool.h"
#include "imu_app.h"


#define IMU_BUFFER_LENGTH 10

struct CQueue imu_queue_[1] = { 0 };
struct can_frame imu_buf_[IMU_BUFFER_LENGTH] = { 0 };
struct ImuData imuData_; //private variable, only use in the imu scope
bool imuOpen_ = false; //No use IMU
uint8_t imuProducer_ = 0x00; //No use IMU

bool Imu_Init() {

  Sensor_MGT sensorMgt = Get_Sensor_MGT_Param();
  imuOpen_ = sensorMgt.m_imu.m_isOpen;
  imuProducer_ = sensorMgt.m_imu.m_producer; //0x01: dingyi; 0x02: Lins Tech; 0x03: WIT
  if (imuOpen_ == false) {
    return NULL;
  }
  CQueue_Create(imu_queue_, imu_buf_, IMU_BUFFER_LENGTH, sizeof(struct can_frame),
      C_QUEUE_UPDATE_NEW);
  ImuFilter_Init();
  //USART1_Bebug_Print_Num("[Imu_init]", (int)imu_queue_, 2, 1); // 勿删除
  imuData_.x_acc = 0.0;
  imuData_.y_deg = 0.0;
  imuData_.z_deg_sp = 0.0;
  return NULL == imu_queue_ ? false : true;
}
extern bool g_b_can_sp_show_flag;
bool ImuPkgPrase(uint8_t imuType, struct ImuData *data) {
	bool res = false;
	//yuhong 20220908 如果IMU为启用，或者IMU的类型小于等于0
	if (imuOpen_ == false || imuType==0 ) {
		return res;
	}
	IMU_FILTER_DATA *p = get_imu_original_data();
	IMU_FILTER_DATA *pklm = get_imu_klm_filter_data();
	//通过了上面参数的检查，则将返回结果赋值为TRUE
	res = true;
	switch (imuType) {
	case 0x01: //Dingyi
		dingyiImuParse(data);
		//车速插值及场景判断兼容处理
		p->acc[X_P] = data->x_acc * 9.8;
		p->angular_v[Z_P] = data->z_deg_sp;
		p->angle[Y_P] = data->y_deg;
		p->angle[Z_P] = data->z_deg;
		pklm->acc[X_P] = p->acc[X_P];
		set_imu_comm_flag(true);		//2022-09-14 IMU通讯是否正常检测
		break;
	case 0x02: //Lins Tech
		LinsTechPkgParse(data);
		//车速插值及场景判断兼容处理
		p->acc[X_P] = data->x_acc * 9.8;
		p->angular_v[Z_P] = data->z_deg_sp;
		p->angle[Y_P] = data->y_deg;
		p->angle[Z_P] = data->z_deg;
		pklm->acc[X_P] = p->acc[X_P];
		set_imu_comm_flag(true);		//2022-09-14 IMU通讯是否正常检测
		break;
	case 0x03: //WIT
		IMU_WitTechPkgParse(data);
		//车速插值及场景判断兼容处理
		p->acc[X_P] = data->x_acc * 9.8;
		p->angular_v[Z_P] = data->z_deg_sp;
		p->angle[Y_P] = data->y_deg;
		p->angle[Z_P] = data->z_deg;
		pklm->acc[X_P] = p->acc[X_P];
		set_imu_comm_flag(true);		//2022-09-14 IMU通讯是否正常检测
		break;
	case 0x04:	//wt UART  JY901  原始Y角速度和X加速度接入  gcz 2022-08-30
		p = get_imu_original_data();
		data->z_deg_sp = p->angular_v[Z_P];
		data->x_acc = p->acc[X_P] / 9.8;
		if (g_b_can_sp_show_flag)
		{
		  gcz_serial_v1_dma_printf(SERIAL_UART1,"0.[x_acc] %0.6f\r\n", data->x_acc); // 勿删除
		  gcz_serial_v1_dma_printf(SERIAL_UART1,"0.[z_deg_sp] %0.6f", data->z_deg_sp); // 勿删除
		}
		break;
	default:
		//上述的imu类型都为命中则返回结果为false
		res = false;
		break;
	}
	return res;
}
//Lins Tech IMU解析
static float LINS_ANGLE_RESOLUTION = 360 / 65536.f;
static float LINS_ACCELERATION_RESOLUTION = 20 / 65536.f;
static float LINS_ANGULAR_VEL_RESOLUTION = 1260 / 65536.f;
static uint16_t LINS_GROUP_SIZE = 3; //0x181, 0x281, 0x381
static uint16_t LINS_FRAME_START_ID = 0x181;

bool LinsFindBeginIMUCanInfo(struct can_frame *frame, uint32_t frame_id) {
  while (1) {
    if (!CQueue_GetData(imu_queue_, frame)) {
      return false;
    }

    if (frame_id == frame->TargetID) {
      return true;
    } else {
      CQueue_Pop(imu_queue_);
    }
  }
}
#include "tool.h"
bool LinsTechPkgParse(struct ImuData *data) {
  struct can_frame can;
  bool popResult = false;
  float yDegree = 0;
  float xACC = 0;
  float zDegreeSpeed = 0;

  if (imuOpen_ == false || !LinsFindBeginIMUCanInfo(&can, LINS_FRAME_START_ID)
      || CQueue_GetSize(imu_queue_) < LINS_GROUP_SIZE) {
    data->y_deg = 0.0;
    data->x_acc = 0.0;
    data->z_deg_sp = 0.0;
    return false;;
  }
  //Parse the Lins Tech IMU data
  for (int j = 0; j < 3; j++) {
    if (!CQueue_GetData(imu_queue_, &can)) {
      continue;
    }
    //Pop one data of the imu queue
    CQueue_Pop(imu_queue_);
//      USART1_Bebug_Print_Num(">>>>>>>>>>>>>>>>>>>CQueue_getData:", j, 0, 1);
//		USART1_Bebug_Print_Num(">>>>>>>>>>>>>>>>>>>CQueue_getData:", can.TargetID, 2, 1);
    switch (can.TargetID) {
    case 0x181:
      data->y_deg = ((short) (can.data[5] * 256 + can.data[4])) * LINS_ANGLE_RESOLUTION;
//				USART1_Bebug_Print_Num("[**data->y_deg]", (can.data[5]*256+can.data[4])*ANGLE_RESOLUTION,1, 1);
//				USART1_Bebug_Print_float("[>>data->y_deg]", data->y_deg, 1);
      break;
    case 0x281:
      data->x_acc = ((short) (can.data[1] * 256 + can.data[0])) * LINS_ACCELERATION_RESOLUTION;
//				USART1_Bebug_Print_Num("[**data->x_acc]", (can.data[1]*256+can.data[0])*ACCELERATION_RESOLUTION,1, 1);
      	  	  	  gcz_serial_v1_dma_printf(SERIAL_UART1,"[>>data->x_acc]%0.6f\r\n", data->x_acc);
      break;
    case 0x381:
      data->z_deg_sp = ((short) (can.data[5] * 256 + can.data[4])) * LINS_ANGULAR_VEL_RESOLUTION;
//				USART1_Bebug_Print_Num("[**data->z_deg_sp]", (can.data[5]*256+can.data[4])*ANGULAR_VEL_RESOLUTION,1, 1);
//				USART1_Bebug_Print_float("[>>data->z_deg_sp]", data->z_deg_sp, 1);
      break;
    default:
      break;
    }
  }
  return true;
}
bool IMU_WitTechPkgParse(struct ImuData *data) {

  static float WIT_ACCELERATION_RESOLUTION = 16.f / 32768.f;
  static float WIT_ANGLE_RESOLUTION = 2000.f / 32768.f;
  static float WIT_ANGULAR_VEL_RESOLUTION = 180.f / 32768.f;
  //USART1_Bebug_Print_Num(">>>>>>>>>>>>>>>>>>>Entry IMU_WIT:", 1, 2, 1);

  struct can_frame can;
  bool popResult = false;
  float yDegree = 0;
  float xACC = 0;
  float zDegreeSpeed = 0;
  if (imuOpen_ == false || CQueue_GetSize(imu_queue_) < 4) {
    data->y_deg = 0.0;
    data->x_acc = 0.0;
    data->z_deg_sp = 0.0;
    return false;
  }
  //Parse the Lins Tech IMU data
  for (int j = 0; j < 4; j++) {
    if (!CQueue_GetData(imu_queue_, &can)) {
      continue;
    }
    //Pop one data of the imu queue
    CQueue_Pop(imu_queue_);
//      USART1_Bebug_Print_Num(">>>>>>>>>>>>>>>>>>>CQueue_getData:", j, 0, 1);
    //USART1_Bebug_Print_Num(">>>>>>>>>>>>>>>>>>>CQueue_getData:", can.TargetID, 2, 1);
    if (can.data[0] == 0x55) {
      switch (can.data[1]) {

      case 0x51: //Acceleration calculation
        //highData = can.data[3];
        //lowData = can.data[2];
        data->x_acc = (short) (can.data[3] << 8 | can.data[2]) * WIT_ACCELERATION_RESOLUTION;
//					USART1_Bebug_Print_Num("[**data->x_acc]", (short)(can.data[3]<<8|can.data[2])*WIT_ACCELERATION_RESOLUTION,1, 1);
//					USART1_Bebug_Print_Flt("[>>data->x_acc]", data->x_acc, 1,1);
        break;
      case 0x52: //yaw rate calculation
        data->z_deg_sp = ((short) (can.data[5] * 256 + can.data[4])) * WIT_ANGLE_RESOLUTION;
//					USART1_Bebug_Print_Num("[**data->z_deg_sp]", ((short)(can.data[5]*256+can.data[4]))*WIT_ANGLE_RESOLUTION,1, 1);
//					USART1_Bebug_Print_Flt("[>>data->z_deg_sp]", data->z_deg_sp, 1,1);
        break;
      case 0x53: //Yaw degree output
        data->y_deg = ((short) (can.data[7] * 256 + can.data[6])) * WIT_ANGULAR_VEL_RESOLUTION;
//					USART1_Bebug_Print_Num("[**data->y_deg]", ((short)(can.data[7]*256+can.data[6]))*WIT_ANGULAR_VEL_RESOLUTION,1, 1);
//					USART1_Bebug_Print_Flt("[>>data->y_deg]", data->y_deg, 1,1);
        break;
      case 0x54:
        break;
      default:
        break;
      }
    }
  }
  return true;
}
bool dingyiImuParse(struct ImuData *data) {
  static int64_t last_time = -1;
  struct can_frame can;
  //USART1_Bebug_Print_Num("[last_time 1111111]", last_time, 1); // 勿删除
  if (!CQueue_Pop(imu_queue_)) {
    //USART1_Bebug_Print_Num("[last_time]", 2, 1); // 勿删除
    return false;
  }
  if (last_time > 0 && SystemtimeClock - last_time >= 500) {
    //USART1_Bebug_Print_Num("[last_time]", last_time, 1); // 勿删除
    CQueue_Clear(imu_queue_);
    last_time = SystemtimeClock;
    return false;
  }
  USART1_Bebug_Print_Num("[last_time]", 4, 1, 1); // 勿删除
  last_time = SystemtimeClock;

  data->x_acc = ((int16_t) ((can.data[2] << 8) | can.data[1])) / 32768.f * 16.f;

  //USART1_Bebug_Print_float("[data->x_acc22222]", data->x_acc,2); // 勿删除

  data->z_deg_sp = ((int16_t) ((can.data[4] << 8) | can.data[3])) / 32768.f * 2000.f;
  data->y_deg = ((int16_t) ((can.data[6] << 8) | can.data[5])) / 32768.f * 180.f;
  return true;

}
bool Imu_TranmitData(struct ImuData *data) {

  bool result = false;

  //USART1_Bebug_Print_Num("[imuProducer_]", imuProducer_, 1, 1); // 勿删除
  result = ImuPkgPrase(imuProducer_, data);

  if (!result) //IMU package parse error
  {
    imuData_.x_acc = 0.0f;
    imuData_.y_deg = 0.0f;
    imuData_.z_deg = 0.0f;
    imuData_.z_deg_sp = 0.0f;
  } else {
    ImuFilter_Process(data->z_deg_sp, &(imuData_.z_deg_sp), &(imuData_.z_deg));
    imuData_.x_acc = data->x_acc;
    imuData_.y_deg = data->y_deg;
    //imuData_.z_deg_sp = data->z_deg_sp;

    //USART1_Bebug_Print_Flt("[imuData_.z_deg_sp]", imuData_.z_deg_sp, 1, 1); // 勿删除
  }

  return result;
}

bool Imu_receiveData(struct can_frame *rx_frame) {
  //The imu not config, need to return directly
  if (imuOpen_ == false) {
    return false;
  }
 //USART1_Bebug_Print_Num("[imuProducer_]", imuProducer_, 1, 1); // 勿删除
  switch (imuProducer_) {
  case 0x01: ////Dingyi IMU
    if (rx_frame->TargetID == 0x18fee0d8 || rx_frame->TargetID == 0x18feefd8) {
      CQueue_Push(imu_queue_, (void *) (rx_frame));
    }
    break;
  case 0x02: //Lins Tech
    if (rx_frame->TargetID == 0x181 || rx_frame->TargetID == 0x281 || rx_frame->TargetID == 0x381) {
      CQueue_Push(imu_queue_, (void *) (rx_frame));
    }
    break;
  case 0x03: //WIT
    if (rx_frame->TargetID == 0x50) {
      CQueue_Push(imu_queue_, (void *) (rx_frame));
    }
    break;
  default:
    break;
  }
  return true;
}

void GetImuData(struct ImuData *data) {
  data->x_acc = imuData_.x_acc;
  data->y_deg = imuData_.y_deg;
  data->z_deg_sp = imuData_.z_deg_sp;
}
