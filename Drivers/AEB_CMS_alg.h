/*
 * AEB_CMS_alg.h
 *
 *  Created on: 2021-11-25
 *  Author: xujie
 */

#ifndef AEB_CMS_ALG_H_
#define AEB_CMS_ALG_H_
#include "proportional_valve.h"
#include "common.h"
#include "gpio.h"
#include "aeb_cms_sss_para_config.h"
#include "circular_queue.h"
//gcz 2022-05-10
extern int g_iShow_RT_speed[2];

#define BSD_TTC_LOW       	4
#define BSD_TTC_MIDDLE 		2.7
#define BSD_TTC_HIGH 		1.2

#define BSD_DISTANCE_LOW       	4
#define BSD_DISTANCE_MIDDLE 	2.7
#define BSD_DISTANCE_HIGH 		1.2

#define BSD_TTC_VALUE	 	2.5
#define BSD_DEC_VALUE	 	1.0
#define SPEED_LIMIT			60.0
#define DISTANCE_MIN		1.0f
#define DISTANCE_MAX		2.0f

// define struct of CMS config parameter
typedef struct{
	float cms_uradar_dec_out_prev;
	float cms_uradar_dec_out;
	float cms_hmw_dec_out;
	float cms_hmw_dec_out_prev;
	float cms_ttc_dec_out_prev;
	float cms_ttc_dec_out;
	float cam_distance;
	float uradar_distance;

	uint8_t cms_is_warning_flag;
	uint8_t cms_is_break_flag;
	uint8_t cms_cam_keep_count;
	uint8_t cms_cam_delay_count;
	uint32_t cms_cam_over_timestamp;
	uint8_t cms_uradar_keep_count;
	uint8_t cms_uradar_delay_count;

	uint8_t cms_ttc_is_warning_flag;
	uint8_t cms_ttc_is_break_flag;
	uint8_t cms_ttc_cam_keep_count;
	uint8_t cms_ttc_cam_delay_count;
	uint8_t cms_ttc_uradar_keep_count;
	uint8_t cms_ttc_uradar_delay_count;

}CMS_Break_State;
typedef struct{
	float TTC_AEB;
	float TTC_AEB_dec_out;
	uint8_t aeb_TTC_is_break_flag;

}AEB_Break_State;
typedef struct{
	////////////////////////////////////////////////////
	// TOthers
	float max_Dynamic_HMW_speed;    //km/h
	float max_enable_speed;    //km/h
	// recal HWM and TTC warning Level
	float Cam_HMW_warning_time; //too close to lead car
	float Cam_TTC_warning_1_time; //
	float Cam_TTC_warning_2_time; //
	// Vehicle parameters
	float ttc_max_dec_output;
	float Vehicle_max_react_dec;
	float Vehicle_min_react_dec;
	// parameters of CIPV
	float cipv_max_distance;
	uint8_t min_track_number;
	// Parameters of Uradar
	float Uradar_max_enable_speed;
}AEB_CMS_Control_Parameters;

typedef struct{
  uint8_t id;
	uint8_t valid;
	uint8_t warningZone;
	uint8_t warningLevel;
	uint8_t motion_state;
	bool levelOneValid;
	bool levelTwoValid;
	bool levelThreeValid;
	float vel_x;
	float vel_y;
	float ttc_x;
	float ttc_y;
	float dist_x;
	float dist_y;
	float decelerationOutput;
}SRR_Obstacle_Paras;
//gcz 2022-09-12增加左侧超车场景是否需要刹车判断程序
typedef enum _judge_result
{
	GO_STAIGHT = 0,						//直行					是否刹车自主决定
	LEFT_SIDE_OVERTAKE_JUDGEING,		//左侧超车判断中			可以不刹车
	LEFT_SIDE_OVERTAKE,			//左侧超车				可以不刹车
	TURN_LEFT,							//左转					可以不刹车
	TURN_RIGHT,							//右转					是否刹车自主决定
	OTHER								//其它					是否刹车自主决定
}JUDGE_RESULT;

enum LOG_LEVEL
{
    LOG_NONE  	= 0x00,
    LOG_ERROR  	= 0x01,
    LOG_WARNING = 0x02,
    LOG_DEBUG  	= 0x03,
    LOG_IN_OUT 	= 0x04,
    LOG_Brake_Cancel = 0x05
};

void Got_Vehicle_Para_form_GPIO(_VEHICLE_PARA* stVehicleParas);
void PWM_Control_Dec_to_pwm(float deceleration);

float AEB_CMS_Break_control(uint8_t got_cipv,
							Obstacle_Basic_Data* obs_basic_data,
							Obstacle_Information* cipv,
							_VEHICLE_PARA* stVehicleParas,
							Camera_Essential_Data* cam_essiential,
							URADER_MESSAGE	Urader_Company_Message[]);
float AEB_TTC_Break(Obstacle_Information* cipv,
					Obstacle_Basic_Data* obs_basic_data,
					_VEHICLE_PARA* stVehicleParas);
float CMS_TTC_Break(Obstacle_Information* cipv,
					Obstacle_Basic_Data* obs_basic_data,
					_VEHICLE_PARA* stVehicleParas,
					Camera_Essential_Data* cam_essiential,
					URADER_MESSAGE Urader_Company_Message[]);
float CMS_HMW_Break(Obstacle_Information* cipv,
					Obstacle_Basic_Data* obs_basic_data,
					_VEHICLE_PARA* stVehicleParas,
					Camera_Essential_Data* cam_essiential,
					URADER_MESSAGE Urader_Company_Message[],
					uint8_t Approaching);
float CMS_URadar_Break(_VEHICLE_PARA* stVehicleParas,
					URADER_MESSAGE Urader_Company_Message[]);

float AEB_TTC_Break_Time_Point(Obstacle_Information* cipv,
					Obstacle_Basic_Data *obs_basic_data,
					float aeb_decelerate_set,
					float aeb_stop_distance,
					float air_break_sys_delay_time);
float 	Cal_TTC(Obstacle_Information* cipv);
uint8_t Approaching_to_lead_car(Obstacle_Information* cipv, uint8_t size_of_history);

uint8_t Approaching_to_lead_car_uradar(float diatance, uint8_t size_of_history);
uint8_t Approaching_to_target(Circular_Queue* cipv_diantance_queue,Obstacle_Information* cipv);

uint8_t Break_Trigger(Obstacle_Information* cipv, _VEHICLE_PARA* stVehicleParas, Camera_Essential_Data* cam_essiential,Obstacle_Basic_Data* obs_basic_data);
uint8_t Break_Cancel(Obstacle_Information* cipv, _VEHICLE_PARA* stVehicleParas,Camera_Essential_Data* cam_essiential,Obstacle_Basic_Data* obs_basic_data,float old_decout);
uint8_t Driver_control(_VEHICLE_PARA* stVehicleParas);
void 	AEB_State_reset(void);
void 	CMS_State_reset(void);
void 	CMS_HMW_State_reset(void);
void 	CMS_TTC_State_reset(void);
void 	AEB_CMS_alg_init(void);
float 	Get_Uradar_distance(URADER_MESSAGE Urader_Company_Message[],_VEHICLE_PARA* stVehicleParas);
void 	Filter_noise(float *dec_out, float dec_out_k,uint8_t *is_break_flag, uint8_t *count_delay, uint8_t *count_keep);
void 	Log_print(enum LOG_LEVEL level, uint8_t cancel_n, uint8_t func_n, const char* description, const char* function_name, Obstacle_Information* cipv,_VEHICLE_PARA* stVehicleParas,Camera_Essential_Data* Cam_Essi_data,Obstacle_Basic_Data* obs_basic_data);
//test
float 	AEBS_Test_Brake(uint8_t set_brake_perssure,_VEHICLE_PARA stVehicleParas);
float 	Ret_test(void);
void	Vehicle_Turn_Signal_Keep(_VEHICLE_PARA* stVehicleParas);
float AngleRadar_TTC_Break(_VEHICLE_PARA* stVehicleParas);
uint8_t obstacleDirectionMatch(uint8_t obstacleDirection);
uint8_t getAngleRadarMalfunction( void );

SRR_Obstacle_Paras getSrrWarningAndZoneParas( void );

extern float 	g_AEB_CMS_outside_dec_output;
extern uint8_t 	g_AEBS_outside_Test_Brake_Warning;
extern uint8_t 	g_AEB_CMS_outside_BrakeBy_CMSHMW;
extern uint8_t	g_AEB_CMS_outside_BrakeBy_CMSTTC;
extern uint8_t 	g_AEB_CMS_outside_BrakeBy_AEBTTC;
extern uint8_t 	g_AEB_ttc_warning_L2;
extern uint8_t 	g_is_hmw_warning;
extern uint8_t 	g_AEB_ON_OFF_Displayer;
extern uint8_t 	g_LDW_ON_OFF_Displayer;
#endif /* AEB_CMS_ALG_H_ */
