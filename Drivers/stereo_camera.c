/*
 * stereo_camera.c
 *
 *  Created on: 2021-6-22
 *      Author: shuai
 */
#include "stereo_camera.h"
#include "data_center_services.h"
#include "can_task.h"
#include "common.h"
#include "aeb_cms_sss_para_config.h"
#include "AEB_CMS_alg.h"
#include "SSS_alg.h"

struct can_frame rx_camera_data; 		// 0x79F
struct can_frame rx_obstacle_info; 		// 0x7A0
struct can_frame rx_obstacle_info_a; 	// 0x7A1
struct can_frame rx_obstacle_info_b; 	// 0x7A2
struct can_frame rx_obstacle_info_tland;// 0x7A3
struct can_frame rx_obstacle_info_lbll; // 0x7A4
struct can_frame rx_obstacle_info_lbrl; // 0x7A5
struct can_frame rx_obstacle_info_rbll; // 0x7A6
struct can_frame rx_obstacle_info_rbrl; // 0x7A7q
struct can_frame tx_camera_input; 		// 0x7B0
struct can_frame tx_to_can_pool; 		// 0x790

Camera_Essential_Data 	camera_data;
Obstacle_Basic_Data 	obstacle_Basic_data;
Obstacle_Information 	obstacle_cipv_data;
Camera_All_Info 		CameraMessage;
Camera_LDW_data 		camera_LDW_data;

Camera_SN_info 			camera_sn_info;

uint8_t Camera_Info_Push_Flag;
uint8_t cipv_check;
extern float dec_output_AEB_SSS;

void stere_camera_data_analysis()
{
	// 0x7A1数据解析
	obstacle_cipv_data.ObstacleID 		= rx_obstacle_info_a.data[0];
	obstacle_cipv_data.TrackNumber 		= rx_obstacle_info_a.data[1] & 0x3F;
	obstacle_cipv_data.ObstacleWidth 	= (float)(rx_obstacle_info_a.data[2] + ((rx_obstacle_info_a.data[3] & 0x0F) << 8))*0.01;
	obstacle_cipv_data.ObstacleHeight 	= (float)(((rx_obstacle_info_a.data[3] & 0xF0) >> 4) + (rx_obstacle_info_a.data[4] << 4))*0.01;
	obstacle_cipv_data.RelativeSpeedZ 	= (float)(rx_obstacle_info_a.data[5] + ((rx_obstacle_info_a.data[6] & 0x0F) << 8))*0.0625 - 127.9375;
	obstacle_cipv_data.DistanceY 		= (float)(((rx_obstacle_info_a.data[6] & 0xF0) >> 4) + (rx_obstacle_info_a.data[7] << 4))*0.01 -20.47;
	// 0x7A2数据解析
	obstacle_cipv_data.DistanceX 		= (float)(rx_obstacle_info_b.data[0] + ((rx_obstacle_info_b.data[1] & 0x7F) << 8))*0.01 -163.83;
	obstacle_cipv_data.DistanceZ 		= (float)(rx_obstacle_info_b.data[2] + ((rx_obstacle_info_b.data[3] & 0x7F) << 8))*0.01;
	obstacle_cipv_data.TTC 				= (float)(rx_obstacle_info_b.data[4] & 0x3F)*0.1;
	obstacle_cipv_data.HMW 				= (float)(((rx_obstacle_info_b.data[4] & 0xC0) >> 6) + ((rx_obstacle_info_b.data[5] & 0x0F) << 2))*0.1;
	obstacle_cipv_data.RelativeSpeedX 	= (float)((rx_obstacle_info_b.data[6] + ((rx_obstacle_info_b.data[7] & 0x03) << 8)))*0.1 - 51.1;
	obstacle_cipv_data.CIPV 			= (rx_obstacle_info_b.data[7] & 0x04) >> 2;
	obstacle_cipv_data.ObstacleType 	= (rx_obstacle_info_b.data[7] & 0x78) >> 3;

	// 因双目输出HMWGrade不可靠，自己计算。
	if(obstacle_cipv_data.HMW <= obstacle_Basic_data.HMWAlarmThreshold)
	{
		camera_data.HMWGrade = 2;
	}else if(obstacle_cipv_data.HMW <= 2.7 )
	{
		camera_data.HMWGrade = 1;
	}else{
		camera_data.HMWGrade = 3;
	}
}

void Camera_CIPV_Init()
{
	obstacle_cipv_data.ObstacleID 		= 0;
	obstacle_cipv_data.TrackNumber 		= 0;
	obstacle_cipv_data.ObstacleWidth 	= 40.95;
	obstacle_cipv_data.ObstacleHeight 	= 40.95;
	obstacle_cipv_data.RelativeSpeedZ	= 128.0;
	obstacle_cipv_data.DistanceY 		= 20.47;
	obstacle_cipv_data.DistanceX 		= 163.83;
	obstacle_cipv_data.DistanceZ 		= 327.67;
	obstacle_cipv_data.TTC				= 6.3;
	obstacle_cipv_data.HMW 				= 6.3;
	obstacle_cipv_data.RelativeSpeedX 	= 51.1;
	obstacle_cipv_data.CIPV 			= 0;
	obstacle_cipv_data.ObstacleType 	= 0;
}

void Camera_Init()
{
	rx_camera_data.TargetID 		= CAMERA_ESSENTIAL_DATA_ID;
	rx_camera_data.lenth			= 8;
	rx_camera_data.MsgType 			= CAN_DATA_FRAME;
	rx_camera_data.RmtFrm 			= CAN_FRAME_FORMAT_SFF;
	rx_camera_data.RefreshRate 		= CAMERA_ESSENTIAL_REFRESH_RATE;

	rx_obstacle_info.TargetID 		= CAMERA_OBSTACLE_INFO_ID;
	rx_obstacle_info.lenth 			= 8;
	rx_obstacle_info.MsgType 		= CAN_DATA_FRAME;
	rx_obstacle_info.RmtFrm 		= CAN_FRAME_FORMAT_SFF;
	rx_obstacle_info.RefreshRate 	= CAMERA_OBSTACLE_INFO_REFRESH_RATE;

	rx_obstacle_info_a.TargetID 	= CAMERA_OBSTACLE_INFO_A_ID;
	rx_obstacle_info_a.lenth 		= 8;
	rx_obstacle_info_a.MsgType 		= CAN_DATA_FRAME;
	rx_obstacle_info_a.RmtFrm 		= CAN_FRAME_FORMAT_SFF;
	rx_obstacle_info_a.RefreshRate 	= CAMERA_OBSTACLE_INFO_A_REFRESH_RATE;

	rx_obstacle_info_b.TargetID 	= CAMERA_OBSTACLE_INFO_B_ID;
	rx_obstacle_info_b.lenth 		= 8;
	rx_obstacle_info_b.MsgType 		= CAN_DATA_FRAME;
	rx_obstacle_info_b.RmtFrm 		= CAN_FRAME_FORMAT_SFF;
	rx_obstacle_info_b.RefreshRate 	= CAMERA_OBSTACLE_INFO_B_REFRESH_RATE;

	tx_camera_input.TargetID 		= CAMERA_INPUT_DATA_ID;
	tx_camera_input.lenth 			= 8;
	tx_camera_input.MsgType 		= CAN_DATA_FRAME;
	tx_camera_input.RmtFrm 			= CAN_FRAME_FORMAT_SFF;
	tx_camera_input.RefreshRate 	= CAMERA_INPUT_DATA_REFRESH_RATE;

	tx_to_can_pool.TargetID 		= CONTROLER_OUTPUT_INFO_ID;
	tx_to_can_pool.lenth 			= 8;
	tx_to_can_pool.MsgType 			= CAN_DATA_FRAME;
	tx_to_can_pool.RmtFrm 			= CAN_FRAME_FORMAT_SFF;
	tx_to_can_pool.RefreshRate 		= CONTROLER_OUTPUT_INFO_REFRESH_RATE;

	for(uint8_t i=0; i<8; i++)
	{
		rx_camera_data.data[i] 		= 0xFF;
		rx_obstacle_info.data[i] 	= 0xFF;
		rx_obstacle_info_a.data[i] 	= 0xFF;
		rx_obstacle_info_b.data[i] 	= 0xFF;
		tx_camera_input.data[i] 	= 0xFF;
		tx_to_can_pool.data[i] 		= 0x00;
	}
	camera_LDW_data.LeftLaneStyle 	= 0;
	camera_LDW_data.LeftLaneStatus 	= 0;
	camera_LDW_data.RightLaneStyle 	= 0;
	camera_LDW_data.RightLaneStatus = 0;

	camera_data.LeftLDW 			= 0;
	camera_data.RightLDW 			= 0;
	camera_data.LDWSentivity 		= 0;
	camera_data.Invalid 			= 0;
	camera_data.OffSound 			= 0;
	camera_data.HMWGrade 			= 0;
	camera_data.HMW 				= 6.3;
	camera_data.ErrorCode 			= 0;
	camera_data.HMWEnable 			= 0;
	camera_data.FCWLevel 			= 0;
	camera_data.AmbientLuminance 	= 0;
	camera_data.FCWStatus 			= 0;
	camera_data.HMWWarning 			= 0;

	obstacle_Basic_data.ObstacleNumber 		= 0;
	obstacle_Basic_data.HMWAlarmThreshold 	= 0.0;

	Camera_CIPV_Init();

	Camera_Info_Push_Flag 			= 0;
	cipv_check 						= 0;

	Camera_Info_Push(&camera_data, &obstacle_Basic_data, &obstacle_cipv_data);// 数据中心数据初始化。
}

void Camera_CAN_Transmition(struct can_frame *tx_frame,float speed)
{
	tx_camera_input.data[1] = (uint8_t)(speed); // 车速60km/h
	// fprintf(USART1_STREAM,"speed:%0.2f    uint8_t_speed:%d\r\n",speed,(uint8_t)(speed));

	if(g_AEB_CMS_outside_dec_output > 0.0){
		tx_camera_input.data[6] = 0xff;// (uint8_t)(AEB_CMS_outside_dec_output * 10) | 0x01;
	}
	else{
		tx_camera_input.data[6] = 0xfe;// (uint8_t)(AEB_CMS_outside_dec_output * 10) & 0xfe;
	}
	*tx_frame = tx_camera_input;
}

void CAN_Pool_Transmition(struct can_frame *tx_frame)
{
	uint8_t force = 0x00;
	if(g_AEB_CMS_outside_dec_output > 0.0){
		force = (uint8_t)(g_AEB_CMS_outside_dec_output * 10);
	}else if(dec_out_sss > 0.0){
		force = (uint8_t)(dec_out_sss * 10);
	}
	if(	dec_output_AEB_SSS > 0.0){
		force = (uint8_t)(dec_output_AEB_SSS * 10);
	}
#ifdef	AEB_CMS_SSS_CAN_V0_0_
	tx_to_can_pool.data[0] |= g_AEB_CMS_Enable;
	tx_to_can_pool.data[0] |= (g_CMS_HMW_Enable << 1);
	tx_to_can_pool.data[0] |= (g_CMS_TTC_Enable << 2);
	tx_to_can_pool.data[0] |= (g_AEB_TTC_Enable << 3);
	tx_to_can_pool.data[0] |= 	  (g_SSS_Enable << 4);
	tx_to_can_pool.data[0] |= (g_HMW_Dynamic_Thr_Enable << 5);
	tx_to_can_pool.data[0] |= (g_Chassis_Safe_Strategy_Enable << 6);
	tx_to_can_pool.data[0] |= (g_LDW_Enable << 7);

	tx_to_can_pool.data[1] |= g_CMS_hmw_warning;
	tx_to_can_pool.data[1] |= (g_CMS_ttc_warning << 1);
	tx_to_can_pool.data[1] |= (g_AEB_CMS_outside_BrakeBy_CMSHMW << 2);
	tx_to_can_pool.data[1] |= (g_AEB_CMS_outside_BrakeBy_CMSTTC << 3);
	tx_to_can_pool.data[1] |= (g_AEB_ttc_warning_L1 << 4);
	tx_to_can_pool.data[1] |= (g_AEB_ttc_warning_L2 << 5);
	tx_to_can_pool.data[1] |= (g_AEB_CMS_outside_BrakeBy_AEBTTC << 6);
	tx_to_can_pool.data[1] |= (g_SSS_warning << 7);

	tx_to_can_pool.data[2] |= g_SSS_outside_isBrake;
	tx_to_can_pool.data[2] |= (force << 1);

	tx_to_can_pool.data[3] |= (force >> 7);
	tx_to_can_pool.data[3] |= (((uint8_t)(rParms.CMS_HMW_Warning_Time_Thr * 10) & 0x3F)<< 1);
	tx_to_can_pool.data[3] |= (((uint8_t)(rParms.CMS_TTC_Warning_Time_Level_First * 10) & 0x3F)<< 7);

	tx_to_can_pool.data[4] |= (((uint8_t)(rParms.CMS_TTC_Warning_Time_Level_First * 10) & 0x3F) >> 1);
	tx_to_can_pool.data[4] |= (((uint8_t)(rParms.CMS_HMW_Brake_Time_Thr * 10) & 0x1F) << 5);

	tx_to_can_pool.data[5] |= (((uint8_t)(rParms.CMS_HMW_Brake_Time_Thr * 10) & 0x1F) >> 3);
	tx_to_can_pool.data[5] |= (((uint8_t)(rParms.CMS_TTC_Brake_Time_Thr * 10) & 0x1F) << 2);
	tx_to_can_pool.data[5] |= (((uint8_t)(rParms.AEB_TTC_Warning_Time_Level_First * 10) & 0x3F) << 7);

	tx_to_can_pool.data[6] |= (((uint8_t)(rParms.AEB_TTC_Warning_Time_Level_First * 10) & 0x3F) >> 1);
	tx_to_can_pool.data[6] |= (((uint8_t)(rParms.AEB_TTC_Warning_Time_Level_Second * 10) & 0x3F) << 4);

	tx_to_can_pool.data[7] |= (((uint8_t)(rParms.AEB_TTC_Warning_Time_Level_Second * 10) & 0x3F) >> 3);
	// tx_to_can_pool.data[7] |= (((uint8_t)(rParms. * 10) & 0x3F) >> 3);
#endif
	tx_to_can_pool.data[0] = force;
	tx_to_can_pool.data[1] = (g_CMS_HMW_Enable << 7) + (g_AEB_CMS_Enable << 6) + ((uint8_t)(rParms.CMS_HMW_Warning_Time_Thr * 10) & 0x3F);
	tx_to_can_pool.data[2] = (rParms.switch_g.AEB_TTC_Enable << 7) + (g_CMS_TTC_Enable << 6) + ((uint8_t)(rParms.CMS_HMW_Warning_Time_Thr * 10) & 0x3F);
	tx_to_can_pool.data[3] = (g_HMW_Dynamic_Thr_Enable << 7) + (g_SSS_Enable << 6) + ((uint8_t)(rParms.AEB_TTC_Warning_Time_Level_First * 10) & 0x3F);
	tx_to_can_pool.data[4] = (g_LDW_Enable << 7) + (g_Chassis_Safe_Strategy_Enable << 6) + ((uint8_t)(rParms.AEB_TTC_Warning_Time_Level_Second * 10) & 0x3F);
	tx_to_can_pool.data[5] = (g_AEB_CMS_outside_BrakeBy_CMSHMW << 7)+(g_CMS_ttc_warning << 6)+(g_CMS_hmw_warning << 5)+((uint8_t)(rParms.CMS_HMW_Brake_Time_Thr * 10) & 0x1F);
	tx_to_can_pool.data[6] = (g_AEB_ttc_warning_L2 << 7)+(g_AEB_ttc_warning_L1 << 6)+(g_AEB_CMS_outside_BrakeBy_CMSTTC << 5)+((uint8_t)(rParms.CMS_TTC_Brake_Time_Thr * 10) & 0x1F);
	tx_to_can_pool.data[7] = (g_SSS_outside_isBrake << 7)+(g_SSS_warning << 6)+(g_SSS_warning << 5);

	*tx_frame = tx_to_can_pool;
}

void Urader_CAN_Transmition(struct can_frame *tx_frame,float speed)
{
	tx_camera_input.data[0] = (((uint8_t)(speed)) >> 4) & 0xff; // 低字节在前
	tx_camera_input.data[1] = ((uint8_t)(speed)) & 0xf;
	if(stVehicleParas.LeftFlag == 1)
		tx_camera_input.data[3] = 0x09;
	else if(stVehicleParas.RightFlag == 1)
		tx_camera_input.data[3] = 0x0a;
	else
		tx_camera_input.data[3] = 0;
	*tx_frame = tx_camera_input;
}

void Camera_CAN_Analysis(struct can_frame *rx_frame)
{
	static uint8_t	Lflag = 0;	// 左车道偏离
	static uint8_t	Rflag = 0;	// 右车道偏离

	switch(rx_frame->TargetID){
	case CAMERA_SN_INFO_ID:			camera_sn_info.stream_code = ((rx_frame->data[1] & 0x3f) << 8) | rx_frame->data[0];
									camera_sn_info.day = ((rx_frame->data[2] & 0x07) << 2) | ((rx_frame->data[1] & 0xc0) >> 6);
									camera_sn_info.mon = (rx_frame->data[2] & 0x78) >> 3;
									camera_sn_info.year = ((rx_frame->data[3] & 0x3f) << 1) | ((rx_frame->data[2] & 0x80) >> 7);
									camera_sn_info.factory_code = ((rx_frame->data[4] & 0x03) << 2) | ((rx_frame->data[3] & 0xC0) >> 6) ;
									break;
	case CAMERA_ESSENTIAL_DATA_ID:
		if(Camera_Info_Push_Flag == 0)
		{
			Camera_CIPV_Init();// 当上一周期没有CIPV为1的障碍物时，补发上一周的基础数据。
			camera_data.HMWGrade = 3;
			// CameraMessage.ttc = 0;
			// CameraMessage.hmw = 0;
			Camera_Info_Push(&camera_data, &obstacle_Basic_data, &obstacle_cipv_data);// 解析后的数据提交数据中心
		}else{
			Camera_Info_Push_Flag = 0;
		}

		rx_camera_data = *rx_frame;
		// 0x79f数据解析
		camera_data.LeftLDW 		= (rx_camera_data.data[0] & 0x80) >> 7;
		camera_data.RightLDW 		= (rx_camera_data.data[0] & 0x40) >> 6;
		camera_data.LDWSentivity 	= (rx_camera_data.data[0] & 0x30) >> 4;
		camera_data.Invalid 		= (rx_camera_data.data[0] & 0x0E) >> 1;
		camera_data.OffSound 		= rx_camera_data.data[0] & 0x01;
		camera_data.HMWGrade 		= (rx_camera_data.data[1] & 0xC0) >> 6; // 输出数据不可靠
		camera_data.HMW 			= (float)(rx_camera_data.data[1] & 0x3F)*0.1;
		camera_data.ErrorCode		= (rx_camera_data.data[2] & 0xFE) >> 1;
		camera_data.HMWEnable 		= rx_camera_data.data[2] & 0x01;
		camera_data.FCWLevel 		= (rx_camera_data.data[5] & 0xC0) >> 6;
		camera_data.AmbientLuminance = (rx_camera_data.data[5] & 0x38) >> 3;
		camera_data.FCWStatus 		= (rx_camera_data.data[5] & 0x04) >> 2;
		camera_data.HMWWarning 		= rx_camera_data.data[7] & 0x01;
		break;
	case CAMERA_OBSTACLE_INFO_ID:
		rx_obstacle_info = *rx_frame;
		// 0x7A0数据解析
		obstacle_Basic_data.ObstacleNumber = rx_obstacle_info.data[0];
		obstacle_Basic_data.TimeID = rx_obstacle_info.data[1];
		// fprintf(USART1_STREAM,"Camdz_can=%0.2f\r\n", obstacle_cipv_data.DistanceZ);

		obstacle_Basic_data.HMWAlarmThreshold = (float)(rx_obstacle_info.data[7] & 0x1F)*0.1;
		break;
	case CAMERA_OBSTACLE_INFO_A_ID:
		rx_obstacle_info_a = *rx_frame;
		break;
	case CAMERA_OBSTACLE_INFO_B_ID:

		rx_obstacle_info_b = *rx_frame;

		cipv_check = (rx_obstacle_info_b.data[7] & 0x04) >> 2;
		// 过滤CIPV为1的障碍物
		if(cipv_check == 1)
		{
			stere_camera_data_analysis();
			Camera_Info_Push(&camera_data, &obstacle_Basic_data, &obstacle_cipv_data);// 解析后的数据提交数据中心。
			Camera_Info_Push_Flag = 1;// 发送带有CIPV为1的数据包，并标记数据已发送。

			// turnleft = stVehicleParas.LeftFlagTemp;
			// turnright = stVehicleParas.RightFlagTemp;

			// if(turnleft | turnright)
			// 	ret_pressure = 0;
			// if(stVehicleParas.BrakeFlag == 1)
			// 	ret_pressure = 0;
			// if(warning_status.AEBstatus != 0)
			// 	ret_pressure = 0;
			// if((stVehicleParas.LeftFlagTemp == 0) |
			// 		(stVehicleParas.LeftFlagTemp == 0) |
			// 		(stVehicleParas.BrakeFlag == 0) |
			// 		(warning_status.AEBstatus == 0)
			// 		)
			// if((obstacle_cipv_data.HMW < 2.0) && (obstacle_cipv_data.HMW > 1.5))
			// {
			// 	Camera_Info_Push_Flag = 0;
			// }
			CameraMessage.ttc = obstacle_cipv_data.TTC;
			CameraMessage.hmw = obstacle_cipv_data.HMW;

		}
		else
		{
			CameraMessage.ttc = 6.3;
			CameraMessage.hmw = 6.3;
		}
		break;
	case 0x7A3:
		rx_obstacle_info_tland 			= *rx_frame;
		camera_LDW_data.LeftLaneStyle 	= rx_obstacle_info_tland.data[0] & 0x0F;
		camera_LDW_data.LeftLaneStatus 	= rx_obstacle_info_tland.data[0]& 0x30 >> 4;
		camera_LDW_data.RightLaneStyle 	= rx_obstacle_info_tland.data[1] & 0x0F;
		camera_LDW_data.RightLaneStatus = rx_obstacle_info_tland.data[1]& 0x30 >> 4;
		camera_LDW_data.CarDepth 		= rx_obstacle_info_tland.data[2]*0.01;
		camera_LDW_data.SpeedOfRushLane = (rx_obstacle_info_tland.data[3] + (rx_obstacle_info_tland.data[4]&0x03)<<8)*0.1 - 51.1;
		break;
	case 0x7A4: rx_obstacle_info_lbll = *rx_frame; break;
	case 0x7A5: rx_obstacle_info_lbrl = *rx_frame; break;
	case 0x7A6: rx_obstacle_info_rbll = *rx_frame; break;
	case 0x7A7: rx_obstacle_info_rbrl = *rx_frame; break;
	default:break;
	}
}
