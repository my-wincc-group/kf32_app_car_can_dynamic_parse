/*
 * cqueue.c
 *
 *  Created on: 2022-1-4
 *      Author: Administrator
 */

#include <stdio.h>
#include <stdlib.h>
#include "usart.h"
#include "cqueue.h"
#include <stdbool.h>
#include "EC200U.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "data_loss_prevention.h"

LinkQueue 	dataUp_q ;
/*
 * 初始化队列
 * 参数：队列指针，队列最大容量
 * 说明：开始必然是空队列，队尾指针和队头指针都指向头结点
 */
void initQueue(LinkQueue *queue, uint8_t maxSize)
{
    queue->front = queue->end = 0;

   // 清空内容
   for(uint8_t i=0; i<MAX_QUEUE; i++){
	   memset(&(queue->buf[i].nData), 0, sizeof(NodeData_S));
	   queue->buf[i].has_data = 0;
   }
}
/*
 * 判断队列是否为空
 */
bool isEmpty(LinkQueue *queue)
{
    if(queue->end == queue->front){
    	return true;
    }else{
    	return false;
    }
}

/*
 * 队列还能存储的个数
 */
uint8_t Get_Queue_Can_Store_Number(LinkQueue *queue)
{
	uint8_t diff = 0;
	if(queue->end - queue->front > 0)
		diff = MAX_QUEUE - (queue->end - queue->front);
	else
		diff = MAX_QUEUE - (queue->front - queue->end);
//	 USART1_Bebug_Print_Num("[DIFF]", diff, 1);
	return diff;
}

void Test_Print_CMD_Seq(uint8_t cmd, uint16_t seq)
{
//	USART1_Bebug_Print_Num("[SEND]seq", seq, 0);
//	USART1_Bebug_Print_Num("CMD", cmd, 2);
}
/*
 * 插入队列数据
 * 参数1:队列指针
 * 参数2：插入节点数据
 * 参数3：是否需要回复；false无需回复；true回复
 * 说明：为了数据上报特设定的格式；
 * 	入队,只在一端入队，另一端出队，同样入队不需要判满
 * 	data_len：添加说明：因为与平台交互用的是ASCII码模式，需要有\0结束符，用strlen()计算时会截断，所有要指明长度
 */
void insertQueue(LinkQueue *queue, NodeData_S *nData)
{
    if(Get_Queue_Can_Store_Number(queue) == 0){
#if 0
    	if(nData->is_wait_reply == NEED_WAIT_REPLY){	// 仅存储需要回复的 双指令，单指令直接丢弃
			// 存储到W25Q16外部FLASH中,预留
			Set_One_Data_Loss_Prevention(*nData);
    	}
#else
    	if(nData->is_single_cmd == DOUBLE_CMD && nData->is_store == NEED_STORED){	// 仅存储需要回复的 双指令，单指令直接丢弃
			// 存储到W25Q16外部FLASH中,预留
			Set_One_Data_Loss_Prevention(*nData);
    	}
#endif
    	return ;
    }

    // 赋值
    if(queue->buf[queue->end].has_data){
#if 0
    	if(nData->is_wait_reply == NEED_WAIT_REPLY){	// 仅存储需要回复的 双指令，单指令直接丢弃
			// 存储到W25Q16外部FLASH中,预留
			Set_One_Data_Loss_Prevention(*nData);
    	}
#else
    	if(nData->is_single_cmd == DOUBLE_CMD && nData->is_store == NEED_STORED){	// 仅存储需要回复的 双指令，单指令直接丢弃
			// 存储到W25Q16外部FLASH中,预留
			Set_One_Data_Loss_Prevention(*nData);
    	}
#endif
    }else{
    	memcpy(&(queue->buf[queue->end].nData), nData, sizeof(NodeData_S));
    	queue->buf[queue->end].has_data = 1;
    }

    // 测试输出
//    fprintf(USART1_STREAM, "\r\n*********** insert queue ****start*******\r\n");
//	for(uint16_t i = 0; i<queue->buf[queue->end]->nData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ",queue->buf[queue->end]->nData.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n*********** insert queue ****end*******\r\n");


    if(queue->end >= (MAX_QUEUE-1))  queue->end = 0;
    else queue->end++;

//    USART1_Bebug_Print_Num("[QUEUE INSERT]Size", Get_Queue_Can_Store_Number(queue), 0, 1);
//    USART1_Bebug_Print_Num("front", queue->front, 0, 1);
//    USART1_Bebug_Print_Num("end", queue->end, 1, 1);
}

NodeData_S popQueue(LinkQueue *queue)
{
	return queue->buf[queue->front].nData;
}
/*
 * 删除队头数据，出队
 * 参数：队列指针
 * 说明：出队，需要判空
 */
void deleteQueue(LinkQueue *queue)
{
    Queue q = NULL;

    if (!isEmpty(queue)) {
    	Clear_Queue_One_Buffer_Data(&(queue->buf[queue->front]));

    	if(queue->front >= (MAX_QUEUE-1))  queue->front = 0;
    	else queue->front++;

//        USART1_Bebug_Print_Num("[QUEUE DELETE]Size", Get_Queue_Can_Store_Number(queue), 0, 1);
//        USART1_Bebug_Print_Num("front", queue->front, 0, 1);
//        USART1_Bebug_Print_Num("end", queue->end, 1, 1);
    }
}

//销毁
/*
 * 销毁队列
 * 参数：队列指针
 */
void destoryQueue(LinkQueue *queue)
{
    while(!isEmpty(queue)){
    	deleteQueue(queue);
    }

    USART1_Bebug_Print("[QUEUE DESTORY]", "Queue Destory OK.", 1);
}

void Clear_Queue_One_Buffer_Data(Node * node)
{
   memset(&(node->nData), 0, sizeof(NodeData_S));
   node->has_data = 0;
}

void Send_Single_Data_By_Queue(uint8_t *data)
{
	NodeData_S nodeData_single = {0};

//	USART1_Bebug_Print("CMD", data, 1);

	memset(&nodeData_single, 0, sizeof(NodeData_S));
	sprintf(nodeData_single.data, "%s\r\n", data);
	nodeData_single.data_len = strlen(nodeData_single.data);
	nodeData_single.is_wait_reply = NO_WAIT_REPLY;
	nodeData_single.is_single_cmd = SINGLE_CMD;
	insertQueue(&dataUp_q, &nodeData_single);
}

