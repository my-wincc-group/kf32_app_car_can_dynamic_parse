/*
 * cqueue.h
 *
 *  Created on: 2022-1-4
 *      Author: Administrator
 */

#ifndef CQUEUE_H_
#define CQUEUE_H_
#include "system_init.h"
#include <stdbool.h>
#define NODE_DATAINFO_SIZE		40
#define NODE_DATA_SIZE			600
#define MAX_QUEUE				15

// 单双指令
enum{
	SINGLE_CMD,
	DOUBLE_CMD,
};
// 网络异常后是否存储
enum{
	NOT_STORED,
	NEED_STORED,
};

// 需要/不需要等待回复
enum{
	NO_WAIT_REPLY,
	NEED_WAIT_REPLY,        // 需要回复（需要等待平台回复的指令，需要等待收到Send OK）
	NEED_WAIT_REPLY_REPLY,  // 需要回复， 且是回复平台指令（平台主动请求，设备回复）
};
// 节点数据结构体
typedef struct _Node_Data{
	uint8_t data_info[NODE_DATAINFO_SIZE];
    uint8_t data[NODE_DATA_SIZE];
    volatile uint16_t data_len;				// 数据长度
    volatile uint8_t is_wait_reply;			// 1 need wait;0 no wait;2 reply指令不是request，有状态码
    volatile uint8_t is_single_cmd;			// 1 is single cmd; 0 is double cmd;
    volatile uint8_t is_store;				// 网络异常后，是否存储；0不存，1存
}NodeData_S;

/*
 * 队列的结点结构，特为数据上报设定
 */
typedef struct Node{
	NodeData_S	nData;						// 节点数据
	volatile uint8_t has_data;
} Node, *Queue;

//队列的结构，嵌套
typedef struct{
	Node buf[MAX_QUEUE];
	volatile uint8_t front;
    volatile uint8_t end;
} LinkQueue;


extern LinkQueue 	dataUp_q;


void initQueue(LinkQueue *queue, uint8_t maxSize);
bool isEmpty(LinkQueue *queue);
void insertQueue(LinkQueue *queue, NodeData_S *nData);
void deleteQueue(LinkQueue *queue);
void destoryQueue(LinkQueue *queue);
uint8_t Get_Queue_Can_Store_Number(LinkQueue *queue);
void Test_Print_CMD_Seq(uint8_t cmd, uint16_t seq);
void Clear_Queue_One_Buffer_Data(Node * node);
void Send_Single_Data_By_Queue(uint8_t *data);
NodeData_S popQueue(LinkQueue *queue);

#endif /* CQUEUE_H_ */
