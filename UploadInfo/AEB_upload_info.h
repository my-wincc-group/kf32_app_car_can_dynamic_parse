/*
 * AEB_upload_info.h
 *
 *  Created on: 2022-4-28
 *      Author: 37030
 */

#ifndef AEB_UPLOAD_INFO_H_
#define AEB_UPLOAD_INFO_H_
#include "stdio.h"
#include "aeb_cms_sss_para_config.h"
#if 1
#define ULTR_WAVE_RADAR_NBR			4
#define CAN_USED_NBR				6

#define TTC_WARNING_TYPE				1
#define HMW_WARNING_TYPE				2
//（一）路面感知信息
typedef struct __road_sense_info
{
	float _F_TTC_time;					//TTC时间
	float _F_HMW_time;					//HMW时间
}ROAD_SENSE_INFO;
//（三）状态信息汇总
//1.车身状态信息
typedef struct __vehicl_status_info
{
	volatile uint8_t _U8_speed_can_online_status;					//1.车速CAN在线状态
	volatile uint8_t _U8_brake_light_status;							//2.刹车灯打开
	volatile uint8_t _U8_gear_status;								//(3).档位状态
	volatile uint8_t _U8_steering_light_status;						//4.转向灯状态
	volatile uint8_t _U8_speed_valid_status;							//5车速信息是否有效	申工和才酷的接口，暂时保留项，默认1.
}VEHICL_STATUS_INFO;
//2.AEB状态信息
typedef struct __aeb_status_info
{
	volatile uint8_t  _U8_AEB_on_off_status;							//1.AEB开关状态
	volatile uint8_t  _U8_LDW_on_off_status;							//2.LDW开关状态
	volatile uint8_t  _U8_AEB_switch_source;							//3.AEB开关源
	volatile uint8_t  _U8_LDW_switch_source;							//4.LDW开关源
	volatile uint8_t  _U8_AEB_brake_status;							//(5).AEB制动状态
	volatile uint8_t  _U8_event_status;								//(6).事件的开始与结束
	volatile uint8_t  _U8_event_type;								//(7).事件分类 【1：fcw】、【2：hmw】、【3：ldw】、【4：aeb】、【5：司机踩下刹车】 【6：角雷达】
	volatile uint8_t  _U8_FCW_level;									//(8).FCW报警等级
	volatile uint8_t  _U8_HMW_level;									//(9).HMW报警等级
	volatile uint8_t  _U8_LDW_action;								//(10)LDW预警动作	0未触发 		1：左 	2：右
	volatile uint8_t  _U8_BSD_level;									//(11).BSD报警等级
	volatile uint8_t  _U8_BSD_warning_direction;									//(12).BSD报警方向
}AEB_STATUS_INFO;
//3.外设状态信息
	//1)相机状态
typedef struct __camera_status_info
{
	uint8_t _U8_online_status;								//1.相机在线状态
	uint8_t _U8_ambientLuminance_status;		   			//2.相机外部环境亮度
}CAMERA_STATUS_INFO;
	//2)比例阀状态
typedef struct __proporvalve_status_info
{
	uint8_t _U8_online_status;								//1.比例阀在线状态
}PROPOR_VALVE_STATUS_INFO;
	//3)超声波状态
typedef struct __ult_radar_status_info
{
	uint8_t _U8_online_status;								//1.超声波 在线状态
}ULT_RADAR_STATUS_INFO;
	//4)毫米波状态
typedef struct __mmw_radar_status_info
{
	uint8_t _U8_online_status;								//1.毫米波 在线状态
}MMW_RADAR_STATUS_INFO;
	//5)小屏幕状态
typedef struct __displayer_status_info
{
	uint8_t _U8_online_status;								//1.小屏幕在线状态
}DISPLAYER_STATUS_INFO;
	//6)4G状态
typedef struct __4g_status_info
{
	uint8_t _U8_online_status;								//1.4G 在线状态
}_4G_STATUS_INFO;
	//7)GPS状态
typedef struct __gps_status_info
{
	uint8_t _U8_online_status;								//1.GPS 在线状态
}GPS_STATUS_INFO;
	//8)BT状态
typedef struct __bt_status_info
{
	uint8_t _U8_online_status;								//1.BT 在线状态		保留项   参数默认为：0
}BT_STATUS_INFO;
typedef struct __peripheral_status_info										//3.外设状态信息
{
	CAMERA_STATUS_INFO 			_ST_camre;										//1)相机状态
	PROPOR_VALVE_STATUS_INFO 	_ST_propovavle;									//2)比例阀状态
	ULT_RADAR_STATUS_INFO		_ST_ult_radar;									//3)超声波状态
	MMW_RADAR_STATUS_INFO		_ST_mmw_radar;									//4)毫米波状态
	DISPLAYER_STATUS_INFO		_ST_displayer;									//5)小屏幕状态
	_4G_STATUS_INFO				_ST_4g;											//6)4G状态
	GPS_STATUS_INFO				_ST_gps;										//7)GPS状态
	BT_STATUS_INFO				_ST_bt;											//8)BT状态
}PERIPHERAL_STATUS_INFO;

typedef struct __status_info											//（三）状态信息汇总
{
	VEHICL_STATUS_INFO 			_ST_vehicle_status;							//1.车身状态
	AEB_STATUS_INFO 			_ST_AEB_status;								//2.AEB状态信息
	PERIPHERAL_STATUS_INFO 		_ST_perph_status;							//3.外设状态信息
}STATUS_INFO;

//（四）故障信息
//1.车身与控制器故障
typedef struct _fault_vehicle
{
	uint8_t _U8_fault_vehicle;								//1.车身故障			保留项   参数默认为：0
	uint8_t _U8_fault_controller;							//2.控制器故障		保留项   参数默认为：0
}FAULT_VEHICLE;
//2.CAN通讯故障
typedef struct _fault_can
{
	uint8_t _U8_fault_can_bus_status;						//1.CAN总线状态
	uint8_t _U8_fault_can_err_status;						//2.CAN错误状态
	uint8_t _U8_fault_can_err_code;							//3.CAN错误代码
	uint8_t _U8_fault_can_err_direction;					//4.CAN错误方向
}FAULT_CAN;
//3.外设故障
typedef struct _fault_periph
{
	uint8_t _U8_periphfault_camera;							//1.相机故障
	uint8_t _U8_periphfault_proporvalve;					//2.比例阀故障
	int8_t _U8_periphfault_ult_radar[MAX_ULT_RADAR_NBR];	//3.超声波雷达故障
	uint8_t _U8_periphfault_MMW_radar;						//4.毫米波雷达故障		保留项，默认固定参数：0
	uint8_t _U8_periphfault_displayer;						//5.小屏幕故障			保留项，默认固定参数：0
	uint8_t _U8_periphfault_4G_COMM;						//6.4G通讯故障			保留项，默认固定参数：0
	uint8_t _U8_periphfault_GPS_aerial;						//7.GPS通讯故障			保留项，默认固定参数：0
	uint8_t _U8_periphfault_BT;								//8.蓝牙故障				保留项，默认固定参数：0
	uint8_t _U8_periphfault_IMU;								//9.IMU故障
}FAULT_PERIPH;
typedef struct _fault_info												//（四）故障信息
{
	FAULT_VEHICLE 				_ST_vehicle;							//1.车身与控制器故障
	FAULT_CAN 					_ST_can[CAN_USED_NBR];					//2.CAN通讯故障
	FAULT_PERIPH				_ST_periph;								//3.外设故障
}FAULT_INFO;

//（五）AEB制动信息

typedef struct _ult_radar_brake
{
//	uint8_t _ARRY_ult_brake[ULTR_WAVE_RADAR_NBR];						//超声波雷达制动
	uint8_t _U8_ult_id;//[ULTR_WAVE_RADAR_NBR];						//超声波雷达制动
}ULT_RADAR_BRAKE;

typedef struct _brake_info
{
	ULT_RADAR_BRAKE _ST_ult_radar_brake_info;								//超声波雷达制动信息
}BRAKE_INFO;
typedef struct _aeb_upload_info
{
	ROAD_SENSE_INFO	_ST_road_info;											//（一）路面感知信息
	STATUS_INFO _ST_status;													//（三）状态信息汇总
	FAULT_INFO _ST_fault;													//（四）故障信息
	BRAKE_INFO _ST_brake;													//（五）AEB制动信息
	volatile uint8_t   event_id;
}AEB_UPLOAD_INFO;

#define AEB_SOURCE_DISPLAYER		1
#define AEB_SOURCE_PC_AT			2
#define AEB_SOURCE_BT				3
#define AEB_SOURCE_GPIO				4

typedef struct _status_collection_cache
{
	uint8_t u8_event_status;
	uint8_t u8_event_type;
	uint8_t u8_HMW_level;
	uint8_t u8_AEB_key_source;
	uint8_t u8_LDW_key_source;
	uint8_t u8_proportional_valve_fault_code;
	FAULT_CAN st_can_status_detection_cache[CAN_USED_NBR];
}STATUS_CACHE;
//设置AEB的开关源
void set_AEB_key_source(uint8_t value);
//设置LDW的开关源
void set_LDW_key_source(uint8_t value);
//设置比例阀错误码
void set_proportional_valve(uint8_t value);
//设置HMW报警等级
//参数：报警等级
void set_hmw_level(uint8_t hmw_level);
//调用全部数据的结构的操作接口，刷新报警事件缓存数据到接口中
AEB_UPLOAD_INFO *other_user_get_upload_info();
//调用全部数据的结构的操作接口，不刷新报警事件的缓存信息
AEB_UPLOAD_INFO * other_user_get_upload_info_no_refresh_alarm_event();
//测试函数
void test_func();
//版本信息刷新测试函数
void refresh_test(STREAM* USART_Print,Device_Version_Struct *p_st_ver);
//*************以下函数需放入主循环中调用，实时检测*********************
//初始报警事件缓存
void InitWaringEventCache();
//主循环轮询检测所有CAN状态调用函数
void check_all_can_status();
//判断所有报警事件是否发生并需要插入缓存,放到循环中去调用，实时检测
void judgement_all_warning_event_is_need_to_be_insert();
//判断车身触发动作是否发生
bool judge_vehiclebody_trigger_action_does_it_happen();
//判断LDW预警动作
bool judge_LDW_event_trigger_action();
//刷新系统版本信息
//参数：待刷新的对象地址
void refresh_sys_version_info(Device_Version_Struct *p_st_ver);
//设置刷新版本状态标志位
void set_refresh_flag(bool val);
//获取刷新版本状态标志位
bool get_refresh_flag();
//2022-08-11
bool judge_trigger_action(uint8_t cur_action,uint8_t orignl_action,int64_t waittime_ms,uint8_t *p_ctrl_stat,int64_t *p_cur_time);

//gcz   2022-06-03 开放工具函数
//查找指定位置，指定字符的返回字符串
//参数：输入字符串    目标字符  需要查找的索引个数
//返回：目标字符为开始的字符串
char* FindIndexPosStrng(char *pInBuffer,char cTargetChar,unsigned int IndexNbr);
void TrimHeadTailCRLF(char* cContent);		/* 去掉头尾回车换行 */
void TrimLeftBlank(char* cContent);		/* 去掉字符串左侧空格 */
#endif
#endif /* AEB_UPLOAD_INFO_H_ */
