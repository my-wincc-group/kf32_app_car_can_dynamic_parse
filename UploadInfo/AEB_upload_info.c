/*
 * AEB_upload_info.c
 *
 *  Created on: 2022-4-28
 *      Author: gcz
 */
#include "AEB_upload_info.h"
#include "string.h"
#include "can_task.h"
#include "data_center_services.h"
#include "stereo_camera.h"
#include "common.h"
#include "aeb_cms_sss_para_config.h"
#include "canhl.h"
#include "upgrade_common.h"
#include "aebs_version_management.h"
#include "aeb_sensor_management.h"
#include "AEB_CMS_alg.h"
#include "tool.h"
#include "imu_app.h"

//FAULT_CAN g_st_can_status_detection_cache[CAN_USED_NBR];
//uint8_t g_u8_proportional_valve_fault_code = 0;
static STATUS_CACHE  					s_st_collection_status_cache;
extern _STRUCT_CAN_COMM_STA 			stCanCommSta;
extern Camera_Essential_Data 			camera_data;
extern uint8_t 							g_AEB_CMS_Enable;
extern uint8_t 							g_LDW_Enable;
extern KunLun_AEB_Para_Cfg 	wParms;
extern URADER_MESSAGE					detection_Urader_Company_Message[2];
extern Warning_Event_S 	fcw;
extern Warning_Event_S 	hmw_L1;
extern Warning_Event_S 	hmw_L2;
extern Warning_Event_S 	ldw;
extern Warning_Event_S 	aeb_CAM;
extern Warning_Event_S 	aeb_MMW;
extern Warning_Event_S 	aeb_ULT;
extern Warning_Event_S	driver_brake;
extern Warning_Event_S	aeb_bsd;
extern Warning_Event_S	aeb_bsd_L1;
extern Warning_Event_S 	aeb_bsd_L2;
extern Warning_Event_S 	aeb_bsd_L3;
extern uint8_t g_AEB_CMS_outside_BrakeBy_AEBTTC;
extern uint8_t g_AEB_CMS_outside_BrakeBy_CMSTTC;
extern uint8_t g_AEB_CMS_outside_BrakeBy_CMSHMW;
extern uint8_t g_AEBS_outside_Test_Brake_Warning;


static AEB_UPLOAD_INFO 					s_st_aeb_upload_info;
static uint8_t 							g_ult_frame_comm_status[2] = {OFFLINE,OFFLINE};

//CAN总线状态
#define CAN_BOFF_BUS_ON				0			//总线开启
#define CAN_BOFF_BUS_OFF			1			//总线关闭

//CAN出错状态
#define CAN_CERROR_FALSE			0			//无错
#define CAN_CERROR_TRUE				1			//出措

enum
{
	ACTION_CLOSE = 0,
	ACTION_OPEN,
	ACTION_OPEN_UNREMITTING,
	ACTION_WAIT_CLOSE,
};


void generate_upload_info();
bool judge_LDW_event_trigger_action();

//----------------------------工具函数-------------------------
//查找指定位置，指定字符的返回字符串
//参数：输入字符串    目标字符  需要查找的索引个数
//返回：目标字符为开始的字符串
char* FindIndexPosStrng(char *pInBuffer,char cTargetChar,unsigned int IndexNbr)
{
	unsigned int len = strlen(pInBuffer),i1,cnt = 0,res = FALSE;
	for (i1 = 0;i1 < len;i1++)
	{
		if (pInBuffer[i1] == cTargetChar)
		{
			cnt++;
		}
		if (cnt == IndexNbr)
		{
			res = TRUE;
			break;
		}
	}
	if (res == TRUE)
	{
		return (pInBuffer + i1);
	}
	else
	{
		return NULL;
	}

}
void TrimHeadTailCRLF(char* cContent)		/* 去掉头尾回车换行 */
{
	int i1,length;
	length = strlen(cContent);
	if ((cContent[0] == '\r') && (cContent[1] == '\n')
		&& (cContent[length - 2] == '\r') && (cContent[length - 1] == '\n'))
	{
		for(i1 = 2; i1 < length - 2; i1++)
		{
			*(cContent+i1 - 2) = *(cContent+i1) ;
		}
		*(cContent+i1) = 0;
	}
}

void TrimLeftBlank(char* cContent)		/* 去掉字符串左侧空格 */
{
	int i1,length;
	length = strlen(cContent);

	for(i1 = 0; i1 < length; i1++)
	{
		if(*(cContent+i1) != ' ')
			break;
	}
	strcpy(cContent,cContent+i1);
	for(i1 = strlen(cContent); i1 < length; i1++)
	{
		*(cContent+i1) = 0;
	}
}
//-------------一、内部调用程序--------------------------------------------------------------------------------------------------------------------
//-------------1.CAN状态检测-----------------------------------
//将CAN状态从实时检测缓存，导入最终操作提供的接口结构中
static void insert_can_comm_status_info(FAULT_CAN * p_can_status_obj,FAULT_CAN *p_can_status_cache)
{
	memcpy(p_can_status_obj,p_can_status_cache,sizeof(FAULT_CAN));
}
//检测CANx的错误状态
static void check_canx_status(CAN_SFRmap* CANx,FAULT_CAN *p_can_status_cache)
{
	CAN_ErrorTypeDef l_canErrorStruct;
	unsigned char tx_err_count = 0,rx_err_count = 0;
	//判断总线状态
	if (CAN_Get_Transmit_Status(CANx,CAN_BUS_OFF_STATUS) == SET)
		p_can_status_cache->_U8_fault_can_bus_status = CAN_BOFF_BUS_OFF;
	else
		p_can_status_cache->_U8_fault_can_bus_status = CAN_BOFF_BUS_ON;
	//判断错误状态
	if (CAN_Get_Transmit_Status(CANx,CAN_ERROR_STATUS) == SET)
		p_can_status_cache->_U8_fault_can_err_status = CAN_CERROR_TRUE;
	else
		p_can_status_cache->_U8_fault_can_err_status = CAN_CERROR_FALSE;

	//错误具体判断
	if (p_can_status_cache->_U8_fault_can_err_status == CAN_CERROR_FALSE)
	{
		p_can_status_cache->_U8_fault_can_err_code = 0;
		p_can_status_cache->_U8_fault_can_err_direction = 0;
	}
	else
	{
		CAN_Get_Error_Code(CANx,&l_canErrorStruct);
		p_can_status_cache->_U8_fault_can_err_code = l_canErrorStruct.m_ErrorCode + 1;
		p_can_status_cache->_U8_fault_can_err_direction = l_canErrorStruct.m_ErrorDirection + 1;
	}

//	tx_err_count = CAN_Get_Error_Counter(CAN0_SFR + sizeof(CAN_SFRmap) * i1,CAN_ERROR_AT_TX);//发送时错误计数
//	rx_err_count = CAN_Get_Error_Counter(CAN0_SFR + sizeof(CAN_SFRmap) * i1,CAN_ERROR_AT_RX);//接收时错误计数
}
//------------2.超声波数据帧状态检测及判定-----------------------------------------------------------------

//超声波雷达通讯帧700和701检测
static uint8_t check_ult_comm_single_frame_status(uint8_t frame_id,uint32_t ctrl_times)
{
	uint8_t res = OFFLINE;
	if (g_upload_info_check_ult_radar_exist_hearbeat_cnt[frame_id] >= 1)
	{
		g_upload_info_check_ult_radar_exist_hearbeat_cnt[frame_id]++;//每次CAN中断接收到会复位1，当通讯异常断帧，此处每次都会自增，自增到目标次数，即可标记异常状态
		if (g_upload_info_check_ult_radar_exist_hearbeat_cnt[frame_id] >= ctrl_times)
		{
			g_upload_info_check_ult_radar_exist_hearbeat_cnt[frame_id] = 0;
		}
		else
			res = ONLINE;
	}

	return res;
}
//本函数需要放到ms,s等时间可控处，该函数需要定时计数
//参数：需要计数的总次数
static void check_ult_comm_frame_status(uint32_t ctrl_times)
{
	g_ult_frame_comm_status[ID_700] = check_ult_comm_single_frame_status(ID_700,ctrl_times);
	g_ult_frame_comm_status[ID_701] = check_ult_comm_single_frame_status(ID_701,ctrl_times);
}
//复位检测结构数据，需根据check_ult_comm_frame_status()函数的返回值进行判定，放在其后面执行
static void reset_detection_struct_data()
{
	if (g_ult_frame_comm_status[ID_700] == OFFLINE)
	{
		uint8_t i1;
		for (i1 = 0;i1 < 4;i1++)
		{
			detection_Urader_Company_Message[0].Urader_work_stata[i1] = NOURADER;
			detection_Urader_Company_Message[0].distance[i1] = 0;
		}
	}

	if (g_ult_frame_comm_status[ID_701] == OFFLINE)
	{
		uint8_t i1;
		for (i1 = 4;i1 < 12;i1++)
		{
			detection_Urader_Company_Message[0].Urader_work_stata[i1] = NOURADER;
			detection_Urader_Company_Message[0].distance[i1] = 0;
		}
	}
}
//check_ult_comm_frame_status()与 reset_detection_struct_data()的标准用法，放到主循环的位置
//参数：时间间隔，单位ms
static void check_ult_frame_status(uint32_t interval)
{
	static uint32_t previousMillis = 0;
	if(SystemtimeClock - previousMillis >= interval)
	{
		previousMillis = SystemtimeClock;
		check_ult_comm_frame_status(1000/interval);//检测时间片是否超时，生成每一帧的状态，超时时间为1s
		reset_detection_struct_data();		//根据帧状态，进行侦测是否需要复位异常帧的所有雷达状态
	}
}
//--------------------------------------------------------------------------------------

//--------------3.超声波状态判定生成判定帧************************

//获取超声波雷达状态信息
static int8_t get_ult_radar_info(uint8_t index)
{
	int8_t res;
	if (detection_Urader_Company_Message[0].Urader_work_stata[index] == ACCESSIBILITY)//未检测到障碍物
		res = -3;
	else if (detection_Urader_Company_Message[0].Urader_work_stata[index] == NOURADER)//未插入雷达
		res = -2;
	else if (detection_Urader_Company_Message[0].Urader_work_stata[index] == NOWORK)//雷达不需要工作
		res = -1;
	else
		res = 0;
	return res;
}

//函数说明 : 获取超声波雷达的外设错误
//参数 : 超声波的
//返回:	无
static void get_ult_radar_periph_code(unsigned char *pOutBuffer,uint8_t Nbr)
{
	int8_t res;
	uint8_t i1;
	for (i1 = 0;i1 < Nbr;i1++)
	{
		if (wParms.switch_g.ultrasonic_radar_monitor_ensble[i1] == 0)//未开启侦测开关，则上报0
			pOutBuffer[i1] = 0;
		else
		{
			if ((res = get_ult_radar_info(i1)) != 0)
				pOutBuffer[i1] = res;
			else
				pOutBuffer[i1] = 1;
		}
	}
}
//获取车辆的转向状态
static uint8_t get_steer_sigl_status()
{
	uint8_t res = 0;
	if (stVehicleParas.LeftFlag == 1)
		res = 1;
	else if (stVehicleParas.RightFlag == 1)
		res = 2;
	return res;
}
//获取档位状态码
static uint8_t get_gear_status()
{
	if (stVehicleParas.ReverseGear == -1)
		return 0;
	else
		return (stVehicleParas.ReverseGear == 1) ? 3 : 5;
}
//获取HWM预警等级
uint8_t get_hmw_warining_level()
{
	uint8_t hmw_level = 0;
	//HMW
	//gcz 2022-05-29	stVehicleParas.Car_Gear的用法不对，目前GPIO或CAN检测到的倒挡状态都是使用stVehicleParas.ReverseGear
	if((stVehicleParas.fVehicleSpeed > 0) && (stVehicleParas.ReverseGear != 1/*stVehicleParas.Car_Gear != 3*/)){

		if(g_AEB_CMS_outside_BrakeBy_CMSHMW == 0x01){
			//gcz 2022-05-19
			hmw_level = 2;
			//fprintf(USART1_STREAM,"hmw_brake:1\r\n");
		}else if(g_CMS_hmw_warning == 0x01){
			//gcz 2022-05-19
			hmw_level = 1;
			//fprintf(USART1_STREAM,"hmw_warning:1\r\n");
		}else if(displayer_show_info.HMWGrade != 0){
		//	displayer_tx_frame.data[2] |= 3 << 6; // HMW_Warning Level 1 just warning
		}
	}
	//gcz 2022-05-19
	set_hmw_level(hmw_level);
	return hmw_level;
}
//获取FCW预警等级
uint8_t get_fcw_warning_level()
{
	uint8_t fcw_level = 0;
	if(g_AEB_CMS_outside_BrakeBy_AEBTTC == 0x01 || g_AEB_CMS_outside_BrakeBy_CMSTTC == 0x01 || g_AEBS_outside_Test_Brake_Warning == 0x01){
		fcw_level = 2;
	}else if(g_CMS_ttc_warning == 0x01 || g_AEB_ttc_warning_L1 == 0x01 || g_AEB_ttc_warning_L2 == 0x01){
		fcw_level = 1;
	}
	return fcw_level;
}

//---------------4.报警开始结束状态与事件类型的判断及缓存处理------------------------------------------------
#define WARNING_EVENT_CACHE_NBR					64
typedef struct __warning_event_info
{
	TRIGGER_EVENT event;
	bool startEnd;
	uint16_t event_id;
	float warning_time;
	uint8_t warning_time_type;
	uint8_t warning_level;
	uint8_t Flag;
}WARN_EVENT_INFO;
typedef struct __waring_event
{
	WARN_EVENT_INFO _ST_warning_event_cache[WARNING_EVENT_CACHE_NBR];
	uint8_t PutPtr;
	uint8_t GetPtr;
}WARING_EVENT_CACHE;
static WARING_EVENT_CACHE s_st_warning_event_info_cache;
//初始报警事件缓存
void InitWaringEventCache()
{
	int8_t i1;
	s_st_warning_event_info_cache.GetPtr = 0;
	s_st_warning_event_info_cache.PutPtr = 0;
	for (i1 = 0;i1 < WARNING_EVENT_CACHE_NBR;i1++)
	{
		s_st_warning_event_info_cache._ST_warning_event_cache[i1].event = NO_EVENT;
		s_st_warning_event_info_cache._ST_warning_event_cache[i1].startEnd = FALSE;
		s_st_warning_event_info_cache._ST_warning_event_cache[i1].event_id = 0;
		s_st_warning_event_info_cache._ST_warning_event_cache[i1].Flag = FALSE;
	}
}
//插入报警事件到缓存队列
//参数：缓存事件类型  开始或结束状态    上报的ID号
static void insert_waring_event_to_cache(TRIGGER_EVENT event,bool startEnd,uint16_t event_id,float warning_time,uint8_t warning_time_type,uint8_t warning_level)
{
	if (s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.PutPtr].Flag == FALSE)
	{
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.PutPtr].event_id = event_id;
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.PutPtr].startEnd = startEnd;
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.PutPtr].event = event;
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.PutPtr].Flag = TRUE;
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.PutPtr].warning_time = warning_time;
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.PutPtr].warning_time_type = warning_time_type;
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.PutPtr].warning_level = warning_level;
	//	fprintf(USART1_STREAM,"CACHE_PUT:%d  event_id:%d  startEnd:%d  event:%d\r\n",s_st_warning_event_info_cache.PutPtr,s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].event_id,s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].startEnd,s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].event);
		s_st_warning_event_info_cache.PutPtr++;
		if (s_st_warning_event_info_cache.PutPtr >= WARNING_EVENT_CACHE_NBR)
			s_st_warning_event_info_cache.PutPtr = 0;
	}
}
//查询数据从队列中获取到
//参数：获取到的缓存值
//返回：是否获取成功
static bool query_waring_event_from_cache(WARN_EVENT_INFO *pStWarningEvent)
{
	bool res = FALSE;
	if (s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].Flag == TRUE)
	{
	//	fprintf(USART1_STREAM,"cache_get:%d  event_id:%d  startEnd:%d  event:%d\r\n",s_st_warning_event_info_cache.GetPtr,s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].event_id,s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].startEnd,s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].event);
		res = TRUE;
		memcpy(pStWarningEvent,&s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr],sizeof(WARN_EVENT_INFO));
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].Flag = FALSE;
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].event = NO_EVENT;
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].startEnd = FALSE;
		s_st_warning_event_info_cache._ST_warning_event_cache[s_st_warning_event_info_cache.GetPtr].event_id = 0;
		s_st_warning_event_info_cache.GetPtr++;
		if (s_st_warning_event_info_cache.GetPtr >= WARNING_EVENT_CACHE_NBR)
			s_st_warning_event_info_cache.GetPtr = 0;
	}
	return res;
}
//判断报警事件的发生并插入缓存
//说明：参考Upload_Warn_Condition_Json()函数，剔除里面生成与发生数据部分程序，改为报警事件入缓存
static void judgement_warning_event_for_insert(uint8_t condition, Warning_Event_S *warn_s, TRIGGER_EVENT event,
		float warning_time,uint8_t warning_time_type,uint8_t warning_level)
{
//	gcz_serial_v1_dma_printf(SERIAL_UART1,"2-.[sys_clk]:%d ",SystemtimeClock);
//	gcz_serial_v1_dma_printf(SERIAL_UART1,"FFF---%04X event:  %d  %d\r\n",warn_s,event,condition);
	if(condition && !warn_s->isStart)
	{			// start
		warn_s->isStart = TRUE;
		insert_waring_event_to_cache(event, 1, warn_s->cnt,warning_time,warning_time_type,warning_level);
//		fprintf(USART1_STREAM,"2.[sys_clk]:%d ",SystemtimeClock);
//
//		fprintf(USART1_STREAM,"SSS---event:  %d\r\n",event);
	}
	else if(!condition && warn_s->isStart)
	{
//		fprintf(USART1_STREAM,"2.[sys_clk]:%d ",SystemtimeClock);
//		fprintf(USART1_STREAM,"EEE---ewvent:  %d %d  %d\r\n",event,condition,warn_s->isStart);
		// end
		warn_s->isStart = FALSE;

		if(warn_s->cnt == 255)
		{				// 循环发送，重新从0开始发送数据
			insert_waring_event_to_cache(event, 0, warn_s->cnt,warning_time,warning_time_type,warning_level);
			warn_s->cnt = 0;
		}
		else
		{
			insert_waring_event_to_cache(event, 0, warn_s->cnt,warning_time,warning_time_type,warning_level);
			warn_s->cnt++;
		}

	}
}
//设置报警事件类型与动作状态
//参数：报警事件类型		开始或结束
static void set_event_warningtype_isbegin(uint8_t warningtype,uint8_t isbegin)
{
	s_st_collection_status_cache.u8_event_type 		= 	warningtype;
	s_st_collection_status_cache.u8_event_status 	= 	isbegin;
}
//获取报警事件到缓存
static AEB_UPLOAD_INFO *get_warning_event_and_all_data()
{
	WARN_EVENT_INFO l_st_event;
	if (query_waring_event_from_cache(&l_st_event) == TRUE)
	{
		set_event_warningtype_isbegin(l_st_event.event,l_st_event.startEnd);
		s_st_aeb_upload_info.event_id = l_st_event.event_id;
		if (l_st_event.warning_time_type == TTC_WARNING_TYPE)
		{
			s_st_aeb_upload_info._ST_road_info._F_TTC_time = l_st_event.warning_time;
			s_st_aeb_upload_info._ST_road_info._F_HMW_time = 6.3;
		}
		else
		{
			s_st_aeb_upload_info._ST_road_info._F_TTC_time = 6.3;
			s_st_aeb_upload_info._ST_road_info._F_HMW_time = l_st_event.warning_time;
		}
		if (l_st_event.event == FCW_EVENT)
			s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_FCW_level = l_st_event.warning_level;
		else if (l_st_event.event == HMW_EVENT)
			s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_HMW_level = l_st_event.warning_level;
		else if (l_st_event.event == BSD_EVENT)
			s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_BSD_level = l_st_event.warning_level;
	}
	else
		set_event_warningtype_isbegin(0,0);
	generate_upload_info();
	return &s_st_aeb_upload_info;
}
//判断刹车是否踩下动作
static bool judge_brake_event_trigger_action()
{
	bool res = TRUE;
	static uint8_t m_brake_ctrl_state = ACTION_CLOSE;
	switch (m_brake_ctrl_state)
	{
	case ACTION_CLOSE:  				if (stVehicleParas.BrakeFlag != 0)
										{
											m_brake_ctrl_state = ACTION_OPEN;
										}
										else
											res = FALSE;
										break;
	case ACTION_OPEN:					if (stVehicleParas.BrakeFlag == 0)
										{
											res = FALSE;
											m_brake_ctrl_state = ACTION_CLOSE;
										}
										break;
	default:	res = FALSE;
				m_brake_ctrl_state = ACTION_CLOSE;
				break;
	}
	return res;
}
//----------------------二、外部调用函数--------------------------------------------------------------------
//主循环轮询检测所有CAN状态调用函数
void check_all_can_status()
{
	uint8_t i1;
	CAN_ErrorTypeDef l_canErrorStruct;
	unsigned char tx_err_count = 0,rx_err_count = 0;

	for (i1 = 0;i1 < 6;i1++)
	{
		check_canx_status(CAN0_SFR + sizeof(CAN_SFRmap) * i1,&s_st_collection_status_cache.st_can_status_detection_cache[i1]);
	}
}

//判断所有报警事件是否发生并需要插入缓存,放到循环中去调用，实时检测
void judgement_all_warning_event_is_need_to_be_insert()
{
	//gcz 2022-08-31 增加g_AEB_ttc_warning_L2的数据上报触发 以及预警信号过滤保持
	static uint8_t ctrl_state_fcw = ACTION_CLOSE,ctrl_state_hmw = ACTION_CLOSE;
	static int64_t cur_time_fcw = 0,cur_time_hmw = 0;


//	bool res_fcw = judge_trigger_action(g_CMS_ttc_warning || g_AEB_ttc_warning_L1 || g_AEB_ttc_warning_L2,false,100,&ctrl_state_fcw,&cur_time_fcw);
//	bool res_hmw = judge_trigger_action(g_CMS_hmw_warning,false,100,&ctrl_state_hmw,&cur_time_hmw);
	judgement_warning_event_for_insert(g_CMS_ttc_warning || g_AEB_ttc_warning_L1 || g_AEB_ttc_warning_L2, &fcw, FCW_EVENT,camera_share.ObsInormation.TTC,TTC_WARNING_TYPE,get_fcw_warning_level());
	judgement_warning_event_for_insert(g_CMS_hmw_warning, &hmw_L1, HMW_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE,get_hmw_warining_level());
	//gcz 2022-08-31 修改触发源为g_AEB_CMS_outside_BrakeBy_CMSHMW
	judgement_warning_event_for_insert(g_AEB_CMS_outside_BrakeBy_CMSHMW, &hmw_L2, HMW_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE,get_hmw_warining_level());
	//gcz 2022-08-11
	SRR_Obstacle_Paras m_srr_para = getSrrWarningAndZoneParas();
	static uint8_t ctrl_state_L1 = ACTION_CLOSE,ctrl_state_L2 = ACTION_CLOSE,ctrl_state_L3 = ACTION_CLOSE;
	static int64_t cur_time_L1 = 0,cur_time_L2 = 0,cur_time_L3 = 0;
	//yuhong 20220816 先保留一级预警，其余预警关闭
	bool res_L1 = judge_trigger_action(m_srr_para.levelOneValid,false,500,&ctrl_state_L1,&cur_time_L1);
	//bool res_L2 = judge_trigger_action(m_srr_para.levelTwoValid,false,500,&ctrl_state_L2,&cur_time_L2);
	//bool res_L3 = judge_trigger_action(m_srr_para.levelThreeValid,false,500,&ctrl_state_L3,&cur_time_L3);

//	fprintf(USART1_STREAM,"1st:%d %d\r\n2nd:%d %d\r\n3th:%d %d\r\n",res_1st,m_srr_para.levelOneValid,
//																	res_2st,m_srr_para.levelTwoValid,
//																	res_3st,m_srr_para.levelThreeValid);
	//yuhong 20220816 先保留一级预警，其余预警关闭

	judgement_warning_event_for_insert(res_L1, &aeb_bsd_L1, BSD_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE,m_srr_para.warningLevel);
	//judgement_warning_event_for_insert(res_L2, &aeb_bsd_L2, BSD_EVENT);
	//judgement_warning_event_for_insert(res_L3, &aeb_bsd_L3, BSD_EVENT);

	//gcz 2022-05-30 修改LDW报警动作判断机制，开始触发后，直到结束才触发结束
//	judgement_warning_event_for_insert(camera_data.LeftLDW || camera_data.RightLDW, &ldw, LDW_EVENT);
	judgement_warning_event_for_insert(judge_LDW_event_trigger_action(), &ldw, LDW_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE,0);///*camera_data.LeftLDW || camera_data.RightLDW*//*(displayer_show_info.LDW==1) || (displayer_show_info.LDW==2)*/
//	judgement_warning_event_for_insert(judge_brake_event_trigger_action(), &driver_brake, DRIVER_BRAKE_EVENT);
	static uint8_t brake_state = 0;	// 开始时AEB状态

	//gcz 2022-09-01 增加预警数据时间控制
	float warning_time;
	uint8_t waring_time_type;
	static uint8_t BrakeBy_CMSHMW = 0,BrakeBy_CMSTTC = 0,AEBTTC = 0;
	//刹车不断，一直缓存时间，用于最后结束时传入时间参数
	if (BrakeBy_CMSHMW == 1)
	{
		warning_time = camera_share.ObsInormation.HMW;
		waring_time_type = HMW_WARNING_TYPE;

	//	fprintf(USART1_STREAM,"1.-%0.2f %d----------\r\n",warning_time,waring_time_type);
	}
	else if (BrakeBy_CMSTTC == 1)
	{
		warning_time = camera_share.ObsInormation.TTC;
		waring_time_type = TTC_WARNING_TYPE;
	//	fprintf(USART1_STREAM,"2.-%0.2f %d----------\r\n",warning_time,waring_time_type);
	}
	else if (AEBTTC == 1)
	{
		warning_time = camera_share.ObsInormation.TTC;
		waring_time_type = TTC_WARNING_TYPE;
	}
	//刹车预警触发时，记录触发的预警时间类型及时间，用于后面缓存传入
	if (g_AEB_CMS_outside_BrakeBy_CMSHMW == 1)
	{
		warning_time = camera_share.ObsInormation.HMW;
		waring_time_type = HMW_WARNING_TYPE;
		BrakeBy_CMSHMW = g_AEB_CMS_outside_BrakeBy_CMSHMW;
	//	gcz_serial_v1_dma_printf(SERIAL_UART1,"[1].-%0.2f %d----------\r\n",warning_time,waring_time_type);
	}
	else if (g_AEB_CMS_outside_BrakeBy_CMSTTC == 1)
	{
		warning_time = camera_share.ObsInormation.TTC;
		waring_time_type = TTC_WARNING_TYPE;
		BrakeBy_CMSTTC = g_AEB_CMS_outside_BrakeBy_CMSTTC;
	//	gcz_serial_v1_dma_printf(SERIAL_UART1,"[2].-%0.2f %d----------\r\n",warning_time,waring_time_type);
	}
	else if (g_AEB_CMS_outside_BrakeBy_AEBTTC == 1)
	{
		warning_time = camera_share.ObsInormation.TTC;
		waring_time_type = TTC_WARNING_TYPE;
		AEBTTC = g_AEB_CMS_outside_BrakeBy_AEBTTC;
	}
#define NO_BRAKE_STATE				0
#define	BRAKE_BEGIN_STATE			1
#define	BRAKE_FINISH_STATE			2
	static uint8_t brake_warning_ctrl_state = NO_BRAKE_STATE;
	if ((brake_warning_ctrl_state == NO_BRAKE_STATE) || (brake_warning_ctrl_state == BRAKE_FINISH_STATE))
	{
		if (Brake_state_share != 0)
		{
			brake_warning_ctrl_state = BRAKE_BEGIN_STATE;
	//		gcz_serial_v1_dma_printf(SERIAL_UART1,"++++++++++++++++++++++++++\r\n");
		}
		else
			brake_warning_ctrl_state = NO_BRAKE_STATE;
	}
	else if (brake_warning_ctrl_state == BRAKE_BEGIN_STATE)
	{
		if (Brake_state_share == 0)
		{
			brake_warning_ctrl_state = BRAKE_FINISH_STATE;
	//		gcz_serial_v1_dma_printf(SERIAL_UART1,"--------+++--------------------\r\n");
		}
	}
	if ((brake_warning_ctrl_state == BRAKE_BEGIN_STATE) || (brake_warning_ctrl_state == BRAKE_FINISH_STATE))
	{
		switch(Brake_state_share)
		{
		case 0:	// AEB 时间结束
			if(brake_state == 1) 		judgement_warning_event_for_insert(0, &aeb_CAM, AEB_EVENT,warning_time,waring_time_type,2);
			else if(brake_state == 2) 	judgement_warning_event_for_insert(0, &aeb_MMW, AEB_EVENT,warning_time,waring_time_type,2);
			else if(brake_state == 3) 	judgement_warning_event_for_insert(0, &aeb_ULT, AEB_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE,2);
			else if(brake_state == 4) 	judgement_warning_event_for_insert(0, &aeb_bsd, AEB_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE,m_srr_para.warningLevel);
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"3.-%0.2f %d----------\r\n",warning_time,waring_time_type);
			BrakeBy_CMSHMW = 0;
			BrakeBy_CMSTTC = 0;
			AEBTTC = 0;
			break;
		case 1:	// 双目 start
			brake_state = Brake_state_share;
			judgement_warning_event_for_insert(1, &aeb_CAM, AEB_EVENT,warning_time,waring_time_type,2);
			break;
		case 2:	// 毫米波 start
			brake_state = Brake_state_share;
			judgement_warning_event_for_insert(2, &aeb_MMW, AEB_EVENT,warning_time,waring_time_type,2);
			break;
		case 3:	// 超声波雷达 start
			brake_state = Brake_state_share;
			judgement_warning_event_for_insert(3, &aeb_ULT, AEB_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE,2);
			break;
		case 4:	// 角雷达 start
			brake_state = Brake_state_share;
			judgement_warning_event_for_insert(4, &aeb_bsd, AEB_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE,m_srr_para.warningLevel);
			break;
		}
	}

//	switch(Brake_state_share)
//	{
//	case 0:	// AEB 时间结束
//		if(brake_state == 1) 		judgement_warning_event_for_insert(0, &aeb_CAM, AEB_EVENT,warning_time,waring_time_type);
//		else if(brake_state == 2) 	judgement_warning_event_for_insert(0, &aeb_MMW, AEB_EVENT,warning_time,waring_time_type);
//		else if(brake_state == 3) 	judgement_warning_event_for_insert(0, &aeb_ULT, AEB_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE);
//		else if(brake_state == 4) 	judgement_warning_event_for_insert(0, &aeb_bsd, AEB_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE);
//		fprintf(USART1_STREAM,"3.-%0.2f %d----------\r\n",warning_time,waring_time_type);
//		BrakeBy_CMSHMW = 0;
//		BrakeBy_CMSTTC = 0;
//		AEBTTC = 0;
//		break;
//	case 1:	// 双目 start
//		brake_state = Brake_state_share;
//		judgement_warning_event_for_insert(1, &aeb_CAM, AEB_EVENT,warning_time,waring_time_type);
//		break;
//	case 2:	// 毫米波 start
//		brake_state = Brake_state_share;
//		judgement_warning_event_for_insert(2, &aeb_MMW, AEB_EVENT,warning_time,waring_time_type);
//		break;
//	case 3:	// 超声波雷达 start
//		brake_state = Brake_state_share;
//		judgement_warning_event_for_insert(3, &aeb_ULT, AEB_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE);
//		break;
//	case 4:	// 角雷达 start
//		brake_state = Brake_state_share;
//		judgement_warning_event_for_insert(4, &aeb_bsd, AEB_EVENT,camera_share.ObsInormation.HMW,HMW_WARNING_TYPE);
//		break;
//	}
}

//函数说明 : 设置AEB的开关源
//参数 : 源类型
//返回:	无
void set_AEB_key_source(unsigned char value)
{
	s_st_collection_status_cache.u8_AEB_key_source = value;
}
//函数说明 : 设置LDW的开关源
//参数 : 源类型
//返回:	无void set_LDW_key_source(unsigned char value)
void set_LDW_key_source(unsigned char value)
{
	s_st_collection_status_cache.u8_LDW_key_source = value;
}
//函数说明 : 设置比例阀错误码
//参数 : 比例阀错误码
//返回:	无
void set_proportional_valve(unsigned char value)
{
	s_st_collection_status_cache.u8_proportional_valve_fault_code = value;
}
//设置HMW报警等级
//参数：报警等级
void set_hmw_level(uint8_t hmw_level)
{
	s_st_collection_status_cache.u8_HMW_level 		= 	hmw_level;
}

//判断车身触发动作是否发生
bool judge_vehiclebody_trigger_action_does_it_happen()
{
	bool res = FALSE;
	static uint8_t brake_ctrl_state = ACTION_CLOSE;
	static uint8_t turn_left_ctrl_state = ACTION_CLOSE;
	static uint8_t turn_right_ctrl_state = ACTION_CLOSE;

	switch (brake_ctrl_state)
	{
	case ACTION_CLOSE:  				if (stVehicleParas.BrakeFlag != 0)
										{
											brake_ctrl_state = ACTION_OPEN;
											res = TRUE;
										}
										break;
	case ACTION_OPEN:					if (stVehicleParas.BrakeFlag != 0)
										{
											brake_ctrl_state = ACTION_OPEN_UNREMITTING;
										}
										else
											brake_ctrl_state = ACTION_CLOSE;
										break;
	case ACTION_OPEN_UNREMITTING:		if (stVehicleParas.BrakeFlag == 0)
										{
											brake_ctrl_state = ACTION_CLOSE;
											res = TRUE;		// add lmz 开始和结束都触发
										}
										break;
	default:	brake_ctrl_state = ACTION_CLOSE;break;
	}

	switch (turn_left_ctrl_state)
	{
	case ACTION_CLOSE:  				if (get_steer_sigl_status() == 1)
										{
											turn_left_ctrl_state = ACTION_OPEN;
											res = TRUE;
										}
										break;
	case ACTION_OPEN:					if (get_steer_sigl_status() == 1)
										{
											turn_left_ctrl_state = ACTION_OPEN_UNREMITTING;
										}
										else
											turn_left_ctrl_state = ACTION_CLOSE;
										break;
	case ACTION_OPEN_UNREMITTING:		if (get_steer_sigl_status() == 0)
										{
											turn_left_ctrl_state = ACTION_CLOSE;
											res = TRUE;		// add lmz 开始和结束都触发
										}
										break;
	default:	turn_left_ctrl_state = ACTION_CLOSE;break;
	}

	switch (turn_right_ctrl_state)
	{
	case ACTION_CLOSE:  				if (get_steer_sigl_status() == 2)
										{
											turn_right_ctrl_state = ACTION_OPEN;
											res = TRUE;
										}
										break;
	case ACTION_OPEN:					if (get_steer_sigl_status() == 2)
										{
											turn_right_ctrl_state = ACTION_OPEN_UNREMITTING;
										}
										else
											turn_right_ctrl_state = ACTION_CLOSE;
										break;
	case ACTION_OPEN_UNREMITTING:		if (get_steer_sigl_status() == 0)
										{
											turn_right_ctrl_state = ACTION_CLOSE;
											res = TRUE;		// add lmz 开始和结束都触发
										}
										break;
	default:	turn_right_ctrl_state = ACTION_CLOSE;break;
	}
	return res;
}
//判断LDW左线偏离预警动作
static bool judge_LDW_left_event_trigger_action()
{
	bool res = TRUE;
	static uint8_t m_left_ldw_state = ACTION_CLOSE;
	static int64_t cur_time_left = 0,cur_time_check_left = 0;
	switch (m_left_ldw_state)
	{
	case ACTION_CLOSE:  				if (camera_data.LeftLDW != 0)
										{
											if ((SystemtimeClock - cur_time_check_left) >= 100)
												m_left_ldw_state = ACTION_OPEN;
										//	fprintf(USART1_STREAM, "1.LEFT LDW OPEN\r\n");
										}
										else
										{
											cur_time_check_left = SystemtimeClock;
											res = FALSE;
										}
										break;
	case ACTION_OPEN:					if (camera_data.LeftLDW == 0)
										{
											m_left_ldw_state = ACTION_WAIT_CLOSE;
											cur_time_left = SystemtimeClock;
										//	fprintf(USART1_STREAM, "2.LEFT LDW ready to WAIT CLOSE   %ld\r\n",cur_time_left);
										}
										break;
	case ACTION_WAIT_CLOSE:				if ((SystemtimeClock - cur_time_left) < 1000)
										{
											if (camera_data.LeftLDW != 0)
											{
												cur_time_left = SystemtimeClock;
											}
										}
										else
										{
											res = FALSE;
											m_left_ldw_state = ACTION_CLOSE;
										//	fprintf(USART1_STREAM, "3.LEFT LDW CLOSE %ld %ld\r\n",cur_time_left,SystemtimeClock);
										}
										break;
	default:	res = FALSE;
				m_left_ldw_state = ACTION_CLOSE;break;
	}
	return res;
}
//判断LDW左线偏离预警动作
static bool judge_LDW_right_event_trigger_action()
{
	bool res = TRUE;
	static uint8_t m_right_ldw_state = ACTION_CLOSE;
	static int64_t cur_time_right = 0,cur_time_check_right = 0;;

	switch (m_right_ldw_state)
	{
	case ACTION_CLOSE:  				if (camera_data.RightLDW != 0)
										{
											if ((SystemtimeClock - cur_time_check_right) >= 100)
												m_right_ldw_state = ACTION_OPEN;
										//	fprintf(USART1_STREAM, "1.RIGHT LDW OPEN\r\n");
										}
										else
										{
											cur_time_check_right = SystemtimeClock;
											res = FALSE;
										}
										break;
	case ACTION_OPEN:					if (camera_data.RightLDW == 0)
										{
											m_right_ldw_state = ACTION_WAIT_CLOSE;
											cur_time_right = SystemtimeClock;
										}
										break;
	case ACTION_WAIT_CLOSE:				if ((SystemtimeClock - cur_time_right) < 1000)
										{
											if (camera_data.RightLDW != 0)
											{
												cur_time_right = SystemtimeClock;
											}
										}
										else
										{
											res = FALSE;
											m_right_ldw_state = ACTION_CLOSE;
										}
										break;
	default:	res = FALSE;
				m_right_ldw_state = ACTION_CLOSE;break;
	}
	return res;
}
//综合判断LDW偏离预警动作
bool judge_LDW_event_trigger_action()
{
	bool res = FALSE;
	bool left_ldw = false,right_ldw = false;
	static int64_t curtimer_start_left = 0,curtimer_start_right = 0,curtimer_left = 0,curtimer_right = 0;
	if(stVehicleParas.fVehicleSpeed > 0)
	{
		//有转向信号，不报LDW预警，与小屏幕机制一致
		if ((stVehicleParas.LeftFlag == 0) && (stVehicleParas.RightFlag == 0))
		{
		//	fprintf(USART1_STREAM, "***L:%d R:%d\r\n",stVehicleParas.LeftFlag,stVehicleParas.RightFlag);
			left_ldw = judge_LDW_left_event_trigger_action();
			right_ldw = judge_LDW_right_event_trigger_action();
			if ((left_ldw == TRUE) || (right_ldw == TRUE))
			{
				//同侧灯抑制同侧LDW
				if (((stVehicleParas.LeftFlag != 1) && (left_ldw == TRUE))
					|| ((stVehicleParas.RightFlag != 1) && (right_ldw == TRUE)))
				res = TRUE;
			}


			if ((left_ldw == 0) && (right_ldw != 0) && (stVehicleParas.RightFlag == 0))
			{
				curtimer_start_left = SystemtimeClock;
				curtimer_right = SystemtimeClock;
				if (((SystemtimeClock - curtimer_start_right) >= 100) || ((SystemtimeClock - curtimer_left) <= 5000))
				{
					s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_action = 0;
				//	fprintf(USART1_STREAM, "***right  0\r\n");
				}
				else
				{
					s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_action = 2;
				//	fprintf(USART1_STREAM, "***right  2\r\n");
				}
			}
			else if ((left_ldw != 0) && (right_ldw == 0) && (stVehicleParas.LeftFlag == 0))
			{
				curtimer_start_right = SystemtimeClock;
				curtimer_left = SystemtimeClock;
				if (((SystemtimeClock - curtimer_start_left) >= 100) ||((SystemtimeClock - curtimer_right) <= 5000))
				{
					s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_action = 0;
				//	fprintf(USART1_STREAM, "---Left  0\r\n");
				}
				else
				{
					s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_action = 1;
				//	fprintf(USART1_STREAM, "---Left  1\r\n");
				}
			}
			else
			{
				curtimer_left = -6000;
				curtimer_start_left = SystemtimeClock;
				curtimer_right = -6000;
				curtimer_start_right = SystemtimeClock;
				s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_action = 0;
			}
		}
		else
		{
			curtimer_left = -6000;
			curtimer_start_left = SystemtimeClock;
			curtimer_right = -6000;
			curtimer_start_right = SystemtimeClock;
			s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_action = 0;
		}
//调试自测代码
//		static bool e;
//		e = judge_LDW_left_event_trigger_action();
//		if (((e == TRUE) && (stVehicleParas.LeftFlag != 1)))
//			res = TRUE;
//
//		static bool t;
//		t = judge_LDW_right_event_trigger_action();
//		if (((t == TRUE) && (stVehicleParas.RightFlag != 1)))
//			res = TRUE;
	}
	else
	{
		curtimer_left = -6000;
		curtimer_start_left = SystemtimeClock;
		curtimer_right = -6000;
		curtimer_start_right = SystemtimeClock;
		s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_action = 0;
	}

//	fprintf(USART1_STREAM,"[%d] ",s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_action);
	return res;
}
//2022-08-11
bool judge_trigger_action(uint8_t cur_action,uint8_t orignl_action,int64_t waittime_ms,uint8_t *p_ctrl_stat,int64_t *p_cur_time)
{
	bool res = TRUE;
	switch (*p_ctrl_stat)
	{
	case ACTION_CLOSE:  				if (cur_action != orignl_action)
										{
										//	if ((SystemtimeClock - cur_time) >= 100)
											*p_ctrl_stat = ACTION_OPEN;
										}
										else
										{
											*p_cur_time = SystemtimeClock;
											res = FALSE;
										}
										break;
	case ACTION_OPEN:					if (cur_action == orignl_action)
										{
											*p_ctrl_stat = ACTION_WAIT_CLOSE;
											*p_cur_time = SystemtimeClock;
										}
										break;
	case ACTION_WAIT_CLOSE:				if ((SystemtimeClock - *p_cur_time) < waittime_ms)
										{
											if (cur_action != orignl_action)
											{
												*p_cur_time = SystemtimeClock;
											}
										}
										else
										{
											res = FALSE;
											*p_ctrl_stat = ACTION_CLOSE;
										}
										break;
	default:	res = FALSE;
				*p_ctrl_stat = ACTION_CLOSE;
				break;
	}
	return res;
}
//调用全部数据的结构的操作接口，刷新报警事件缓存数据到接口中
AEB_UPLOAD_INFO * other_user_get_upload_info()
{

	return get_warning_event_and_all_data();
//	return &s_st_aeb_upload_info;
}
//调用全部数据的结构的操作接口，不刷新报警事件的缓存信息
AEB_UPLOAD_INFO * other_user_get_upload_info_no_refresh_alarm_event()
{
	generate_upload_info();
	return &s_st_aeb_upload_info;
}
//实时刷新收集上报所需数据到指定数据结构
//生成上传信息的调用函数，该函数被调用时，生成所有全部数据及状态，放到主循环中实时生成也可以，现在用法是放在了other_user_get_upload_info和other_user_get_upload_info_no_refresh_alarm_event中，随需要时及时生成。
void generate_upload_info()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	check_ult_frame_status(1);//检测状态超时时间片，判定700和701帧的状态
	//（一）路面感知信息
	//（三）状态信息汇总
	//1.车身状态信息
	s_st_aeb_upload_info._ST_status._ST_vehicle_status._U8_speed_can_online_status 			= stCanCommSta.stSpeed.status;						//1.车速CAN在线状态
	s_st_aeb_upload_info._ST_status._ST_vehicle_status._U8_brake_light_status 				= stVehicleParas.BrakeFlag;							//(2).刹车打开
	s_st_aeb_upload_info._ST_status._ST_vehicle_status._U8_gear_status 						= get_gear_status();								//(3).档位状态 stVehicleParas.Car_Gear;
	s_st_aeb_upload_info._ST_status._ST_vehicle_status._U8_steering_light_status 			= get_steer_sigl_status();							//(4).转向灯状态
	s_st_aeb_upload_info._ST_status._ST_vehicle_status._U8_speed_valid_status 				= 1;												//(5)车速信息是否有效	申工和才酷的接口，暂时保留项，默认1.
	//2.AEB状态信息
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_AEB_on_off_status 					= g_AEB_CMS_Enable;									//1.AEB开关状态
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_on_off_status 					= g_LDW_Enable;										//2.LDW开关状态
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_AEB_switch_source 					= s_st_collection_status_cache.u8_AEB_key_source;	//3.AEB开关源
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_LDW_switch_source 					= s_st_collection_status_cache.u8_LDW_key_source;	//4.LDW开关源
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_AEB_brake_status 					= Brake_state_share;								//(5).AEB制动状态
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_event_status 						= s_st_collection_status_cache.u8_event_status;		//(6).事件的开始与结束
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_event_type 							= s_st_collection_status_cache.u8_event_type;		//(7).事件分类
//	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_FCW_level 							= get_fcw_warning_level();	//gcz 2022-07-07 明珠定制//camera_data.FCWLevel & 0x03;						//(8).FCW报警等级
//	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_HMW_level 							= get_hmw_warining_level(); //gcz 2022-07-07 明珠定制//s_st_collection_status_cache.u8_HMW_level;		//(9).HMW报警等级
	SRR_Obstacle_Paras m_srr_para = getSrrWarningAndZoneParas();
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_BSD_level 							= m_srr_para.warningLevel;
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_BSD_warning_direction 				= m_srr_para.warningZone;
	//3.外设状态信息
	if (m_sensor_mgt.m_dbl_cam.m_isOpen != FUNC_OPEN)
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_camre._U8_online_status 		= 0;
	else
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_camre._U8_online_status 		= (stCanCommSta.stCamera.status == OFFLINE) ? 1 : 2;//1.相机在线状态
	s_st_aeb_upload_info._ST_status._ST_perph_status._ST_camre._U8_ambientLuminance_status	= camera_data.AmbientLuminance;						//2.相机外部环境亮度
	s_st_aeb_upload_info._ST_status._ST_perph_status._ST_propovavle._U8_online_status		= (stCanCommSta.Proportional_valve.status == OFFLINE) ? 1 : 2;	//1.比例阀在线状态
//	if (m_sensor_mgt.m_ultra_radar.m_isOpen != FUNC_OPEN)
	// update lmz 20220818
	if((m_sensor_mgt.m_ultra_radar.m_front.m_main_sw != FUNC_OPEN)
		&& (m_sensor_mgt.m_ultra_radar.m_end.m_main_sw != FUNC_OPEN)
		&& (m_sensor_mgt.m_ultra_radar.m_left.m_main_sw != FUNC_OPEN)
		&& (m_sensor_mgt.m_ultra_radar.m_right.m_main_sw != FUNC_OPEN))
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_ult_radar._U8_online_status = 0;
	else
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_ult_radar._U8_online_status 		= (stCanCommSta.stHRadar.status == OFFLINE) ? 1 : 2;						//1.超声波 在线状态

//	if ((m_sensor_mgt.m_mmw_radar.m_isOpen != FUNC_OPEN)
//		|| ((m_sensor_mgt.m_mmw_radar.m_isOpen == FUNC_OPEN) && (m_sensor_mgt.m_mmw_radar.m_right.m_isOpen != SENSOR_OPEN)))
	if((m_sensor_mgt.m_mmw_radar.m_front.m_main_sw != FUNC_OPEN)
		&& (m_sensor_mgt.m_mmw_radar.m_right.m_main_sw != FUNC_OPEN))
	{
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_mmw_radar._U8_online_status = 0;
	}
	else
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_mmw_radar._U8_online_status 	= (stCanCommSta.stSrr.status == OFFLINE) ? 1 : 2;						//1.毫米波 在线状态
	s_st_aeb_upload_info._ST_status._ST_perph_status._ST_displayer._U8_online_status 		= (stCanCommSta.stDisplayer.status == OFFLINE) ? 1 : 2;					//1.小屏幕在线状态

	if (m_sensor_mgt.m_4g.m_isOpen != FUNC_OPEN)
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_4g._U8_online_status           = 0;
	else
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_4g._U8_online_status 			= (stCanCommSta.stWireless.status == OFFLINE) ? 1 : 2;					//1.4G 在线状态

	if (m_sensor_mgt.m_gps.m_isOpen != FUNC_OPEN)
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_gps._U8_online_status			= 0;
	else
		s_st_aeb_upload_info._ST_status._ST_perph_status._ST_gps._U8_online_status			= (gps_info.isConnect == false) ? 1 : 2;											//1.GPS 在线状态
	s_st_aeb_upload_info._ST_status._ST_perph_status._ST_bt._U8_online_status 				= 0;												//1.BT 在线状态		保留项   参数默认为：0

	//（四）故障信息
	//1.车身与控制器故障
	s_st_aeb_upload_info._ST_fault._ST_vehicle._U8_fault_vehicle   							= 0;												//1.车身故障			保留项   参数默认为：0
	s_st_aeb_upload_info._ST_fault._ST_vehicle._U8_fault_controller   						= 0;												//2.控制器故障		保留项   参数默认为：0
	//2.CAN通讯故障
	uint8_t i1;
	for (i1 = 0;i1 < CAN_USED_NBR;i1++)
		insert_can_comm_status_info(&s_st_aeb_upload_info._ST_fault._ST_can[i1],&s_st_collection_status_cache.st_can_status_detection_cache[i1]);
	//3.外设故障
	s_st_aeb_upload_info._ST_fault._ST_periph._U8_periphfault_camera 						= (camera_data.AmbientLuminance == 4) ? 91 : camera_data.ErrorCode;							//1.相机故障
	_STRUCT_VALVE_PARA *p_st_valve = get_valveparas();
	s_st_aeb_upload_info._ST_fault._ST_periph._U8_periphfault_proporvalve 					= p_st_valve->Fault_Code;							//2.比例阀故障
	get_ult_radar_periph_code(s_st_aeb_upload_info._ST_fault._ST_periph._U8_periphfault_ult_radar,MAX_ULT_RADAR_NBR);							//(3).超声波雷达故障
	s_st_aeb_upload_info._ST_fault._ST_periph._U8_periphfault_MMW_radar						= getAngleRadarMalfunction();												//4.毫米波雷达故障		保留项，默认固定参数：0
	s_st_aeb_upload_info._ST_fault._ST_periph._U8_periphfault_displayer						= 0;												//5.小屏幕故障			保留项，默认固定参数：0
	s_st_aeb_upload_info._ST_fault._ST_periph._U8_periphfault_4G_COMM						= 0;												//6.4G通讯故障			保留项，默认固定参数：0
	s_st_aeb_upload_info._ST_fault._ST_periph._U8_periphfault_GPS_aerial					= 0;												//7.GPS通讯故障			保留项，默认固定参数：0
	s_st_aeb_upload_info._ST_fault._ST_periph._U8_periphfault_BT							= 0;												//8.蓝牙故障				保留项，默认固定参数：0
	s_st_aeb_upload_info._ST_fault._ST_periph._U8_periphfault_IMU							= g_imu_detection_result;													//9.IMU故障
	//（五）AEB制动信息
//	for (i1 = 0;i1 < ULTR_WAVE_RADAR_NBR;i1++)
//		s_st_aeb_upload_info._ST_brake._ST_ult_radar_brake_info._ARRY_ult_brake[i1]			= 0;												//超声波雷达制动  默认参数固定为0
	s_st_aeb_upload_info._ST_brake._ST_ult_radar_brake_info._U8_ult_id					= stVehicleParas.ult_warning_id;						//超声波雷达制动
}
//测试函数
void test_func()
{
	AEB_UPLOAD_INFO *p = other_user_get_upload_info_no_refresh_alarm_event();
	fprintf(USART1_STREAM,"***********************information*****************************************\r\n");
#if 1
	fprintf(USART1_STREAM,"vehicle_speed_can_online_status:                   %d\r\n",		p->_ST_status._ST_vehicle_status._U8_speed_can_online_status);
	fprintf(USART1_STREAM,"vehicle_brake_light_status:                        %d\r\n",		p->_ST_status._ST_vehicle_status._U8_brake_light_status);
	fprintf(USART1_STREAM,"vehicle_gear_status:                               %d\r\n",		p->_ST_status._ST_vehicle_status._U8_gear_status);
	fprintf(USART1_STREAM,"vehicle_steering_light_status:                     %d\r\n",		p->_ST_status._ST_vehicle_status._U8_steering_light_status);
	fprintf(USART1_STREAM,"vehicle_speed_valid_status:                        %d\r\n",		p->_ST_status._ST_vehicle_status._U8_speed_valid_status);


	fprintf(USART1_STREAM,"controller_AEB_on_off_status:                      %d\r\n",		p->_ST_status._ST_AEB_status._U8_AEB_on_off_status);
	fprintf(USART1_STREAM,"controller_LDW_on_off_status:                      %d\r\n",		p->_ST_status._ST_AEB_status._U8_LDW_on_off_status);
	fprintf(USART1_STREAM,"controller_AEB_switch_source:                      %d\r\n",		p->_ST_status._ST_AEB_status._U8_AEB_switch_source);
	fprintf(USART1_STREAM,"controller_LDW_switch_source:                      %d\r\n",		p->_ST_status._ST_AEB_status._U8_LDW_switch_source);
#endif
	fprintf(USART1_STREAM,"controller_AEB_brake_status:                       %d\r\n",		p->_ST_status._ST_AEB_status._U8_AEB_brake_status);
	fprintf(USART1_STREAM,"controller_event_status:                           %d\r\n",		p->_ST_status._ST_AEB_status._U8_event_status);
	fprintf(USART1_STREAM,"controller_event_type:                             %d\r\n",		p->_ST_status._ST_AEB_status._U8_event_type);
	fprintf(USART1_STREAM,"controller_FCW_level:                              %d\r\n",		p->_ST_status._ST_AEB_status._U8_FCW_level);
	fprintf(USART1_STREAM,"controller_HMW_level:                              %d\r\n",		p->_ST_status._ST_AEB_status._U8_HMW_level);
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_event_status 						= s_st_collection_status_cache.u8_event_status;		//(6).事件的开始与结束
	s_st_aeb_upload_info._ST_status._ST_AEB_status._U8_event_type 							= s_st_collection_status_cache.u8_event_type;		//(7).事件分类

#if 1
	fprintf(USART1_STREAM,"peripheral_camera_online_status:                   %d\r\n",		p->_ST_status._ST_perph_status._ST_camre._U8_online_status);
	fprintf(USART1_STREAM,"peripheral_camera_ambientLuminance_status:         %d\r\n",		p->_ST_status._ST_perph_status._ST_camre._U8_ambientLuminance_status);
	fprintf(USART1_STREAM,"peripheral_proportional_valve_online_status:       %d\r\n",		p->_ST_status._ST_perph_status._ST_propovavle._U8_online_status);
	fprintf(USART1_STREAM,"peripheral_ultrasonic_online_status:               %d\r\n",		p->_ST_status._ST_perph_status._ST_ult_radar._U8_online_status);
	fprintf(USART1_STREAM,"peripheral_MMW_online_status:                      %d\r\n",		p->_ST_status._ST_perph_status._ST_mmw_radar._U8_online_status);
	fprintf(USART1_STREAM,"peripheral_displayer_online_status:                %d\r\n",		p->_ST_status._ST_perph_status._ST_displayer._U8_online_status);
	fprintf(USART1_STREAM,"peripheral_4G_online_status:                       %d\r\n",		p->_ST_status._ST_perph_status._ST_4g._U8_online_status);
	fprintf(USART1_STREAM,"peripheral_GPS_online_status:                      %d\r\n",		p->_ST_status._ST_perph_status._ST_gps._U8_online_status);
	fprintf(USART1_STREAM,"peripheral_BT_online_status:                       %d\r\n",		p->_ST_status._ST_perph_status._ST_bt._U8_online_status);

	fprintf(USART1_STREAM,"fault_vehicle:                                     %d\r\n",		p->_ST_fault._ST_vehicle._U8_fault_vehicle);
	fprintf(USART1_STREAM,"fault_controller:                                  %d\r\n",		p->_ST_fault._ST_vehicle._U8_fault_controller);
	uint8_t i1;
	for (i1 = 0;i1 < CAN_USED_NBR;i1++)
	fprintf(USART1_STREAM,"[%d]fault_can_bus_status:                           %d\r\n   fault_can_err_status:                           %d\r\n   fault_can_err_code:                             %d\r\n   fault_can_err_direction:                        %d\r\n",
																							i1,
																							p->_ST_fault._ST_can[i1]._U8_fault_can_bus_status,
																							p->_ST_fault._ST_can[i1]._U8_fault_can_err_status,
																							p->_ST_fault._ST_can[i1]._U8_fault_can_err_code,
																							p->_ST_fault._ST_can[i1]._U8_fault_can_err_direction);
	fprintf(USART1_STREAM,"periphfault_camera:                                %d\r\n",		p->_ST_fault._ST_periph._U8_periphfault_camera);
	fprintf(USART1_STREAM,"periphfault_proportional_valve:                    %d\r\n",		p->_ST_fault._ST_periph._U8_periphfault_proporvalve);
	for (i1 = 0;i1 < MAX_ULT_RADAR_NBR;i1++)
		fprintf(USART1_STREAM,"[%d]periphfault_ultrasonic_radar:                   %d\r\n",		i1,p->_ST_fault._ST_periph._U8_periphfault_ult_radar[i1]);
	fprintf(USART1_STREAM,"periphfault_MMW_radar:                             %d\r\n",		p->_ST_fault._ST_periph._U8_periphfault_MMW_radar);
	fprintf(USART1_STREAM,"periphfault_displayer:                             %d\r\n",		p->_ST_fault._ST_periph._U8_periphfault_displayer);
	fprintf(USART1_STREAM,"periphfault_4G_COMM:                               %d\r\n",		p->_ST_fault._ST_periph._U8_periphfault_4G_COMM);
	fprintf(USART1_STREAM,"periphfault_GPS_aerial:                            %d\r\n",		p->_ST_fault._ST_periph._U8_periphfault_GPS_aerial);
//	fprintf(USART1_STREAM,"periphfault_BT:                                    %d\r\n",		p->_ST_fault._ST_periph._U8_periphfault_BT);
	fprintf(USART1_STREAM,"periphfault_IMU:                                   %d\r\n",		p->_ST_fault._ST_periph._U8_periphfault_IMU);
//	for (i1 = 0;i1 < ULTR_WAVE_RADAR_NBR;i1++)
//		fprintf(USART1_STREAM,"[%d]braking_ultrasonic:                             %d\r\n",		i1,p->_ST_brake._ST_ult_radar_brake_info._ARRY_ult_brake[i1]);
	fprintf(USART1_STREAM,"braking_ultrasonic:                                %d\r\n",		i1,p->_ST_brake._ST_ult_radar_brake_info._U8_ult_id);
#endif
	fprintf(USART1_STREAM,"------------------------------------------------------------------\r\n\r\n");
}
SW_HW_Version_Info sw_hw_version;
Comm_Protocol_Version_Info comm_protocol_version;
//根据实时状态获取收集系统内个子模块版本信息
static void get_all_sys_version_info(Device_Version_Struct *p_st_ver)
{
	strcpy(p_st_ver->AEBS_SW_Version, sw_hw_version.AEBS_SW_Version);
	strcpy(p_st_ver->AEBS_SN, sw_hw_version.AEBS_SN);
	strcpy(p_st_ver->AEBS_PN, sw_hw_version.AEBS_PN);

	uint8_t cMon = 0;
	uint8_t buffer[15];
	memset(buffer,0,sizeof(buffer));
	if (stCanCommSta.stCamera.status == ONLINE)
	{
		sprintf(p_st_ver->Camera_SW_Version,"A%d.%d.%d.%d",sw_hw_version.Camera_SW_Version.v0,sw_hw_version.Camera_SW_Version.v1,sw_hw_version.Camera_SW_Version.v2,sw_hw_version.Camera_SW_Version.v3);
//		static uint32_t a = 0;
//		if (SystemtimeClock - a >= 100000)
//		{
//			a = SystemtimeClock;
//			fprintf(USART1_STREAM, "aaaaaaaaaaaaaaaaa");
//		}
		sprintf(p_st_ver->Camera_HW_Version,"V%d.%d.%d",sw_hw_version.Camera_HW_Version.v0,sw_hw_version.Camera_HW_Version.v1,sw_hw_version.Camera_HW_Version.v2);

		if (camera_sn_info.mon > 9)
		{
			cMon = 'C' - (12 - camera_sn_info.mon);
		}
		else
		{
			cMon = camera_sn_info.mon + '0';
		}
		sprintf(buffer,"%d%02d%c%02d%04d",camera_sn_info.factory_code,camera_sn_info.year,cMon,camera_sn_info.day,camera_sn_info.stream_code);
	}
	else
	{
		strcpy(p_st_ver->Camera_SW_Version,"A-.-.-.-");
		strcpy(p_st_ver->Camera_HW_Version,"V-.-.-");

	}

	strcpy(p_st_ver->Camera_SN,buffer);
	strcpy(p_st_ver->Camera_PN, "");
	if (stCanCommSta.stDisplayer.status == ONLINE)
	{
		sprintf(p_st_ver->Displayer_SW_Version,"V%d.%d.%d",sw_hw_version.Displayer_SW_Version.v0,sw_hw_version.Displayer_SW_Version.v1,sw_hw_version.Displayer_SW_Version.v2);
	}
	else
	{
		strcpy(p_st_ver->Displayer_SW_Version,"V-.-.-");
	}
	strcpy(p_st_ver->Displayer_SN, sw_hw_version.Displayer_SN);
	strcpy(p_st_ver->Displayer_PN, sw_hw_version.Displayer_PN);


	if (stCanCommSta.stHRadar.status == ONLINE)
		sprintf(p_st_ver->URadar_Version,"V%d.%d",sw_hw_version.URadar_Version.v0,sw_hw_version.URadar_Version.v1);
	else
		sprintf(p_st_ver->URadar_Version,"V-.-");


	if (stCanCommSta.stRadar.status == ONLINE)
		sprintf(p_st_ver->MRadar_Version,"V%d.%d.%d",sw_hw_version.MRadar_Version.v0,sw_hw_version.MRadar_Version.v1,sw_hw_version.MRadar_Version.v2);
	else
		sprintf(p_st_ver->MRadar_Version,"V-.-.-");

	if (stCanCommSta.stCamera.status == ONLINE)
		sprintf(p_st_ver->Camera_CAN_ProtocoI_Version,"V%d.%d.%d",comm_protocol_version.Camera_CAN_ProtocoI_Version.v0,comm_protocol_version.Camera_CAN_ProtocoI_Version.v1,comm_protocol_version.Camera_CAN_ProtocoI_Version.v2);
	else
		sprintf(p_st_ver->Camera_CAN_ProtocoI_Version,"V-.-.-");

	sprintf(p_st_ver->AEBS_AT_PC_ProtocoI_Version,"V%d.%d.%d",comm_protocol_version.AEBS_AT_PC_ProtocoI_Version.v0,comm_protocol_version.AEBS_AT_PC_ProtocoI_Version.v1,comm_protocol_version.AEBS_AT_PC_ProtocoI_Version.v2);
	sprintf(p_st_ver->AEBS_AT_BT_ProtocoI_Version,"V%d.%d.%d",comm_protocol_version.AEBS_AT_BT_ProtocoI_Version.v0,comm_protocol_version.AEBS_AT_BT_ProtocoI_Version.v1,comm_protocol_version.AEBS_AT_BT_ProtocoI_Version.v2);
	sprintf(p_st_ver->AEBS_CAN_ProtocoI_Version,"V%d.%d",comm_protocol_version.AEBS_CAN_ProtocoI_Version.v0,comm_protocol_version.AEBS_CAN_ProtocoI_Version.v1);
	sprintf(p_st_ver->Displayer_CAN_ProtocoI_Version,"V%d.%d",comm_protocol_version.Displayer_CAN_ProtocoI_Version.v0,comm_protocol_version.Displayer_CAN_ProtocoI_Version.v1);
	sprintf(p_st_ver->Cloud_Platform_ProtocoI_Version,"V%d.%d",comm_protocol_version.Cloud_Platform_ProtocoI_Version.v0,comm_protocol_version.Cloud_Platform_ProtocoI_Version.v1);
}
//版本信息刷新测试函数
void refresh_test(STREAM* USART_Print,Device_Version_Struct *p_st_ver)
{

	fprintf(USART_Print, "-----------------refresh test--------------------\r\n");

	fprintf(USART_Print,"AEBS_SW_Version:                   %s\r\n",p_st_ver->AEBS_SW_Version);
	fprintf(USART_Print,"AEBS_SN:                           %s\r\n",p_st_ver->AEBS_SN);
	fprintf(USART_Print,"AEBS_PN:                           %s\r\n",p_st_ver->AEBS_PN);

	uint8_t buffer[15];
	memset(buffer,0,sizeof(buffer));

	fprintf(USART_Print,"Camera_SW_Version:                 %s\r\n",p_st_ver->Camera_SW_Version);
	fprintf(USART_Print,"Camera_HW_Version:                 %s\r\n",p_st_ver->Camera_HW_Version);
	fprintf(USART_Print,"Camera_SN:                         %s\r\n",p_st_ver->Camera_SN);
	fprintf(USART_Print,"Displayer_SW_Version:              %s\r\n",p_st_ver->Displayer_SW_Version);
	fprintf(USART_Print,"Displayer_SN:                      %s\r\n",p_st_ver->Displayer_SN);
	fprintf(USART_Print,"Displayer_PN:                      %s\r\n",p_st_ver->Displayer_PN);
	fprintf(USART_Print,"URadar_Version:                    %s\r\n",p_st_ver->URadar_Version);
	fprintf(USART_Print,"MRadar_Version:                    %s\r\n",p_st_ver->MRadar_Version);
	fprintf(USART_Print,"Camera_CAN_ProtocoI_Version:       %s\r\n",p_st_ver->Camera_CAN_ProtocoI_Version);
	fprintf(USART_Print,"AEBS_AT_PC_ProtocoI_Version:       %s\r\n",p_st_ver->AEBS_AT_PC_ProtocoI_Version);
	fprintf(USART_Print,"AEBS_AT_BT_ProtocoI_Version:       %s\r\n",p_st_ver->AEBS_AT_BT_ProtocoI_Version);
	fprintf(USART_Print,"AEBS_CAN_ProtocoI_Version:         %s\r\n",p_st_ver->Displayer_CAN_ProtocoI_Version);
	fprintf(USART_Print,"Displayer_CAN_ProtocoI_Version:    %s\r\n",p_st_ver->Displayer_CAN_ProtocoI_Version);
	fprintf(USART_Print,"Cloud_Platform_ProtocoI_Version:   %s\r\n",p_st_ver->Cloud_Platform_ProtocoI_Version);

	fprintf(USART_Print, "-------------------------------------\r\n");
}
typedef struct __ctrl_state
{
	uint8_t state;
	uint32_t timer;
}CTRL_STATE;
enum
{
	STATE_VERSION_INIT = 0,
	STATE_VERSION_REFRESH_JUDGE,
	STATE_VERSION_GET_WAIT,
};
static CTRL_STATE s_st_version_refresh_ctrl_state;
bool judge_can_device_status_ischanged(uint8_t *camera_status,uint8_t *displayer_status,uint8_t *hradar_status,uint8_t *radar_status)
{
	uint8_t cnt = 0;
	if (stCanCommSta.stCamera.status != *camera_status)
	{
		*camera_status = stCanCommSta.stCamera.status;
		cnt++;
	}
	if (stCanCommSta.stDisplayer.status != *displayer_status)
	{
		*displayer_status = stCanCommSta.stDisplayer.status;
		cnt++;
	}
	if (stCanCommSta.stHRadar.status != *hradar_status)
	{
		*hradar_status = stCanCommSta.stHRadar.status;
		cnt++;
	}
	if (stCanCommSta.stRadar.status != *radar_status)
	{
		*radar_status = stCanCommSta.stRadar.status;
		cnt++;
	}
	return ((cnt == 0) ? FALSE : TRUE);
}
static bool st_refres_flag = FALSE;
//设置刷新版本状态标志位
void set_refresh_flag(bool val)
{
	st_refres_flag = val;
}
//获取刷新版本状态标志位
bool get_refresh_flag()
{
	return st_refres_flag;
}
//刷新系统版本信息
void refresh_sys_version_info(Device_Version_Struct *p_st_ver)
{
	static uint8_t Camera_status = OFFLINE,Displayer_status = OFFLINE,HRadar_status = OFFLINE,Radar_status = OFFLINE;
	bool res = FALSE;
	static uint32_t timer = 0;
	switch(s_st_version_refresh_ctrl_state.state)
	{
	case STATE_VERSION_INIT:			s_st_version_refresh_ctrl_state.state = STATE_VERSION_GET_WAIT;
										timer = SystemtimeClock;
										break;
	case STATE_VERSION_REFRESH_JUDGE:	if (judge_can_device_status_ischanged(&Camera_status,&Displayer_status,&HRadar_status,&Radar_status) == TRUE)
										{
											s_st_version_refresh_ctrl_state.state = STATE_VERSION_GET_WAIT;
											timer = SystemtimeClock;
										}
										break;
	case STATE_VERSION_GET_WAIT:		if ((SystemtimeClock - timer) >= 800)
										{
											s_st_version_refresh_ctrl_state.state = STATE_VERSION_REFRESH_JUDGE;
											timer = SystemtimeClock;
											get_all_sys_version_info(p_st_ver);
											res = TRUE;
											set_refresh_flag(res);
										}
										break;

	default: 	s_st_version_refresh_ctrl_state.state = STATE_VERSION_INIT;
				s_st_version_refresh_ctrl_state.timer = 0;
				set_refresh_flag(res);
				break;
	}
}
