#include "app_program.h"
#include "serial_app.h"
#include "soft_timer_app.h"
#include "imu_app.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

//所有模块应用程序的初始化
void whole_module_app_init()
{
	imu_app_init();//IMU应用初始化，由于前期设计原理，这里仅做IMU功能是否正常检测的应用功能
}
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

//串口通讯任务
void comm_task()
{
	process_serial_rx_sub_task();
	process_serial_tx_sub_task();
}
//定时器任务任务
void timer_task()
{
	process_soft_timer_sub_task();
}
//主任务，所有子任务均在此进行填写
void main_task()
{
	comm_task();
	timer_task();
}
