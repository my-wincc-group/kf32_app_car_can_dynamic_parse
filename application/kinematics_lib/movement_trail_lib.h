#ifndef __MOVEMENT_TRAIL_H
#define __MOVEMENT_TRAIL_H
#include <stdbool.h>
#include <stdint.h>
/*
 * 作者：郭超卓
 * 日期：2022年9月16日
 * */
#define PI	3.1415926

typedef struct __prediction_postion
{
	float x;
	float y;
}PREDICT_POS;
typedef enum move_direction
{
	HOMODROMOUS = 0, //纵向同向
	ANTIDROMIC,		 //纵向逆向
	MOTIONLESS		 //纵向静止
}MOVE_DIRECT;

typedef enum circular_area
{
	INTERION = 0, 	 //内部
	EXTERNAL,		 //外部
}CIRCL_AREA;
//参数：目标时间    x当前位置	y当前位置	△Vx	   △Vy   自车速度km/h 	预测坐标的数组
void linear_predict_future_postion(float t,float x_pos,float y_pos,float delta_v_x,float delta_v_y,float v_ego_km_pre_h,PREDICT_POS *p);
//判断目标与本车的运动方向
//参数：自车车速		相对车速      单位：m/s
MOVE_DIRECT check_movement_direction(float v_ego,float delat_v_vertical);
//根据目标XY距离判断目标在圆的哪个区域
//参数：自车车速    自车转弯角速度	目标横向距离		目标纵向距离
//返回：在圆形轨迹圆内部还是圆外部
CIRCL_AREA judgement_circular_type(float v_ego,float deg_sp,float horizontal_distance,float vertical_distance,float *p_f_cal_x_dis);
#endif
