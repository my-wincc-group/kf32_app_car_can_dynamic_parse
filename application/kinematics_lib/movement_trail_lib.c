#include "movement_trail_lib.h"
#include "math.h"
/*
 * 作者：郭超卓
 * 日期：2022年9月16日
 * */


//线性预测未来位置
//参数：目标时间    x当前位置	y当前位置	△Vx	   △Vy   自车速度km/h 	预测坐标的数组
void linear_predict_future_postion(float t,float x_pos,float y_pos,float delta_v_x,float delta_v_y,float v_ego_km_pre_h,PREDICT_POS *p)
{
	float v_ego_m_pre_s,v_target_x,v_target_xy;
	float s_target_xy;
	float theta;
	float dx,dy,dx_ego;
//	float x_future_pos;
//	float y_future_pos;
	//1.计算自车车速m/s
	v_ego_m_pre_s = v_ego_km_pre_h/3.6;
	//2.计算目标车辆X轴的绝对车速
	v_target_x = v_ego_m_pre_s + delta_v_x;
	//3.计算XY合成矢量方向车速
	v_target_xy = sqrt(pow(v_target_x,2) + pow(delta_v_y,2));
	//4.计算t秒后，VXY速度下，XY矢量方向走的距离
	s_target_xy = v_target_xy * t;
	if (delta_v_y != 0.0)
	{
		//5.计算方向角θ——弧度
		theta = atan(v_target_x/delta_v_y);
		//6.计算目标车辆x方向的运动距离
		dx = sin(theta) * s_target_xy;
		//7.计算目标车辆y方向的运动距离
		dy = cos(theta) * s_target_xy;
	}
	else
	{
		//6.
		dx = s_target_xy;
		//7.
		dy = 0;
	}
	//8.计算自车车辆x方向的运动距离
	dx_ego = v_ego_m_pre_s * t;
	//9.计算预测点
	p->x = x_pos - dx_ego + dx;
	p->y = y_pos - dy;
}

//判断目标与本车的运动方向
//参数：自车车速		纵向相对速度   	横向相对速度	   单位：m/s
MOVE_DIRECT check_movement_direction(float v_ego,float delat_v_vertical)
{
	uint8_t target_movement_type = 0;
	//自车车速与X相对车速几乎一致，说明适横穿过来的物体
	if ((fabs(v_ego - fabs(delat_v_vertical))) <= 0.5)
	{
		target_movement_type = MOTIONLESS;
	}
	else
	{
		//同向而行，自车追目标时，X相对车速小于自车车速
		if (fabs(delat_v_vertical) < v_ego)
		{
			target_movement_type = HOMODROMOUS;
		}
		//对向而行，目标朝自车行驶来时，X相对车速大于自车车速
		else if (fabs(delat_v_vertical) > v_ego)
		{
			target_movement_type = ANTIDROMIC;
		}
	}
	return target_movement_type;
}
//在圆形轨迹上通过横向距离Y计算X位置
//参数：自车车速    自车转弯角速度	目标横向距离
//返回：处于圆形轨迹上的纵向坐标vertical_distance
float calc_x_postion_in_circular_trace(float v_ego,float deg_sp,float horizontal_distance)
{
	float v_ego_mps = v_ego / 3.6;
	float deg_sp_radian = fabs(deg_sp) / 180 * PI;
	float radius = v_ego_mps / deg_sp_radian;
	float vertical_distance = sqrt(-fabs(horizontal_distance)+2*radius*fabs(horizontal_distance));
	return vertical_distance;
}
//根据目标XY距离判断目标在圆的哪个区域
//参数：自车车速    自车转弯角速度	目标横向距离		目标纵向距离		获取到的计算出来的圆上的x距离值
//返回：在圆形轨迹圆内部还是圆外部
CIRCL_AREA judgement_circular_type(float v_ego,float deg_sp,float horizontal_distance,float vertical_distance,float *p_f_cal_x_dis)
{
	CIRCL_AREA res;
	if (deg_sp == 0.0)
		res = EXTERNAL;
	else
	{
		*p_f_cal_x_dis = calc_x_postion_in_circular_trace(v_ego,deg_sp,horizontal_distance);
		if (vertical_distance < *p_f_cal_x_dis)
		{
			res = INTERION;
		}
		else
		{
			res = EXTERNAL;
		}
	}
	return res;
}
