#include "serial_v1.h"
#include "serial_app.h"
#include "wt_jy901_imu.h"
#include "tool.h"
#include "alg_lib.h"
#include "soft_timer.h"
#include "stdio.h"
#include "common.h"
#include "imu_app.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

typedef struct __serial_data
{
	uint8_t *arry_buffer;
	uint32_t u32_length;
}SERIAL_STREAM;
#ifdef USE_SERIAL_RX_UART0
SERIAL_STREAM g_st_serial_uart0_read;
uint8_t g_arry_uart0_read_buffer[SERIAL_0_RCV_LENGTH];
#endif
#ifdef USE_SERIAL_RX_UART1
SERIAL_STREAM g_st_serial_uart1_read;
uint8_t g_arry_uart1_read_buffer[SERIAL_1_RCV_LENGTH];
#endif
#ifdef USE_SERIAL_RX_UART2
SERIAL_STREAM g_st_serial_uart2_read;
uint8_t g_arry_uart2_read_buffer[SERIAL_2_RCV_LENGTH];
#endif
#ifdef USE_SERIAL_RX_UART3
SERIAL_STREAM g_st_serial_uart3_read;
uint8_t g_arry_uart3_read_buffer[SERIAL_3_RCV_LENGTH];
#endif
#ifdef USE_SERIAL_RX_UART4
SERIAL_STREAM g_st_serial_uart4_read;
uint8_t g_arry_uart4_read_buffer[SERIAL_4_RCV_LENGTH];
#endif
#ifdef USE_SERIAL_RX_UART5
SERIAL_STREAM g_st_serial_uart5_read;
uint8_t g_arry_uart5_read_buffer[SERIAL_5_RCV_LENGTH];
#endif
#ifdef USE_SERIAL_RX_UART6
SERIAL_STREAM g_st_serial_uart6_read;
uint8_t g_arry_uart6_read_buffer[SERIAL_6_RCV_LENGTH];
#endif
#ifdef USE_SERIAL_RX_UART7
SERIAL_STREAM g_st_serial_uart7_read;
uint8_t g_arry_uart7_read_buffer[SERIAL_7_RCV_LENGTH];
#endif

#ifdef USE_SERIAL_TX_UART0
SERIAL_STREAM g_st_serial_uart0_write;
uint8_t g_arry_uart0_write_buffer[SERIAL_0_SND_LENGTH];
#endif
#ifdef USE_SERIAL_TX_UART1
SERIAL_STREAM g_st_serial_uart1_write;
uint8_t g_arry_uart1_write_buffer[SERIAL_1_SND_LENGTH];
#endif
#ifdef USE_SERIAL_TX_UART2
SERIAL_STREAM g_st_serial_uart2_write;
uint8_t g_arry_uart2_write_buffer[SERIAL_2_SND_LENGTH];
#endif
#ifdef USE_SERIAL_TX_UART3
SERIAL_STREAM g_st_serial_uart3_write;
uint8_t g_arry_uart3_write_buffer[SERIAL_3_SND_LENGTH];
#endif
#ifdef USE_SERIAL_TX_UART4
SERIAL_STREAM g_st_serial_uart4_write;
uint8_t g_arry_uart4_write_buffer[SERIAL_4_SND_LENGTH];
#endif
#ifdef USE_SERIAL_TX_UART5
SERIAL_STREAM g_st_serial_uart5_write;
uint8_t g_arry_uart5_write_buffer[SERIAL_5_SND_LENGTH];
#endif
#ifdef USE_SERIAL_TX_UART6
SERIAL_STREAM g_st_serial_uart6_write;
uint8_t g_arry_uart6_write_buffer[SERIAL_6_SND_LENGTH];
#endif
#ifdef USE_SERIAL_TX_UART7
SERIAL_STREAM g_st_serial_uart7_write;
uint8_t g_arry_uart7_write_buffer[SERIAL_7_SND_LENGTH];
#endif

bool g_b_imu_show = false;
float g_f_radio;

//处理串口接收任务
//说明：主循环调用
void process_serial_rx_sub_task()
{
#ifdef USE_SERIAL_RX_UART0
	g_st_serial_uart0_read.arry_buffer = g_arry_uart0_read_buffer;
	if (g_st_serial_opt.serial_rx_read_from_cahce(SERIAL_UART0,g_st_serial_uart0_read.arry_buffer,&g_st_serial_uart0_read.u32_length))
	{
		;
	}
#endif
#ifdef USE_SERIAL_RX_UART1
	g_st_serial_uart1_read.arry_buffer = g_arry_uart1_read_buffer;
	if (g_st_serial_opt.serial_rx_read_from_cahce(SERIAL_UART1,g_st_serial_uart1_read.arry_buffer,&g_st_serial_uart1_read.u32_length))
	{
		;
	}
#endif
#ifdef USE_SERIAL_RX_UART2
#endif
#ifdef USE_SERIAL_RX_UART3
#endif
#ifdef USE_SERIAL_RX_UART4
	IMU_FILTER_DATA *p = get_imu_original_data();
	g_st_serial_uart4_read.arry_buffer = g_arry_uart4_read_buffer;
	if (g_st_serial_opt.serial_rx_read_from_cahce(SERIAL_UART4,g_st_serial_uart4_read.arry_buffer,&g_st_serial_uart4_read.u32_length))
	{
		uint8_t res = wt_jy901_analysis(g_st_serial_uart4_read.arry_buffer,g_st_serial_uart4_read.u32_length);
		if (res == FRAME_ACC)
		{
			//获取原始数据，对ACC进行一阶卡尔曼滤波
			g_st_imu_klm_filter_data.acc[X_P] = klm_imu_filter(res,X_P,g_st_klm_acc,p->acc[X_P]);
			set_imu_comm_flag(true);		//2022-09-14 IMU通讯是否正常检测
			float v_m = stVehicleParas.fVehicleSpeed/3.6;
			float deg_sp = p->angular_v[Z_P] / 180 * 3.1415926;
			g_f_radio = v_m / deg_sp;
			if (g_b_imu_show)
			{
				gcz_serial_v1_dma_printf(SERIAL_UART1,"[WT]SYSCLK:%d ",SystemtimeClock);
				gcz_serial_v1_dma_printf(SERIAL_UART1,"[ACC-KLM]X:%0.8f ",g_st_imu_klm_filter_data.acc[X_P]);
				gcz_serial_v1_dma_printf(SERIAL_UART1,"[ACC]X:%0.8f ",p->acc[X_P]);
				gcz_serial_v1_dma_printf(SERIAL_UART1,"[ACC]Y:%0.8f ",p->acc[Y_P]);

				gcz_serial_v1_dma_printf(SERIAL_UART1,"[DEG_SP]Z:%0.8f ",p->angular_v[Z_P]);

				gcz_serial_v1_dma_printf(SERIAL_UART1,"[ANG]Z:%0.8f ",p->angle[Z_P]);

				gcz_serial_v1_dma_printf(SERIAL_UART1,"[V_CAR]V:%0.8f v_m%0.8f ",stVehicleParas.fVehicleSpeed,v_m);
				gcz_serial_v1_dma_printf(SERIAL_UART1,"[RADIO]R:%0.8f\r\n",g_f_radio);
			}
		}
	}
	poweron_check_imu_axis_status(p->acc);
#endif
#ifdef USE_SERIAL_RX_UART5
#endif
#ifdef USE_SERIAL_RX_UART6
#endif
#ifdef USE_SERIAL_RX_UART7
#endif
}

//处理串口发送任务
//说明：主循环轮询调用该函数即可
void process_serial_tx_sub_task()
{
#ifdef USE_SERIAL_TX_UART0
	g_st_serial_uart0_write.arry_buffer = g_arry_uart0_write_buffer;
	g_st_serial_opt.serial_tx_polling_auto_write(SERIAL_UART0,g_st_serial_uart0_write.arry_buffer,&g_st_serial_uart0_write.u32_length);
#endif
#ifdef USE_SERIAL_TX_UART1
	g_st_serial_uart1_write.arry_buffer = g_arry_uart1_write_buffer;
	g_st_serial_opt.serial_tx_polling_auto_write(SERIAL_UART1,g_st_serial_uart1_write.arry_buffer,&g_st_serial_uart1_write.u32_length);
#endif
#ifdef USE_SERIAL_TX_UART2
	g_st_serial_uart2_write.arry_buffer = g_arry_uart2_write_buffer;
	g_st_serial_opt.serial_tx_polling_auto_write(SERIAL_UART2,g_st_serial_uart2_write.arry_buffer,&g_st_serial_uart2_write.u32_length);
#endif
#ifdef USE_SERIAL_TX_UART3
	g_st_serial_uart3_write.arry_buffer = g_arry_uart3_write_buffer;
	g_st_serial_opt.serial_tx_polling_auto_write(SERIAL_UART3,g_st_serial_uart3_write.arry_buffer,&g_st_serial_uart3_write.u32_length);
#endif
#ifdef USE_SERIAL_TX_UART4
	g_st_serial_uart4_write.arry_buffer = g_arry_uart4_write_buffer;
	//UART4自动发送缓存内数据的处理函数
	g_st_serial_opt.serial_tx_polling_auto_write(SERIAL_UART4,g_st_serial_uart4_write.arry_buffer,&g_st_serial_uart4_write.u32_length);
#endif
#ifdef USE_SERIAL_TX_UART5
	g_st_serial_uart5_write.arry_buffer = g_arry_uart5_write_buffer;
	g_st_serial_opt.serial_tx_polling_auto_write(SERIAL_UART5,g_st_serial_uart5_write.arry_buffer,&g_st_serial_uart5_write.u32_length);
#endif
#ifdef USE_SERIAL_TX_UART6
	g_st_serial_uart6_write.arry_buffer = g_arry_uart6_write_buffer;
	g_st_serial_opt.serial_tx_polling_auto_write(SERIAL_UART6,g_st_serial_uart6_write.arry_buffer,&g_st_serial_uart6_write.u32_length);
#endif
#ifdef USE_SERIAL_TX_UART7
	g_st_serial_uart7_write.arry_buffer = g_arry_uart7_write_buffer;
	g_st_serial_opt.serial_tx_polling_auto_write(SERIAL_UART7,g_st_serial_uart7_write.arry_buffer,&g_st_serial_uart7_write.u32_length);
#endif
}
