#ifndef __SERIAL_APP_H
#define __SERIAL_APP_H
#include "serial_v1.h"

/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

extern bool g_b_imu_show;
//处理串口接收任务
//说明：主循环调用
void process_serial_rx_sub_task();
//处理串口发送任务
//主循环调用
void process_serial_tx_sub_task();
#endif
