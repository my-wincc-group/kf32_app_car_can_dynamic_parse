#ifndef __IMU_APP_H
#define __IMU_APP_H
#include <stdbool.h>
#include <stdint.h>
#include "alg_lib.h"
#include "board_config.h"


typedef enum
{
	FUC_NORMAL = 0,
	COMM_ABNORMAL,
	FUNC_ABNORMAL
}IMU_DECT_RES;

extern IMU_DECT_RES g_imu_detection_result;


//设置IMU的通讯状态
void set_imu_comm_flag(bool status);
//IMU功能应用初始化
void imu_app_init();
//上电启动检测IMU功能
void poweron_check_imu_axis_status(float acc[]);
#endif
