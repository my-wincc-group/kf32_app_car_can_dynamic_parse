#include "imu_app.h"
#include "alg_lib.h"
#include <stdlib.h>
#include "math.h"
#include "tool.h"
#include "wt_jy901_imu.h"
/*
 * 作者：郭超卓
 * 日期：2022年9月14日
 * */

SOFT_TIMER_OBJ g_st_sft_tmr_obj_imu_app;
extern volatile int64_t SystemtimeClock;

typedef enum _imu_func_status
{
	IMU_INIT = 0,
	IMU_COMM_NORMAL,
	IMU_COMM_LOSS,
	IMU_ACC_AXIS_NORMAL,
	IMU_ACC_AXIS_ABNORMAL,
//	IMU_XZ_AXIS_NORMAL,
//	IMU_X_AXIS_ABNORMAL,
//	IMU_Z_AXIS_ABNORMAL,
//	IMU_XZ_AXIS_ABNORMAL,
}IMU_FUNC_STATUS;

IMU_DECT_RES g_imu_detection_result = COMM_ABNORMAL;

typedef enum _imu_detect_status
{
	POWER_ON = 0,
	CHECK_COMM,
	CHECK_AXIS,
	CHECK_OVER
}IMU_DECT_STATUS;
typedef struct __imu_fuc_detect
{
	IMU_FUNC_STATUS comm_status;	//IMU通讯状态
	IMU_FUNC_STATUS axis_status;	//IMU轴状态
	IMU_FUNC_STATUS detection_result;
	IMU_DECT_STATUS detection_status_mchn;		//检测功能的状态机
	uint16_t timer;
	uint16_t calc_timer;
	bool comm_flag;					//通讯状态检查标志位
}IMU_FUC_DETC;
IMU_FUC_DETC g_imu_fuc_detection;

//设置IMU的通讯状态
void set_imu_comm_flag(bool status)
{
	g_imu_fuc_detection.comm_flag = status;
}

void imu_comm_check_timer_call_back(void * arg)
{
	IMU_FUC_DETC *p = (IMU_FUC_DETC *)arg;
	if  (p->comm_flag == false)
	{
		p->comm_status = IMU_COMM_LOSS;
	}
	else
		p->comm_status = IMU_COMM_NORMAL;

	p->comm_flag = false;

	if (p->timer >= 1)
		p->timer++;
}
//IMU功能应用初始化
void imu_app_init()
{
	g_imu_fuc_detection.comm_status = IMU_INIT;
	g_imu_fuc_detection.axis_status = IMU_INIT;
	g_imu_fuc_detection.timer = 0;
	g_imu_fuc_detection.calc_timer = 0;
	g_imu_fuc_detection.detection_status_mchn = POWER_ON;
	//创建软定时器
	g_st_soft_timer_opt.sft_tmr_creat(&g_st_sft_tmr_obj_imu_app,			//定时器对象
										&SystemtimeClock,					//滴答定时器时钟接口
										TRIGGER_PERIOD,						//定时器触发模式
										500,								//定时周期
										(void *)&g_imu_fuc_detection);		//回调函数传入的参数
	//开启定时器执行回调函数
	g_st_soft_timer_opt.sft_tmr_start(&g_st_sft_tmr_obj_imu_app,			//定时器对象
										imu_comm_check_timer_call_back);	//定时周期执行回调函数
}

//检测IMU的X轴加速度状态
bool check_imu_acc_axis_status(float f_acc,float f_min,float f_max)
{
	bool res = false;
//	gcz_serial_v1_dma_printf(SERIAL_UART1,"%f\r\n",f_x_acc);
	if ((f_acc <= f_max) && (f_acc >= f_min))
		res = true;
	return res;
}
//检测IMU的Z轴角速度状态
//bool check_imu_z_deg_sp_axis_status(float f_z_deg_sp)
//{
//	bool res = false;
//	if (fabs(f_z_deg_sp) <= 0.5)
//		res = true;
//	return res;
//}
#define IMU_CACHE_NBR			11
typedef struct __imu_cache
{
	float f_val[IMU_CACHE_NBR];
	uint8_t cnt;
}IMU_CACHE;

//IMU_CACHE g_st_imu_filter_X_acc_cache[3];
//IMU_CACHE g_st_imu_filter_Z_dep_sp_cache;
//IMU的缓存队列，后续供冒泡均值过滤使用
//void imu_cache_queue(IMU_CACHE *p_imu_cache,float f_val,uint8_t nbr)
//{
////	gcz_printf(USART1_SFR,"****cnt=%d,f_val=%0.2f\r\n",p_imu_cache->cnt,p_imu_cache->f_val[p_imu_cache->cnt]);
//	p_imu_cache->f_val[p_imu_cache->cnt] = f_val;
//	p_imu_cache->cnt++;
//	if (p_imu_cache->cnt >= nbr)
//		p_imu_cache->cnt = 0;
//}
//上电启动检测IMU功能
void poweron_check_imu_axis_status(float acc[])
{
	if (g_imu_fuc_detection.detection_status_mchn == POWER_ON)
	{
		g_imu_fuc_detection.detection_status_mchn = CHECK_COMM;
		g_imu_fuc_detection.timer = 1;
	}
	else if (g_imu_fuc_detection.detection_status_mchn == CHECK_COMM)
	{
		if (g_imu_fuc_detection.timer >= 3) //(3-1) *500 = 1s
		{
			if (g_imu_fuc_detection.comm_status == IMU_COMM_NORMAL)
			{
				g_imu_fuc_detection.detection_result = IMU_COMM_NORMAL;
				g_imu_fuc_detection.detection_status_mchn = CHECK_AXIS;
				g_imu_fuc_detection.timer = 1;
			}
			else
			{
				g_imu_fuc_detection.detection_result = IMU_COMM_LOSS;
				g_imu_fuc_detection.detection_status_mchn = CHECK_OVER;
				g_imu_fuc_detection.timer = 0;
			}
		}
	}
	else if (g_imu_fuc_detection.detection_status_mchn == CHECK_AXIS)
	{
		static float x_acc_fliter_res[3] = {0.0,0.0,0.0};
		static uint32_t cnt = 0;
		if (g_imu_fuc_detection.timer <= 5)//(5-1) *500 = 2s
		{
			if ((SystemtimeClock - g_imu_fuc_detection.calc_timer) >= 40)
			{
				g_imu_fuc_detection.calc_timer = SystemtimeClock;
				cnt++;
//				imu_cache_queue(&g_st_imu_filter_X_acc_cache[X_P],acc[X_P],IMU_CACHE_NBR);
//				x_acc_fliter_res[X_P] += bubble_sort_avg_filter(g_st_imu_filter_X_acc_cache[X_P].f_val,IMU_CACHE_NBR,0.25);
//
//				imu_cache_queue(&g_st_imu_filter_X_acc_cache[Y_P],acc[Y_P],IMU_CACHE_NBR);
//				x_acc_fliter_res[Y_P] += bubble_sort_avg_filter(g_st_imu_filter_X_acc_cache[Y_P].f_val,IMU_CACHE_NBR,0.25);
//
//				imu_cache_queue(&g_st_imu_filter_X_acc_cache[Z_P],acc[Z_P],IMU_CACHE_NBR);
//				x_acc_fliter_res[Z_P] += bubble_sort_avg_filter(g_st_imu_filter_X_acc_cache[Z_P].f_val,IMU_CACHE_NBR,0.25);


				x_acc_fliter_res[X_P] += acc[X_P];

				x_acc_fliter_res[Y_P] += acc[Y_P];

				x_acc_fliter_res[Z_P] += acc[Z_P];

//				gcz_serial_v1_dma_printf(SERIAL_UART1,"------------------------\r\n"
//													  "[cal]acc:%0.6f\r\n"
//													  "[org]acc:%0.6f\r\n",x_acc_fliter_res[Z_P]/cnt,acc[Z_P]);
			}
		}
		else
		{
			bool res_acc[3];
			g_imu_fuc_detection.timer = 0;
			g_imu_fuc_detection.calc_timer  = 0;
			x_acc_fliter_res[X_P] /= cnt;
			x_acc_fliter_res[Y_P] /= cnt;
			x_acc_fliter_res[Z_P] /= cnt;

			res_acc[X_P] = check_imu_acc_axis_status(x_acc_fliter_res[X_P]/9.8,-0.5,0.5);
			res_acc[Y_P] = check_imu_acc_axis_status(x_acc_fliter_res[Y_P]/9.8,-0.5,0.5);
			res_acc[Z_P] = check_imu_acc_axis_status(x_acc_fliter_res[Z_P]/9.8,0.6,1.4);

			if ((res_acc[X_P] == true) && (res_acc[Y_P] == true) && (res_acc[Z_P] == true))
			{
				g_imu_fuc_detection.axis_status = IMU_ACC_AXIS_NORMAL;
			}
			else
			{
				g_imu_fuc_detection.axis_status = IMU_ACC_AXIS_ABNORMAL;
			}

			g_imu_fuc_detection.detection_result = g_imu_fuc_detection.axis_status;
			g_imu_fuc_detection.detection_status_mchn = CHECK_OVER;

//#define name2str(name) (#name)
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"[1%s]%d acc:%0.6f\r\n",name2str(detection_result),g_imu_fuc_detection.detection_result,x_acc_fliter_res[X_P]);
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"[2%s]%d acc:%0.6f\r\n",name2str(detection_result),g_imu_fuc_detection.detection_result,x_acc_fliter_res[Y_P]);
//			gcz_serial_v1_dma_printf(SERIAL_UART1,"[3%s]%d acc:%0.6f\r\n",name2str(detection_result),g_imu_fuc_detection.detection_result,x_acc_fliter_res[Z_P]);
		}
	}
	else if (g_imu_fuc_detection.detection_status_mchn == CHECK_OVER)
	{
		if (g_imu_fuc_detection.detection_result == IMU_COMM_LOSS)
			g_imu_detection_result = COMM_ABNORMAL;
		else if (g_imu_fuc_detection.detection_result == IMU_ACC_AXIS_NORMAL)
			g_imu_detection_result = FUC_NORMAL;
		else
			g_imu_detection_result = FUNC_ABNORMAL;
	}
}
