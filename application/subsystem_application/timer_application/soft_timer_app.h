#ifndef __SOFT_TIMER_APP_H
#define __SOFT_TIMER_APP_H

/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */


//处理软定时器响应任务
//说明：主循环调用
void process_soft_timer_sub_task();
#endif
