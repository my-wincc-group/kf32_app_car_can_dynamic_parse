#include "soft_timer_app.h"
#include "soft_timer.h"
#include "wt_jy901_imu.h"

//软件定时器触发执行任务
void process_soft_timer_sub_task()
{
	for (uint16_t i1 = 0;i1 <= MAX_SUPPORT_SOFT_TIMER_NBR;i1++)
		g_st_soft_timer_opt.sft_tmr_running(g_st_soft_timer_opt.p_sft_tme_obj_list[i1]);
}
