#include "soft_timer.h"
#include "tool.h"

//系统滴答时钟频率
//#define	FRQ_1HZ			1
//#define	FRQ_10HZ		10
//#define	FRQ_100HZ		100
//#define	FRQ_1000HZ		1000

//定时器运行状态
#define	SOFT_TIMER_STATE_INIT	0
#define	SOFT_TIMER_STATE_START	1
#define	SOFT_TIMER_STATE_CLOSE	2

//软定时器创建
//参数：定时器对象	系统滴答时钟		系统滴答时钟频率		触发模式		执行周期(ms)		回调函数的传入参数
bool soft_timer_creat(SOFT_TIMER_OBJ *p_st_sft_tmr_obj,
						volatile int64_t *sys_tick_clk,
			//			uint32_t sys_clk_freq,
						uint8_t trigger_mode,
						uint32_t execute_period_tick,
						void *call_back_arg)
{
	if (p_st_sft_tmr_obj == NULL)
		return false;
	p_st_sft_tmr_obj->p_sys_tick_clk 		= sys_tick_clk;
//	p_st_sft_tmr_obj->sys_clk_freq 			= sys_clk_freq;
	p_st_sft_tmr_obj->trigger_mode 			= trigger_mode;
	p_st_sft_tmr_obj->execute_period_tick 	= execute_period_tick;
	p_st_sft_tmr_obj->timer_running_state	= SOFT_TIMER_STATE_INIT;
	p_st_sft_tmr_obj->call_back_arg			= call_back_arg;


	g_st_soft_timer_opt.p_sft_tme_obj_list[g_st_soft_timer_opt.sft_tmr_list_ptr] = p_st_sft_tmr_obj;
	g_st_soft_timer_opt.sft_tmr_list_ptr++;
	if (g_st_soft_timer_opt.sft_tmr_list_ptr >= MAX_SUPPORT_SOFT_TIMER_NBR)
	{
		g_st_soft_timer_opt.sft_tmr_list_ptr = MAX_SUPPORT_SOFT_TIMER_NBR;
	//	gcz_serial_v1_polling_printf(SERIAL_UART1,"beyond the maximum support number\r\n");
	}
	return true;
}
//软件定时器开始执行
bool soft_timer_start(SOFT_TIMER_OBJ *p_st_sft_tmr_obj,soft_timer_func excute_call_back)
{
	if (p_st_sft_tmr_obj == NULL)
		return false;
	p_st_sft_tmr_obj->timer_running_state = SOFT_TIMER_STATE_START;
	p_st_sft_tmr_obj->soft_timer_call_back = excute_call_back;
	p_st_sft_tmr_obj->last_sys_tick_clk = *p_st_sft_tmr_obj->p_sys_tick_clk;
	return true;
}

//软件定时器结束
bool soft_timer_close(SOFT_TIMER_OBJ *p_st_sft_tmr_obj)
{
	if (p_st_sft_tmr_obj == NULL)
		return false;
	p_st_sft_tmr_obj->timer_running_state = SOFT_TIMER_STATE_CLOSE;
	p_st_sft_tmr_obj->soft_timer_call_back = NULL;
	return true;
}

//软件定时器修改执行周期
bool soft_timer_modify_execute_period(SOFT_TIMER_OBJ *p_st_sft_tmr_obj,uint32_t execute_period_tick)
{
	if (p_st_sft_tmr_obj == NULL)
		return false;
	p_st_sft_tmr_obj->execute_period_tick = execute_period_tick;
	return true;
}
//软件定时器运行调用函数
bool soft_timer_running(SOFT_TIMER_OBJ *p_st_sft_tmr_obj)
{
	if (p_st_sft_tmr_obj == NULL)
		return false;
	if (p_st_sft_tmr_obj->timer_running_state == SOFT_TIMER_STATE_START)
	{
		if ((*p_st_sft_tmr_obj->p_sys_tick_clk - p_st_sft_tmr_obj->last_sys_tick_clk) >= p_st_sft_tmr_obj->execute_period_tick)
		{
			if (p_st_sft_tmr_obj->trigger_mode == TRIGGER_ONE_SHOT)
			{
				p_st_sft_tmr_obj->soft_timer_call_back(p_st_sft_tmr_obj->call_back_arg);
				soft_timer_close(p_st_sft_tmr_obj);
			}
			else
			{
				p_st_sft_tmr_obj->soft_timer_call_back(p_st_sft_tmr_obj->call_back_arg);
				p_st_sft_tmr_obj->last_sys_tick_clk = *p_st_sft_tmr_obj->p_sys_tick_clk;
			}
		}
	}
	else
	{
		p_st_sft_tmr_obj->last_sys_tick_clk = *p_st_sft_tmr_obj->p_sys_tick_clk;
	}
	return true;
}

SOFT_TIMER_OPT g_st_soft_timer_opt =
{
	.sft_tmr_creat = soft_timer_creat,
	.sft_tmr_start = soft_timer_start,
	.sft_tmr_close = soft_timer_close,
	.sft_tmr_modify_execute_period = soft_timer_modify_execute_period,
	.sft_tmr_running = soft_timer_running,
};
