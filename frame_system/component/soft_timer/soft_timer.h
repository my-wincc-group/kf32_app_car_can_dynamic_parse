#ifndef __SOFT_TIMER_H
#define __SOFT_TIMER_H
#include <stdbool.h>
#include <stdint.h>
//定时器触发类型
#define TRIGGER_ONE_SHOT	1
#define	TRIGGER_PERIOD		2

typedef void (*soft_timer_func)(void *);

typedef struct _soft_timer_obj
{
	volatile int64_t 	*p_sys_tick_clk;						//系统滴答时钟
//	uint32_t 	sys_clk_freq;							//系统滴答时钟频率 1HZ/10HZ/100HZ
	uint8_t 	trigger_mode;							//触发模式
	uint32_t 	execute_period_tick;							//执行周期
	uint32_t 	timer_running_state;					//定时器运行状态
	int64_t 	last_sys_tick_clk;							//上一次系统时钟
	soft_timer_func	soft_timer_call_back;				//定时执行任务的回调函数
	void *call_back_arg;								//回调函数传入参数
}SOFT_TIMER_OBJ;

#define MAX_SUPPORT_SOFT_TIMER_NBR		255
typedef struct _soft_timer_opt
{
//---------------定时器创建调用-------------------------------------
	bool (*sft_tmr_creat)(SOFT_TIMER_OBJ *,volatile int64_t *,uint8_t,uint32_t,void *);	//软定时器创建
//---------------应用层调用-------------------------------------
	bool (*sft_tmr_start)(SOFT_TIMER_OBJ *,soft_timer_func);					//软件定时器开启
	bool (*sft_tmr_close)(SOFT_TIMER_OBJ *);									//软件定时器关闭
	bool (*sft_tmr_modify_execute_period)(SOFT_TIMER_OBJ *,uint32_t);			//软件定时器修改周期tick
//---------------系统层轮询调用---------------------------------
	bool (*sft_tmr_running)(SOFT_TIMER_OBJ *);									//软件定时器运行轮询调用
//***************定时器加入自动管理******************************
	SOFT_TIMER_OBJ *p_sft_tme_obj_list[MAX_SUPPORT_SOFT_TIMER_NBR];
	uint16_t sft_tmr_list_ptr;
}SOFT_TIMER_OPT;
extern SOFT_TIMER_OPT g_st_soft_timer_opt;
#endif
