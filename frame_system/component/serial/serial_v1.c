#include "serial_v1.h"
#include "string.h"
#include "tool.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

//定义串口接收时，封包协议处理的回调函数

//接收缓存
typedef struct _serial_cache
{
//	uint8_t 	_U8_buffer[SERIAL_4_RCV_LENGTH];	//接收缓存
	uint8_t 	*_p_U8_buffer;	//接收缓存
	uint32_t 	_U32_length;				//接收长度
	bool 		_B_flag;					//是否接收成功标志位
}SERIAL_CACHE;
//接收缓存操作对象
typedef struct _serial_rcv_obj
{
//	SERIAL_CACHE _ST_serial_cache[RX_BUFFER_NBR];	//缓存对象
	SERIAL_CACHE *_p_ST_serial_cache;	//缓存对象
	uint16_t 		_U16_put_ptr;						//入栈指针
	uint16_t 		_U16_get_ptr;						//出栈指针
}SERIAL_OBJ;
////发送缓存
//typedef struct _serial_send_cache
//{
////	uint8_t 	_U8_buffer[TX_BUFFER_NBR];	//接收缓存
//	uint8_t 	*_p_U8_buffer;	//接收缓存
//	uint32_t 	_U32_length;				//接收长度
//	bool 		_B_flag;					//是否接收成功标志位
//}SERIAL_CACHE;
////发送缓存操作对象
//typedef struct _serial_send_obj
//{
////	SERIAL_CACHE _ST_serial_cache[TX_BUFFER_LEN];	//缓存对象
//	SERIAL_CACHE *_p_ST_serial_cache;	//缓存对象
//	uint16_t 		_U16_put_ptr;						//入栈指针
//	uint16_t 		_U16_get_ptr;						//出栈指针
//}SERIAL_OBJ;


//串口使用配置
#ifdef USE_SERIAL_RX_UART0
static uint8_t g_arry_serial_0_rx_buffer[SERIAL_0_RX_BUFFER_NBR][SERIAL_0_RCV_LENGTH];
static SERIAL_CACHE g_st_serial_0_cache_rx[SERIAL_0_RX_BUFFER_NBR];
SERIAL_OBJ g_st_uart0_obj_rx;
#endif
#ifdef USE_SERIAL_TX_UART0
static uint8_t g_arry_serial_0_tx_buffer[SERIAL_0_TX_BUFFER_NBR][SERIAL_0_SND_LENGTH];
static SERIAL_CACHE g_st_serial_0_cache_tx[SERIAL_0_TX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart0_obj_tx;
#endif

#ifdef USE_SERIAL_RX_UART1
static uint8_t g_arry_serial_1_rx_buffer[SERIAL_1_RX_BUFFER_NBR][SERIAL_1_RCV_LENGTH];
static SERIAL_CACHE g_st_serial_1_cache_rx[SERIAL_1_RX_BUFFER_NBR];
SERIAL_OBJ g_st_uart1_obj_rx;
#endif
#ifdef USE_SERIAL_TX_UART1
static uint8_t g_arry_serial_1_tx_buffer[SERIAL_1_TX_BUFFER_NBR][SERIAL_1_SND_LENGTH];
static SERIAL_CACHE g_st_serial_1_cache_tx[SERIAL_1_TX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart1_obj_tx;
#endif

#ifdef USE_SERIAL_RX_UART2
static uint8_t g_arry_serial_2_rx_buffer[SERIAL_2_RX_BUFFER_NBR][SERIAL_2_RCV_LENGTH];
static SERIAL_CACHE g_st_serial_2_cache_rx[SERIAL_2_RX_BUFFER_NBR];
SERIAL_OBJ g_st_uart2_obj_rx;
#endif
#ifdef USE_SERIAL_TX_UART2
static uint8_t g_arry_serial_2_tx_buffer[SERIAL_2_TX_BUFFER_NBR][SERIAL_2_SND_LENGTH];
static SERIAL_CACHE g_st_serial_2_cache_tx[SERIAL_2_TX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart2_obj_tx;
#endif

#ifdef USE_SERIAL_RX_UART3
static uint8_t g_arry_serial_3_rx_buffer[SERIAL_3_RX_BUFFER_NBR][SERIAL_3_RCV_LENGTH];
static SERIAL_CACHE g_st_serial_3_cache_rx[SERIAL_3_RX_BUFFER_NBR];
SERIAL_OBJ g_st_uart3_obj_rx;
#endif
#ifdef USE_SERIAL_TX_UART3
static uint8_t g_arry_serial_3_tx_buffer[SERIAL_3_TX_BUFFER_NBR][SERIAL_3_SND_LENGTH];
static SERIAL_CACHE g_st_serial_3_cache_tx[SERIAL_3_TX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart3_obj_tx;
#endif

#ifdef USE_SERIAL_RX_UART4
static uint8_t g_arry_serial_4_rx_buffer[SERIAL_4_RX_BUFFER_NBR][SERIAL_4_RCV_LENGTH];
static SERIAL_CACHE g_st_serial_4_cache_rx[SERIAL_4_RX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart4_obj_rx;
#endif
#ifdef USE_SERIAL_TX_UART4
static uint8_t g_arry_serial_4_tx_buffer[SERIAL_4_TX_BUFFER_NBR][SERIAL_4_SND_LENGTH];
static SERIAL_CACHE g_st_serial_4_cache_tx[SERIAL_4_TX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart4_obj_tx;
#endif

#ifdef USE_SERIAL_RX_UART5
static uint8_t g_arry_serial_5_rx_buffer[SERIAL_5_RX_BUFFER_NBR][SERIAL_5_RCV_LENGTH];
static SERIAL_CACHE g_st_serial_5_cache_rx[SERIAL_5_RX_BUFFER_NBR];
SERIAL_OBJ g_st_uart5_obj_rx;
#endif
#ifdef USE_SERIAL_TX_UART5
static uint8_t g_arry_serial_5_tx_buffer[SERIAL_5_TX_BUFFER_NBR][SERIAL_5_SND_LENGTH];
static SERIAL_CACHE g_st_serial_5_cache_tx[SERIAL_5_TX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart5_obj_tx;
#endif

#ifdef USE_SERIAL_RX_UART6
static uint8_t g_arry_serial_6_rx_buffer[SERIAL_6_RX_BUFFER_NBR][SERIAL_6_RCV_LENGTH];
static SERIAL_CACHE g_st_serial_6_cache_rx[SERIAL_6_RX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart6_obj_rx;
#endif
#ifdef USE_SERIAL_TX_UART6
static uint8_t g_arry_serial_6_tx_buffer[SERIAL_6_TX_BUFFER_NBR][SERIAL_6_SND_LENGTH];
static SERIAL_CACHE g_st_serial_6_cache_tx[SERIAL_6_TX_BUFFER_NBR];
SERIAL_OBJ g_st_uart6_obj_tx;
#endif

#ifdef USE_SERIAL_RX_UART7
static uint8_t g_arry_serial_7_rx_buffer[SERIAL_7_RX_BUFFER_NBR][SERIAL_7_RCV_LENGTH];
static SERIAL_CACHE g_st_serial_7_cache_rx[SERIAL_7_RX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart7_obj_rx;
#endif
#ifdef USE_SERIAL_TX_UART7
static uint8_t g_arry_serial_7_tx_buffer[SERIAL_7_TX_BUFFER_NBR][SERIAL_7_SND_LENGTH];
static SERIAL_CACHE g_st_serial_7_cache_tx[SERIAL_7_TX_BUFFER_NBR];
static SERIAL_OBJ g_st_uart7_obj_tx;
#endif


/*
//----------------------函数声明----------------------------------------------------
//初始化串口缓存
void init_serial();
//串口接收，入队列API
//参数：串口号0~7      待缓存数据		数据长度
//返回：true成功 false失败
bool serial_rx_put_char_to_queue(uint8_t serial_nbr,const uint8_t *buffer,uint32_t length);
//串口接收，出队列API
//参数：串口号0~7      缓存数据		数据长度
//返回：true成功 false失败
bool serial_rx_get_data_from_queue(uint8_t serial_nbr,uint8_t *buffer,uint32_t *length);
//串口发送，入队列API
//参数：串口号0~7      待缓存数据		数据长度
//返回：true成功 false失败
bool serial_tx_put_data_to_queue(uint8_t serial_nbr,const uint8_t *buffer,uint32_t length);
//串口发送，出队列API
//参数：串口号0~7      缓存数据		数据长度
//返回：true成功 false失败
bool serial_tx_get_data_from_queue(uint8_t serial_nbr,uint8_t *buffer,uint32_t *length);
//串口调用启动发送缓存获取成功数据API
//参数：串口号0~7      缓存数据		数据长度
//返回：true成功 false失败
bool serial_tx_use_hardware_write(uint8_t serial_nbr,uint8_t *buffer,uint32_t *length);
//串口中断API函数
//内含封包协议处理
void serial_rx_isr(uint8_t serial_nbr,uint8_t data);
//串口超时中断API函数
void serial_rx_timeout_isr(uint8_t serial_nbr);
//串口插入数据，移动指针到下一个位置
bool rx_insert_move_to_next_ptr(uint8_t serial_nbr);
//--------------------------------------------------------------------------
*/
//	1.获取接收串口数据结构对象
SERIAL_OBJ * get_serial_struct_obj_rx(uint8_t serial_nbr)
{
	SERIAL_OBJ *p_st_serial_obj;
	switch(serial_nbr)
	{
#ifdef USE_SERIAL_RX_UART0
	case 0:p_st_serial_obj = &g_st_uart0_obj_rx;break;
#endif
#ifdef USE_SERIAL_RX_UART1
	case 1:p_st_serial_obj = &g_st_uart1_obj_rx;break;
#endif
#ifdef USE_SERIAL_RX_UART2
	case 2:p_st_serial_obj = &g_st_uart2_obj_rx;break;
#endif
#ifdef USE_SERIAL_RX_UART3
	case 3:p_st_serial_obj = &g_st_uart3_obj_rx;break;
#endif
#ifdef USE_SERIAL_RX_UART4
	case 4:p_st_serial_obj = &g_st_uart4_obj_rx;break;
#endif
#ifdef USE_SERIAL_RX_UART5
	case 5:p_st_serial_obj = &g_st_uart5_obj_rx;break;
#endif
#ifdef USE_SERIAL_RX_UART6
	case 6:p_st_serial_obj = &g_st_uart6_obj_rx;break;
#endif
#ifdef USE_SERIAL_RX_UART7
	case 7:p_st_serial_obj = &g_st_uart7_obj_rx;break;
#endif
		default:p_st_serial_obj = NULL;break;
	}
	return p_st_serial_obj;
}


//	2.获取发送串口数据结构对象
SERIAL_OBJ *get_serial_struct_obj_tx(uint8_t serial_nbr)
{
	SERIAL_OBJ *p_st_serial_obj;
	switch(serial_nbr)
	{
#ifdef USE_SERIAL_TX_UART0
	case 0:p_st_serial_obj = &g_st_uart0_obj_tx;break;
#endif
#ifdef USE_SERIAL_TX_UART1
	case 1:p_st_serial_obj = &g_st_uart1_obj_tx;break;
#endif
#ifdef USE_SERIAL_TX_UART2
	case 2:p_st_serial_obj = &g_st_uart2_obj_tx;break;
#endif
#ifdef USE_SERIAL_TX_UART3
	case 3:p_st_serial_obj = &g_st_uart3_obj_tx;break;
#endif
#ifdef USE_SERIAL_TX_UART4
	case 4:p_st_serial_obj = &g_st_uart4_obj_tx;break;
#endif
#ifdef USE_SERIAL_TX_UART5
	case 5:p_st_serial_obj = &g_st_uart5_obj_tx;break;
#endif
#ifdef USE_SERIAL_TX_UART6
	case 6:p_st_serial_obj = &g_st_uart6_obj_tx;break;
#endif
#ifdef USE_SERIAL_TX_UART7
	case 7:p_st_serial_obj = &g_st_uart7_obj_tx;break;
#endif
		default:p_st_serial_obj = NULL;break;
	}
	return p_st_serial_obj;
}
//获取串口接收缓存数量
uint16_t get_serial_rx_queue_nbr(uint8_t serial_nbr)
{
	uint16_t rx_q_nbr;
	switch(serial_nbr)
	{
#ifdef USE_SERIAL_RX_UART0
	case 0:rx_q_nbr = SERIAL_0_RX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_RX_UART1
	case 1:rx_q_nbr = SERIAL_1_RX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_RX_UART2
	case 2:rx_q_nbr = SERIAL_2_RX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_RX_UART3
	case 3:rx_q_nbr = SERIAL_3_RX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_RX_UART4
	case 4:rx_q_nbr = SERIAL_4_RX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_RX_UART5
	case 5:rx_q_nbr = SERIAL_5_RX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_RX_UART6
	case 6:rx_q_nbr = SERIAL_6_RX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_RX_UART7
	case 7:rx_q_nbr = SERIAL_7_RX_BUFFER_NBR;break;
#endif
	default:rx_q_nbr = 0;break;
	}
	return rx_q_nbr;
}
//获取串口接收缓存数量
uint16_t get_serial_rx_buffer_length(uint8_t serial_nbr)
{
	uint16_t rx_length;
	switch(serial_nbr)
	{
#ifdef USE_SERIAL_RX_UART0
	case 0:rx_length = SERIAL_0_RCV_LENGTH;break;
#endif
#ifdef USE_SERIAL_RX_UART1
	case 1:rx_length = SERIAL_1_RCV_LENGTH;break;
#endif
#ifdef USE_SERIAL_RX_UART2
	case 2:rx_length = SERIAL_2_RCV_LENGTH;break;
#endif
#ifdef USE_SERIAL_RX_UART3
	case 3:rx_length = SERIAL_3_RCV_LENGTH;break;
#endif
#ifdef USE_SERIAL_RX_UART4
	case 4:rx_length = SERIAL_4_RCV_LENGTH;break;
#endif
#ifdef USE_SERIAL_RX_UART5
	case 5:rx_length = SERIAL_5_RCV_LENGTH;break;
#endif
#ifdef USE_SERIAL_RX_UART6
	case 6:rx_length = SSERIAL_6_RCV_LENGTH;break;
#endif
#ifdef USE_SERIAL_RX_UART7
	case 7:rx_length = SERIAL_7_RCV_LENGTH;break;
#endif
	default:rx_length = 0;break;
	}
	return rx_length;
}
//获取串口发送缓存数量
uint16_t get_serial_tx_queue_nbr(uint8_t serial_nbr)
{
	uint16_t tx_q_nbr;
	switch(serial_nbr)
	{
#ifdef USE_SERIAL_TX_UART0
	case 0:tx_q_nbr = SERIAL_0_TX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_TX_UART1
	case 1:tx_q_nbr = SERIAL_1_TX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_TX_UART2
	case 2:tx_q_nbr = SERIAL_2_TX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_TX_UART3
	case 3:tx_q_nbr = SERIAL_3_TX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_TX_UART4
	case 4:tx_q_nbr = SERIAL_4_TX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_TX_UART5
	case 5:tx_q_nbr = SERIAL_5_TX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_TX_UART6
	case 6:tx_q_nbr = SERIAL_6_TX_BUFFER_NBR;break;
#endif
#ifdef USE_SERIAL_TX_UART7
	case 7:tx_q_nbr = SERIAL_7_TX_BUFFER_NBR;break;
#endif
	default:tx_q_nbr = 0;break;
	}
	return tx_q_nbr;
}
//获取串口发送缓存数量
uint16_t get_serial_tx_buffer_length(uint8_t serial_nbr)
{
	uint16_t tx_length;
	switch(serial_nbr)
	{
#ifdef USE_SERIAL_TX_UART0
	case 0:tx_length = SERIAL_0_SND_LENGTH;break;
#endif
#ifdef USE_SERIAL_TX_UART1
	case 1:tx_length = SERIAL_1_SND_LENGTH;break;
#endif
#ifdef USE_SERIAL_TX_UART2
	case 2:tx_length = SERIAL_2_SND_LENGTH;break;
#endif
#ifdef USE_SERIAL_TX_UART3
	case 3:tx_length = SERIAL_3_SND_LENGTH;break;
#endif
#ifdef USE_SERIAL_TX_UART4
	case 4:tx_length = SERIAL_4_SND_LENGTH;break;
#endif
#ifdef USE_SERIAL_TX_UART5
	case 5:tx_length = SERIAL_5_SND_LENGTH;break;
#endif
#ifdef USE_SERIAL_TX_UART6
	case 6:tx_length = SERIAL_6_SND_LENGTH;break;
#endif
#ifdef USE_SERIAL_TX_UART7
	case 7:tx_length = SERIAL_7_SND_LENGTH;break;
#endif
	default:tx_length = 0;break;
	}
	return tx_length;
}//-------------------------------------------------------------------------------
//初始化一个接收串口
void init_serial_single_obj(SERIAL_OBJ *p_st_serial,SERIAL_CACHE *p_st_serial_cache,uint16_t total_nbr,
								uint8_t *buffer[],uint32_t buferlen)
{
	p_st_serial->_U16_get_ptr = 0;
	p_st_serial->_U16_put_ptr = 0;
	p_st_serial->_p_ST_serial_cache = p_st_serial_cache;
	if (p_st_serial->_p_ST_serial_cache == NULL)
		return;
	for (uint8_t i1 = 0;i1 < total_nbr;i1++)
	{
		p_st_serial->_p_ST_serial_cache[i1]._B_flag		= false;
		p_st_serial->_p_ST_serial_cache[i1]._U32_length 	= 0;
		p_st_serial->_p_ST_serial_cache[i1]._p_U8_buffer =	buffer[i1];
		memset(p_st_serial->_p_ST_serial_cache[i1]._p_U8_buffer,0,buferlen);
	}
}
//初始化串口接收对象
void init_serial_obj_rx(uint8_t serial_nbr)
{
	SERIAL_OBJ *p_st_serial_rx = NULL;
	p_st_serial_rx = get_serial_struct_obj_rx(serial_nbr);
	if (p_st_serial_rx == NULL)
		return;

	p_st_serial_rx->_U16_get_ptr = 0;
	p_st_serial_rx->_U16_put_ptr = 0;
	SERIAL_CACHE *p_st_serial_cache_rx;
	uint32_t length = get_serial_rx_buffer_length(serial_nbr);
	uint16_t rx_q_nbr = get_serial_rx_queue_nbr(serial_nbr),j1;
	uint8_t *buffer[MAX_SERIAL_RX_BUFFER_NBR];

	switch(serial_nbr)
	{
#ifdef USE_SERIAL_RX_UART0
	case 0:p_st_serial_obj = &g_st_uart0_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_serial_0_rx_buffer[j1];
			break;
#endif
#ifdef USE_SERIAL_RX_UART1
	case 1:p_st_serial_obj = &g_st_uart1_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_serial_1_rx_buffer[j1];
			break;
#endif
#ifdef USE_SERIAL_RX_UART2
	case 2:p_st_serial_obj = &g_st_uart2_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_serial_2_rx_buffer[j1];
			break;
#endif
#ifdef USE_SERIAL_RX_UART3
	case 3:p_st_serial_obj = &g_st_uart3_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_serial_3_rx_buffer[j1];
			break;
#endif
#ifdef USE_SERIAL_RX_UART4
	case 4:p_st_serial_cache_rx = g_st_serial_4_cache_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_serial_4_rx_buffer[j1];
			}
			break;
#endif
#ifdef USE_SERIAL_RX_UART5
	case 5:p_st_serial_obj = &g_st_uart5_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_serial_5_rx_buffer[j1];
			break;
#endif
#ifdef USE_SERIAL_RX_UART6
	case 6:p_st_serial_obj = &g_st_uart6_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_serial_6_rx_buffer[j1];
			break;
#endif
#ifdef USE_SERIAL_RX_UART7
	case 7:p_st_serial_obj = &g_st_uart7_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_serial_7_rx_buffer[j1];
			break;
#endif
		default:p_st_serial_cache_rx = NULL;break;
	}
	init_serial_single_obj(p_st_serial_rx,p_st_serial_cache_rx,rx_q_nbr,
								buffer,length);
}
//初始化一个发送串口
//void init_serial_single_obj_tx(SERIAL_OBJ *p_st_serial_tx,SERIAL_CACHE *p_st_serial_cache,uint16_t tx_total_nbr,
//								uint8_t *buffer[],uint32_t buferlen)
//{
//	p_st_serial_tx->_U16_get_ptr = 0;
//	p_st_serial_tx->_U16_put_ptr = 0;
//	p_st_serial_tx->_p_ST_serial_cache = p_st_serial_cache;
//	if (p_st_serial_tx->_p_ST_serial_cache == NULL)
//		return;
//	for (uint8_t i1 = 0;i1 < tx_total_nbr;i1++)
//	{
//		p_st_serial_tx->_p_ST_serial_cache[i1]._B_flag		= false;
//		p_st_serial_tx->_p_ST_serial_cache[i1]._U32_length 	= 0;
//		p_st_serial_tx->_p_ST_serial_cache[i1]._p_U8_buffer =	buffer[i1];
////		gcz_serial_v1_polling_printf(SERIAL_UART1,"1--[%d]%04X  ",i1,p_st_serial_tx->_p_ST_serial_cache[i1]);
////		gcz_serial_v1_polling_printf(SERIAL_UART1,"%04X  %04X\r\n",p_st_serial_tx->_p_ST_serial_cache[i1]._p_U8_buffer,buffer[i1]);
//		memset(p_st_serial_tx->_p_ST_serial_cache[i1]._p_U8_buffer,0,buferlen);
//	}
//}
//初始化串口发送对象
void init_serial_obj_tx(uint8_t serial_nbr)
{
	SERIAL_OBJ *p_st_serial_tx = NULL;
	p_st_serial_tx = get_serial_struct_obj_tx(serial_nbr);
	if (p_st_serial_tx == NULL)
		return;
	p_st_serial_tx->_U16_get_ptr = 0;
	p_st_serial_tx->_U16_put_ptr = 0;

	SERIAL_CACHE *p_st_serial_cache_tx;
	uint16_t tx_q_nbr = get_serial_tx_queue_nbr(serial_nbr);
	uint32_t length = get_serial_tx_buffer_length(serial_nbr);
	uint8_t *buffer[MAX_SERIAL_TX_BUFFER_NBR],j1;

	switch(serial_nbr)
	{
#ifdef USE_SERIAL_TX_UART0
	case 0:p_st_serial_cache_tx = g_st_serial_0_cache_tx;
			for (j1 = 0;j1 < tx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_serial_0_tx_buffer[j1];
			}
			break;
#endif
#ifdef USE_SERIAL_TX_UART1
	case 1:p_st_serial_cache_tx = g_st_serial_1_cache_tx;
			for (j1 = 0;j1 < tx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_serial_1_tx_buffer[j1];
			}
			break;
#endif
#ifdef USE_SERIAL_TX_UART2
	case 2:p_st_serial_cache_tx = g_st_serial_2_cache_tx;
			for (j1 = 0;j1 < tx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_serial_2_tx_buffer[j1];
			}
			break;
#endif
#ifdef USE_SERIAL_TX_UART3
	case 3:p_st_serial_cache_tx = g_st_serial_3_cache_tx;
			for (j1 = 0;j1 < tx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_serial_3_tx_buffer[j1];
			}
			break;
#endif
#ifdef USE_SERIAL_TX_UART4
	case 4:p_st_serial_cache_tx = g_st_serial_4_cache_tx;
			for (j1 = 0;j1 < tx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_serial_4_tx_buffer[j1];
			}
			break;
#endif
#ifdef USE_SERIAL_TX_UART5
	case 5:p_st_serial_cache_tx = g_st_serial_5_cache_tx;
			for (j1 = 0;j1 < tx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_serial_5_tx_buffer[j1];
			}
			break;
#endif
#ifdef USE_SERIAL_TX_UART6
	case 6:p_st_serial_cache_tx = g_st_serial_6_cache_tx;
			for (j1 = 0;j1 < tx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_serial_6_tx_buffer[j1];
			}
			break;
#endif
#ifdef USE_SERIAL_TX_UART7
	case 7:p_st_serial_cache_tx = g_st_serial_7_cache_tx;
			for (j1 = 0;j1 < tx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_serial_7_tx_buffer[j1];
			}
			break;
#endif
		default:p_st_serial_cache_tx = NULL;break;
	}
	init_serial_single_obj(p_st_serial_tx,p_st_serial_cache_tx,tx_q_nbr,
								buffer,length);
}
//void init_serial_obj_tx(SERIAL_OBJ *p_st_serial_tx,uint16_t tx_total_nbr)
//{
//	p_st_serial_tx->_U16_get_ptr = 0;
//	p_st_serial_tx->_U16_put_ptr = 0;
//	for (uint8_t i1 = 0;i1 < tx_total_nbr;i1++)
//	{
//		p_st_serial_tx->_ST_serial_cache[i1]._B_flag		= false;
//		p_st_serial_tx->_ST_serial_cache[i1]._U32_length 	= 0;
//		memset(p_st_serial_tx->_ST_serial_cache[i1]._U8_buffer,0,TX_BUFFER_LEN);
//	}
//}
//初始化接收串口
void init_serial_rx()
{
#ifdef USE_SERIAL_RX_UART0
	init_serial_obj_rx(SERIAL_UART0);
#endif

#ifdef USE_SERIAL_RX_UART1
	init_serial_obj_rx(SERIAL_UART1);
#endif

#ifdef USE_SERIAL_RX_UART2
	init_serial_obj_rx(SERIAL_UART2);
#endif

#ifdef USE_SERIAL_RX_UART3
	init_serial_obj_rx(SERIAL_UART3);
#endif

#ifdef USE_SERIAL_RX_UART4
	init_serial_obj_rx(SERIAL_UART4);

#endif

#ifdef USE_SERIAL_RX_UART5
	init_serial_obj_rx(SERIAL_UART5);
#endif

#ifdef USE_SERIAL_RX_UART6
	init_serial_obj_rx(SERIAL_UART6);
#endif

#ifdef USE_SERIAL_RX_UART7
	init_serial_obj_rx(SERIAL_UART7);
#endif
}
//初始化发送串口
void init_serial_tx()
{
#ifdef USE_SERIAL_TX_UART0
	init_serial_obj_tx(SERIAL_UART0);
#endif

#ifdef USE_SERIAL_TX_UART1
	init_serial_obj_tx(SERIAL_UART1);
#endif

#ifdef USE_SERIAL_TX_UART2
	init_serial_obj_tx(SERIAL_UART2);
#endif

#ifdef USE_SERIAL_TX_UART3
	init_serial_obj_tx(SERIAL_UART3);
#endif

#ifdef USE_SERIAL_TX_UART4
	init_serial_obj_tx(SERIAL_UART4);
#endif

#ifdef USE_SERIAL_TX_UART5
	init_serial_obj_tx(SERIAL_UART5);
#endif

#ifdef USE_SERIAL_TX_UART6
	init_serial_obj_tx(SERIAL_UART6);
#endif

#ifdef USE_SERIAL_TX_UART7
	init_serial_obj_tx(SERIAL_UART7);
#endif
}
//初始化串口
void init_serial()
{
	init_serial_rx();
	init_serial_tx();
}

//一、串口缓存出入指针操作维护
////（一）--------------------------串口接收缓存“出入”指针移动操作，用于封包处理-----------------------------------//
//	1.接收缓存封包时“放入指针”移动到下一个缓存位置
void rx_put_cache_move_to_next_ptr(SERIAL_OBJ *p_st_serial_rx,uint16_t rx_total_nbr)
{
	if (p_st_serial_rx->_p_ST_serial_cache == NULL)
		return;
	p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._B_flag 	= true;
	p_st_serial_rx->_U16_put_ptr++;
	if (p_st_serial_rx->_U16_put_ptr >= rx_total_nbr)
		p_st_serial_rx->_U16_put_ptr = 0;
}
//	2.接收缓存封包时“拿出指针”移动到下一个缓存位置
void rx_get_cache_move_to_next_ptr(SERIAL_OBJ *p_st_serial_rx,uint16_t rx_total_nbr)
{
	if (p_st_serial_rx->_p_ST_serial_cache == NULL)
		return;
	p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_get_ptr]._B_flag = false;
	p_st_serial_rx->_U16_get_ptr++;
	if (p_st_serial_rx->_U16_get_ptr >= rx_total_nbr)
		p_st_serial_rx->_U16_get_ptr = 0;
}
////（二）--------------------------串口发送缓存“出入”指针移动操作，用于封包处理-----------------------------------//
//	1.发送缓存封包时"放入指针"移动到下一个缓存位置
void tx_put_cache_move_to_next_ptr(SERIAL_OBJ *p_st_serial_tx,uint16_t tx_total_nbr)
{
	if (p_st_serial_tx->_p_ST_serial_cache == NULL)
		return;
	p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_put_ptr]._B_flag 	= true;
	p_st_serial_tx->_U16_put_ptr++;
	if (p_st_serial_tx->_U16_put_ptr >= tx_total_nbr)
		p_st_serial_tx->_U16_put_ptr = 0;
}
//	2.发送缓存封包时"拿出指针"移动到下一个缓存位置
void tx_get_cache_move_to_next_ptr(SERIAL_OBJ *p_st_serial_tx,uint16_t tx_total_nbr)
{
	if (p_st_serial_tx->_p_ST_serial_cache == NULL)
		return;
	p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_get_ptr]._B_flag 	= false;
	p_st_serial_tx->_U16_get_ptr++;
	if (p_st_serial_tx->_U16_get_ptr >= tx_total_nbr)
		p_st_serial_tx->_U16_get_ptr = 0;
}
////（三）--------------------------串口获取发送与接收的操作对象-----------------------------------//
//串口插入数据，移动指针到下一个位置
bool rx_insert_move_to_next_ptr(uint8_t serial_nbr)
{
	SERIAL_OBJ *p_st_serial_obj = NULL;
	p_st_serial_obj = get_serial_struct_obj_rx(serial_nbr);
	if (p_st_serial_obj == NULL)
		return false;

	uint16_t rx_q_nbr = get_serial_rx_queue_nbr(serial_nbr);

	rx_put_cache_move_to_next_ptr(p_st_serial_obj,rx_q_nbr);
	return true;
}
//二、串口数据-插入与获取-缓存队列操作
////（一）--------------------------串口接收缓存“出入”处理-----------------------------------//
//	1.串口-接收-数据-入-缓存队列
//参数：串口映射编号		输入缓存		输入长度		输入缓存对象		输入缓存个数		封包处理回调函数
void put_serial_char_to_queue_rx(uint8_t serial_nbr,
								const uint8_t *buffer,uint32_t length,
								SERIAL_OBJ *p_st_serial_rx,uint16_t rx_total_nbr,uint32_t tx_buffer_len,
								rx_stream_process_callback p_process_stream_func)
{

	if (p_st_serial_rx->_p_ST_serial_cache == NULL)
		return;
	if (p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._B_flag == false)
	{
	//	p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._p_U8_buffer = g_arry_serial_4_rx_buffer[p_st_serial_rx->_U16_put_ptr];
//		gcz_serial_v1_polling_printf(SERIAL_UART1,"5++[%d]%04X ",p_st_serial_rx->_U16_put_ptr,&p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]);
//		gcz_serial_v1_polling_printf(SERIAL_UART1,"%04X  ",p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._p_U8_buffer);
//		gcz_serial_v1_polling_printf(SERIAL_UART1,"%04X\r\n",g_arry_serial_4_rx_buffer[p_st_serial_rx->_U16_put_ptr]);
		if ((p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length + length) < tx_buffer_len)
		{
			if (p_process_stream_func != NULL)//如果有接口函数，则自行定义如何处理协议进行入包封包
			{
				bool res = false;
				res = p_process_stream_func(serial_nbr,
											buffer,
											length,
											p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._p_U8_buffer,
											&p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length);
				if (res)
				{
					rx_put_cache_move_to_next_ptr(p_st_serial_rx,rx_total_nbr);
				}
				else
				{
					//利用长度0作为超时封包
					if (p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length == 0)
					{
						rx_put_cache_move_to_next_ptr(p_st_serial_rx,rx_total_nbr);
					}
				}

			}
			else//系统默认入包封包模式
			{
				if (length == 1)
				{
					p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._p_U8_buffer[p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length] = buffer[0];
					p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length += 1;
				}
				else if (length > 1)
				{
					p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length += length;
					memcpy(&p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._p_U8_buffer[p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length],
						buffer,length);
				}
				//利用长度0作为超时封包
				if (p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length == 0)
				{
					rx_put_cache_move_to_next_ptr(p_st_serial_rx,rx_total_nbr);
				}
			}
		}
		else
		{
			//超长封包
			uint32_t recal_len = length - (p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length + length - tx_buffer_len);
			if (length == 1)
			{
				p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._p_U8_buffer[p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length] = buffer[0];
				p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length += 1;
			}
			else
			{
				memcpy(&p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._p_U8_buffer[p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length],
						buffer,recal_len);
				p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_put_ptr]._U32_length = tx_buffer_len;
			}
			rx_put_cache_move_to_next_ptr(p_st_serial_rx,rx_total_nbr);
		}
	}
}
//	2.串口-接收-数据-出-缓存队列
bool get_serial_data_from_queue_rx(uint8_t *buffer,uint32_t *length,SERIAL_OBJ *p_st_serial_rx,uint16_t rx_total_nbr)
{
	bool res = false;
	if (p_st_serial_rx->_p_ST_serial_cache == NULL)
		return res;
	if (p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_get_ptr]._B_flag == true)
	{
		*length 	= p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_get_ptr]._U32_length;
		memcpy(buffer,p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_get_ptr]._p_U8_buffer,*length);
		memset(p_st_serial_rx->_p_ST_serial_cache[p_st_serial_rx->_U16_get_ptr]._p_U8_buffer,0,*length);

		rx_get_cache_move_to_next_ptr(p_st_serial_rx,rx_total_nbr);
		res = true;
	}
	return res;
}
////（二）--------------------------串口发送缓存“出入”处理-----------------------------------//
//	1.串口-发送-数据-入-缓存队列
void put_serial_data_to_queue_tx(const uint8_t *buffer,uint32_t length,SERIAL_OBJ *p_st_serial_tx,uint16_t tx_total_nbr)
{
	if (p_st_serial_tx->_p_ST_serial_cache == NULL)
		return;
	if (p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_put_ptr]._B_flag == false)
	{
//		gcz_serial_v1_polling_printf(SERIAL_UART1,"2--[%d]%04X  ",p_st_serial_tx->_U16_put_ptr,p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_put_ptr]);
//		gcz_serial_v1_polling_printf(SERIAL_UART1,"%04X  %04X\r\n",p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_put_ptr]._p_U8_buffer,
//																	g_st_serial_1_cache_tx[p_st_serial_tx->_U16_put_ptr]);

		p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_put_ptr]._U32_length 	= length;
		memcpy(p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_put_ptr]._p_U8_buffer,buffer,length);
	//	gcz_serial_v1_polling_printf(SERIAL_UART1,"[%d]%s",p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_put_ptr]._U32_length,p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_put_ptr]._p_U8_buffer);


		tx_put_cache_move_to_next_ptr(p_st_serial_tx,tx_total_nbr);
	}
}
//	2.串口-发送-数据-出-缓存队列
bool get_serial_data_from_queue_tx(uint8_t *buffer,uint32_t *length,SERIAL_OBJ *p_st_serial_tx,uint16_t tx_total_nbr)
{
	bool res = false;
	if (p_st_serial_tx->_p_ST_serial_cache == NULL)
		return res;
	if (p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_get_ptr]._B_flag == true)
	{
		*length 	= p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_get_ptr]._U32_length;
		memcpy(buffer,p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_get_ptr]._p_U8_buffer,*length);
		memset(p_st_serial_tx->_p_ST_serial_cache[p_st_serial_tx->_U16_get_ptr]._p_U8_buffer,0,*length);
	//	gcz_serial_v1_polling_printf(SERIAL_UART1,"++**--[%d]%s",length,buffer);
		tx_get_cache_move_to_next_ptr(p_st_serial_tx,tx_total_nbr);
		res = true;
	}
	return res;
}
//三、串口数据-插入与获取-缓存队列操作
////（一）--------------------------串口接收数据-“出入”缓存处理-----------------------------------//
//	1.串口-接收-数据-插入
bool serial_rx_put_char_to_queue(uint8_t serial_nbr,const uint8_t *buffer,uint32_t length)
{
	SERIAL_OBJ *p_st_serial_obj = NULL;
	p_st_serial_obj = get_serial_struct_obj_rx(serial_nbr);
	if (p_st_serial_obj == NULL)
	{
		return false;
	}

	uint16_t rx_q_nbr = get_serial_rx_queue_nbr(serial_nbr);
	uint32_t rx_length = get_serial_rx_buffer_length(serial_nbr);

	put_serial_char_to_queue_rx(serial_nbr,buffer,length,p_st_serial_obj,rx_q_nbr,rx_length,g_st_serial_opt.process_rx_stream_callback[serial_nbr]);
	return true;
}
//	2.串口-接收-数据-获取
bool serial_rx_get_data_from_queue(uint8_t serial_nbr,uint8_t *buffer,uint32_t *length)
{
	SERIAL_OBJ *p_st_serial_obj = NULL;
	p_st_serial_obj = get_serial_struct_obj_rx(serial_nbr);
	if (p_st_serial_obj == NULL)
		return false;
	uint16_t rx_q_nbr = get_serial_rx_queue_nbr(serial_nbr);
	return get_serial_data_from_queue_rx(buffer,length,p_st_serial_obj,rx_q_nbr);
}
////（二）--------------------------串口发送数据-“出入”缓存处理-----------------------------------//
//	1.串口-发送-数据-插入
bool serial_tx_put_data_to_queue(uint8_t serial_nbr,const uint8_t *buffer,uint32_t length)
{
	SERIAL_OBJ *p_st_serial_obj = NULL;
	p_st_serial_obj = get_serial_struct_obj_tx(serial_nbr);
	if (p_st_serial_obj == NULL)
		return false;
	uint16_t tx_q_nbr = get_serial_tx_queue_nbr(serial_nbr);
//	gcz_serial_v1_polling_printf(SERIAL_UART1,"**--[%d]%s",length,buffer);
	put_serial_data_to_queue_tx(buffer,length,p_st_serial_obj,tx_q_nbr);
	return true;
}
//	1.串口-发送-数据-获取
bool serial_tx_get_data_from_queue(uint8_t serial_nbr,uint8_t *buffer,uint32_t *length)
{
	SERIAL_OBJ *p_st_serial_obj = NULL;;
	p_st_serial_obj = get_serial_struct_obj_tx(serial_nbr);
	if (p_st_serial_obj == NULL)
		return false;
	uint16_t tx_q_nbr = get_serial_tx_queue_nbr(serial_nbr);
	return get_serial_data_from_queue_tx(buffer,length,p_st_serial_obj,tx_q_nbr);
}

bool serial_tx_use_hardware_write(uint8_t serial_nbr,uint8_t *buffer,uint32_t *length)
{
	bool res = false;
	if (serial_tx_get_data_from_queue(serial_nbr,buffer,length))
	{
	//	gcz_serial_v1_polling_printf(SERIAL_UART1,"11++**--[%d]%s",*length,buffer);
		g_st_serial_opt.serial_hardware_transmit_callback[serial_nbr](serial_nbr,buffer,*length);
		res = true;
	}
	return res;
}

//四、串口数据-中断-接口函数
//1.串口中断接收API函数
void serial_rx_isr(uint8_t serial_nbr,uint8_t data)
{
	serial_rx_put_char_to_queue(serial_nbr,&data,1);
}
//2.串口接收超时中断API函数
void serial_rx_timeout_isr(uint8_t serial_nbr)
{
	serial_rx_put_char_to_queue(serial_nbr,NULL,0);
}
SERIAL_OPT g_st_serial_opt =
{
	.serial_init_func 			= init_serial,
//	.serial_rx_to_cahce			= serial_rx_put_char_to_queue,
	.serial_rx_read_from_cahce	= serial_rx_get_data_from_queue,
	.serial_tx_write_to_cahce	= serial_tx_put_data_to_queue,
	.serial_tx_polling_auto_write	= serial_tx_use_hardware_write,
	.serial_rx_handler			= serial_rx_isr,
	.serial_rx_timeout_handler	= serial_rx_timeout_isr,
	.serial_rx_put_move_ptr_next = rx_insert_move_to_next_ptr,
};
