#ifndef __SERIAL_V1_H
#define __SERIAL_V1_H
#include <stdbool.h>
#include <stdint.h>
#include "board_config.h"

/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

//配置使用UART1-蓝牙串口
#define USE_SERIAL_RX_UART4
//#define USE_SERIAL_TX_UART4
//#define USE_SERIAL_TX_UART1


//接收缓存配置
#ifdef USE_SERIAL_RX_UART0
#define	SERIAL_0_RX_BUFFER_NBR			5
#define SERIAL_0_RCV_LENGTH				600
#endif
#ifdef USE_SERIAL_RX_UART1
#define	SERIAL_1_RX_BUFFER_NBR			3
#define SERIAL_1_RCV_LENGTH				150
#endif
#ifdef USE_SERIAL_RX_UART4
#define	SERIAL_4_RX_BUFFER_NBR			50
#define SERIAL_4_RCV_LENGTH				64
#endif
//发送缓存配置
#ifdef USE_SERIAL_TX_UART0
#define	SERIAL_0_TX_BUFFER_NBR			3
#define SERIAL_0_SND_LENGTH				600
#endif
#ifdef USE_SERIAL_TX_UART1
#define	SERIAL_1_TX_BUFFER_NBR			3
#define SERIAL_1_SND_LENGTH				100
#endif
#ifdef USE_SERIAL_TX_UART4
#define	SERIAL_4_TX_BUFFER_NBR			3
#define SERIAL_4_SND_LENGTH				100
#endif

//配置最大的接收缓存数量，系统需要使用
#define MAX_SERIAL_RX_BUFFER_NBR		SERIAL_4_RX_BUFFER_NBR
//配置最大的发送缓存数量，系统需要使用
#define MAX_SERIAL_TX_BUFFER_NBR		1//SERIAL_1_TX_BUFFER_NBR
//串口使用配置
//可独立选择使用UARTx的接收或发送缓存，进行初始化，未define的不进行初始化
//可选配置
/*
#define USE_SERIAL_RX_UART0~7		0~7
#define USE_SERIAL_TX_UART0~7		0~7
 */


//串口接收后封包解析的CALLBACK函数定义
typedef bool (*rx_stream_process_callback)(uint8_t,const uint8_t *,uint32_t,uint8_t *,uint32_t *);
typedef void (*uart_transmit)(uint8_t,uint8_t *,uint32_t);
//串口操作函数定义
typedef struct _serial_opt
{
//---------------初始化调用-------------------------------------
	void (*serial_init_func)();												//串口初始化
//---------------应用层调用-------------------------------------
	bool (*serial_rx_read_from_cahce)(uint8_t,uint8_t *,uint32_t *);		//串口接收数据流拿出缓存
	bool (*serial_tx_write_to_cahce)(uint8_t ,const uint8_t *,uint32_t);	//串口发送数据放入缓存
//---------------系统层轮询调用---------------------------------
	bool (*serial_tx_polling_auto_write)(uint8_t ,uint8_t *,uint32_t *);	//串口轮询式自动发送写入的缓存数据
//---------------中断调用--------------------------------------
	void (*serial_rx_handler)(uint8_t,uint8_t);								//串口接收数据中断处理函数
	void (*serial_rx_timeout_handler)(uint8_t);								//串口接收超时中断处理函数
//---------------协议处理接口及调用-----------------------------
	bool (*serial_rx_put_move_ptr_next)(uint8_t);							//串口封包移动指针位置
	rx_stream_process_callback process_rx_stream_callback[MAX_NBR];			//串口数据协议处理及封包回调函数
//---------------发送函数的驱动接口-----------------------------
	uart_transmit	serial_hardware_transmit_callback[MAX_NBR];				//串口硬件发送回调函数
}SERIAL_OPT;

//获取串口操作对象
extern SERIAL_OPT g_st_serial_opt;
#endif
