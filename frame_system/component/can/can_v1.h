#ifndef __CAN_V1_H
#define __CAN_V1_H
#include <stdbool.h>
#include <stdint.h>
#include "board_config.h"

//配置最大的接收缓存数量，系统需要使用
#define MAX_CAN_RX_BUFFER_NBR		5
//配置最大的发送缓存数量，系统需要使用
#define MAX_CAN_TX_BUFFER_NBR		5

#endif
