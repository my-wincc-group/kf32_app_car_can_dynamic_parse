#include "can_v1.h"
#include "string.h"
typedef struct __can_cache
{
	uint32_t 	_U32_can_id;			//CAN帧ID
	uint8_t		*_p_U32_can_buffer;		//CAM数据接收数组指针
	uint16_t 	_U16_length;			//数据长度
	int64_t 	_I64_meas_sys_clk;		//测量时系统时间
	bool 		_B_flag;				//封包标志位
}CAN_CACHE;

typedef struct __can_obj
{
	CAN_CACHE *_p_ST_can_cache;		//缓存队列结构指针
	uint16_t _U16_put_ptr;				//放入指针
	uint16_t _U16_get_ptr;				//获取指针
}CAN_OBJ;


//	1.获取接收CAN数据结构对象
CAN_OBJ * get_can_struct_obj_rx(uint8_t can_nbr)
{
	CAN_OBJ *p_st_can_obj;
	switch(can_nbr)
	{
#ifdef USE_CAN_RX_CAN0
	case 0:p_st_can_obj = &g_st_can0_obj_rx;break;
#endif
#ifdef USE_CAN_RX_CAN1
	case 1:p_st_can_obj = &g_st_can1_obj_rx;break;
#endif
#ifdef USE_CAN_RX_CAN2
	case 2:p_st_can_obj = &g_st_can2_obj_rx;break;
#endif
#ifdef USE_CAN_RX_CAN3
	case 3:p_st_can_obj = &g_st_can3_obj_rx;break;
#endif
#ifdef USE_CAN_RX_CAN4
	case 4:p_st_can_obj = &g_st_can4_obj_rx;break;
#endif
#ifdef USE_CAN_RX_CAN5
	case 5:p_st_can_obj = &g_st_can5_obj_rx;break;
#endif
#ifdef USE_CAN_RX_CAN6
	case 6:p_st_can_obj = &g_st_can6_obj_rx;break;
#endif
#ifdef USE_CAN_RX_CAN7
	case 7:p_st_can_obj = &g_st_can7_obj_rx;break;
#endif
		default:p_st_can_obj = NULL;break;
	}
	return p_st_can_obj;
}
//获取发送CAN数据结构对象
CAN_OBJ * get_can_struct_obj_tx(uint8_t can_nbr)
{
	CAN_OBJ *p_st_can_obj;
	switch(can_nbr)
	{
#ifdef USE_CAN_TX_CAN0
	case 0:p_st_can_obj = &g_st_can0_obj_tx;break;
#endif
#ifdef USE_CAN_TX_CAN1
	case 1:p_st_can_obj = &g_st_can1_obj_tx;break;
#endif
#ifdef USE_CAN_TX_CAN2
	case 2:p_st_can_obj = &g_st_can2_obj_tx;break;
#endif
#ifdef USE_CAN_TX_CAN3
	case 3:p_st_can_obj = &g_st_can3_obj_tx;break;
#endif
#ifdef USE_CAN_TX_CAN4
	case 4:p_st_can_obj = &g_st_can4_obj_tx;break;
#endif
#ifdef USE_CAN_TX_CAN5
	case 5:p_st_can_obj = &g_st_can5_obj_tx;break;
#endif
#ifdef USE_CAN_TX_CAN6
	case 6:p_st_can_obj = &g_st_can6_obj_tx;break;
#endif
#ifdef USE_CAN_RTX_CAN7
	case 7:p_st_can_obj = &g_st_can7_obj_tx;break;
#endif
		default:p_st_can_obj = NULL;break;
	}
	return p_st_can_obj;
}
//2.获取CAN接收缓存数量
uint16_t get_can_rx_queue_nbr(uint8_t can_nbr)
{
	uint16_t rx_q_nbr;
	switch(can_nbr)
	{
#ifdef USE_CAN_RX_CAN0
	case 0:rx_q_nbr = CAN_0_RX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_RX_CAN1
	case 1:rx_q_nbr = CAN_1_RX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_RX_CAN2
	case 2:rx_q_nbr = CAN_2_RX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_RX_CAN3
	case 3:rx_q_nbr = CAN_3_RX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_RX_CAN4
	case 4:rx_q_nbr = CAN_4_RX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_RX_CAN5
	case 5:rx_q_nbr = CAN_5_RX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_RX_CAN6
	case 6:rx_q_nbr = CAN_6_RX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_RX_CAN7
	case 7:rx_q_nbr = CAN_7_RX_BUFFER_NBR;break;
#endif
	default:rx_q_nbr = 0;break;
	}
	return rx_q_nbr;
}
//获取CAN发送缓存数量
uint16_t get_can_tx_queue_nbr(uint8_t can_nbr)
{
	uint16_t tx_q_nbr;
	switch(can_nbr)
	{
#ifdef USE_CAN_TX_CAN0
	case 0:tx_q_nbr = CAN_0_TX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_TX_CAN1
	case 1:tx_q_nbr = CAN_1_TX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_TX_CAN2
	case 2:tx_q_nbr = CAN_2_TX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_TX_CAN3
	case 3:tx_q_nbr = CAN_3_TX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_TX_CAN4
	case 4:tx_q_nbr = CAN_4_TX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_TX_CAN5
	case 5:tx_q_nbr = CAN_5_TX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_TX_CAN6
	case 6:tx_q_nbr = CAN_6_TX_BUFFER_NBR;break;
#endif
#ifdef USE_CAN_TX_CAN7
	case 7:tx_q_nbr = CAN_7_TX_BUFFER_NBR;break;
#endif
	default:tx_q_nbr = 0;break;
	}
	return tx_q_nbr;
}
//3.获取CAN接收缓存数量
uint16_t get_can_rx_buffer_length(uint8_t can_nbr)
{
	uint16_t rx_length;
	switch(can_nbr)
	{
#ifdef USE_CAN_RX_CAN0
	case 0:rx_length = CAN_0_RCV_LENGTH;break;
#endif
#ifdef USE_CAN_RX_CAN1
	case 1:rx_length = CAN_1_RCV_LENGTH;break;
#endif
#ifdef USE_CAN_RX_CAN2
	case 2:rx_length = CAN_2_RCV_LENGTH;break;
#endif
#ifdef USE_CAN_RX_CAN3
	case 3:rx_length = CAN_3_RCV_LENGTH;break;
#endif
#ifdef USE_CAN_RX_CAN4
	case 4:rx_length = CAN_4_RCV_LENGTH;break;
#endif
#ifdef USE_CAN_RX_CAN5
	case 5:rx_length = CAN_5_RCV_LENGTH;break;
#endif
#ifdef USE_CAN_RX_CAN6
	case 6:rx_length = SCAN_6_RCV_LENGTH;break;
#endif
#ifdef USE_CAN_RX_CAN7
	case 7:rx_length = CAN_7_RCV_LENGTH;break;
#endif
	default:rx_length = 0;break;
	}
	return rx_length;
}
uint16_t get_can_tx_buffer_length(uint8_t can_nbr)
{
	uint16_t tx_length;
	switch(can_nbr)
	{
#ifdef USE_CAN_TX_CAN0
	case 0:tx_length = CAN_0_SND_LENGTH;break;
#endif
#ifdef USE_CAN_TX_CAN1
	case 1:tx_length = CAN_1_SND_LENGTH;break;
#endif
#ifdef USE_CAN_TX_CAN2
	case 2:tx_length = CAN_2_SND_LENGTH;break;
#endif
#ifdef USE_CAN_TX_CAN3
	case 3:tx_length = CAN_3_SND_LENGTH;break;
#endif
#ifdef USE_CAN_TX_CAN4
	case 4:tx_length = CAN_4_SND_LENGTH;break;
#endif
#ifdef USE_CAN_TX_CAN5
	case 5:tx_length = CAN_5_SND_LENGTH;break;
#endif
#ifdef USE_CAN_TX_CAN6
	case 6:tx_length = SCAN_6_SND_LENGTH;break;
#endif
#ifdef USE_CAN_TX_CAN7
	case 7:tx_length = CAN_7_SND_LENGTH;break;
#endif
	default:tx_length = 0;break;
	}
	return tx_length;
}
//4.初始化一个接收CAN
void init_can_single_obj(CAN_OBJ *p_st_can_rx,CAN_CACHE *p_st_can_cache,uint16_t total_nbr,
								uint8_t *buffer[],uint32_t buferlen)
{
	p_st_can_rx->_U16_get_ptr = 0;
	p_st_can_rx->_U16_put_ptr = 0;
	p_st_can_rx->_p_ST_can_cache = p_st_can_cache;
	if (p_st_can_rx->_p_ST_can_cache == NULL)
		return;
	for (uint8_t i1 = 0;i1 < total_nbr;i1++)
	{
		p_st_can_rx->_p_ST_can_cache[i1]._B_flag		= false;
		p_st_can_rx->_p_ST_can_cache[i1]._U16_length 	= 0;
		p_st_can_rx->_p_ST_can_cache[i1]._p_U32_can_buffer =	buffer[i1];
		memset(p_st_can_rx->_p_ST_can_cache[i1]._p_U32_can_buffer,0,buferlen);
	}
}
//5.初始化CAN接收对象
void init_can_obj_rx(uint8_t can_nbr)
{
	CAN_OBJ *p_st_can_rx = NULL;
	p_st_can_rx = get_can_struct_obj_rx(can_nbr);
	if (p_st_can_rx == NULL)
		return;

	p_st_can_rx->_U16_get_ptr = 0;
	p_st_can_rx->_U16_put_ptr = 0;
	CAN_CACHE *p_st_can_cache_rx;
	uint32_t length = get_can_rx_buffer_length(can_nbr);
	uint16_t rx_q_nbr = get_can_rx_queue_nbr(can_nbr),j1;
	uint8_t *buffer[MAX_CAN_RX_BUFFER_NBR];

	switch(can_nbr)
	{
#ifdef USE_CAN_RX_CAN0
	case 0:p_st_can_obj = &g_st_can0_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_0_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN1
	case 1:p_st_can_obj = &g_st_can1_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_1_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN2
	case 2:p_st_can_obj = &g_st_can2_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_2_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN3
	case 3:p_st_can_obj = &g_st_can3_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_3_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN4
	case 4:p_st_serial_cache_rx = g_st_serial_4_cache_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_can_4_rx_buffer[j1];
			}
			break;
#endif
#ifdef USE_CAN_RX_CAN5
	case 5:p_st_can_obj = &g_st_can5_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_5_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN6
	case 6:p_st_can_obj = &g_st_can6_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_6_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN7
	case 7:p_st_can_obj = &g_st_can7_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_7_rx_buffer[j1];
			break;
#endif
		default:p_st_can_cache_rx = NULL;break;
	}
	init_can_single_obj(p_st_can_rx,p_st_can_cache_rx,rx_q_nbr,
								buffer,length);
}
//初始化CAN发送对象
void init_can_obj_tx(uint8_t can_nbr)
{
	CAN_OBJ *p_st_can_rx = NULL;
	p_st_can_rx = get_can_struct_obj_rx(can_nbr);
	if (p_st_can_rx == NULL)
		return;

	p_st_can_rx->_U16_get_ptr = 0;
	p_st_can_rx->_U16_put_ptr = 0;
	CAN_CACHE *p_st_can_cache_rx;
	uint32_t length = get_can_rx_buffer_length(can_nbr);
	uint16_t rx_q_nbr = get_can_rx_queue_nbr(can_nbr),j1;
	uint8_t *buffer[MAX_CAN_RX_BUFFER_NBR];

	switch(can_nbr)
	{
#ifdef USE_CAN_RX_CAN0
	case 0:p_st_can_obj = &g_st_can0_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_0_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN1
	case 1:p_st_can_obj = &g_st_can1_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_1_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN2
	case 2:p_st_can_obj = &g_st_can2_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_2_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN3
	case 3:p_st_can_obj = &g_st_can3_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_3_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN4
	case 4:p_st_serial_cache_rx = g_st_serial_4_cache_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
			{
				buffer[j1] = g_arry_can_4_rx_buffer[j1];
			}
			break;
#endif
#ifdef USE_CAN_RX_CAN5
	case 5:p_st_can_obj = &g_st_can5_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_5_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN6
	case 6:p_st_can_obj = &g_st_can6_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_6_rx_buffer[j1];
			break;
#endif
#ifdef USE_CAN_RX_CAN7
	case 7:p_st_can_obj = &g_st_can7_obj_rx;
			for (j1 = 0;j1 < rx_q_nbr;j1++)
				buffer[j1] = g_arry_can_7_rx_buffer[j1];
			break;
#endif
		default:p_st_can_cache_rx = NULL;break;
	}
	init_can_single_obj(p_st_can_rx,p_st_can_cache_rx,rx_q_nbr,
								buffer,length);
}
