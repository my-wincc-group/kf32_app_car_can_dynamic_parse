#ifndef __ALG_LIB_H
#define __ALG_LIB_H
#include <stdbool.h>
#include <stdint.h>
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */
#define FLOAT_INVALID				9999.0			//浮点数无效值定义
typedef struct _kalman_1st {
    float X_last; //上一时刻的最优结果
    float X_mid;  //当前时刻的预测结果
    float X_now;  //当前时刻的最优结果
    float P_mid;  //当前时刻预测结果的协方差
    float P_now;  //当前时刻最优结果的协方差
    float P_last; //上一时刻最优结果的协方差
    float kg;     //kalman增益
    float A;      //系统参数
    float Q;
    float R;
    float H;
}KLM_CAL_1ST_ORDER;

typedef struct _kalman_2nd {
    float X_last[2][1]; //上一时刻的最优结果
    float X_mid[2][1];  //当前时刻的预测结果
    float X_now[2][1];  //当前时刻的最优结果
    float P_mid[2][2];  //当前时刻预测结果的协方差
    float P_now[2][2];  //当前时刻最优结果的协方差
    float P_last[2][2]; //上一时刻最优结果的协方差
    float kg[2][2];     //kalman增益
    float A[2][2];      //系统参数
    float Q[2][2];
    float R[2][2];
    float H[2][2];
}KLM_CAL_2ND_ORDER;
//卡尔曼计算结果
typedef struct __klm_filter_result
{
	 float X_revisionary[2][1];				//修正的状态
	 float X_predicted[2][1];				//预测的状态
}KLM_FILTER_RES;
//卡尔曼一阶递归滤波器创建
void kalman_1st_order_create(KLM_CAL_1ST_ORDER *p,float T_Q,float T_R);
//卡尔曼一阶递归滤波
float kalman_1st_order_filter(KLM_CAL_1ST_ORDER* p,float dat);
#if 0
//二阶矩阵转置
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_transposition(float mat[2][2],float mat_transp[2][2]);
//二阶矩阵的逆矩阵
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_inversion(float mat[2][2],float mat_invers[2][2]);
//二阶矩阵的乘法2x2
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x2_multiplication(float mat1[2][2],float mat2[2][2],float mat_multip[2][2]);
//二阶矩阵的乘法2x2 * 2x1
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x2_times_2x1_multiplication(float mat1[2][2],float mat2[2][1],float mat_multip[2][1]);
//二阶矩阵的加法2x2 + 2x2
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x2_addition(float mat1[2][2],float mat2[2][2],float mat_add[2][2]);
//二阶矩阵的加法2x1 + 2x1
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x1_addition(float mat1[2][1],float mat2[2][1],float mat_add[2][1]);
//二阶矩阵的减法2x2 - 2x2
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x2_subtraction(float mat1[2][2],float mat2[2][2],float mat_sub[2][2]);
//二阶矩阵的减法 2x2 - 2x1
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x1_subtraction(float mat1[2][1],float mat2[2][1],float mat_sub[2][1]);
#endif
//设置二阶卡卡尔曼参数A
void set_2nd_order_klm_paramA(KLM_CAL_2ND_ORDER *p,float T_A[2][2]);
//设置二阶卡卡尔曼参数P
void set_2nd_order_klm_paramP(KLM_CAL_2ND_ORDER *p,float T_P[2][2]);
//设置二阶卡卡尔曼参数A与P
void set_2nd_order_klm_param(KLM_CAL_2ND_ORDER *p,float T_A[2][2],float T_P[2][2]);
//二阶卡尔曼二次封装最终调用接口API
KLM_FILTER_RES excute_klm_2nd_order_filter(KLM_CAL_2ND_ORDER *p,float var1,float var2,float sys_A[2][2]);
//卡尔曼二阶滤波器创建
void kalman_2nd_order_create(KLM_CAL_2ND_ORDER *p,float X_last[2][1],float P_last[2][2],float T_Q[2][2],float T_R[2][2],float T_A[2][2],float T_H[2][2]);
//卡尔曼二阶滤波器
void kalman_2nd_order_filter(KLM_CAL_2ND_ORDER* p,float dat[2][1]);

//均值滤波
//参数：数值缓存		缓存个数
float avg_filter(float fval[],uint8_t nbr);
//冒泡法进行均值滤波
//参数：数值缓存		缓存个数		系数：滤波需要滤掉前后的百分之几
float bubble_sort_avg_filter(float fval[],uint8_t nbr,float filter_coe);
#endif
