#include "alg_lib.h"
#include "tool.h"
#include "board_config.h"
#include "math.h"
#include "usart.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */
//卡尔曼递归过滤器

//STEP1：计算卡尔曼增益
//Kk= eESTk-1/(eESTk-1 + eMEAS)
//STEP2：计算估算值
//公式：X^k = X^k-1 + Kk(Zk - X^k-1)
//X^k:当前估计值
//X^k-1:上一次估计值
//Zk:当前测量值
//STEP3：更新估计误差
//eESTk = (1-Kk)eESTk-1

/**
  * @name   kalmanCreate
  * @brief  创建一个卡尔曼滤波器
  * @param  p:  滤波器
  *         T_Q:系统噪声协方差
  *         T_R:测量噪声协方差
  *
  * @retval none
  */
void kalman_1st_order_create(KLM_CAL_1ST_ORDER *p,float T_Q,float T_R)
{
    p->X_last = (float)0;
    p->P_last = 0;
    p->Q = T_Q;
    p->R = T_R;
    p->A = 1;
    p->H = 1;
    p->X_mid = p->X_last;
}
/**
  * @name   KalmanFilter
  * @brief  卡尔曼一阶滤波器
  * @param  p:  滤波器
  *         dat:待滤波数据
  * @retval 滤波后的数据
  */
float kalman_1st_order_filter(KLM_CAL_1ST_ORDER* p,float dat)
{
    p->X_mid =p->A*p->X_last;                     //x(k|k-1) = AX(k-1|k-1)+BU(k)
    p->P_mid = p->A*p->P_last+p->Q;               //p(k|k-1) = Ap(k-1|k-1)A'+Q
    p->kg = p->P_mid/(p->P_mid+p->R);             //kg(k) = p(k|k-1)H'/(Hp(k|k-1)'+R)
    p->X_now = p->X_mid+p->kg*(dat-p->X_mid);     //x(k|k) = X(k|k-1)+kg(k)(Z(k)-HX(k|k-1))
    p->P_now = (1-p->kg)*p->P_mid;                //p(k|k) = (I-kg(k)H)P(k|k-1)
    p->P_last = p->P_now;                         //状态更新
    p->X_last = p->X_now;
    return p->X_now;
}
//均值滤波
//参数：数值缓存		缓存个数
float avg_filter(float fval[],uint8_t nbr)
{
	uint8_t i1,j1;
	float ftemp = 0;

	float fval_cal[nbr];
	for(i1 = 0;i1 < nbr;i1++)
		fval_cal[i1] = fval[i1];

	uint16_t cnt = 0;
	ftemp = 0;
	for(i1 = 0;i1 < nbr;i1++)
	{
		if (fval_cal[i1] != FLOAT_INVALID)
		{
			ftemp += fval_cal[i1];
			cnt++;
		}
	}
	if (cnt != 0)
		ftemp /= cnt;
	else
		ftemp = FLOAT_INVALID;
	return ftemp;
}
//冒泡法进行均值滤波
//参数：数值缓存		缓存个数		系数：滤波需要滤掉前后的百分之几
float bubble_sort_avg_filter(float fval[],uint8_t nbr,float filter_coe)
{
	uint8_t i1,j1;
	float ftemp = 0;

	float fval_cal[nbr];
	for(i1 = 0;i1 < nbr;i1++)
		fval_cal[i1] = fval[i1];

	for(i1 = 0;i1 < nbr;i1++)
	{
		for(j1 = 0;j1 < nbr - i1;j1++)
		{
			if ((fval_cal[j1] > fval_cal[j1+1]) && ((j1 + 1) < (nbr - i1)))
			{
				ftemp = fval_cal[j1];
				fval_cal[j1] = fval_cal[j1 + 1];
				fval_cal[j1 + 1] = ftemp;
			}

		}
	}

	uint16_t filter_limt_nbr = nbr * filter_coe + 0.5;//四射五入
	uint16_t cnt = 0;
	ftemp = 0;
	for(i1 = filter_limt_nbr;i1 < nbr - filter_limt_nbr;i1++)
	{
		if (fval_cal[i1] != FLOAT_INVALID)
		{
			ftemp += fval_cal[i1];
			cnt++;
		}
	}
	if (cnt != 0)
		ftemp /= cnt;
	else
		ftemp = FLOAT_INVALID;
	return ftemp;
}
//二阶矩阵转置
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_transposition(float mat[2][2],float mat_transp[2][2])

{
	uint8_t m,n;
	for (m = 0;m < 2;m++)
	{
		for (n = 0;n < 2;n++)
		{
			mat_transp[n][m] = mat[m][n];
		}
	}
}
//二阶矩阵的逆矩阵
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_inversion(float mat[2][2],float mat_invers[2][2])
{
	uint8_t m,n;
	//二维矩阵的逆矩阵求法是，A*/|A|;
	//A*，左上到右下对角线元素位置互换，左下到忧伤对角线元素符号对调
	//|A|=a*d-c*d	[a b;c d]
	float A_start[2][2];
	float det;
	A_start[0][0] = mat[1][1];
	A_start[1][1] = mat[0][0];

	A_start[0][1] = -mat[0][1];//(mat[1][0] < 0) ? -fabs(mat[0][1]):fabs(mat[0][1]);
	A_start[1][0] = -mat[1][0];//(mat[0][1] < 0) ? -fabs(mat[1][0]):fabs(mat[1][0]);

	det = mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];

	mat_invers[0][0] = A_start[0][0] / det;
	mat_invers[0][1] = A_start[0][1] / det;
	mat_invers[1][0] = A_start[1][0] / det;
	mat_invers[1][1] = A_start[1][1] / det;
}
//二阶矩阵的乘法2x2
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x2_multiplication(float mat1[2][2],float mat2[2][2],float mat_multip[2][2])
{
	mat_multip[0][0] = mat1[0][0]*mat2[0][0]+ mat1[0][1]* mat2[1][0];
	mat_multip[0][1] = mat1[0][0]*mat2[0][1]+ mat1[0][1]* mat2[1][1];
	mat_multip[1][0] = mat1[1][0]*mat2[0][0]+ mat1[1][1]* mat2[1][0];
	mat_multip[1][1] = mat1[1][0]*mat2[0][1]+ mat1[1][1]* mat2[1][1];
}
//二阶矩阵的乘法2x2 * 2x1
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x2_times_2x1_multiplication(float mat1[2][2],float mat2[2][1],float mat_multip[2][1])
{
	mat_multip[0][0] = mat1[0][0]*mat2[0][0]+ mat1[0][1]* mat2[1][0];
	mat_multip[1][0] = mat1[1][0]*mat2[0][0]+ mat1[1][1]* mat2[1][0];
}
//二阶矩阵的加法2x2 + 2x2
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x2_addition(float mat1[2][2],float mat2[2][2],float mat_add[2][2])
{
	mat_add[0][0] = mat1[0][0]+mat2[0][0];
	mat_add[0][1] = mat1[0][1]+mat2[0][1];
	mat_add[1][0] = mat1[1][0]+mat2[1][0];
	mat_add[1][1] = mat1[1][1]+mat2[1][1];
}
//二阶矩阵的加法2x1 + 2x1
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x1_addition(float mat1[2][1],float mat2[2][1],float mat_add[2][1])
{
	mat_add[0][0] = mat1[0][0]+mat2[0][0];
	mat_add[1][0] = mat1[1][0]+mat2[1][0];
}
//二阶矩阵的减法 2x2 - 2x2
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x2_subtraction(float mat1[2][2],float mat2[2][2],float mat_sub[2][2])
{
	mat_sub[0][0] = mat1[0][0]-mat2[0][0];
	mat_sub[0][1] = mat1[0][1]-mat2[0][1];
	mat_sub[1][0] = mat1[1][0]-mat2[1][0];
	mat_sub[1][1] = mat1[1][1]-mat2[1][1];
}
//二阶矩阵的减法 2x2 - 2x1
//参数：输入2阶矩阵数组      计算结果输出
void second_matrix_2x1_subtraction(float mat1[2][1],float mat2[2][1],float mat_sub[2][1])
{
	mat_sub[0][0] = mat1[0][0]-mat2[0][0];
	mat_sub[1][0] = mat1[1][0]-mat2[1][0];
}
//2x2 二阶矩阵指针数组赋值
void mat_2x2_opt_point_assigment(float *p[2],float result[2][2])
{
	result[0][0] = p[0][0];
	result[0][1] = p[0][1];
	result[1][0] = p[1][0];
	result[1][1] = p[1][1];
}
//2x2 二阶矩阵数组赋值
void mat_2x2_opt_arry_assigment(float arry[2][2],float result[2][2])
{
	result[0][0] = arry[0][0];
	result[0][1] = arry[0][1];
	result[1][0] = arry[1][0];
	result[1][1] = arry[1][1];
}
/**
  * @name   kalmanCreate
  * @brief  创建一个二阶卡尔曼滤波器
  * @param  p:  滤波器
  *         T_Q:系统噪声协方差
  *         T_R:测量噪声协方差
  *
  * @retval none
  */
void kalman_2nd_order_create(KLM_CAL_2ND_ORDER *p,float X_last[2][1],float P_last[2][2],float T_Q[2][2],float T_R[2][2],float T_A[2][2],float T_H[2][2])
{
    p->X_last[0][0] = X_last[0][0];
    p->X_last[1][0] = X_last[1][0];
    p->P_last[0][0] = P_last[0][0];
    p->P_last[0][1] = P_last[0][1];
    p->P_last[1][0] = P_last[1][0];
    p->P_last[1][1] = P_last[1][1];

    mat_2x2_opt_arry_assigment(p->P_last,P_last);
    mat_2x2_opt_arry_assigment(p->Q,T_Q);
    mat_2x2_opt_arry_assigment(p->R,T_R);
    mat_2x2_opt_arry_assigment(p->A,T_A);
    mat_2x2_opt_arry_assigment(p->H,T_H);

    p->X_mid[0][0] = p->X_last[0][0];
    p->X_mid[1][0] = p->X_last[1][0];
}

/**
  * @name   KalmanFilter
  * @brief  卡尔曼二阶滤波器
  * @param  p:  滤波器
  *         dat:待滤波数据
  * @retval 滤波后的数据
  */
void kalman_2nd_order_filter(KLM_CAL_2ND_ORDER* p,float dat[2][1])
{
//    p->X_mid =p->A*p->X_last;                     //x(k|k-1) = AX(k-1|k-1)+BU(k)
//    p->P_mid = p->A*p->P_last+p->Q;               //p(k|k-1) = Ap(k-1|k-1)A'+Q
//    p->kg = p->P_mid/(p->P_mid+p->R);             //kg(k) = p(k|k-1)H'/(Hp(k|k-1)'+R)
//    p->X_now = p->X_mid+p->kg*(dat-p->X_mid);     //x(k|k) = X(k|k-1)+kg(k)(Z(k)-HX(k|k-1))
//    p->P_now = (1-p->kg)*p->P_mid;                //p(k|k) = (I-kg(k)H)P(k|k-1)
//    p->P_last = p->P_now;                         //状态更新
//    p->X_last = p->X_now;
//    return p->X_now;

	//1.状态预测		X^ = AX(k-1)   p->X_mid =p->A*p->X_last;
	second_matrix_2x2_times_2x1_multiplication(p->A,p->X_last,p->X_mid);

//  USART1_Bebug_Print_Flt("[p->X_mid[0][0]]", p->X_mid[0][0], 1, 1);
//  USART1_Bebug_Print_Flt("[p->X_mid[1][0]]", p->X_mid[1][0], 1, 1);

	//2.协方差预测  	P^(k)=AP(k-1)A'+Q	·    p->P_mid = p->A*p->P_last*p->A'+p->Q;
	float P_mid_temp[2][2],P_mid_temp_final[2][2],A_trans[2][2];

	second_matrix_2x2_multiplication(p->A,p->P_last,P_mid_temp);
	second_matrix_transposition(p->A,A_trans);//2022-09-07算法修复   新增A转置的乘入
	second_matrix_2x2_multiplication(P_mid_temp,A_trans,P_mid_temp_final);
	second_matrix_2x2_addition(P_mid_temp_final,p->Q,p->P_mid);

	//3.计算卡尔曼增益	K(k)=P(k)H'/(HP(k)H'+R)   p->kg = p->P_mid/(p->P_mid+p->R);  这里由于二阶矩阵,H取单位矩阵，忽略
	float P_mid_R_temp[2][2],P_mid_R_temp_inverse[2][2];
	second_matrix_2x2_addition(p->P_mid,p->R,P_mid_R_temp);
	second_matrix_inversion(P_mid_R_temp,P_mid_R_temp_inverse);
	second_matrix_2x2_multiplication(p->P_mid,P_mid_R_temp_inverse,p->kg);

	//4.状态修正		X(k)=X^(k)+K(k)(Y(k)-HX^(k))    p->X_now = p->X_mid+p->kg*(dat-p->X_mid);
	float dat_Xmid_temp[2][1],kg_dat_Ximd_temp[2][1];
	second_matrix_2x1_subtraction(dat,p->X_mid,dat_Xmid_temp);
	second_matrix_2x2_times_2x1_multiplication(p->kg,dat_Xmid_temp,kg_dat_Ximd_temp);
	second_matrix_2x1_addition(p->X_mid,kg_dat_Ximd_temp,p->X_now);

	//5.协方差修正	P(k)=[In-K(k)H]P^(k)    p->P_now = (1-p->kg)*p->P_mid;
	float eye[2][2] = {{1,0},{0,1}},eye_kg_temp[2][2];
	second_matrix_2x2_subtraction(eye,p->kg,eye_kg_temp);
	second_matrix_2x2_multiplication(eye_kg_temp,p->P_mid,p->P_now);

	//    p->P_last = p->P_now;
  p->P_last[0][0] = p->P_now[0][0];
  p->P_last[0][1] = p->P_now[0][1];
  p->P_last[1][0] = p->P_now[1][0];
  p->P_last[1][1] = p->P_now[1][1];

  //    p->X_last = p->X_now;
  p->X_last[0][0] = p->X_now[0][0];
  p->X_last[1][0] = p->X_now[1][0];

}
//设置二阶卡卡尔曼参数A
void set_2nd_order_klm_paramA(KLM_CAL_2ND_ORDER *p,float T_A[2][2])
{
    mat_2x2_opt_arry_assigment(p->A,T_A);
}

//设置二阶卡卡尔曼参数P
void set_2nd_order_klm_paramP(KLM_CAL_2ND_ORDER *p,float T_P[2][2])
{
    mat_2x2_opt_arry_assigment(p->P_last,T_P);
}

//设置二阶卡卡尔曼参数A与P
void set_2nd_order_klm_param(KLM_CAL_2ND_ORDER *p,float T_A[2][2],float T_P[2][2])
{
	set_2nd_order_klm_paramA(p,T_A);
	set_2nd_order_klm_paramA(p,T_P);
}

//设置二阶卡卡尔曼参数A与P
//二阶卡尔曼二次封装中间调用接口API
static KLM_FILTER_RES klm_2nd_order_filter(KLM_CAL_2ND_ORDER *p,float dat[2][1])
{
	KLM_FILTER_RES l_st_res;
	kalman_2nd_order_filter(p,dat);
	l_st_res.X_predicted[0][0] = p->X_mid[0][0];
	l_st_res.X_predicted[1][0] = p->X_mid[1][0];
	l_st_res.X_revisionary[0][0] = p->X_now[0][0];
	l_st_res.X_revisionary[1][0] = p->X_now[1][0];
	return l_st_res;
}
//二阶卡尔曼二次封装最终调用接口API
KLM_FILTER_RES excute_klm_2nd_order_filter(KLM_CAL_2ND_ORDER *p,float var1,float var2,float sys_A[2][2])
{
	float dat[2][1];
	dat[0][0] = var1;
	dat[1][0] = var2;
	set_2nd_order_klm_paramA(p,sys_A);
	return klm_2nd_order_filter(p,dat);
}
