#ifndef __TOOL_H
#define __TOOL_H
#include "stdint.h"
#include "stdbool.h"
#include "system_init.h"
#include "board_config.h"

/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */



//工具函数，轮询式调试打印
void gcz_polling_printf(USART_SFRmap* USARTx,const char *fmt, ...);
//工具函数，轮询式调试打印，无需调用硬件串口类型
void gcz_serial_v1_polling_printf(uint8_t serial_nbr,const char *fmt, ...);
//工具函数，DMA式调试打印，无需调用硬件串口类型
void gcz_serial_v1_dma_printf(uint8_t serial_nbr,const char *fmt, ...);
#endif
