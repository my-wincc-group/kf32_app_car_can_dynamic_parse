#include "tool.h"
#include <stdio.h>
#include <stdarg.h>
#include "usart.h"
#include "board_config.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

//打印函数
void gcz_polling_printf(USART_SFRmap* USARTx,const char *fmt, ...)
{
	char str[strlen(fmt) + 50];
	va_list args;

	va_start(args,fmt);
	vsprintf(str,fmt,args); //必须用vprintf
	va_end(args);

	USART_Send(USARTx,  str,  strlen(str));
}

//打印函数
void gcz_serial_v1_polling_printf(uint8_t serial_nbr,const char *fmt, ...)
{
	USART_SFRmap *USARTx;
	switch(serial_nbr)
	{
	case SERIAL_UART0:	USARTx = USART0_SFR;
							break;
	case SERIAL_UART1:	USARTx = USART1_SFR;
							break;
	case SERIAL_UART2:	USARTx = USART2_SFR;
							break;
	case SERIAL_UART3:	USARTx = USART3_SFR;
							break;
	case SERIAL_UART4:	USARTx = USART4_SFR;
						break;
	case SERIAL_UART5:	USARTx = USART5_SFR;
							break;
	case SERIAL_UART6:	USARTx = USART6_SFR;
							break;
	case SERIAL_UART7:	USARTx = USART7_SFR;
							break;
	}
	char str[strlen(fmt) + 50];
	va_list args;

	va_start(args,fmt);
	vsprintf(str,fmt,args); //必须用vprintf
	va_end(args);

	USART_Send(USARTx,  str,  strlen(str));
}
//打印函数
void gcz_serial_v1_dma_printf(uint8_t serial_nbr,const char *fmt, ...)
{
	char str[strlen(fmt) + 50];
	va_list args;

	va_start(args,fmt);
	vsprintf(str,fmt,args); //必须用vprintf
	va_end(args);

	switch(serial_nbr)
	{
//	case SERIAL_UART0:	USARTx = USART0_SFR;
//							break;
	case SERIAL_UART1:	Usart1_DMA_Transmit(str, strlen(str));
							break;
//	case SERIAL_UART2:	USARTx = USART2_SFR;
//							break;
//	case SERIAL_UART3:	USARTx = USART3_SFR;
//							break;
//	case SERIAL_UART4:	USARTx = USART4_SFR;
//						break;
//	case SERIAL_UART5:	USARTx = USART5_SFR;
//							break;
//	case SERIAL_UART6:	USARTx = USART6_SFR;
//							break;
//	case SERIAL_UART7:	USARTx = USART7_SFR;
//							break;
	}

}
