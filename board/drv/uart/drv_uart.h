#ifndef __DRV_UART_H
#define __DRV_UART_H
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

//uart1初始化
void drv_uart1_init();
//uart4初始化
void drv_uart4_init();
#endif
