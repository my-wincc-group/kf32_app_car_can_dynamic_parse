#include "drv_uart.h"
#include "usart.h"
#include "board_config.h"
#include "serial_v1.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */
void usart_transmit_polling(uint8_t serial_nbr,uint8_t *buffer,uint32_t length)
{
	USART_SFRmap *USARTx;
	switch(serial_nbr)
	{
	case SERIAL_UART0:	USARTx = USART0_SFR;
							break;
	case SERIAL_UART1:	USARTx = USART1_SFR;
							break;
	case SERIAL_UART2:	USARTx = USART2_SFR;
							break;
	case SERIAL_UART3:	USARTx = USART3_SFR;
							break;
	case SERIAL_UART4:	USARTx = USART4_SFR;
						break;
	case SERIAL_UART5:	USARTx = USART5_SFR;
							break;
	case SERIAL_UART6:	USARTx = USART6_SFR;
							break;
	case SERIAL_UART7:	USARTx = USART7_SFR;
							break;
	}
	USART_Send(USARTx, buffer, length);
}
void uart1_gpio_cfg()
{
	GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_15, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOE_SFR, GPIO_PIN_MASK_0, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOA_SFR, GPIO_Pin_Num_15, GPIO_RMP_AF5_USART1);	  // 重映射为USART1
	GPIO_Pin_RMP_Config (GPIOE_SFR, GPIO_Pin_Num_0, GPIO_RMP_AF5_USART1);     // 重映射为USART1
	GPIO_Pin_Lock_Config (GPIOA_SFR, GPIO_PIN_MASK_15, TRUE);                  // 配置锁存
	GPIO_Pin_Lock_Config (GPIOE_SFR, GPIO_PIN_MASK_0, TRUE);                  // 配置锁存
}
//uart4的引脚电气配置
void uart4_gpio_cfg()
{
	GPIO_Write_Mode_Bits(GPIOD_SFR, GPIO_PIN_MASK_3, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Write_Mode_Bits(GPIOD_SFR, GPIO_PIN_MASK_4, GPIO_MODE_RMP);          // 重映射IO口功能模式
	GPIO_Pin_RMP_Config (GPIOD_SFR, GPIO_Pin_Num_3, GPIO_RMP_AF6_USART4);	  // 重映射为USART4
	GPIO_Pin_RMP_Config (GPIOD_SFR, GPIO_Pin_Num_4, GPIO_RMP_AF6_USART4);     // 重映射为USART4
	GPIO_Pin_Lock_Config (GPIOD_SFR, GPIO_PIN_MASK_3, TRUE);                  // 配置锁存
	GPIO_Pin_Lock_Config (GPIOD_SFR, GPIO_PIN_MASK_4, TRUE);                  // 配置锁存
}
//uart4初始化
void uart4_init()
{
	// 外接BT	9600 bps
	//polling
	uart4_gpio_cfg();
	INT_Interrupt_Priority_Config(INT_USART4, 3, 3);
	USART_Async_config(USART4_SFR, USART4_BAUD);
	USART_RDR_INT_Enable(USART4_SFR, TRUE);
	INT_Interrupt_Enable(INT_USART4, TRUE);
	USART_ReceiveData(USART4_SFR);		//清接收标志位
}
void uart1_init()
{
#if 0
	//gcz 2022-06-03  修改主优先级和子优先级
	INT_Interrupt_Priority_Config(INT_USART1,1,0);
	//INT_Set_Interrupt_Priority(INT_USART1, 15);
	USART_Async_config(USART1_SFR, 115200);
	Enable_Usart_Interrupt(USART1_SFR, INT_USART1);
	//gcz  2022-06-03 增加接收空闲中断，用于封包
//	USART_Receive_Idle_Frame_Config(USART1_SFR,TRUE);
//	USART_IDLE_INT_Enable(USART1_SFR,TRUE);
#endif
	//DMA
	INT_Interrupt_Priority_Config(INT_USART1, 1, 0);
	USART_Async_config(USART1_SFR, USART1_BAUD);
	Enable_Usart_Interrupt(USART1_SFR, INT_USART1);

	DMA_USART1_Init();					// USART1_DMA初始化
	Enable_DMA_USART1(TRUE);
	USART_SendData(USART1_SFR ,0x00);	// 启动  usart1-dma 发送
	// 增加空闲中断
	USART_Receive_Idle_Frame_Config(USART1_SFR,TRUE);
	USART_IDLE_INT_Enable(USART1_SFR,TRUE);
}
//uart1初始化
void drv_uart1_init()
{
	uart1_gpio_cfg();		//gpio配置
	uart1_init();			//串口4初始化
	g_st_serial_opt.serial_hardware_transmit_callback[SERIAL_UART1] = usart_transmit_polling;//挂接发送函数
}
//uart4初始化
void drv_uart4_init()
{
	uart4_gpio_cfg();		//gpio配置
	uart4_init();			//串口4初始化
	g_st_serial_opt.serial_hardware_transmit_callback[SERIAL_UART4] = usart_transmit_polling;//挂接发送函数
}


