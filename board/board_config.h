#ifndef __BOARD_CONFIG_H
#define __BOARD_CONFIG_H
#include "soft_timer.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

//串口号
enum
{
	SERIAL_UART0 = 0,
	SERIAL_UART1,
	SERIAL_UART2,
	SERIAL_UART3,
	SERIAL_UART4,
	SERIAL_UART5,
	SERIAL_UART6,
	SERIAL_UART7,
	MAX_NBR
};

//软件定时器对象定义
extern SOFT_TIMER_OBJ g_st_sft_tmr_obj_wt_jy901;
#endif
