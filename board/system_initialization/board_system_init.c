#include "board_system_init.h"
#include "serial_v1.h"
#include "sensor_initialization.h"
#include "alg_lib.h"
#include "drv_initalization.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

//系统全局变量初始化
void system_board_init()
{
	g_st_serial_opt.serial_init_func();	//串口组件初始化
	board_drv_init();					//板级MCU片上驱动
	sensor_init();						//传感器初始化
}
