#ifndef __WT_JC901_H
#define __WT_JC901_H
#include <stdbool.h>
#include <stdint.h>
#include "alg_lib.h"
#include "board_config.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */

#define WT_JY901_PKG_LEN 	11
#define	FRAME_ACC			0X51		//加速度
#define	FRAME_ANGULAR_V		0X52		//角速度
#define	FRAME_ANGLE			0X53		//角度
#define	FRAME_MAGN_F		0X54		//磁场

#define ACCELERATION_PARAM		16
#define	ANGULAR_VEL_PARAM		2000
#define	ANGLE_PARAM				180

#define X_P				0
#define Y_P				1
#define Z_P				2

#define WT901_IMU_CACHE_NBR			5

typedef struct _imu_sample_data
{
	float acc[3][WT901_IMU_CACHE_NBR];							//加速度X-Y-Z
	float angular_v[3][WT901_IMU_CACHE_NBR];						//角速度X-Y-Z
	float angle[3][WT901_IMU_CACHE_NBR];							//角度X-Y-Z
}IMU_SAMPLEING_DATA;

typedef struct _imu_fiter_data
{
	float acc[3];							//加速度X-Y-Z
	float angular_v[3];						//角速度X-Y-Z
	float angle[3];							//角度X-Y-Z
}IMU_FILTER_DATA;


extern KLM_CAL_1ST_ORDER g_st_klm_acc[3];
extern KLM_CAL_1ST_ORDER g_st_klm_angular[3];
extern KLM_CAL_1ST_ORDER g_st_klm_angle[3];
extern IMU_FILTER_DATA g_st_imu_data;
extern IMU_FILTER_DATA g_st_imu_klm_filter_data;
//传感器初始化
void wt_jc901_init();
//维特JY901数据解析
uint8_t wt_jy901_analysis(uint8_t *buffer,uint32_t length);
//获取IMU实时数据
IMU_FILTER_DATA *get_imu_original_data();
//获取IMU实时klm一阶数据
IMU_FILTER_DATA *get_imu_klm_filter_data();
//卡尔曼滤波处理后的IMU数据
float klm_imu_filter(uint8_t imu_type,uint8_t axis,KLM_CAL_1ST_ORDER* p,float dat);
#endif
