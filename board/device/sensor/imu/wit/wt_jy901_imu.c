#include "wt_jy901_imu.h"
#include "serial_v1.h"
#include "drv_uart.h"
#include "tool.h"
#include "aeb_sensor_management.h"
/*
 * 作者：郭超卓
 * 日期：2022年8月16日
 * */
//50ms，5个值的均值数据
IMU_SAMPLEING_DATA g_st_imu_sampling_data;
//原始值
IMU_FILTER_DATA	g_st_imu_original_data;
//一阶卡尔曼值
KLM_CAL_1ST_ORDER g_st_klm_acc[3];
#if 0
KLM_CAL_1ST_ORDER g_st_klm_angular[3];
KLM_CAL_1ST_ORDER g_st_klm_angle[3];
#endif

SOFT_TIMER_OBJ g_st_sft_tmr_obj_wt_jy901;
extern volatile int64_t SystemtimeClock;
IMU_FILTER_DATA g_st_imu_data;
IMU_FILTER_DATA g_st_imu_klm_filter_data;

//获取IMU缓存数据
IMU_SAMPLEING_DATA *get_imu_data_cache()
{
	return &g_st_imu_sampling_data;
}
//获取IMU实时原始数据
IMU_FILTER_DATA *get_imu_original_data()
{
	return &g_st_imu_original_data;
}
//获取IMU实时klm一阶数据
IMU_FILTER_DATA *get_imu_klm_filter_data()
{
	return &g_st_imu_klm_filter_data;
}
//维特901协议接收处理
#define NO_PKG_DATA		0
#define PKG_HEAD_OK		1
#define PKG_RCVING		2
uint8_t  wt_jy901_protocol_process(uint8_t data)
{
	static bool startBit = 0;
	static uint8_t cnt = 0;
	uint8_t res = NO_PKG_DATA;

	if (data == 0x55)
	{
		startBit = true;
		cnt = 1;
		res = PKG_HEAD_OK;
	//	gcz_serial_v1_polling_printf(SERIAL_UART1,"1.cnt:%d %02x\r\n",cnt,data);
	}
	else
	{
		if ((startBit == true) && (cnt < 11))
		{
			res = PKG_RCVING;
			cnt++;
		//	gcz_serial_v1_polling_printf(SERIAL_UART1,"2.cnt:%d %02x\r\n",cnt,data);
		}
		else
		{
			startBit = false;
			cnt = 0;
		//	gcz_serial_v1_polling_printf(SERIAL_UART1,"11\r\n");
		}
	}
	return res;
}
//插入解析数据到缓存
void put_imu_data_to_cache(uint8_t frame_type,int16_t data,uint8_t direction)
{
	static uint8_t put_ptr[3] = {0,0,0};
	if (frame_type == FRAME_ACC)
	{
		g_st_imu_sampling_data.acc[direction][put_ptr[X_P]] = data / 32768.0 * ACCELERATION_PARAM*9.8;
		g_st_imu_original_data.acc[direction] = g_st_imu_sampling_data.acc[direction][put_ptr[X_P]];

		if ((++put_ptr[X_P]) >= WT901_IMU_CACHE_NBR)
			put_ptr[X_P] = 0;
	}
	else if (frame_type == FRAME_ANGULAR_V)
	{
		g_st_imu_sampling_data.angular_v[direction][put_ptr[Y_P]] = data / 32768.0 * ANGULAR_VEL_PARAM;
		g_st_imu_original_data.angular_v[direction] = g_st_imu_sampling_data.angular_v[direction][put_ptr[Y_P]];
	//	gcz_serial_v1_polling_printf(SERIAL_UART1,"[ANGULAR_V-ana]%d:%0.4f\r\n",direction,g_st_imu_original_data.angular_v[direction]);

		if ((++put_ptr[Y_P]) >= WT901_IMU_CACHE_NBR)
			put_ptr[Y_P] = 0;
	}
	else if (frame_type == FRAME_ANGLE)
	{
		g_st_imu_sampling_data.angle[direction][put_ptr[Z_P]] = data / 32768.0 * ANGLE_PARAM;
		g_st_imu_original_data.angle[direction] = g_st_imu_sampling_data.angle[direction][put_ptr[Z_P]];
		if ((++put_ptr[Z_P]) >= WT901_IMU_CACHE_NBR)
			put_ptr[Z_P] = 0;
	}
}
//数据解析函数
uint8_t wt_jy901_analysis(uint8_t *buffer,uint32_t length)
{
	uint8_t res = 0;
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if (!(m_sensor_mgt.m_imu.m_isOpen == FUNC_OPEN) && (m_sensor_mgt.m_imu.m_producer == 4))
		return res;
	if ((length == WT_JY901_PKG_LEN)
		&& (buffer[0] == 0x55)
		&& ((buffer[1] >= 0x51) && (buffer[1] <= 0x54)))
	{
//		if (buffer[0] == 0x55)
//		{
//			gcz_serial_v1_polling_printf(SERIAL_UART1,"sysclk:%d\r\n",SystemtimeClock);
//		}
		uint32_t sum = 0;
		for (int i = 0; i < WT_JY901_PKG_LEN - 1; i++)
		{
			sum += buffer[i];
	//		gcz_serial_v1_polling_printf(SERIAL_UART1,"%02x ",buffer[i]);
		}
		sum &= 0x00FF; //Removing the third high byte base on the IMU productor suggestion

		if (sum == buffer[WT_JY901_PKG_LEN - 1])
		{
			short accel[3];
			short angularVelocity[3];
			short angle[3];
			res = buffer[1];
			switch (buffer[1])
			{
				case 0x50:
				case 0x58:
				case 0x5A:
								break;
				case FRAME_ACC:
								accel[0] = (short)((short)buffer[3] << 8 | buffer[2]);
								accel[1] = (short)((short)buffer[5] << 8 | buffer[4]);
								accel[2] = (short)((short)buffer[7] << 8 | buffer[6]);


								put_imu_data_to_cache(FRAME_ACC,accel[0],X_P);
								put_imu_data_to_cache(FRAME_ACC,accel[1],Y_P);
								put_imu_data_to_cache(FRAME_ACC,accel[2],Z_P);
		//						temperature = (buffer[9] << 8 | buffer[8]) / 100;
								break;
				case FRAME_ANGULAR_V:
								//angular velocity calculation
//								angularVelocity[0] = (short)((short)buffer[3] << 8 | buffer[2]);
//								angularVelocity[1] = (short)((short)buffer[5] << 8 | buffer[4]);
//								angularVelocity[2] = (short)((short)buffer[7] << 8 | buffer[6]);

								angularVelocity[0] = (short)((short)buffer[3] << 8 | buffer[2]);
								angularVelocity[1] = (short)((short)buffer[5] << 8 | buffer[4]);
								angularVelocity[2] = (short)((short)buffer[7] << 8 | buffer[6]);

								put_imu_data_to_cache(FRAME_ANGULAR_V,angularVelocity[0],X_P);
								put_imu_data_to_cache(FRAME_ANGULAR_V,angularVelocity[1],Y_P);
								put_imu_data_to_cache(FRAME_ANGULAR_V,angularVelocity[2],Z_P);
		//						malfunction = (buffer[9] << 8 | buffer[8]);
								break;
				case FRAME_ANGLE:
								angle[0] = (short)((short)buffer[3] << 8 | buffer[2]);
								angle[1] = (short)((short)buffer[5] << 8 | buffer[4]);
								angle[2] = (short)((short)buffer[7] << 8 | buffer[6]);

					//			gcz_serial_v1_polling_printf(SERIAL_UART1,"%d %d %d\r\n",angle[0],angle[1],angle[2]);
								put_imu_data_to_cache(FRAME_ANGLE,angle[0],X_P);
								put_imu_data_to_cache(FRAME_ANGLE,angle[1],Y_P);
								put_imu_data_to_cache(FRAME_ANGLE,angle[2],Z_P);
								break;
				default:
						break;
			}
		}
	}
	return res;
}
//维特901串口封包回调函数
//参数：串口号	输入缓存		输入长度		输出缓存		输出长度
//返回值：true：系统自动封包		false：无需封包
//说明：该函数嵌入到接收中断中，因此，应避免少处理，仅做字符判断，且不打印
bool wt_jy901_rx_process(uint8_t serial_nbr,const uint8_t *inbuffer,uint32_t inlength,uint8_t *outbuffer,uint32_t *outlength)
{
	bool res = false;
	bool rcv_flag = false;

	if (inlength == 1)
	{
		uint8_t stste = wt_jy901_protocol_process(inbuffer[0]);
		if (stste == PKG_HEAD_OK)
		{
			outbuffer[0] = inbuffer[0];
			*outlength = 1;
			rcv_flag = true;
	//		gcz_serial_v1_polling_printf(SERIAL_UART1,"[3]%02x %d\r\n",inbuffer[0],*outlength);
		}
		else if (stste == PKG_RCVING)
		{
			outbuffer[*outlength] = inbuffer[0];
			*outlength += 1;
			rcv_flag = true;
	//		gcz_serial_v1_polling_printf(SERIAL_UART1,"[4]%02x %d\r\n",inbuffer[0],*outlength);
		}
		if (rcv_flag == true)
		{
		//	gcz_serial_v1_polling_printf(SERIAL_UART1,"[5]%02x %d\r\n",inbuffer[0],*outlength);
			if ((*outlength == 11)
				&& (outbuffer[0] == 0x55)
				&& ((outbuffer[1] >= 0x51)
				&& (outbuffer[1] <= 0x54)))
			{
			//	gcz_serial_v1_polling_printf(SERIAL_UART1,"[6]%02x %d\r\n",outbuffer[1],*outlength);
				res = true;
			}
//			//格式封包
//			//(1)回车换行封包
//			if ((*outlength >= 2)
//				&& ((outbuffer[*outlength - 1] == 0x0a)
//				|| (outbuffer[*outlength - 2] == 0x0d)))
//			{
//				res = true;
//				g_st_serial_opt.serial_rx_put_move_ptr_next(serial_nbr);
//			}
		}
	}
	return res;
}
//jy901定时回调函数，用于每隔50个tick(ms)进行一次平均
void jy_901_timer_call_back(void * arg)
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if (!(m_sensor_mgt.m_imu.m_isOpen == FUNC_OPEN) && (m_sensor_mgt.m_imu.m_producer == 4))
		return;
	g_st_imu_data.acc[X_P] = avg_filter(g_st_imu_sampling_data.acc[X_P],WT901_IMU_CACHE_NBR);
	g_st_imu_data.acc[Y_P] = avg_filter(g_st_imu_sampling_data.acc[Y_P],WT901_IMU_CACHE_NBR);
#if 1
	g_st_imu_data.acc[Z_P] = avg_filter(g_st_imu_sampling_data.acc[Z_P],WT901_IMU_CACHE_NBR);
#endif

#if 1
	g_st_imu_data.angular_v[X_P] = avg_filter(g_st_imu_sampling_data.angular_v[X_P],WT901_IMU_CACHE_NBR);
	g_st_imu_data.angular_v[Y_P] = avg_filter(g_st_imu_sampling_data.angular_v[Y_P],WT901_IMU_CACHE_NBR);
#endif
	g_st_imu_data.angular_v[Z_P] = avg_filter(g_st_imu_sampling_data.angular_v[Z_P],WT901_IMU_CACHE_NBR);

#if 1
	g_st_imu_data.angle[X_P] = avg_filter(g_st_imu_sampling_data.angle[X_P],WT901_IMU_CACHE_NBR);
#endif
	g_st_imu_data.angle[Y_P] = avg_filter(g_st_imu_sampling_data.angle[Y_P],WT901_IMU_CACHE_NBR);
	g_st_imu_data.angle[Z_P] = avg_filter(g_st_imu_sampling_data.angle[Z_P],WT901_IMU_CACHE_NBR);
}
//卡尔曼滤波处理后的IMU数据
float klm_imu_filter(uint8_t imu_type,uint8_t axis,KLM_CAL_1ST_ORDER* p,float dat)
{
	float klm_res;
	if (imu_type == FRAME_ACC)
		klm_res = kalman_1st_order_filter(&p[axis],dat);
	else if (imu_type == FRAME_ANGULAR_V)
		klm_res = kalman_1st_order_filter(&p[axis],dat);
	else if (imu_type == FRAME_ANGLE)
		klm_res = kalman_1st_order_filter(&p[axis],dat);

	return klm_res;
}
//传感器初始化
void wt_jc901_init()
{
	for (uint8_t i1 = 0;i1 < 3;i1++)
	{
		kalman_1st_order_create(&g_st_klm_acc[i1],2,1);
#if 0
		kalman_1st_order_create(&g_st_klm_angular[i1],2,1);
		kalman_1st_order_create(&g_st_klm_angle[i1],2,1);
#endif
	}
#if 0
	//创建软定时器
	g_st_soft_timer_opt.sft_tmr_creat(&g_st_sft_tmr_obj_wt_jy901,			//定时器对象
										&SystemtimeClock,					//滴答定时器时钟接口
										TRIGGER_PERIOD,						//定时器触发模式
										50,									//定时周期
										(void *)NULL);						//回调函数传入的参数
	//开启定时器执行回调函数
	g_st_soft_timer_opt.sft_tmr_start(&g_st_sft_tmr_obj_wt_jy901,			//定时器对象
										jy_901_timer_call_back);			//定时周期执行回调函数
#endif
	//串口协议封包处理回调函数对接,必须优先于驱动
	g_st_serial_opt.process_rx_stream_callback[SERIAL_UART4] = wt_jy901_rx_process;
	//设备外设接口uart4的驱动初始化
	drv_uart4_init();
}

