（1）system_initialization用于对系统所有需要的变量、参数进行初始化，这里会调用
sensor文件夹中的sensor_initialization.h对传感器进行初始化


//框架系统调用关系
system_initialization
——>sensor_initialization
	——>sensor->imu->wt->wt_jc901_imu.h
		——>drv->uart->drv_uart.h