/*
 * _4g_server.h
 *
 *  Created on: 2022-3-10
 *      Author: Administrator
 */

#ifndef _4G_SERVER_H_
#define _4G_SERVER_H_
#include "upgrade_common.h"
#include "cqueue.h"
#include "flash.h"

#define NTP_ENABLE						1	// 1:enable,0:disable; 20220913 lmz note	被主动获取服务器的信息替换掉了，因为中途能获取平台的时间信息

void 	EC200U_Server_Init();
void 	UTCTIME_Count_In_SysClick_IT(uint32_t sysTimeClock);	// 在滴答定时器函数体
void 	Connect_Server();
void 	Reconnect_Server(uint8_t enable_position);			// 连接服务器
uint8_t _4G_Connect_Server_ASCII();	// 二进制方式连接服务器
bool 	Analysis_Reply_Secret_ASCII(uint8_t *data, uint16_t contentLen);
bool 	Analysis_Reply_Device_Register_ASCII(uint8_t *data, uint16_t contentLen);
bool 	_4G_Recv_Connect_Server_Message(uint8_t *string);

#if NTP_ENABLE
#else
	extern void AT_Request_To_Server_Connect_Info_1();
#endif

#endif /* _4G_SERVER_H_ */
