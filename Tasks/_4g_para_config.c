/*
 * platform_alg_para.c
 *
 *  Created on: 2022-2-24
 *      Author: Administrator
 */
#include "_4g_para_config.h"
#include "modbus_crc16.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "cqueue.h"
#include "upgrade_common.h"
#include "EC200U.h"
#include "at_parse_alg_para.h"
#include "gpio.h"
#include "usart.h"
#include "_4g_server.h"
#include "data_loss_prevention.h"
#include "aeb_sensor_management.h"
#include "car_can_analysis.h"
#include "params/vehicle_params.h"
#include "params/srr_params.h"
/* ------------------------宏 枚举------------------------------- */
// 二进制协议状态码 state code
enum {
	STATE_OK,
	STATE_ERR,
	STATE_NO_FIND_CMD = 5014,	// 此操作不支持
};

/******************************* 全局变量 **********************************/
UART_RCV_OPT g_st_uart0_rcv_obj;
volatile uint16_t req_index 	= 0;		// request array 下标
volatile bool is_reply 			= false;
/******************************* 全局函数 **********************************/
bool 	Analysis_Reply_ASCII(uint8_t *data, uint16_t len);
bool	Analysis_Request_ASCII(uint8_t *data, uint16_t len);

void 	Analysis_Reply_Common_Info_ASCII(uint8_t *data, uint16_t contentLen);
bool 	Analysis_Vehicle_Type_ASCII(uint8_t *data, uint16_t contentLen);
bool 	Analysis_Request_AEB_Config_Param_ASCII(uint8_t *data, uint16_t contentLen);
bool 	Analysis_Reply_AEB_Config_Param_ASCII(uint8_t *data, uint16_t dataLen);
bool	_4G_Recv_Platform_Interaction_ASCII_Message(uint8_t *string,uint32_t length);//gcz 2022-06-02 增加参数：数据长度的输入
bool 	_4G_Recv_Not_Caught_Exception(uint8_t *string);
void 	Set_AEB_Config_Param_ASCII(KunLun_AEB_Para_Cfg algP, NodeData_S nodeData);
bool 	Request_Get_Vehicle_Type_ASCII();
bool 	Request_Get_AEB_Config_Param_ASCII();
bool	Request_Device_Soft_Hardware_Info_ASCII();
bool 	Reply_Device_Soft_Hardware_Info_ASCII(uint32_t seqNum);
void 	Collection_Device_Soft_Hardware_Info_ASCII();
void 	Print_AEB_Config_Param_ASCII(KunLun_AEB_Para_Cfg algP);


//gcz   2022-06-02 增加串口接收缓存初始化
void init_uart_x_rcv_cache(UART_RCV_OPT *pStCache)
{
	memset(pStCache,0,sizeof(UART_RCV_OPT));
}
//串口0接收初始化
void InitUart0RcvCache()
{
	init_uart_x_rcv_cache(&g_st_uart0_rcv_obj);
}
/*
 * 判断小屏幕是否异常，异常就重启
 * 参数：上报数据包指针
 * 说明：检测周期为10秒
 */
void Checkout_Displayer_Error_And_Let_It_Restart_In_10S(AEB_UPLOAD_INFO *p)
{
	static uint32_t display_err_clk = 0;
	static uint16_t loop_times = 0;
	if(SystemtimeClock - display_err_clk < 10) return ;	// interval:10ms
	display_err_clk = SystemtimeClock;

	uint8_t screen_online_status = p->_ST_status._ST_perph_status._ST_displayer._U8_online_status & 0x03;
	// 0不存在；1离线；2在线
	if(screen_online_status == 1){		// 离线
		// 先关闭使能
		if(loop_times == 999){			// 关闭使能
			loop_times++;
			GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_15, GPIO_MODE_OUT);		// PB15 -- 输出控制供电拐角
			GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_15, Bit_RESET);
		}else if(loop_times == 1000){	// 开启使能
			loop_times++;
			// 再打开使能
			GPIO_Set_Output_Data_Bits(GPIOB_SFR, GPIO_PIN_MASK_15, Bit_SET);
		}else if(loop_times >= 2000){	// 流出时间让设备重启后，正常通讯
			loop_times = 0;
		}else loop_times++;
	}else if(screen_online_status == 2){// 在线
		loop_times = 0;
	}
}
//--------------------------------------------
void Upload_Info_Collection_ASCII()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return ;

	if(upgrade_info.step == UPGRADE_SUB_DMN) return ;
	if(upgrade_info.step == UPGRADE_URL) return ;

	judgement_all_warning_event_is_need_to_be_insert();		//判断报警事件，并插入缓存
	AEB_UPLOAD_INFO *info = other_user_get_upload_info();	//从缓存查询事件并导入接口数据结构对象中

	// 数据上报信息收集
	Collection_Warning_Info_ASCII(info);					// 预警
	Collection_VehicleBody_Info_ASCII(info);				// 车身信息
	Collection_VehicleBase_Info_ASCII(info);				// 车辆基本信息 10min
	Collection_HeartBeat_ASCII();							// 心跳 10秒
	Collection_GPS_Info_ASCII();							// GPS信息
	Collection_Device_Soft_Hardware_Info_ASCII();			// 设备的软硬件信息
	Checkout_Displayer_Error_And_Let_It_Restart_In_10S(info); // 检测小屏幕异常，10秒
}

/*
 * 设备的软硬件信息
 * 触发条件：系统上电会刷一次，后面每当一个CAN设备通讯正常，都会刷一次，掉线一个，也会刷一次
 */
void Collection_Device_Soft_Hardware_Info_ASCII()
{
//	if(!server_info.isConnect) return ;
	if(!upgrade_info.isUpgradeOK) return ;

	if (get_refresh_flag() == TRUE)
	{
		set_refresh_flag(FALSE);
	//	refresh_test(USART1_STREAM, &device_version_str);//测试刷新输出

		Request_Device_Soft_Hardware_Info_ASCII();
	}
}
/*
 * 将character类型的float类型转成float格式
 * 参数1：数据
 * 参数2：长度
 */
float Character2Float(uint8_t *data, uint8_t len)
{
	float retVal = 0.0f;
	uint8_t value_p[4] = {0};
	for(uint8_t i=0; i<len; i++){
		value_p[i] = data[i];
	}
	retVal = *(float*)value_p;

	return retVal;
}

void Print_AEB_Config_Param_ASCII(KunLun_AEB_Para_Cfg algP)
{
	fprintf(USART1_STREAM, "------------------ASCII---------begin----------\r\n");

	fprintf(USART1_STREAM, "Vehicle_Type:%d\r\n", server_info.vehicle_type);	// add lmz 20220224
	fprintf(USART1_STREAM, "parm_version:%ld\r\n", algP.params_version);
	fprintf(USART1_STREAM, "AEB_CMS_Enable:%d\r\n", algP.switch_g.AEB_CMS_Enable);
	fprintf(USART1_STREAM, "CMS_HMW_Enable:%d\r\n", algP.switch_g.CMS_HMW_Enable);
	fprintf(USART1_STREAM, "CMS_TTC_Enable:%d\r\n", algP.switch_g.CMS_TTC_Enable);
	fprintf(USART1_STREAM, "AEB_TTC_Enable:%d\r\n", algP.switch_g.AEB_TTC_Enable);
	fprintf(USART1_STREAM, "SSS_Enable:%d\r\n", algP.switch_g.SSS_Enable);
	fprintf(USART1_STREAM, "HMW_Dynamic_Thr_Enable:%d\r\n", algP.switch_g.HMW_Dynamic_Thr_Enable);
	fprintf(USART1_STREAM, "Chassis_Safe_Strategy_Enable:%d\r\n", algP.switch_g.Chassis_Safe_Strategy_Enable);
	fprintf(USART1_STREAM, "LDW_Enable:%d\r\n", algP.switch_g.LDW_Enable);

	fprintf(USART1_STREAM, "Displayer_Switch_Enable:%d\r\n", algP.switch_g.Displayer_Switch_Enable);
	fprintf(USART1_STREAM, "Retract_Brake_Enable:%d\r\n", algP.switch_g.Retract_Brake_Enable);

	fprintf(USART1_STREAM, "Brake_Cooling_Time:%4.2f\r\n", algP.Brake_Cooling_Time);
	fprintf(USART1_STREAM, "Driver_Brake_Cooling_Time:%4.2f\r\n", algP.Driver_Brake_Cooling_Time);
	fprintf(USART1_STREAM, "Max_Brake_Keep_Time:%4.2f\r\n", algP.Max_Brake_Keep_Time);
	fprintf(USART1_STREAM, "Air_Brake_Delay_Time:%4.2f\r\n", algP.Air_Brake_Delay_Time);
	fprintf(USART1_STREAM, "Max_Percent_Decelerate:%4.2f\r\n", algP.Max_Percent_Decelerate);
	fprintf(USART1_STREAM, "Min_Enable_Speed:%4.2f\r\n", algP.Min_Enable_Speed);
	fprintf(USART1_STREAM, "Max_Output_dec:%4.2f\r\n", algP.Max_Output_dec);
	fprintf(USART1_STREAM, "Ratio_Force_To_Deceleration:%4.2f\r\n", algP.Ratio_Force_To_Deceleration);
	fprintf(USART1_STREAM, "CMS_HMW_Brake_Time_Thr:%4.2f\r\n", algP.CMS_HMW_Brake_Time_Thr);
	fprintf(USART1_STREAM, "CMS_HMW_Brake_Force_Feel_Para:%4.2f\r\n", algP.CMS_HMW_Brake_Force_Feel_Para);
	fprintf(USART1_STREAM, "CMS_HMW_Warning_Time_Thr:%4.2f\r\n", algP.CMS_HMW_Warning_Time_Thr);
//	fprintf(USART1_STREAM, "CMS_HMW_Dynamic_Offset_Speed_L:%4.2f\r\n", algP.CMS_HMW_Dynamic_Offset_Speed_L);
//	fprintf(USART1_STREAM, "CMS_HMW_Dynamic_Offset_Value_L:%4.2f\r\n", algP.CMS_HMW_Dynamic_Offset_Value_L);
//	fprintf(USART1_STREAM, "CMS_HMW_Dynamic_Offset_Speed_H:%4.2f\r\n", algP.CMS_HMW_Dynamic_Offset_Speed_H);
//	fprintf(USART1_STREAM, "CMS_HMW_Dynamic_Offset_Value_H:%4.2f\r\n", algP.CMS_HMW_Dynamic_Offset_Value_H);

	fprintf(USART1_STREAM, "CMS_HMW_Time_Offset_Night:%4.2f\r\n", algP.CMS_HMW_Time_Offset_Night);
	fprintf(USART1_STREAM, "CMS_HMW_Time_Offset_Night_StartT:%4.2f\r\n", algP.CMS_HMW_Time_Offset_Night_StartT);
	fprintf(USART1_STREAM, "CMS_HMW_Time_Offset_Night_EndT:%4.2f\r\n", algP.CMS_HMW_Time_Offset_Night_EndT);

	fprintf(USART1_STREAM, "CMS_TTC_Brake_Time_Thr:%4.2f\r\n", algP.CMS_TTC_Brake_Time_Thr);
	fprintf(USART1_STREAM, "CMS_TTC_Brake_Force_Feel_Para:%4.2f\r\n", algP.CMS_TTC_Brake_Force_Feel_Para);
	fprintf(USART1_STREAM, "CMS_TTC_Warning_Time_Level_First:%4.2f\r\n", algP.CMS_TTC_Warning_Time_Level_First);
	fprintf(USART1_STREAM, "CMS_TTC_Warning_Time_Level_Second:%4.2f\r\n", algP.CMS_TTC_Warning_Time_Level_Second);
	fprintf(USART1_STREAM, "AEB_Decelerate_Set:%4.2f\r\n", algP.AEB_Decelerate_Set);
	fprintf(USART1_STREAM, "AEB_Stop_Distance:%4.2f\r\n", algP.AEB_Stop_Distance);
	fprintf(USART1_STREAM, "AEB_TTC_Warning_Time_Level_First:%4.2f\r\n", algP.AEB_TTC_Warning_Time_Level_First);
	fprintf(USART1_STREAM, "AEB_TTC_Warning_Time_Level_Second:%4.2f\r\n", algP.AEB_TTC_Warning_Time_Level_Second);
	fprintf(USART1_STREAM, "SSS_Brake_Force:%4.2f\r\n", algP.SSS_Brake_Force);
	fprintf(USART1_STREAM, "SSS_Break_Enable_Distance:%4.2f\r\n", algP.SSS_Break_Enable_Distance);
	fprintf(USART1_STREAM, "SSS_Warning_Enable_Distance:%4.2f\r\n", algP.SSS_Warning_Enable_Distance);
	fprintf(USART1_STREAM, "SSS_Max_Enable_Speed:%4.2f\r\n", algP.SSS_Max_Enable_Speed);
	fprintf(USART1_STREAM, "SSS_FR_FL_Install_Distance_To_Side:%4.2f\r\n", algP.SSS_FR_FL_Install_Distance_To_Side);
	fprintf(USART1_STREAM, "SSS_Stop_Distance:%4.2f\r\n", algP.SSS_Stop_Distance);
	fprintf(USART1_STREAM, "SSS_Default_Turn_Angle:%5.2f\r\n", algP.SSS_Default_Turn_Angle);
//	fprintf(USART1_STREAM, "Vehicle_Speed_Obtain_Method:%d\r\n", algP.Vehicle_Speed_Obtain_Method);
	fprintf(USART1_STREAM, "WheelSpeed_Coefficient:%5.2f\r\n", algP.WheelSpeed_Coefficient);
//	fprintf(USART1_STREAM, "Vehicle_State_Obtain_Method:%d\r\n", algP.Vehicle_State_Obtain_Method);

	fprintf(USART1_STREAM, "PWM_magnetic_valve_T:%d\r\n", algP.PWM_magnetic_valve_T);
	fprintf(USART1_STREAM, "PWM_magnetic_valve_N:%d\r\n", algP.PWM_magnetic_valve_N);
	fprintf(USART1_STREAM, "PWM_magnetic_valve_Switch:%d\r\n", algP.PWM_magnetic_valve_Switch);
	fprintf(USART1_STREAM, "PWM_magnetic_valve_Scale:%d\r\n", algP.PWM_magnetic_valve_Scale);

	fprintf(USART1_STREAM, "Vechile_Width:%4.2f\r\n", algP.Vechile_Width);
	fprintf(USART1_STREAM, "Vehicle_Speed_Obtain_Method:%d\r\n", algP.Vehicle_Speed_Obtain_Method);
	fprintf(USART1_STREAM, "Vehicle_State_Obtain_Method:%d\r\n", algP.Vehicle_State_Obtain_Method);
//	fprintf(USART1_STREAM, "Vehicle_Brake_Control_Mode:%d\r\n", algP.Vehicle_Brake_Control_Mode);

	fprintf(USART1_STREAM, "CAN_0_Baud_Rate:%d\r\n", algP.CAN_0_Baud_Rate);
	fprintf(USART1_STREAM, "CAN_1_Baud_Rate:%d\r\n", algP.CAN_1_Baud_Rate);
	fprintf(USART1_STREAM, "CAN_2_Baud_Rate:%d\r\n", algP.CAN_2_Baud_Rate);
	fprintf(USART1_STREAM, "CAN_3_Baud_Rate:%d\r\n", algP.CAN_3_Baud_Rate);
	fprintf(USART1_STREAM, "CAN_4_Baud_Rate:%d\r\n", algP.CAN_4_Baud_Rate);
	fprintf(USART1_STREAM, "CAN_5_Baud_Rate:%d\r\n", algP.CAN_5_Baud_Rate);

	fprintf(USART1_STREAM, "------------ASCII-----------end--------------\r\n");
}


/******************************** Analysis **********************************************************/
/*
 * 帧头0xAA55
 * 不同步时间
 * (返回：0正常；；2需要重新解析)阉割
 * 返回：false未解析；true解析成功
 * 说明：平台请求解析
 */
bool Analysis_Request_ASCII(uint8_t *data, uint16_t len)
{
//	if(!g_sensor_mgt.m_car_dvr.m_isOpen) return false;
	if(len < 31) return false;
	// 内容长度
	uint16_t contentLen = data[29] | data[30] << 8;
	if (contentLen > (len - 35)) return false;

	// 校验
	uint16_t crc16_modbus_clac = MODBUS_CRC16_v3(data, 31+contentLen);
	uint16_t crc16_modbus_src  = data[31+contentLen] | data[32+contentLen] << 8;
	if(crc16_modbus_clac != crc16_modbus_src){
//		fprintf(USART1_STREAM, "clac:%04X, src:%04X.\r\n", crc16_modbus_clac, crc16_modbus_src);
		USART1_Bebug_Print("ERROR", "request no pass crc16_modbus.", 1);
		return false;
	}

	// test print
//	fprintf(USART1_STREAM, "\r\n************FA 55 recv*****request******\r\n");
//	for(uint16_t j=0; j<len; j++){
//		fprintf(USART1_STREAM, "%02X ", data[j]);
//	}
//	fprintf(USART1_STREAM, "\r\n************FA 55 recv***********\r\n");

	uint32_t seqNum = data[2] | data[3]<<8;	// 保证接收与回复的序号一致

	// 设备SN		20B	data[5]~data[24]
	uint8_t sn[20] = {0};
	uint8_t j=0;
	for(uint8_t i=4; i<24; i++){
		if(data[i] != 0){
			sn[j] = data[i];
			j++;
		}
	}
	sn[j] = '\0';

	// 指令码
	uint8_t cmd = data[24];

	// 时间戳
	uint32_t timeStamp = data[25] | data[26]<<8 | data[27]<<16 | data[28]<<24;

#if DEBUG_PRINT_SW
//	USART1_Bebug_Print_Num("SN", sn, 1);
	USART1_Bebug_Print_Num("[RECV]seq", seqNum, 0, 1);
	USART1_Bebug_Print_Num("CMD", cmd, 2, 1);
//	USART1_Bebug_Print_Num("UTCTIME", timeStamp, 1);
//	USART1_Bebug_Print_Num("Len", contentLen, 1);
#endif
	uint8_t replyData[2] = {0};
	// 命令解析
	switch(cmd)
	{
	case HEARTBEAT_FRAME:{	// 回复心跳，告知平台设备在线
		// 直接返回心跳信息
		replyData[0] = 0x01;
		Send_Reply_Platform_ASCII(HEARTBEAT_FRAME, STATE_OK, seqNum, replyData, 1);
	}break;
	case GET_DSHINFO_FRAME:{	// 平台获取设备软硬件信息
		Reply_Device_Soft_Hardware_Info_ASCII(seqNum);
	}break;
	case GPS_INTERVAL_FRAME:{	// 设置GPS时间间隔
		if(Analysis_Request_Set_GPS_Interval_ASCII(data+31)){
			replyData[0] = 0x01;
			replyData[1] = gps_Interval_coef * GPS_TIME_INTERVAL / 1000;				// 时间间隔
			Send_Reply_Platform_ASCII(GPS_INTERVAL_FRAME, STATE_OK, seqNum, replyData, 2);
		}else{
			replyData[0] = 0x00;
			replyData[1] = Get_GPS_Interval();		// 时间间隔
			Send_Reply_Platform_ASCII(GPS_INTERVAL_FRAME, STATE_ERR, seqNum, replyData, 2);
		}
	}break;
	case SET_VTYPE_FRAME:{		// 设置车辆类型
		if(Analysis_Vehicle_Type_ASCII(data+31, contentLen)){
			replyData[0] = 0x01;		// 成功
			Send_Reply_Platform_ASCII(SET_VTYPE_FRAME, STATE_OK, seqNum, replyData, 1);
		}else{
			replyData[0] = 0x00;		// 失败
			Send_Reply_Platform_ASCII(SET_VTYPE_FRAME, STATE_ERR, seqNum, replyData, 1);
		}
	}break;
	case GET_VTYPE_FRAME:{		// 获取车辆类型
		replyData[0] = server_info.vehicle_type;
		Send_Reply_Platform_ASCII(GET_VTYPE_FRAME, STATE_OK, seqNum, replyData, 1);
	}break;
	case SET_VPARAM_FRAME:{		// 设置AEB配置参数
		if(Analysis_Request_AEB_Config_Param_ASCII(data+31, contentLen)){
			wParms.params_version++;	// 参数版本号增加
			replyData[0] = 0x01;
			Send_Reply_Platform_ASCII(SET_VPARAM_FRAME, STATE_OK, seqNum, replyData, 1);
		}else{
			replyData[0] = 0x00;
			Send_Reply_Platform_ASCII(SET_VPARAM_FRAME, STATE_ERR, seqNum, replyData, 1);
		}
	}break;
	case GET_VPARAM_FRAME:{		// 平台获取AEB配置参数
		KunLun_AEB_Para_Cfg algP = {0};
		if(Read_AEB_Config_Param(&algP)){
//			Print_AEB_Config_Param_ASCII(algP);

//			fprintf(USART1_STREAM, "Vehicle_Type:%d\r\n", server_info.vehicle_type);
			USART1_Bebug_Print_Num("Vehicle_Type", server_info.vehicle_type, 1, 1);
			Send_Reply_Set_AEB_Config_Param_ASCII(seqNum, algP);
		}else{
			replyData[0] = 0x00;		// 失败
			Send_Reply_Platform_ASCII(GET_VPARAM_FRAME, STATE_ERR, seqNum, replyData, 1);
		}
	}break;
	case SET_SENSOR_CFG_FRAME:{	// 平台设置传感器配置参数
		if(Analysis_Request_Set_Sensor_Cfg_Param_ASCII(data+31)){
			replyData[0] = 0x01;		// 成功
			Send_Reply_Platform_ASCII(SET_SENSOR_CFG_FRAME, STATE_OK, seqNum, replyData, 1);
		}else{
			replyData[0] = 0x00;		// 失败
			Send_Reply_Platform_ASCII(SET_SENSOR_CFG_FRAME, STATE_ERR, seqNum, replyData, 1);
		}
	}break;
	case GET_SENSOR_CFG_FRAME:{	// 平台获取传感器配置参数
		Reply_Platform_Request_Sensor_Cfg_Param_Info_ASCII(seqNum);
	}break;

	case CAR_DVR_SERVER_SET_PARAM:
	{	// 平台设置记录仪参数
	    if(Analysis_Request_Set_CarDvr_Param_ASCII(data+31))
		{
		   	replyData[0] = 0x01;		// 成功
			Send_Reply_Platform_ASCII(CAR_DVR_SERVER_SET_PARAM, STATE_OK, seqNum, replyData, 1);
			//设置记录仪函数
			Request_CARDVR_Set_Gsensor_C6_ASCII();
	    }else{
			replyData[0] = 0x00;		// 失败
			Send_Reply_Platform_ASCII(CAR_DVR_SERVER_SET_PARAM, STATE_ERR, seqNum, replyData, 1);
	    }
	}break;
	case CAR_DVR_SERVER_QUERY_PARAM:
	{	// 平台查询参数
		Send_Reply_Platform_Query_CarDvr_Param_Info_ASCII(seqNum);
	}break;
	case CAR_DVR_TAKE_PICTURE:
	{	//平台遥控触发拍摄
	//	获取时间戳等相关信息然后转发给记录仪
		Server_Request_CARDVR_Take_Picture_D1_ASCII(data, len-4);//len-4	去掉结尾的校验与换行4字节
		replyData[0] = 0x01;		// 成功
		Send_Reply_Platform_ASCII(CAR_DVR_TAKE_PICTURE, STATE_OK, seqNum, replyData, 1);
	}break;
#if CAR_CAN_ANALYSIS_DEBUG
	case SET_CAR_CAN_ANALYSIS_PARAM:{	// 平台设置车CAN解析配置参数 20220927 lmz add
		if(Analysis_From_Platform_Car_Can_Analysis_Info(data+31, contentLen)){	// 成功后，等待回复平台后，需要重启设备
			replyData[0] = 0x01;		// 成功
			Send_Reply_Platform_ASCII(SET_CAR_CAN_ANALYSIS_PARAM, STATE_OK, seqNum, replyData, 1);
		}else{
			replyData[0] = 0x00;		// 失败
			Send_Reply_Platform_ASCII(SET_CAR_CAN_ANALYSIS_PARAM, STATE_ERR, seqNum, replyData, 1);
		}
	}break;
	case GET_CAR_CAN_ANALYSIS_PARAM:{	// 平台获取车CAN解析配置参数 20220927 lmz add
		Reply_Platform_Request_Car_Can_Analysis_Cfg_Param_Info_ASCII(seqNum);
	}break;
#endif
	default:{		// 没能解析的指令码，直接反馈5014 20220927 lmz add
		replyData[0] = 0x00;		// 失败
		Send_Reply_Platform_ASCII(cmd, STATE_NO_FIND_CMD, seqNum, replyData, 1);
	}break;
	}

	return true;
}


void Analysis_States_Code(uint16_t states, uint8_t cmd)
{
	switch(states){
	case 0:			// 成功
		break;
	case P_ERR_5001:	// 平台找不到此设备
		USART1_Bebug_Print("ERROR", "The Platform Could Not Find The Device.", 1);
//		Reconnect_Server(1);
	break;
	case P_ERR_5002:	// 注册设备时，提供的时间戳超时
		USART1_Bebug_Print("ERROR", "Register Device, Provide Time Out.", 1);
		Reconnect_Server(0);
		break;
	case P_ERR_5003: //不满足ota升级条件
		upgrade_info.step = REQUEST;
		if(!upgrade_info.need_upgrade){
			upgrade_info.inPlatform_status = 2;
			USART1_Bebug_Print_Num("[RECV]", cmd, 2, 1);
		}
//		USART1_Bebug_Print("UPGRADE", "The Upgrade Contidion Is Not Met.");
		break;
	case P_ERR_5004:	// crc16校验失败
		USART1_Bebug_Print("ERROR", "CRC16_Modbus Is Failed.", 1);
		Reconnect_Server(1);
	break;
	case P_ERR_5005:	// 设备注册sign校验失败
		USART1_Bebug_Print("ERROR", "Register Device Sign Checkout Failed", 1);
		static uint8_t register_err = 0;
		// 清空秘钥
		if(register_err > 2){
			register_err = 0;
			uint8_t secret[SECRET_SIZE] = {0};
			Set_Secret(secret);
		}else register_err++;

		Reconnect_Server(0);
	break;
	case P_ERR_5006:	// SN长度超出范围
		USART1_Bebug_Print("ERROR", "SN Out Of Range.", 1);
//		Reconnect_Server(1);
	break;
	case P_ERR_5007:	// 设备尚未注册
		USART1_Bebug_Print("ERROR", "Device Not Registered.", 1);
//		Reconnect_Server(1);
	break;
	case P_ERR_5008:	// 附件大小超过限制
		USART1_Bebug_Print("ERROR", "Attachment Size Out Of Limit.", 1);
	break;
	case P_ERR_5009:	// 注册时提供的userId或password错误
		USART1_Bebug_Print("ERROR", "UserId or Password Is Error.", 1);
//		Reconnect_Server(1);
	break;
	case P_ERR_5010:	// 注册时指定的userId没有获取此设备secret的权限
		USART1_Bebug_Print("WARNING", "Userid Do Not Have Permission.", 1);
//		Reconnect_Server(1);
	break;
	case P_ERR_5011:{
		USART1_Bebug_Print("ERROR", "Get Device AEB Parameters Failed.", 1);
	}break;
	case P_ERR_5012:{
		USART1_Bebug_Print("ERROR", "Get Sensor Configure Parameters Failed.", 1);
	}break;
#if NTP_ENABLE
#else
	case P_ERR_5013:{
		USART1_Bebug_Print("ERROR", "Connect Server Address Error, Need To Reconnect.", 1);
		server_info.con_server_reason = 2;
//		AT_Request_To_Server_Connect_Info_1();
	}break;
#endif
	}
}
/*
 * 解析平台回复
 * 回复帧头为0x55BB
 * 同步时间
 * (返回：0正常；1异常失败；2需要重新解析)阉割
 * 返回：false未解析；true解析成功
 * 说明：平台答复解析
 */
bool Analysis_Reply_ASCII(uint8_t *data, uint16_t len)
{
	if(len < 32) return false;

	// 内容长度
	uint16_t contentLen = data[31] | data[32] << 8;
	if (contentLen > (len - 35))
		return false;
	// 校验
	uint16_t crc16_modbus_clac = MODBUS_CRC16_v3(data, 33+contentLen);
	uint16_t crc16_modbus_src  = data[33+contentLen] | data[34+contentLen] << 8;
	if(crc16_modbus_clac != crc16_modbus_src){
//		fprintf(USART1_STREAM, "clac:%04X, src:%04X.\r\n", crc16_modbus_clac, crc16_modbus_src);
		USART1_Bebug_Print("ERROR", "reply no pass crc16_modbus.", 1);
		return false;
	}

	server_info.offline_times = 0;  //离线次数归零  收到平台数据则归零
	// test print
//	fprintf(USART1_STREAM, "\r\n************F5 AA recv****replay*******\r\n");
//	for(uint16_t j=0; j<len; j++){
//		fprintf(USART1_STREAM, "%02X ", data[j]);
//	}
//	fprintf(USART1_STREAM, "\r\n************F5 AA recv***********\r\n");

	// 序号			4B	data[2]~data[3]  低-高
	uint16_t seqNum = data[2] | data[3]<<8;

	// 设备SN		20B	data[5]~data[24]
	uint8_t sn[20] = {0};
	uint8_t j=0;
	for(uint8_t i=4; i<24; i++){
		if(data[i] != 0){
			sn[j] = data[i];
			j++;
		}
	}
	sn[j] = '\0';

	// 指令码
	uint8_t cmd = data[24];

	// 时间戳
	uint32_t timeStamp = data[27] | data[28]<<8 | data[29]<<16 | data[30]<<24;
	if(timeStamp > 1652777920) g_utctime = timeStamp;	// 与平台同步时间

#if DEBUG_PRINT_SW
//	USART1_Bebug_Print_Num("SN", sn, 1);
	USART1_Bebug_Print_Num("[RECV]seq", seqNum, 0, 1);
	USART1_Bebug_Print_Num("CMD", cmd, 2, 1);
//	USART1_Bebug_Print_Num("UTCTIME", g_utctime, 1, 1);
//	USART1_Bebug_Print_Num("Len", contentLen, 1);VehParam_GetParams
#endif
	// 状态码 2Byte
	uint16_t states = data[25] | data[26]<<8;
	Analysis_States_Code(states, cmd);
	if(states != 0) {
		return false;
	}


	// 命令解析
	switch(cmd)
	{
	case GET_VTYPE_FRAME:	// 获取车辆类型
		Analysis_Vehicle_Type_ASCII(data+33, contentLen);
		break;
	case GET_VPARAM_FRAME:{	// 解析平台反馈的AEB配置参数，并存储
		if(Analysis_Reply_AEB_Config_Param_ASCII(data+33, contentLen)){
			USART1_Bebug_Print("REPLY", "Have Received Platform AEB Parameters.", 1);
		}
	}break;
	case SECRET_FRAME:		// 解析秘钥
		Analysis_Reply_Secret_ASCII(data+33, contentLen);
		break;
	case REGISTR_FRAME:		// 设备注册
		Analysis_Reply_Device_Register_ASCII(data+33, contentLen);
		break;
	case GPS_FRAME:			// GPS信息
	case WARN_FRAME:		// 报警信息
	case VBODY_FRAME:		// 设备车身信息
	case VBASE_FRAMR:		// 车辆设备基本信息
	case OTA_REPLY_FRAME:	// OTA升级完成上报，反馈
	case HEARTBEAT_FRAME:	// 心跳
	case SET_VPARAM_FRAME:	// 设置AEB配置参数
	case SET_VTYPE_FRAME:	// 设置车辆类型
	case SET_SENSOR_CFG_FRAME:	// 主动上报传感器配置参数
		Analysis_Reply_Common_Info_ASCII(data+33, contentLen);
		break;
	case OTA_REQUEST_FRAME:	// OTA请求更新状态
		Analysis_Reply_UpgradePackage_Info_ASCII(data+33, contentLen);
		break;
	case GPS_INTERVAL_FRAME:// 解析平台回复的设置GPS上报时间间隔
		Analysis_Reply_Set_GPS_Interval_ASCII(data+33, contentLen);
		break;
	case CAR_DVR_RECORD_VIDEO:	// 行车记录仪录制视频
		// 将接收的平台的E1回复，直接转接记录仪
		Request_Recv_Platform_Later_Reply_CARDVR_E1_ASCII(data+2, contentLen);
		break;
#if CAR_CAN_ANALYSIS_DEBUG
	case GET_CAR_CAN_ANALYSIS_PARAM:{	// 设备获取车CAN解析配置信息 20220927 lmz add
		if(Analysis_From_Platform_Car_Can_Analysis_Info(data+33, contentLen)){	// 解析成功后，需要重启设备
			USART1_Bebug_Print("DEBUG", "Get Platform CC Data, Device Restart Test", 1);
			SYSCTL_System_Reset_Enable(TRUE);	// 控制MCU软重启
		}
	}break;
#endif
	default:return false;
	}

	return true;
}

bool Analysis_Vehicle_Type_ASCII(uint8_t *data, uint16_t contentLen)
{
	// 内容
	uint8_t vehicle_type = data[0];
//	fprintf(USART1_STREAM, "vehicle_type:%d\r\n", vehicle_type);
	USART1_Bebug_Print_Num("vehicle_type", vehicle_type, 1, 1);
	// 设置车类型
	if(vehicle_type>0 && vehicle_type<102){
		server_info.vehicle_type = vehicle_type;

		if(!Set_Vehicle_Type_ExFlh(vehicle_type)){
			Set_Vehicle_Type_ExFlh(vehicle_type);
		}
		return true;
	}

	return false;
}

/*
 * 从二进制协议中提取出有效的数据
 * 参数1：from字符串
 * 参数2：to字符串
 * 参数3：from字符串长度
 * 返回：成功true;失败false;
 */
bool From_Characters_Analysis_Valid_Info(uint8_t *fromStr, uint8_t *toStr, uint8_t fromStrLen)
{
	uint8_t j = 0;

	for(uint8_t i=0; i<fromStrLen; i++){
		if(fromStr[i] != 0){
			toStr[j] = fromStr[i];
			j++;
		}
	}
	toStr[j] = '\0';

	return true;
}
/*
 * 对接平台参数配置
 * 参数1：配置数据；
 * 参数2：不是真实长度，但比真实长度要长；
 */
bool Analysis_Request_AEB_Config_Param_ASCII(uint8_t *data, uint16_t contentLen)
{
	uint8_t index = 0;

	// 内容
	//	AEB_CMS_Enable 1Byte
	wParms.switch_g.AEB_CMS_Enable = data[index+4];
	//	CMS_HMW_Enable 1Byte
	wParms.switch_g.CMS_HMW_Enable = data[index+5];
	//	CMS_TTC_Enable 1Byte
	wParms.switch_g.CMS_TTC_Enable = data[index+6];
	//	AEB_TTC_Enable 1Byte
	wParms.switch_g.AEB_TTC_Enable = data[index+7];
	//	SSS_Enable	 1Byte
	wParms.switch_g.SSS_Enable = data[index+8];
	//	HMW_Dynamic_Thr_Enable 1Byte
	wParms.switch_g.HMW_Dynamic_Thr_Enable = data[index+9];
	//	Chassis_Safe_Strategy_Enable 1Byte
	wParms.switch_g.Chassis_Safe_Strategy_Enable = data[index+10];
	//	LDW_Enable 	1Byte
	wParms.switch_g.LDW_Enable = data[index+11];

	wParms.switch_g.Displayer_Switch_Enable = data[index+12];
	wParms.switch_g.Retract_Brake_Enable = data[index+13];

	//	Brake_Cooling_Time 4Byte
	wParms.Brake_Cooling_Time = Character2Float(data+index+14, 4);	// 17
	//	Driver_Brake_Cooling_Time	4Byte
	wParms.Driver_Brake_Cooling_Time = Character2Float(data+index+18, 4);	// 21
	//	Max_Brake_Keep_Time		4Byte
	wParms.Max_Brake_Keep_Time = Character2Float(data+index+22, 4);	// 25
	//	Air_Brake_Delay_Time	4Byte
	wParms.Air_Brake_Delay_Time = Character2Float(data+index+26, 4);	// 29
	//	Max_Percent_Decelerate	4Byte
	wParms.Max_Percent_Decelerate = Character2Float(data+index+30, 4);	// 33
	//	Min_Enable_Speed	4Byte
	wParms.Min_Enable_Speed = Character2Float(data+index+34, 4);	// 37

	//	Max_Output_dec	4Byte
	wParms.Max_Output_dec = Character2Float(data+index+38, 4);	// 137

	//	Ratio_Force_To_Deceleration	4Byte
	wParms.Ratio_Force_To_Deceleration = Character2Float(data+index+42, 4);	// 41
	//	CMS_HMW_Brake_Time_Thr	4Byte
	wParms.CMS_HMW_Brake_Time_Thr = Character2Float(data+index+46, 4);	// 45
	//	CMS_HMW_Brake_Force_Feel_Para	4Byte
	wParms.CMS_HMW_Brake_Force_Feel_Para = Character2Float(data+index+50, 4);	// 49
	//	CMS_HMW_Warning_Time_Thr	4Byte
	wParms.CMS_HMW_Warning_Time_Thr = Character2Float(data+index+54, 4);	// 53

	// CMS_HMW_Time_Offset_Night
	wParms.CMS_HMW_Time_Offset_Night = Character2Float(data+index+58, 4);
	// CMS_HMW_Time_Offset_Night_StartT
	wParms.CMS_HMW_Time_Offset_Night_StartT = Character2Float(data+index+62, 4);
	// CMS_HMW_Time_Offset_Night_EndT
	wParms.CMS_HMW_Time_Offset_Night_EndT = Character2Float(data+index+66, 4);

	//	CMS_TTC_Brake_Time_Thr	4Byte
	wParms.CMS_TTC_Brake_Time_Thr = Character2Float(data+index+70, 4);	// 73
	//	CMS_TTC_Brake_Force_Feel_Para	4Byte
	wParms.CMS_TTC_Brake_Force_Feel_Para = Character2Float(data+index+74, 4);	// 77
	//	CMS_TTC_Warning_Time_Level_First	4Byte
	wParms.CMS_TTC_Warning_Time_Level_First = Character2Float(data+index+78, 4);	// 81
	//	CMS_TTC_Warning_Time_Level_Second	4Byte
	wParms.CMS_TTC_Warning_Time_Level_Second = Character2Float(data+index+82, 4);	// 85
	//	AEB_Decelerate_Set	4Byte
	wParms.AEB_Decelerate_Set = Character2Float(data+index+86, 4);	// 89
	//	AEB_Stop_Distance	4Byte
	wParms.AEB_Stop_Distance = Character2Float(data+index+90, 4);	// 93
	//	AEB_TTC_Warning_Time_Level_First	4Byte
	wParms.AEB_TTC_Warning_Time_Level_First = Character2Float(data+index+94, 4);	// 97
	//	AEB_TTC_Warning_Time_Level_Second	4Byte
	wParms.AEB_TTC_Warning_Time_Level_Second = Character2Float(data+index+98, 4);	// 101
	//	SSS_Brake_Force	4Byte
	wParms.SSS_Brake_Force = Character2Float(data+index+102, 4);	// 105
	//	SSS_Break_Enable_Distance	4Byte
	wParms.SSS_Break_Enable_Distance = Character2Float(data+index+106, 4);	// 109
	//	SSS_Warning_Enable_Distance	4Byte
	wParms.SSS_Warning_Enable_Distance = Character2Float(data+index+110, 4);	// 113
	//	SSS_Max_Enable_Speed	4Byte
	wParms.SSS_Max_Enable_Speed = Character2Float(data+index+114, 4);	// 117
	//	SSS_FR_FL_Install_Distance_To_Side	4Byte
	wParms.SSS_FR_FL_Install_Distance_To_Side = Character2Float(data+index+118, 4);	// 121
	//	SSS_Stop_Distance	4Byte
	wParms.SSS_Stop_Distance = Character2Float(data+index+122, 4);	// 125
	//	SSS_Default_Turn_Angle	4Byte
	wParms.SSS_Default_Turn_Angle = Character2Float(data+index+126, 4);	// 129
	//	WheelSpeed_Coefficient	4Byte
	wParms.WheelSpeed_Coefficient = Character2Float(data+index+130, 4);	// 133

	//	PWM_magnetic_valve_T	1Byte
	wParms.PWM_magnetic_valve_T = data[index+134];
	//	PWM_magnetic_valve_N	1Byte
	wParms.PWM_magnetic_valve_N = data[index+135];
	//	PWM_magnetic_valve_Switch 	1Byte
	wParms.PWM_magnetic_valve_Switch = data[index+136];
	//	PWM_magnetic_valve_Scale	1Byte
	wParms.PWM_magnetic_valve_Scale = data[index+137];

	//	Vehicle_Width	4Byte
	wParms.Vechile_Width = Character2Float(data+index+138, 4);	// 145
	//	Vehicle_Speed_Obtain_Method	1Byte
	wParms.Vehicle_Speed_Obtain_Method = data[index+142];
	//	Vehicle_State_Obtain_Method 1Byte
	wParms.Vehicle_State_Obtain_Method = data[index+143];
	//	Vehicle_Brake_Control_Mode	1Byte
//	wParms.Vehicle_Brake_Control_Mode = data[index+141];

	////////////////////////////////////////////////////////
	/////////// CAN 波特率
	////////////////////////////////////////////////////////
	//	CAN_0_Baud_Rate
	wParms.CAN_0_Baud_Rate = data[index+145]<<8 | data[index+144];
	//	CAN_1_Baud_Rate
	wParms.CAN_1_Baud_Rate = data[index+147]<<8 | data[index+146];
	//	CAN_2_Baud_Rate
	wParms.CAN_2_Baud_Rate = data[index+149]<<8 | data[index+148];
	//	CAN_3_Baud_Rate
	wParms.CAN_3_Baud_Rate = data[index+151]<<8 | data[index+150];
	//	CAN_4_Baud_Rate
	wParms.CAN_4_Baud_Rate = data[index+153]<<8 | data[index+152];
	//	CAN_5_Baud_Rate
	wParms.CAN_5_Baud_Rate = data[index+155]<<8 | data[index+154];

#if DEBUG_VEHICLE_SRR_CFG_ASCII
	////////////////////////////////////////////////////////
	/////////// 车身配置
	////////////////////////////////////////////////////////
	struct VehicleParams vehilce_params = {0};
	vehilce_params.width = Character2Float(data+index+156, 4);	// 159
	vehilce_params.length = Character2Float(data+index+160, 4);	// 163
	vehilce_params.height = Character2Float(data+index+164, 4);	// 167
	vehilce_params.front_to_rear_axle = Character2Float(data+index+168, 4);	// 171
//	USART1_Bebug_Print_Flt("width:", vehilce_params.width , 1, 1);
//	USART1_Bebug_Print_Flt("length:", vehilce_params.length , 1, 1);
//	USART1_Bebug_Print_Flt("height:", vehilce_params.height , 1, 1);
//	USART1_Bebug_Print_Flt("front_to_rear_axle:", vehilce_params.front_to_rear_axle , 1, 1);

	////////////////////////////////////////////////////////
	/////////// 角雷达配置
	////////////////////////////////////////////////////////
	struct SrrParams srr_params = {0};
	srr_params.braking_time = Character2Float(data+index+172, 4);	// 175
	srr_params.braking_force = Character2Float(data+index+176, 4);	// 179
	srr_params.dist_to_vehicle_front = Character2Float(data+index+180, 4);	// 183
	srr_params.dist_to_vehicle_center = Character2Float(data+index+184, 4);	// 187
//	USART1_Bebug_Print_Flt("braking_time:", srr_params.braking_time , 1, 1);
//	USART1_Bebug_Print_Flt("braking_force:", srr_params.braking_force , 1, 1);
//	USART1_Bebug_Print_Flt("dist_to_vehicle_front:", srr_params.dist_to_vehicle_front , 1, 1);
//	USART1_Bebug_Print_Flt("dist_to_vehicle_center:", srr_params.dist_to_vehicle_center , 1, 1);
#endif

	// 打印输出
//	fprintf(USART1_STREAM, "平台设置配置参数\r\n");
//	Print_AEB_Config_Param_ASCII(wParms);
	// 写操作
	if(Write_AEB_Config_Param(wParms)){
		rParms = wParms;
//		USART1_Bebug_Print("Write OK", "Write Config Parameter OK.", 1);
#if DEBUG_VEHICLE_SRR_CFG_ASCII
		// 写车身配置和角雷达配置
		VehParam_Set_At_Params(&vehilce_params);
		SrrParam_Set_At_Params(&srr_params);
		if(SrrParam_SyncExternFlash() && VehParam_SyncExternFlash()){
			return true;
		}else{
			USART1_Bebug_Print("Write Failed", "Write Vehicle And SRR Configure Information.", 1);
			return false;
		}
#else
		return true;
#endif
	}else{
		USART1_Bebug_Print("Write FAILED", "Write Config Parameter Failed.", 1);
		// 加载默认参数
		Read_AEB_Config_Param(&wParms);
	}

	return false;
}

/*
 * 返回后，下标从29开始计数
 */
bool Request_Protocal_Head_ASCII(uint16_t seqNum, uint8_t cmdCode, uint8_t *data)
{
	// 协议头
	data[0] = 0xFA;
	data[1] = 0x55;
	// 序号
	data[2] = seqNum;
	data[3] = seqNum>>8;

	// SN号
	uint8_t devSN_len = strlen(server_info.sn);
	if(devSN_len >= 10){
		for(uint8_t i=23; i>3; i--){
			if(devSN_len > 0){
				data[i] = server_info.sn[devSN_len-1];
				devSN_len--;
			}else{
				data[i] = 0;
			}
		}
	}else{
		USART1_Bebug_Print("ERROR", "Get Device SN Failed.", 1);
		return false;
	}
	// 指令
	data[24] = cmdCode;
	// 时间戳
	data[25] = g_utctime;
	data[26] = g_utctime>>8;
	data[27] = g_utctime>>16;
	data[28] = g_utctime>>24;

	return true;
}
/*
 * 返回后，下标从31开始计数
 */
bool Reply_Protocal_Head_ASCII(uint16_t seqNum, uint8_t cmdCode, uint16_t stateCode, uint8_t *data)
{
	// 协议头
	data[0] = 0xF5;
	data[1] = 0xAA;
	// 序号
	data[2] = seqNum;
	data[3] = seqNum>>8;

	// SN号
	uint8_t devSN_len = strlen(server_info.sn);
	if(devSN_len >= 10){
		for(uint8_t i=23; i>3; i--){
			if(devSN_len > 0){
				data[i] = server_info.sn[devSN_len-1];
				devSN_len--;
			}else{
				data[i] = 0;
			}
		}
	}else{
		USART1_Bebug_Print("ERROR", "Get Device SN Failed.", 1);
		return false;
	}
	// 指令码
	data[24] = cmdCode;
	// 状态码
	data[25] = stateCode;
	data[26] = stateCode>>8;
	// 时间戳
//	fprintf(USART1_STREAM, "utctime:%d\r\n", g_utctime);

	data[27] = g_utctime;
	data[28] = g_utctime>>8;
	data[29] = g_utctime>>16;
	data[30] = g_utctime>>24;

	// for(uint8_t i=0; i<31;i++){
	// 	fprintf(USART1_STREAM, "%02X ", data[i]);
	// }
	// fprintf(USART1_STREAM, "\r\n");
	return true;
}

/*
 * 询问方式
 */
bool Request_Get_AEB_Config_Param_ASCII()
{
	if(!server_info.isConnect) return false;
	static uint16_t aeb_seqNum = 1;

	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(aeb_seqNum, GET_VPARAM_FRAME, nodeData.data)) return false;

	if(aeb_seqNum > UPLOAD_MAX_SEQ_NUM) aeb_seqNum = 1;
	else aeb_seqNum ++;

	req_index = 31;

	// 内容
	uint8_t data = 0;
	Set_Request_ASCII_Info_Template(&nodeData, &data, UINT_8, 1);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, &data, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(GET_VPARAM_FRAME, aeb_seqNum);
	nodeData.is_wait_reply = NEED_WAIT_REPLY;
	nodeData.is_store = NEED_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

//	for(uint16_t i=0; i<nodeData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n");
	return true;
}

bool Send_Reply_Set_AEB_Config_Param_ASCII(uint32_t seqNum, KunLun_AEB_Para_Cfg algP)
{
	if(!server_info.isConnect) return false;

	// 填数
	NodeData_S nodeData1 = {0};
	if(!Reply_Protocal_Head_ASCII(seqNum, GET_VPARAM_FRAME, 0x00, nodeData1.data)) return false;

	req_index = 33;
	is_reply = true;

//	USART1_Bebug_Print("SEND", "Send AEB Config Parameter.");
	Set_AEB_Config_Param_ASCII(algP, nodeData1);

	is_reply = false;
	return true;
}

bool Request_Set_AEB_Config_Param_ASCII(KunLun_AEB_Para_Cfg algP)
{
	if(!server_info.isConnect) return false;

	static uint16_t set_aeb_param_seqNum = 1;

	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(set_aeb_param_seqNum, SET_VPARAM_FRAME, nodeData.data)) return false;

	req_index = 31;
	Set_AEB_Config_Param_ASCII(algP, nodeData);
	Test_Print_CMD_Seq(SET_VPARAM_FRAME, set_aeb_param_seqNum);

	if(set_aeb_param_seqNum > UPLOAD_MAX_SEQ_NUM) set_aeb_param_seqNum = 1;
	else set_aeb_param_seqNum ++;
	return true;
}

void Set_AEB_Config_Param_ASCII(KunLun_AEB_Para_Cfg algP, NodeData_S nodeData)
{
	// 内容
	char *params_version = (char *)&algP.params_version;
	Set_Request_ASCII_Info_Template(&nodeData, params_version, UINT_32, 4);

	////////////////////////////////////////////////////////
	/////////// GlobalSwitch switch_g
	////////////////////////////////////////////////////////
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.AEB_CMS_Enable, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.CMS_HMW_Enable, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.CMS_TTC_Enable, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.AEB_TTC_Enable, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.SSS_Enable, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.HMW_Dynamic_Thr_Enable, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.Chassis_Safe_Strategy_Enable, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.LDW_Enable, UINT_8, 1);

	// 添加项 20220518
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.Displayer_Switch_Enable, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.switch_g.Retract_Brake_Enable, UINT_8, 1);

	////////////////////////////////////////////////////////
	/////////// AEB、CMS common parameters
	////////////////////////////////////////////////////////
	char *comVar = (char*)&algP.Brake_Cooling_Time;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.Driver_Brake_Cooling_Time;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.Max_Brake_Keep_Time;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.Air_Brake_Delay_Time;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.Max_Percent_Decelerate;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.Min_Enable_Speed;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.Max_Output_dec;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.Ratio_Force_To_Deceleration;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	////////////////////////////////////////////////////////
	/////////// CMS parameters - HMW
	////////////////////////////////////////////////////////
	comVar = (char*)&algP.CMS_HMW_Brake_Time_Thr;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.CMS_HMW_Brake_Force_Feel_Para;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.CMS_HMW_Warning_Time_Thr;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.CMS_HMW_Time_Offset_Night;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.CMS_HMW_Time_Offset_Night_StartT;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.CMS_HMW_Time_Offset_Night_EndT;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	////////////////////////////////////////////////////////
	/////////// CMS parameters - TTC
	////////////////////////////////////////////////////////
	comVar = (char*)&algP.CMS_TTC_Brake_Time_Thr;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.CMS_TTC_Brake_Force_Feel_Para;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.CMS_TTC_Warning_Time_Level_First;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.CMS_TTC_Warning_Time_Level_Second;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	////////////////////////////////////////////////////////
	/////////// AEB
	////////////////////////////////////////////////////////
	comVar = (char*)&algP.AEB_Decelerate_Set;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.AEB_Stop_Distance;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.AEB_TTC_Warning_Time_Level_First;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.AEB_TTC_Warning_Time_Level_Second;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	////////////////////////////////////////////////////////
	/////////// SSS
	////////////////////////////////////////////////////////
	comVar = (char*)&algP.SSS_Brake_Force;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.SSS_Break_Enable_Distance;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.SSS_Warning_Enable_Distance;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.SSS_Max_Enable_Speed;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.SSS_FR_FL_Install_Distance_To_Side;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.SSS_Stop_Distance;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	comVar = (char*)&algP.SSS_Default_Turn_Angle;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	////////////////////////////////////////////////////////
	/////////// wheel speed parameter
	////////////////////////////////////////////////////////
	comVar = (char*)&algP.WheelSpeed_Coefficient;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);


	////////////////////////////////////////////////////////
	/////////// Electron megnetic valve
	////////////////////////////////////////////////////////
	Set_Request_ASCII_Info_Template(&nodeData, &algP.PWM_magnetic_valve_T, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.PWM_magnetic_valve_N, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.PWM_magnetic_valve_Switch, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &algP.PWM_magnetic_valve_Scale, UINT_8, 1);

	////////////////////////////////////////////////////////
	/////////// Vehicle parameters
	////////////////////////////////////////////////////////
	comVar = (char*)&algP.Vechile_Width;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	// Vehicle_Speed_Obtain_Method
	Set_Request_ASCII_Info_Template(&nodeData, &algP.Vehicle_Speed_Obtain_Method, UINT_8, 1);

	// Vehicle_State_Obtain_Method
	Set_Request_ASCII_Info_Template(&nodeData, &algP.Vehicle_State_Obtain_Method, UINT_8, 1);
	comVar = NULL;

	////////////////////////////////////////////////////////
	/////////// CAN 波特率
	////////////////////////////////////////////////////////
	//	CAN_0_Baud_Rate
	comVar = (char *)&algP.CAN_0_Baud_Rate;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, UINT_16, 2);
	//	CAN_1_Baud_Rate
	comVar = (char *)&algP.CAN_1_Baud_Rate;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, UINT_16, 2);
	//	CAN_2_Baud_Rate
	comVar = (char *)&algP.CAN_2_Baud_Rate;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, UINT_16, 2);
	//	CAN_3_Baud_Rate
	comVar = (char *)&algP.CAN_3_Baud_Rate;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, UINT_16, 2);
	//	CAN_4_Baud_Rate
	comVar = (char *)&algP.CAN_4_Baud_Rate;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, UINT_16, 2);
	//	CAN_5_Baud_Rate
	comVar = (char *)&algP.CAN_5_Baud_Rate;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, UINT_16, 2);

#if DEBUG_VEHICLE_SRR_CFG_ASCII
	////////////////////////////////////////////////////////
	/////////// 车身配置 +VehicleWidth
	////////////////////////////////////////////////////////
	struct VehicleParams *vehicle_params = VehParam_GetParams();
	comVar = (char*)&vehicle_params->width;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);
	// vehicle_Length
	comVar = (char*)&vehicle_params->length;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);
	// vehicle_Height
	comVar = (char*)&vehicle_params->height;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);
	// vehicle_FromtToRearAxle
	comVar = (char*)&vehicle_params->front_to_rear_axle;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);

	////////////////////////////////////////////////////////
	/////////// 角雷达配置
	////////////////////////////////////////////////////////
	struct SrrParams *srr_params =  SrrParam_GetParams();
	// srr_BrakingTime
	comVar = (char*)&srr_params->braking_time;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);
	// srr_BrakingForce
	comVar = (char*)&srr_params->braking_force;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);
	// srr_DistToVehicleFront
	comVar = (char*)&srr_params->dist_to_vehicle_front;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);
	// srr_DistToVehicleCenter
	comVar = (char*)&srr_params->dist_to_vehicle_center;
	Set_Request_ASCII_Info_Template(&nodeData, comVar, FLOAT, 4);
#endif

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, comVar, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	if(is_reply) nodeData.is_wait_reply = NEED_WAIT_REPLY_REPLY;
	else nodeData.is_wait_reply = NEED_WAIT_REPLY;
	nodeData.is_store = NEED_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

//	USART1_Bebug_Print_Num("nodeData.data_len", nodeData.data_len, 1, 1);
//	for(uint16_t i=0; i<nodeData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n");
}

/*
 * 共有回复信息解析
 * 包括：GPS、设备车身、车辆设备、预警信息
 */
void Analysis_Reply_Common_Info_ASCII(uint8_t *data, uint16_t contentLen)
{
	// 内容
	if(contentLen == 1){
		if(data[0] == 1){
			if(upgrade_info.step == RESPONSE)
				_4g_message_state = _4G_UPGRADE_OK;
		}else{
			USART1_Bebug_Print("ERROR", "Common Info Value Is Not 1.", 1);
		}
		// 接收到数据，服务器在线
		server_info.offline_times = 0;
	}else{
		USART1_Bebug_Print("ERROR", "Common Info Len Is Not 1.", 1);
	}
}

volatile uint8_t reply_request_flag = 0;		// 1:reply;2:request
volatile uint16_t head_addr = 0;
bool _4G_Recv_Platform_Interaction_ASCII_Message(uint8_t *string,uint32_t length)//gcz 2022-06-02 增加参数：数据长度的输入
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return false;

//	if(upgrade_info.step == UPGRADE_SUB_DMN) return ;
//	if(upgrade_info.step == UPGRADE_URL) return ;
	// 解析平台返回信息
	for(uint16_t i=0; i < length; i++){

		// 解析平台被动反馈消息
		if(string[i]==0xF5 && string[i+1]==0xAA){	// analysis reply
			head_addr = i;
			reply_request_flag = 1;
		}

		// 解析平台主动下发信息
		if(string[i]==0xFA && string[i+1]==0x55){	// analysis request
			head_addr = i;
			reply_request_flag = 2;
		}

		if(head_addr > 0 && reply_request_flag == 0) return false;

		// 解析数据
		if(reply_request_flag == 1){				// analysis reply 平台被动反馈消息
			reply_request_flag = 0;

			if(Analysis_Reply_ASCII(string+head_addr, length - head_addr)){
				goto BY_PARSING;
			}
		}
		else if(reply_request_flag ==2){			// analysis request 平台主动下发信息
			reply_request_flag = 0;

			if(Analysis_Request_ASCII(string+head_addr, length - head_addr)){
				goto BY_PARSING;
			}
		}
	}
	Enable_Usart_Interrupt(USART0_SFR, INT_USART0);	// 开中断		 gcz 2022-06-03去除
	return false;

BY_PARSING:
	Enable_Usart_Interrupt(USART0_SFR, INT_USART0);	// 开中断		 gcz 2022-06-03去除
	return true;
}

volatile uint8_t debug_4g_info_times 	= 0;
uint8_t bebug_4g_info_buf[200] = {0};
bool _4g_Recv_USART1_Send_Debug_CMD_Message(uint8_t *string)
{
	// 若通过指令查询4G模块的信息，捕获打印
	if(debug_get_4g_info){
		if(debug_4g_info_times > 4){
			goto DEBUG_PRINT;
		}else {
			uint8_t len = strlen(bebug_4g_info_buf);

			if(len >= 100){
				goto DEBUG_PRINT;
			}else{
				memcpy(bebug_4g_info_buf + len, string, strlen(string));
			}

			debug_4g_info_times++;
		}
	}
	return false;

DEBUG_PRINT:
	debug_4g_info_times = 0;
	debug_get_4g_info 	= 0;

	// 打印输出信息
	USART1_Bebug_Print("GET 4G INFO", bebug_4g_info_buf, 1);
	memset(bebug_4g_info_buf, 0, 100);

	// 开USART0中断
	Enable_Usart_Interrupt(USART0_SFR, INT_USART0);		// 开中断		 gcz 2022-06-03去除

	return true;
}
/*
 * 在USART0 中断函数中（4G模块通讯）
 * 参数为已收到的以结束符结尾的字符串
 */
bool _4G_Message_Handle_Function(uint8_t *string,uint32_t length)//gcz 2022-06-02 增加参数：数据长度的输入
{
	if(_4G_Recv_Data_Upload_Message_From_Platform_Reply(string)){
		goto MESSAGE_END;
	}
	// 连接服务器解析
	if(_4G_Recv_Connect_Server_Message(string)){
		goto MESSAGE_END;
	}

	// OTA升级解析
	if(_4G_Recv_OTA_Message(string)){
		goto MESSAGE_END;
	}

	// 与平台信息交互信息解析 	//gcz 2022-06-02 增加参数：数据长度的输入
	if(_4G_Recv_Platform_Interaction_ASCII_Message(string,length)){
		goto MESSAGE_END;
	}

	// GPS信息解析				//gcz 2022-06-03 增加参数：数据长度的输入
	if(_4G_Recv_GPS_Message(string,length)){
		goto MESSAGE_END;
	}

	// 4G信号强度解析			//gcz 2022-06-03 增加参数：数据长度的输入
	if(_4G_Recv_Signal_Intensity_Message(string,length)){
		goto MESSAGE_END;
	}

	// 异常调试信息
	if(_4G_Recv_Not_Caught_Exception(string)){
		goto MESSAGE_END;
	}

	if(_4g_Recv_USART1_Send_Debug_CMD_Message(string)){
		goto MESSAGE_END;
	}

MESSAGE_END:
	return true;
}

bool _4G_Recv_Not_Caught_Exception(uint8_t *string)
{
	// +QISEND:
	if(strstr((const char*)string, (const char*)"+QISEND:")){
		USART1_Bebug_Print("EXCEP", string, 1);
		return true;
	}

	// +QIOPEN:
	if(strstr((const char*)string, (const char*)"+QIOPEN:")){
		USART1_Bebug_Print("EXCEP", string, 1);
		return true;
	}

	// +QIURC:
//	if(strstr((const char*)string, (const char*)"+QIURC:")){
//		fprintf(USART1_STREAM, "%s\r\n", string);
//		return true;
//	}
	return false;
}

bool Analysis_Reply_AEB_Config_Param_ASCII(uint8_t *data, uint16_t dataLen)
{
	uint8_t index = 0;
	// 内容
	//	AEB_CMS_Enable 1Byte
	wParms.switch_g.AEB_CMS_Enable = data[index+4];
	//	CMS_HMW_Enable 1Byte
	wParms.switch_g.CMS_HMW_Enable = data[index+5];
	//	CMS_TTC_Enable 1Byte
	wParms.switch_g.CMS_TTC_Enable = data[index+6];
	//	AEB_TTC_Enable 1Byte
	wParms.switch_g.AEB_TTC_Enable = data[index+7];
	//	SSS_Enable	 1Byte
	wParms.switch_g.SSS_Enable = data[index+8];
	//	HMW_Dynamic_Thr_Enable 1Byte
	wParms.switch_g.HMW_Dynamic_Thr_Enable = data[index+9];
	//	Chassis_Safe_Strategy_Enable 1Byte
	wParms.switch_g.Chassis_Safe_Strategy_Enable = data[index+10];
	//	LDW_Enable 	1Byte
	wParms.switch_g.LDW_Enable = data[index+11];

	wParms.switch_g.Displayer_Switch_Enable = data[index+12];
	wParms.switch_g.Retract_Brake_Enable = data[index+13];

	//	Brake_Cooling_Time 4Byte
	wParms.Brake_Cooling_Time = Character2Float(data+index+14, 4);	// 17
	//	Driver_Brake_Cooling_Time	4Byte
	wParms.Driver_Brake_Cooling_Time = Character2Float(data+index+18, 4);	// 21
	//	Max_Brake_Keep_Time		4Byte
	wParms.Max_Brake_Keep_Time = Character2Float(data+index+22, 4);	// 25
	//	Air_Brake_Delay_Time	4Byte
	wParms.Air_Brake_Delay_Time = Character2Float(data+index+26, 4);	// 29
	//	Max_Percent_Decelerate	4Byte
	wParms.Max_Percent_Decelerate = Character2Float(data+index+30, 4);	// 33
	//	Min_Enable_Speed	4Byte
	wParms.Min_Enable_Speed = Character2Float(data+index+34, 4);	// 37

	//	Max_Output_dec	4Byte
	wParms.Max_Output_dec = Character2Float(data+index+38, 4);	// 137

	//	Ratio_Force_To_Deceleration	4Byte
	wParms.Ratio_Force_To_Deceleration = Character2Float(data+index+42, 4);	// 41
	//	CMS_HMW_Brake_Time_Thr	4Byte
	wParms.CMS_HMW_Brake_Time_Thr = Character2Float(data+index+46, 4);	// 45
	//	CMS_HMW_Brake_Force_Feel_Para	4Byte
	wParms.CMS_HMW_Brake_Force_Feel_Para = Character2Float(data+index+50, 4);	// 49
	//	CMS_HMW_Warning_Time_Thr	4Byte
	wParms.CMS_HMW_Warning_Time_Thr = Character2Float(data+index+54, 4);	// 53

	// CMS_HMW_Time_Offset_Night
	wParms.CMS_HMW_Time_Offset_Night = Character2Float(data+index+58, 4);
	// CMS_HMW_Time_Offset_Night_StartT
	wParms.CMS_HMW_Time_Offset_Night_StartT = Character2Float(data+index+62, 4);
	// CMS_HMW_Time_Offset_Night_EndT
	wParms.CMS_HMW_Time_Offset_Night_EndT = Character2Float(data+index+66, 4);

	//	CMS_TTC_Brake_Time_Thr	4Byte
	wParms.CMS_TTC_Brake_Time_Thr = Character2Float(data+index+70, 4);	// 73
	//	CMS_TTC_Brake_Force_Feel_Para	4Byte
	wParms.CMS_TTC_Brake_Force_Feel_Para = Character2Float(data+index+74, 4);	// 77
	//	CMS_TTC_Warning_Time_Level_First	4Byte
	wParms.CMS_TTC_Warning_Time_Level_First = Character2Float(data+index+78, 4);	// 81
	//	CMS_TTC_Warning_Time_Level_Second	4Byte
	wParms.CMS_TTC_Warning_Time_Level_Second = Character2Float(data+index+82, 4);	// 85
	//	AEB_Decelerate_Set	4Byte
	wParms.AEB_Decelerate_Set = Character2Float(data+index+86, 4);	// 89
	//	AEB_Stop_Distance	4Byte
	wParms.AEB_Stop_Distance = Character2Float(data+index+90, 4);	// 93
	//	AEB_TTC_Warning_Time_Level_First	4Byte
	wParms.AEB_TTC_Warning_Time_Level_First = Character2Float(data+index+94, 4);	// 97
	//	AEB_TTC_Warning_Time_Level_Second	4Byte
	wParms.AEB_TTC_Warning_Time_Level_Second = Character2Float(data+index+98, 4);	// 101
	//	SSS_Brake_Force	4Byte
	wParms.SSS_Brake_Force = Character2Float(data+index+102, 4);	// 105
	//	SSS_Break_Enable_Distance	4Byte
	wParms.SSS_Break_Enable_Distance = Character2Float(data+index+106, 4);	// 109
	//	SSS_Warning_Enable_Distance	4Byte
	wParms.SSS_Warning_Enable_Distance = Character2Float(data+index+110, 4);	// 113
	//	SSS_Max_Enable_Speed	4Byte
	wParms.SSS_Max_Enable_Speed = Character2Float(data+index+114, 4);	// 117
	//	SSS_FR_FL_Install_Distance_To_Side	4Byte
	wParms.SSS_FR_FL_Install_Distance_To_Side = Character2Float(data+index+118, 4);	// 121
	//	SSS_Stop_Distance	4Byte
	wParms.SSS_Stop_Distance = Character2Float(data+index+122, 4);	// 125
	//	SSS_Default_Turn_Angle	4Byte
	wParms.SSS_Default_Turn_Angle = Character2Float(data+index+126, 4);	// 129
	//	WheelSpeed_Coefficient	4Byte
	wParms.WheelSpeed_Coefficient = Character2Float(data+index+130, 4);	// 133

	//	PWM_magnetic_valve_T	1Byte
	wParms.PWM_magnetic_valve_T = data[index+134];
	//	PWM_magnetic_valve_N	1Byte
	wParms.PWM_magnetic_valve_N = data[index+135];
	//	PWM_magnetic_valve_Switch 	1Byte
	wParms.PWM_magnetic_valve_Switch = data[index+136];
	//	PWM_magnetic_valve_Scale	1Byte
	wParms.PWM_magnetic_valve_Scale = data[index+137];

	//	Vehicle_Width	4Byte
	wParms.Vechile_Width = Character2Float(data+index+138, 4);	// 145
	//	Vehicle_Speed_Obtain_Method	1Byte
	wParms.Vehicle_Speed_Obtain_Method = data[index+142];
	//	Vehicle_State_Obtain_Method 1Byte
	wParms.Vehicle_State_Obtain_Method = data[index+143];
	//	Vehicle_Brake_Control_Mode	1Byte
//	wParms.Vehicle_Brake_Control_Mode = data[index+141];

	////////////////////////////////////////////////////////
	/////////// CAN 波特率
	////////////////////////////////////////////////////////
	//	CAN_0_Baud_Rate
	wParms.CAN_0_Baud_Rate = data[index+145]<<8 | data[index+144];
	//	CAN_1_Baud_Rate
	wParms.CAN_0_Baud_Rate = data[index+147]<<8 | data[index+146];
	//	CAN_2_Baud_Rate
	wParms.CAN_0_Baud_Rate = data[index+149]<<8 | data[index+148];
	//	CAN_3_Baud_Rate
	wParms.CAN_0_Baud_Rate = data[index+151]<<8 | data[index+150];
	//	CAN_4_Baud_Rate
	wParms.CAN_0_Baud_Rate = data[index+153]<<8 | data[index+152];
	//	CAN_5_Baud_Rate
	wParms.CAN_0_Baud_Rate = data[index+155]<<8 | data[index+154];

#if DEBUG_VEHICLE_SRR_CFG_ASCII
	////////////////////////////////////////////////////////
	/////////// 车身配置
	////////////////////////////////////////////////////////
	struct VehicleParams vehilce_params = {0};
	vehilce_params.width = Character2Float(data+index+156, 4);	// 159
	vehilce_params.length = Character2Float(data+index+160, 4);	// 163
	vehilce_params.height = Character2Float(data+index+164, 4);	// 167
	vehilce_params.front_to_rear_axle = Character2Float(data+index+168, 4);	// 171
//	USART1_Bebug_Print_Flt("width:", vehilce_params.width , 1, 1);
//	USART1_Bebug_Print_Flt("length:", vehilce_params.length , 1, 1);
//	USART1_Bebug_Print_Flt("height:", vehilce_params.height , 1, 1);
//	USART1_Bebug_Print_Flt("front_to_rear_axle:", vehilce_params.front_to_rear_axle , 1, 1);

	////////////////////////////////////////////////////////
	/////////// 角雷达配置
	////////////////////////////////////////////////////////
	struct SrrParams srr_params = {0};
	srr_params.braking_time = Character2Float(data+index+172, 4);	// 175
	srr_params.braking_force = Character2Float(data+index+176, 4);	// 179
	srr_params.dist_to_vehicle_front = Character2Float(data+index+180, 4);	// 183
	srr_params.dist_to_vehicle_center = Character2Float(data+index+184, 4);	// 187
//	USART1_Bebug_Print_Flt("braking_time:", srr_params.braking_time , 1, 1);
//	USART1_Bebug_Print_Flt("braking_force:", srr_params.braking_force , 1, 1);
//	USART1_Bebug_Print_Flt("dist_to_vehicle_front:", srr_params.dist_to_vehicle_front , 1, 1);
//	USART1_Bebug_Print_Num("dist_to_vehicle_center:", srr_params.dist_to_vehicle_center , 1, 1);
#endif

	// 打印输出
	Print_AEB_Config_Param_ASCII(wParms);
	// 先备份，写失败，就恢复
	KunLun_AEB_Para_Cfg l_alg_cfg_param_t = {0};
	Read_AEB_Config_Param(&l_alg_cfg_param_t);

	// 写操作
	if(Write_AEB_Config_Param(wParms)){
		USART1_Bebug_Print("Write OK", "Write Config Parameter OK.", 1);
#if DEBUG_VEHICLE_SRR_CFG_ASCII
		// 写车身配置和角雷达配置
		VehParam_Set_At_Params(&vehilce_params);
		SrrParam_Set_At_Params(&srr_params);
		if(SrrParam_SyncExternFlash() && VehParam_SyncExternFlash()){
			return true;
		}else{
			USART1_Bebug_Print("Write Failed", "Write Vehicle And SRR Configure Information.", 1);
			return false;
		}
#else
		return true;
#endif
	}else{
		USART1_Bebug_Print("Write FAILED", "Write Config Parameter Failed.", 1);

		// 恢复原来参数
		wParms = l_alg_cfg_param_t;
		Write_AEB_Config_Param(l_alg_cfg_param_t);
	}

	return false;
}

/*
 * 主动上报信息
 * 说明：车辆类型、配置参数、GPS上报的时间间隔、传感器配置参数
 */
void Request_Vehicle_Config_Param_ASCII()
{
	// 上报车型信息 和 配置参数信息
	Request_Set_Vehicle_Type_ASCII(server_info.vehicle_type);	// 车型信息
	Request_Set_AEB_Config_Param_ASCII(rParms);					// 配置参数

	// 获取GPS时间间隔
	uint8_t interval = Get_GPS_Interval();
	Request_Set_Gps_Interval_ASCII(interval);					// GPS上报的时间间隔

//	Request_Sensor_Cfg_Param_Info_ASCII();						// 传感器配置参数
//	Request_Get_Car_Can_Analysis_Cfg_Param_Info_ASCII();		// 车CAN解析配置
}

bool Request_Set_Vehicle_Type_ASCII(uint8_t vehicle_type)
{
	if(!server_info.isConnect) return false;

	static uint16_t vehicle_seqNum = 1;

	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(vehicle_seqNum, SET_VTYPE_FRAME, nodeData.data)) return false;

	if(vehicle_seqNum > UPLOAD_MAX_SEQ_NUM) vehicle_seqNum = 1;
	else vehicle_seqNum ++;

	req_index = 31;

	// 车辆类型
	Set_Request_ASCII_Info_Template(&nodeData, &vehicle_type, UINT_8, 1);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, &vehicle_type, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(SET_VTYPE_FRAME, vehicle_seqNum);
	nodeData.is_wait_reply = NEED_WAIT_REPLY;
	nodeData.is_store = NEED_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

	return true;
}

bool Request_Get_Vehicle_Type_ASCII()
{
	if(!server_info.isConnect) return false;

	static uint16_t vehicle_seqNum = 1;

	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(vehicle_seqNum, GET_VTYPE_FRAME, nodeData.data)) return false;

	if(vehicle_seqNum > UPLOAD_MAX_SEQ_NUM) vehicle_seqNum = 1;
	else vehicle_seqNum ++;

	req_index = 31;

	// 内容 0
	uint8_t data = 0;
	Set_Request_ASCII_Info_Template(&nodeData, &data, UINT_8, 1);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, &data, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(GET_VTYPE_FRAME, vehicle_seqNum);
	nodeData.is_wait_reply = NEED_WAIT_REPLY;
	nodeData.is_store = NEED_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

//	for(uint16_t i=0; i<nodeData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n");

	return true;
}


/*
 * 回复平台获取设备软硬件相关信息
 * 指令码：0xC3
 */
bool Reply_Device_Soft_Hardware_Info_ASCII(uint32_t seqNum)
{
	NodeData_S nodeData_dev = {0};
	memset(nodeData_dev.data_info, 0, NODE_DATAINFO_SIZE);
	memset(nodeData_dev.data, 0, NODE_DATA_SIZE);
	nodeData_dev.data_len = 0;

	req_index = 33;
	is_reply = true;
	// 帧头 0xAA55 C2
	if(!Reply_Protocal_Head_ASCII(seqNum, GET_DSHINFO_FRAME, 0x00, nodeData_dev.data)) return false;

	////////////////////////////////////////////////////////
	/////////// 设备中所有模组的版本号  device_version_str
	////////////////////////////////////////////////////////
	//	AEBS_SW_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_SW_Version, CHAR, SH_INFO_SIZE);
	//	AEBS_SN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_SN, CHAR, SH_INFO_SIZE);
	//	AEBS_PN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_PN, CHAR, SH_INFO_SIZE);
	//	Camera_SW_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_SW_Version, CHAR, SH_INFO_SIZE);
	//	Camera_HW_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_HW_Version, CHAR, SH_INFO_SIZE);
	//	Camera_SN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_SN, CHAR, SH_INFO_SIZE);
	//	Camera_PN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_PN, CHAR, SH_INFO_SIZE);
	//	Displayer_SW_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Displayer_SW_Version, CHAR, SH_INFO_SIZE);
	//	Displayer_SN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Displayer_SN, CHAR, SH_INFO_SIZE);
	//	Displayer_PN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Displayer_PN, CHAR, SH_INFO_SIZE);
	//	URadar_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.URadar_Version, CHAR, SH_INFO_SIZE);
	//	MRadar_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.MRadar_Version, CHAR, SH_INFO_SIZE);
	//	Camera_CAN_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_CAN_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	AEBS_AT_PC_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_AT_PC_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	AEBS_AT_BT_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_AT_BT_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	AEBS_CAN_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_CAN_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	Displayer_CAN_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Displayer_CAN_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	Cloud_Platform_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Cloud_Platform_ProtocoI_Version, CHAR, SH_INFO_SIZE);

	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Cloud_Platform_ProtocoI_Version, CRC16, 2);

	is_reply = false;

	// 放到队列中进行发送
	sprintf(nodeData_dev.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData_dev.data_len);

//	if(nodeData_dev.data_len != 0){
//		fprintf(USART1_STREAM, "%d, 2> ", nodeData_dev.data_len);
//		for(uint16_t i=0; i<nodeData_dev.data_len; i++){
//			fprintf(USART1_STREAM, "%02X ", nodeData_dev.data[i]);
//		}
//		fprintf(USART1_STREAM, "\r\n");
//	}
	Test_Print_CMD_Seq(GET_DSHINFO_FRAME, seqNum);
	nodeData_dev.is_wait_reply = NEED_WAIT_REPLY;
	nodeData_dev.is_store = NEED_STORED;
	nodeData_dev.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData_dev);

	return true;
}

/*
 * 上报设备软硬件相关信息
 * 指令码：0xC2
 */
bool Request_Device_Soft_Hardware_Info_ASCII()
{
	NodeData_S nodeData_dev = {0};
	memset(nodeData_dev.data_info, 0, NODE_DATAINFO_SIZE);
	memset(nodeData_dev.data, 0, NODE_DATA_SIZE);
	nodeData_dev.data_len = 0;

	static uint32_t devSHInfo_seqNum = 1;
	// 帧头 0xAA55 C2
	if(!Request_Protocal_Head_ASCII(devSHInfo_seqNum, UPLOAD_DSHINFO_FRAME, nodeData_dev.data)) return false;

	devSHInfo_seqNum++;

	req_index = 31;

	////////////////////////////////////////////////////////
	/////////// 设备中所有模组的版本号  device_version_str
	////////////////////////////////////////////////////////
	//	AEBS_SW_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_SW_Version, CHAR, SH_INFO_SIZE);
	//	AEBS_SN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_SN, CHAR, SH_INFO_SIZE);
	//	AEBS_PN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_PN, CHAR, SH_INFO_SIZE);
	//	Camera_SW_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_SW_Version, CHAR, SH_INFO_SIZE);
	//	Camera_HW_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_HW_Version, CHAR, SH_INFO_SIZE);
	//	Camera_SN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_SN, CHAR, SH_INFO_SIZE);
	//	Camera_PN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_PN, CHAR, SH_INFO_SIZE);
	//	Displayer_SW_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Displayer_SW_Version, CHAR, SH_INFO_SIZE);
	//	Displayer_SN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Displayer_SN, CHAR, SH_INFO_SIZE);
	//	Displayer_PN
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Displayer_PN, CHAR, SH_INFO_SIZE);
	//	URadar_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.URadar_Version, CHAR, SH_INFO_SIZE);
	//	MRadar_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.MRadar_Version, CHAR, SH_INFO_SIZE);
	//	Camera_CAN_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Camera_CAN_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	AEBS_AT_PC_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_AT_PC_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	AEBS_AT_BT_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_AT_BT_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	AEBS_CAN_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.AEBS_CAN_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	Displayer_CAN_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Displayer_CAN_ProtocoI_Version, CHAR, SH_INFO_SIZE);
	//	Cloud_Platform_ProtocoI_Version
	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Cloud_Platform_ProtocoI_Version, CHAR, SH_INFO_SIZE);

	Set_Request_ASCII_Info_Template(&nodeData_dev, device_version_str.Cloud_Platform_ProtocoI_Version, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData_dev.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData_dev.data_len);
	Test_Print_CMD_Seq(UPLOAD_DSHINFO_FRAME, devSHInfo_seqNum);
	nodeData_dev.is_wait_reply = NEED_WAIT_REPLY;
	nodeData_dev.is_store = NEED_STORED;
	nodeData_dev.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData_dev);

	return true;
}

char * debug_strx = NULL;
uint8_t Debug_Triggle_CMD()
{
	// 上报设置车辆类型
	if((debug_strx = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*VTYPE"))){
		StrtokStr str = StrtokString(debug_strx + 14);
		debug_strx = NULL;

		if(str.cnt == 1){
			 uint8_t vehicleType = atoi(str.data[0]);

			if(vehicleType >= 1 && vehicleType <= 101){
				USART1_Bebug_Print_Num("[>>]Set Vehicle Type", vehicleType, 1, 1);
				Request_Set_Vehicle_Type_ASCII(vehicleType);
				return 0;
			}
		}
		USART1_Bebug_Print("ERROR", "Valid Range:[1, 101].", 1);
	}else
	// 上报获取车辆类型
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYGET*VTYPE")){
		Request_Get_Vehicle_Type_ASCII();
		USART1_Bebug_Print(">>", "Get Vehicle Type.", 1);
	}else
	// 上报设置AEB配置参数
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*DPARAM")){
		KunLun_AEB_Para_Cfg algP;
		if(Read_AEB_Config_Param(&algP)){
			Print_AEB_Config_Param_ASCII(wParms);

			Request_Set_AEB_Config_Param_ASCII(wParms);
			USART1_Bebug_Print(">>", "Set AEB Parameters.", 1);
		}
	}else
	// 上报获取AEB配置参数
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYGET*DPARAM")){
		Request_Get_AEB_Config_Param_ASCII();
		USART1_Bebug_Print(">>", "Get AEB Parameters.", 1);
	}else
	// 上报设备故障和状态码
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*SHINFO")){

		Request_Device_Soft_Hardware_Info_ASCII();
		USART1_Bebug_Print(">>", "Set Device Software And Hardware Information.", 1);
	}else
	// 加载默认参数
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*AEB_DEFAULT_PARA")){
		Load_And_Write_AEB_Default_Para();
		Write_AEB_Config_Param(wParms);
	}else
	// 打开所有中断
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*IT_OPEN")){
		Enable_Usart_Interrupt(USART0_SFR, INT_USART0);		// 开中断
		USART1_Bebug_Print(">>", "Open ALL Interrupt.", 1);
	}else
	// 关闭所有中断
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*IT_CLOSE")){
		Disable_Usart_Interrupt(USART0_SFR, INT_USART0);	// 关中断
		USART1_Bebug_Print(">>", "Close ALL Interrupt.", 1);
	}else
	// 测试
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*DLP_ON")){
		server_info.isConnect = 0;
		USART1_Bebug_Print(">>", "Enable DLP Function.", 1);
	}else
	// 测试
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*DLP_OFF")){
		server_info.isConnect = 1;
		USART1_Bebug_Print(">>", "Disable DLP Function.", 1);
	}else
		// 查看数据防丢失的id
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYGET*DLP_ID")){
		uint8_t id_s[DLP_ID_SIZE] = {0};
		if(Get_Data_Loss_Prevention_ID(id_s)){
			USART1_Bebug_Print("DLP ID Is ", id_s, 1);
		}else{
			USART1_Bebug_Print("ERROR", "Get DLP ID Failed.", 1);
		}
	}else
	// 测试上报传感器参数配置0xC5
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYTRIG*SENSOR_CFG")){
		Request_Sensor_Cfg_Param_Info_ASCII();
		USART1_Bebug_Print(">>", "Trigger Sensor Upload.", 1);
	}else
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYTGET*CARCAN_CFG")){
		Debug_Print_Car_Can_Analysis_Info();
//		USART1_Bebug_Print(">>", "Print CAR_CAN_Analysis Params.", 1);
	}
}

// 直接存放数据，无需计算下标值，
void Set_Request_ASCII_Info_Template(NodeData_S *data_s, uint8_t *writeData, Write_Type_E dataType, uint8_t dataLen)
{
	switch(dataType){
	case FILL_0:{
		for(uint8_t i=0; i<dataLen; i++){
			*(data_s->data+req_index+i) = 0;
		}
		req_index += dataLen;
	}break;
	case CHAR:{	// 行程ID号和secret、SN
		uint8_t len = strlen(writeData);
		for(uint16_t i=(req_index+dataLen-1); i>=req_index; i--){
			if(len > 0){
				*(data_s->data+i) = writeData[len-1];
				len--;
			}else{
				*(data_s->data+i) = 0;
			}
		}
		req_index += dataLen;
	}break;
	case UINT_8:{
		*(data_s->data+req_index) = writeData[0];
		req_index += dataLen;
//		USART1_Bebug_Print_Num("[DEBUG]U8", writeData[0], 2);
	}break;
	case UINT_16:{
		*(data_s->data+req_index) = writeData[0];
		*(data_s->data+req_index+1) = writeData[1];
//		USART1_Bebug_Print_Num("[DEBUG]U16", writeData[0], 2);
//		USART1_Bebug_Print_Num("U16", writeData[1], 2);
		req_index += dataLen;
	}break;
	case UINT_32:{
		*(data_s->data+req_index) = writeData[0];
		*(data_s->data+req_index+1) = writeData[1];
		*(data_s->data+req_index+2) = writeData[2];
		*(data_s->data+req_index+3) = writeData[3];
//		USART1_Bebug_Print_Num("[DEBUG]U32", writeData[0], 2);
//		USART1_Bebug_Print_Num("U32", writeData[1], 2);
//		USART1_Bebug_Print_Num("U32", writeData[2], 2);
//		USART1_Bebug_Print_Num("U32", writeData[3], 2);
		req_index += dataLen;
	}break;
	case FLOAT:
		memcpy(data_s->data+req_index, writeData, dataLen);	// 拷贝数据
//		fprintf(USART1_STREAM, "FLOAT:%02X, %02X, %02X, %02X\r\n", writeData[0], writeData[1], writeData[2], writeData[3]);
		req_index += dataLen;					// 下标偏移
		break;
	case CRC16:{
		// 计算总长
		uint16_t data_Len = 0;
		if(is_reply) {
			data_Len = 33;
			uint16_t contentLen = req_index - data_Len;
			data_s->data[31] = contentLen;
			data_s->data[32] = contentLen >> 8;
		}
		else{
			data_Len = 31;
			uint16_t contentLen = req_index - data_Len;
			data_s->data[29] = contentLen;
			data_s->data[30] = contentLen >> 8;
		}

//		if(test_D1 == 1){
//			test_D1 = 0;
//			fprintf(USART1_STREAM, "********CRC16*******start***%d***\r\n", req_index);
//			for(uint8_t i=0; i<req_index; i++){
//				fprintf(USART1_STREAM, "%02X ", data_s->data[i]);
//			}
//			fprintf(USART1_STREAM, "\r\n********CRC16*****end********\r\n");
//		}
		uint16_t crc16 = MODBUS_CRC16_v3(data_s->data, req_index);
		*(data_s->data+req_index) = crc16;
		*(data_s->data+req_index+1) = crc16>>8;
		// 发数据
		*(data_s->data+req_index+2) = 0x0D;
		*(data_s->data+req_index+3) = 0x0A;
		data_s->data_len = req_index+4;

		return ;
	}break;
	}
}




