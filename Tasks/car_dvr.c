/*
 * car_dvr.c
 *
 *  Created on: 2022-8-8
 *      Author: yanjie
 */

#include "car_dvr.h"

/************************************* 发送命令给行车记录仪 *********************************************/
bool Send_Car_Dvr_Cmd()
{
	if(upgrade_info.step == UPGRADE_SUB_DMN) return false;
	if(upgrade_info.step == UPGRADE_URL) return false;
	if(!g_sensor_mgt.m_car_dvr.m_isOpen) return false;
	static uint32_t Send_Car_Cmd_clk = 1;
	static uint16_t no_ack_conut = 1;    //行车记录仪无反应时逐渐增加通讯间隔时间倍数
	if(!server_info.isConnect) return false;

	if(SystemtimeClock - Send_Car_Cmd_clk < 100*no_ack_conut) return false; //定时100ms循环一次
		Send_Car_Cmd_clk = SystemtimeClock;

	IWDT_Feed_The_Dog();

	switch(Car_Dvr_info.step)
	{
		case IDLE:{
			if(Car_Dvr_info.loop_times >= 50 || Car_Dvr_info.D1SaveIndex != Car_Dvr_info.D1SendIdex) { //定时50s发送ping指令
				uint8_t *data = Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SendIdex].data;
				volatile uint16_t Len = Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SendIdex].data_len;
				no_ack_conut = 1;
				if(Car_Dvr_info.D1SaveIndex != Car_Dvr_info.D1SendIdex){

					Send_Data_To_CARDVR_BY_USART2(data,Len);
					Car_Dvr_info.step = D1;
					Car_Dvr_info.loop_times = 0;
//					USART1_Bebug_Print("CarDVr", "IDLE D1",1);
					memcpy(Car_Dvr_info.D1Code.id,data+31,25);
//					USART1_Bebug_Print("CarDVr D1Code.id",Car_Dvr_info.D1Code.id,1);
				}else{
					Request_CARDVR_Ping_11_ASCII();
					Car_Dvr_info.step = PING;
					Car_Dvr_info.loop_times = 0;

//					USART1_Bebug_Print("CarDVr", "IDLE PING",1);
				}
			}
		}break;
		case D1:{
			if(Car_Dvr_info.loop_times >= 40 ){
				uint8_t *data = Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SendIdex].data;
				volatile uint16_t Len = Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SendIdex].data_len;

				if(Car_Dvr_info.D1SaveIndex != Car_Dvr_info.D1SendIdex){
					Send_Data_To_CARDVR_BY_USART2(data,Len);
					Car_Dvr_info.step = D1;
					USART1_Bebug_Print_Num("CarDVr D1 D1SendCount",Car_Dvr_info.D1SendCount,1,1);
					Car_Dvr_info.loop_times = 0;
					Car_Dvr_info.D1SendCount ++;
				}

				if(Car_Dvr_info.D1SendCount >= 3){
					Car_Dvr_info.D1SendCount = 0;
					Car_Dvr_info.step = ERRO;
					Car_Dvr_info.status =  C_ERR_NO_ACK;			//记录仪无通讯回应
					//发送记录仪掉线信息到云端
				}
//				fprintf(USART1_STREAM, "D1***D1\r\n");
//				USART1_Bebug_Print("CarDvr","D1");
			}
		}break;
		case E1:{
			if(Car_Dvr_info.loop_times >= 1000 ){					//等待E1时间大于100s重新发送D1
				uint8_t *data = Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SendIdex].data;
				volatile uint16_t Len = Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SendIdex].data_len;
				Send_Data_To_CARDVR_BY_USART2(data,Len);

				Car_Dvr_info.step = D1;
				Car_Dvr_info.loop_times = 0;
//				fprintf(USART1_STREAM, "E1***D1\r\n");
				USART1_Bebug_Print("CarDVr","E1 Step",1);
			}

		}break;
		case PING:{
			if(Car_Dvr_info.loop_times >= 50 ){
				Request_CARDVR_Ping_11_ASCII();
				Car_Dvr_info.step = PING;
				Car_Dvr_info.loop_times = 0;
				Car_Dvr_info.PingSendCount ++;
				if(Car_Dvr_info.PingSendCount >= 5){
					Car_Dvr_info.PingSendCount = 0;
					Car_Dvr_info.step = ERRO;
					Car_Dvr_info.status =  C_ERR_NO_ACK;			//记录仪无通讯回应
				}
//				USART1_Bebug_Print("CarDVr","PING PING",1);
			}

		}break;
		case ERRO:{
			switch(Car_Dvr_info.status){
				case C_ERR_NO_ACK:{
//					USART1_Bebug_Print("CarDVr ERRO","C_ERR_NO_ACK",1);
//					if(Car_Dvr_info.loop_times >= 10 ){			//定时10s发送ping指令

						Send_Car_Cmd_clk = SystemtimeClock;
						Send_Car_Cmd_clk ++;
						Request_CARDVR_Ping_11_ASCII();
//						fprintf(USART1_STREAM, "C_ERR_NO_ACK PING\r\n");
						Send_E1_ErrCode_ToServer(CAR_DVR_RECORD_VIDEO, Car_Dvr_info.status,1);
						Car_Dvr_info.step = PING;
						no_ack_conut ++;
						if(no_ack_conut >= 10000)
						{
							no_ack_conut = 10000;
						}
						Car_Dvr_info.loop_times = 0;
//					}
				}break;
//				case C_ERR_5000:{
//
//				}break;
//				case C_ERR_5001:{
//
//				}break;
//				case C_ERR_5002:{
//
//				}break;
				default:{

					if(Car_Dvr_info.loop_times >= 10 ){			//

						Send_Car_Cmd_clk = SystemtimeClock;
						Send_Car_Cmd_clk ++;
						Request_CARDVR_Ping_11_ASCII();
//						fprintf(USART1_STREAM, "C_ERR_NO_ACK PING\r\n");
						USART1_Bebug_Print("CarDVr ERRO","default",1);
						Car_Dvr_info.step = PING;
						Car_Dvr_info.loop_times = 0;
					}
				}
				break;
			}
			break;


		}
		default:
//			USART1_Bebug_Print("CarDVr","default IDLE",1);
			Car_Dvr_info.step = IDLE;
			Car_Dvr_info.loop_times = 0;
			break;
	}
	Car_Dvr_info.loop_times++;
	if(Car_Dvr_info.loop_times >=1010){
//		fprintf(USART1_STREAM, "Car_Dvr_info.loop_times == %d\r\n",Car_Dvr_info.loop_times);
		USART1_Bebug_Print_Num("CarDVr loop_times",Car_Dvr_info.loop_times,1,1);
		Car_Dvr_info.step = IDLE;
		Car_Dvr_info.loop_times = 0;
	}
	return 0;
}
/************************************* 解析行车记录仪 *********************************************/


volatile uint8_t reply_request_flag_car = 0;		// 1:reply;2:request
volatile uint16_t head_addr_car = 0;
CarDvr_Cfg car_dvr_param = {0};
// 解析USART1信息（行车记录仪）
bool USART2_Recv_Car_DVR_Interaction_ASCII_Message(uint8_t *string, uint16_t length)
{
	for(uint16_t i=0; i < length; i++){
		// 解析行车记录仪反馈信息
		if(string[i]==0xF5 && string[i+1]==0xEE){	// analysis reply
			head_addr_car = i;
			reply_request_flag_car = 1;
		}

		// 解析行车记录仪下发信息
		if(string[i]==0xFE && string[i+1]==0x55){	// analysis request
			head_addr_car = i;
			reply_request_flag_car = 2;
		}

		if(head_addr_car > 0 && reply_request_flag_car == 0) return false;

		// 解析数据
		if(reply_request_flag_car == 1){				// analysis reply
			reply_request_flag_car = 0;

			if(Analysis_Reply_ASCII_CARDVR(string+head_addr_car, length - head_addr_car) == 2){
				goto BY_PARSING_CAR;
			}
		}
		else if(reply_request_flag_car ==2){			// analysis request
			reply_request_flag_car = 0;

			if(Analysis_Request_ASCII_CARDVR(string+head_addr_car, length - head_addr_car) == 2){
				goto BY_PARSING_CAR;
			}
		}
	}

	Enable_Usart_Interrupt(USART2_SFR, INT_USART2);	// 开中断		 gcz 2022-06-03去除
	return false;

BY_PARSING_CAR:
	Enable_Usart_Interrupt(USART2_SFR, INT_USART2);	// 开中断		 gcz 2022-06-03去除
	return true;
}

CarDvrInfo Car_Dvr_info = {0};
/*
 *删除空格
 */
void del_space(char  *src,char *des,uint8_t len)
{
    while (len--) {
        if (*src == '\0') {
        	src++;
        } else {
            *des = *src;
            src++;
            des++; //指向数组的指针往后移动
        }
    }
    *des = '\0';
}
/*
 * 解析错误码
 * */
bool Analysis_CARDVR_States_Code(uint16_t states, uint8_t cmd)
{
	switch(states){
	case C_ERR_NO:			// 成功
		return TRUE;

		break;
	case C_ERR_NO_ACK:	// 平台找不到此设备
//		fprintf(USART1_STREAM, "C_ERR_NO_ACK.\r\n");
		USART1_Bebug_Print("CARDVR","C_ERR_NO_ACK",1);
		return FALSE;

	break;
	case C_ERR_3000:	// 注册设备时，提供的时间戳超时
//		fprintf(USART1_STREAM, "C_ERR_3000.\r\n");
		USART1_Bebug_Print("CARDVR","C_ERR_3000 TimeOut",1);
		return FALSE;

		break;
	case C_ERR_5000:  //未插入存储卡                    不发送D1指令
		USART1_Bebug_Print("CARDVR","C_ERR_5000 no sd card",1);
//		fprintf(USART1_STREAM, "C_ERR_5000 no sd card.\r\n");
		return FALSE;
		break;
	case C_ERR_5001:	// 未插入SIM卡		不发送D1指令
//		fprintf(USART1_STREAM, "C_ERR_5001 no sim card.\r\n");
		USART1_Bebug_Print("CARDVR","C_ERR_5001 no sim card",1);
		return FALSE;

	break;
	case C_ERR_5002:	// 无法连接网络		不发送D1指令
//		fprintf(USART1_STREAM, "C_ERR_5002 no net .\r\n");
		USART1_Bebug_Print("CARDVR","C_ERR_5002 no net",1);
		return FALSE;
	break;
	case C_ERR_5003:	// SN长度超出范围
//		fprintf(USART1_STREAM, "C_ERR_5003 no Gps.\r\n");
		USART1_Bebug_Print("CARDVR","C_ERR_5003 no Gps.",1);
		return TRUE;

	break;
	case C_ERR_5004:	// 录像状态异常		不发送D1指令
//		fprintf(USART1_STREAM, "C_ERR_5004 recording err.\r\n");
		USART1_Bebug_Print("CARDVR","C_ERR_5004 recording err.",1);
		return FALSE;

	break;
	case C_ERR_5005:	// 存储卡读写异常		不发送D1指令
//		fprintf(USART1_STREAM, "C_ERR_5005 sd wr err.\r\n");
		USART1_Bebug_Print("CARDVR","C_ERR_5005 sd wr err.",1);
		return FALSE;
	break;
	default:
//		fprintf(USART1_STREAM, "unkown err %d.\r\n",states);
		USART1_Bebug_Print_Num("CARDVR  unkown err",states,1,1);
	}
	return FALSE;
//	platform_err_id = states;
}
/*
C_ERR_5000 = 5000,				//未插入存储卡
C_ERR_5001, 					//未插入SIM卡
C_ERR_5002, 					//无法连接网络
C_ERR_5003,  					//未定位
C_ERR_5004, 					//录像状态异常
C_ERR_5005, 					//存储卡读写异常
*/
//E1 中有错误码后直接发送给云端  //待与云端验证功能
bool Send_E1_ErrCode_ToServer(uint8_t cmdCode, uint16_t stateCode, uint32_t seqNum)
{
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(seqNum, cmdCode,nodeData.data)) return false;	// 29

	// 内容长度  E1 一般为70左右0x46
	uint16_t dataLen = 0;
	dataLen += 40;//25;
	nodeData.data[29] = dataLen;
	nodeData.data[30] = dataLen>>8;
	req_index = 31;
	uint8_t id_string[25] = {0};
	sprintf(id_string, "%s_%d", server_info.itinerary_id, 0);

	Set_Request_ASCII_Info_Template(&nodeData,(uint8_t*)id_string, CHAR, 25);
	uint8_t tmp = 2;//Media_Type 0:图像,1:音频,2:视频,3:zip
	Set_Request_ASCII_Info_Template(&nodeData,(uint8_t*)(&tmp), UINT_8, 1);
	tmp = 5; //MediaFormat  0:jpeg,1:tif,2:mp3,3:mav,4:wmv,5:mp4,6:avi7:zip其它保留
	Set_Request_ASCII_Info_Template(&nodeData,(uint8_t*)(&tmp), UINT_8, 1);

	uint8_t status_tmp = 0; //仅仅上传故障码，其它信息默认为0
	switch(stateCode)
	{
		case C_ERR_NO_ACK:       //未插入存储卡
		{
			status_tmp = 199;
		}break;
		case C_ERR_5000:       //未插入存储卡
		{
			status_tmp = 200;
		}break;
		case C_ERR_5001:		//未插入SIM卡
		{
			status_tmp = 201;
		}break;
		case C_ERR_5002:		//无法连接网络
		{
			status_tmp = 254;
		}break;
		case C_ERR_5003:
		{
			status_tmp = 0;     //行车记录仪无GPS不影响其它功能所以视为正常
		}break;
		case C_ERR_5004:		//录像状态异常
		{
			status_tmp = 204;
		}break;
		case C_ERR_5005:		//存储卡读写异常
		{
			status_tmp = 205;
		}break;
		default:
		{
			status_tmp = 255;
		}
	}

	//仅仅上传故障码，其它信息默认为0
	Set_Request_ASCII_Info_Template(&nodeData,(uint8_t*)(&status_tmp), UINT_8, 1);
	//source_location 默认为0
	Set_Request_ASCII_Info_Template(&nodeData,(uint8_t*)(&tmp), UINT_8, 1);

	//gps信息默认为0

	Set_Request_ASCII_Info_Template(&nodeData,(uint8_t*)(&tmp), UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData,(uint8_t*)(&tmp), UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData,(uint8_t*)(&tmp), UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData,(uint8_t*)(&tmp), UINT_8, 1);

	//校验
//	test_D1 = 1;
	Set_Request_ASCII_Info_Template(&nodeData, &tmp, CRC16, 2);

//	 放到队列中进行发送
//	 fprintf(USART1_STREAM, "\r\n************55 AA send queue***********\r\n");
//	 for(uint16_t i=0; i<37+dataLen; i++){
//	 	fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	 }
//	 fprintf(USART1_STREAM, "\r\n************55 AA send queue***********\r\n");

	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(cmdCode, seqNum);
	nodeData.is_wait_reply 	= NEED_WAIT_REPLY;
	nodeData.is_single_cmd 	= DOUBLE_CMD;
	nodeData.is_store 		= NEED_STORED;
	insertQueue(&dataUp_q, &nodeData);

	return true;
}
/*
 * 解析平台回复
 * 回复帧头为0x55BB
 * 同步时间
 * (返回：0正常；1异常失败；2需要重新解析)阉割
 * 返回：false未解析；true解析成功
 * 说明：平台答复解析
 */
bool Analysis_Reply_ASCII_CARDVR(uint8_t *data, uint16_t len)
{
	if(!g_sensor_mgt.m_car_dvr.m_isOpen) return false;
	if(len < 32) return false;
//	fprintf(USART1_STREAM, "%s \r\n",__func__);
	//gcz  2022-06-02  优化lmz协议判断机制-------------------------------------------
	// 内容长度
	uint16_t contentLen = data[31] | data[32] << 8;
	if (contentLen > (len - 33))
		return false;
	// 校验
	uint16_t crc16_modbus_clac = MODBUS_CRC16_v3(data, 33+contentLen);
	uint16_t crc16_modbus_src  = data[33+contentLen] | data[34+contentLen] << 8;
	if(crc16_modbus_clac != crc16_modbus_src){
		USART1_Bebug_Print_Num("clc_CRC16", crc16_modbus_clac, 2, 1);
		USART1_Bebug_Print_Num("src_CRC16", crc16_modbus_src, 2, 1);
		USART1_Bebug_Print("ERROR", "Reply No Pass CRC16 Modbus.", 1);
		return false;
	}
	// 序号			4B	data[2]~data[3]  低-高
	uint16_t seqNum = data[2] | data[3]<<8;

	// 设备SN		20B	data[4]~data[24]
	uint8_t sn[25] = {0};
	uint8_t j=0;
	for(uint8_t i=4; i<24; i++){
		if(data[i] != 0){
			sn[j] = data[i];
			j++;
		}
	}
	sn[j] = '\0';

	// 指令码
	uint8_t cmd = data[24];
	bool status_tmp = TRUE;
	// 状态码 2Byte
	Car_Dvr_info.status= data[25] | data[26]<<8;
	status_tmp = Analysis_CARDVR_States_Code(Car_Dvr_info.status, cmd);

	// 时间戳
	uint32_t timeStamp = data[27] | data[28]<<8 | data[29]<<16 | data[30]<<24;
#if DEBUG_PRINT_SW
	USART1_Bebug_Print_Num("seqNum", seqNum, 1, 1);
	USART1_Bebug_Print_Num("CMD", cmd, 2, 1);
	USART1_Bebug_Print("CarDVr DVR sn", sn, 1);
#endif
	// 命令解析
	switch(cmd)
	{
	case CAR_DVR_TAKE_PICTURE:	// 行车记录仪拍照  0XD1
//		判断接收，错误状态则转为空闲状态，正常或者无GPS  C_ERR_5003可以正常使用
        if (status_tmp)
        {
			Car_Dvr_info.step = E1;
        }
		else
		{
			//添加将D1错误码转换为E1错误码升送至云端代码，错误码内容以E1指令形式上传至云端
			Send_E1_ErrCode_ToServer(CAR_DVR_RECORD_VIDEO, Car_Dvr_info.status,1);
			uint8_t data_tmp[24] = {0};

			Car_Dvr_info.step = IDLE;
		}
		Car_Dvr_info.D1SendCount = 0;
		Car_Dvr_info.loop_times = 0;
		Car_Dvr_info.D1SendIdex ++;  //只发送一次D1
#if DEBUG_PRINT_SW
		USART1_Bebug_Print_Num("CarDvr D1SaveIndex",Car_Dvr_info.D1SaveIndex,1,1);
		USART1_Bebug_Print_Num("CarDvr D1SendIndex",Car_Dvr_info.D1SendIdex,1,1);
#endif
		if(Car_Dvr_info.D1SendIdex >= _CAR_DVR_D1_COUNT)
			Car_Dvr_info.D1SendIdex = 0;
//		for(uint16_t i=0;i<len; i++){
//			fprintf(USART1_STREAM, "%02X ", data[i]);
//		}



		break;
	case CAR_DVR_PING_MCU:	// 记录仪将软件版本号和硬件版本号反馈  0x11
		Analysis_CARDVR_Hard_Soft_Ware_Info_ASCII(data+33, contentLen);
		Car_Dvr_info.step = IDLE;
		Car_Dvr_info.loop_times = 0;
		if(Car_Dvr_info.status != C_ERR_5003 && Car_Dvr_info.status != C_ERR_NO){ //正常情况下（无GPS和其它正常）不上传ping的返回结果
			Send_E1_ErrCode_ToServer(CAR_DVR_RECORD_VIDEO, Car_Dvr_info.status,1);
		}
		break;
	case CAR_DVR_SERVER_SET_PARAM:
	{		// 获取AEB设备的软件和硬件版本 0x11
//		for(uint16_t i=0; i<len; i++){
//			fprintf(USART1_STREAM, "%02X ", data[i]);
//		}
		USART1_Bebug_Print("CAR_DVR","CAR_DVR_SERVER_SET_PARAM ok!",1);
	}break;
	default:return false;
	}

	return true;
}

/*
 * 帧头0xAA55
 * 不同步时间
 * (返回：0正常；；2需要重新解析)阉割
 * 返回：false未解析；true解析成功
 * 说明：平台请求解析
 */
uint8_t Analysis_Request_ASCII_CARDVR(uint8_t *data, uint16_t len)
{
	if(!g_sensor_mgt.m_car_dvr.m_isOpen) return false;
//	fprintf(USART1_STREAM, "%s \r\n",__func__);
	if(len < 31) return false;
	//gcz  2022-06-02  优化lmz协议判断机制-------------------------------------------
	// 内容长度
	uint16_t contentLen = data[29] | data[30] << 8;
	if (contentLen > (len - 33))
		return false;
	// 校验
	uint16_t crc16_modbus_clac = MODBUS_CRC16_v3(data, 31+contentLen);
	uint16_t crc16_modbus_src  = data[31+contentLen] | data[32+contentLen] << 8;
	if(crc16_modbus_clac != crc16_modbus_src){
		USART1_Bebug_Print_Num("clc_CRC16", crc16_modbus_clac, 2, 1);
		USART1_Bebug_Print_Num("src_CRC16", crc16_modbus_src, 2, 1);
		USART1_Bebug_Print("ERROR", "Reply No Pass CRC16 Modbus.", 1);
		Car_Dvr_info.step = IDLE;
		Car_Dvr_info.loop_times = 0;
//		Car_Dvr_info.D1SendCount = 0;
		return false;   //待定
	}
	//---------------------------------------------------------------------------------------

	// test print
//	fprintf(USART1_STREAM, "\r\n************F5 EE recv*****request******\r\n");
//	for(uint16_t j=0; j<len; j++){
//		fprintf(USART1_STREAM, "%02X ", data[j]);
//	}
//	fprintf(USART1_STREAM, "\r\n************F5 EE recv***********\r\n");

	uint16_t seqNum = data[2] | data[3]<<8;	// 保证接收与回复的序号一致

	// 设备SN		20B	data[5]~data[24]
	uint8_t sn[20] = {0};
	uint8_t j=0;
	for(uint8_t i=4; i<24; i++){
		if(data[i] != 0){
			sn[j] = data[i];
			j++;
		}
	}
	sn[j] = '\0';

	// 指令码
	uint8_t cmd = data[24];

	// 时间戳
	uint32_t timeStamp = data[25] | data[26]<<8 | data[27]<<16 | data[28]<<24;

#if DEBUG_PRINT_SW
	USART1_Bebug_Print_Num("CarDVr cmd:", cmd, 2, 1);
	USART1_Bebug_Print_Num("CarDVr seqNum:", seqNum, 1, 1);
	USART1_Bebug_Print_Num("CarDVr len:", contentLen, 2, 1);
#endif
//	uint8_t replyData[2] = {0};
	// 命令解析
	switch(cmd)
	{
	case CAR_DVR_RECORD_VIDEO:{	// 行车记录仪录制视频  0xE1
		// 直接转接给平台
		Analysis_CARDVR_Record_Video_ASCII(data+2, contentLen);
//		fprintf(USART1_STREAM, "E1 recv ok\r\n");
		Car_Dvr_info.step = IDLE;
		Car_Dvr_info.D1SendCount = 0;
		Car_Dvr_info.loop_times = 0;
	}break;
	case CAR_DVR_PING_MCU:{		// 获取AEB设备的软件和硬件版本 0x11
		Replay_CARDVR_AEB_Device_Soft_Hard_Ware_Info_ASCII(seqNum);
//		fprintf(USART1_STREAM, "Ping recv ok\r\n");
	}break;

	default:return false;
	}

	return true;
}


// 接收平台反馈的E1后 ，在告知E1记录仪
bool Request_Recv_Platform_Later_Reply_CARDVR_E1_ASCII(uint8_t *data, uint16_t dataLen)
{
	if(!server_info.isConnect) return false;
//	fprintf(USART1_STREAM, "%s, %d\r\n", __FUNCTION__, __LINE__);

	static uint16_t set_aeb_param_seqNum = 1;

	// 填数
	NodeData_S nodeData = {0};
	nodeData.data[0] = 0xF5;
	nodeData.data[1] = 0xEE;
//	if(dataLen == 0)
//		memcpy(nodeData.data + 2, data, 33);	// 内容长度为0
//	else
		memcpy(nodeData.data + 2, data, 31 + dataLen);

	 is_reply = true;
	 req_index = 33 + dataLen;
	Set_Request_ASCII_Info_Template(&nodeData, data, CRC16, 2);
	is_reply = false;
	//	 放到队列中进行发送
//	 fprintf(USART1_STREAM, "\r\n************Reply_CARDVR_E1***********\r\n");
//	 for(uint16_t i=0; i<nodeData.data_len; i++){
//	 	fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	 }
//	 fprintf(USART1_STREAM, "\r\n************Reply_CARDVR_E1***********\r\n");

	Send_Data_To_CARDVR_BY_USART2(nodeData.data, nodeData.data_len);

	return true;
}
/*
 * 接收到记录仪E1后，直接转发给平台
 * dataLen:内容长度
 */
bool Analysis_CARDVR_Record_Video_ASCII(uint8_t *data, uint16_t dataLen)
{
	fprintf(USART1_STREAM, "%s \r\n",__func__);
	// 解析数据后，并将数据转发给平台E1
	NodeData_S nodeData = {0};

	nodeData.data[0] = 0xFA;
	nodeData.data[1] = 0x55;

	// 数据长度直接拷贝
	memcpy(nodeData.data + 2, data, 29 + dataLen);
	memset(nodeData.data + 4,0,20);
	memcpy(nodeData.data + 4 + 20 - strlen(server_info.sn), server_info.sn, strlen(server_info.sn));//将记录仪的SN修改为AEB的SN
	req_index = 31 + dataLen;
	Set_Request_ASCII_Info_Template(&nodeData, "aaa", CRC16, 2);

//	fprintf(USART1_STREAM, "\r\n************F5 EE recv*****request***%d***\r\n", dataLen);
//	for(uint16_t j=0; j<29 + dataLen; j++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[j]);
//	}
//	fprintf(USART1_STREAM, "\r\n************F5 EE recv***********\r\n");

#if 1
	// 解析内容
	// id
	uint8_t id[25] = {0};
	uint8_t j=0;
	uint8_t index = 29;
	for(uint8_t i=0; i<25; i++){
		if(data[i+index] != 0){
			id[j] = data[i+index];
			j++;
		}
	}
	id[j] = '\0';
	// Media_Type
	uint8_t Media_Type = data[25+index];
	// MediaFormat
	uint8_t MediaFormat = data[26+index];
	// event_code
	uint8_t event_code = data[27+index];
	// source_location
	uint8_t source_location = data[28+index];

	// gpsData
	uint8_t gpsData[200] = {0};
	j=0;
	for(uint8_t i=29; i<dataLen; i++){
		if(data[i+index] != 0){
			gpsData[j] = data[i+index];
			j++;
		}
	}
	gpsData[j] = '\0';
#if DEBUG_PRINT_SW
//	fprintf(USART1_STREAM, "id:%s\r\n", id);
//	fprintf(USART1_STREAM, "Media_Type:%d\r\n", Media_Type);
//	fprintf(USART1_STREAM, "MediaFormat:%d\r\n", MediaFormat);
//	fprintf(USART1_STREAM, "event_code:%d\r\n", event_code);
//	fprintf(USART1_STREAM, "source_location:%d\r\n", source_location);
//	fprintf(USART1_STREAM, "gpsData:%s\r\n", gpsData);

	USART1_Bebug_Print("id:",id,1);
	USART1_Bebug_Print_Num("event_code:",event_code,1,1);
#endif

#endif
	//	 放到队列中进行发送
//	 fprintf(USART1_STREAM, "\r\n************CARDVR_Record_Video to platform***********\r\n");
//	 for(uint16_t i=0; i<nodeData.data_len; i++){
//	 	fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	 }
//	event_code
//	 fprintf(USART1_STREAM, "\r\n************CARDVR_Record_Video***********\r\n");

	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	nodeData.is_wait_reply 	= NEED_WAIT_REPLY;
	nodeData.is_single_cmd 	= DOUBLE_CMD;
	nodeData.is_store 		= NEED_STORED;
	insertQueue(&dataUp_q, &nodeData);
//	fprintf(USART1_STREAM, "Car_Dvr_info.event_code is %d \r\n",event_code);

//	if(event_code <= 5){		// 对eventcode做过滤，可以使部分D1进行重发
//		if(event_code == 0)
//		if((Car_Dvr_info.D1SaveIndex != Car_Dvr_info.D1SendIdex)&&strstr((const char*)id, (const char*)server_info.itinerary_id))   //
//			Car_Dvr_info.D1SendIdex ++;			//发送成功，然后增加发送计数器 此处需要优化，记录仪可能重发E1指令
//		fprintf(USART1_STREAM, "########Car_Dvr_info.D1SaveIndex is %d \r\n",Car_Dvr_info.D1SaveIndex);
//		fprintf(USART1_STREAM, "########Car_Dvr_info.D1SendIndex is %d \r\n",Car_Dvr_info.D1SendIdex);
//		Car_Dvr_info.D1SendIdex ++;
//		if(Car_Dvr_info.D1SendIdex >= _CAR_DVR_D1_COUNT)
//			Car_Dvr_info.D1SendIdex = 0;
//	}
	return true;
}

void Send_Data_To_CARDVR_BY_USART2(uint8_t *data, uint16_t dataLen)
{
	USART_Send(USART2_SFR, data, dataLen);
}


void CAR_DVR_Message_Handle()
{
	if(usart2_idel_flag){
		usart2_idel_flag = 0;

		USART2_Recv_Car_DVR_Interaction_ASCII_Message(uart2_buf, usart2_buf_len);
		usart2_buf_len = 0;
	}
}

/*
 * 返回后，下标从29开始计数
 */
bool Request_CARDVR_Protocal_Head_ASCII(uint16_t seqNum, uint8_t cmdCode, uint8_t *data)
{
	// 协议头
	data[0] = 0xFE;
	data[1] = 0x55;
	// 序号
	data[2] = seqNum;
	data[3] = seqNum>>8;

	// SN号
	Get_Device_SN(server_info.sn);
	uint8_t devSN_len = strlen(server_info.sn);
	char sn[DEV_SN_SIZE] = {0};

	if(devSN_len >= 10){
		for(uint8_t i=23; i>3; i--){
			if(devSN_len > 0){
				data[i] = server_info.sn[devSN_len-1];
				devSN_len--;
			}else{
				data[i] = 0;
			}
		}
	}else{
		USART1_Bebug_Print("ERROR", "Get Device SN Failed.", 1);
		return false;
	}
	// 指令
	data[24] = cmdCode;
	// 时间戳
	data[25] = g_utctime;
	data[26] = g_utctime>>8;
	data[27] = g_utctime>>16;
	data[28] = g_utctime>>24;

	return true;
}

/*
 * 返回后，下标从31开始计数
 */
bool Reply_CARDVR_Protocal_Head_ASCII(uint16_t seqNum, uint8_t cmdCode, uint16_t stateCode, uint8_t *data)
{
	// 协议头
	data[0] = 0xF5;
	data[1] = 0xEE;
	// 序号
	data[2] = seqNum;
	data[3] = seqNum>>8;

	// SN号
	uint8_t devSN_len = strlen(server_info.sn);
	if(devSN_len >= 10){
		for(uint8_t i=23; i>3; i--){
			if(devSN_len > 0){
				data[i] = server_info.sn[devSN_len-1];
				devSN_len--;
			}else{
				data[i] = 0;
			}
		}
	}else{
		USART1_Bebug_Print("ERROR", "Analysis Device SN Failed.", 1);
		return false;
	}
	// 指令码
	data[24] = cmdCode;
	// 状态码
	data[25] = stateCode;
	data[26] = stateCode>>8;
	// 时间戳
//	fprintf(USART1_STREAM, "g_utctime:%d\r\n", g_utctime);

	data[27] = g_utctime;
	data[28] = g_utctime>>8;
	data[29] = g_utctime>>16;
	data[30] = g_utctime>>24;

	// for(uint8_t i=0; i<31;i++){
	// 	fprintf(USART1_STREAM, "%02X ", data[i]);
	// }
	// fprintf(USART1_STREAM, "\r\n");
	return true;
}

bool Analysis_CARDVR_Hard_Soft_Ware_Info_ASCII(uint8_t *data, uint16_t dataLen)
{
	uint8_t car_dvr_hardWare[15] = {0};
	uint8_t car_dvr_softWare[15] = {0};

	// 硬件版本
	uint8_t j=0;
	for(uint8_t i=0; i<15; i++){
		if(data[i] != 0){
			car_dvr_hardWare[j] = data[i];
			j++;
		}
	}
	car_dvr_hardWare[j] = '\0';

	// 软件版本
	j=0;
	for(uint8_t i=15; i<30; i++){
		if(data[i] != 0){
			car_dvr_softWare[j] = data[i];
			j++;
		}
	}
	car_dvr_softWare[j] = '\0';

//	fprintf(USART1_STREAM, "Hardware Version:%s\r\n", car_dvr_hardWare);
//	fprintf(USART1_STREAM, "Software Version:%s\r\n", car_dvr_softWare);

	return true;
}

bool Request_CARDVR_Ping_11_ASCII()
{

	if(!server_info.isConnect) return false;
	static uint16_t seqNum = 0;

	if(seqNum > UPLOAD_MAX_SEQ_NUM) seqNum = 1;
	else seqNum ++;

	// 填数
	NodeData_S nodeData = {0};
	if(!Request_CARDVR_Protocal_Head_ASCII(seqNum, CAR_DVR_PING_MCU, nodeData.data)) return false;

	req_index = 31;

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, "aaa", CRC16, 2);

	Send_Data_To_CARDVR_BY_USART2(nodeData.data, nodeData.data_len);

//	Car_Dvr_info.step = PING;
//	Car_Dvr_info.PingSendCount = 0;

//	for(uint16_t i=0; i<nodeData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n");

	return true;
}


bool Request_CARDVR_Take_Picture_D1_ASCII()
{
	if(!g_sensor_mgt.m_car_dvr.m_isOpen) return false;
//	if(!server_info.isConnect) return false;

//	static uint16_t seqNum = 0;
//
//	if(seqNum > UPLOAD_MAX_SEQ_NUM) seqNum = 1;
//	else seqNum ++;
	// 填数
	NodeData_S nodeData = {0};
	memset(nodeData.data, 0, sizeof(nodeData.data));
	if(!Request_CARDVR_Protocal_Head_ASCII(upload_warningInfo_seqNum, CAR_DVR_TAKE_PICTURE, nodeData.data)) return false;

	req_index = 31;

	// 内容
	uint8_t id_string[35] = {0};
	sprintf(id_string, "%s_%d", server_info.itinerary_id, upload_warningInfo_seqNum);
	memcpy(Car_Dvr_info.D1Code.id,id_string,25);
	Set_Request_ASCII_Info_Template(&nodeData, id_string, CHAR, 25);

//	if(strstr((const char*)id_string, (const char*)server_info.itinerary_id))
//		USART1_Bebug_Print("CarDVr id_string:", id_string,1);
//		fprintf(USART1_STREAM, "id_string is %s \r\n",id_string);

	// 来源0：AEB控制器
	uint8_t device_src = 0;
	Set_Request_ASCII_Info_Template(&nodeData, &device_src, UINT_8, 1);

	// 类型 0:照片；1：视频
	uint8_t type = 1;
	Set_Request_ASCII_Info_Template(&nodeData, &type, UINT_8, 1);

	uint8_t *tmp_t = NULL;
	// 拍摄时长
	uint16_t duration = car_dvr_param.record_seconds;
	tmp_t = (uint8_t *)&duration;
	Set_Request_ASCII_Info_Template(&nodeData, tmp_t, UINT_16, 2);

	// 拍照间隔，单位毫秒
	uint16_t interval = 0;
	tmp_t = (uint8_t *)&interval;
	Set_Request_ASCII_Info_Template(&nodeData, tmp_t, UINT_16, 2);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, tmp_t, CRC16, 2);

	memcpy(Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SaveIndex].data,nodeData.data, nodeData.data_len);
	Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SaveIndex].data_len = nodeData.data_len;
	Car_Dvr_info.D1SaveIndex ++;

#if DEBUG_PRINT_SW
//	USART1_Bebug_Print("CarDVr", id_string);
//	USART1_Bebug_Print_Num("CarDVrSaveIndex",Car_Dvr_info.D1SaveIndex,1,1);
//	USART1_Bebug_Print_Num("CarDVrSendIndex",Car_Dvr_info.D1SendIdex,1,1);
//	fprintf(USART1_STREAM, "#########D1SaveIndex is %d \r\n",Car_Dvr_info.D1SaveIndex);
//	fprintf(USART1_STREAM, "#########D1SendIndex is %d \r\n",Car_Dvr_info.D1SendIdex);
#endif
	if (Car_Dvr_info.D1SaveIndex >= _CAR_DVR_D1_COUNT)
		Car_Dvr_info.D1SaveIndex = 0;
//		return false;
//	}
//	Send_Data_To_CARDVR_BY_USART2(nodeData.data, nodeData.data_len);


//	for(uint16_t i=0; i<nodeData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	}

//	fprintf(USART1_STREAM, "*********Request_CARDVR_Take_Picture_D1_ASCII save D1 data！*********\r\n");

	return true;
}



bool Request_CARDVR_Set_Gsensor_C6_ASCII()
{
//需要获得设备的
//	if(!server_info.isConnect) return false;
	static uint16_t seqNum = 0;

	if(seqNum > UPLOAD_MAX_SEQ_NUM) seqNum = 1;
	else seqNum ++;

	// 填数
	NodeData_S nodeData = {0};
	if(!Request_CARDVR_Protocal_Head_ASCII(seqNum, CAR_DVR_SERVER_SET_PARAM, nodeData.data)) return false;

	req_index = 31;
	// 来源0：AEB控制器
	uint8_t device_src = 0;
	Set_Request_ASCII_Info_Template(&nodeData, &device_src, UINT_8, 1);
	// 是否打开gsensor
	uint8_t gsensor_enable =car_dvr_param.g_sensor_isOpen;
	Set_Request_ASCII_Info_Template(&nodeData, &gsensor_enable, UINT_8, 1);
	// gsensor灵敏度 
	uint8_t gsensor_level =car_dvr_param.g_sensor_level;
	Set_Request_ASCII_Info_Template(&nodeData, &gsensor_level, UINT_8, 1);
	// gsensor拍摄时长
	uint16_t duration =car_dvr_param.record_seconds;
	Set_Request_ASCII_Info_Template(&nodeData, (char*)&duration, UINT_16, 1);
	//校验
	Set_Request_ASCII_Info_Template(&nodeData, "aaa", CRC16, 2);

	Send_Data_To_CARDVR_BY_USART2(nodeData.data, nodeData.data_len);

//	Car_Dvr_info.step = PING;
//	Car_Dvr_info.PingSendCount = 0;
	USART1_Bebug_Print("CarDVr", "Request_CARDVR_Set_Gsensor_C6_ASCII",1);

//	for(uint16_t i=0; i<nodeData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n");

	return true;
}


void Collection_CARDVR_Take_Picture_D1_ASCII(AEB_UPLOAD_INFO *info)			// 心跳信息
{
	if(!server_info.isConnect) return;
	if(!upgrade_info.isUpgradeOK) return;

	static uint32_t collection_takePic_clk = 0;
	static uint16_t collection_interval_times = 0;
	if(SystemtimeClock - collection_takePic_clk < 25000) return ;	// 扫描周期25s
	collection_takePic_clk = SystemtimeClock;
	collection_interval_times++;

	// 获取软硬件版本
	if(collection_interval_times == 1){
		Request_CARDVR_Ping_11_ASCII();
	}

	if(collection_interval_times == 2){
		collection_interval_times = 0;
		// 触发预警
//		Collection_Warning_Info_ASCII(info);
//		// 让记录仪拍照
//		Request_CARDVR_Take_Picture_D1_ASCII();
	}
}

bool Replay_CARDVR_AEB_Device_Soft_Hard_Ware_Info_ASCII(uint16_t seqNum)
{
//	fprintf(USART1_STREAM, "%s \r\n",__func__);
//	USART1_Bebug_Print("CarDVr", "Replay_CARDVR_AEB_Device_Soft_Hard_Ware_Info_ASCII",1);
	NodeData_S nodeData = {0};
	if(!Reply_CARDVR_Protocal_Head_ASCII(seqNum, CAR_DVR_PING_MCU, 0x00, nodeData.data)) return false;

	req_index = 33;
	// 获取软件版本号
	Set_Request_ASCII_Info_Template(&nodeData, upgrade_info.run_v, CHAR, 15);

//	fprintf(USART1_STREAM, "softV:%s\r\n", upgrade_info.run_v);
	// 获取硬件版本号
	uint8_t hardware_v[HARDWARE_INFO_SIZE] = {0};
	if(!Get_Hardware_Info(hardware_v)){
		sprintf(hardware_v, "V0.0.0.L0C0");
	}
	Set_Request_ASCII_Info_Template(&nodeData, hardware_v, CHAR, 15);
//	fprintf(USART1_STREAM, "hardV:%s\r\n", hardware_v);
	//校验
	is_reply = true;
	Set_Request_ASCII_Info_Template(&nodeData, "aaa", CRC16, 2);
	is_reply = false;
	Send_Data_To_CARDVR_BY_USART2(nodeData.data, nodeData.data_len);

//	fprintf(USART1_STREAM, "\r\n************** Send 11 To CAR DVR *******start*********\r\n");
//	 for(uint8_t i=0; i<nodeData.data_len;i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	 }
//	 fprintf(USART1_STREAM, "\r\n************** Send 11 To CAR DVR *******end*********\r\n");
}



uint8_t Get_CarDvr_Cfg_Param(uint8_t *data,  uint16_t dataLen)
{
	return W25QXX_Read_One_Sector_Chk(data, OUT_FLASH_CARDVR_CFG_START, dataLen);

}

uint8_t Read_CarDvr_Cfg_Param(CarDvr_Cfg *param)
{
	uint8_t data[CARDVR_CFG_SIZE] = {0};
	memset(data, 0, CARDVR_CFG_SIZE);

	if(Get_CarDvr_Cfg_Param(data, sizeof(CarDvr_Cfg))){		// 获取数据
//		USART1_Bebug_Print("Read OK", "Read Sensor Configure Parameter Success.");
		memcpy(param, data, sizeof(CarDvr_Cfg));	// copy
		return 1;
	}

	USART1_Bebug_Print("Read Failed", "Read Sensor Configure Parameter Failed.",1);
	memset(param, 0, sizeof(CarDvr_Cfg));

	return 0;
}

uint8_t W25QXX_Write_CarDvr_Cfg_Param(uint8_t *data,  uint16_t dataLen)
{
	W25QXX_Write_One_Sector_Chk(data, OUT_FLASH_CARDVR_CFG_START, dataLen);//OUT_FLASH_SENSOR_CFG_START

	memset(data, 0, sizeof(data));
	if(W25QXX_Read_One_Sector_Chk(data, OUT_FLASH_CARDVR_CFG_START, dataLen)){
		return true;
	}else{
		// 失败就就擦除扇区
		W25QXX_Erase_Sector(OUT_FLASH_CARDVR_CFG_START/EX_FLASH_SECTOR_SIZE);
		return false;
	}
}


uint8_t Write_CarDvr_Cfg_Param(CarDvr_Cfg param)
{
	uint8_t data[CARDVR_CFG_SIZE] = {0};
	memcpy(data, &param, sizeof(param));

	if(W25QXX_Write_CarDvr_Cfg_Param(data, sizeof(CarDvr_Cfg))){
//		USART1_Bebug_Print("Write OK", "write Sensor Configure Parameter Success.");
		return 1;
	}

	USART1_Bebug_Print("Write Failed", "Write_CarDvr_Cfg_Param Failed.",1);

	return 0;
}

uint8_t Set_Car_Dvr_Param(CarDvr_Cfg param)
{
    car_dvr_param = param;
	return Write_CarDvr_Cfg_Param(param);
}
CarDvr_Cfg Get_Car_Dvr_Param()
{
    return car_dvr_param;
}


/*
 * 平台设置传感器配置参数
 */
uint8_t Analysis_Request_Set_CarDvr_Param_ASCII(uint8_t *data)
{

	CarDvr_Cfg param;
	uint8_t index = 0;
	
	param.from = data[index];
	param.g_sensor_isOpen = data[index+1];					// g_sensor开关与触发登机
	param.g_sensor_level = data[index+2];
	param.record_seconds = (uint16_t)data[index+3];				// 记录时间长度只取第四个字节，第五字节忽略
	param.warning_fcw = data[index+5];				
    param.warning_hmw = data[index+6];
	param.warning_ldw = data[index+7];
	param.warning_aeb = data[index+8];
	
//	fprintf(USART1_STREAM, "from %d \r\n", param.from);
//	fprintf(USART1_STREAM, "param.g_sensor_isOpen %d \r\n", param.g_sensor_isOpen);
//	fprintf(USART1_STREAM, "param.g_sensor_level %d \r\n", param.g_sensor_level);
//	fprintf(USART1_STREAM, "param.record_seconds %d \r\n", param.record_seconds);
//	fprintf(USART1_STREAM, "param.warning_fcw %d \r\n", param.warning_fcw);
//	fprintf(USART1_STREAM, "param.warning_hmw %d \r\n", param.warning_hmw);
//	fprintf(USART1_STREAM, "param.warning_ldw %d \r\n", param.warning_ldw);
//	fprintf(USART1_STREAM, "param.warning_aeb %d \r\n", param.warning_aeb);
	car_dvr_param = param;
	// 先做备份，若写失败就恢复
	Read_CarDvr_Cfg_Param(&param);

	// 写操作
	if(Write_CarDvr_Cfg_Param(car_dvr_param)){
//		USART1_Bebug_Print("Write OK", "Write CarDvr Config Parameter OK.",1);
		return 1;
	}else{
		USART1_Bebug_Print("Write FAILED", "Write CarDvr Config Parameter Failed.",1);
		// 恢复
		car_dvr_param = param;
		Write_CarDvr_Cfg_Param(car_dvr_param);
	}
	/*需要添加设置Gsensor的相关内容*/	

	return 0;
}




/*
 * 平台查询行车记录仪参数
 */
uint8_t Send_Reply_Platform_Query_CarDvr_Param_Info_ASCII(uint32_t seqNum)
{

	NodeData_S nodeData = {0};
	memset(nodeData.data_info, 0, NODE_DATAINFO_SIZE);
	memset(nodeData.data, 0, NODE_DATA_SIZE);
	nodeData.data_len = 0;

	req_index = 33;
	is_reply = true;
	if(!Reply_Protocal_Head_ASCII(seqNum, CAR_DVR_SERVER_QUERY_PARAM, 0x01, nodeData.data)) return false;


	CarDvr_Cfg param;
	Read_CarDvr_Cfg_Param(&param);
	car_dvr_param = param;
	// 纬度
               //指令来源
	Set_Request_ASCII_Info_Template(&nodeData, &(param.from), UINT_8, 1);
	uint8_t g_sensor_isOpen= car_dvr_param.g_sensor_isOpen;
	Set_Request_ASCII_Info_Template(&nodeData, &g_sensor_isOpen, UINT_8, 1);
	uint8_t g_sensor_level = car_dvr_param.g_sensor_level;
	Set_Request_ASCII_Info_Template(&nodeData, &g_sensor_level, UINT_8, 1);
	uint16_t record_seconds = car_dvr_param.record_seconds;
	Set_Request_ASCII_Info_Template(&nodeData, (char*)&record_seconds, UINT_16, 2);
	uint8_t warning_fcw = car_dvr_param.warning_fcw;
	Set_Request_ASCII_Info_Template(&nodeData, &warning_fcw, UINT_8, 1);
	uint8_t warning_hmw = car_dvr_param.warning_hmw;
	Set_Request_ASCII_Info_Template(&nodeData, &warning_hmw, UINT_8, 1);
	uint8_t warning_ldw = car_dvr_param.warning_ldw;
	Set_Request_ASCII_Info_Template(&nodeData, &warning_ldw, UINT_8, 1);
	uint8_t warning_aeb = car_dvr_param.warning_aeb;
	Set_Request_ASCII_Info_Template(&nodeData, &warning_aeb, UINT_8, 1);

//校验
	Set_Request_ASCII_Info_Template(&nodeData,&warning_aeb, CRC16, 2);
	is_reply = false;

//	nodeData.data[33+dataLen] = crc16_modbus;
//	nodeData.data[34+dataLen] = crc16_modbus>>8;

	// 发数据
//	nodeData.data[35+dataLen] = 0x0D;
//	nodeData.data[36+dataLen] = 0x0A;
//	nodeData.data_len = 37+dataLen;
// 放到队列中进行发送
//	fprintf(USART1_STREAM, "nodeData.data_len =%d ", nodeData.data_len);
//	 fprintf(USART1_STREAM, "\r\n************55 AA send queue***********\r\n");
//	 for(uint16_t i=0; i<nodeData.data_len; i++){
//	 	fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	 }
//	 fprintf(USART1_STREAM, "\r\n************55 AA send queue***********\r\n");
//
//	fprintf(USART1_STREAM, "from %d \r\n", car_dvr_param.from);
//	fprintf(USART1_STREAM, "param.g_sensor_isOpen %d \r\n", car_dvr_param.g_sensor_isOpen);
//	fprintf(USART1_STREAM, "param.g_sensor_level %d \r\n", car_dvr_param.g_sensor_level);
//	fprintf(USART1_STREAM, "param.record_seconds %d \r\n", car_dvr_param.record_seconds);
//	fprintf(USART1_STREAM, "param.warning_fcw %d \r\n", car_dvr_param.warning_fcw);
//	fprintf(USART1_STREAM, "param.warning_hmw %d \r\n", car_dvr_param.warning_hmw);
//	fprintf(USART1_STREAM, "param.warning_ldw %d \r\n", car_dvr_param.warning_ldw);
//	fprintf(USART1_STREAM, "param.warning_aeb %d \r\n", car_dvr_param.warning_aeb);

	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
//	Test_Print_CMD_Seq(cmdCode, seqNum);
	nodeData.is_wait_reply 	= NO_WAIT_REPLY;
	nodeData.is_single_cmd 	= DOUBLE_CMD;
	nodeData.is_store 		= NEED_STORED;
	insertQueue(&dataUp_q, &nodeData);

	is_reply = false;
	return true;

}

/*
 * 平台遥控触发行车记录拍摄
 */

 bool Server_Request_CARDVR_Take_Picture_D1_ASCII(uint8_t *data, uint16_t len)
 {

	data[0] = 0xFE;
	data[1] = 0x55;
	NodeData_S nodeData = {0};
	memcpy(nodeData.data,data,len);
	nodeData.data_len = len;
	req_index = len;
	Set_Request_ASCII_Info_Template(&nodeData, "a", CRC16, 2);
	memcpy(Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SaveIndex].data,nodeData.data, nodeData.data_len);
	Car_Dvr_info.CmdD1Buf[Car_Dvr_info.D1SaveIndex].data_len = nodeData.data_len;
	Car_Dvr_info.D1SaveIndex ++;
 
#if DEBUG_PRINT_SW
// 	for(uint16_t i=0;i<nodeData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	}
	USART1_Bebug_Print_Num("D1SaveIndex：",Car_Dvr_info.D1SaveIndex,1,1);
	USART1_Bebug_Print_Num("D1SendIndex", Car_Dvr_info.D1SendIdex,1,1);
#endif
	return true;
 
 }
/*初始化记录仪参数*/
void CarDvr_Param_Init()
{
	CarDvr_Cfg param;

	if(Read_CarDvr_Cfg_Param(&param))
	{
		car_dvr_param = param;
	}
	else
	{
		car_dvr_param.from = 0;
		car_dvr_param.g_sensor_isOpen = 1;				// g_sensor开关
		car_dvr_param.g_sensor_level = 1;
		car_dvr_param.record_seconds = 15;				// 记录时间长度只取第四个字节，第五字节忽略
		car_dvr_param.warning_fcw = 1;
		car_dvr_param.warning_hmw = 1;
		car_dvr_param.warning_ldw = 0;
		car_dvr_param.warning_aeb = 1;

		Write_CarDvr_Cfg_Param(car_dvr_param);
	}
	//开机获取初始化参数后设置记录仪参数
	Request_CARDVR_Set_Gsensor_C6_ASCII();

//	fprintf(USART1_STREAM, "car_dvr_param.from %d \r\n", car_dvr_param.from);
//	fprintf(USART1_STREAM, "car_dvr_param.g_sensor_isOpen %d \r\n", car_dvr_param.g_sensor_isOpen);
//	fprintf(USART1_STREAM, "car_dvr_param.g_sensor_level %d \r\n", car_dvr_param.g_sensor_level);
//	fprintf(USART1_STREAM, "car_dvr_param.record_seconds %d \r\n", car_dvr_param.record_seconds);
//	fprintf(USART1_STREAM, "car_dvr_param.warning_fcw %d \r\n", car_dvr_param.warning_fcw);
//	fprintf(USART1_STREAM, "car_dvr_param.warning_hmw %d \r\n", car_dvr_param.warning_hmw);
//	fprintf(USART1_STREAM, "car_dvr_param.warning_ldw %d \r\n", car_dvr_param.warning_ldw);
//	fprintf(USART1_STREAM, "car_dvr_param.warning_aeb %d \r\n", car_dvr_param.warning_aeb);
}



