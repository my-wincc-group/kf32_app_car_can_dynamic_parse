/*
 * _4g_data_upload.h
 *
 *  Created on: 2021-12-13
 *      Author: Administrator
 */

#ifndef _4G_DATA_UPLOAD_H_
#define _4G_DATA_UPLOAD_H_
#include "upgrade_common.h"
#include "cqueue.h"
#include "aeb_cms_sss_para_config.h"
#include "AEB_upload_info.h"
/* ------------------------宏 枚举------------------------------- */
#define GPS_TIME_INTERVAL		250
#define UPLOAD_MAX_SEQ_NUM		60000

enum{
	GPS_NOT_SIGNAL,
	GPS_SUCCESS,
	ERROR_50,
	ERROR_504,	// 会话进行中
	ERROR_505,	// 会话未激活
	ERROR_516,	// 当前未定位
	PDP_QIDEACT,// 需要激活现场
};
typedef enum _Write_Type_E
{
	FILL_0,
	CHAR,
	UINT_8,
	UINT_16,
	UINT_32,
	FLOAT,
	CRC16,
}Write_Type_E;

typedef struct _4G_Signal_Strength_S{
	uint8_t rssi;					// 接收信号强度指示。[0,99]
	uint8_t ber;					// 信道误码率。百分比格式。[0,99]
	uint8_t isStart;	// 功能开关；1开启；0不开启
}_4G_Signal_Strength_S;

/* ------------------------全局函数声明------------------------------- */
// 数据上传
void 	Upload_Info_Collection();
void 	Upload_Info_Collection();			// 上传信息收集
// GPS
void 	Get_GPS_Info_In_2_Second();
void 	GPS_Start();
void 	GPS_Restart();

bool 	_4G_Recv_Signal_Intensity_Message(uint8_t *string,uint32_t length);//gcz 2022-06-03 增加参数：数据长度的输入
void 	Get_4G_Signal_Strength_In_4_Second();
GPS_INFO Get_GPS_Info();
uint8_t Analysis_USART1_Data_Upload_CMD();
void 	Set_Request_ASCII_Info_Template(NodeData_S *data_s, uint8_t *writeData, Write_Type_E dataType, uint8_t dataLen);

float 	Get_Accumulated_Mileage();
void 	Analysis_GPS_DATA();

extern void Accumulate_Car_Meliage_In_1_Second();
void 	AEB_Output_Data();
void 	AEB_Input_Data();

bool 	GPS_Time_To_UTCTIME(uint8_t *dmy, uint8_t *hms, uint8_t *lon, uint8_t *ew);
bool	TimeString_To_UTCTIME(char * time_string, uint8_t timeZone);

bool 	Request_Protocal_Head_ASCII(uint16_t seqNum, uint8_t cmdCode,  uint8_t *data);
bool 	Reply_Protocal_Head_ASCII(uint16_t seqNum, uint8_t cmdCode, uint16_t stateCode, uint8_t *data);
bool 	_4G_Recv_GPS_Message(uint8_t *string,uint32_t length);//gcz 2022-06-03 增加参数：数据长度的输入
//bool 	_4G_Recv_Data_Uploade_Message(uint8_t *string);

/**************************  ASCII    *************************************/
void 	EC200U_Data_Upload_Init();
void 	Upload_Info_Collection_ASCII();

bool 	Send_Reply_Platform_ASCII(uint8_t cmdCode, uint16_t stateCode, uint32_t seqNum, uint8_t *data, uint16_t dateLen);

bool 	Analysis_Reply_Set_GPS_Interval_ASCII(uint8_t *data, uint8_t dataLen);
bool 	Analysis_Request_Set_GPS_Interval_ASCII(uint8_t *data);
void 	Request_Vehicle_Config_Param_ASCII();
bool 	Request_Set_Gps_Interval_ASCII(uint8_t interval);
void 	Collection_Warning_Info_ASCII(AEB_UPLOAD_INFO *p);
void 	Collection_VehicleBody_Info_ASCII(AEB_UPLOAD_INFO *p);
void 	Collection_VehicleBase_Info_ASCII(AEB_UPLOAD_INFO *p);
void 	Collection_GPS_Info_ASCII();
void 	Collection_HeartBeat_ASCII();

extern bool 	Request_CARDVR_Take_Picture_D1_ASCII();
void    Send_Config_AGPS();
void    Send_Enable_AGPS();
void    Send_Enable_GNSS();

/* ------------------------全局变量声明------------------------------- */
extern volatile uint16_t req_index;			// request array 下标
extern bool 			get_4G_signal_start;
extern volatile uint8_t gps_Interval_coef;
extern volatile bool 	is_reply;
extern volatile uint16_t upload_warningInfo_seqNum;
#endif /* _4G_DATA_UPLOAD_H_ */
