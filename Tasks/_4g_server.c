/*
 * _4g_server.c
 *
 *  Created on: 2022-3-10
 *      Author: Administrator
 *  说明：通过4G模块TCP/IP协议连接上Server(平台)
 */

#include "_4g_server.h"
#include "common.h"
#include "EC200U.h"
#include "usart.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "w25qxx.h"
#include "_4g_upgrade.h"
//#include "md5.h"
#include "_4g_para_config.h"
#include "cqueue.h"
#include "modbus_crc16.h"
#include "_4g_data_upload.h"
#include "aeb_sensor_management.h"
/************************* 宏定义 **************************************/
#define CONNECT_SERVER_USER_ID			"zkhyabc"
#define CONNECT_SERVER_USER_CODE		"cbacba"
#define SERVER_DOMAIN_NAME				"http://data-router.zhkhy.com/aeb/router?"

/************************* 结构体定义 **************************************/
typedef struct _Con_Server_Info
{
	uint16_t code;		// 响应码
	uint8_t ip[20];		// ip
	uint16_t port;		// 端口
	uint32_t time;		// 时间，同步设备时钟
//	uint8_t msg[24];	// 错误信息，不解析，通过code就知道原因
}ConServer_Info;
/************************* 全局变量定义 **************************************/
volatile uint32_t g_utctime			= 0;		// UTCTIME时间，单位秒
//volatile uint16_t ota_loop_times 	= 0;
Server_Info server_info 			= {0};
NodeData_S nodeData_server_share 	= {0};
extern _4G_Signal_Strength_S signal_strength;
ConServer_Info conserver_info 		= {0};

/************************* 全局函数声明 **************************************/
void 	AT();
void 	AT_CPIN_To_Get_SIM_Status();
void 	AT_CREG_To_Judge_GSM();
void 	AT_ATE0();
void 	AT_QIOPEN_TO_Connect_Server();
void 	AT_QICLOSE_Server();
void 	Device_Login_Platform();

bool	Request_Get_Secret_CMD_ASCII();
bool 	Request_Register_Device_CMD_ASCII();
extern bool 	Request_OTA_Finish_CMD_ASCII();

void EC200U_Server_Init()
{
	// 连接服务器：启动OTA
	server_info.isConnect	= 0;
	server_info.step		= WAIT;
	server_info.step_step	= 0;
	server_info.reconnect_server_position = 0;
	server_info.err_loop_times = 0;
	server_info.con_server_reason = 1;
	// 获取SN
	if(Get_Device_SN(server_info.sn)){
		server_info.snIsOk = 1;
//		USART1_Bebug_Print("DEBUG SN", server_info.sn);
	}else{
		server_info.snIsOk = 0;
		USART1_Bebug_Print("ERROR", "Get Device SN Failed.", 1);
	}

	// 获取PN
	if(Get_Device_PN(server_info.pn)){
//		USART1_Bebug_Print("DEBUG PN", server_info.pn);
	}else{
		USART1_Bebug_Print("ERROR", "Get Device PN Failed.", 1);
	}

	// 获取secret
	if(Get_Secret(server_info.secret)){
		server_info.isSecret = 1;
		// USART1_Bebug_Print("DEBUG Secret", server_info.secret);
	}else{
		server_info.isSecret = 0;
		USART1_Bebug_Print("ERROR", "Get Device Secret Failed.", 1);
	}

//	USART1_Bebug_Print("DEBUG SN",  server_info.sn);
//	USART1_Bebug_Print("DEBUG PN",  server_info.pn);
//	USART1_Bebug_Print("DEBUG Secret",  server_info.secret);

	// 触发连接服务器
	AT();
}

/*
 * UTCTIME计数（在滴答定时器中）
 */
uint32_t utctime_t = 0;
void UTCTIME_Count_In_SysClick_IT(uint32_t sysTimeClock)
{
	// 实时时间，4G连接成功后，会将utctime更新成网络时间
	if(sysTimeClock - utctime_t >= 1000){	// 1s
		utctime_t = sysTimeClock;
		g_utctime++;
	}
}

/*
 * 将UTC时间转成时间结构体，时间戳
 * 参数：UTC时间
 * 返回：时间结构体
 */
TimeType Get_CurrentTime_Stamp()
{
	TimeType t;
	t = alg_Utc2LocalTime(g_utctime,8);
	return t;
}

/*
 * 重连服务器
 * 参数：0直接连接服务器；1从AT位置开始重连服务器
 */
void Reconnect_Server(uint8_t enable_position)
{
	upgrade_info.rx_mode = RECV_CMD;
	Enable_Usart_Interrupt(USART0_SFR, INT_USART0);	// 开中断		 gcz 2022-06-03去除

	server_info.reconnect_server_position = enable_position;

	// 若连续连接服务器都失败，就重新获取IP和Port 20220913 lmz add
#if NTP_ENABLE
	AT_QICLOSE_Server();
	USART1_Bebug_Print("RECONNECT", "Reconnect Server.", 1);
#else
	if(server_info.loop_con_server_times >= 5){
		server_info.loop_con_server_times  = 0;
		server_info.con_server_reason = 3;
//		AT_Request_To_Server_Connect_Info_1();
	}else{
		server_info.loop_con_server_times++;
		AT_QICLOSE_Server();
		USART1_Bebug_Print("RECONNECT", "Reconnect Server.", 1);
	}
#endif
}

/*
 * 连接服务器
 */
void Connect_Server()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return ;

	static uint32_t connect_server_clk = 0;
	static bool 	connect_server_over_time_flag = false;
	if(SystemtimeClock - connect_server_clk < 250) return ;		// 250ms
	connect_server_clk = SystemtimeClock;

	// 服务器连接超时, 60秒
	if(connect_server_over_time_flag){
		if(server_info.offline_times >= 5){
			server_info.offline_times = 0;
			connect_server_over_time_flag = false;

			Reconnect_Server(0);
			USART1_Bebug_Print("RECONNECT", "HeartBeat Over Time, Reconnect Server.", 1);
		}
	}

	if(server_info.isConnect) return ;		// 成功连接服务器，就直接返回
	if(!server_info.snIsOk)	return ;		// SN无效直接返回

	uint8_t ret = _4G_Connect_Server_ASCII();

	switch(ret){
	case 1:	{	// 连接成功
		server_info.isConnect 	= 1;
		server_info.step 		= WAIT;
		USART1_Bebug_Print("SERVER", "Connect Server OK.", 1);
		connect_server_over_time_flag = true;
		server_info.loop_con_server_times = 0;

		// OTA升级后需要重连服务器后，再触发回复平台（0xB8）
		if(upgrade_info.ota_reconnect_server){
			upgrade_info.ota_reconnect_server = 0;
			Request_OTA_Finish_CMD_ASCII();			// 回复平台，成功后才算升级结束
			return ;
		}

		// 若OTA升级未触发时，而APP程序升级成功了则需告知平台（0xB8）
		if(!upgrade_info.isUpgradeOK){
			OTA_Start();
		}else{
			// 若GPS未启动，就启动
			 if(!gps_info.isConnect) GPS_Start();

			// 若获取4G信号强度未启动，就启动
			if(!signal_strength.isStart) signal_strength.isStart = 1;
		}
	}break;
	case 2:	{	// 重连服务器中
		Reconnect_Server(0);
	}break;
	default:break;// 连接中
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/////	连接服务器
////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * 发送AT
 */
void AT()
{
	server_info.step 	= ATCMD;

//	Send_Single_Data_By_Queue("AT");
	uint8_t cmd[10] 		= {0};
	sprintf(cmd, "AT\r\n");
	EC200U_SendData(cmd, strlen(cmd), 1);
}
#if NTP_ENABLE
/*
 * 获取阿里云时间
 */
void AT_NTP()
{
	server_info.step 	= NTP;
//	Send_Single_Data_By_Queue("AT+QNTP=1, \"ntp.aliyun.com\", 123");
	uint8_t cmd[30] 	= {0};
	sprintf(cmd, "AT+QNTP=1, \"ntp.aliyun.com\", 123\r\n");
	EC200U_SendData(cmd, strlen(cmd), 1);
}
#else
/*
 * 向服务器索要连接IP、端口及时间（同步设备时间）
 * 说明：添加时间20220913，为了防护服务器安全。
 */
void AT_Request_To_Server_Connect_Info_1()
{
	server_info.step 	= GET_SERVER_INFO_1;

	memset(server_info.http_cmd, 0, sizeof(server_info.http_cmd));
//	strcpy(server_info.sn, "1226060112");
	// 计算签名：CRC16_Modus(SN+userid+password)
	sprintf(server_info.http_cmd, "%s%s%s", server_info.sn, (uint8_t *)CONNECT_SERVER_USER_ID, (uint8_t *)CONNECT_SERVER_USER_CODE);
	uint16_t sign = MODBUS_CRC16_v3(server_info.http_cmd, strlen(server_info.http_cmd));
//	sign = 0x1234;
	memset(server_info.http_cmd, 0, sizeof(server_info.http_cmd));
	sprintf(server_info.http_cmd, "%suserid=%s&sn=%s&sign=%02x%02x&reason=%d", (uint8_t *)SERVER_DOMAIN_NAME, \
			(uint8_t *)CONNECT_SERVER_USER_ID, server_info.sn, sign&0xFF, (sign>>8)&0xFF, server_info.con_server_reason);

	USART1_Bebug_Print("DEBUG", server_info.http_cmd, 1);
	// send data
	uint8_t cmd[30] = {0};
	sprintf(cmd, "AT+QHTTPURL=%d,80\r\n", strlen(server_info.http_cmd));
	EC200U_SendData(cmd, strlen(cmd), 1);
}

void AT_Request_To_Server_Connect_Info_2()
{
	server_info.step 	= GET_SERVER_INFO_2;
	EC200U_SendData(server_info.http_cmd, strlen(server_info.http_cmd), 1);
}

void AT_Request_To_Server_Connect_Info_3()
{
	server_info.step 	= GET_SERVER_INFO_3;
	uint8_t cmd[20] = {0};
	sprintf(cmd, "AT+QHTTPGET=80\r\n");
	EC200U_SendData(cmd, strlen(cmd), 1);
}

void AT_Request_To_Server_Connect_Info_4()
{
	server_info.step 	= GET_SERVER_INFO_4;
	uint8_t cmd[20] = {0};
	sprintf(cmd, "AT+QHTTPREAD=80\r\n");
	EC200U_SendData(cmd, strlen(cmd), 1);
}

/*
 * 参数1：string 解析字符串
 * 参数2：key 关键字
 * 参数3：key_len 关键字长度
 * 参数4：key_type 关键字长度 1--int; 2--string;
 * 参数5：p_ret 反馈字符串
 * 返回：true成功； false失败
 */
#include "board_config.h"
#include "tool.h"
bool From_Json_String_Analysis_Key(const char *string, const char *key, uint8_t key_len, uint8_t key_type, char *p_ret)
{
	char *p_str = NULL;
	uint8_t j1=0, len = strlen(key);
	bool res = false;

//	gcz_serial_v1_dma_printf(SERIAL_UART1,"====%d   [%s]\r\n",key_type,string);

	switch(key_type){
	case 1: {		// int
		len += 2;
		if((p_str = strstr(string, key))){
			for(uint8_t i1=len; i1<len+key_len; i1++){
				if(p_str[i1] != ',' || p_str[i1] != '}'){
					p_ret[j1] = p_str[i1];
					j1++;
				}else break;
			}
			if(j1 > 0) res = true;
		}
	}break;
	case 2:{		// string
		len += 3;
		if((p_str = strstr(string, key))){
			for(uint8_t i1=len; i1<len+key_len; i1++){
				if(p_str[i1] != '\"'){
					p_ret[j1] = p_str[i1];
					j1++;
				}else break;
			}

			if(strncmp(p_ret, "ull", 3) == 0){		// 是null原型，内容为空时平台反馈null
				memset(p_ret, 0, sizeof(p_ret));
			}
			if(j1 > 0) res = true;
		}
	}break;
	default: res = false;break;
	}

	return res;
}
/*
 * 解析平台反馈的连接服务器的信息
 * 参数：平台反馈信息串
 * 返回：true成功；false失败
 */
bool Analysis_Con_Server_Info(uint8_t *string)
{
	// {"code":0,"msg":"中文乱码（UTF-8）不解析","data":{"ip":"39.107.254.216","port":8876,"time":1663053083},"success":true,"count":1}
	if(strlen(string) < 20) return false;
	USART1_Bebug_Print("Con_Server_Info", string, 1);

	char *p_str = NULL;
	bool res = true;

	// 强解析 code
	uint8_t p_code[5] = {0};
	if(From_Json_String_Analysis_Key(string, "code", 5, 1, p_code) == false){
		USART1_Bebug_Print("ERROR", "code error", 1);
		res = false;
	}
	conserver_info.code = atoi(p_code);

	switch(conserver_info.code){
	case 0: break; 	// 成功
	case P_ERR_4001: {	// 用户或密码不对
		USART1_Bebug_Print("ERROR", "User Or Password Is Error.", 1);
		return false;
	}break;
	case P_ERR_5001: {	// SN不存在
		USART1_Bebug_Print("ERROR", "SN Is Not Exist.", 1);
		return false;
	}break;
	case P_ERR_6017: {	// 签名认证失败
		USART1_Bebug_Print("ERROR", "Signature Failed.", 1);
		return false;
	}break;
	case 4088: {	// 权限不足
		USART1_Bebug_Print("ERROR", "Permission Denied.", 1);
		return false;
	}break;
	default:{
		USART1_Bebug_Print_Num("[Out Of Range]code", conserver_info.code, 1, 1);
		return false;
	}break;
	}

	// ip
	if(From_Json_String_Analysis_Key(string, "ip", 20, 2, conserver_info.ip) == false){
		USART1_Bebug_Print("ERROR", "ip error", 1);
		res = false;
	}

	// port
	if(From_Json_String_Analysis_Key(string, "port", 5, 1, p_code) == false ){
		USART1_Bebug_Print("ERROR", "port error", 1);
		res = false;
	}
	conserver_info.port = atoi(p_code);

	// time
	char time_s[16] = {0};
	if(From_Json_String_Analysis_Key(string, "time", 16, 1, time_s) == false){
		USART1_Bebug_Print("ERROR", "time error", 1);
		res = false;
	}
	conserver_info.time = atoi(time_s);

	// msg
//	if(From_Json_String_Analysis_Key(string, "msg", 24, 2, conserver_info.msg) == false){
//		USART1_Bebug_Print("ERROR", "msg error", 1);
//		res = false;
//	}
#if 0
	USART1_Bebug_Print_Num("\r\n[code]", conserver_info.code, 1, 1);
	USART1_Bebug_Print("ip", conserver_info.ip, 1);
	USART1_Bebug_Print_Num("[port]", conserver_info.port, 1, 1);
	USART1_Bebug_Print_Num("[time]", conserver_info.time, 1, 1);
	USART1_Bebug_Print("msg", conserver_info.msg, 1);
#endif

	if(res == true){
		// 同步设备时钟 UTCTIME
		g_utctime = conserver_info.time;
	}
	return res;
}
#endif
/*
 * 发送AT+CPIN?获取SIM卡状态
 */
void AT_CPIN_To_Get_SIM_Status()
{
	server_info.step 	= SIM;

//	Send_Single_Data_By_Queue("AT+CPIN?");
	uint8_t cmd[10] 	= {0};
	sprintf(cmd, "AT+CPIN?\r\n");
	EC200U_SendData(cmd, strlen(cmd), 1);
}
/*
 * 发送AT+CREG?判断GSM网络是否正常
 */
void AT_CREG_To_Judge_GSM()
{
	server_info.step 	= GSM;

//	Send_Single_Data_By_Queue("AT+CGREG?");
	uint8_t cmd[10] 	= {0};
	sprintf(cmd, "AT+CGREG?\r\n");
	EC200U_SendData(cmd, strlen(cmd), 1);
}

/*
 * 关掉回显模式
 */
void AT_ATE0()
{
	server_info.step 	= ATE0;
//	Send_Single_Data_By_Queue("ATE0");
	uint8_t cmd[10] 	= {0};
	sprintf(cmd, "ATE0\r\n");
	EC200U_SendData(cmd, strlen(cmd), 1);
}

/*
 * 发送AT+CPIN?获取SIM卡状态
 */
void AT_QIOPEN_TO_Connect_Server()
{
	server_info.step 		= CONNECT_SERVER;
	server_info.isConnect	= 0;
	uint8_t cmd[150] 		= {0};
#if NTP_ENABLE
	sprintf(cmd, "AT+QIOPEN=1, %d, %s, %s, %d, 0, 1\r\n", CONNECT_ID,  CONNECT_PROTO_TYPE,  CONNECT_IP,  CONNECT_PORT);// 直吐
#else
	sprintf(cmd, "AT+QIOPEN=1,%d,%s,%s,%d,0,1\r\n", CONNECT_ID,  CONNECT_PROTO_TYPE,  conserver_info.ip,  conserver_info.port);// 直吐
//	sprintf(cmd, "AT+QIOPEN=1, %d, %s, %s, %d, 0, 2\r\n", CONNECT_ID,  CONNECT_PROTO_TYPE,  CONNECT_IP,  CONNECT_PORT);// 透传
#endif
//	USART1_Bebug_Print("SERVER", cmd, 1);
	EC200U_SendData(cmd, strlen(cmd), 1);
}
/*
 * 断开与服务器端连接
 */
void AT_QICLOSE_Server()
{
	server_info.step 		= CLOSE_SERVER;
	server_info.isConnect	= 0;
	uint8_t cmd[20] 		= {0};
	Enable_Usart_Interrupt(USART0_SFR, INT_USART0);

	sprintf(cmd, "AT+QICLOSE=%d\r\n",  CONNECT_ID);
//	USART1_Bebug_Print("SERVER", cmd, 1);

	EC200U_SendData(cmd, strlen(cmd), 1);
//	Send_Single_Data_By_Queue(cmd);
}

/*
 * 将时间字符串转成UTC时间
 * 参数：时间字符串："2021/11/01, 09:50:40+00"
 * 返回：UTC时间
 */
bool TimeString_To_UTCTIME(char * time_string,  uint8_t timeZone)
{
	//gcz 2022-06-27   修改year防止显示异常
	uint8_t year_c[5], mon_c[3], day_c[3], hour_c[3], min_c[3], second_c[3];
	// fprintf(USART1_STREAM, "123>%s\r\n", time_string);
	// 22/01/20,12:41:01+00"OK
	// 2022/01/20,12:41:01+00"
	uint8_t time_str[30] = {0};
	uint8_t j = 0;
	for(uint8_t i=0; i<strlen(time_string); i++){
		if(time_string[i] == ' '){

		}else{
			time_str[j] = time_string[i];
			j++;
		}
	}
	char* p = strtok(time_str, ",");
	char p1[20] = { 0 };
	memcpy(p1, p, strlen(p));

	p = strtok(NULL, ",");
	char p2[20] = { 0 };
	memcpy(p2, p, strlen(p));

	char* p1p = strtok(p1, "/");
	strcpy(year_c, p1p);

	p1p = strtok(NULL, "/");
	strcpy(mon_c, p1p);

	p1p = strtok(NULL, "/");
	strcpy(day_c, p1p);

	char *p2p = strtok(p2, ":");
	strcpy(hour_c, p2p);

	p2p = strtok(NULL, ":");
	strcpy(min_c, p2p);

	p2p = strtok(NULL, "+");
	strcpy(second_c, p2p);

	TimeType local_t;
	uint16_t year_i = atoi(year_c);
	if( year_i < 100){
		local_t.tm_year = year_i + 2000;
	}else{
		local_t.tm_year = year_i;//+2000
	}
	local_t.tm_mon 	= atoi(mon_c);
	local_t.tm_mday = atoi(day_c);
	local_t.tm_hour = atoi(hour_c) + timeZone;
	local_t.tm_min 	= atoi(min_c);
	local_t.tm_sec 	= atoi(second_c);
	// 若时间超出了24h，就天数加1，时间-24h；若2月份超出了28天，月份加1，
	// 闰年
	bool runNian = false;
	uint8_t february = 28;
	if (((local_t.tm_year%100 != 0) && (local_t.tm_year%4 == 0)) || (local_t.tm_year%400 == 0)){
		runNian = true;
		february = 29;
	}
	// 计算
	if(local_t.tm_hour >= 24){
		local_t.tm_hour -= 24;	// 小时-24

		if(local_t.tm_mon == 2){
			if(local_t.tm_mday == february){
				local_t.tm_mon += 1;
				local_t.tm_mday = 1;
			}else{
				local_t.tm_mday += 1;
			}
		}
		// 4 6 9 11---30
		if(local_t.tm_mon==4 || local_t.tm_mon==6 || local_t.tm_mon==9 /*|| local_t.tm_mon==10*/ \
				|| local_t.tm_mon==11){
			if(local_t.tm_mday == 30){
				local_t.tm_mon += 1;
				local_t.tm_mday = 1;
			}else{
				local_t.tm_mday += 1;
			}
		}
		// 1 3 5 7 8 10---31
		if(local_t.tm_mon==1 || local_t.tm_mon==3 || local_t.tm_mon==5 || local_t.tm_mon==7 \
				|| local_t.tm_mon==8 || local_t.tm_mon==10){
			if(local_t.tm_mday == 31){
				local_t.tm_mon += 1;
				local_t.tm_mday = 1;
			}else{
				local_t.tm_mday += 1;
			}
		}
		// 12
		if(local_t.tm_mon==12){
			if(local_t.tm_mday == 31){
				local_t.tm_year += 1;
				local_t.tm_mon = 1;
				local_t.tm_mday = 1;
			}else{
				local_t.tm_mday += 1;
			}
		}
	}

	// AT+CCLK?+CCLK: "22/01/20, 12:41:01+00"OK
	uint8_t time_p[30] = {0};
	sprintf(time_p, "%d-%02d-%02d %02d:%02d:%02d\n", local_t.tm_year, local_t.tm_mon, local_t.tm_mday,
			local_t.tm_hour, local_t.tm_min, local_t.tm_sec);
	USART1_Bebug_Print("NET TIME", time_p, 1);

	uint32_t utctime_t = alg_LocalTime2Utc(local_t, timeZone);
	if(utctime_t > 1652425793){				// 2022/5/13 15:9:53
		g_utctime = utctime_t;
		return true;
	}

	utctime_t = 0;

	return false;
}

/*
 * 设备登录平台的操作
 */
uint8_t ota_loop_err_times 	= 0;
uint8_t _4G_module_err_times = 0;
void Device_Login_Platform()
{
	server_info.err_loop_times 	= 0;
	_4G_module_err_times 		= 0;
	// 若无秘钥就获取，否则不获取
	if(strlen(server_info.secret) >= 6){
		Request_Register_Device_CMD_ASCII();
	}else
		Request_Get_Secret_CMD_ASCII();		// 获取秘钥
}
/*
 * 0:未完成
 * 1:连接成功；
 * 2:重连
 */
uint8_t _4G_Connect_Server_ASCII()
{
	// 二进制方式连接服务器
	static char *strx = NULL;
	// Json方式连接服务器
	switch(server_info.step)
	{
	case EC200U_RESTART:{
		if(_4g_message_state == _4G_RDY){
			_4g_message_state = _4G_WAIT;
			server_info.err_loop_times = 0;
			AT_CPIN_To_Get_SIM_Status();
			return 0;
		}
		// 拉低3.5秒 拉高500ms 拉低1秒 使能
		if(server_info.err_loop_times == 0){
			EC200U_POWERKEY(Bit_RESET);
		}if(server_info.err_loop_times == 14){		// 关机操作1：拉低3.5秒PWRKEY
			EC200U_POWERKEY(Bit_SET);
		}else if(server_info.err_loop_times == 16){	// 关机操作2：拉高500ms PWRKEY
			EC200U_POWERKEY(Bit_RESET);
		}else if(server_info.err_loop_times == 20){
			// 状态切换到未连接
			server_info.isConnect = 0;
			signal_strength.isStart = 0;
			gps_info.isConnect 	= 0;
			Module_4G_Software_Start();			// 重新使能
			EC200U_POWERKEY(Bit_SET);
		}else if(server_info.err_loop_times == 21){
			EC200U_POWERKEY(Bit_RESET);
		}else if(server_info.err_loop_times == 29){
			EC200U_POWERKEY(Bit_SET);
			AT();
			server_info.err_loop_times = 0;
			return 0;
		}
		server_info.err_loop_times++;
	}break;
	case ATCMD:{
		if(_4g_message_state == _4G_RDY || _4g_message_state == _4G_OK){
			_4g_message_state	= _4G_WAIT;
			ota_loop_err_times 	= 0;
			AT_CPIN_To_Get_SIM_Status();
			return 0;
		}
		// 异常ERROR
		if(_4g_message_state == _4G_CME_ERROR || _4g_message_state == _4G_CMS_ERROR){
			_4g_message_state 	= _4G_WAIT;
			AT();
			return 0;
		}

		// 若10秒未能接收到AT命令执行结果，重启模块
		if(ota_loop_err_times >= 10){
			ota_loop_err_times = 0;
			server_info.err_loop_times = 20;
			server_info.step = EC200U_RESTART;

			USART1_Bebug_Print("AT", "(1)Check Whether The 4G Card Is Properly Inserted;", 1);
			USART1_Bebug_Print("AT", "(2)Whether The Device SN Has Been Registered On The Platform.", 1);
			return 0;
		}

		// 超时判断
		if(server_info.err_loop_times > 3){						// 等待1S (4*250ms = 1S)
			server_info.err_loop_times = 0;
			ota_loop_err_times++;
			AT();
		}else server_info.err_loop_times++;
	}break;
	case SIM:{
		if(_4g_message_state == _4G_READY){
			_4g_message_state 	= _4G_WAIT;
			server_info.err_loop_times 		= 0;
			AT_CREG_To_Judge_GSM();					// 判断GSM网络注册正常
			return 0;
		}

		if(_4g_message_state == _4G_ERROR_10){
			_4g_message_state = _4G_WAIT;
			server_info.err_loop_times++;
			if(server_info.err_loop_times > 20){
				server_info.err_loop_times = 20;
				server_info.step = EC200U_RESTART;
				return 0;
			}
		}

		// 若60秒未能接收到AT命令执行结果，重启模块
		if(ota_loop_err_times >= 20){
			ota_loop_err_times 	= 0;
			server_info.err_loop_times 		= 20;
			server_info.step 	= EC200U_RESTART;

			USART1_Bebug_Print("AT", "(1)Check Whether The 4G Card Is Properly Inserted;", 1);
			USART1_Bebug_Print("AT", "(2)Whether The Device SN Has Been Registered On The Platform.", 1);
			return 0;
		}

		// 超时判断
		if(server_info.err_loop_times > 3){						// 等待1S (4*250ms = 1S)
			server_info.err_loop_times = 0;
			ota_loop_err_times++;
			AT_CPIN_To_Get_SIM_Status();
		}else server_info.err_loop_times++;
	}break;
	case GSM:{
		if(_4g_message_state == _4G_GSM){
			_4g_message_state 	= _4G_WAIT;
			ota_loop_err_times 	= 0;
			server_info.err_loop_times 		= 0;
#if NTP_ENABLE
			AT_NTP();								// 获取网络时间
#else
			AT_Request_To_Server_Connect_Info_1();	// 主动获取服务器的连接信息
#endif
			return 0;
		}

		// 检测SIM的状态
		if(_4g_message_state == _4G_GSM_NO_RIGISTER){
			_4g_message_state 	= _4G_WAIT;
			server_info.err_loop_times 		= 0;

			if(ota_loop_err_times > 20){
				ota_loop_err_times = 0;
				USART1_Bebug_Print("ERROR", "The SIM Card Is Not Registered ", 1);
			}else{
				AT_CREG_To_Judge_GSM();
				ota_loop_err_times ++;
			}
		}

		// 超时判断
		if(server_info.err_loop_times > 3){						// 等待1S (4*250ms = 1S)
			server_info.err_loop_times = 0;
			AT_CREG_To_Judge_GSM();
		}else server_info.err_loop_times++;
	}break;
#if NTP_ENABLE			// 20220913 lmz note	被主动获取服务器的信息替换掉了，因为中途能获取平台的时间信息
	case NTP:{
		// 超时判断
		if(server_info.err_loop_times > 3){						// 等待1S (4*250ms = 1S)
			server_info.err_loop_times = 0;
			AT_NTP();
		}else server_info.err_loop_times++;
	}break;
#else			// 20220913 lmz add 将连接平台的服务器信息改成获取方式（通过HTTP GET READ）
	case GET_SERVER_INFO_1:{	// 发送QHTTPURL
		if(server_info.err_loop_times > 7){						// 等待2S (8*250ms = 1S)
			server_info.err_loop_times = 0;
			AT_Request_To_Server_Connect_Info_1();
		}else server_info.err_loop_times++;
	}break;
	case GET_SERVER_INFO_2:{	// 发送URL
		if(server_info.err_loop_times > 7){						// 等待2S (8*250ms = 1S)
			server_info.err_loop_times = 0;
			AT_Request_To_Server_Connect_Info_1();
		}else server_info.err_loop_times++;
	}break;
	case GET_SERVER_INFO_3:{	// 发送QHTTPGET
		if(server_info.err_loop_times > 7){						// 等待2S (8*250ms = 1S)
			server_info.err_loop_times = 0;
			AT_Request_To_Server_Connect_Info_1();
		}else server_info.err_loop_times++;
	}break;
	case GET_SERVER_INFO_4:{	// 发送QHTTPREAD
		if(server_info.err_loop_times > 7){						// 等待2S (8*250ms = 1S)
			server_info.err_loop_times = 0;
			AT_Request_To_Server_Connect_Info_1();
		}else server_info.err_loop_times++;
	}break;
#endif
	case ATE0:{
		if(_4g_message_state == _4G_OK){
			_4g_message_state 	= _4G_WAIT;
			server_info.err_loop_times 		= 0;
			ota_loop_err_times 	= 0;
			AT_QIOPEN_TO_Connect_Server();			// 连接服务器
		}

		// 超时判断
		if(server_info.err_loop_times > 3){						// 等待1S (4*250ms = 1S)
			server_info.err_loop_times = 0;
			AT_ATE0();
		}else server_info.err_loop_times++;

	}break;
	case CONNECT_SERVER:{
		if(_4g_message_state == _4G_ERROR_561){		// socket连接失败
			_4g_message_state 	= _4G_WAIT;
			server_info.err_loop_times 		= 0;
			AT_QIOPEN_TO_Connect_Server();
			return 0;
		}

		// 异常处理
		if(_4g_message_state == _4G_ERROR_566 || _4g_message_state == _4G_ERROR_563){	// socket连接失败
			_4g_message_state 	= _4G_WAIT;
			server_info.err_loop_times 		= 0;
			AT_QICLOSE_Server();
			return 0;
		}

		if(_4g_message_state == _4G_ERROR_567){		// socket被关闭
			_4g_message_state 		= _4G_WAIT;
			// 重启模块
			server_info.err_loop_times 			= 20;
			server_info.step 		= EC200U_RESTART;
			return 0;
		}

//		if(_4g_message_state == _4G_CON_SERVER_OK){
//			_4g_message_state 		= _4G_WAIT;
//			Device_Login_Platform();
//
//			return 0;
//		}

		// 100秒后就软重启4G模块
		if(_4G_module_err_times == 4){
			_4G_module_err_times 	= 0;
			server_info.err_loop_times 			= 20;
			server_info.step 		= EC200U_RESTART;
			return 0;
		}

		// 连接服务器超时判断，50秒后就重启连服务器
		if(_4G_module_err_times == 2 ){
			_4G_module_err_times = 0;
			USART1_Bebug_Print("Connect Server", "Connect Server a Timeout[50S], Reconnect Server.", 1);
			return 2;
		}

		// 超时判断
		if(server_info.err_loop_times > 98){					// 等待25S (100*250 = 25S)
			server_info.err_loop_times = 0;
			_4G_module_err_times++;
			AT_QIOPEN_TO_Connect_Server();
		}else server_info.err_loop_times++;
	}break;
	case CLOSE_SERVER:{
		if(_4g_message_state == _4G_ERROR_567){		// socket被关闭
			_4g_message_state = _4G_WAIT;

			// 重启模块
			server_info.err_loop_times 			= 20;
			server_info.step 		= EC200U_RESTART;
			server_info.isConnect 	= 0;
			return 0;
		}

		if(_4g_message_state == _4G_OK || _4g_message_state == _4G_PDP_CLOSE){			// close server success
			_4g_message_state 	= _4G_WAIT;
			server_info.err_loop_times 		= 0;
			// connect server
			if(server_info.reconnect_server_position == 1) AT();
			else AT_QIOPEN_TO_Connect_Server();
			return 0;
		}

		if(_4g_message_state == _4G_ERROR){			// 收到ERROR，说明4G模块与服务器已经断开，直接重连
			server_info.err_loop_times 		= 0;
			_4g_message_state 	= _4G_WAIT;
			AT_QIOPEN_TO_Connect_Server();
			return 0;
		}

//		if(_4g_message_state == _4G_CON_SERVER_OK){
//			_4g_message_state 		= _4G_WAIT;
//			Device_Login_Platform();
//
//			return 0;
//		}

		// 超时后软重启4G模块,30S
		if(_4G_module_err_times > 2){
			_4G_module_err_times = 0;
			server_info.err_loop_times 		= 20;
			server_info.step 	= EC200U_RESTART;
			return 0;
		}

		// 超时判断
		if(server_info.err_loop_times > 38)	{					// 等待10S (40*250 = 10S)
			server_info.err_loop_times = 0;
			_4G_module_err_times++;

			AT_QICLOSE_Server();
		}else server_info.err_loop_times++;
	}break;
	case SECRET:{
		// 异常处理
		if(_4g_message_state == _4G_INVALIED_REQ){	// Invalid Request
			_4g_message_state = _4G_WAIT;
			if(ota_loop_err_times > 3){
				ota_loop_err_times = 0;
				USART1_Bebug_Print("SECRET", "The Device Not Registered.[1S]", 1);
				memset(server_info.secret, 0, strlen(server_info.secret));		// 清空秘钥，重新获取

				return 2;
			}else ota_loop_err_times++;

			return 0;
		}

		if(_4g_message_state == _4G_SECRET_OK){
			_4g_message_state 	= _4G_WAIT;
			ota_loop_err_times 	= 0;
			server_info.err_loop_times 		= 0;
			USART1_Bebug_Print("SECRET", server_info.secret, 1);
			// write secret
			if(!Set_Secret(server_info.secret)){
				Set_Secret(server_info.secret);
			}
			Request_Register_Device_CMD_ASCII();	// 注册设备
		}

		// 重连10秒后，就重连服务器
		if(ota_loop_err_times > 10){
			ota_loop_err_times = 0;
			USART1_Bebug_Print("SECRET", "Secret a Timeout, Reconnect Server.[10S]", 1);
			return 2;
		}

		// 超时判断
		if(server_info.err_loop_times > 3){	// 等待1S (4*250ms = 1S)
			server_info.err_loop_times = 0;
			ota_loop_err_times++;
			Request_Get_Secret_CMD_ASCII();
		}else server_info.err_loop_times++;
	}break;
	case REGISTER:{
		// 异常处理
		if(_4g_message_state == _4G_INVALIED_REQ){	// Invalid Request
			_4g_message_state = _4G_WAIT;

			if(ota_loop_err_times > 3){
				ota_loop_err_times = 0;
				USART1_Bebug_Print("REGISTER", "The Device Not Registered.[1S]", 1);
				memset(server_info.secret, 0, strlen(server_info.secret));		// 清空秘钥，重新获取

				return 2;
			}else ota_loop_err_times++;
		}

		if(_4g_message_state == _4G_ITINERART_ID_OK){
			_4g_message_state 	= _4G_WAIT;
			server_info.step 	= WAIT;
			server_info.err_loop_times 		= 0;

			return 1;
		}
		// 重连10秒后，就重连服务器
		if(ota_loop_err_times > 10){
			ota_loop_err_times = 0;
			USART1_Bebug_Print("REGISTER", "Registered a Timeout, Reconnect Server.[10S]", 1);
			return 2;
		}

		// 重连10秒后，就重连服务器
		if(ota_loop_err_times > 10){
			ota_loop_err_times = 0;
			USART1_Bebug_Print("REGISTER", "Registered a Timeout, Reconnect Server.", 1);
			return 2;
		}

		// 超时判断
		if(server_info.err_loop_times > 3){	// 等待1S (4*250ms = 1S)
			server_info.err_loop_times = 0;
			ota_loop_err_times++;
			Request_Register_Device_CMD_ASCII();
		}else server_info.err_loop_times++;
	}break;
	default:break;
	}
	return 0;
}


bool Request_Get_Secret_CMD_ASCII()
{
	memset(&nodeData_server_share, 0, sizeof(NodeData_S));

	static uint16_t secret_seqNum = 0;
	if(secret_seqNum > UPLOAD_MAX_SEQ_NUM) secret_seqNum = 1;
	else secret_seqNum++;

	// 填数
	if(!Request_Protocal_Head_ASCII(secret_seqNum,  SECRET_FRAME,  nodeData_server_share.data)) return false;

	server_info.step = SECRET;
	server_info.step_step = 1;

	req_index = 31;

	// userid
//	uint8_t userid[10] = "zkhyabc";
	Set_Request_ASCII_Info_Template(&nodeData_server_share, (uint8_t *)CONNECT_SERVER_USER_ID, CHAR, 10);
	// passwd
//	uint8_t passwd[16] = "cbacba";
	Set_Request_ASCII_Info_Template(&nodeData_server_share, (uint8_t *)CONNECT_SERVER_USER_CODE, CHAR, 16);
	// 校验
	Set_Request_ASCII_Info_Template(&nodeData_server_share,  "",  CRC16,  2);
	// 放到队列中进行发送
	sprintf(nodeData_server_share.data_info,  "AT+QISEND=%d, %d\r\n",  CONNECT_ID,  nodeData_server_share.data_len);
	 
	Usart0_DMA_Transmit(nodeData_server_share.data_info, strlen(nodeData_server_share.data_info));
	delay_ms(1);
	Usart0_DMA_Transmit(nodeData_server_share.data, nodeData_server_share.data_len);

	return true;
}


bool Request_Register_Device_CMD_ASCII()
{
	memset(&nodeData_server_share, 0, sizeof(NodeData_S));

	static uint16_t register_seqNum = 0;
	if(register_seqNum > UPLOAD_MAX_SEQ_NUM) register_seqNum = 1;
	else register_seqNum++;

	// 填数
	if(!Request_Protocal_Head_ASCII(register_seqNum, REGISTR_FRAME, nodeData_server_share.data)) return false;	// 29

	server_info.step = REGISTER;
	server_info.step_step = 1;

	// 开始下标
	req_index = 31;

	// 内容
	// sign签名 = CRC16_Modbus(SN+UTCTIME+secret)
	uint8_t chk[50] = {0};
	sprintf(chk,  "%s%ld%s",  server_info.sn,  g_utctime,  server_info.secret);
	uint16_t sign = MODBUS_CRC16_v3(chk,  strlen(chk));
	uint8_t *sign_t = (uint8_t *)&sign;
	Set_Request_ASCII_Info_Template(&nodeData_server_share,  sign_t,  UINT_16,  2);
	// 车类型
	uint8_t vehicle_type = server_info.vehicle_type;
	Set_Request_ASCII_Info_Template(&nodeData_server_share,  &vehicle_type,  UINT_8,  1);

	Set_Request_ASCII_Info_Template(&nodeData_server_share,  &vehicle_type,  CRC16,  2);
	// 放到队列中进行发送
	sprintf(nodeData_server_share.data_info,  "AT+QISEND=%d, %d\r\n",  CONNECT_ID,  nodeData_server_share.data_len);

	// test print
//	fprintf(USART1_STREAM, "\r\n-----------Request_Register-----start--------------\r\n ");
//	for(uint16_t i=0; i<nodeData_server_share.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData_server_share.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n------------Request_Register-----end-------------\r\n ");

	Usart0_DMA_Transmit(nodeData_server_share.data_info, strlen(nodeData_server_share.data_info));
	delay_ms(1);
	Usart0_DMA_Transmit(nodeData_server_share.data, nodeData_server_share.data_len);

	return true;
}

bool Analysis_Reply_Secret_ASCII(uint8_t *data,  uint16_t contentLen)
{
	// 内容
	uint8_t secret[SECRET_SIZE] = {0};
	memcpy(secret,  data,  contentLen);
	secret[contentLen] = '\0';

	memcpy(server_info.secret,  secret,  sizeof(secret));
	// 存储secret
	if(strlen(secret) >= 6){
		_4g_message_state = _4G_SECRET_OK;
		return true;
	}

	return false;
}

bool Analysis_Reply_Device_Register_ASCII(uint8_t *data,  uint16_t contentLen)
{
	// 内容
	memcpy(server_info.itinerary_id,  data,  contentLen);
	server_info.itinerary_id[contentLen] = '\0';
//	fprintf(USART1_STREAM,  "itinerary_id:%s\r\n",  server_info.itinerary_id);
	if(strlen(server_info.itinerary_id) >= 12){
		_4g_message_state = _4G_ITINERART_ID_OK;
		return true;
	}
	return false;
}

/*
 * 將时间改成"21/11/01, 09:50:40+00"，直接调用函数
 * 根据经度，确定时区
 */
bool GPS_Time_To_UTCTIME(uint8_t *dmy,  uint8_t *hms,  uint8_t *lon,  uint8_t *ew)
{
	// 160322
	// 062407.00
	uint8_t gpsTime[30] = {0};
	uint8_t year[3] 	= {0};
	uint8_t month[3] 	= {0};
	uint8_t day[3] 		= {0};
	uint8_t minute[3] 	= {0};
	uint8_t hour[3]		= {0};
	uint8_t second[3] 	= {0};

	strncpy(year, dmy+4, 2); 	year[2] 	= '\0';
	strncpy(month, dmy+2, 2); 	month[2] 	= '\0';
	strncpy(day, dmy, 2); 		day[2] 		= '\0';

	strncpy(hour, hms, 2); 		hour[2] 	= '\0';
	strncpy(minute, hms+2, 2); 	minute[2] 	= '\0';
	strncpy(second, hms+4, 2); 	second[2] 	= '\0';

	sprintf(gpsTime, "%d/%d/%d, %d:%d:%d+00", atoi(year), atoi(month), atoi(day), atoi(hour), atoi(minute), atoi(second));
//	fprintf(USART1_STREAM,  "%s\r\n",  gpsTime);
	float longtitude 	= atof(lon) / 100;
	int8_t timeZone 	= 0;

	// 正值--东经;负值--西经
	if(ew[0] != 'E'){
		longtitude = 0 - longtitude;
	}

//	fprintf(USART1_STREAM,  "lon:%10.5f, %s, %s\r\n",  longtitude,  lon,  ew);
	if(longtitude>-7.5 && longtitude<=7.5){
		timeZone = 0;
	}else if(longtitude>7.5 && longtitude<22.5){
		timeZone = 1;
	}else if(longtitude>22.5 && longtitude<37.5){
		timeZone = 2;
	}else if(longtitude>37.5 && longtitude<52.5){
		timeZone = 3;
	}else if(longtitude>52.5 && longtitude<67.5){
		timeZone = 4;
	}else if(longtitude>67.5 && longtitude<82.5){
		timeZone = 5;
	}else if(longtitude>82.5 && longtitude<97.5){
		timeZone = 6;
	}else if(longtitude>97.5 && longtitude<112.5){
		timeZone = 7;
	}else if(longtitude>112.5 && longtitude<127.5){
		timeZone = 8;
	}else if(longtitude>127.5 && longtitude<142.5){
		timeZone = 9;
	}else if(longtitude>142.5 && longtitude<157.5){
		timeZone = 10;
	}else if(longtitude>157.5 && longtitude<172.5){
		timeZone = 11;
	}
	else if(longtitude>172.5 && longtitude<=180.0 || longtitude>-180.0 && longtitude<=-172.5){
		timeZone = -11;
	}else if(longtitude>-172.5 && longtitude<=-157.5){
		timeZone = -10;
	}else if(longtitude>-157.5 && longtitude<=-142.5){
		timeZone = -9;
	}else if(longtitude>-127.5 && longtitude<=-127.5){
		timeZone = -8;
	}else if(longtitude>-112.5 && longtitude<=-112.5){
		timeZone = -7;
	}else if(longtitude>-97.5 && longtitude<=-82.5){
		timeZone = -6;
	}else if(longtitude>-82.5 && longtitude<=-67.5){
		timeZone = -5;
	}else if(longtitude>-67.5 && longtitude<=-52.5){
		timeZone = -4;
	}else if(longtitude>-52.5 && longtitude<=-37.5){
		timeZone = -3;
	}else if(longtitude>-37.5 && longtitude<=-22.5){
		timeZone = -2;
	}else if(longtitude>-22.5 && longtitude<=-7.5){
		timeZone = -1;
	}

	if(TimeString_To_UTCTIME(gpsTime, timeZone) == false) return false;

	return true;
}



/*
 * 在USART0中断函数中 ，接收连接服务器的信息解析
 * 参数：接收到结束符的字符串
 * 说明：仅在未连接服务器时，执行
 */
bool _4G_Recv_Connect_Server_Message(uint8_t *string)
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return false;

	// 连接服务器直接返回
	if(server_info.isConnect) return false;

	char * strx = NULL;

//	USART1_Bebug_Print("ConServer", string, 1);
	switch(server_info.step)
	{
	case EC200U_RESTART:{
		if(strstr((const char*)string, (const char*)"RDY")){
			_4g_message_state = _4G_RDY;
			return true;
		}
	}break;
	case ATCMD:{
		if(strstr((const char*)string, (const char*)"RDY")){
			_4g_message_state = _4G_RDY;
			return true;
		}
		// OK
		if(strstr((const char*)string, (const char*)"OK")){
			_4g_message_state = _4G_OK;
			return true;
		}
		// CME ERROR
		if(strstr((const char*)string, (const char*)"CME ERROR")){
			_4g_message_state = _4G_CME_ERROR;
			USART1_Bebug_Print("AT", string, 1);
			return true;
		}
		// CMS ERROR
		if(strstr((const char*)string, (const char*)"CMS ERROR")){
			_4g_message_state = _4G_CMS_ERROR;
			USART1_Bebug_Print("AT", string, 1);
			return true;
		}
	}break;
	case SIM:{
		if(strstr((const char*)string, (const char*)"+CPIN: READY")){
			_4g_message_state = _4G_READY;
			return true;					// 判断GSM网络注册正常
		}else if(strstr((const char*)string, (const char*)"+CME ERROR: 10")){
			_4g_message_state = _4G_ERROR_10;
			return true;
		}
	}break;
	case GSM:{
		if(strstr((const char*)string, (const char*)"+CGREG: 0,1")){
			_4g_message_state = _4G_GSM;
			return true;
		}
		if(strstr((const char*)string, (const char*)"+CGREG: 0,5")){
			_4g_message_state = _4G_GSM;
			return true;
		}
		if(strstr((const char*)string, (const char*)"+CGREG: 0,0")){
			_4g_message_state = _4G_GSM_NO_RIGISTER;
			return true;
		}
		if(strstr((const char*)string, (const char*)"+CGREG: 0,2")){
			_4g_message_state = _4G_GSM_NO_RIGISTER;
			return true;
		}
	}break;
#if NTP_ENABLE		// 20220913 lmz note	被主动获取服务器的信息替换掉了，因为中途能获取平台的时间信息
	case NTP:{
		if(strx = strstr((const char*)string, (const char*)"+QNTP:")){
			// +QNTP: 0,"2022/01/20,12:41:01+00"
			if(strlen(strx) <= 40){
				if(TimeString_To_UTCTIME(strx + 10, 8)){	// 解析utctime

					Send_Config_AGPS();
					Send_Enable_AGPS();
					Send_Enable_GNSS();

					AT_ATE0();								// 关闭回显模式
					server_info.err_loop_times 	= 0;
					strx = NULL;
					return true;
				}else AT_NTP();

				strx = NULL;
				return false;
			}
			strx = NULL;
		}
	}break;
#else		//  20220913 lmz add 将连接平台的服务器信息改成获取方式（通过HTTP GET READ）
	case GET_SERVER_INFO_1:{
		if(strstr((const char*)string, (const char*)"CONNECT")){
			server_info.err_loop_times = 0;
			AT_Request_To_Server_Connect_Info_2();
			return true;
		}
	}break;
	case GET_SERVER_INFO_2:{
		if(strstr((const char*)string, (const char*)"OK")){
			server_info.err_loop_times = 0;
			AT_Request_To_Server_Connect_Info_3();
			return true;
		}
	}break;
	case GET_SERVER_INFO_3:{
		if(strstr((const char*)string, (const char*)"+QHTTPGET:")){
			server_info.err_loop_times = 0;
			AT_Request_To_Server_Connect_Info_4();
			return true;
		}
	}break;
	case GET_SERVER_INFO_4:{		// 获取连接平台的信息 20220913 lmz add
		if(Analysis_Con_Server_Info(string) == true){
			Send_Config_AGPS();
			Send_Enable_AGPS();
			Send_Enable_GNSS();

			AT_ATE0();								// 关闭回显模式
			server_info.err_loop_times 	= 0;
			return true;
		}else{
			return false;
		}
	}break;
#endif
	case ATE0:{
		if(strstr((const char*)string, (const char*)"OK")){
			_4g_message_state = _4G_OK;
			return true;
		}
	}break;
	case CONNECT_SERVER:{
		if(strstr((const char*)string, (const char*)"+QIOPEN: 0,561")){	// 打开PDP场景失败
			USART1_Bebug_Print("CONNECT SERVER", string, 1);
			_4g_message_state = _4G_ERROR_561;
			return true;
		}

		if(strstr((const char*)string, (const char*)"+QIOPEN: 0,563")){	// socket被占用
			USART1_Bebug_Print("CONNECT SERVER", string, 1);
			_4g_message_state = _4G_ERROR_563;
			return true;
		}

		if(strstr((const char*)string, (const char*)"+QIOPEN: 0,0")){	// 连接server成功
			USART1_Bebug_Print("CONNECT SERVER", string, 1);
			_4g_message_state = _4G_WAIT;
			server_info.offline_times = 0;
			Device_Login_Platform();
			return true;
		}

		if(strstr((const char*)string, (const char*)"+QIOPEN: 0,566")){	// socket 连接失败
			USART1_Bebug_Print("CONNECT SERVER", string, 1);
			_4g_message_state = _4G_ERROR_566;
			return true;
		}

		if(strstr((const char*)string, (const char*)"+QIOPEN: 0,567")){	// socket被关闭
			USART1_Bebug_Print("CONNECT SERVER", string, 1);
			_4g_message_state = _4G_ERROR_567;
			return true;
		}

		if(strstr((const char*)string, (const char*)"+QIOPEN: 0,5")){	// 未检测到的异常充当567
			USART1_Bebug_Print("CONNECT SERVER", string, 1);
			_4g_message_state = _4G_ERROR_567;
			return true;
		}
	}break;
	case CLOSE_SERVER:{
		if(strstr((const char*)string, (const char*)"OK")){ // _4G_OK
			USART1_Bebug_Print("CLOSE SERVER", string, 1);
			_4g_message_state = _4G_OK;
			return true;
		}

		// ERROR
		if(strstr((const char*)string, (const char*)"ERROR")){
			USART1_Bebug_Print("CLOSE SERVER", string, 1);
			_4g_message_state = _4G_ERROR_561;//_4G_ERROR;
			return true;
		}

		// +QIURC: "pdpdeact"
		if(strstr((const char*)string, (const char*)"+QIURC: \"pdpdeact\"")){
			USART1_Bebug_Print("CLOSE SERVER", string, 1);
			_4g_message_state = _4G_PDP_CLOSE;
			return true;
		}

		// +QIURC: "closed"
		if(strstr((const char*)string, (const char*)"+QIURC: \"closed\"")){
			USART1_Bebug_Print("CLOSE SERVER", string, 1);
			_4g_message_state = _4G_PDP_CLOSE;
			return true;
		}

		if(strstr((const char*)string, (const char*)"+QIOPEN: 0,0")){	// 连接server成功
			USART1_Bebug_Print("CLOSE SERVER", string, 1);
			_4g_message_state = _4G_WAIT;
			server_info.offline_times = 0;
			Device_Login_Platform();
			return true;
		}
	}break;
	case SET_BAUD:{	// 连接服务器之前的操作(EC200U自检)
//		USART1_Bebug_Print("4G BIT", string, 1);
		// 自动检测4G模块通讯波特率
		if(strstr((const char*)string, (const char*)"+IPR: 230400")){
			USART1_Bebug_Print("4G BIT", string, 1);
			find_ec200u_baud = true;
			return true;
		}else if(strstr((const char*)string, (const char*)"+IPR: 115200")){
			USART1_Bebug_Print("4G BIT", string, 1);
			find_ec200u_baud = true;
			return true;
		}else if(strstr((const char*)string, (const char*)"+IPR: 38400")){
			USART1_Bebug_Print("4G BIT", string, 1);
			find_ec200u_baud = true;
			return true;
		}else if(strstr((const char*)string, (const char*)"+IPR: 19200")){
			USART1_Bebug_Print("4G BIT", string, 1);
			find_ec200u_baud = true;
			return true;
       }

		if(strstr((const char*)string, (const char*)"OK")){
			USART1_Bebug_Print("4G BIT", string, 1);
			_4g_message_state = _4G_OK;
			return true;
		}
	}break;
	default:break;
	}

	return false;
}

