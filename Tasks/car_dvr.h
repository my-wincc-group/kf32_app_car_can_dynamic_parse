/*
 * car_dvr.h
 *
 *  Created on: 2022-8-8
 *      Author: yanjie
 */

#ifndef CAR_DVR_H_
#define CAR_DVR_H_

#include "_4g_para_config.h"
#include "modbus_crc16.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "cqueue.h"
#include "upgrade_common.h"
#include "EC200U.h"
#include "at_parse_alg_para.h"
#include "gpio.h"
#include "usart.h"
#include "_4g_server.h"
#include "data_loss_prevention.h"
#include "aeb_sensor_management.h"


typedef enum{
	IDLE = 0,
	D1,
	E1,
	PING,
	ERRO,
}CAR_DVR_STAGE_t;
// 行车记录仪指令状态
#define _CAR_DVR_D1_COUNT	60  //D1指令缓存60条

//记录仪故障状态码
typedef enum _CAR_DVR_ERR_ID{		//描述									是否断开连接
	C_ERR_NO,						//成功										否
	C_ERR_NO_ACK,
	C_ERR_3000 = 3000,				//未登录                                  	是
	C_ERR_3001,						//MODEM注册，提供的deviceId不存在         	是
	C_ERR_3002,						//MODEM注册，userId或password错误         	是
	C_ERR_3003,						//MODEM注册，没有绑定指定deviceId的权限   	是
	C_ERR_3004,						//MODEM登录，deviceId不存在               	是
	C_ERR_3005,						//MODEM登录，secret失效或错误需重新注册   	是
	C_ERR_3006,						//按照协议解析数据失败                    	否
	C_ERR_3007,						//不支持的操作                            	否
	C_ERR_3008,						//附件大小超过限制                        	否
	C_ERR_4004 = 4004,				//数据不存在                              	否
	C_ERR_5000 = 5000,				//未插入存储卡                    不发送D1指令
	C_ERR_5001, 					//未插入SIM卡		不发送D1指令
	C_ERR_5002, 					//无法连接网络		不发送D1指令
	C_ERR_5003,  					//未定位		            可以发送D1指令
	C_ERR_5004, 					//录像状态异常		不发送D1指令
	C_ERR_5005, 					//存储卡读写异常		不发送D1指令
}Car_Dvr_ERR_ID;



typedef struct  _CarDvr_Cfg
{
	uint8_t from;                //指令来源
	uint16_t record_seconds;				// 记录时间长度
	uint8_t	warning_fcw;
	uint8_t	warning_hmw;
	uint8_t	warning_ldw;
	uint8_t	warning_aeb;
	uint8_t	g_sensor_isOpen;					// g_sensor开关与触发登机
	uint8_t	g_sensor_level;

}CarDvr_Cfg;

// 记录仪节点数据结构体 数据长度100字节以内
typedef struct _CAR_DVR_Node_Data{
	uint8_t data_info[NODE_DATAINFO_SIZE];
    uint8_t data[NODE_DATA_SIZE/6];
    volatile uint16_t data_len;						// 数据长度
}Car_Dvr_NodeData_S;
// 记录仪E1指令内容
typedef struct _CAR_DVR_E1_Data{
	uint8_t id[25];
    uint8_t Media_Type[1];//0:图像,1:音频,2:视频,3:zip
    uint8_t MediaFormat[1];//文件格式
    uint8_t event_code[1];//触发指令来源
    uint8_t source_location[1]; //资源存储位置
    uint8_t gpsData[50];
}CAR_DVR_E1_Data_S;
// 记录仪D1指令内容
typedef struct _CAR_DVR_D1_Data{
	uint8_t id[25];
    uint8_t from[1];
    uint8_t type[1];
    uint16_t duration[1];
    uint16_t interval[1];
}CAR_DVR_D1_Data_S;
typedef struct _CAR_DVR_INFO
{
	uint16_t status;			//记录仪的状态（存储记录仪状态是否有异常）
	CAR_DVR_STAGE_t step;
	uint8_t  PingState;			//记录仪的ping指令返回状态
	uint8_t  UpState;			//记录仪的视频状态  发送成功/发送中/发送失败
	uint8_t  SerRsentCount;		//云端指令重发次数
	uint8_t  D1AckState;		//记录仪的D1指令返回状态   1 未收到返回信息，0收到返回信息
	uint8_t  D1SaveIndex;		//D1指令存储下标
	volatile uint8_t  D1SendIdex;		//D1指令发送下标
	volatile uint8_t  D1SendCount;		//D1指令发送次数
	uint8_t  E1SendCount;		//E1指令发送次数
	uint8_t  E1SaveCount;		//E1指令剩余条数（由于盒子通讯问题导致，视频已经上传成功，收到E1的数据无法上传的条数）
	uint8_t  PingSendCount;		//
//	uint8_t  DvrCmdState;		//记录仪的指令状态，处于哪一条指令的通讯过程，ping指令与D1指令顺序执行，不进行交叉执行；
								//如果处于ping的指令状态未收到回复则不发送D1指令，如果处于D1的指令状态则不发送ping指令
    volatile	uint16_t loop_times;			//状态机循环次数
	uint8_t AEBDuration;		//AEB视频时长
	uint8_t FCWDuration;		//FCW视频时长
	uint8_t HMWDuration;		//HMW视频时长
	Car_Dvr_NodeData_S	CmdD1Buf[_CAR_DVR_D1_COUNT];			//D1指令缓存60条
	CAR_DVR_D1_Data_S D1Code;
	CAR_DVR_E1_Data_S E1Code;
//


}CarDvrInfo;

extern CarDvr_Cfg car_dvr_param;
extern uint8_t uart2_buf[200];
extern uint16_t usart2_buf_len;
extern uint8_t usart2_idel_flag;
extern CarDvrInfo Car_Dvr_info;
#define CARDVR_CFG_SIZE			512

/*************************** 行车记录仪 **********************************************/
bool 	USART2_Recv_Car_DVR_Interaction_ASCII_Message(uint8_t *string, uint16_t length);
bool 	Analysis_Reply_ASCII_CARDVR(uint8_t *data, uint16_t len);
uint8_t Analysis_Request_ASCII_CARDVR(uint8_t *data, uint16_t len);
bool 	Analysis_CARDVR_Record_Video_ASCII(uint8_t *data, uint16_t dataLen);
bool 	Request_Recv_Platform_Later_Reply_CARDVR_E1_ASCII(uint8_t *data, uint16_t dataLen);
void 	Send_Data_To_CARDVR_BY_USART2(uint8_t *data, uint16_t dataLen);
bool 	Request_CARDVR_Protocal_Head_ASCII(uint16_t seqNum, uint8_t cmdCode,  uint8_t *data);
bool 	Reply_CARDVR_Protocal_Head_ASCII(uint16_t seqNum, uint8_t cmdCode, uint16_t stateCode, uint8_t *data);
void	Collection_CARDVR_Take_Picture_D1_ASCII(AEB_UPLOAD_INFO *info);
bool 	Analysis_CARDVR_Hard_Soft_Ware_Info_ASCII(uint8_t *data, uint16_t dataLen);
bool 	Request_CARDVR_Ping_11_ASCII();
//bool 	Request_CARDVR_Take_Picture_D1_ASCII();
bool 	Replay_CARDVR_AEB_Device_Soft_Hard_Ware_Info_ASCII(uint16_t seqNum);


CarDvr_Cfg Get_Car_Dvr_Param();
uint8_t Set_Car_Dvr_Param(CarDvr_Cfg param);
uint8_t Write_CarDvr_Cfg_Param(CarDvr_Cfg param);
uint8_t W25QXX_Write_CarDvr_Cfg_Param(uint8_t *data,  uint16_t dataLen);
uint8_t Read_CarDvr_Cfg_Param(CarDvr_Cfg *param);
uint8_t Get_CarDvr_Cfg_Param(uint8_t *data,  uint16_t dataLen);

uint8_t Analysis_Request_Set_CarDvr_Param_ASCII(uint8_t *data);
uint8_t Send_Reply_Platform_Query_CarDvr_Param_Info_ASCII(uint32_t seqNum);
bool Server_Request_CARDVR_Take_Picture_D1_ASCII(uint8_t *data, uint16_t len);
bool Request_CARDVR_Set_Gsensor_C6_ASCII();
void CarDvr_Param_Init();
bool Send_Car_Dvr_Cmd();
void CAR_DVR_Message_Handle();
bool 	Request_CARDVR_Take_Picture_D1_ASCII();
bool Send_E1_ErrCode_ToServer(uint8_t cmdCode, uint16_t stateCode, uint32_t seqNum);
#endif /* CAR_DVR_H_ */
