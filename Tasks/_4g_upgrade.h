/*
 * _4g_upgrade.h
 *
 *  Created on: 2021-12-3
 *      Author: Administrator
 * 功能说明：包括OTA升级|数据上传|获取GPS定位信息|蓝牙功能
 */

#ifndef _4G_UPGRADE_H_
#define _4G_UPGRADE_H_
#include "upgrade_common.h"
#include "common.h"
#include "EC200U.h"
#define CAR_CAN_ANALYSIS_DEBUG			1
/****************************宏、枚举定义**************************************/
#define SUB_PKG_SIZE		4096
#define FIRST_PKG_SIZE		4104		// 4096+8
#define END_PKG_CHK_SIZE	12			// 4 + 8
//#define	BOOTLOADER_SIZE		64			// 在外部FLASH中缓冲区大小为60KB
enum{
	OTA_APP,
	OTA_SCREEN,
	OTA_CAMERA,
	OTA_BOOTLd,
	OTA_NO,
};

enum{
	SCREEN_SEND_INIT,
	SCREEN_READ_DATA,
	SEND_STRAT_FRAME,
	SEND_DATA_FRAME,
	SEND_END_FRAME,
};
enum{
	SCREEN_RECV_INIT,
	SCREEN_RECV_OK,		// 发下一包
	SCREEN_RECV_ERR,	// 重发
};
/********************************结构体定义**************************************/

/****************************全局变量**************************************/


/****************************全局函数**************************************/
// OTA升级
void 	EC200U_Upgrade_Init();
uint8_t OTA_Upgrade();
bool 	OTA_Upgrade_ASCII();
char 	getche(void);
void 	OTA_Start();
void 	OTA_End();

void 	OTA_Upgrade_Download_In_USART0IT(uint8_t data);		// 在串口0中断函数体
void 	Analysis_USART1_4G_CMD();
void 	Analysis_Reply_UpgradePackage_Info_ASCII(uint8_t *data, uint16_t contentLen);
bool 	_4G_Recv_OTA_Message(uint8_t *string);
uint8_t OTA_Sub_PKG_Dwn_Write_Data_In_Main();
bool 	Send_Package_To_Small_Screen_By_Can_Way();
extern uint8_t Upgrade_Bootloader_Program();
void 	Upgrade_Warning_And_OTA_Overtime_Judge();	// 升级指示灯，并判断超时
bool 	Request_OTA_Finish_CMD_ASCII();
#endif /* _4G_UPGRADE_H_ */
