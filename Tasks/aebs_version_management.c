/*
 * aebs_version_management.c
 *
 *  Created on: 2022-4-13
 *      Author: xujie
 */

#include "aebs_version_management.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "upgrade_common.h"

static uint8_t flag_got_v0_cam = 0x00;
static uint8_t flag_got_v1_cam = 0x00;
static uint8_t flag_got_v2_cam = 0x00;
static uint8_t flag_got_v3_cam = 0x00;

static uint8_t flag_got_SW_V_Displayer = 0x00;
static uint8_t flag_got_SN_PN_Displayer = 0x00;


void AEBS_Version_Init(void)
{
	memcpy(sw_hw_version.AEBS_SN, server_info.sn, strlen(server_info.sn));
	memcpy(sw_hw_version.AEBS_PN, server_info.pn, strlen(server_info.pn));
	memcpy(sw_hw_version.AEBS_SW_Version, upgrade_info.run_v, strlen(upgrade_info.run_v));

	sw_hw_version.Got_Version_Info_Camera 		= 0x00;
	sw_hw_version.Got_Version_Info_Displayer 	= 0x00;
	sw_hw_version.Got_Version_Info_URadar 		= 0x00;
	sw_hw_version.Got_Version_Info_MRadar 		= 0x00;
	//Comm_Protocol_Version_Info
	/*comm_protocol_version.Camera_CAN_ProtocoI_Version.v0 = 0;
	comm_protocol_version.Camera_CAN_ProtocoI_Version.v1 = 1;
	comm_protocol_version.Camera_CAN_ProtocoI_Version.v2 = 3;*/

	comm_protocol_version.AEBS_AT_BT_ProtocoI_Version.v0 = 0;
	comm_protocol_version.AEBS_AT_BT_ProtocoI_Version.v1 = 1;
	comm_protocol_version.AEBS_AT_BT_ProtocoI_Version.v2 = 11;

	comm_protocol_version.AEBS_AT_PC_ProtocoI_Version.v0 = 0x00;
	comm_protocol_version.AEBS_AT_PC_ProtocoI_Version.v1 = 0x00;
	comm_protocol_version.AEBS_AT_PC_ProtocoI_Version.v2 = 0x06;

	comm_protocol_version.AEBS_CAN_ProtocoI_Version.v0 = 0x00;
	comm_protocol_version.AEBS_CAN_ProtocoI_Version.v1 = 0x02;
	//comm_protocol_version.AEBS_CAN_ProtocoI_Version.v2 = 0x02;

	comm_protocol_version.Cloud_Platform_ProtocoI_Version.v0 = 0x00;
	comm_protocol_version.Cloud_Platform_ProtocoI_Version.v1 = 0x02;
	//comm_protocol_version.Cloud_Platform_ProtocoI_Version.v2 = 0x02;

	comm_protocol_version.Displayer_CAN_ProtocoI_Version.v0 = 0;
	comm_protocol_version.Displayer_CAN_ProtocoI_Version.v1 = 8;
	//comm_protocol_version.Displayer_CAN_ProtocoI_Version.v2 = 0;

}

void CAN_Analysis_Camera_Version(struct can_frame *rx_frame){
	if(sw_hw_version.Got_Version_Info_Camera == 0x00 && rx_frame->TargetID == 0x79f){
		uint8_t SW_Version = rx_frame->data[3];
		uint8_t HW_Version = rx_frame->data[4];
		uint8_t CP_Version = rx_frame->data[6];
		uint8_t VersionSegment = rx_frame->data[5];
		VersionSegment &= 0x03;
		if(VersionSegment == 0x00){
			flag_got_v0_cam = 0x01;
			sw_hw_version.Camera_SW_Version.v0 = SW_Version;
			sw_hw_version.Camera_HW_Version.v0 = HW_Version;
			comm_protocol_version.Camera_CAN_ProtocoI_Version.v0 = CP_Version;
		}else if(VersionSegment == 0x01){
			flag_got_v1_cam = 0x01;
			sw_hw_version.Camera_SW_Version.v1 = SW_Version;
			sw_hw_version.Camera_HW_Version.v1 = HW_Version;
			comm_protocol_version.Camera_CAN_ProtocoI_Version.v1 = CP_Version;
		}else if(VersionSegment == 0x02){
			flag_got_v2_cam = 0x01;
			sw_hw_version.Camera_SW_Version.v2 = SW_Version;
			sw_hw_version.Camera_HW_Version.v2 = HW_Version;
			comm_protocol_version.Camera_CAN_ProtocoI_Version.v2 = CP_Version;
		}
		else if(VersionSegment == 0x03){
			flag_got_v3_cam = 0x01;
			sw_hw_version.Camera_SW_Version.v3 = SW_Version;
			sw_hw_version.Camera_HW_Version.v3 = HW_Version;
		}
		if((flag_got_v0_cam == 0x01)&&(flag_got_v1_cam == 0x01)&&(flag_got_v2_cam == 0x01)&&(flag_got_v3_cam == 0x01)){
			sw_hw_version.Got_Version_Info_Camera = 0x01;
			flag_got_v0_cam = 0x00;
			flag_got_v1_cam = 0x00;
			flag_got_v2_cam = 0x00;
			flag_got_v3_cam = 0x00;
		}
	}
	/*if(stCanCommSta.stCamera.status == OFFLINE){
		sw_hw_version.Got_Version_Info_Camera = 0x00;
	}*/
}
void CAN_Analysis_Displayer_Version(struct can_frame *rx_frame){
	if(sw_hw_version.Got_Version_Info_Displayer == 0x00 && (rx_frame->TargetID == 0x18FFED81 || rx_frame->TargetID == 0x18FFED83)){
		if(rx_frame->TargetID == 0x18FFED81){
			sw_hw_version.Displayer_SW_Version.v0 = rx_frame->data[1];
			sw_hw_version.Displayer_SW_Version.v1 = rx_frame->data[2];
			sw_hw_version.Displayer_SW_Version.v2 = rx_frame->data[3];
			flag_got_SW_V_Displayer = 0x01;
		}else if(rx_frame->TargetID == 0x18FFED83){
			flag_got_SN_PN_Displayer = 0x01;
			sw_hw_version.Displayer_SN[0] = rx_frame->data[0];
			sw_hw_version.Displayer_SN[1] = rx_frame->data[1];
			sw_hw_version.Displayer_SN[2] = rx_frame->data[2];
			sw_hw_version.Displayer_SN[3] = rx_frame->data[3];
			sw_hw_version.Displayer_PN[0] = rx_frame->data[4];
			sw_hw_version.Displayer_PN[1] = rx_frame->data[5];
			sw_hw_version.Displayer_PN[2] = rx_frame->data[6];
			sw_hw_version.Displayer_PN[3] = rx_frame->data[7];
			fprintf(USART1_STREAM,">> Displayer_SN:%s\r\n",sw_hw_version.Displayer_SN);
			fprintf(USART1_STREAM,">> Displayer_PN:%s\r\n",sw_hw_version.Displayer_PN);
		}
		if(flag_got_SN_PN_Displayer == 0x01 && flag_got_SW_V_Displayer == 0x01){
			sw_hw_version.Got_Version_Info_Displayer = 0x01;
			flag_got_SN_PN_Displayer = 0x00;
			flag_got_SW_V_Displayer == 0x00;
		}

	}
}
void CAN_Analysis_URadar_Version(struct can_frame *rx_frame){
	if(sw_hw_version.Got_Version_Info_URadar == 0x00 && (rx_frame->TargetID == 0x700)){
		//uint8_t version = rx_frame->data[3];
		sw_hw_version.URadar_Version.v0 = rx_frame->data[3] >> 4;
		sw_hw_version.URadar_Version.v1 = rx_frame->data[3] & 0x0F;

		sw_hw_version.Got_Version_Info_URadar = 0x01;
	}
}
void CAN_Analysis_MRadar_Version(struct can_frame *rx_frame){

}
