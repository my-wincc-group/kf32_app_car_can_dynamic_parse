/*
 * data_loss_prevention.c
 *
 *  Created on: 2022-6-28
 *      Author: Administrator
 */
#include "upgrade_common.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "data_loss_prevention.h"
#include "usart.h"
#include "EC200U.h"
#include "dma.h"
#include "canhl.h"
#include "displayer.h"
#include "_4g_server.h"
#include "aeb_sensor_management.h"
#define Data_Len_1024					1024
#define	DATA_LOSS_SIZE					Data_Len_1024		// 1024个扇区
/*
 * 测试指令：
 * 1、使能数据防丢失功能：<ZKHYSET*DLP_ON>			// 实际就是关闭服务器连接标志位 server_info.isConnet = false;
 * 2、关闭数据防丢失功能：<ZKHYSET*DLP_OFF>		// 实际就是打开服务器连接标志位 server_info.isConnet = true;
 * 3、查看数据防丢失下标ID：<ZKHYGET*DLP_ID>		// 反馈[OK]DLP ID Is 12_12. 前12是front指向位置，后12是end指向位置
 */
/* ----------------------- 变量  ------------------------------- */
typedef struct _Data_Loss_Info
{
	volatile uint16_t seq;				// 序号
	volatile uint16_t front;			// 帧头序号，取值范围[0,1023]
	volatile uint16_t end;				// 帧尾序号，取值范围[0,1023]
	volatile uint32_t rw_addr;			// 读写地址
	volatile uint8_t write_buf[EX_FLASH_SECTOR_SIZE];	// 写缓冲区
//	volatile uint8_t loop_times_restart_device;	// 连续存储次数达到一定次数（100），就立刻重启设备
}Data_Loss_Info;

Data_Loss_Info data_loss_info 					= {0};
Upload_Data_Queue_Info upload_data_queue_info 	= {0};
volatile uint8_t _4g_queue_state 				= _4G_WAIT;
NodeData_S data_node;						// 消息队列方式上报数据到平台
extern volatile int64_t SystemtimeClock;

/* ----------------------- 函数声明  ------------------------------- */
bool 	Get_Data_Loss_Prevent_From_External_Flash();
bool 	Get_Data_Loss_Prevent_From_RAM();

// 防止小屏幕黑屏操作，每次读写慰问一下小屏幕
void Prevent_Display_Black_Screen()
{
	struct can_frame displayer;
	Displayer_CANTx_Radar_Get(&displayer);
	CAN_Transmit_DATA(CAN4_SFR, displayer);
}

void Data_Loss_Prevention_Init()
{
	// 数据防丢失
	upload_data_queue_info.have_one_data 	= 0;
	upload_data_queue_info.wait_times 		= 0;
	upload_data_queue_info.send_times 		= 0;
	upload_data_queue_info.data_wait_time 	= 0;
	upload_data_queue_info.step 			= SEND_NO;
	upload_data_queue_info.reconnect_server_flag = 0;

	data_loss_info.end 		= 0;
	data_loss_info.front 	= 0;
	data_loss_info.seq 		= 0;
	data_loss_info.rw_addr 	= 0;
//	data_loss_info.loop_times_restart_device = 0;
	memset((char*)data_loss_info.write_buf, 0, EX_FLASH_SECTOR_SIZE);
//	Set_Data_Loss_Prevention_ID(0, 0);

	uint8_t id_s[DLP_ID_SIZE] = {0};
	if(Get_Data_Loss_Prevention_ID(id_s)){
		Set_Queue_In_External_Flash_Data_Front_End_Value(id_s);
	}else{
		sprintf(id_s, "0_0");
		Set_Queue_In_External_Flash_Data_Front_End_Value(id_s);
	}
}
/*
 * 获取暂存到外部FLASH中的上报数据
 * 参数：直吐模式下的发送数据结构体数据
 * 返回：true有数据；false无数据
 */
bool Get_One_Data_Loss_Prevention(NodeData_S *nData)
{
	// 收尾相同，没有数据
	if(data_loss_info.front == data_loss_info.end){
//		USART1_Bebug_Print("NO DATA", "No Data (Data Loss Prevention) In External Flash.");
		return false;
	}

	uint8_t read_buf[W25_RW_DATA_SIZE] = {0};
	// 地址
	data_loss_info.rw_addr = OUT_FLASH_LOSS_OF_DATA_START + (data_loss_info.end - 0) * Data_Len_1024;

	// 获取数据，并将数据转成结构体数据
	if(W25QXX_Read_One_Sector_Chk(read_buf, data_loss_info.rw_addr, sizeof(NodeData_S))){	// 获取数据校验通过
		memcpy(nData, read_buf, sizeof(NodeData_S));

		data_loss_info.end++;
		if(data_loss_info.end >= DATA_LOSS_SIZE){
			data_loss_info.end = 0;
			uint16_t erase_sector_id = OUT_FLASH_LOSS_OF_DATA_START / EX_FLASH_SECTOR_SIZE + DATA_LOSS_SIZE - 1;
			W25QXX_Erase_Sector(erase_sector_id);
			// 记录队列前编号和队列后编号
			Set_Data_Loss_Prevention_ID(data_loss_info.front, data_loss_info.end);
			USART1_Bebug_Print_Num("[ERASE]ID Is", erase_sector_id, 1, 1);
		}else{
			// 取出后的数据空间，清空（存0），（一次仅能扇区一个扇区的大小（4KB），而一条数据大小占用空间为1KB）。
			if(data_loss_info.end % 4 == 0){
				uint16_t erase_sector_id = OUT_FLASH_LOSS_OF_DATA_START / EX_FLASH_SECTOR_SIZE + (data_loss_info.end / 4 - 1);
				W25QXX_Erase_Sector(erase_sector_id);
				// 记录队列前编号和队列后编号
				Set_Data_Loss_Prevention_ID(data_loss_info.front, data_loss_info.end);
				USART1_Bebug_Print_Num("[ERASE]ID Is", erase_sector_id, 1, 1);
			}
		}
		USART1_Bebug_Print_Num("[GET] seq", data_loss_info.seq, 0, 1);
		USART1_Bebug_Print_Num("front_id", data_loss_info.front, 0, 1);
		USART1_Bebug_Print_Num("end_id", data_loss_info.end, 1, 1);
		return true;
	}else{	// 获取数据校验没通过
		USART1_Bebug_Print("ERROR", "Get Data (Data Loss Prevention) Failed", 1);
		memset(read_buf, 0, sizeof(read_buf));
		memcpy(nData, read_buf, sizeof(NodeData_S));

		// 异常处理
		if(data_loss_info.front > data_loss_info.end){
			data_loss_info.end++;
			Set_Data_Loss_Prevention_ID(data_loss_info.front, data_loss_info.end);
		}else if(data_loss_info.front < data_loss_info.end){
			data_loss_info.end = data_loss_info.front;
			Set_Data_Loss_Prevention_ID(data_loss_info.front, data_loss_info.end);
		}
		return false;
	}
}

/*
 * 单条数据（1024KB）需要先逐条加密操作
 * 为了方便写入，每次擦除的最小单位为4KB
 */
bool Add_CHK_Value_To_Write_Data(uint8_t *pBuffer, uint16_t dataLen, uint8_t *writeBuf)
{
	uint8_t chk_sum = 0;
	// get checkout value
	memset(writeBuf, 0, dataLen+1);
	for(uint16_t j=0; j<dataLen; j++){
		writeBuf[j] = pBuffer[j];
		chk_sum += pBuffer[j];
	}
	// 存储时将校验存放到最后1字节
	writeBuf[dataLen] = chk_sum;
//	fprintf(USART1_STREAM, "write chk:%02X\r\n", chk_sum);

//	fprintf(USART1_STREAM, "\r\n********** Single Data Info ******start*******\r\n");
//	for(uint16_t i=0; i<dataLen+1; i++){
//		fprintf(USART1_STREAM, "%02X ", writeBuf[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n********** Single Data Info ******end*******\r\n");
	return true;
}

/*
 * 数据防丢失
 */
bool Set_One_Data_Loss_Prevention(NodeData_S nData)
{
	USART1_Bebug_Print_Num("[Storage]", data_loss_info.seq % 4, 1, 1);
//	data_loss_info.loop_times_restart_device++;

	data_loss_info.seq ++;
	uint8_t temp_buff[W25_RW_DATA_SIZE] = {0};
	uint8_t write_buff[W25_RW_DATA_SIZE] = {0};

	// 将结构体数据转成uint8_t
	memcpy(temp_buff, &nData, sizeof(NodeData_S));

	// 将写入数据加上校验值
	Add_CHK_Value_To_Write_Data(temp_buff, sizeof(NodeData_S), write_buff);

	// 将写入的数据存放在4KB的缓冲区中，每包缓冲区大小为1KB
	memcpy((char*)(data_loss_info.write_buf + (data_loss_info.seq-1) % 4 * Data_Len_1024), write_buff, sizeof(NodeData_S)+1);

	// 满4包，写外部FLASH一个扇区 4KB
	if(data_loss_info.seq % 4 == 0){
		// 地址
		data_loss_info.rw_addr = OUT_FLASH_LOSS_OF_DATA_START + (data_loss_info.seq / 4 - 1) * EX_FLASH_SECTOR_SIZE;

		// 写入整个扇区
		W25QXX_Erase_Sector(data_loss_info.rw_addr / EX_FLASH_SECTOR_SIZE);					//擦除这个扇区
		W25QXX_Write_NoCheck((char*)(data_loss_info.write_buf), data_loss_info.rw_addr, EX_FLASH_SECTOR_SIZE);
		memset((char*)(data_loss_info.write_buf), 0, EX_FLASH_SECTOR_SIZE);

		// front记录4条
		if(data_loss_info.seq >= DATA_LOSS_SIZE){
			data_loss_info.seq = 0;
			data_loss_info.front = 0;
		}else{
			data_loss_info.front += 4;
		}

		// 记录队列前编号和队列后编号
		Set_Data_Loss_Prevention_ID(data_loss_info.front, data_loss_info.end);
		USART1_Bebug_Print_Num("[SET] seq", data_loss_info.seq, 0, 1);
		USART1_Bebug_Print_Num("front_id", data_loss_info.front, 0, 1);
		USART1_Bebug_Print_Num("end_id", data_loss_info.end, 1, 1);
	}

	// 若连续存储数据，说明设备MCU与4G模块程序已经跑飞，临时方案让设备重启
//	if(data_loss_info.loop_times_restart_device > 100){
//		data_loss_info.loop_times_restart_device = 0;
//		SYSCTL_System_Reset_Enable(TRUE);
//		USART1_Bebug_Print("RESTART", "System Restart.", 1);
//	}
	return true;
}

/*
 * 在外部FLASH中设置数据指向 front和 end
 */
void Set_Queue_In_External_Flash_Data_Front_End_Value(uint8_t *id_s)
{
	uint8_t flag = 0;
	uint8_t data_front[DATA_LEN_8B] = {0};
	uint8_t data_end[DATA_LEN_8B] = {0};
	uint8_t j=0;

	for(uint8_t i=0; i<DLP_ID_SIZE; i++){
		if(id_s[i] != 0){
			if(id_s[i] == '_'){
				flag = 1;
				j = 0;
				continue;
			}

			if(flag == 1){
				data_end[j] = id_s[i];
				j++;
			}else{
				data_front[j] = id_s[i];
				j++;
			}
		}
	}
	data_end[strlen(data_end)] = '\0';
	data_front[strlen(data_front)] = '\0';

	data_loss_info.front = atoi(data_front);
	if(data_loss_info.front > DATA_LOSS_SIZE){
		data_loss_info.front = 0;
		data_loss_info.seq 	 = 0;
	}else{
		data_loss_info.seq = data_loss_info.front;
	}
	data_loss_info.end = atoi(data_end);

	USART1_Bebug_Print_Num("front_id", data_loss_info.front, 0, 1);
	USART1_Bebug_Print_Num("end_id", data_loss_info.end, 1, 1);
}


/*
 * [直吐模式]队列中发送data_info
 */
void Queue_Send_Data_Info()
{
	if(strlen(data_node.data_info) < 1) return ;

	upload_data_queue_info.step = SEND_DATA_INFO;
	Usart0_DMA_Transmit(data_node.data_info, strlen(data_node.data_info));
//	USART_Send(USART0_SFR, data_node.data_info, strlen(data_node.data_info));

	// test print
//	Usart1_DMA_Transmit(data_node.data_info, strlen(data_node.data_info));
}
/*
 * [直吐模式]队列中发送data
 * 说明：情况1发送直吐模式下的数据部分；情况2发送单条指令，例如获取GPS或者获取4G信号强度；
 */
void Queue_Send_Data()
{
	if(data_node.data_len < 1) return ;

	upload_data_queue_info.step = SEND_DATA;
	EC200U_SendData(data_node.data, data_node.data_len, 1);

#if DEBUG_PRINT_SW
	// 测试输出，发送数据的序号，数据长度，指令
	if(data_node.data[24] != 0x00 && data_node.is_single_cmd == DOUBLE_CMD){	// 指令不为0x00
		USART1_Bebug_Print_Num("[QUEUE Send]Seq", data_node.data[2] | data_node.data[3]<<8, 0, 1);
		uint32_t data_len = 0;
		if(data_node.is_wait_reply == NEED_WAIT_REPLY_REPLY){
			data_len = data_node.data[31] | data_node.data[32]<<8;
		}else{
			data_len = data_node.data[29] | data_node.data[30]<<8;
		}
		USART1_Bebug_Print_Num("ContentLen", data_len, 0, 1);
		USART1_Bebug_Print_Num("CMD", data_node.data[24], 2, 1);
	}
#else
	if(data_node.data[24] != 0x00 && data_node.is_single_cmd == DOUBLE_CMD){	// 指令不为0x00
		if(data_node.is_wait_reply == NEED_WAIT_REPLY_REPLY){
			// 回复车CAN解析配置后，等待10ms，就重启设备  20220927 lmz add
			if(data_node.data[24] == SET_CAR_CAN_ANALYSIS_PARAM && data_node.data[25] == 0x00){	// 且回复状态为成功
				delay_ms(10);
//				USART1_Bebug_Print("DEBUG", "Device Restart Test", 1);
				SYSCTL_System_Reset_Enable(TRUE);	// 控制MCU软重启
			}
		}
	}
#endif
}

/*
 * 查询4G模块是否已将信息送达服务器
 * 	AT+QISEND=0,0 	//查询发送数据长度，已确认数据长度和未确认数据长度。
	+QISEND: 10,10,0
 *
 *	+QISEND: <total_send_length>,<ackedbytes>,<unackedbytes>
 *	<total_send_length> 整型。发送数据总长度。单位：字节。
 *	<ackedbytes> 整型。收到数据总长度。单位：字节。
 *	<unackedbytes> 整型。未收到数据总长度。单位：字节。
 */
//void Query_Queue_Send_Data_Status()
//{
//	uint8_t query_cmd[20] = {0};
//	upload_data_queue_info.step = QUERY_STATUS;
//	sprintf(query_cmd, "AT+QISEND=%d,0\r\n", CONNECT_ID);
//	EC200U_SendData(query_cmd, strlen(query_cmd), 1);
//}

/*
 * 数据上传入口
 * 因与EC200U模块采用了直吐模式，所以每次上传数据分两次：
 * 先发送data_info；再发送data；
 */
uint8_t test_cmd = 0;
void Upload_Data_To_Platform_By_Queue()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return ;

	if(upgrade_info.step == UPGRADE_SUB_DMN) return ;
	if(upgrade_info.step == UPGRADE_URL) return ;
	if(upgrade_info.step == RESPONSE) return ;
	if(!server_info.isConnect) return ;

	static uint32_t dataUp_clk 	= 0;		// 上传时，时间片标志
	if(SystemtimeClock - dataUp_clk < 20) return ;	// 20ms*50 = 1000ms
	dataUp_clk = SystemtimeClock;

	// 取出数据
	if(!upload_data_queue_info.have_one_data){	// 没有数据取数据

		// 队列不为空就发送数据；队列为空就判断外部FLASH中是否存在防丢失数据
		if(!isEmpty(&dataUp_q)){	// 队列中存在数据
			// 若数据长度为0，直接删除
			data_node = popQueue(&dataUp_q);

			upload_data_queue_info.have_one_data = 1;

			// [触发] 需要等待回复
			if(data_node.is_single_cmd == DOUBLE_CMD){
				// 发送数据中没有结束符(0D 0A)就删除
				if(data_node.data[data_node.data_len-2]!=0x0D || data_node.data[data_node.data_len-1]!=0x0A){
					deleteQueue(&dataUp_q);
					upload_data_queue_info.have_one_data = 0;
					USART1_Bebug_Print("ERROR", "queue delete().", 1);
				}else{
					// 若网络断开了，就不发送双指令了，需要存储的直接存储，否则直接从queue中删除
					if(!server_info.isConnect){
						if(data_node.is_single_cmd == DOUBLE_CMD && data_node.is_store == NEED_STORED){
							Set_One_Data_Loss_Prevention(data_node);
						}

						upload_data_queue_info.step = SEND_NO;
						upload_data_queue_info.have_one_data = 0;
						deleteQueue(&dataUp_q);
					}else {
						Queue_Send_Data_Info();
					}
				}
				return ;
			}
		}else{					// 队列为空，才读外部FLASH数据
			// 从外部FLASH中取，并放到队列中
			if(server_info.isConnect && upgrade_info.isUpgradeOK){	// 设备正常启动后，开始上报丢失数据
				// 获取内存中缓冲区中的数据
				Get_Data_Loss_Prevent_From_RAM();

				// 从外部FLASH中获取已经存储的数据
				Get_Data_Loss_Prevent_From_External_Flash();
				return ;
			}
		}
	}

	// 单指令发送就清空
	if(upload_data_queue_info.have_one_data){					// 有一条数据要发送
		// 异常处理：发送数据时，断网
		if(!server_info.isConnect){
			// 若网络断开了，就不发送双指令了，需要存储的直接存储，否则直接从queue中删除
			if(data_node.is_single_cmd == DOUBLE_CMD && data_node.is_store == NEED_STORED){
				Set_One_Data_Loss_Prevention(data_node);
			}

			upload_data_queue_info.step = SEND_NO;
			upload_data_queue_info.have_one_data = 0;
			deleteQueue(&dataUp_q);
		}
		// 正常处理
		if(data_node.is_single_cmd == DOUBLE_CMD){
#if 1
			// 发送判断
			switch(upload_data_queue_info.step)
			{
			case SEND_DATA_INFO:{								// 发送data_info
				if(_4g_queue_state == _4G_RECV_ARROWS){			// 只要输入的长度等于真实字符串长度就返回>
					_4g_queue_state = _4G_WAIT;
					upload_data_queue_info.wait_times = 0;
					Queue_Send_Data();
					return ;
				}

				// 等待1秒
				if(upload_data_queue_info.wait_times >= 49){
					upload_data_queue_info.wait_times = 0;
					if(upload_data_queue_info.send_times >= 10){  // 10秒后就重连服务器
					  upload_data_queue_info.send_times = 0;
					  Reconnect_Server(0);
					}else{
					  upload_data_queue_info.send_times++;
					  Queue_Send_Data_Info();
					  USART1_Bebug_Print("QUEUE", "Delay 1S To Send Data Info", 1);
					}
				}else{
					upload_data_queue_info.wait_times++;
				}
			}break;
			case SEND_DATA:{
				if(_4g_queue_state == _4G_SEND_FAIL){			// 发送缓冲区满了,暂定为等待
					_4g_queue_state = _4G_WAIT;
					return ;
				}

				if(_4g_queue_state == _4G_INVALIED_REQ){		// 非法请求，重新发送
					_4g_queue_state = _4G_WAIT;
					upload_data_queue_info.wait_times = 0;
					Queue_Send_Data_Info();
					return ;
				}

//				if(_4g_queue_state == _4G_SEND_OK){				// 收到Send OK 后，发送查询
//					_4g_queue_state = _4G_WAIT;
//					upload_data_queue_info.wait_times = 0;
//					upload_data_queue_info.wait_times = 0;
//					Query_Queue_Send_Data_Status();
//					return ;
//				}

				// 等待1秒
				if(upload_data_queue_info.wait_times >= 49){
					upload_data_queue_info.wait_times = 0;
					upload_data_queue_info.data_wait_time++;
					// 多发送一次data
					if(upload_data_queue_info.data_wait_time > 1){
						upload_data_queue_info.data_wait_time = 0;
						Queue_Send_Data_Info();
					}else{
						Queue_Send_Data();
						USART1_Bebug_Print("QUEUE", "Delay 1S To Send Data", 1);
					}
				}else{
					upload_data_queue_info.wait_times++;
				}
			}break;
//			case QUERY_STATUS:{
//				// 等待1秒
//				if(upload_data_queue_info.wait_times >= 49){
//					upload_data_queue_info.wait_times = 0;
//					upload_data_queue_info.data_wait_time++;
//					// 多发送一次查询状态
//					if(upload_data_queue_info.data_wait_time > 1){
//						upload_data_queue_info.data_wait_time = 0;
//						Queue_Send_Data_Info();
//					}else{
//						Query_Queue_Send_Data_Status();
//						USART1_Bebug_Print("QUEUE", "Delay 1S To Query Status");
//					}
//				}else{
//					upload_data_queue_info.wait_times++;
//				}
//			}break;
			default:break;
			}
#endif
		}else{	// 不需要回复
#if 1
			Queue_Send_Data();
//			USART1_Bebug_Print("QUEUE PRINT", data_node.data);
			deleteQueue(&dataUp_q);
			upload_data_queue_info.have_one_data = 0;
			upload_data_queue_info.step = SEND_NO;
#endif
		}
	}
}

//uint8_t total_send_length_s[5] = {0};
//uint8_t acked_bytes_s[5] = {0};
//uint8_t unacked_bytes_s[5] = {0};
//volatile uint16_t total_send_length = 0;
//volatile uint16_t acked_bytes = 0;
//volatile uint16_t unacked_bytes = 0;
//volatile uint8_t flag = 0, j = 0;
//
//bool Anylasis_Query_Cmd_Reply(uint8_t *string)
//{
////	USART1_Bebug_Print("DEBUG", string);
//	// +QISEND: 10,10,0  -> 10,10,0
//
//	memset(total_send_length_s, 0, 5);
//	memset(acked_bytes_s, 0, 5);
//	memset(unacked_bytes_s, 0, 5);
//
//	for(uint8_t i=0; i<strlen(string); i++){
//		switch(flag){
//		case 0:{
//			if(string[i] != ','){
//				total_send_length_s[j] = string[i];
//				j++;
//			}else{
//				total_send_length_s[j] = '\0';
//				j = 0;
//				flag = 1;
//			}
//		}break;
//		case 1:{
//			if(string[i] != ','){
//				acked_bytes_s[j] = string[i];
//				j++;
//			}else{
//				acked_bytes_s[j] = '\0';
//				j = 0;
//				flag = 2;
//			}
//		}break;
//		case 2:{
//			unacked_bytes_s[j] = string[i];
//			j++;
//		}break;
//		}
//	}
//	acked_bytes_s[j] = '\0';
//	j = 0;
//	flag = 0;
//
//	// 转换并判断
//	total_send_length = atoi(total_send_length_s);
//	acked_bytes = atoi(acked_bytes_s);
//	unacked_bytes = atoi(unacked_bytes_s);
//
////	USART1_Bebug_Print_Num("total", total_send_length, 0);
////	USART1_Bebug_Print_Num("acked", acked_bytes, 0);
////	USART1_Bebug_Print_Num("unacked", unacked_bytes, 1);
//
////	if(total_send_length==acked_bytes && unacked_bytes==0){
////		return true;
////	}
//
//	if(total_send_length == (acked_bytes + unacked_bytes)){
//		if(upload_data_queue_info.have_one_data){
//			upload_data_queue_info.have_one_data = 0;
//			deleteQueue(&dataUp_q);
//			upload_data_queue_info.wait_times = 0;
//			upload_data_queue_info.step = SEND_NO;
//		}
//		return true;
//	}
//
//	USART1_Bebug_Print_Num("[!=]total", total_send_length, 0, 1);
//	USART1_Bebug_Print_Num("[!=]sum", acked_bytes + unacked_bytes, 1, 1);
//	return false;
//}
/*
 * 在USART0中断函数中，数据上报解析
 * 参数：接收到结束符的字符串
 * 说明：在连上服务器后执行
 */
bool _4G_Recv_Data_Upload_Message_From_Platform_Reply(uint8_t *string)
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return false;
	if(!server_info.isConnect) return false;

	// Invalid Request
	if(strstr((const char*)string, (const char*)"Invalid Request")){
		_4g_queue_state = _4G_INVALIED_REQ;
		upload_data_queue_info.send_times = 0;
		USART1_Bebug_Print("QUEUE INFO", string, 1);
		return true;
	}

	// 接收到>,只要输入的长度等于真实字符串长度就返回
	if(strstr((const char*)string, (const char*)">")){
		_4g_queue_state = _4G_RECV_ARROWS;
		upload_data_queue_info.send_times = 0;
//		USART1_Bebug_Print("QUEUE INFO", ">");
		return true;
	}

	// SEND FAIL
	if(strstr((const char*)string, (const char*)"SEND FAIL")){
		_4g_queue_state = _4G_SEND_FAIL;
		upload_data_queue_info.send_times = 0;
		USART1_Bebug_Print("QUEUE INFO", string, 1);
		return true;
	}

	if(strstr((const char*)string, (const char*)"SEND OK")){
		// 接收到SEND OK就清空
		upload_data_queue_info.send_times = 0;
		if(upload_data_queue_info.have_one_data){
			// 回复车CAN解析配置后，等待100ms，就重启设备 20221019 lmz add
			if(data_node.data[24] == SET_CAR_CAN_ANALYSIS_PARAM && data_node.data[25] == 0x00){	// 且存车CAN动态解析参数成功
				USART1_Bebug_Print("DEBUG", "Send OK, Device Restart Test", 1);
				deleteQueue(&dataUp_q);
				delay_ms(2000);
				SYSCTL_System_Reset_Enable(TRUE);	// 控制MCU软重启
			}

			upload_data_queue_info.have_one_data = 0;
			deleteQueue(&dataUp_q);
			upload_data_queue_info.wait_times = 0;
			upload_data_queue_info.step = SEND_NO;
		}

		return true;
	}

	// 增加询问是否已经数据送到服务器机制
//	if(strstr((const char*)string, (const char*)"+QISEND:")){
//		USART1_Bebug_Print("QUEUE INFO", string);
		// 解析数据后做判断，
//		if(Anylasis_Query_Cmd_Reply(string + 8)){

//		}
//		return true;
//	}

	return false;
}

uint16_t Get_Data_Loss_Info_Sequence()
{
	return data_loss_info.seq;
}

/*
 * 外部FLASH中是否存在数据
 */
bool Get_Data_Loss_Prevent_From_External_Flash()
{
	if(Get_Queue_Can_Store_Number(&dataUp_q) > 0){		// 保证queue中未满
		NodeData_S nodeData = {0};

		Prevent_Display_Black_Screen();					// 防止小屏幕黑屏的，临时方案

		if(Get_One_Data_Loss_Prevention(&nodeData)){	// 存在数据，就转存到队列中
			insertQueue(&dataUp_q, &nodeData);
			return true;
		}
	}
	return false;
}
/*
 * 数据防丢失中是否序号不满4的倍数无法存储数据（data_loss_info.seq）
 * 若长时间不满4的倍数，这些数据可能永远被遗忘了
 */
bool Get_Data_Loss_Prevent_From_RAM()
{
	// 队列不满，DLP中有数据
	if (Get_Queue_Can_Store_Number(&dataUp_q) >= 3) {
		volatile uint8_t seq_remain = data_loss_info.seq % 4;		// 剩余个数，范围[0, 3]
		if(seq_remain > 0){
//			USART1_Bebug_Print_Num("DEBUG", seq_remain, 1, 1);
			// 取出数据，转存到队列中
			NodeData_S nData = {0};
			switch(seq_remain){
			case 1:{		// 剩余1个
				memcpy(&nData, (uint8_t *)data_loss_info.write_buf, sizeof(NodeData_S));
				insertQueue(&dataUp_q, &nData);
				data_loss_info.seq--;
				// 测试
				if(data_loss_info.seq == data_loss_info.front){
					USART1_Bebug_Print("DEBUG1", "data_loss_info.seq == data_loss_info.front", 1);
				}

				return true;
			}break;
			case 2:{		// 剩余2个
				memcpy(&nData, (uint8_t *)data_loss_info.write_buf, sizeof(NodeData_S));
				insertQueue(&dataUp_q, &nData);
				data_loss_info.seq--;

				memset(&nData, 0, sizeof(NodeData_S));
				memcpy(&nData, (uint8_t *)data_loss_info.write_buf + Data_Len_1024, sizeof(NodeData_S));
				insertQueue(&dataUp_q, &nData);
				data_loss_info.seq--;
				// 测试
				if(data_loss_info.seq == data_loss_info.front){
					USART1_Bebug_Print("DEBUG2", "data_loss_info.seq == data_loss_info.front", 1);
				}

				return true;
			}break;
			case 3:{		// 剩余3个
				memcpy(&nData, (uint8_t *)data_loss_info.write_buf, sizeof(NodeData_S));
				insertQueue(&dataUp_q, &nData);
				data_loss_info.seq--;

				memset(&nData, 0, sizeof(NodeData_S));
				memcpy(&nData, (uint8_t *)data_loss_info.write_buf + Data_Len_1024, sizeof(NodeData_S));
				insertQueue(&dataUp_q, &nData);
				data_loss_info.seq--;

				memset(&nData, 0, sizeof(NodeData_S));
				memcpy(&nData, (uint8_t *)data_loss_info.write_buf + 2 * Data_Len_1024, sizeof(NodeData_S));
				insertQueue(&dataUp_q, &nData);
				data_loss_info.seq--;
				// 测试
				if(data_loss_info.seq == data_loss_info.front){
					USART1_Bebug_Print("DEBUG3", "data_loss_info.seq == data_loss_info.front", 1);
				}
				return true;
			}break;
			}
		}
	}

	return false;
}

