/*
 * tooling.h
 *
 *  Created on: 2022-03-13
 *      Author: Mark
 */

#ifndef _TOOLING_H_
#define _TOOLING_H_
#include "upgrade_common.h"
#include "gpio.h"
#include "w25qxx.h" //external flash drivers
// #include "w25qxx_dma.h"
#include "usart.h"
#include "i2c.h"
#include "_4g_data_upload.h"
#include "common.h"
/* ------------------------宏 枚举------------------------------- */
#define EEPROM_ADDR 	0xA8
#define BUFFER_SIZE_Write 64


// 字符串截取
typedef struct _stringStrtok{
	uint8_t data[10][50];
	uint8_t cnt;
}stringStrtok;
/*
 * EMC工装测试的入口函数
 * 说明：
 */
uint8_t Tooling_for_EMC( void );

/*
 * 工装测试的入口函数
 * 说明：
 */
uint32_t Tooling_In_MainLoop( void );
/*
 * LED 测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*LED:ON  表示LED灯开;
 * 	     ZKHYTEST*LED:OFF 表示LED灯关闭
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 */
FunctionalState LEDTest_Logic(char *strx);

/*
 * 车辆信号灯状态读取测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*LED:STOP  表示车辆的刹车灯信号;
 * 	     ZKHYTEST*LED:LEFT  表示车辆的左转向灯信号;
 * 	     ZKHYTEST*LED:RIGHT 表示车辆的右转向灯信号;
 * 	     ZKHYTEST*LED:BACK  表示车辆的倒车灯信号;
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 */
FunctionalState CarSignalTest_Logic(char *strx);

/*
 * Flash读写测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*FLASH:Write,8  表示写入FLASH 8个字节，写入内容为0xFF;
 * 	     ZKHYTEST*FLASH:Read,8     表示读出FLASH 8个字节，读出内容为0xFF;
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 */
FunctionalState FlashTest_Logic(char *strx);

/*
 * USART测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*USART:USARTX 表示对于USARTX进行测试
 * 	     USART2：超声波雷达
 * 	     USART4: 蓝牙
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 */
FunctionalState USARTTest_Logic(char *strx);


/*
 * I2C测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*I2C:WRITE 表示对于I2C进行写测试
 * 	     ZKHYTEST*I2C:READ 表示对于I2C进行写测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 */
FunctionalState I2CTest_Logic(char *strx);

/*
 * CAN测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*CAN:SEND 表示对于CAN进行写测试
 * 	     ZKHYTEST*CAN:RECV 表示对于CAN进行读测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 */
FunctionalState CANTest_Logic(char *strx);

/*****************************************************
 * 4G模块测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*4G:SIMDET 表示对SIM卡进行检测是否存在
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState Module4GTest_Logic(char *strx);

/*****************************************************
 * Wheel Speed Input测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*SPD:SPD 读取IO测试的脉冲数
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState WheelSpeedTest_Logic(char *strx);

/*****************************************************
 * 与工装握手测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*CONN:HANDSHAKE 握手测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState HandShakeTest_Logic(char *strx);

/*****************************************************
 * DAC测试程序
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*DAC:TEST DAC测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState DACTest_Logic(char *strx);

/*****************************************************
 * Bus 485测试程序
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*BUS:TEST485 485总线测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState Bus485Test_Logic(char *strx);


/*****************************************************
 * TTL 蓝牙串口测试程序
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*TTL:TEST TTL蓝牙串口测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState TTLTest_Logic(char *strx);

/*****************************************************
 * 设备PN/SN测试程序
 * 参数：读取写入PN/SN号码 ——
 * 	     ZKHYTEST*PNSN:WRITE,SN Part Number写入测试
 * 	     ZKHYTEST*PNSN:WRITE,SN Serial Number写入测试
 * 	     ZKHYTEST*PNSN:READ,SN Part Number读取测试
 * 	     ZKHYTEST*PNSN:READ,SN Serial Number读取测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState PNSNTest_Logic(char *strx);

/*****************************************************
 * 设备HW/SW VERSION测试程序
 * 参数：读取写入HW/SW版本号 ——
 * 	     ZKHYTEST*HWSW:WRITE,HW HWVersion写入测试
 * 	     ZKHYTEST*HWSW:WRITE,SW SW version写入测试
 * 	     ZKHYTEST*HWSW:READ,HW HWVersion读取测试
 * 	     ZKHYTEST*HWSW:READ,SW SW version读取测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState HWSWTest_Logic(char *strx);

/*****************************************************
 * CANFA重启测试程序
 * 参数：重新启动CANFA
 * 	     ZKHYTEST*CANFA:RESET,重启CANFA
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState CANFATest_Logic(char *strx);

/*
 * LED 测试
 * 参数：接收字符串 —— 1: 开启工装测试；0：关闭工装测试
 * 返回：FALSE失败；TRUE成功
 * 说明：
 */
FunctionalState ToolingTest_Logic(char *strx);

/*
 * 按照,进行分割字符串
 * 参数：分割字符串
 * 返回：0失败，1成功
 * 说明： 1223,456,789,adc,456,85ad21,dfdsa00>
 */
stringStrtok ChartoString(uint8_t *string);

void checkUsartReceiveData(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length);

/*
 * 4G返回字符按照,进行分割字符串
 * 参数：分割字符串
 * 返回：0失败，1成功
 * 说明： 1223,456,789,adc,456,85ad21,dfdsa00
 */
stringStrtok ReplytoString(uint8_t *string);

/*
 * 清空USART0缓冲区
 * 返回：无
 */
void Clear_Buffer(void);

//处理4G回应缓存
bool Tooling_Dispose_4g_Rcv_Msg();

/*****************************************************
 * 板卡PN/SN测试程序
 * 参数：读取写入PN/SN号码 ——
 * 	     ZKHYTEST*BOARD:WRITEPN,PN ——Part Number写入测试
 * 	     ZKHYTEST*BOARD:WRITESN,SN ——Serial Number写入测试
 * 	     ZKHYTEST*BOARD:READPN     ——Part Number读取测试
 * 	     ZKHYTEST*BOARD:READSN     ——Serial Number读取测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState Board_PNSNTest_Logic(char *strx);

#endif /* _TOOLING_H_ */
