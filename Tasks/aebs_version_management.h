/*
 * aebs_version_management.h
 *
 *  Created on: 2022-4-13
 *      Author: xujie
 */

#ifndef AEBS_VERSION_MANAGEMENT_H_
#define AEBS_VERSION_MANAGEMENT_H_
#include "stdint.h"
#include "upgrade_common.h"
#include "canhl.h"

typedef struct{
	uint8_t v0;
	uint8_t v1;
	uint8_t v2;
	uint8_t v3;
}Version_Num_Std;
typedef struct{
	//Version_Num_Std AEBS_SW_Version_Std;
	//uint8_t AEBS_SW_Version_L;
	//uint8_t AEBS_SW_Version_C;
	uint8_t AEBS_SW_Version[SH_INFO_SIZE];	// ���ڰ汾��
	char AEBS_SN[SH_INFO_SIZE];
	char AEBS_PN[SH_INFO_SIZE];
	//////////////////////////peripheral_Version////////////////////////
	uint8_t Got_Version_Info_Camera;
	uint8_t Got_Version_Info_Displayer;
	uint8_t Got_Version_Info_URadar;
	uint8_t Got_Version_Info_MRadar;
	Version_Num_Std Camera_SW_Version;
	Version_Num_Std Camera_HW_Version;
	Version_Num_Std Displayer_SW_Version;
	char Displayer_SN[SH_INFO_SIZE];
	char Displayer_PN[SH_INFO_SIZE];
	Version_Num_Std URadar_Version;
	Version_Num_Std MRadar_Version;
}SW_HW_Version_Info;

typedef struct{
	Version_Num_Std Camera_CAN_ProtocoI_Version;
	Version_Num_Std AEBS_AT_PC_ProtocoI_Version;
	Version_Num_Std AEBS_AT_BT_ProtocoI_Version;
	Version_Num_Std AEBS_CAN_ProtocoI_Version;
	Version_Num_Std Displayer_CAN_ProtocoI_Version;
	Version_Num_Std Cloud_Platform_ProtocoI_Version;
}Comm_Protocol_Version_Info;
///////////////////////////////////////////////////////////////////////
//Version_Info:
//////////////////////////////////////////////////////////////////////
extern SW_HW_Version_Info sw_hw_version;
extern Comm_Protocol_Version_Info comm_protocol_version;


///////////////////////////////////////////////////////////////////////
//Function: Got the Version Info
//////////////////////////////////////////////////////////////////////
void AEBS_Version_Init(void);
void CAN_Analysis_Camera_Version(struct can_frame *rx_frame);
void CAN_Analysis_Displayer_Version(struct can_frame *rx_frame);
void CAN_Analysis_URadar_Version(struct can_frame *rx_frame);
void CAN_Analysis_MRadar_Version(struct can_frame *rx_frame);
#endif /* AEBS_VERSION_MANAGEMENT_H_ */
