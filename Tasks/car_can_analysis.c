/*
 * car_can_analysis.c
 *
 *  Created on: 2022-9-26
 *      Author: Administrator
 */
#include "car_can_analysis.h"
#include "cqueue.h"
#include "_4g_data_upload.h"
#include "usart.h"
#include "aeb_sensor_management.h"
Car_Can_Object_Str g_car_can_object_info = {0};

#define CAR_CAN_INIT_CFG		2		// 【1：金旅大巴】【2：解放箱货】【3：BYD E6】

// 默认参数初始化
Car_CAN_Cfg_Param_Template cc_cfg_tmpl_arr[13] = {	// 执行顺序不能改变
		// 提示 is_used, byte_order, frame_id, start_bit, bit_count, factor, offset, min, max, {vector table}, set_value_num, invalid
#if (CAR_CAN_INIT_CFG == 1)			// 金旅大巴
		{CAR_CAN_USED, CAR_MOTOROLA, 0x0CFE6CEE, 56, 16, 0.00390625f, 0.0f, 0.0f, 255.996f, {0}, 1},					// 金旅大巴speed			// 单位：千米/小时  √

		{CAR_CAN_USED, CAR_MOTOROLA, 0x18A70017, 32, 2, 1.0f, 0.0f, 0.0f, 3.0f, {0,1}, 2, 0x80000003},					// 左转向灯状态	// 左转向值【0：不触发时的值】【1：触发左转向时值】

		{CAR_CAN_USED, CAR_MOTOROLA, 0x18A70017, 34, 2, 1.0f, 0.0f, 0.0f, 3.0f, {0,1}, 2, 0x80000003},					// 右转向灯状态	// 右转向值【0：不触发时的值】【1：触发右转向时值】

//		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F004, 26, 2, 1.0f, 0.0f, 0.0f, 255.0f, {0,1,2}, 3, 0},						// （司机刹车）制动踏板状态	// 司机刹车值【0：司机没刹车】【1：深踩制动踏板】【2：浅踩制动踏板】
		{CAR_CAN_USED, CAR_MOTOROLA, 0x18A70017, 36, 2, 1.0f, 0.0f, 0.0f, 3.0f, {0,1}, 2, 0x80000003},					// 制动状态（待定）	// 司机刹车值【0：制动关】【1：开始制动】

		{CAR_CAN_USED, CAR_MOTOROLA, 0x18A70017, 18, 2, 1.0f, 0.0f, 0.0f, 3.0f, {0,1}, 2, 0x80000003},					// 档位信息		// 【0：倒挡关】【1：R档】

		{CAR_CAN_USED, CAR_MOTOROLA, 0x18FEC1EE, 24, 32, 5.0f, 0.0f, 0.0f, 429496729.0f, {1}, 1, 0},					// （总）里程计		// 单位：【0：千米】【1：米】 需要确认里程1和里程1与这个总里程数的区别

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F002, 32, 16, 0.1f, 0.0f, -780.0f, 779.9f, {0,0}, 2, 0x80007FFF},				// 方向盘转角	// 单位：【0：角度】【1：弧度】【0：右打脉冲数增加】【1：右打脉冲数减少】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 0, 8, 1.0f, 0.0f, 0.0f, 100.0f, {0}, 1, 0},								// 油门深度	// 单位：【0：百分比】【1：其他】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 8, 8, 1.0f, 0.0f, 0.0f, 100.0f, {0}, 1, 0},								// （刹车踏板）制动深度	// 单位：【0：百分比】【1：其他】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 0, 4, 1.0f, 0.0f, 0.0f, 255.0f, {0}, 1, 0},								// 偏航角		// 单位：【0：角度】【1：弧度】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 32, 16, 0.1f, 0.0f, -780.0f, 779.9f, {0}, 1, 0},						// 发动机转速	// 单位：【0：转/分钟】【1：转/秒】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 0, 4, 1.0f, 0.0f, 0.0f, 255.0f, {0}, 1, 0},								// 俯仰角	// 单位：【0：角度】【1：弧度】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F021, 0, 16, 0.0004882f, -16.0f, -16.0f, 16.0f, {0}, 1, 0x8000FFFF},			// 加速度	(减速时有值，加速时无值)// 单位：【0：米/(秒*秒)】【1：千米/(小时*小时)】
#endif

#if (CAR_CAN_INIT_CFG == 2)			// 解放箱货
		{CAR_CAN_USED, CAN_INTEL, 0xCFE6CEE, 48, 16, 0.00390625f, 0.0f, 0.0f, 250.996f, {0}, 1, 0},						// 厢货车速speed			// 单位：千米/小时  √
//		{CAR_CAN_USED, CAN_INTEL, 0x18FEF100, 0, 16, 0.00390625f, 0.0f, 0.0f, 250.996f, {0}, 1, 0},						// 厢货车速speed			// 单位：千米/小时  √

		{CAR_CAN_USED, CAN_INTEL, 0xCFDCC21, 8, 1, 1.0f, 0.0f, 0.0f, 1.0f, {0,1}, 2, 0x80000001},						// 左转向灯状态	// 左转向值【0：不触发时的值】【1：触发左转向时值】

		{CAR_CAN_USED, CAN_INTEL, 0xCFDCC21, 9, 1, 1.0f, 0.0f, 0.0f, 1.0f, {0,1}, 2, 0x80000001},						// 右转向灯状态	// 右转向值【0：不触发时的值】【1：触发右转向时值】

//		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F004, 26, 2, 1.0f, 0.0f, 0.0f, 255.0f, {0,1,2}, 3, 0},						// （司机刹车）制动踏板状态	// 司机刹车值【0：司机没刹车】【1：深踩制动踏板】【2：浅踩制动踏板】
		{CAR_CAN_USED, CAN_INTEL, 0x18FEF100, 28, 2, 1.0f, 0.0f, 0.0f, 3.0f, {0,1}, 2, 0x80000003},						// 制动状态（待定）	// 司机刹车值【0：制动关】【1：开始制动】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x18A70017, 18, 2, 1.0f, 0.0f, 0.0f, 3.0f, {0,1}, 2, 0x80000003},					// 档位信息		// 【0：倒挡关】【1：R档】

		{CAR_CAN_USED, CAN_INTEL, 0x18FEE017, 32, 32, 0.125f, 0.0f, 0.0f, 526385151.9f, {0}, 1, 0},						// （总）里程计		// 单位：【0：千米】【1：米】 需要确认里程1和里程1与这个总里程数的区别

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F002, 32, 16, 0.1f, 0.0f, -780.0f, 779.9f, {0,0}, 2, 0x80007FFF},				// 方向盘转角	// 单位：【0：角度】【1：弧度】【0：右打脉冲数增加】【1：右打脉冲数减少】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 0, 8, 1.0f, 0.0f, 0.0f, 100.0f, {0}, 1, 0},								// 油门深度	// 单位：【0：百分比】【1：其他】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 8, 8, 1.0f, 0.0f, 0.0f, 100.0f, {0}, 1, 0},								// （刹车踏板）制动深度	// 单位：【0：百分比】【1：其他】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 0, 4, 1.0f, 0.0f, 0.0f, 255.0f, {0}, 1, 0},								// 偏航角		// 单位：【0：角度】【1：弧度】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 32, 16, 0.1f, 0.0f, -780.0f, 779.9f, {0}, 1, 0},						// 发动机转速	// 单位：【0：转/分钟】【1：转/秒】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 0, 4, 1.0f, 0.0f, 0.0f, 255.0f, {0}, 1, 0},								// 俯仰角	// 单位：【0：角度】【1：弧度】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F021, 0, 16, 0.0004882f, -16.0f, -16.0f, 16.0f, {0}, 1, 0x8000FFFF},			// 加速度	(减速时有值，加速时无值)// 单位：【0：米/(秒*秒)】【1：千米/(小时*小时)】

#endif

#if (CAR_CAN_INIT_CFG == 3)			// BYD E6
		{CAR_CAN_USED, CAN_INTEL, 0x1C01F002, 0, 12, 0.06875f, 0.0f, 0.0f, 281.4625f, {0}, 1, 0x80000FFF},	// BYD E6 speed			// 单位：千米/小时	√

		{CAR_CAN_USED, CAN_INTEL, 0x1C01F004, 11, 1, 1.0f, 0.0f, 0.0f, 1.0f, {0,1}, 2, 0},					// BYD E6 左转向灯状态	// 左转向值【0：不触发时的值】【1：触发左转向时值】

		{CAR_CAN_USED, CAN_INTEL, 0x1C01F004, 12, 1, 1.0f, 0.0f, 0.0f, 1.0f, {0,1}, 2, 0},					// BYD E6 右转向灯状态	// 右转向值【0：不触发时的值】【1：触发右转向时值】

		{CAR_CAN_USED, CAN_INTEL, 0x1C01F004, 26, 2, 1.0f, 0.0f, 0.0f, 255.0f, {0,1,2}, 3, 0},				// BYD E6（司机刹车）制动踏板状态	// 司机刹车值【0：司机没刹车】【1：深踩制动踏板】【2：浅踩制动踏板】

		{CAR_CAN_USED, CAN_INTEL, 0x1C01F003, 28, 4, 1.0f, 0.0f, 0.0f, 12.0f, {1,3,4,2}, 4, 0},				// BYD E6档位信息		// 【1：P档】【3：N档】【4：D档】【2：R档】

		{CAR_CAN_USED, CAN_INTEL, 0x1C01F007, 8, 24, 1.0f, 0.0f, 0.0f, 14680064.0f, {0}, 1, 0x80FFFFFF},	// BYD E6 里程计		// 单位：【0：千米】【1：米】 需要确认里程1和里程1与这个总里程数的区别

		{CAR_CAN_USED, CAN_INTEL, 0x1C01F002, 32, 16, 0.1f, 0.0f, -780.0f, 779.9f, {0,0}, 2, 0x80007FFF},		// BYD E6 方向盘转角	// 单位：【0：角度】【1：弧度】 【0：右打脉冲数增加】【1：右打脉冲数减少】

		{CAR_CAN_USED, CAN_INTEL, 0x1C01F003, 0, 8, 1.0f, 0.0f, 0.0f, 100.0f, {0}, 1, 0},					// BYD E6 油门深度	// 单位：【0：百分比】【1：其他】
//		{CAR_CAN_USED, CAN_INTEL, 0x1C01F019, 8, 8, 1.0f, 0.0f, 0.0f, 100.0f, {0}, 1},						// BYD E6 油门深度	// 单位：【0：百分比】【1：其他】

		{CAR_CAN_USED, CAN_INTEL, 0x1C01F003, 8, 8, 1.0f, 0.0f, 0.0f, 100.0f, {0}, 1, 0},					// BYD E6 （刹车踏板）制动深度	// 单位：【0：百分比】【1：其他】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 0, 4, 1.0f, 0.0f, 0.0f, 255.0f, {0}, 1, 0},					// 偏航角		// 单位：【0：角度】【1：弧度】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 32, 16, 0.1f, 0.0f, -780.0f, 779.9f, {0}, 1, 0},			// 发动机转速	// 单位：【0：转/分钟】【1：转/秒】

		{CAR_CAN_UNUSED, CAN_INTEL, 0x1C01F003, 0, 4, 1.0f, 0.0f, 0.0f, 255.0f, {0}, 1, 0},					// 俯仰角	// 单位：【0：角度】【1：弧度】

		{CAR_CAN_USED, CAN_INTEL, 0x1C01F021, 0, 16, 0.0004882f, -16.0f, -16.0f, 16.0f, {0}, 1, 0x8000FFFF},// 加速度	(减速时有值，加速时无值)// 单位：【0：米/(秒*秒)】【1：千米/(小时*小时)】
#endif
};


extern float Character2Float(uint8_t *data, uint8_t len);

// 获取外部FLASH中车CAN解析协议参数
bool Get_Car_Can_Analysis_Info(Car_Can_Object_Str *p)
{
	uint8_t data[DATA_LEN_512B] = { 0 };
	memset(data, 0, DATA_LEN_512B);

	if (Read_Car_Can_Analysis_Info(data, DATA_LEN_512B)) { // 获取数据
		memcpy(p, data, sizeof(Car_Can_Object_Str)); // copy
//		USART1_Bebug_Print("Car Can Analysis", "Read cfg Parameter Success.", 1);
		return true;
	}

	USART1_Bebug_Print("Car Can Analysis", "Read cfg Parameter Failed.", 1);
	return false;
}

// 设置外部FLASH中的车CAN解析协议参数
bool Set_Car_Can_Analysis_Info(Car_Can_Object_Str p)
{
	uint8_t data[DATA_LEN_512B] = { 0 };
	memcpy(data, &p, sizeof(Car_Can_Object_Str));

	if (Write_Car_Can_Analysis_Info(data, DATA_LEN_512B)) { // 写数据
//		USART1_Bebug_Print("Car Can Analysis", "Write cfg Parameter Success.", 1);
		return true;
	}

	USART1_Bebug_Print("Car Can Analysis", "Write cfg Parameter Failed.", 1);

	return false;
}


inline void Car_Can_Cfg_To_Assign_template(Car_Can_Dymc_Cfg_Str *p, Car_CAN_Cfg_Param_Template p_cc_cfg)
{
	memset(p, 0, sizeof(Car_Can_Dymc_Cfg_Str));

	p->config_info = p_cc_cfg.is_used;				// 是否使用，【0不使用】【1使用】
	p->config_info |= p_cc_cfg.byte_order<<1;		// 字节顺序，【0小端模式】【大端模式】

	//
	switch(p_cc_cfg.set_value_num){	// 信息触发的值，个数
	case 2:{
		p->config_info |= p_cc_cfg.set_value.value1<<2;	//
		p->config_info |= p_cc_cfg.set_value.value2<<5;
	}break;
	case 3:{
		p->config_info |= p_cc_cfg.set_value.value1<<2;
		p->config_info |= p_cc_cfg.set_value.value2<<5;
		p->config_info |= p_cc_cfg.set_value.value3<<8;
	}break;
	case 4:{
		p->config_info |= p_cc_cfg.set_value.value1<<2;	// P // 倒挡信息
		p->config_info |= p_cc_cfg.set_value.value2<<5;	// N
		p->config_info |= p_cc_cfg.set_value.value3<<8;	// D
		p->config_info |= p_cc_cfg.set_value.value4<<11;// R
	}break;
	default:{
		p->config_info |= p_cc_cfg.set_value.value1<<2;	// 单位
	}break;
	}

	p->frame_id = p_cc_cfg.frame_id;					// 帧ID
	p->start_bit = p_cc_cfg.start_bit;					// 起始地址

	p->bit_count = p_cc_cfg.bit_count;					// 有效长度
	p->factor = p_cc_cfg.factor;						// 比例因数
	p->offset = p_cc_cfg.offset;						// 补偿值
	p->min = p_cc_cfg.min;								// 最小值
	p->max = p_cc_cfg.max;								// 最大值
	p->invalid = p_cc_cfg.invalid;						// 无效值
}
/*
 * 车CAN解析配置发送模板
 */
inline void Car_Can_Cfg_To_Send_template(NodeData_S *m_noda_data, Car_Can_Dymc_Cfg_Str p)
{
	char *l_cfg_param = NULL;
	// config_info
	l_cfg_param = (uint8_t *)&p.config_info;
	Set_Request_ASCII_Info_Template(m_noda_data, l_cfg_param, UINT_16, 2);

	//frame_id
	l_cfg_param = (uint8_t *)&p.frame_id;
	Set_Request_ASCII_Info_Template(m_noda_data, l_cfg_param, UINT_32, 4);

	// start_bit
	Set_Request_ASCII_Info_Template(m_noda_data, &p.start_bit, UINT_8, 1);

	// bit count
	Set_Request_ASCII_Info_Template(m_noda_data, &p.bit_count, UINT_8, 1);

	// factor
	l_cfg_param = (uint8_t *)&p.factor;
	Set_Request_ASCII_Info_Template(m_noda_data, l_cfg_param, FLOAT, 4);

	// offset
	l_cfg_param = (uint8_t *)&p.offset;
	Set_Request_ASCII_Info_Template(m_noda_data, l_cfg_param, FLOAT, 4);

	// min
	l_cfg_param = (uint8_t *)&p.min;
	Set_Request_ASCII_Info_Template(m_noda_data, l_cfg_param, FLOAT, 4);

	// max
	l_cfg_param = (uint8_t *)&p.max;
	Set_Request_ASCII_Info_Template(m_noda_data, l_cfg_param, FLOAT, 4);

	// invalid
	l_cfg_param = (uint8_t *)&p.invalid;
	Set_Request_ASCII_Info_Template(m_noda_data, l_cfg_param, UINT_32, 4);
}

/*
 * 车CAN解析配置解析模板
 */
inline void Car_Can_Cfg_To_Analysis_template(Car_Can_Dymc_Cfg_Str *p, uint8_t *data, const uint8_t len)
{
#if 0
	uint8_t print[6] = {0};
	for(uint16_t i=0; i<len;i++){
		sprintf(print, "%02X ", data[i]);
		Usart1_DMA_Transmit(print, strlen(print));
	}
	sprintf(print, "\r\n\r\n");
	Usart1_DMA_Transmit(print, strlen(print));
#endif
	// 01 00 EE 6C FE 0C 30 10 FA FF 7F 3B 00 00 00 00 00 00 00 00 FA FE 7A 43 00 00 00 00
	p->config_info = (data[1] << 8) | data[0];
	p->frame_id = (data[5] << 24) | (data[4] << 16) | (data[3] << 8) | data[2];
	p->start_bit = data[6];
	p->bit_count = data[7];
	p->factor = Character2Float(data+8, 4);
	p->offset = Character2Float(data+12, 4);
	p->min = Character2Float(data+16, 4);
	p->max = Character2Float(data+20, 4);		// index + 23
	p->invalid = (data[27] << 24) | (data[26] << 16) | (data[25] << 8) | data[24];
}
/*
 * 车CAN解析配置调试打印模板
 */
void Car_Can_Cfg_To_Print_template(uint8_t print_id, Car_Can_Dymc_Cfg_Str p, uint8_t diff_flag)
{
	uint8_t print_info[30] = {0};
	switch(print_id){
	case CAR_CAN_ANALY_CAR_SPEED:		strcpy(print_info, "[Car Speed]");break;
	case CAR_CAN_ANALY_TURN_LEFT:		strcpy(print_info, "[Turn Left]");break;
	case CAR_CAN_ANALY_TURN_RIGHT:		strcpy(print_info, "[Turn Right]");break;
	case CAR_CAN_ANALY_DRIVER_BRAKE:	strcpy(print_info, "[Driver Brake]");break;
	case CAR_CAN_ANALY_GEAR:			strcpy(print_info, "[Car Gear]");break;
	case CAR_CAN_ANALY_ODOM:			strcpy(print_info, "[Car Odom]");break;
	case CAR_CAN_ANALY_STEER_WHEEL:		strcpy(print_info, "[Steer Wheel Angle]");break;
	case CAR_CAN_ANALY_THROTTAL_DEPTH:	strcpy(print_info, "[Throttle Depth]");break;
	case CAR_CAN_ANALY_BRAKE_DEPTH:		strcpy(print_info, "[Brake Depth]");break;
	case CAR_CAN_ANALY_YAW_ANGLE:		strcpy(print_info, "[Yaw Angle]");break;
	case CAR_CAN_ANALY_ENGINE_SPEED:	strcpy(print_info, "[Engine Speed]");break;
	case CAR_CAN_ANALY_PITCH_ANGLE:		strcpy(print_info, "[Pitch Angle]");break;
	case CAR_CAN_ANALY_ACCELERATION:	strcpy(print_info, "[Accelerated Speed]");break;
	default:return ;
	}
	Usart1_DMA_Transmit(print_info, strlen(print_info));

	sprintf(print_info, "------isEnable: %s\r\n", (p.config_info & 0x01) == 1 ? "Yes":"No");
	Usart1_DMA_Transmit(print_info, strlen(print_info));
	sprintf(print_info, "byteOrder: %s\t", ((p.config_info >> 1) & 0x01) == 1? "Motorola":"Intel");
	Usart1_DMA_Transmit(print_info, strlen(print_info));

	sprintf(print_info, "frameID:0x%08X\t", p.frame_id);
	Usart1_DMA_Transmit(print_info, strlen(print_info));
	USART1_Bebug_Print_Num("startBit", p.start_bit, 0, 1);
	USART1_Bebug_Print_Num("bitLen", p.bit_count, 1, 1);
	USART1_Bebug_Print_Flt("factor", p.factor, 2, 1);
	USART1_Bebug_Print_Flt("offset", p.offset, 2, 1);
	USART1_Bebug_Print_Flt("min", p.min, 2, 1);
	USART1_Bebug_Print_Flt("max", p.max, 2, 1);

	switch(diff_flag){
	case 2:{
		USART1_Bebug_Print_Num("value1", (p.config_info>>2) & 0x07, 0, 1);
		USART1_Bebug_Print_Num("value2", (p.config_info>>5) & 0x07, 1, 1);
	}break;
	case 3:{
		USART1_Bebug_Print_Num("value1", (p.config_info>>2) & 0x07, 0, 1);
		USART1_Bebug_Print_Num("value2", (p.config_info>>5) & 0x07, 0, 1);
		USART1_Bebug_Print_Num("value3", (p.config_info>>8) & 0x07, 1, 1);
	}break;
	case 4:{
#if 1
		USART1_Bebug_Print_Num("P_Gear", (p.config_info>>2) & 0x07, 0, 1);
		USART1_Bebug_Print_Num("N_Gear", (p.config_info>>5) & 0x07, 0, 1);
		USART1_Bebug_Print_Num("D_Gear", (p.config_info>>8) & 0x07, 0, 1);
		USART1_Bebug_Print_Num("R_Gear", (p.config_info>>11) & 0x07, 1, 1);
#else
		USART1_Bebug_Print_Num("value1", (p.config_info>>2) & 0x07, 0, 1);
		USART1_Bebug_Print_Num("value2", (p.config_info>>5) & 0x07, 0, 1);
		USART1_Bebug_Print_Num("value3", (p.config_info>>8) & 0x07, 0, 1);
		USART1_Bebug_Print_Num("value4", (p.config_info>>11) & 0x07, 1, 1);
#endif
	}break;
	default:{
		USART1_Bebug_Print_Num("Unit", (p.config_info>>2) & 0x07, 1, 1);
	}break;
	}

	if(p.invalid >> 31){		// 使能对无效值的过滤功能
		sprintf(print_info, "invalid_value:0x%08X\r\n", p.invalid & 0x7FFFFFFF);
		Usart1_DMA_Transmit(print_info, strlen(print_info));
	}
}

// 加载默认参数
void Load_Default_Car_Can_Anslysis_Info(Car_Can_Object_Str * p)
{
	memset(p, 0, sizeof(Car_Can_Object_Str));

	Car_Can_Cfg_To_Assign_template(&p->car_speed, cc_cfg_tmpl_arr[0]);
	Car_Can_Cfg_To_Assign_template(&p->car_turn_left, cc_cfg_tmpl_arr[1]);
	Car_Can_Cfg_To_Assign_template(&p->car_turn_right, cc_cfg_tmpl_arr[2]);
	Car_Can_Cfg_To_Assign_template(&p->car_driver_brake, cc_cfg_tmpl_arr[3]);
	Car_Can_Cfg_To_Assign_template(&p->car_gear, cc_cfg_tmpl_arr[4]);

	Car_Can_Cfg_To_Assign_template(&p->car_odom, cc_cfg_tmpl_arr[5]);
	Car_Can_Cfg_To_Assign_template(&p->car_steer_wheel_angle, cc_cfg_tmpl_arr[6]);
	Car_Can_Cfg_To_Assign_template(&p->car_throttle_depth, cc_cfg_tmpl_arr[7]);
	Car_Can_Cfg_To_Assign_template(&p->car_brake_depth, cc_cfg_tmpl_arr[8]);
	Car_Can_Cfg_To_Assign_template(&p->car_yaw_angle, cc_cfg_tmpl_arr[9]);

	Car_Can_Cfg_To_Assign_template(&p->car_engine_speed, cc_cfg_tmpl_arr[10]);
	Car_Can_Cfg_To_Assign_template(&p->car_pitch_angle, cc_cfg_tmpl_arr[11]);
	Car_Can_Cfg_To_Assign_template(&p->car_accelerated_speed, cc_cfg_tmpl_arr[12]);

	Debug_Print_Car_Can_Analysis_Info();
}

// 解析平台设置的车CAN解析协议参数
bool Analysis_From_Platform_Car_Can_Analysis_Info(uint8_t *data, uint16_t data_len)
{
#if 0
	uint8_t print[6] = {0};
	for(uint16_t i=0; i<data_len;i++){
		sprintf(print, "%02X ", data[i]);
		Usart1_DMA_Transmit(print, strlen(print));
	}
	sprintf(print, "\r\n\r\n");
	Usart1_DMA_Transmit(print, strlen(print));
#endif

	memset(&g_car_can_object_info, 0, sizeof(Car_Can_Object_Str));
	uint16_t index = 0;
	const uint8_t len = 28;		// 有效数据长度

	if(data_len < len * 13) return false;		// 长度太短直接返回

	// 解析
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_speed, data + index, len); 				index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_turn_left, data + index, len); 			index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_turn_right, data + index, len); 		index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_driver_brake, data + index, len);		index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_gear, data + index, len); 				index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_odom, data + index, len); 				index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_steer_wheel_angle, data + index, len); 	index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_throttle_depth, data + index, len); 	index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_brake_depth, data + index, len); 		index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_yaw_angle, data + index, len); 			index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_engine_speed, data + index, len);		index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_pitch_angle, data + index, len);		index += len;
	Car_Can_Cfg_To_Analysis_template(&g_car_can_object_info.car_accelerated_speed, data + index, len);

#if 0
	Debug_Print_Car_Can_Analysis_Info();
#endif
	// 存
	if(Set_Car_Can_Analysis_Info(g_car_can_object_info)){
		// 将设置成功的反馈信息发送成功后，就重启设备
		return true;
	}else{	// 配置失败，加载原有配置
		USART1_Bebug_Print("Car Can Analysis", " Write Failed, Load The Original Data.", 1);
		Set_Car_Can_Analysis_Info(g_car_can_object_info);
	}

	return false;
}

// 向平台请求车CAN解析配置参数
bool Request_Get_Car_Can_Analysis_Cfg_Param_Info_ASCII()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return false;
//	if(!server_info.isConnect) return false;

	// 填数
	static uint16_t car_can_analy_seq = 1;
	NodeData_S m_noda_data;
	is_reply = false;

	if(!Request_Protocal_Head_ASCII(car_can_analy_seq, GET_CAR_CAN_ANALYSIS_PARAM, m_noda_data.data)){
		return false;
	}

	car_can_analy_seq++;
	req_index = 31;

	// 内容为空，长度为0

	// 校验
	Set_Request_ASCII_Info_Template(&m_noda_data, "", CRC16, 2);

	// 放到队列中进行发送
	sprintf(m_noda_data.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, m_noda_data.data_len);
	m_noda_data.is_wait_reply = NEED_WAIT_REPLY;
	m_noda_data.is_store = NEED_STORED;
	m_noda_data.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &m_noda_data);

	return true;
}

// 回复平台车CAN解析协议参数
bool Reply_Platform_Request_Car_Can_Analysis_Cfg_Param_Info_ASCII(uint16_t seqNum)
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return false;

	NodeData_S m_noda_data;
	// 先获取参数配置
	Car_Can_Object_Str l_cc;
	if(!Get_Car_Can_Analysis_Info(&l_cc)){
		// 加载默认参数
		Load_Default_Car_Can_Anslysis_Info(&l_cc);
	}

	// 帧头 0xAA55 C9
	if(!Reply_Protocal_Head_ASCII(seqNum, GET_CAR_CAN_ANALYSIS_PARAM, 0x00, m_noda_data.data)){
		return false;
	}

	is_reply = true;
	req_index = 33;

	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_speed);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_turn_left);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_turn_right);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_driver_brake);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_gear);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_odom);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_steer_wheel_angle);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_throttle_depth);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_brake_depth);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_yaw_angle);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_engine_speed);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_pitch_angle);
	Car_Can_Cfg_To_Send_template(&m_noda_data, l_cc.car_accelerated_speed);

	// 校验
	Set_Request_ASCII_Info_Template(&m_noda_data, "", CRC16, 2);

	// 放到队列中进行发送
	sprintf(m_noda_data.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, m_noda_data.data_len);
	m_noda_data.is_wait_reply = NEED_WAIT_REPLY_REPLY;
	m_noda_data.is_store = NEED_STORED;
	m_noda_data.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &m_noda_data);

	return false;
}

void Debug_Print_Car_Can_Analysis_Info()
{
	USART1_Bebug_Print("", "----------------------------------------------------------------------", 1);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_CAR_SPEED, g_car_can_object_info.car_speed, 1);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_TURN_LEFT, g_car_can_object_info.car_turn_left, 2);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_TURN_RIGHT, g_car_can_object_info.car_turn_right, 2);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_DRIVER_BRAKE, g_car_can_object_info.car_driver_brake, 2);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_GEAR, g_car_can_object_info.car_gear, 4);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_ODOM, g_car_can_object_info.car_odom, 1);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_STEER_WHEEL, g_car_can_object_info.car_steer_wheel_angle, 1);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_THROTTAL_DEPTH, g_car_can_object_info.car_throttle_depth, 1);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_BRAKE_DEPTH, g_car_can_object_info.car_brake_depth, 1);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_YAW_ANGLE, g_car_can_object_info.car_yaw_angle, 1);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_ENGINE_SPEED, g_car_can_object_info.car_engine_speed, 1);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_PITCH_ANGLE, g_car_can_object_info.car_pitch_angle, 1);
	Car_Can_Cfg_To_Print_template(CAR_CAN_ANALY_ACCELERATION, g_car_can_object_info.car_accelerated_speed, 1);
	USART1_Bebug_Print("", "----------------------------------------------------------------------", 1);
}


/*
 * 获取车CAN解析协议参数所有数据
 */
Car_Can_Object_Str* Get_Car_Can_Analysis_Cfg_Param()
{
	return &g_car_can_object_info;
}

bool Car_Can_Analysis_Init()
{
	memset(&g_car_can_object_info, 0, sizeof(Car_Can_Object_Str));

	// 获取外部FLASH中车CAN解析配置
	if(Get_Car_Can_Analysis_Info(&g_car_can_object_info) == false){
		// 要是加载失败，是否需要向平台索取，待定

		// 暂定失败后，加载默认参数，加载默认参数时不进行保存
		Load_Default_Car_Can_Anslysis_Info(&g_car_can_object_info);
//		Set_Car_Can_Analysis_Info(g_car_can_object_info);
		return false;
	}

//	Debug_Print_Car_Can_Analysis_Info(g_car_can_object_info);

	return true;
}

inline uint8_t Shift_Opertion(uint8_t num)
{
    uint8_t shift_value = 0;
    switch (num) {
    case 1:shift_value = 0x01; break;
    case 2:shift_value = 0x03; break;
    case 3:shift_value = 0x07; break;
    case 4:shift_value = 0x0F; break;
    case 5:shift_value = 0x1F; break;
    case 6:shift_value = 0x3F; break;
    case 7:shift_value = 0x7F; break;
    case 8:shift_value = 0xFF; break;
    default:shift_value = 0;
    }

    return shift_value;
}

/*
 * 大端模式CA解析
 * 参数1：CAN data[64]
 * 参数2：起始bit下标
 * 参数3：解析bit长度
 * 参数4：解析出的u32数据
 */
bool Motorola_Mode_Analysis(uint8_t *f_data, uint8_t start_bit, uint8_t length, uint32_t *ret_u32)
{
	if (start_bit > 63) return false;
	if (length > 32) return false;

	uint8_t stt_index = start_bit / 8;
	uint8_t stt_right = start_bit % 8;
	uint8_t stt_left = 8 - stt_right;

	// 超出可用长度就返回
	uint8_t can_use_len = stt_index * 8 + stt_left;
//	printf("can_use_len:%d\n", can_use_len);
	if (length > can_use_len) return false;

	uint8_t loop_times = length / 8;

	//printf("stt_index:%d, stt_right:%d, stt_left:%d, len:%d\n", stt_index, stt_right, stt_left, length);
	*ret_u32 = 0;

	if (stt_left >= length) {		// length值（长度）小于等于8 且没有换行
		*ret_u32 = ((f_data[stt_index] >> stt_right) & Shift_Opertion(length));
	}
	else {
		//printf("times:%d, keep:%d\n", loop_times, remain_len);
		if (loop_times == 0) {	// length值小于8且换行了
			*ret_u32 = ((f_data[stt_index] >> stt_right) & Shift_Opertion(stt_left)) | \
				((f_data[stt_index - 1] & Shift_Opertion(length - stt_left)) << stt_left);
		}
		else {					// length长度大于等于8 且换行了
			uint8_t data_t = 0;
			uint8_t wr_len = 0;
			if ((length % 8) > 0) loop_times++;
			uint8_t length_t = length;

			for (uint8_t i = 1; i <= loop_times; i++) {
				data_t = 0;

				//printf("index_t:%d, stt_left:%d, length_t:%d\n", stt_index - i, stt_left, length_t);

			   if (i < loop_times) {    // 直接写8bit
				   length_t = length - 8 * i;
					data_t = (f_data[stt_index - i + 1] >> stt_right) & Shift_Opertion(stt_left) | \
						((f_data[stt_index - i] & Shift_Opertion(stt_right)) << stt_left);
					//wr_len++;
				}
				else {  // 尾包，判断是否换行了
					if (stt_left >= length_t) { //  没有换行，直接写
						data_t = (f_data[stt_index - i + 1] >> stt_right) & Shift_Opertion(length_t);
					}
					else {  // 换行了，分两行去写
						data_t = ((f_data[stt_index - i + 1] >> stt_right) & Shift_Opertion(stt_left)) | \
							((f_data[stt_index - i] & Shift_Opertion(length_t - stt_left)) << stt_left);
					}
				}

				//printf("wr_len:%d, data_t:%02X\n", wr_len, data_t);
				*ret_u32 |= data_t << ((loop_times - wr_len - 1) * 8);
				wr_len++;
			}
		}
	}

	return true;
}

/*
 * 小端模式CA解析
 * 参数1：CAN data[64]
 * 参数2：起始bit下标
 * 参数3：解析bit长度
 * 参数4：解析出的u32数据
 */
bool Intel_Mode_Analysis(uint8_t *f_data, uint8_t start_bit, uint8_t length, uint32_t *ret_u32)
{
	if (start_bit > 64) return false;   // 起始bit地址不能超出63
	if (length > 32) return false;      // 读取数据长度不能超出32bit
	// 判断读长度是否超出了已知长度
//	printf("can_use_len:%d\n", start_bit + length);
	if ((start_bit + length) > 64)  return false;

	uint8_t stt_index = start_bit / 8;
	uint8_t stt_right = start_bit % 8;
	uint8_t stt_left = 8 - stt_right;

	uint8_t loop_times = length / 8;
	uint8_t loop_keep = length % 8;

	*ret_u32 = 0;

	if (stt_left >= length) {    // 长度小于等于8 且 没有换行
		*ret_u32 = (f_data[stt_index] >> stt_right) & Shift_Opertion(length);
	}
	else {
		if (loop_times == 0) {      // 长度小于8 且 换行了
			*ret_u32 = ((f_data[stt_index] >> stt_right) & Shift_Opertion(stt_left)) | \
				((f_data[stt_index + 1] & Shift_Opertion(length - stt_left)) << stt_left);
		}
		else {
			uint8_t wr_len = 0;
			uint32_t length_t = length;
			uint8_t data_t = 0;
			if (loop_keep > 0) loop_times++;

			for (uint8_t i = 1; i <= loop_times; i++) {
				data_t = 0;

				//printf("index_t:%d, stt_left:%d, length_t:%d\n", stt_index + i-1, stt_left, length_t);

				if(i < loop_times){                 // 长度能整除8bit（8bit的整数倍）
					length_t = length - 8 * i;
					data_t = ((f_data[stt_index + i - 1] >> stt_right) & Shift_Opertion(stt_left)) | \
						((f_data[stt_index + i] & Shift_Opertion(stt_right)) << stt_left);
				}
				else {
					if (stt_left >= length_t) {    // 剩余的长度，无需换行取
						data_t = (f_data[stt_index + i - 1] >> stt_right) & Shift_Opertion(length_t);
					}
					else {                          // 剩余的长度，需要换行才能取全
						data_t = ((f_data[stt_index + i -1] >> stt_right) & Shift_Opertion(stt_left)) | \
							((f_data[stt_index + i] & Shift_Opertion(length_t - stt_left)) << stt_left);
					}
				}

				//printf("wr_len:%d, data_t:%02X\n", wr_len, data_t);
				*ret_u32 |= (data_t << (wr_len * 8));
				wr_len++;
			}
		}
	}
	return true;
}


/*
 * 车CAN动态解析CAN信号数据
 * 参数1：信号编号
 * 参数2：CAN数据
 */
float Car_Can_Decode_Signal_Data(uint8_t signal_id, uint8_t *p_data)
{
	Car_Can_Dymc_Cfg_Str *l_signal_handle = NULL;
	float l_ret_flt_value = 0.0f;
	uint32_t ret_u32 = 0;
	bool ret_status = false;

	switch(signal_id){
	case CAR_CAN_ANALY_CAR_SPEED: 		l_signal_handle = &g_car_can_object_info.car_speed; break;
	case CAR_CAN_ANALY_TURN_LEFT: 		l_signal_handle = &g_car_can_object_info.car_turn_left; break;
	case CAR_CAN_ANALY_TURN_RIGHT: 		l_signal_handle = &g_car_can_object_info.car_turn_right; break;
	case CAR_CAN_ANALY_DRIVER_BRAKE: 	l_signal_handle = &g_car_can_object_info.car_driver_brake; break;
	case CAR_CAN_ANALY_GEAR: 			l_signal_handle = &g_car_can_object_info.car_gear; break;

	case CAR_CAN_ANALY_ODOM: 			l_signal_handle = &g_car_can_object_info.car_odom; break;
	case CAR_CAN_ANALY_STEER_WHEEL: 	l_signal_handle = &g_car_can_object_info.car_steer_wheel_angle; break;
	case CAR_CAN_ANALY_THROTTAL_DEPTH: 	l_signal_handle = &g_car_can_object_info.car_throttle_depth; break;
	case CAR_CAN_ANALY_BRAKE_DEPTH: 	l_signal_handle = &g_car_can_object_info.car_brake_depth; break;
	case CAR_CAN_ANALY_YAW_ANGLE: 		l_signal_handle = &g_car_can_object_info.car_yaw_angle; break;

	case CAR_CAN_ANALY_ENGINE_SPEED: 	l_signal_handle = &g_car_can_object_info.car_engine_speed; break;
	case CAR_CAN_ANALY_PITCH_ANGLE: 	l_signal_handle = &g_car_can_object_info.car_pitch_angle; break;
	case CAR_CAN_ANALY_ACCELERATION: 	l_signal_handle = &g_car_can_object_info.car_accelerated_speed; break;
	default: return l_ret_flt_value;
	}

	uint8_t l_byte_order = (l_signal_handle->config_info >> 1) & 0x01;

#if 0		// debug print
	uint8_t de_print[100] = {0};
	sprintf(de_print, "byte:%d,start_bit:%d, len:%d, factor:%0.8f, offset:%0.2f\r\n ", l_byte_order, \
			l_signal_handle->start_bit, l_signal_handle->bit_count, \
			l_signal_handle->factor, l_signal_handle->offset);
	USART1_Bebug_Print("", de_print, 1);
#endif

	// analysis
	if(l_byte_order == CAR_MOTOROLA){	// motorola
		if(Motorola_Mode_Analysis(p_data, \
				l_signal_handle->start_bit, \
				l_signal_handle->bit_count, \
				&ret_u32)){
			ret_status = true;
		}
	}else{								// Intel
		if(Intel_Mode_Analysis(p_data, \
				l_signal_handle->start_bit, \
				l_signal_handle->bit_count, \
				&ret_u32)){
			ret_status = true;
		}
	}

	// clac
	if(ret_status){
//		USART1_Bebug_Print_Num(">> ", ret_u32, 3, 1);

		// 增加无效值的过滤
		if((l_signal_handle->invalid >> 31) == 1){
			if(ret_u32 == (l_signal_handle->invalid & 0x7FFFFFFF)) return l_ret_flt_value;
		}

		l_ret_flt_value = (float)(ret_u32 * l_signal_handle->factor + l_signal_handle->offset);

		if(l_ret_flt_value < l_signal_handle->min) l_ret_flt_value = l_signal_handle->min;
		if(l_ret_flt_value > l_signal_handle->max) l_ret_flt_value = l_signal_handle->max;
//		USART1_Bebug_Print_Flt("l_ret_flt_value:", l_ret_flt_value, 1, 1);
#if 0		// debug print
		uint8_t dbg_print[50] = {0};
		sprintf(dbg_print, "ret_u32:0x%08X, factor:%0.8f, unit:%d, ret_flt:%0.2f\r\n", ret_u32, l_signal_handle->factor, ((l_signal_handle->config_info>>2)&0x07), l_ret_flt_value);
		USART1_Bebug_Print("DEBUG", dbg_print, 1);
#endif
	}

	return l_ret_flt_value;
}
