/*
 *  _4g_para_config.h
 *
 *  Created on: 2022-2-24
 *      Author: Administrator
 */

#ifndef _4G_PARA_CONFIG_H_
#define _4G_PARA_CONFIG_H_
#include "_4g_server.h"
#include "_4g_upgrade.h"
#include "_4g_data_upload.h"
#include "car_dvr.h"
/* ------------------------宏 枚举------------------------------- */
#define DEBUG_VEHICLE_SRR_CFG_ASCII		1		// 在A3和A4中是否使能车身配置和角雷达配置信息

#define	BUFFER_NBR	8
#define BUFFER_LEN	600
typedef struct _uart_rcv_cache
{
	uint8_t handle_buf[BUFFER_LEN];
	uint32_t handle_buf_len;
}UART_RCV_CACHE;
typedef struct _uart_rcv_opt
{
	UART_RCV_CACHE st_uart_cache[BUFFER_NBR];
	uint16_t handle_index_after;
	uint16_t handle_index_before;
}UART_RCV_OPT;

/* ------------------------全局变量------------------------------- */
extern UART_RCV_OPT g_st_uart0_rcv_obj;

/* ------------------------全局函数声明------------------------------- */
// 获取4G的信号强度
bool	 _4G_Message_Handle_Function(uint8_t *string,uint32_t length);
uint8_t Debug_Triggle_CMD();

//gcz 2022-06-02串口0接收初始化
void 	InitUart0RcvCache();
void 	CAR_DVR_Message_Handle();
bool 	Request_CARDVR_Ping_11_ASCII();
void 	Send_Data_To_CARDVR_BY_USART2(uint8_t *data, uint16_t dataLen);
bool 	Request_CARDVR_Take_Picture_D1_ASCII();
bool 	Request_Set_AEB_Config_Param_ASCII(KunLun_AEB_Para_Cfg algP);
bool 	Request_Set_Vehicle_Type_ASCII(uint8_t vehicle_type);
bool 	Send_Reply_Set_AEB_Config_Param_ASCII(uint32_t seqNum, KunLun_AEB_Para_Cfg algP);
#endif /* _4G_PARA_CONFIG_H_ */
