/*
 * aeb_sensor_management.h
 *
 *  Created on: 2022-7-26
 *      Author: Administrator
 */

#ifndef AEB_SENSOR_MANAGEMENT_H_
#define AEB_SENSOR_MANAGEMENT_H_
#include "system_init.h"
#include "stdbool.h"
/*
 * 功能实现事先说明：
 * 1、这一版仅加载3个传感器的属性（开关，厂家，安装位置）展开配置，配置参数需要单独自行配置；
 * 2、
 */
/************************* 宏定义/结构体 **********************************/
// 功能开关
enum
{
	FUNC_CLOSE,		// 功能关闭(默认)
	FUNC_OPEN,			// 功能开启
	SENSOR_CLOSE = 0,	// 传感器关
	SENSOR_OPEN,		// 传感器开
};

// 安装位置
enum
{
	SITE_NO_CARE,		// 位置：不关心
	SITE_FRONT,			// 位置：前 (默认)
	SITE_END,			// 位置：后
	SITE_LEFT,			// 位置：左
	SITE_RIGHT,			// 位置：右
};


// 4G模块厂家
enum
{
	QUECTEL=1,	// 移远模块(默认)
};
// 双面相机
enum
{
	ZKHY=1,		// 中科慧眼(默认)
};
// 超声波雷达
enum
{
	UTRAL=1,
};

// 毫米波雷达
enum
{
	MMW_S,
};

// IMU惯性测量仪
enum
{
	IMU_T,
};

// 行车记录仪
enum
{
	SHUNHE=1,		// 顺禾
	YIKA,		// 伊卡
	MAIGU,		// 麦谷
	JIMI,		// 几米
};

/* 传感器配置（公用）*/
typedef struct _Sensor_Com_CFG
{
	volatile uint8_t m_isOpen;			// 是否开启
	uint8_t m_producer;					// 厂家
//	uint8_t	m_sensor_num;				// 数目
//	uint8_t	m_scene;					// 场景
}Sensor_Com_CFG;

/* 传感器位置  */
typedef struct _Sensor_Utral_Site_Info
{
	volatile uint8_t m_main_sw;			// 单排控制开关	m_main_sw

	uint8_t m_controler;				// 控制器编号，从1开始取值
	uint8_t m_producer;					// 厂家
	uint8_t m_sensor_num;				// 传感器数据
	// 车头行进方向为正， 车前方从左到右编号依次1~6
	// 车头行进方向为正，车后方从左到右编号依次1~6
	// 车头行进方向为正，车左方从车头到车尾编号依次1~6
	// 车头行进方向为正，车右方从车头到车尾编号依次1~6

	// 每个雷达开关，其中暂定3号和4号就是车牌左右位置；
	uint8_t m_rdr_sw;	// 第5bit表示6号；第4bit表示5号；第3bit表示4号；第2bit表示3号；第1bit表示2号；第0bit表示1号；
}Sensor_Utral_Site_Info;

/* 传感器位置  */
typedef struct _Sensor_MMW_Site_Info
{
	volatile uint8_t m_main_sw;			// 单排控制开关

	uint8_t m_sensor_num;				// 传感器数据
	uint8_t m_producer;					// 厂家
	// 前后默认1个雷达就是1号雷达，左右两侧默认2个
	// 车头行进方向为正，车左方从车头到车尾编号依次1~2
	// 车头行进方向为正，车右方从车头到车尾编号依次1~2
	uint8_t m_rdr_sw;
}Sensor_MMW_Site_Info;

/* 超声波雷达配置 */
typedef struct _Sensor_UtraRdr_CFG
{
	Sensor_Utral_Site_Info m_front;			// 默认8个（2+4+2）
	Sensor_Utral_Site_Info m_end;			// 默认8个（2+4+2）
	Sensor_Utral_Site_Info m_left;			// 默认4个
	Sensor_Utral_Site_Info m_right;			// 默认4个
}Sensor_UtraRdr_CFG;

/* 毫米波雷达配置  */
typedef struct _Sensor_MMWRdr_CFG
{
//	volatile uint8_t m_isOpen;				// 总控制开关

	Sensor_MMW_Site_Info m_front;			// 默认1个
	Sensor_MMW_Site_Info m_right;			// 默认1个
	Sensor_MMW_Site_Info m_end;				// 默认0个
	Sensor_MMW_Site_Info m_left;			// 默认0个
}Sensor_MMWRdr_CFG;

/* 传感器管理（最终结构体）  */
typedef struct _Sensor_MGT
{
	Sensor_Com_CFG 		m_4g;			// 4G功能
	Sensor_Com_CFG		m_gps;			// GPS功能
	Sensor_Com_CFG 		m_dbl_cam;		// 双面相机
	Sensor_Com_CFG 		m_sgl_cam;		// 单目相机
	Sensor_UtraRdr_CFG 	m_ultra_radar;	// 超声波雷达
	Sensor_MMWRdr_CFG 	m_mmw_radar;	// 毫米波雷达
	Sensor_Com_CFG 		m_imu;			// IMU惯性测量仪
	Sensor_Com_CFG 		m_car_dvr;		// 行车记录仪
	Sensor_Com_CFG		m_actuator;		// 执行机构（比例阀，EBS电子制动系统）

	uint8_t				m_project;		// 描述项目类型
	uint8_t 			m_product;		// 描述产品类型
	bool                veh_srr_demo_sw; //演示行人横穿demo 开关  lyj 2022-9-24
	// 预留
//	Sensor_Com_CFG m_spd_ecder;			// 车速编码器
//	Sensor_Com_CFG m_infra_cam;			// 红外相机
//	Sensor_Com_CFG m_blind_detect_cam;	// 盲区检测相机
}Sensor_MGT;

/************************* 全局变量 **********************************/
extern Sensor_MGT g_sensor_mgt;
/************************* 全局函数 **********************************/
extern void 		Sensor_Management_Init();
extern uint8_t 		Set_Sensor_MGT_Param(Sensor_MGT sensor_mgt);
extern uint8_t 		Read_Sensor_Mgt_Cfg_Param(Sensor_MGT *sensor_mgt);
extern void 		Print_Sensor_Cfg_Param();
extern uint8_t 		Analysis_Request_Set_Sensor_Cfg_Param_ASCII(uint8_t *data);
extern uint8_t	 	Reply_Platform_Request_Sensor_Cfg_Param_Info_ASCII(uint16_t seqNum);
extern uint8_t 		Request_Sensor_Cfg_Param_Info_ASCII();
/*
* 获取传感器管理接口（对外）
*/
static inline Sensor_MGT Get_Sensor_MGT_Param()
{
	return  g_sensor_mgt;
}
#endif /* AEB_SENSOR_MANAGEMENT_H_ */
