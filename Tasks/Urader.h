/*
 * Urader.h
 *
 *  Created on: 2021-9-1
 *      Author: wangzhenbao
 */

#ifndef URADER_H_
#define URADER_H_
#include "system_init.h"

void Urader_RxValve_Data_Analysis(uint32_t ulSysTime);

#endif /* URADER_H_ */
