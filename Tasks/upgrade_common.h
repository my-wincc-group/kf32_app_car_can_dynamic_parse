/*
 * upgrade_common.h
 *
 *  Created on: 2021-12-6
 *      Author: Administrator
 * 说明:2种升级模式
 * upgrade_info.line_isUpgrade	控制模式
 * -------------------------------------------------------------
 * UPGRADE_OTA					上电OTA升级
 * UPGRADE_LINE					串口升级
 * UPGRADE_NO					不升级
 * -------------------------------------------------------------
 */

#ifndef UPGRADE_COMMON_H_
#define UPGRADE_COMMON_H_
#include "system_init.h"
#include "utc_time.h"
#include "stdbool.h"
#include "play_sound.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
/*******************************版本设定**********************************************/
#define MAIN_V				1
#define SUB_V					3
#define REV_V					11
#define PRODUCT_N			1		// 产品名称 S水瓶座；L猎户座
#define UT_N					2		// 超声波雷达数量
#define MMW_N					1		// 毫米波雷达数量
/*******************************全局开关**********************************************/
#define DEBUG_PRINT_SW			1		// 调试信息打印开关

/*******************************宏定义**********************************************/
#define OTA_SIZE					4500			// 4096 + 200
/************************* SERVER INFO ******************************/
#define CONNECT_ID					0				// id
#define CONNECT_PROTO_TYPE			"\"TCP\""
#define	SERVER_DEBUG 				0
#if	SERVER_DEBUG
//	#define CONNECT_IP				"\"180.76.243.232\""	// "\"43.249.193.233\""
//	#define CONNECT_PORT			9818
	#define CONNECT_IP				"\"39.107.254.216\""
	#define CONNECT_PORT			8876
#else
	#define CONNECT_IP				"\"39.107.254.216\""
	#define CONNECT_PORT			8876

//	#define CONNECT_IP				"\"39.102.42.22\""
//	//#define CONNECT_IP			"\"www.gxsq.com\""
//	#define CONNECT_PORT			9818
#endif

#define SH_INFO_SIZE				20	// 软硬件信息长度

/* ------------------------枚举------------------------------- */
typedef enum _UPLOAD_DATA_FRAME_ASSCII{
	SECRET_FRAME		= 0xB1,		// 注册获取秘钥
	REGISTR_FRAME		= 0xB2,		// 登录获取行程ID
	GPS_FRAME			= 0xB3,		// GPS
	WARN_FRAME			= 0xB4,		// 预警信息
	VBODY_FRAME			= 0xB5,		// 车身信息
	VBASE_FRAMR			= 0xB6,		// 车辆基本信息
	OTA_REQUEST_FRAME	= 0xB7,		// OTA请求
	OTA_REQ_DMN_BIN_FRAME = 0xBA,	// 要求开始下发bin包
	OTA_RECV_BIN_FRAME	= 0xBB,		// 下发bin操作
	OTA_OVERTIME_FRAME	= 0xBC,		// 接收平台超时指令，询问设备状态
	OTA_REPLY_FRAME		= 0xB8,		// OTA结果回复
	GPS_INTERVAL_FRAME	= 0xB9,		// GPS时间间隔
	SET_VTYPE_FRAME		= 0xA1,		// 设置车辆类型
	GET_VTYPE_FRAME		= 0xA2,		// 获取车辆类型
	SET_VPARAM_FRAME	= 0xA3,		// 设置车辆参数
	GET_VPARAM_FRAME	= 0xA4,		// 获取车辆参数
	HEARTBEAT_FRAME		= 0xC1,		// 心跳
	UPLOAD_DSHINFO_FRAME= 0xC2,		// 上报设备软硬件信息
	GET_DSHINFO_FRAME	= 0xC3,		// 平台获取设备软硬件信息
	GET_SENSOR_CFG_FRAME= 0xC4,		// 获取传感器配置参数
	SET_SENSOR_CFG_FRAME= 0xC5,		// 设置传感器配置参数

	/**** 行车记录仪 ******/
	CAR_DVR_TAKE_PICTURE	= 0xD1,	// 行车记录仪拍照-包括云端远程触发拍摄
	CAR_DVR_RECORD_VIDEO	= 0xE1,	// 行车记录仪录制视频
	CAR_DVR_PING_MCU		= 0x11,	// 行车记录仪与MCU交互
	CAR_DVR_SERVER_SET_PARAM       = 0xC6, // 平台设置行车记录仪参数(设备发送设置命令也使用此邋ID)
	CAR_DVR_SERVER_QUERY_PARAM    = 0xC7, //平台查询行车记录仪参数

	/***** 车CAN协议解析 ****/
	SET_CAR_CAN_ANALYSIS_PARAM    = 0xC8, // 设置/上报车CAN协议解析配置
	GET_CAR_CAN_ANALYSIS_PARAM    = 0xC9, // 平台获取车CAN协议解析配置

}UPLOAD_DATA_FRAME_ASSCII;

typedef enum{
	RECV_CMD=0,
	RECV_BIN
}RECV_MODE;

typedef enum{
	UPGRADE_NO,		// 不升级
	UPGRADE_LINE,	// 串口升级
	UPGRADE_OTA,	// 4G OTA升级
}Upgrade_Mode;

// 接收4G 消息状态
typedef enum _4G_Message_E{
	_4G_WAIT,
	_4G_RDY,			// 接收到4G模块使能标志
	_4G_OK,
	_4G_CME_ERROR,
	_4G_CMS_ERROR,
	_4G_FIND_BAUD,
	_4G_NTP,		//
	_4G_READY,		// SIM卡状态，+CPIN: READY无密码；
	_4G_GSM,		// +CGREG: 0,1
	_4G_GSM_NO_RIGISTER,	// 未注册
	_4G_CREG_1,		// CS 域 域网络注册状态, 已注册，归属地网络; +CREG: 1
	_4G_INVALIED_REQ,	// 非法请求，平台反馈
	_4G_CON_SERVER_OK,	// +QIOPEN:
	_4G_SECRET_OK,		// 获取秘钥成功
	_4G_ITINERART_ID_OK,// 获取行程I成功D
	_4G_PDP_CLOSE,	// 收到Close后，需要调用AT+QICLOSE=0
	_4G_SEND_FAIL,	// 连接已建立，但缓冲区满了
	_4G_SEND_OK,	// 连接已建立，且发送成功
	_4G_RECV_ARROWS,// 接收>
	_4G_CONNECT,	// HTTP(S)服务器连接成功,之后发送http地址信息
	_4G_HTTPGET,	// 发送 HTTPS GET 请求，后接收，内容中含下载包的大小
	_4G_ERROR,		// 需要重连服务器,连接为建立，异常断开或参数不正确
	_4G_ERROR_10,	// 未插入 SIM 卡
	_4G_ERROR_50,	// 指令解析失败
	_4G_ERROR_58,	//
	_4G_ERROR_501,	// 无效参数，未用
	_4G_ERROR_504,	// 会话仍在进行
	_4G_ERROR_505,	// 会话未激活
	_4G_ERROR_516,	// 当前未定位
	_4G_ERROR_561,	// 打开PDP场景失败
	_4G_ERROR_563,	// Socket标识被占用
	_4G_ERROR_566,	// Socket连接失败
	_4G_ERROR_567,	// socket被关闭
	_4G_ERROR_714,	// HTTP(S) DNS 错误
	_4G_GPS_INFO,	// +QGPSLOC:
	_4G_CSQ_INFO,	// 4G信号强度
//	_4G_RESULT,		// 收到 result
	_4G_UPGRADE_OK,	// 升级成功
}_4G_Message_E;

/* 4G通讯状态 */
typedef enum{
	WAIT,
	RDY,			// 4G 模块被使能后，收到起始字符RDY
	NTP,			// NTP时间
	GET_SERVER_INFO_1,// 获取链接服务器信息，20220913 lmz add
	GET_SERVER_INFO_2,
	GET_SERVER_INFO_3,
	GET_SERVER_INFO_4,
	ATCMD,			// AT指令
	ATI,			// ATI指令,获取EC200U-CN SN码
	SIM,			// SIM卡状态
	GSM,			// 注册GSM网络
	ATE0,			// 关闭回显模式，关闭自发自收功能
	CONNECT_SERVER,	// 连接服务器
	CLOSE_SERVER,	// 关闭连接
	SECRET,			// 获取秘钥请求 和 获取秘钥发送数据
	REGISTER,		// 注册
	REQUEST,		// 请求
	UPGRADE_URL,	// 设置URL地址信息
	UPGRADE_SUB_DMN,// 分包下载
	RESPONSE,		// 升级后回复
	SET_BAUD,		// OTA升级的波特率为19200
	EC200U_RESTART,
}Quectel4GStatus;
//typedef enum{
//	BIN_RECV_WAIT	= 0,
//	BIN_RECV_READY	= 1,
//	BIN_RECV_GO		= 2,
//	BIN_RECV_END	= 3,
//	BIN_STORE_CHECK	= 4
//}BIN_STATUS;
// 【1：FCW】、【2：HMW】、【3：LDW】、【4：AEB】
typedef enum {
	NO_EVENT,
	FCW_EVENT,
	HMW_EVENT,
	LDW_EVENT,
	AEB_EVENT,
	DRIVER_BRAKE_EVENT,
	BSD_EVENT
}TRIGGER_EVENT;

// ASCII 平台状态码
typedef enum _PLATFORM_ERR_ID{
	P_ERR_NO,
	P_ERR_4001 = 4001,	// 用户名或密码错误
	P_ERR_5001 = 5001,	// 平台找不到此设备（设备没有绑定）
	P_ERR_5002,			// 注册设备时，提供的时间戳超时
	P_ERR_5003,			// 不满足OTA升级条件
	P_ERR_5004,			// crc16校验失败
	P_ERR_5005,			// 设备登录sign校验失败
	P_ERR_5006,			// SN长度长度范围
	P_ERR_5007,			// 设备尚未登录
	P_ERR_5008,			// 附件大小超出限制
	P_ERR_5009,			// 注册时提供的usrid或password错误
	P_ERR_5010,			// 注册时制定的userid没有获取次设备secret的权限
	P_ERR_5011,			// 获取设备aeb配置参数失败（没有此设备的配置信息）
	P_ERR_5012,			// 获取传感器配置参数失败
	P_ERR_5013,			// 连接地址错误，重新获取路由连接
	P_ERR_6017 = 6017,	// 签名认证失败
}PLATFORM_ERR_ID_E;

/****************************** 结构体 *********************************************/
/* 版本号 */
typedef struct _version{
	uint8_t main_v;			// 主
	uint8_t sub_v;			// 次
	uint8_t rev_v;			// 修订
	uint8_t sensor_v[10];	// 传感器版本号
}AppVersion;

// OTA接收数据包
typedef struct _OTA_RX_PKG{
	volatile uint16_t 	cnt;				// 计数
	volatile uint8_t 	data[OTA_SIZE];		// OTA升级BUFFER1
	volatile uint32_t 	addr;				// 写地址
	volatile uint16_t	write_len;
	// 分包下载
	volatile uint8_t 	pkg_id;				// 分包ID
	volatile uint8_t 	pkg_total_times;	// 总包数
	volatile uint32_t 	src_chk;
	volatile uint32_t 	clc_chk;
	volatile uint16_t 	remain;				// 剩余数量
	volatile uint8_t 	times;				// 整包数量
	volatile uint16_t 	recv_size;
	volatile uint8_t 	overtime_flag;		// 接收升级时超时
	volatile uint8_t	recv_bin_times;		// 接收升级包的次数
}OTA_RX_PKG;

// OTA升级包信息
typedef struct _Upgrade_INFO{
	uint8_t 			https[100];					// 下载平台地址
	volatile uint32_t 	size;						// 接收升级包的大小
	volatile uint8_t 	bin_type;					// 0应用程序；1bootloader程序
	uint8_t				run_v[VERSION_SIZE];		// APP运行版本号
	uint8_t 			plat_upgrade_v[VERSION_SIZE];// 平台升级版本,不存储
	uint8_t 			boot_v[VERSION_SIZE];
	uint8_t 			screen_v[VERSION_SIZE];
	volatile uint8_t  	isMultiple;					// 是4096的倍数；true是；false否
	volatile uint32_t 	clc_chk;					// 校验值

	volatile uint8_t 	step;						// OTA升级步骤
	volatile uint8_t 	step_step;
	volatile uint8_t 	inPlatform_status;			// 0：未升级; 1:需要升级; 2:已升级;3:非工作值
	volatile uint8_t 	need_upgrade;				// 0不需要升级；1需要升级
	volatile uint8_t 	isUpgradeOK;				// true成功,false失败；判断升级是否成功

	volatile uint8_t	rx_mode;					// 接收模式
	volatile uint8_t 	upgrade_mode;				// 升级模式

	volatile uint8_t 	play_sound;					// 播放声音；1播放；0不播放
	volatile uint8_t 	ota_status;					// ota升级状态，1升级成功；0升级失败
	volatile uint8_t 	ota_reconnect_server;		// 1升级成功后重连服务器；0不触发重连
	volatile uint8_t 	ota_type;					// OTA下载包的类型

	volatile uint8_t 	screen_isUpgrade;			// 小屏幕升级标志；1需要升级；0不需要升级或升级完成
	volatile uint8_t 	app_isUpgrade;				// 【0：不回复平台】【1：回复平台OTA升级成功】【2：回复平台OTA升级失败(bootloader move failed)】
}Upgrade_Info;


typedef struct _Server_Info{	// 20220314
	volatile uint8_t 	isConnect;			// ture连接，false未连接
	volatile uint8_t 	step;				// 连接步骤
	volatile uint8_t 	step_step;			// 内部接收指令，划分阶段变量[0-3]
	volatile uint8_t  	isSecret;			// true获取,false没获取
	volatile uint8_t 	snIsOk;				// SN是否有效；true有效，false无效

	uint8_t 			secret[SECRET_SIZE];// 秘钥
	uint8_t 			sn[DEV_SN_SIZE];	// SN
	uint8_t 			pn[DEV_PN_SIZE];	// PN
	uint8_t 			itinerary_id[20];	// 行程ID
	volatile uint8_t 	vehicle_type;		// 车辆类型
	volatile uint8_t 	offline_times;		// 离线次数，6次，对应1min
	volatile uint8_t 	reconnect_server_position;
	volatile uint8_t 	err_loop_times;
	volatile uint8_t 	loop_con_server_times;	// 连续连接服务器的次数 20220913 lmz add
	uint8_t 			http_cmd[100];		// http指令缓冲区
	uint16_t 			con_server_reason;	// 1:上电; 2:收到5013时; 3:一直连不上服务时，【其余待定】
}Server_Info;


/* ------------------------结构体------------------------------- */

typedef enum{
	NO_STATE,
	ENABLE_GNSS,
	CONFIG_AGPS,
	ENABLE_AGPS,
	DISABLE_GNSS,
//	ENABLE_QGPSNMEA,
//	ENABLE_QGPSNMEATYPE,
//	DISABLE_QGPSNMEA,
//	GET_QGPSNMEA,
	GET_QGPSLOC,
//	MANUAL_CMD,			// 手动输入指令
	AT_QIDEACT,
}GPS_Stage;
/*
 * GPGGA:head,经度，纬度，UCT时间，GPS状态，卫星数，hdop，海拔
 * GPRMC：head,经度，纬度，UCT时间，地面速率，模式
 * GPGSA:head,模式，定位类型
 * GPGSV:没有解析
 * GPVTG:head，地面速率，模式
 * GPSLOC：head,时分秒，经度，纬度，hdop，海拔，定位类型，对地速度，日月年，卫星数
 */
typedef struct _GPS_INFO{
	volatile uint8_t 	isConnect;		// GPS是否连接
	volatile uint8_t	step;			// GPS流程步骤
	uint8_t 			lat[12];		// 纬度
	uint8_t 			lon[12];		// 经度
	uint8_t 			ns[2];			// 南北
	uint8_t				ew[2];			// 东西
	uint8_t 			hms[10];		// UTC时间，时分秒格式
	uint8_t 			dmy[8];			// UTC日期。格式：ddmmyy（引自GPRMC语句） 日月年
	uint8_t 			status;			// GPS状态，0=未定位，1=单点定位，2=差分定位，3=无效PPS，4=固定解，5=浮点解，6=正在估算
	volatile uint8_t	sateNum;		// 卫星数
	volatile uint8_t	fs;				// 定位类型，1=没有定位，2=2D 定位，3=3D 定位
	float				hdop;			// 水平精度因子hdop
	float 				altitude;		// 海拔
	float 				cogt;			// 以正北方为参考基准的地面航向，格式度分（ddd.mm），范围（0~359.59）
	float				sog;			// 地面速率，单位（节）;1节（kn）=1海里/小时=（1852/3600）m/s 是速度单位
	float 				kph;			// 地面速率，单位（KM/H），与sog性质一样，不解析
}GPS_INFO;

// 字符串截取
typedef struct _strtokStr{
	uint8_t data[4][20];
	uint8_t cnt;
}StrtokStr;
/* ------------------------全局变量------------------------------- */
extern volatile uint32_t 	g_utctime;			// UTCTIME时间
extern Server_Info 			server_info;
extern Upgrade_Info 		upgrade_info;
extern OTA_RX_PKG 			down_pkg;
extern GPS_INFO				gps_info;
extern volatile uint8_t 	_4g_message_state;
extern volatile uint8_t 	_4g_gps_state;
extern volatile uint8_t		debug_get_4g_info;
/* ------------------------全局函数------------------------------- */

StrtokStr 	StrtokString(uint8_t *string);				// 字符串解析，按照,
TimeType 	Get_CurrentTime_Stamp();					// 获取当前时间戳
void 		delay_ms(uint32_t nms);
void 		Close_Interrupt_Function();
void 		Reopen_Interrupt_Function();
#endif /* UPGRADE_COMMON_H_ */
