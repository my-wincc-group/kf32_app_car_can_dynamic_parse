/*
 * usart_upgrade.c
 *
 *  Created on: 2021-11-17
 *      Author: Administrator
 */
#include "usart_upgrade.h"
#include "usart.h"
#include "stdio.h"
#include "stdlib.h"
#include "common.h"
#include "flash.h"
#include "string.h"
#include "EC200U.h"
#include "at_parse_alg_para.h"
#include "aebs_version_management.h"
/* ------------------------枚举/结构体------------------------------- */
enum
{
	LINE_APP_FMW,		// 应用程序固件
	LINE_BOOT_FMW,		// bootloader程序固件
};

/* ------------------------全局变量------------------------------- */
volatile uint8_t 	debug_get_4g_info = 0;
extern SW_HW_Version_Info sw_hw_version;
//gcz   2022-05-27
uint8_t g_u8UartFlag = 0;
volatile uint8_t ToolingFlag = 0;  //yuhong 工装测试模式设定变量
/* -----------------------全局函数声明------------------------------- */
extern uint8_t Upgrade_Bootloader_Program();
void Line_Upgrade_Timeout_Dispose();
bool Line_Write_Bin_Data_Dispose(uint32_t write_len);
//-------------gcz  2022-05-27------------------
//检测 包是否符合“<>↙”格式
bool check_is_valid_uart_pkg(uint8_t *pkg,uint16_t length)
{
	if (length >= 3)
	{
		char * str1 = strstr(pkg,"<");
		char * str2 = strstr(pkg,">");
//		fprintf(USART1_STREAM,"[%s]  [%s]\r\n",str1,str2);
		if ((str1 != NULL) && (str2 != NULL) && ((str2 > str1) && (str2 - str1) <= length))
			return TRUE;
	}
	return FALSE;
}
//----------------------------------------------
void Line_Upgrade_Init()
{
	upgrade_info.rx_mode 		= RECV_CMD;
	upgrade_info.upgrade_mode 	= UPGRADE_NO;
	upgrade_info.isUpgradeOK	= 0;
}
/*
 * 用户升级开始
 */
void Line_Upgrade_Start()
{
	upgrade_info.rx_mode 		= RECV_BIN;
	upgrade_info.upgrade_mode 	= UPGRADE_LINE;
	upgrade_info.isUpgradeOK	= false;
	down_pkg.clc_chk 			= 0;
	down_pkg.src_chk 			= 0;
	down_pkg.cnt  				= 0;
	down_pkg.recv_bin_times 	= 0;
	upgrade_info.clc_chk		= 0;

	switch(upgrade_info.bin_type){
	case LINE_APP_FMW:{		// 应用程序
		// 先清空200KB 外部FLASH空间 application
//		for(uint8_t i=0; i<APP_SECTION_SIZE; i++){		// del lmz 20221024 采取了动态删除扇区
//			W25QXX_Erase_Sector(i);
//		}
		down_pkg.addr = OUT_FLASH_APP_START;
	}break;
	case LINE_BOOT_FMW:{		// bootloader程序
//		// 先清空64KB 外部FLASH空间 bootloader
//		for(uint8_t i=0; i<BOOT_SECTION_SIZE; i++){		// del lmz 20221024 采取了动态删除扇区
//			W25QXX_Erase_Sector(OUT_FLASH_BOOT_START / EX_FLASH_SECTOR_SIZE + i);
//		}
		down_pkg.addr = OUT_FLASH_BOOT_START;
	}break;
	default:return ;
	}
	memset((char *)down_pkg.data, 0, LINE_BUFFER_SIZE);		// 清空buffer

	// 数据下载中
	Send_Display_Message_VoiveNum(DATA_DOWNLOADING_S);

	Close_Interrupt_Function();
}
// 读取外部FLASH的bootloader程序数据校验值
// 成功返回1，失败返回0
//uint8_t Read_ExFlash_Bootloader_Data_Chk()
//{
//	uint32_t read_addr = OUT_FLASH_BOOT_START;
//	uint32_t clc_chk = 0;
//	// 获取外部FLASH的数据（单次读4KB），遍历求校验值
//	for(uint8_t i=0; i<down_pkg.times; i++){
//		memset((char *)down_pkg.data, 0, OTA_SIZE);
//		W25QXX_Read_No_Chk((char *)down_pkg.data, read_addr, EX_FLASH_SECTOR_SIZE);
////		W25QXX_Read((char *)down_pkg.data, read_addr, EX_FLASH_SECTOR_SIZE);
//		read_addr += EX_FLASH_SECTOR_SIZE;
//
//		USART1_Bebug_Print_Num("[Read Addr]", read_addr, 3, 1);
//
//		for(uint16_t j=0; j<EX_FLASH_SECTOR_SIZE; j++){
//			clc_chk += down_pkg.data[j];
//		}
//	}
//
//	if(down_pkg.remain > 0){
//		W25QXX_Read_No_Chk((char *)down_pkg.data, read_addr, down_pkg.remain);
////		W25QXX_Read((char *)down_pkg.data, read_addr, down_pkg.remain);
//
//		for(uint16_t j=0; j<down_pkg.remain; j++){
//			clc_chk += down_pkg.data[j];
//		}
//	}
//
//	USART1_Bebug_Print_Num("[clc_chk]", clc_chk, 3, 1);
//	USART1_Bebug_Print_Num("[src_chk]", down_pkg.src_chk, 3, 1);
//	// 对不校验值，相同就返回1
//	if(clc_chk == down_pkg.src_chk){
//		USART1_Bebug_Print("Line Bootloader Upgrade", "Success", 1);
//		return 1;
//	}
//
//	USART1_Bebug_Print("Line Bootloader Upgrade", "Failed", 1);
//	return 0;
//}

/*
 * 用户升级结束
 */
void Line_Upgrade_End()
{
	upgrade_info.rx_mode 		= RECV_CMD;
	upgrade_info.upgrade_mode 	= UPGRADE_NO;
	upgrade_info.isUpgradeOK	= 1;
	down_pkg.cnt 				= 0;
	down_pkg.addr 				= 0;
	IWDT_Feed_The_Dog();	// add lmz 20221024
	memset((char *)down_pkg.data, 0, OTA_SIZE);

	if(upgrade_info.ota_status == 1){
		switch(upgrade_info.bin_type ){
		case LINE_APP_FMW:{		// 应用程序
#if 1
			// 升级的版本号
			if(Set_APP_UP_Version(upgrade_info.plat_upgrade_v)){
				Set_APP_UP_Version(upgrade_info.plat_upgrade_v);
			}

			uint8_t run_v[VERSION_SIZE] = {0};
			sprintf(run_v, "V0.9.4.L1C5H0");
			if(Set_App_Run_Version(run_v)){
				Set_App_Run_Version(run_v);
			}

			// 包大小
			if(!Set_APP_UP_PKG_size(upgrade_info.size)){
				Set_APP_UP_PKG_size(upgrade_info.size);
			}

			// 在内部FLASH中设置线刷升级标志位置
			if(!Set_Flahs_Way_InFlash(1)){
				Set_Flahs_Way_InFlash(1);
			}

			// 为了兼容
			if(!Set_APP_UP_Bin_Chk_Value_Compatible(upgrade_info.clc_chk)){
				Set_APP_UP_Bin_Chk_Value_Compatible(upgrade_info.clc_chk);
			}

			USART1_Bebug_Print("Application Line Upgrade", "Success", 1);
			// 数据下载成功
			Send_Display_Message_VoiveNum(DATA_DOWNLOAD_OK_S);	// 语音提示
#endif
		}break;
		case LINE_BOOT_FMW:{		// bootloaderc程序
			USART1_Bebug_Print("Bootloader Line Upgrade", "Success", 1);
			down_pkg.src_chk = upgrade_info.clc_chk;

//			Read_ExFlash_Bootloader_Data_Chk();
			Upgrade_Bootloader_Program();
		}break;
		}
	}else{
		switch(upgrade_info.bin_type ){
		case LINE_APP_FMW:{		// 应用程序
#if 1
			for(uint8_t j=0; j<=down_pkg.recv_bin_times; j++){
				W25QXX_Erase_Sector(j);
			}
			// 清空校验值和升级包大小
			if(!Set_APP_UP_PKG_size(0)){
				Set_APP_UP_PKG_size(0);
			}

			// 在内部FLASH中设置线刷升级标志位置
			if(!Set_Flahs_Way_InFlash(0)){
				Set_Flahs_Way_InFlash(0);
			}

			down_pkg.recv_bin_times = 0;
			USART1_Bebug_Print("Application Line Upgrade", "Failed", 1);
			Send_Display_Message_VoiveNum(DATA_DOWNLOAD_ERR_S);	// 语音提示
#endif
		}break;
		case LINE_BOOT_FMW:{		// bootloaderc程序
			for(uint8_t i=0; i<BOOT_SECTION_SIZE; i++){
				W25QXX_Erase_Sector(OUT_FLASH_BOOT_START / EX_FLASH_SECTOR_SIZE + i);
			}

			down_pkg.recv_bin_times = 0;
			USART1_Bebug_Print("Bootloader Line Upgrade", "Failed", 1);
			Send_Display_Message_VoiveNum(DATA_DOWNLOAD_ERR_S);	// 语音提示
		}break;
		}
	}
	IWDT_Feed_The_Dog();	// add lmz 20221024
	Reopen_Interrupt_Function();
}

/*
 * 用户通过usart1升级交互逻辑
 * 参数：无
 * 返回：无
 * 说明：传送方式：问答式存储
 */
int64_t lineUpgrade_time = 0;
uint8_t lineUpgrade_timeout_times = 0;
void Line_Upgrade_Write_Operation()
{
	uint16_t i = 0;
	if(down_pkg.recv_bin_times > down_pkg.times) return ;

	// 添加线刷超时逻辑，超时2秒就需重发
	if(SystemtimeClock - lineUpgrade_time > 20){					// add lmz 20221024
		lineUpgrade_time = SystemtimeClock;
		if(lineUpgrade_timeout_times >= 100){
			lineUpgrade_timeout_times = 0;
			Line_Upgrade_Timeout_Dispose();
			return;
		}else lineUpgrade_timeout_times++;
	}

	// 先接收整包，后接收尾包
	if(down_pkg.recv_bin_times < down_pkg.times){					// 接收整包
		if(down_pkg.cnt >= LINE_BUFFER_SIZE){
			for(i=0; i<EX_FLASH_SECTOR_SIZE; i++){					// 取校验
				down_pkg.clc_chk += down_pkg.data[i];
			}

			down_pkg.src_chk = down_pkg.data[i];i++;
			down_pkg.src_chk += down_pkg.data[i]<<8;i++;
			down_pkg.src_chk += down_pkg.data[i]<<16;i++;
			down_pkg.src_chk += down_pkg.data[i]<<24;i++;

			if(down_pkg.clc_chk == down_pkg.src_chk){				// 比对校验
				Line_Write_Bin_Data_Dispose(EX_FLASH_SECTOR_SIZE);	// update lmz 20221025

				lineUpgrade_timeout_times = 0;						// 清空超时标志

				// 若为最后一包，写
				if(down_pkg.recv_bin_times==down_pkg.times && down_pkg.remain==0){
					// 写校验码
					USART1_Bebug_Print_Num("upgrade_info.src_chk", upgrade_info.clc_chk, 3, 1);// 成功写入回复
					upgrade_info.ota_status = 1;
					Line_Upgrade_End();
					return ;
				}
			}else{
				upgrade_info.ota_status = 0;
				Line_Upgrade_Timeout_Dispose();						// update lmz 20221024
			}

			IWDT_Feed_The_Dog();									// 踢狗
		}
	}else{			// 接收尾包
		if(down_pkg.cnt >= (down_pkg.remain+5)){					// +5 = 4B校验+1B包分组编号
			for(i=0; i<down_pkg.remain; i++){						// 取校验
				down_pkg.clc_chk += down_pkg.data[i];
			}

			down_pkg.src_chk = down_pkg.data[i]; 		i++;
			down_pkg.src_chk += down_pkg.data[i]<<8; 	i++;
			down_pkg.src_chk += down_pkg.data[i]<<16; i++;
			down_pkg.src_chk += down_pkg.data[i]<<24; i++;

			if(down_pkg.clc_chk == down_pkg.src_chk){
				Line_Write_Bin_Data_Dispose(down_pkg.remain);		// update lmz 20221025

				lineUpgrade_timeout_times = 0;						// 清空超时标志
				upgrade_info.ota_status = 1;						// 线刷完成
				Line_Upgrade_End();
				return ;
			}else{
				upgrade_info.ota_status = 0;
				Line_Upgrade_Timeout_Dispose();						// update lmz 20221024
			}

			IWDT_Feed_The_Dog();									// 踢狗
		}
	}
}
/*
 * 线刷升级通过指令 逐条认证完成升级
 * 时间间隔50ms
 */
void Line_Load_Upgrade()
{
	Analysis_DMA_USART1_Data();

	char *strx_1 = NULL;
	// 手动串口升级，需先发送升级包的大小， <ZKHYSET*PACKAGESIZE:v1.0.0.1,26644>
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*PKGINFO"))){	// 19-->20
		// 取数据
		StrtokStr str = StrtokString(strx_1 + 16);
//		USART1_Bebug_Print_Num("[cnt]", str.cnt, 1, 1);
		if(str.cnt == 3){
			// 设置变量
			upgrade_info.size 	= atoi(str.data[1]);							// 升级包大小
			upgrade_info.bin_type = atoi(str.data[2]);							// 升级包的类型

			if(upgrade_info.size > EX_FLASH_SECTOR_SIZE){
				// 判断是否是4096的整数倍数
				if(upgrade_info.size % EX_FLASH_SECTOR_SIZE){
					upgrade_info.isMultiple = 0;
					down_pkg.times 			= upgrade_info.size / EX_FLASH_SECTOR_SIZE;	// 分几次接收
					down_pkg.remain 		= upgrade_info.size % EX_FLASH_SECTOR_SIZE;	// 最后一包大小
				}else{
					upgrade_info.isMultiple = 1;
					down_pkg.times 			= upgrade_info.size / EX_FLASH_SECTOR_SIZE;	// 分几次接收
					down_pkg.remain 		= 0;								// 最后一包大小
				}

				// 记录升级版本号
				strcpy(upgrade_info.plat_upgrade_v, str.data[0]);

				USART1_Bebug_Print_Num("[size]", upgrade_info.size, 1, 1);
				USART1_Bebug_Print_Num("[bin_type]", upgrade_info.bin_type, 1, 1);
				USART1_Bebug_Print_Num("[times]", down_pkg.times, 1, 1);
				USART1_Bebug_Print_Num("[remain]", down_pkg.remain, 1, 1);
				USART1_Bebug_Print("Up_V", upgrade_info.plat_upgrade_v, 1);

				Line_Upgrade_Start();
				USART1_Bebug_Print("", "11111111", 1);			// OK
			}else{
				upgrade_info.size = 0;
				USART1_Bebug_Print("", "22222222", 1);			// ERR
			}
		}else{
			USART1_Bebug_Print("", "22222222", 1);			// ERR
		}
		Clear_UART1_Buffer();
	}
}
/*
 * 解析用户指令
 * 参数：无
 * 返回：无
 * 说明：(1)升级包信息：版本号，包大小；(2)获取版本号； (3)获取SN号
 * 时间间隔：50ms
 */
char *strx_1 = NULL;
void Analysis_USART1_Config_CMD()
{
	// 设置设备的SN号 <ZKHYSET*DEVICESN:1226060003>	//EC200UCNAAR02A01M08
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*DEVICESN"))){
		// 取数据
		StrtokStr str = StrtokString(strx_1 + 17);
		if(str.cnt == 1){
			if(Set_Device_SN(str.data[0])){
				USART1_Bebug_Print(">>", "Set Success.", 1);
				USART1_Bebug_Print("WARNING", "If The DEVICE SN Is Configured For The First Time, Need To Restart.", 1);
			}else{
				USART1_Bebug_Print(">>", "Set Failed.", 1);
			}
		}else{
			USART1_Bebug_Print("ERROR", "Syntax Error. <ZKHYSET*DEVICESN:SN>.", 1);
		}
	}else
	// 获取设备的SN号 <ZKHYCHK*DEVICESN>
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYCHK*DEVICESN"))){
		char sn[DEV_SN_SIZE] = {0};

		if(Get_Device_SN(sn)){
			USART1_Bebug_Print("SN", sn, 1);
		}else{
			USART1_Bebug_Print(">>", "Get SN Failed.", 1);
		}
	}else
	// 设置运行版本号 <ZKHYSET*RUNVERSION:V1.0.0.S1C6>
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*RUNVERSION"))){
		StrtokStr str = StrtokString(strx_1 + 19);
		if(str.cnt == 1){ // 0017 小智 韵达
			if(Set_App_Run_Version(str.data[0])){
				memcpy(upgrade_info.run_v, str.data[0], strlen(str.data[0]));
				USART1_Bebug_Print(">>", "Set Success.", 1);
			}else{
				USART1_Bebug_Print(">>", "Set Run Version Failed.", 1);
			}
		}else{
			USART1_Bebug_Print("ERROR", "Syntax Error. <ZKHYSET*RUNVERSION:Version>.", 1);
		}
	}
	// 设置升级版本号 <ZKHYSET*UPVERSION:V1.0.0.S1C6>
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*UPVERSION"))){
		StrtokStr str = StrtokString(strx_1 + 18);
		if(str.cnt == 1){
			if(Set_APP_UP_Version(str.data[0])){
				memcpy(upgrade_info.plat_upgrade_v, str.data[0], strlen(str.data[0]));
				USART1_Bebug_Print(">>", "Set Success.", 1);
			}else{
				USART1_Bebug_Print("ERROR", "Set Upgrade Version Failed.", 1);
			}
		}else{
			USART1_Bebug_Print("ERROR", "Syntax Error. <ZKHYSET*UPVERSION:Version>.", 1);
		}
	}else
	// 获取运行版本信息	(<ZKHYCHK*VERSION>)
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYCHK*VERSION")){
		// 外部FLASH操作
		uint8_t up_v[VERSION_SIZE]	= {0};
		uint8_t run_v[VERSION_SIZE]	= {0};

		Get_APP_UP_Version(up_v);
		Get_APP_Run_Version(run_v);

		memcpy(upgrade_info.plat_upgrade_v, up_v, strlen(up_v));
		memcpy(upgrade_info.run_v, run_v, strlen(run_v));

		fprintf(USART1_STREAM, ">> RUN_V:%s;UP_V:%s\r\n", run_v, up_v);
//		USART1_Bebug_Print("RUN_V", run_v);
//		USART1_Bebug_Print("UP_V", up_v);
	}else
	// 获取设备的PN号 <ZKHYCHK*DEVICEPN>
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYCHK*DEVICEPN"))){
		char pn[DEV_PN_SIZE] = {0};

		if(Get_Device_PN(pn)){
			USART1_Bebug_Print("PN", pn, 1);
		}else{
			USART1_Bebug_Print("PN", "Get PN Failed.", 1);
		}
	}else
	// 设置设备的PN号 <ZKHYSET*DEVICEPN:0123456789A>
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*DEVICEPN"))){
		StrtokStr str = StrtokString(strx_1 + 17);

		if(str.cnt == 1){
			if(Set_Device_PN(str.data[0])){
				memcpy(sw_hw_version.AEBS_PN, str.data[0], strlen(str.data[0]));
				USART1_Bebug_Print(">>", "Set Success.", 1);
			}else{
				USART1_Bebug_Print("ERROR", "Set PN Failed.", 1);
			}
		}else{
			USART1_Bebug_Print("ERROR", "Syntax Error. <ZKHYSET*DEVICEPN:PN>.", 1);
		}
	}else
	// 获取设备秘钥：<ZKHYCHK*SECRET>
	if((strx_1 = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYCHK*SECRET"))){
		char secret[SECRET_SIZE] = {0};

		if(Get_Secret(secret)){
			USART1_Bebug_Print("Secret", secret, 1);
		}else{
			USART1_Bebug_Print("ERROR", "Get Secret Failed.", 1);
		}
	}else
	// 设置设备秘钥：<ZKHYSET*SECRET:秘钥>
	if((strx_1 = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYSET*SECRET"))){
		StrtokStr str = StrtokString(strx_1 + 15);

		if(str.cnt == 1){
			if(Set_Secret(str.data[0])){
				USART1_Bebug_Print(">>", "Set Success.", 1);
			}else{
				USART1_Bebug_Print("ERROR", "Set Device Secret Failed.", 1);
			}
		}else{
			USART1_Bebug_Print("ERROR", "Syntax Error. <ZKHYSET*SECRET:secret>.", 1);
		}
	}else
	// <ZKHYSET*ERASESECTORS:1>
	if((strx_1 = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYSET*ERASESECTORS"))){
		StrtokStr str = StrtokString(strx_1 + 21);

		if(str.cnt == 1){
			if(atoi(str.data[0]) > 101 || atoi(str.data[0]) < 1){
				USART1_Bebug_Print("ERROR", "Out Of Range. Valid Range:[1-101].", 1);
				return ;
			}

			uint8_t vehicle_type = atoi(str.data[0]);
			W25QXX_Erase_Sector(OUT_FLASH_ALGPPARM_START / EX_FLASH_SECTOR_SIZE + vehicle_type - 1);

			USART1_Bebug_Print_Num("[ERASE] Sectors ID OK", vehicle_type, 1, 1);
		}else{
			USART1_Bebug_Print("ERROR", "Syntax Error. .<ZKHYSET*ERASESECTORS:number>", 1);
		}
	}else
	// <ZKHYSET*ERASEPKG>
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*ERASEPKG")){
		// 先清空200KB 外部FLASH空间 升级包bin文件
		for(uint32_t i=0; i<APP_SECTION_SIZE; i++){
			W25QXX_Erase_Sector(i);
		}
		USART1_Bebug_Print("ERASE", "Erase Package OK.", 1);
	}else
	// <ZKHYSET*ERASEPKG>，擦除外部FLASH数据
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYCLEAR*EX_FLASH")){
		// 先清空200KB 外部FLASH空间 升级包bin文件
		for(uint16_t i=0; i<DATA_LEN_512B; i++){
			W25QXX_Erase_Sector(i);
			// 踢狗
			if(i % 50 == 0){
				USART1_Bebug_Print_Num("[ERASE] Sectors ID", i, 1, 1);
				IWDT_Feed_The_Dog();
			}
		}

		USART1_Bebug_Print("ERASE", "Erases All Data From The External FLASH.", 1);
	}else
	// 设置硬件信息 <ZKHYSET*HAEDWARE:>
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*HARDWARE:"))){
		// 取数据
		StrtokStr str = StrtokString(strx_1 + 17);

		if(str.cnt == 1){
			if(Set_Hardware_Info(str.data[0])){
				USART1_Bebug_Print(">>", "Set Success.", 1);
			}else{
				USART1_Bebug_Print("ERROR", "Set Hardware Information Failed.", 1);
			}
		}else{
			USART1_Bebug_Print("ERROR", "Syntax Error. <ZKHYSET*HARDWARE:info>.", 1);
		}
	}else
	// 获取硬件信息 <ZKHYCHK*HARDWARE>
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYCHK*HARDWARE"))){
		uint8_t hardware_info[HARDWARE_INFO_SIZE] = {0};
		if(Get_Hardware_Info(hardware_info)){
			USART1_Bebug_Print("Hardware info", hardware_info, 1);
		}else{
			USART1_Bebug_Print("ERROR", "Get Hardware Information Failed.", 1);
		}
	}else
	// 通过USART1将指令转发给USART0，并将获取到的信息打印输出
	if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYGET*4GINFO:"))){
		uint8_t tmp[50] = {0};
		uint8_t cmd[50] = {0};
		uint8_t j = 0;

		for(uint8_t i=0; i<strlen(strx_1)-15; i++){
			if(strx_1[15+i] == '>'){
				tmp[j] = '\0';
				break;
			}else{
				tmp[j] = strx_1[15+i];
				j++;
			}
		}
		strx_1 = NULL;

		// 用USART0将指令发送
		if(j >= 2){
			debug_get_4g_info = 1;
			sprintf(cmd, "%s\r\n", tmp);
			EC200U_SendData(cmd, strlen(cmd),1);//20220909 yuhong 修正4G模块版本号获取命令，直接发送而非使用队列
		}else{
			USART1_Bebug_Print("ERROR", "The Query Instruction Is Too Short.", 1);
		}
		j=0;
	}
	else if((strx_1 = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYCHK*TOOLING"))) //yuhong 进入到测试工装程序
	{
		StrtokStr str = StrtokString(strx_1 + strlen("ZKHYCHK*TOOLING:"));
		if(str.cnt == 1)
		{
			ToolingFlag = atoi(str.data[0]);
			if(ToolingFlag==1)
			{
				USART1_Bebug_Print("TOOLING", "ZKHYCHK*TOOLING>>Enter the Tooling Test mode.", 1);
			}
			else
			{
				USART1_Bebug_Print("TOOLING", "ZKHYCHK*TOOLING>>Exit the Tooling Test mode.", 1);
			}
		}
		else
		{
			USART1_Bebug_Print("ERROR", "Syntax Error. <ZKHYCHK*TOOLING:1/0>.", 1);
		}
	}
	//gcz  2022-05-27  安装检测命令回应,命令处理及回应，均已封装在aebs_detection_in_installation()函数
	if (check_is_valid_uart_pkg(User_Rxbuffer,User_Rxcount) == TRUE)
	{
		aebs_detection_in_installation(USART1_STREAM,User_Rxbuffer);
		g_u8UartFlag = UART_1_ID;
	}
	if (check_is_valid_uart_pkg(User_Rxbuffer_BT,User_Rxcount_BT) == TRUE)
	{
		aebs_detection_in_installation(USART4_STREAM,User_Rxbuffer_BT);
		Clear_UART4_Buffer();	// clear buffer
		g_u8UartFlag = UART_4_ID;
	}
}
/*
 * 按照,进行分割字符串
 * 参数：分割字符串
 * 返回：0失败，1成功
 * 说明： 1223,456,789,adc,456,85ad21,dfdsa00>
 */
StrtokStr StrtokString(uint8_t *string)
{
	StrtokStr strtokStr;
	strtokStr.cnt = 0;
	if(string == NULL) return strtokStr;
	// 如下代码，必须要打印，才能正常输出

//	fprintf(USART1_STREAM,"%s",string);	// 必须打印这句话，才有效，暂时舍弃
	uint8_t *p = strtok(string, ",");
	while(p){
		strcpy(strtokStr.data[strtokStr.cnt], p);

		strtokStr.data[strtokStr.cnt][strlen(strtokStr.data[strtokStr.cnt])] = '\0';
		p = strtok(NULL, ",");
		strtokStr.cnt++;
	}
	// 去除解析最后一个字符串的'>'
	for(uint8_t i=0; i<strlen(strtokStr.data[strtokStr.cnt-1]); i++){
		if(strtokStr.data[strtokStr.cnt-1][i] == '>'){
			strtokStr.data[strtokStr.cnt-1][i] = '\0';
			break;
		}
	}
//	for(uint8_t i=0; i<strtokStr.cnt;i++){
//		fprintf(USART1_STREAM,"%s ",strtokStr.data[i]);
//	}
//	fprintf(USART1_STREAM,"\r\n");

	return strtokStr;
}
/*
 * 线刷超时处理
 */
void Line_Upgrade_Timeout_Dispose()		// add lmz 20221024
{
	uint8_t err_buf[24] = {0};
	sprintf(err_buf, "ERRERRERRERRERR[%d]", down_pkg.recv_bin_times);
	USART1_Bebug_Print("", err_buf, 1);

	upgrade_info.rx_mode 		= RECV_BIN;
	upgrade_info.upgrade_mode 	= UPGRADE_LINE;
	upgrade_info.isUpgradeOK	= false;
	down_pkg.clc_chk 			= 0;
	down_pkg.src_chk 			= 0;
	down_pkg.cnt  				= 0;
}

/*
 * 线刷写bin数据逻辑，要求发送下一包ID的逻辑
 */
bool Line_Write_Bin_Data_Dispose(uint32_t write_len)	// add lmz 20221025
{
	upgrade_info.clc_chk += down_pkg.clc_chk;

//	USART1_Bebug_Print_Num("clc_chk_total", upgrade_info.clc_chk, 3, 1);

	// 动态擦除扇区  add lmz 20221024
	uint16_t erase_sector_id = 0;
	erase_sector_id = down_pkg.addr / EX_FLASH_SECTOR_SIZE;
	W25QXX_Erase_Sector(erase_sector_id);
//	USART1_Bebug_Print_Num("[erase_sector_id]", erase_sector_id, 1, 1);
//	USART1_Bebug_Print_Num("[write addr]", down_pkg.addr, 3, 1);

	// 正常包，写
	W25QXX_Write_NoCheck((char *)down_pkg.data, down_pkg.addr, write_len);

	down_pkg.clc_chk = 0;
	down_pkg.src_chk = 0;
	down_pkg.cnt = 0;
	memset((uint8_t *)down_pkg.data, 0, LINE_BUFFER_SIZE);

	down_pkg.addr += EX_FLASH_SECTOR_SIZE;
	down_pkg.recv_bin_times++;

	uint8_t ok_buf[24] = {0};
	sprintf(ok_buf, "OKOKOKOKOKOK[%d]", down_pkg.recv_bin_times);
	USART1_Bebug_Print("", ok_buf, 1);

	return true;
}
