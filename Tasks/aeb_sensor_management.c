/*
 * aeb_sensor_management.c
 *
 *  Created on: 2022-7-26
 *      Author: Administrator
 */

#include "aeb_sensor_management.h"
#include "w25qxx.h"
#include "usart.h"
#include "upgrade_common.h"
#include "_4g_data_upload.h"
#include "flash.h"
#include "tool.h"

typedef struct _Sensor_Param_Upload
{
	volatile uint16_t m_seq;
	NodeData_S m_noda_data;

}Sensor_Param_Upload;

Sensor_MGT g_sensor_mgt = {0};
Sensor_Param_Upload g_sensor_param_upload = {0};

uint8_t Read_Sensor_Mgt_Cfg_Param(Sensor_MGT *sensor_mgt)
{
	uint8_t data[SENSOR_CFG_SIZE] = {0};
	memset(data, 0, SENSOR_CFG_SIZE);

	 // 内部FLASH
	if(Get_Sensor_MGT_Param_InFlash(data)){
		// 数据还原到结构体中
		uint8_t index = 0;
		// 内容
		//4G_Cfg_Param 2B
//		USART1_Bebug_Print_Num("4G low", data[index], 2, 1);
		sensor_mgt->m_4g.m_isOpen = (data[index] & 0x80) >> 7;
		sensor_mgt->m_4g.m_producer = data[index] & 0x0F;

		//Gps_Cfg_Param 2B
//		USART1_Bebug_Print_Num("GPS low", data[index+2], 2, 1);
		sensor_mgt->m_gps.m_isOpen = (data[index+2] & 0x80) >> 7;
		sensor_mgt->m_gps.m_producer = data[index+2] & 0x0F;

		//Dbl_Cam_Cfg_Param 2B
//		USART1_Bebug_Print_Num("DblCAM low", data[index+4], 2, 1);
		sensor_mgt->m_dbl_cam.m_isOpen = (data[index+4] & 0x80) >> 7;
		sensor_mgt->m_dbl_cam.m_producer = data[index+4] & 0x0F;

		//Sgl_Cam_Cfg_Param 2B
//		USART1_Bebug_Print_Num("SglCAM low", data[index+6], 2, 1);
		sensor_mgt->m_sgl_cam.m_isOpen = (data[index+6] & 0x80) >> 7;
		sensor_mgt->m_sgl_cam.m_producer = data[index+6] & 0x0F;

		//Ultra_Front_Cfg_Param 前方超声波雷达 2B
//		USART1_Bebug_Print_Num("Ultra_front low", data[index+8], 2, 1);
//		USART1_Bebug_Print_Num("Ultra_front high", data[index+9], 2, 1);
		sensor_mgt->m_ultra_radar.m_front.m_main_sw = data[index+8] & 0x01;			// 1bit 单侧总开关
		sensor_mgt->m_ultra_radar.m_front.m_producer = (data[index+8] & 0x0E) >> 1;	// 3bit 厂家
		sensor_mgt->m_ultra_radar.m_front.m_sensor_num = (data[index+8] & 0x70) >> 4;	// 3bit 数目
		sensor_mgt->m_ultra_radar.m_front.m_controler =  ((data[index+9] & 0x01) << 1) | (data[index+8]>>7);// 2bit 控制器编号
		sensor_mgt->m_ultra_radar.m_front.m_rdr_sw = (data[index+9] & 0x7E) >> 1;		// 6bit 单个开关

		//Ultra_end_Cfg_Param 后方超声波雷达 2B
//		USART1_Bebug_Print_Num("Ultra_end low", data[index+10], 2, 1);
//		USART1_Bebug_Print_Num("Ultra_end high", data[index+11], 2, 1);
		sensor_mgt->m_ultra_radar.m_end.m_main_sw = data[index+10] & 0x01;				// 1bit 单侧总开关
		sensor_mgt->m_ultra_radar.m_end.m_producer = (data[index+10] & 0x0E) >> 1;		// 3bit 厂家
		sensor_mgt->m_ultra_radar.m_end.m_sensor_num = (data[index+10] & 0x70) >> 4;		// 3bit 数目
		sensor_mgt->m_ultra_radar.m_end.m_controler =  ((data[index+11] & 0x01) << 1) | (data[index+10]>>7);// 2bit 控制器编号
		sensor_mgt->m_ultra_radar.m_end.m_rdr_sw = (data[index+11] & 0x7E) >> 1;			// 6bit 单个开关

		//Ultra_left_Cfg_Param 左侧超声波雷达 2B
//		USART1_Bebug_Print_Num("Ultra_left low", data[index+12], 2, 1);
//		USART1_Bebug_Print_Num("Ultra_left high", data[index+13], 2, 1);
		sensor_mgt->m_ultra_radar.m_left.m_main_sw = data[index+12] & 0x01;				// 1bit 单侧总开关
		sensor_mgt->m_ultra_radar.m_left.m_producer = (data[index+12] & 0x0E) >> 1;		// 3bit 厂家
		sensor_mgt->m_ultra_radar.m_left.m_sensor_num = (data[index+12] & 0x70) >> 4;		// 3bit 数目
		sensor_mgt->m_ultra_radar.m_left.m_controler =  ((data[index+13] & 0x01) << 1) | (data[index+12]>>7);// 2bit 控制器编号
		sensor_mgt->m_ultra_radar.m_left.m_rdr_sw = (data[index+13] & 0x7E) >> 1;		// 6bit 单个开关

		//Ultra_right_Cfg_Param 右侧超声波雷达 2B
//		USART1_Bebug_Print_Num("Ultra_right low", data[index+14], 2, 1);
//		USART1_Bebug_Print_Num("Ultra_right high", data[index+15], 2, 1);
		sensor_mgt->m_ultra_radar.m_right.m_main_sw = data[index+14] & 0x01;			// 1bit 单侧总开关
		sensor_mgt->m_ultra_radar.m_right.m_producer = (data[index+14] & 0x0E) >> 1;	// 3bit 厂家
		sensor_mgt->m_ultra_radar.m_right.m_sensor_num = (data[index+14] & 0x70) >> 4;	// 3bit 数目
		sensor_mgt->m_ultra_radar.m_right.m_controler =  ((data[index+15] & 0x01) << 1) | (data[index+14]>>7);// 2bit 控制器编号
		sensor_mgt->m_ultra_radar.m_right.m_rdr_sw = (data[index+15] & 0x7E) >> 1;		// 6bit 单个开关

		//MMW_Front_Cfg_Param 前方毫米波雷达 2B
//		USART1_Bebug_Print_Num("mmw_front low", data[index+16], 2, 1);
//		USART1_Bebug_Print_Num("mmw_front high", data[index+17], 2, 1);
		sensor_mgt->m_mmw_radar.m_front.m_main_sw = data[index+16] & 0x01;			// 1bit 单侧总开关
		sensor_mgt->m_mmw_radar.m_front.m_producer = (data[index+16] & 0x0E) >> 1;	// 3bit 厂家
		sensor_mgt->m_mmw_radar.m_front.m_sensor_num = (data[index+16] & 0x30) >> 4;	// 2bit 数目
		sensor_mgt->m_mmw_radar.m_front.m_rdr_sw = data[index+17] & 0x01;		// 1bit 单个开关

		//MMW_End_Cfg_Param 后方毫米波雷达 2B
//		USART1_Bebug_Print_Num("mmw_end low", data[index+18], 2, 1);
//		USART1_Bebug_Print_Num("mmw_end high", data[index+19], 2, 1);
		sensor_mgt->m_mmw_radar.m_end.m_main_sw = data[index+18] & 0x01;			// 1bit 单侧总开关
		sensor_mgt->m_mmw_radar.m_end.m_producer = (data[index+18] & 0x0E) >> 1;	// 3bit 厂家
		sensor_mgt->m_mmw_radar.m_end.m_sensor_num = (data[index+18] & 0x30) >> 4;	// 2bit 数目
		sensor_mgt->m_mmw_radar.m_end.m_rdr_sw = data[index+19] & 0x01 ;		// 1bit 单个开关

		//MMW_Left_Cfg_Param 左侧毫米波雷达 2B
//		USART1_Bebug_Print_Num("mmw_left low", data[index+20], 2, 1);
//		USART1_Bebug_Print_Num("mmw_left high", data[index+21], 2, 1);
		sensor_mgt->m_mmw_radar.m_left.m_main_sw = data[index+20] & 0x01;			// 1bit 单侧总开关
		sensor_mgt->m_mmw_radar.m_left.m_producer = (data[index+20] & 0x0E) >> 1;	// 3bit 厂家
		sensor_mgt->m_mmw_radar.m_left.m_sensor_num = (data[index+20] & 0x30) >> 4;	// 2bit 数目
		sensor_mgt->m_mmw_radar.m_left.m_rdr_sw = data[index+21] & 0x03;		// 2bit 2个开关

		//MMW_Right_Cfg_Param 右侧毫米波雷达 2B
//		USART1_Bebug_Print_Num("mmw_right low", data[index+22], 2, 1);
//		USART1_Bebug_Print_Num("mmw_right high", data[index+23], 2, 1);
		sensor_mgt->m_mmw_radar.m_right.m_main_sw = data[index+22] & 0x01;			// 1bit 单侧总开关
		sensor_mgt->m_mmw_radar.m_right.m_producer = (data[index+22] & 0x0E) >> 1;	// 3bit 厂家
		sensor_mgt->m_mmw_radar.m_right.m_sensor_num = (data[index+22] & 0x30) >> 4;	// 2bit 数目
		sensor_mgt->m_mmw_radar.m_right.m_rdr_sw = data[index+23] & 0x03;		// 2bit 2个开关

		//Imu_Cfg_Param 2B
//		USART1_Bebug_Print_Num("imu low", data[index+24], 2, 1);
		sensor_mgt->m_imu.m_isOpen = (data[index+24] & 0x80) >> 7;
		sensor_mgt->m_imu.m_producer = data[index+24] & 0x0F;

		//Car_DVR_Cfg_Param 2B
//		USART1_Bebug_Print_Num("car_dvr low", data[index+26], 2, 1);
		sensor_mgt->m_car_dvr.m_isOpen = (data[index+26] & 0x80) >> 7;
		sensor_mgt->m_car_dvr.m_producer = data[index+26] & 0x0F;

		//Actuator_Cfg_Param 2B
//		USART1_Bebug_Print_Num("brk_act low", data[index+28], 2, 1);
		sensor_mgt->m_actuator.m_isOpen = (data[index+28] & 0x80) >> 7;
		sensor_mgt->m_actuator.m_producer = data[index+28] & 0x0F;

//		USART1_Bebug_Print_Num("project", data[index+30], 2, 1);
//		USART1_Bebug_Print_Num("product", data[index+31], 2, 1);
		sensor_mgt->m_project = data[index+30];		// 项目
		sensor_mgt->m_product = data[index+31];		// 产品类型
		sensor_mgt->veh_srr_demo_sw = data[index+32];		// 角雷达行人横穿demo 开关

//		Sensor_Print(*sensor_mgt);
		return 1;
	}

	USART1_Bebug_Print("Read Failed", "Read Sensor Configure Parameter Failed.", 1);
	memset(sensor_mgt, 0, sizeof(sensor_mgt));

	return 0;
}

uint8_t Write_Sensor_Mgt_Cfg_Param(Sensor_MGT sensor_mgt)
{
	uint8_t data[SENSOR_CFG_SIZE] = {0};
	memset(data, 0, SENSOR_CFG_SIZE);

	// 内部FLASH
	// 不直接去写数据，做一次封装，方便统一两者（参数配置上位机和MCU）的读写操作
	// 4G 模块  4~6bit预留和第二个字节保留
	data[0] = sensor_mgt.m_4g.m_producer & 0x0F;								// 0~3bit 厂家
	data[0] |= sensor_mgt.m_4g.m_isOpen << 7;									// 7bit 功能开关
	data[1] = 0x00;																// 高8bit预留
//	USART1_Bebug_Print_Num("4G low", data[0], 2, 1);

	// GPS 模块  4~6bit预留和第二个字节保留
	data[2] = sensor_mgt.m_gps.m_producer & 0x0F;								// 0~3bit 厂家
	data[2] |= sensor_mgt.m_gps.m_isOpen << 7;									// 7bit 功能开关
	data[3] = 0x00;																// 高8bit预留
//	USART1_Bebug_Print_Num("GPS low", data[2], 2, 1);

	// 双目相机 模块 4~6bit预留和第二个字节保留
	data[4] = sensor_mgt.m_dbl_cam.m_producer & 0x0F;								// 0~3bit 厂家
	data[4] |= sensor_mgt.m_dbl_cam.m_isOpen << 7;								// 7bit 功能开关
	data[5] = 0x00;																// 高8bit预留
//	USART1_Bebug_Print_Num("DblCam low", data[4], 2, 1);

	// 单目相机 模块 4~6bit预留和第二个字节保留
	data[6] = sensor_mgt.m_sgl_cam.m_producer & 0x0F;								// 0~3bit 厂家
	data[6] |= sensor_mgt.m_sgl_cam.m_isOpen << 7;								// 7bit 功能开关
	data[7] = 0x00;																// 高8bit预留
//	USART1_Bebug_Print_Num("SglCam low", data[6], 2, 1);

	// 前方超声波雷达 模块 第二个字节7bit保留
	data[8] = sensor_mgt.m_ultra_radar.m_front.m_main_sw;						// 1bit 功能开关
	data[8] |= (sensor_mgt.m_ultra_radar.m_front.m_producer & 0x07) << 1;		// 1~3bit 厂家
	data[8] |= (sensor_mgt.m_ultra_radar.m_front.m_sensor_num & 0x07) << 4;		// 4~6bit 传感器数量
	data[8] |= sensor_mgt.m_ultra_radar.m_front.m_controler << 7;				// 7bit 控制器的ID 低位
	data[9] = sensor_mgt.m_ultra_radar.m_front.m_controler >> 1;				// 0bit 控制器ID 高位
	data[9] |= sensor_mgt.m_ultra_radar.m_front.m_rdr_sw << 1;					// 1bit 1号雷达开关
	data[9] &= 0x7F;															// 7bit 保留
//	USART1_Bebug_Print_Num("Ultra Front low", data[8], 2, 1);
//	USART1_Bebug_Print_Num("Ultra Front high", data[9], 2, 1);

	// 后方超声波雷达 模块 第二个字节7bit保留
	data[10] = sensor_mgt.m_ultra_radar.m_end.m_main_sw;						// 1bit 功能开关
	data[10] |= (sensor_mgt.m_ultra_radar.m_end.m_producer & 0x07) << 1;		// 1~3bit 厂家
	data[10] |= (sensor_mgt.m_ultra_radar.m_end.m_sensor_num & 0x07) << 4;		// 4~6bit 传感器数量
	data[10] |= sensor_mgt.m_ultra_radar.m_end.m_controler << 7;				// 7bit 控制器的ID 低位
	data[11] = sensor_mgt.m_ultra_radar.m_end.m_controler >> 1;					// 0bit 控制器ID 高位
	data[11] |= sensor_mgt.m_ultra_radar.m_end.m_rdr_sw << 1;					// 1bit 1号雷达开关
	data[11] &= 0x7F;															// 7bit 保留
//	USART1_Bebug_Print_Num("Ultra End low", data[10], 2, 1);
//	USART1_Bebug_Print_Num("Ultra End high", data[11], 2, 1);

	// 左侧超声波雷达 模块 第二个字节7bit保留
	data[12] = sensor_mgt.m_ultra_radar.m_left.m_main_sw;						// 1bit 功能开关
	data[12] |= (sensor_mgt.m_ultra_radar.m_left.m_producer & 0x07) << 1;		// 1~3bit 厂家
	data[12] |= (sensor_mgt.m_ultra_radar.m_left.m_sensor_num & 0x07) << 4;		// 4~6bit 传感器数量
	data[12] |= sensor_mgt.m_ultra_radar.m_left.m_controler << 7;				// 7bit 控制器的ID 低位
	data[13] = sensor_mgt.m_ultra_radar.m_left.m_controler >> 1;				// 0bit 控制器ID 高位
	data[13] |= sensor_mgt.m_ultra_radar.m_left.m_rdr_sw << 1;					// 1bit 1号雷达开关
	data[13] &= 0x7F;															// 7bit 保留
//	USART1_Bebug_Print_Num("Ultra Left low", data[12], 2, 1);
//	USART1_Bebug_Print_Num("Ultra Left high", data[13], 2, 1);

	// 右侧超声波雷达 模块 第二个字节7bit保留
	data[14] = sensor_mgt.m_ultra_radar.m_right.m_main_sw;						// 1bit 功能开关
	data[14] |= (sensor_mgt.m_ultra_radar.m_right.m_producer & 0x07) << 1;		// 1~3bit 厂家
	data[14] |= (sensor_mgt.m_ultra_radar.m_right.m_sensor_num & 0x07) << 4;	// 4~6bit 传感器数量
	data[14] |= sensor_mgt.m_ultra_radar.m_right.m_controler << 7;				// 7bit 控制器的ID 低位
	data[15] = sensor_mgt.m_ultra_radar.m_right.m_controler >> 1;				// 0bit 控制器ID 高位
	data[15] |= sensor_mgt.m_ultra_radar.m_right.m_rdr_sw << 1;					// 1bit 1号雷达开关
	data[15] &= 0x7F;															// 7bit 保留
//	USART1_Bebug_Print_Num("Ultra Right low", data[14], 2, 1);
//	USART1_Bebug_Print_Num("Ultra Right high", data[15], 2, 1);

	// 前方毫米波雷达 第一个字节6~7bit保留 第二个字节1~7bit保留
	data[16] = sensor_mgt.m_mmw_radar.m_front.m_main_sw;						// 1bit 功能开关
	data[16] |= (sensor_mgt.m_mmw_radar.m_front.m_producer & 0x07) << 1;		// 1~3bit 厂家
	data[16] |= (sensor_mgt.m_mmw_radar.m_front.m_sensor_num & 0x07) << 4;		// 4~5bit 传感器数量
	data[16] &= 0x3F;															// 6~7bit 预留
	data[17] = sensor_mgt.m_mmw_radar.m_front.m_rdr_sw;							// 0bit 1号雷达开关
	data[17] &= 0x01;															// 1~7bit 预留
//	USART1_Bebug_Print_Num("MMW Front low", data[16], 2, 1);
//	USART1_Bebug_Print_Num("MMW Front high", data[17], 2, 1);

	// 后方毫米波雷达 第一个字节6~7bit保留 第二个字节1~7bit保留
	data[18] = sensor_mgt.m_mmw_radar.m_end.m_main_sw;							// 1bit 功能开关
	data[18] |= (sensor_mgt.m_mmw_radar.m_end.m_producer & 0x07) << 1;			// 1~3bit 厂家
	data[18] |= (sensor_mgt.m_mmw_radar.m_end.m_sensor_num & 0x07) << 4;			// 4~5bit 传感器数量
	data[18] &= 0x3F;															// 6~7bit 预留
	data[19] = sensor_mgt.m_mmw_radar.m_end.m_rdr_sw;							// 0bit 1号雷达开关
	data[19] &= 0x01;															// 1~7bit 预留
//	USART1_Bebug_Print_Num("MMW End low", data[18], 2, 1);
//	USART1_Bebug_Print_Num("MMW End high", data[19], 2, 1);

	// 左侧毫米波雷达 第一个字节6~7bit保留 第二个字节2~7bit保留
	data[20] = sensor_mgt.m_mmw_radar.m_left.m_main_sw;							// 1bit 功能开关
	data[20] |= (sensor_mgt.m_mmw_radar.m_left.m_producer & 0x07) << 1;			// 1~3bit 厂家
	data[20] |= (sensor_mgt.m_mmw_radar.m_left.m_sensor_num & 0x07) << 4;		// 4~5bit 传感器数量
	data[20] &= 0x3F;															// 6~7bit 预留
	data[21] = sensor_mgt.m_mmw_radar.m_left.m_rdr_sw;							// 0~1bit 1~2号雷达开关
	data[21] &= 0x03;															// 2~7bit 预留
//	USART1_Bebug_Print_Num("MMW Left low", data[20], 2, 1);
//	USART1_Bebug_Print_Num("MMW Left high", data[21], 2, 1);

	// 右侧毫米波雷达 第一个字节6~7bit保留 第二个字节2~7bit保留
	data[22] = sensor_mgt.m_mmw_radar.m_right.m_main_sw;						// 1bit 功能开关
	data[22] |= (sensor_mgt.m_mmw_radar.m_right.m_producer & 0x07) << 1;		// 1~3bit 厂家
	data[22] |= (sensor_mgt.m_mmw_radar.m_right.m_sensor_num & 0x07) << 4;		// 4~5bit 传感器数量
	data[22] &= 0x3F;															// 6~7bit 预留
	data[23] = sensor_mgt.m_mmw_radar.m_right.m_rdr_sw;							// 0~1bit 1~2号雷达开关
	data[23] &= 0x03;															// 2~7bit 预留
//	USART1_Bebug_Print_Num("MMW Right low", data[22], 2, 1);
//	USART1_Bebug_Print_Num("MMW Right high", data[23], 2, 1);

	// IMU 模块  4~6bit预留和第二个字节保留
	data[24] = sensor_mgt.m_imu.m_producer & 0x0F;								// 0~3bit 厂家
	data[24] |= sensor_mgt.m_imu.m_isOpen << 7;									// 7bit 功能开关
	data[25] = 0x00;															// 高8bit预留
//	USART1_Bebug_Print_Num("IMU low", data[24], 2, 1);

	// 行车记录仪 模块  4~6bit预留和第二个字节保留
	data[26] = sensor_mgt.m_car_dvr.m_producer & 0x0F;							// 0~3bit 厂家
	data[26] |= sensor_mgt.m_car_dvr.m_isOpen << 7;								// 7bit 功能开关
	data[27] = 0x00;															// 高8bit预留
//	USART1_Bebug_Print_Num("CarDVR low", data[26], 2, 1);

	// 制动机构 模块  4~6bit预留和第二个字节保留
	data[28] = sensor_mgt.m_actuator.m_producer & 0x0F;							// 0~3bit 厂家
	data[28] |= sensor_mgt.m_actuator.m_isOpen << 7;							// 7bit 功能开关
	data[29] = 0x00;															// 高8bit预留
//	USART1_Bebug_Print_Num("BrkAct low", data[28], 2, 1);

	// 项目类型
	data[30] = sensor_mgt.m_project;
//	USART1_Bebug_Print_Num("Project low", data[30], 2, 1);

	// 产品类型
	data[31] = sensor_mgt.m_product;
//	USART1_Bebug_Print_Num("Product low", data[31], 2, 1);
	data[32] = sensor_mgt.veh_srr_demo_sw;          //演示行人横穿demo 开关  lyj 2022-9-24
	uint8_t clac_chk = 0;
	for (int i = 0; i < SENSOR_CFG_SIZE; i++) {
		clac_chk += data[i];
	}
	data[510] = clac_chk;
//	USART1_Bebug_Print_Num("clc_chk", clac_chk, 2, 1);

	if(Set_Sensor_MGT_Param_InFlash(data)){
//		USART1_Bebug_Print("Write OK", "Write Sensor Configure Parameter Success.", 1);
		return 1;
	}

	USART1_Bebug_Print("Write Failed", "Write Sensor Configure Parameter Failed.", 1);

	return 0;
}

void Load_Sensor_Management_Default_Param(Sensor_MGT *sensor_mgt)
{
	sensor_mgt->m_project							= 1;
	sensor_mgt->m_product							= 1;

	sensor_mgt->m_4g.m_isOpen 						= FUNC_OPEN;
	sensor_mgt->m_4g.m_producer 					= QUECTEL;

	sensor_mgt->m_gps.m_isOpen 						= FUNC_OPEN;
	sensor_mgt->m_gps.m_producer 					= QUECTEL;

	sensor_mgt->m_imu.m_isOpen 						= FUNC_OPEN;
	sensor_mgt->m_imu.m_producer 					= 1;

	sensor_mgt->m_dbl_cam.m_isOpen					= FUNC_OPEN;
	sensor_mgt->m_dbl_cam.m_producer 				= ZKHY;

	sensor_mgt->m_sgl_cam.m_isOpen					= FUNC_CLOSE;
	sensor_mgt->m_sgl_cam.m_producer 				= 1;

	sensor_mgt->m_car_dvr.m_isOpen 					= FUNC_OPEN;
	sensor_mgt->m_car_dvr.m_producer 				= SHUNHE;

	sensor_mgt->m_actuator.m_isOpen 				= FUNC_OPEN;
	sensor_mgt->m_actuator.m_producer 				= 1;

	sensor_mgt->m_ultra_radar.m_front.m_main_sw 	= FUNC_OPEN;
	sensor_mgt->m_ultra_radar.m_front.m_controler 	= 1;
	sensor_mgt->m_ultra_radar.m_front.m_producer	= 1;
	sensor_mgt->m_ultra_radar.m_front.m_sensor_num 	= 2;
	sensor_mgt->m_ultra_radar.m_front.m_rdr_sw 		= 1<<5 | 0<<4 | 0<<3 | 0<<2 | 0<<1 | 1;

	sensor_mgt->m_ultra_radar.m_end.m_main_sw 		= FUNC_CLOSE;
	sensor_mgt->m_ultra_radar.m_end.m_controler 	= 1;
	sensor_mgt->m_ultra_radar.m_end.m_producer		= 1;
	sensor_mgt->m_ultra_radar.m_end.m_sensor_num 	= 0;
	sensor_mgt->m_ultra_radar.m_end.m_rdr_sw 		= 0<<5 | 0<<4 | 0<<3 | 0<<2 | 0<<1 | 0;

	sensor_mgt->m_ultra_radar.m_left.m_main_sw 		= FUNC_CLOSE;
	sensor_mgt->m_ultra_radar.m_left.m_controler 	= 1;
	sensor_mgt->m_ultra_radar.m_left.m_producer		= 1;
	sensor_mgt->m_ultra_radar.m_left.m_sensor_num 	= 0;
	sensor_mgt->m_ultra_radar.m_left.m_rdr_sw 		= 0<<5 | 0<<4 | 0<<3 | 0<<2 | 0<<1 | 0;

	sensor_mgt->m_ultra_radar.m_right.m_main_sw 	= FUNC_OPEN;
	sensor_mgt->m_ultra_radar.m_right.m_controler 	= 1;
	sensor_mgt->m_ultra_radar.m_right.m_producer	= 1;
	sensor_mgt->m_ultra_radar.m_right.m_sensor_num 	= 3;
	sensor_mgt->m_ultra_radar.m_right.m_rdr_sw 		= 0<<5 | 0<<4 | 0<<3 | 1<<2 | 1<<1 | 1;

	sensor_mgt->m_mmw_radar.m_front.m_main_sw 		= FUNC_OPEN;
	sensor_mgt->m_mmw_radar.m_front.m_producer		= 1;
	sensor_mgt->m_mmw_radar.m_front.m_sensor_num 	= 1;
	sensor_mgt->m_mmw_radar.m_front.m_rdr_sw 		= 1;

	sensor_mgt->m_mmw_radar.m_end.m_main_sw 		= FUNC_CLOSE;
	sensor_mgt->m_mmw_radar.m_end.m_producer		= 1;
	sensor_mgt->m_mmw_radar.m_end.m_sensor_num 		= 0;
	sensor_mgt->m_mmw_radar.m_end.m_rdr_sw 			= 0;

	sensor_mgt->m_mmw_radar.m_left.m_main_sw 		= FUNC_CLOSE;
	sensor_mgt->m_mmw_radar.m_left.m_producer		= 1;
	sensor_mgt->m_mmw_radar.m_left.m_sensor_num 	= 0;
	sensor_mgt->m_mmw_radar.m_left.m_rdr_sw 		= 0<<1 | 0;

	sensor_mgt->m_mmw_radar.m_right.m_main_sw 		= FUNC_OPEN;
	sensor_mgt->m_mmw_radar.m_right.m_producer		= 2;
	sensor_mgt->m_mmw_radar.m_right.m_sensor_num 	= 1;
	sensor_mgt->m_mmw_radar.m_right.m_rdr_sw 		= 0<<1 | 1;

	sensor_mgt->m_imu.m_isOpen 						= FUNC_OPEN;
	sensor_mgt->m_imu.m_producer 					= 4;
	sensor_mgt->veh_srr_demo_sw                     = false;	//演示行人横穿demo 开关  lyj 2022-9-24
}

void Print_Sensor_Cfg_Param()
{
//	fprintf(USART1_STREAM, "%s, %d\r\n", __FUNCTION__, __LINE__);
	Sensor_MGT l_sensor_mgt = {0};
	if(!Read_Sensor_Mgt_Cfg_Param(&l_sensor_mgt)){
		USART1_Bebug_Print("Read Sensor Mgt Failed", "", 1);
//		Load_Sensor_Management_Default_Param(&g_sensor_mgt);
//		Write_Sensor_Mgt_Cfg_Param(g_sensor_mgt);
		return ;
	}

	USART1_Bebug_Print("", "-------------------------------------", 1);
	USART1_Bebug_Print_Num("[project]", l_sensor_mgt.m_project, 1, 1);
	USART1_Bebug_Print_Num("[product]", l_sensor_mgt.m_product, 1, 1);

	USART1_Bebug_Print_Num("[4G]Swt", l_sensor_mgt.m_4g.m_isOpen, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_4g.m_producer, 1, 1);

	USART1_Bebug_Print_Num("[GPS]Swt", l_sensor_mgt.m_gps.m_isOpen, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_gps.m_producer, 1, 1);

	USART1_Bebug_Print_Num("[IMU]Swt", l_sensor_mgt.m_imu.m_isOpen, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_imu.m_producer, 1, 1);

	USART1_Bebug_Print_Num("[Double Cam]Swt", l_sensor_mgt.m_dbl_cam.m_isOpen, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_dbl_cam.m_producer, 1, 1);

	USART1_Bebug_Print_Num("[Single Cam]Swt", l_sensor_mgt.m_sgl_cam.m_isOpen, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_sgl_cam.m_producer, 1, 1);

	USART1_Bebug_Print_Num("[CAR DVR]Swt", l_sensor_mgt.m_car_dvr.m_isOpen, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_car_dvr.m_producer, 1, 1);

	USART1_Bebug_Print_Num("[Actuator]Swt", l_sensor_mgt.m_actuator.m_isOpen, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_actuator.m_producer, 1, 1);

//	USART1_Bebug_Print_Num("[Ultra Radar]Main Swt", g_sensor_mgt.m_ultra_radar.m_isOpen, 1, 1);
	USART1_Bebug_Print_Num("\r\n[Ultra Radar][front]Main_Sw", l_sensor_mgt.m_ultra_radar.m_front.m_main_sw, 0, 1);
	USART1_Bebug_Print_Num("controler", l_sensor_mgt.m_ultra_radar.m_front.m_controler, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_ultra_radar.m_front.m_producer, 0, 1);
	USART1_Bebug_Print_Num("sensor_num", l_sensor_mgt.m_ultra_radar.m_front.m_sensor_num, 1, 1);
	USART1_Bebug_Print_Num("\trdr1_sw", l_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr2_sw", (l_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw>>1) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr3_sw", (l_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw>>2) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr4_sw", (l_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw>>3) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr5_sw", (l_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw>>4) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr6_sw", (l_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw>>5) & 0x01, 1, 1);

	USART1_Bebug_Print_Num("\t[end]Main_Sw", l_sensor_mgt.m_ultra_radar.m_end.m_main_sw, 0, 1);
	USART1_Bebug_Print_Num("controler", l_sensor_mgt.m_ultra_radar.m_end.m_controler, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_ultra_radar.m_end.m_producer, 0, 1);
	USART1_Bebug_Print_Num("sensor_num", l_sensor_mgt.m_ultra_radar.m_end.m_sensor_num, 1, 1);
	USART1_Bebug_Print_Num("\trdr1_sw", l_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr2_sw", (l_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw>>1) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr3_sw", (l_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw>>2) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr4_sw", (l_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw>>3) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr5_sw", (l_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw>>4) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr6_sw", (l_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw>>5) & 0x01, 1, 1);

	USART1_Bebug_Print_Num("\t[left]Main_Sw", l_sensor_mgt.m_ultra_radar.m_left.m_main_sw, 0, 1);
	USART1_Bebug_Print_Num("controler", l_sensor_mgt.m_ultra_radar.m_left.m_controler, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_ultra_radar.m_left.m_producer, 0, 1);
	USART1_Bebug_Print_Num("sensor_num", l_sensor_mgt.m_ultra_radar.m_left.m_sensor_num, 1, 1);
	USART1_Bebug_Print_Num("\trdr1_sw", l_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr2_sw", (l_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw>>1) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr3_sw", (l_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw>>2) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr4_sw", (l_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw>>3) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr5_sw", (l_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw>>4) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr6_sw", (l_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw>>5) & 0x01, 1, 1);

	USART1_Bebug_Print_Num("\t[right]Main_Sw", l_sensor_mgt.m_ultra_radar.m_right.m_main_sw, 0, 1);
	USART1_Bebug_Print_Num("controler", l_sensor_mgt.m_ultra_radar.m_right.m_controler, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_ultra_radar.m_right.m_producer, 0, 1);
	USART1_Bebug_Print_Num("sensor_num", l_sensor_mgt.m_ultra_radar.m_right.m_sensor_num, 1, 1);
	USART1_Bebug_Print_Num("\trdr1_sw", l_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr2_sw", (l_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw>>1) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr3_sw", (l_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw>>2) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr4_sw", (l_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw>>3) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr5_sw", (l_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw>>4) & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr6_sw", (l_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw>>5) & 0x01, 1, 1);

	USART1_Bebug_Print_Num("\r\n[MMW][front]Main_Sw", l_sensor_mgt.m_mmw_radar.m_front.m_main_sw, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_mmw_radar.m_front.m_producer, 0, 1);
	USART1_Bebug_Print_Num("sensor_num", l_sensor_mgt.m_mmw_radar.m_front.m_sensor_num, 0, 1);
	USART1_Bebug_Print_Num("rdr1_sw", l_sensor_mgt.m_mmw_radar.m_front.m_rdr_sw & 0x01, 1, 1);

	USART1_Bebug_Print_Num("\t[end]Main_Sw", l_sensor_mgt.m_mmw_radar.m_end.m_main_sw, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_mmw_radar.m_end.m_producer, 0, 1);
	USART1_Bebug_Print_Num("sensor_num", l_sensor_mgt.m_mmw_radar.m_end.m_sensor_num, 0, 1);
	USART1_Bebug_Print_Num("rdr1_sw", l_sensor_mgt.m_mmw_radar.m_end.m_rdr_sw & 0x01, 1, 1);

	USART1_Bebug_Print_Num("\t[left]Main_Sw", l_sensor_mgt.m_mmw_radar.m_left.m_main_sw, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_mmw_radar.m_left.m_producer, 0, 1);
	USART1_Bebug_Print_Num("sensor_num", l_sensor_mgt.m_mmw_radar.m_left.m_sensor_num, 0, 1);
	USART1_Bebug_Print_Num("rdr1_sw", l_sensor_mgt.m_mmw_radar.m_left.m_rdr_sw & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr2_sw", (l_sensor_mgt.m_mmw_radar.m_left.m_rdr_sw>>1) & 0x01, 1, 1);

	USART1_Bebug_Print_Num("\t[right]Main_Sw", l_sensor_mgt.m_mmw_radar.m_right.m_main_sw, 0, 1);
	USART1_Bebug_Print_Num("producer", l_sensor_mgt.m_mmw_radar.m_right.m_producer, 0, 1);
	USART1_Bebug_Print_Num("sensor_num", l_sensor_mgt.m_mmw_radar.m_right.m_sensor_num, 0, 1);
	USART1_Bebug_Print_Num("rdr1_sw", l_sensor_mgt.m_mmw_radar.m_right.m_rdr_sw & 0x01, 0, 1);
	USART1_Bebug_Print_Num("rdr2_sw", (l_sensor_mgt.m_mmw_radar.m_right.m_rdr_sw>>1) & 0x01, 1, 1);
	USART1_Bebug_Print_Num("srr_demo_sw", (l_sensor_mgt.veh_srr_demo_sw) & 0x01, 1, 1);//演示行人横穿demo 开关  lyj 2022-9-24

	//	USART1_Bebug_Print_Num("test_rdr_sw", l_sensor_mgt.m_mmw_radar.m_right.m_rdr_sw, 2, 1);
	USART1_Bebug_Print("", "-------------------------------------", 1);
}
extern bool srr_demo_sw;
void Sensor_Management_Init()
{
	// 加载传感器配置，加载失败就采用默认参数值
	if(Read_Sensor_Mgt_Cfg_Param(&g_sensor_mgt)){
		// 测试打印
//		Print_Sensor_Cfg_Param();
	}else{	// 失败，就加载默认参数
		USART1_Bebug_Print("Sensor Mgt", "Load Default Sensor Default Parameters.", 1);
		Load_Sensor_Management_Default_Param(&g_sensor_mgt);
		Write_Sensor_Mgt_Cfg_Param(g_sensor_mgt);
	}
	srr_demo_sw = g_sensor_mgt.veh_srr_demo_sw& 0x01;              // lyj 行人横穿demo开关的初始化
//	Print_Sensor_Cfg_Param();
	memset(&g_sensor_param_upload, 0, sizeof(Sensor_Param_Upload));
	g_sensor_param_upload.m_seq = 1;
}

/*
 * 获取传感器管理接口（对外）
 */
//static inline Sensor_MGT Get_Sensor_MGT_Param()
//{
//	return  g_sensor_mgt;
//}
/*
 * 设备传感器管理接口（对外）
 */
uint8_t Set_Sensor_MGT_Param(Sensor_MGT sensor_mgt)
{
//	g_sensor_mgt = sensor_mgt;		// 同步
	return Write_Sensor_Mgt_Cfg_Param(sensor_mgt);
}

/************************************ 传感器参数配置区（二进制协议） ******************************************/
/*
 * 平台设置传感器配置参数
 */
uint8_t Analysis_Request_Set_Sensor_Cfg_Param_ASCII(uint8_t *data)
{
	Sensor_MGT l_sensor_mgt = {0};
	memset(&l_sensor_mgt, 0, sizeof(Sensor_MGT));

	uint8_t index = 0;
	// 内容
	//4G_Cfg_Param 2B
//	USART1_Bebug_Print_Num("m_4g.data[0]", data[0], 2, 1);
	l_sensor_mgt.m_4g.m_isOpen = (data[index] & 0x80) >> 7;
	l_sensor_mgt.m_4g.m_producer = data[index] & 0x0F;
//	USART1_Bebug_Print_Num("m_4g.m_isOpen", l_sensor_mgt.m_4g.m_isOpen, 1, 1);
	//Gps_Cfg_Param 2B
	l_sensor_mgt.m_gps.m_isOpen = (data[index+2] & 0x80) >> 7;
	l_sensor_mgt.m_gps.m_producer = data[index+2] & 0x0F;

	//Dbl_Cam_Cfg_Param 2B
	l_sensor_mgt.m_dbl_cam.m_isOpen = (data[index+4] & 0x80) >> 7;
	l_sensor_mgt.m_dbl_cam.m_producer = data[index+4] & 0x0F;

	//Sgl_Cam_Cfg_Param 2B
	l_sensor_mgt.m_sgl_cam.m_isOpen = (data[index+6] & 0x80) >> 7;
	l_sensor_mgt.m_sgl_cam.m_producer = data[index+6] & 0x0F;

	//Ultra_Front_Cfg_Param 前方超声波雷达 2B
//	USART1_Bebug_Print_Num("Ultra_front low", data[index+8], 2, 1);
//	USART1_Bebug_Print_Num("Ultra_front high", data[index+9], 2, 1);
	l_sensor_mgt.m_ultra_radar.m_front.m_main_sw = data[index+8] & 0x01;			// 1bit 单侧总开关
	l_sensor_mgt.m_ultra_radar.m_front.m_producer = (data[index+8] & 0x0E) >> 1;	// 3bit 厂家
	l_sensor_mgt.m_ultra_radar.m_front.m_sensor_num = (data[index+8] & 0x70) >> 4;	// 3bit 数目
	l_sensor_mgt.m_ultra_radar.m_front.m_controler =  ((data[index+9] & 0x01) << 1) | (data[index+8]>>7);// 2bit 控制器编号
	l_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw = (data[index+9] & 0x7E) >> 1;		// 6bit 单个开关

	//Ultra_end_Cfg_Param 后方超声波雷达 2B
//	USART1_Bebug_Print_Num("Ultra_end low", data[index+10], 2, 1);
//	USART1_Bebug_Print_Num("Ultra_end high", data[index+11], 2, 1);
	l_sensor_mgt.m_ultra_radar.m_end.m_main_sw = data[index+10] & 0x01;				// 1bit 单侧总开关
	l_sensor_mgt.m_ultra_radar.m_end.m_producer = (data[index+10] & 0x0E) >> 1;		// 3bit 厂家
	l_sensor_mgt.m_ultra_radar.m_end.m_sensor_num = (data[index+10] & 0x70) >> 4;		// 3bit 数目
	l_sensor_mgt.m_ultra_radar.m_end.m_controler =  ((data[index+11] & 0x01) << 1) | (data[index+10]>>7);// 2bit 控制器编号
	l_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw = (data[index+11] & 0x7E) >> 1;			// 6bit 单个开关

	//Ultra_left_Cfg_Param 左侧超声波雷达 2B
//	USART1_Bebug_Print_Num("Ultra_left low", data[index+12], 2, 1);
//	USART1_Bebug_Print_Num("Ultra_left high", data[index+13], 2, 1);
	l_sensor_mgt.m_ultra_radar.m_left.m_main_sw = data[index+12] & 0x01;				// 1bit 单侧总开关
	l_sensor_mgt.m_ultra_radar.m_left.m_producer = (data[index+12] & 0x0E) >> 1;		// 3bit 厂家
	l_sensor_mgt.m_ultra_radar.m_left.m_sensor_num = (data[index+12] & 0x70) >> 4;		// 3bit 数目
	l_sensor_mgt.m_ultra_radar.m_left.m_controler =  ((data[index+13] & 0x01) << 1) | (data[index+12]>>7);// 2bit 控制器编号
	l_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw = (data[index+13] & 0x7E) >> 1;		// 6bit 单个开关

	//Ultra_right_Cfg_Param 右侧超声波雷达 2B
//	USART1_Bebug_Print_Num("Ultra_right low", data[index+14], 2, 1);
//	USART1_Bebug_Print_Num("Ultra_right high", data[index+15], 2, 1);
	l_sensor_mgt.m_ultra_radar.m_right.m_main_sw = data[index+14] & 0x01;			// 1bit 单侧总开关
	l_sensor_mgt.m_ultra_radar.m_right.m_producer = (data[index+14] & 0x0E) >> 1;	// 3bit 厂家
	l_sensor_mgt.m_ultra_radar.m_right.m_sensor_num = (data[index+14] & 0x70) >> 4;	// 3bit 数目
	l_sensor_mgt.m_ultra_radar.m_right.m_controler =  ((data[index+15] & 0x01) << 1) | (data[index+14]>>7);// 2bit 控制器编号
	l_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw = (data[index+15] & 0x7E) >> 1;		// 6bit 单个开关

	//MMW_Front_Cfg_Param 前方毫米波雷达 2B
//	USART1_Bebug_Print_Num("mmw_front low", data[index+16], 2, 1);
//	USART1_Bebug_Print_Num("mmw_front high", data[index+17], 2, 1);
	l_sensor_mgt.m_mmw_radar.m_front.m_main_sw = data[index+16] & 0x01;			// 1bit 单侧总开关
	l_sensor_mgt.m_mmw_radar.m_front.m_producer = (data[index+16] & 0x0E) >> 1;	// 3bit 厂家
	l_sensor_mgt.m_mmw_radar.m_front.m_sensor_num = (data[index+16] & 0x30) >> 4;	// 2bit 数目
	l_sensor_mgt.m_mmw_radar.m_front.m_rdr_sw = data[index+17] & 0x01;		// 1bit 单个开关

	//MMW_End_Cfg_Param 后方毫米波雷达 2B
//	USART1_Bebug_Print_Num("mmw_end low", data[index+18], 2, 1);
//	USART1_Bebug_Print_Num("mmw_end high", data[index+19], 2, 1);
	l_sensor_mgt.m_mmw_radar.m_end.m_main_sw = data[index+18] & 0x01;			// 1bit 单侧总开关
	l_sensor_mgt.m_mmw_radar.m_end.m_producer = (data[index+18] & 0x0E) >> 1;	// 3bit 厂家
	l_sensor_mgt.m_mmw_radar.m_end.m_sensor_num = (data[index+18] & 0x30) >> 4;	// 2bit 数目
	l_sensor_mgt.m_mmw_radar.m_end.m_rdr_sw = data[index+19] & 0x01 ;		// 1bit 单个开关

	//MMW_Left_Cfg_Param 左侧毫米波雷达 2B
//	USART1_Bebug_Print_Num("mmw_left low", data[index+20], 2, 1);
//	USART1_Bebug_Print_Num("mmw_left high", data[index+21], 2, 1);
	l_sensor_mgt.m_mmw_radar.m_left.m_main_sw = data[index+20] & 0x01;			// 1bit 单侧总开关
	l_sensor_mgt.m_mmw_radar.m_left.m_producer = (data[index+20] & 0x0E) >> 1;	// 3bit 厂家
	l_sensor_mgt.m_mmw_radar.m_left.m_sensor_num = (data[index+20] & 0x30) >> 4;	// 2bit 数目
	l_sensor_mgt.m_mmw_radar.m_left.m_rdr_sw = data[index+21] & 0x03;		// 2bit 2个开关

	//MMW_Right_Cfg_Param 右侧毫米波雷达 2B
//	USART1_Bebug_Print_Num("mmw_right low", data[index+22], 2, 1);
//	USART1_Bebug_Print_Num("mmw_right high", data[index+23], 2, 1);
	l_sensor_mgt.m_mmw_radar.m_right.m_main_sw = data[index+22] & 0x01;			// 1bit 单侧总开关
	l_sensor_mgt.m_mmw_radar.m_right.m_producer = (data[index+22] & 0x0E) >> 1;	// 3bit 厂家
	l_sensor_mgt.m_mmw_radar.m_right.m_sensor_num = (data[index+22] & 0x30) >> 4;	// 2bit 数目
	l_sensor_mgt.m_mmw_radar.m_right.m_rdr_sw = data[index+23] & 0x03;		// 2bit 2个开关

	//Imu_Cfg_Param 2B
//	USART1_Bebug_Print_Num("imu low", data[index+24], 2, 1);
//	USART1_Bebug_Print_Num("imu high", data[index+25], 2, 1);
	l_sensor_mgt.m_imu.m_isOpen = (data[index+24] & 0x80) >> 7;
	l_sensor_mgt.m_imu.m_producer = data[index+24] & 0x0F;

	//Car_DVR_Cfg_Param 2B
//	USART1_Bebug_Print_Num("car_dvr low", data[index+26], 2, 1);
//	USART1_Bebug_Print_Num("car_dvr high", data[index+27], 2, 1);
	l_sensor_mgt.m_car_dvr.m_isOpen = (data[index+26] & 0x80) >> 7;
	l_sensor_mgt.m_car_dvr.m_producer = data[index+26] & 0x0F;

	//Actuator_Cfg_Param 2B
//	USART1_Bebug_Print_Num("brk_act low", data[index+28], 2, 1);
//	USART1_Bebug_Print_Num("brk_act high", data[index+29], 2, 1);
	l_sensor_mgt.m_actuator.m_isOpen = (data[index+28] & 0x80) >> 7;
	l_sensor_mgt.m_actuator.m_producer = data[index+28] & 0x0F;

//	USART1_Bebug_Print_Num("project", data[index+30], 2, 1);
//	USART1_Bebug_Print_Num("product", data[index+31], 2, 1);
	l_sensor_mgt.m_project = data[index+30];		// 项目
	l_sensor_mgt.m_product = data[index+31];		// 产品类型

	// 角雷达demo开关
	l_sensor_mgt.veh_srr_demo_sw = data[index+32] & 0x01;
	srr_demo_sw = data[index+32] & 0x01;
//	Sensor_MGT m_sensor_mgt = {0};
//	gcz_serial_v1_dma_printf(SERIAL_UART1,"Analysis_Request_Set_Sensor_Cfg_Param_ASCII srr_demo_sw : %d \n",srr_demo_sw);

//	if(!Read_Sensor_Mgt_Cfg_Param(&m_sensor_mgt)){
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"Get srr_demo_sw Failed.");
//
//	}
//	m_sensor_mgt.veh_srr_demo_sw = srr_demo_sw;
//
//	// 写配置参数
//	if(!Set_Sensor_MGT_Param(m_sensor_mgt)){
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"Set srr_demo_sw Failed.");
//	}


#if 0 // 测试
	g_sensor_mgt = l_sensor_mgt;
//	Print_Sensor_Cfg_Param();

	return 1;
#else
	// 先做备份，若写失败就恢复
	Read_Sensor_Mgt_Cfg_Param(&g_sensor_mgt);
#endif
	// 写操作
	if(Write_Sensor_Mgt_Cfg_Param(l_sensor_mgt)){
		USART1_Bebug_Print("Write OK", "Write Sensor Config Parameter OK.", 1);
//		g_sensor_mgt = l_sensor_mgt;
		// 打印输出
//		Print_Sensor_Cfg_Param();
		return 1;
	}else{
		USART1_Bebug_Print("Write FAILED", "Write Sensor Config Parameter Failed.", 1);
		// 恢复
		Write_Sensor_Mgt_Cfg_Param(g_sensor_mgt);
	}

	return 0;
}

/*
 * 平台获取传感器配置参数
 */
uint8_t Reply_Platform_Request_Sensor_Cfg_Param_Info_ASCII(uint16_t seqNum)
{
	memset(&g_sensor_param_upload, 0, sizeof(Sensor_Param_Upload));

	uint16_t l_status_code = 0;
	Sensor_MGT l_sensor_mgt = {0};

	req_index = 33;
	is_reply = true;
	// 先获取传感器参数配置
	if(!Read_Sensor_Mgt_Cfg_Param(&l_sensor_mgt)){
		// 获取失败
		l_status_code = 0x5012;
		memset(&l_sensor_mgt, 0, sizeof(Sensor_MGT));
	}

	// 帧头 0xAA55 C2
	if(!Reply_Protocal_Head_ASCII(seqNum, GET_SENSOR_CFG_FRAME, l_status_code, g_sensor_param_upload.m_noda_data.data)){
		return 0;
	}

	char *l_cfg_param = NULL;

	//4G_Cfg_Param
	uint16_t l_4g_cfg_param = l_sensor_mgt.m_4g.m_isOpen<<7 | l_sensor_mgt.m_4g.m_producer;
	l_cfg_param = (uint8_t *)&l_4g_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Gps_Cfg_Param
	uint16_t l_gps_cfg_param = l_sensor_mgt.m_gps.m_isOpen<<7 | l_sensor_mgt.m_gps.m_producer;
	l_cfg_param = (uint8_t *)&l_gps_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Dbl_Cam_Cfg_Param
	uint16_t l_dbl_cam_cfg_param = l_sensor_mgt.m_dbl_cam.m_isOpen<<7 | l_sensor_mgt.m_dbl_cam.m_producer;
	l_cfg_param = (uint8_t *)&l_dbl_cam_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Sgl_Cam_Cfg_Param
	uint16_t l_sgl_cam_cfg_param = l_sensor_mgt.m_sgl_cam.m_isOpen<<7 | l_sensor_mgt.m_sgl_cam.m_producer;
	l_cfg_param = (uint8_t *)&l_sgl_cam_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	uint16_t l_u16_config_info = 0;
#if 1	// 超声波雷达配置参数
	// Ultra_Front_Cfg_Param 车方超声波雷达
	l_u16_config_info = l_sensor_mgt.m_ultra_radar.m_front.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_front.m_producer << 1;	// 3bit	厂家
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_front.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_front.m_controler << 7;	// 2bit 控制器编号
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw << 9;		// 6bit 单个开关
	l_u16_config_info &= 0x7FFF;												// 第15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// Ultra_End_Cfg_Param 后侧超声波雷达
	l_u16_config_info = 0;
	l_u16_config_info = l_sensor_mgt.m_ultra_radar.m_end.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_end.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_end.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_end.m_controler << 7;		// 2bit 控制器编号
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw << 9;		// 6bit 单个开关
	l_u16_config_info &= 0x7FFF;												// 第15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);


	// Ultra_Left_Cfg_Param 左侧超声波雷达
	l_u16_config_info = 0;
	l_u16_config_info = l_sensor_mgt.m_ultra_radar.m_left.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_left.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_left.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_left.m_controler << 7;	// 2bit 控制器编号
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw << 9;		// 6bit 单个开关
	l_u16_config_info &= 0x7FFF;												// 第15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// Ultra_Right_Cfg_Param 右侧超声波雷达
	l_u16_config_info = 0;
	l_u16_config_info = l_sensor_mgt.m_ultra_radar.m_right.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_right.m_producer << 1;	// 3bit	厂家
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_right.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_right.m_controler << 7;	// 2bit 控制器编号
	l_u16_config_info |= l_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw << 9;		// 6bit 单个开关
	l_u16_config_info &= 0x7FFF;												// 第15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);
#endif

#if 1	// 毫米波雷达配置参数
	// MMW_Front_Cfg_Param 车方毫米波雷达
	l_u16_config_info = 0;
	l_u16_config_info = l_sensor_mgt.m_mmw_radar.m_front.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_front.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_front.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info &= 0x003F;												// 第6~7bit 预留
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_front.m_rdr_sw << 8;		// 1bit 单个开关
	l_u16_config_info &= 0x01FF;												// 第9~15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// MMW_End_Cfg_Param 车方毫米波雷达
	l_u16_config_info = 0;
	l_u16_config_info = l_sensor_mgt.m_mmw_radar.m_end.m_main_sw & 0x01;			// 1bit 单侧总开关
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_end.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_end.m_sensor_num << 4;		// 3bit 雷达数目
	l_u16_config_info &= 0x003F;												// 第6~7bit 预留
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_end.m_rdr_sw << 8;			// 1bit 单个开关
	l_u16_config_info &= 0x01FF;												// 第9~15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// MMW_Left_Cfg_Param 车方毫米波雷达
	l_u16_config_info = 0;
	l_u16_config_info = l_sensor_mgt.m_mmw_radar.m_left.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_left.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_left.m_sensor_num << 4;		// 3bit 雷达数目
	l_u16_config_info &= 0x003F;												// 第6~7bit 预留
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_left.m_rdr_sw << 8;			// 2bit 单个开关
	l_u16_config_info &= 0x03FF;												// 第10~15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// MMW_Right_Cfg_Param 车方毫米波雷达
	l_u16_config_info = 0;
	l_u16_config_info = l_sensor_mgt.m_mmw_radar.m_right.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_right.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_right.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info &= 0x003F;												// 第6~7bit 预留
	l_u16_config_info |= l_sensor_mgt.m_mmw_radar.m_right.m_rdr_sw << 8;		// 2bit 单个开关
	l_u16_config_info &= 0x03FF;												// 第10~15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);
#endif

	//Imu_Cfg_Param
	uint16_t l_imu_cfg_param = l_sensor_mgt.m_imu.m_isOpen<<7 | l_sensor_mgt.m_imu.m_producer;
	l_cfg_param = (uint8_t *)&l_imu_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Car_DVR_Cfg_Param
	uint16_t l_car_dvr_cfg_param = l_sensor_mgt.m_car_dvr.m_isOpen<<7 | l_sensor_mgt.m_car_dvr.m_producer;
	l_cfg_param = (uint8_t *)&l_car_dvr_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Actuator_Cfg_Param
	uint16_t l_actuator_cfg_param = l_sensor_mgt.m_actuator.m_isOpen<<7 | l_sensor_mgt.m_actuator.m_producer;
	l_cfg_param = (uint8_t *)&l_actuator_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// 项目类型
	uint8_t type_param = l_sensor_mgt.m_project;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, &type_param, UINT_8, 1);

	// 产品类型
	type_param = l_sensor_mgt.m_product;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, &type_param, UINT_8, 1);

	// 角雷达demo开关
	uint8_t sw = (uint8_t) srr_demo_sw;

	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, &sw, UINT_8, 1);
	gcz_serial_v1_dma_printf(SERIAL_UART1,"Reply_Platform_Request_Sensor_Cfg_Param_Info_ASCII srr_demo_sw : %d \n",srr_demo_sw);
	// 校验
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, &type_param, CRC16, 2);

	// 放到队列中进行发送
	sprintf(g_sensor_param_upload.m_noda_data.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, g_sensor_param_upload.m_noda_data.data_len);
//	Test_Print_CMD_Seq(GET_SENSOR_CFG_FRAME, g_sensor_param_upload.m_seq);
	g_sensor_param_upload.m_noda_data.is_wait_reply = NEED_WAIT_REPLY_REPLY;
	g_sensor_param_upload.m_noda_data.is_store = NEED_STORED;
	g_sensor_param_upload.m_noda_data.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &g_sensor_param_upload.m_noda_data);

	// 测试打印
//	USART1_Bebug_Print("Start", "-------------------------------", 1);
//	uint8_t print[3] = {0};
//	for(uint16_t i=0; i<g_sensor_param_upload.m_noda_data.data_len;i++){
//		sprintf(print, "%02X ", g_sensor_param_upload.m_noda_data.data[i]);
//		Usart1_DMA_Transmit(print, 3);
//	}
//	sprintf(print, "\r\n");
//	Usart1_DMA_Transmit(print, 2);
//	USART1_Bebug_Print("Start", "-------------------------------", 1);

	return 1;
}
/*
 * 发送传感器配置参数
 */
uint8_t Request_Sensor_Cfg_Param_Info_ASCII()
{
	is_reply = false;
	// 填数
	if(!Request_Protocal_Head_ASCII(g_sensor_param_upload.m_seq, SET_SENSOR_CFG_FRAME, g_sensor_param_upload.m_noda_data.data)){
		return 0;
	}

	g_sensor_param_upload.m_seq++;
	req_index = 31;

	char *l_cfg_param = NULL;

	//4G_Cfg_Param
	uint16_t l_4g_cfg_param = g_sensor_mgt.m_4g.m_isOpen<<7 | g_sensor_mgt.m_4g.m_producer;
	l_cfg_param = (uint8_t *)&l_4g_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Gps_Cfg_Param
	uint16_t l_gps_cfg_param = g_sensor_mgt.m_gps.m_isOpen<<7 | g_sensor_mgt.m_gps.m_producer;
	l_cfg_param = (uint8_t *)&l_gps_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Dbl_Cam_Cfg_Param
	uint16_t l_dbl_cam_cfg_param = g_sensor_mgt.m_dbl_cam.m_isOpen<<7 | g_sensor_mgt.m_dbl_cam.m_producer;
	l_cfg_param = (uint8_t *)&l_dbl_cam_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Sgl_Cam_Cfg_Param
	uint16_t l_sgl_cam_cfg_param = g_sensor_mgt.m_sgl_cam.m_isOpen<<7 | g_sensor_mgt.m_sgl_cam.m_producer;
	l_cfg_param = (uint8_t *)&l_sgl_cam_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	uint16_t l_u16_config_info = 0;
#if 1	// 超声波雷达配置参数
	// Ultra_Front_Cfg_Param 车方超声波雷达
	l_u16_config_info = g_sensor_mgt.m_ultra_radar.m_front.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_front.m_producer << 1;	// 3bit	厂家
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_front.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_front.m_controler << 7;	// 2bit 控制器编号
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_front.m_rdr_sw << 9;		// 6bit 单个开关
	l_u16_config_info &= 0x7FFF;												// 第15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// Ultra_End_Cfg_Param 后侧超声波雷达
	l_u16_config_info = 0;
	l_u16_config_info = g_sensor_mgt.m_ultra_radar.m_end.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_end.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_end.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_end.m_controler << 7;		// 2bit 控制器编号
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_end.m_rdr_sw << 9;		// 6bit 单个开关
	l_u16_config_info &= 0x7FFF;												// 第15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);


	// Ultra_Left_Cfg_Param 左侧超声波雷达
	l_u16_config_info = 0;
	l_u16_config_info = g_sensor_mgt.m_ultra_radar.m_left.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_left.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_left.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_left.m_controler << 7;	// 2bit 控制器编号
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_left.m_rdr_sw << 9;		// 6bit 单个开关
	l_u16_config_info &= 0x7FFF;												// 第15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// Ultra_Right_Cfg_Param 右侧超声波雷达
	l_u16_config_info = 0;
	l_u16_config_info = g_sensor_mgt.m_ultra_radar.m_right.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_right.m_producer << 1;	// 3bit	厂家
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_right.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_right.m_controler << 7;	// 2bit 控制器编号
	l_u16_config_info |= g_sensor_mgt.m_ultra_radar.m_right.m_rdr_sw << 9;		// 6bit 单个开关
	l_u16_config_info &= 0x7FFF;												// 第15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);
#endif

#if 1	// 毫米波雷达配置参数
	// MMW_Front_Cfg_Param 车方毫米波雷达
	l_u16_config_info = 0;
	l_u16_config_info = g_sensor_mgt.m_mmw_radar.m_front.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_front.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_front.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info &= 0x003F;												// 第6~7bit 预留
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_front.m_rdr_sw << 8;		// 1bit 单个开关
	l_u16_config_info &= 0x01FF;												// 第9~15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// MMW_End_Cfg_Param 车方毫米波雷达
	l_u16_config_info = 0;
	l_u16_config_info = g_sensor_mgt.m_mmw_radar.m_end.m_main_sw & 0x01;			// 1bit 单侧总开关
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_end.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_end.m_sensor_num << 4;		// 3bit 雷达数目
	l_u16_config_info &= 0x003F;												// 第6~7bit 预留
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_end.m_rdr_sw << 8;			// 1bit 单个开关
	l_u16_config_info &= 0x01FF;												// 第9~15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// MMW_Left_Cfg_Param 车方毫米波雷达
	l_u16_config_info = 0;
	l_u16_config_info = g_sensor_mgt.m_mmw_radar.m_left.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_left.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_left.m_sensor_num << 4;		// 3bit 雷达数目
	l_u16_config_info &= 0x003F;												// 第6~7bit 预留
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_left.m_rdr_sw << 8;			// 2bit 单个开关
	l_u16_config_info &= 0x03FF;												// 第10~15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// MMW_Right_Cfg_Param 车方毫米波雷达
	l_u16_config_info = 0;
	l_u16_config_info = g_sensor_mgt.m_mmw_radar.m_right.m_main_sw & 0x01;		// 1bit 单侧总开关
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_right.m_producer << 1;		// 3bit	厂家
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_right.m_sensor_num << 4;	// 3bit 雷达数目
	l_u16_config_info &= 0x003F;												// 第6~7bit 预留
	l_u16_config_info |= g_sensor_mgt.m_mmw_radar.m_right.m_rdr_sw << 8;		// 2bit 单个开关
	l_u16_config_info &= 0x03FF;												// 第10~15bit 预留
	l_cfg_param = (uint8_t *)&l_u16_config_info;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);
#endif

	//Imu_Cfg_Param
	uint16_t l_imu_cfg_param = g_sensor_mgt.m_imu.m_isOpen<<7 | g_sensor_mgt.m_imu.m_producer;
	l_cfg_param = (uint8_t *)&l_imu_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Car_DVR_Cfg_Param
	uint16_t l_car_dvr_cfg_param = g_sensor_mgt.m_car_dvr.m_isOpen<<7 | g_sensor_mgt.m_car_dvr.m_producer;
	l_cfg_param = (uint8_t *)&l_car_dvr_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	//Actuator_Cfg_Param
	uint16_t l_actuator_cfg_param = g_sensor_mgt.m_actuator.m_isOpen<<7 | g_sensor_mgt.m_actuator.m_producer;
	l_cfg_param = (uint8_t *)&l_actuator_cfg_param;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, l_cfg_param, UINT_16, 2);

	// 项目类型
	uint8_t type_param = g_sensor_mgt.m_project;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, &type_param, UINT_8, 1);

	// 产品类型
	type_param = g_sensor_mgt.m_product;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, &type_param, UINT_8, 1);
	// 角雷达demo开关
	uint8_t sw = (uint8_t) srr_demo_sw;
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, &sw, UINT_8, 1);


	// 校验
	Set_Request_ASCII_Info_Template(&g_sensor_param_upload.m_noda_data, &type_param, CRC16, 2);

	// 放到队列中进行发送
	sprintf(g_sensor_param_upload.m_noda_data.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, g_sensor_param_upload.m_noda_data.data_len);
//	Test_Print_CMD_Seq(SET_SENSOR_CFG_FRAME, g_sensor_param_upload.m_seq);
	g_sensor_param_upload.m_noda_data.is_wait_reply = NEED_WAIT_REPLY;
	g_sensor_param_upload.m_noda_data.is_store = NEED_STORED;
	g_sensor_param_upload.m_noda_data.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &g_sensor_param_upload.m_noda_data);

	// 测试打印
//	USART1_Bebug_Print("Start", "-------------------------------", 1);
//	uint8_t print[3] = {0};
//	for(uint16_t i=0; i<g_sensor_param_upload.m_noda_data.data_len;i++){
//		sprintf(print, "%02X ", g_sensor_param_upload.m_noda_data.data[i]);
//		Usart1_DMA_Transmit(print, 3);
//	}
//	sprintf(print, "\r\n");
//	Usart1_DMA_Transmit(print, 2);
//	USART1_Bebug_Print("Start", "-------------------------------", 1);
//
//	Print_Sensor_Cfg_Param();

	return 1;
}
