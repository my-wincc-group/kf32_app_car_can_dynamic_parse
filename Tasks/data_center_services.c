/*
 * data_center_services.c
 *
 *  Created on: 2021-7-15
 *      Author: shuai
 */
#include "data_center_services.h"
#include "aeb_cms_sss_para_config.h"
#include "kunlun_fcw_ldw.h"
#include "common.h"
#include "aeb_sensor_management.h"
#include "tool.h"
Camera_Info camera_share;
extern uint8_t srrMalfunction_;
void Camera_Info_Init()
{
	camera_share.CammeraEssentialData.LeftLDW 		= 0;
	camera_share.CammeraEssentialData.RightLDW 		= 0;
	camera_share.CammeraEssentialData.LDWSentivity 	= 0;
	camera_share.CammeraEssentialData.Invalid 		= 0;
	camera_share.CammeraEssentialData.OffSound 		= 0;
	camera_share.CammeraEssentialData.HMWGrade 		= 0;
	camera_share.CammeraEssentialData.HMW 			= 6.3;
	camera_share.CammeraEssentialData.ErrorCode 	= 0;
	camera_share.CammeraEssentialData.HMWEnable 	= 0;
	camera_share.CammeraEssentialData.FCWLevel 		= 0;
	camera_share.CammeraEssentialData.AmbientLuminance = 0;
	camera_share.CammeraEssentialData.FCWStatus 	= 0;

	camera_share.ObsBasicData.ObstacleNumber 		= 0;
	camera_share.ObsBasicData.HMWAlarmThreshold 	= 3.1;

	camera_share.ObsInormation.ObstacleID 			= 0;
	camera_share.ObsInormation.TrackNumber 			= 0;
	camera_share.ObsInormation.ObstacleWidth 		= 40.95;
	camera_share.ObsInormation.ObstacleHeight 		= 40.95;
	camera_share.ObsInormation.RelativeSpeedZ 		= 128.0;
	camera_share.ObsInormation.DistanceY 			= 20.47;
	camera_share.ObsInormation.DistanceX 			= 163.83;
	camera_share.ObsInormation.DistanceZ 			= 327.67;
	camera_share.ObsInormation.TTC 					= 6.3;
	camera_share.ObsInormation.HMW 					= 6.3;
	camera_share.ObsInormation.RelativeSpeedX 		= 51.1;
	camera_share.ObsInormation.CIPV 				= 0;
	camera_share.ObsInormation.ObstacleType 		= 0;
}

//void Device_Status_Init()
//{
//	dev_status.FrontCamera = 0;
//	dev_status.MillimeterWaveRadar = 0;
//	dev_status.UltrasonicRadar = 0;
//	dev_status.Valve = 0;
//	dev_status.VehicleCANBus = 0;
//	dev_status.VehicleSpeed = 0;
//	dev_status.Module4G = 0;
//	dev_status.GPS = 0;
//	dev_status.AEBSTask = 2;
//	dev_status.FCWTask = 2;
//	dev_status.LDWTask = 2;
//	dev_status.SSSTask = 2;
//	dev_status.CMSTask = 2;
//}

//void Data_Center_Task(void *pvParameters)
//{
//	while (1)
//	{
//		if( xSemaphoreTakeRecursive( data_mutex, ( TickType_t ) 10 ) == pdTRUE )
//		{
//			if(camera_share.CameraInfoFlag==1){
//				camera_share.CameraInfoFlag=0;
//			}else{
//				Camera_Info_Init();
//			}
//
//			if(dev_status.DeviceStatusFlag==1){
//				dev_status.DeviceStatusFlag=0;
//			}else{
//				Device_Status_Init();
//			}
//			xSemaphoreGiveRecursive( data_mutex );
//		}
//		vTaskDelay(300);
//	}
//}

void Camera_Info_Push(Camera_Essential_Data *camera_data, Obstacle_Basic_Data *obs_basic, Obstacle_Information *obs_info)
{
	camera_share.CammeraEssentialData = *camera_data;
	camera_share.ObsBasicData = *obs_basic;
	camera_share.ObsInormation = *obs_info;
	camera_share.CameraInfoFlag = 1;
}

//----------------------------------gcz  2022-06-01--------------------
enum
{
	LDW_CLOSE = 0,
	LDW_OPEN,
	LDW_OPEN_UNREMITTING,
	LDW_WAIT_CLOSE,
};
//判断LDW左线偏离预警动作
static bool check_LDW_left_warning(uint8_t ldw_val)
{
	bool res = TRUE;
	static uint8_t m_left_ldw_state = LDW_CLOSE;
	static int64_t cur_time_left = 0;
	switch (m_left_ldw_state)
	{
	case LDW_CLOSE:  				if (ldw_val == 1)
									{
										m_left_ldw_state = LDW_OPEN;
									//	fprintf(USART1_STREAM, "1.LEFT LDW OPEN\r\n");
									}
									else
										res = FALSE;
									break;
	case LDW_OPEN:					if (ldw_val != 1)
									{
										m_left_ldw_state = LDW_WAIT_CLOSE;
										cur_time_left = SystemtimeClock;
									//	fprintf(USART1_STREAM, "2.LEFT LDW ready to WAIT CLOSE   %ld\r\n",cur_time_left);
									}
									break;
	case LDW_WAIT_CLOSE:			if ((SystemtimeClock - cur_time_left) < 1000)
									{
										if (ldw_val == 1)
										{
											cur_time_left = SystemtimeClock;
										}
									}
									else
									{
										res = FALSE;
										m_left_ldw_state = LDW_CLOSE;
									//	fprintf(USART1_STREAM, "3.LEFT LDW CLOSE %ld %ld\r\n",cur_time_left,SystemtimeClock);
									}
									break;
	default:	res = FALSE;
				m_left_ldw_state = LDW_CLOSE;break;
	}
	return res;
}
//判断LDW左线偏离预警动作
static bool check_LDW_right_warning(uint8_t ldw_val)
{
	bool res = TRUE;
	static uint8_t m_right_ldw_state = LDW_CLOSE;
	static int64_t cur_time_right = 0;

	switch (m_right_ldw_state)
	{
	case LDW_CLOSE:  				if (ldw_val == 2)
									{
										m_right_ldw_state = LDW_OPEN;
									}
									else
										res = FALSE;
									break;
	case LDW_OPEN:					if (ldw_val != 2)
									{
										m_right_ldw_state = LDW_WAIT_CLOSE;
										cur_time_right = SystemtimeClock;
									}
									break;
	case LDW_WAIT_CLOSE:			if ((SystemtimeClock - cur_time_right) < 1000)
									{
										if (ldw_val == 2)
										{
											cur_time_right = SystemtimeClock;
										}
									}
									else
									{
										res = FALSE;
										m_right_ldw_state = LDW_CLOSE;
									}
									break;
	default:	res = FALSE;
				m_right_ldw_state = LDW_CLOSE;break;
	}
	return res;
}
void Displayer_Pull(Displayer_Show *show_info)
{

	if(stCanCommSta.stCamera.status == OFFLINE)
	{
		Camera_Info_Init();
	}
//	if(warning_status.AEBstatus != 0)
//		dev_status.AEBSTask = 0;
//	else
//		dev_status.AEBSTask = 2;
	if(g_LDW_Enable == 0x00)
	{
		camera_share.CammeraEssentialData.LeftLDW = 0;
		camera_share.CammeraEssentialData.RightLDW = 0;
	}

	// independent set status
//	dev_status.GPS = gps_info.isConnect;
//	dev_status.AEBSTask = g_AEB_CMS_Enable;
//	dev_status.LDWTask = g_LDW_Enable;
//	dev_status.SSSTask = g_SSS_Enable;

	//gcz 2022-05-16
	//相机离线或者在线时输出4遮挡，则小屏幕显示未就绪
	if ((stCanCommSta.stCamera.status == OFFLINE) || (camera_data.AmbientLuminance == 4))
		show_info->FrontCameraStatus = 0;
	else
		show_info->FrontCameraStatus = 1;
	//show_info->FrontCameraStatus = stCanCommSta.stCamera.status;//dev_status.FrontCamera;
	show_info->MillimeterWaveRadarStatus = stCanCommSta.stRadar.status | stCanCommSta.stSrr.status;//dev_status.MillimeterWaveRadar;
	if(srrMalfunction_>0)
	{
		show_info->MillimeterWaveRadarStatus = 0;
	}
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
//	if (m_sensor_mgt.m_ultra_radar.m_isOpen == 1)
	if (m_sensor_mgt.m_ultra_radar.m_front.m_main_sw == 1 \
			|| m_sensor_mgt.m_ultra_radar.m_end.m_main_sw == 1 \
			|| m_sensor_mgt.m_ultra_radar.m_left.m_main_sw == 1 \
			|| m_sensor_mgt.m_ultra_radar.m_right.m_main_sw == 1 )
		show_info->UltrasonicRadarStatus = stCanCommSta.stHRadar.status;//dev_status.UltrasonicRadar;
	else
		show_info->UltrasonicRadarStatus = 0;
	show_info->ValveStatus = stCanCommSta.Proportional_valve.status;//dev_status.Valve;
	show_info->VehicleCANBusStatus = stCanCommSta.stVehicle.status;//dev_status.VehicleCANBus;
	if(rParms.Vehicle_Speed_Obtain_Method == 0x01 && stVehicleParas.fVehicleSpeed > 0.0){
		show_info->VehicleSpeedStatus = 0x01;//dev_status.VehicleSpeed;
	}else{
		show_info->VehicleSpeedStatus = stCanCommSta.stSpeed.status;//dev_status.VehicleSpeed;
	}

	switch(server_info.vehicle_type){
		case JINLV_BUS:
			break;
		case BYD_E6:
			show_info->ValveStatus = stCanCommSta.stVehicle.status;//dev_status.VehicleCANBus;
			break;
		case JIEFANG_TRUCK:
			break;
		default:
			break;
	}

	show_info->Module4GStatus 	= stCanCommSta.stWireless.status;//dev_status.Module4G;
	show_info->GPSStatus 		= gps_info.isConnect;
	show_info->AEBSTaskStatus 	= g_AEB_CMS_Enable;
//	show_info->FCWTaskStatus = dev_status.FCWTask;
	show_info->LDWTaskStatus 	= g_LDW_Enable;
	show_info->SSSTaskStatus 	= g_SSS_Enable;//dev_status.SSSTask;
//	show_info->CMSTaskStatus = dev_status.CMSTask;
	show_info->FCWLevel 		= camera_share.CammeraEssentialData.FCWLevel;

	show_info->LDW = LDW_Lane_Departure_Warning(&stVehicleParas,camera_share,camera_LDW_data);
	//----------------gcz 2022-06-01 过滤小屏幕报警次数----------------------
//	gcz_serial_v1_dma_printf(SERIAL_UART1,"2.%d\r\n",show_info->LDW);
	uint8_t left_ldw = 0,right_ldw = 0;;
	left_ldw = (check_LDW_left_warning(show_info->LDW) == true) ? 1 : 0;
	right_ldw = (check_LDW_right_warning(show_info->LDW) == true) ? 2 : 0;
//	gcz_serial_v1_dma_printf(SERIAL_UART1,"3.%d %d\r\n",left_ldw,right_ldw);
	if ((left_ldw != 0) && (right_ldw != 0))
	{
		;//突然间从左跳到右或从右跳到左时，不采用过滤后的值，采用原值
	}
	else//只对无跳方向的单一值进行过滤处理
	{
		if (left_ldw != 0)
			show_info->LDW = left_ldw;
		if (right_ldw != 0)
			show_info->LDW = right_ldw;
	}
//	gcz_serial_v1_dma_printf(SERIAL_UART1,"4.%d\r\n",show_info->LDW);
	//---------------------------------------------------------------------
	//show the Lane all the time
	if(show_info->LDW != 0x01 && show_info->LDW != 0x02)
		show_info->LDW = 0x03;


	show_info->HMWGrade 		= camera_share.CammeraEssentialData.HMWGrade;
	show_info->DigitalDisplay 	= 3;//之后需要在配置信息中获取。
	show_info->HMWTime 			= camera_share.ObsInormation.HMW;

	if(show_info->HMWTime > 2.75)
		show_info->HMWTime = 6.3;

	show_info->LengthwaysRelativeSpeed 	= camera_share.ObsInormation.RelativeSpeedZ;
	show_info->TTC 						= camera_share.ObsInormation.TTC;
	show_info->LengthwaysDistance 		= camera_share.ObsInormation.DistanceZ;
	show_info->ObstacleType 			= camera_share.ObsInormation.ObstacleType;
}

