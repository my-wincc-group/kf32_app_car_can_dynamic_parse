/*
 * data_loss_prevention.h
 *
 *  Created on: 2022-6-28
 *      Author: Administrator
 */

#ifndef DATA_LOSS_PREVENTION_H_
#define DATA_LOSS_PREVENTION_H_

#include "cqueue.h"

/* ----------------------- 结构体和宏定义 ------------------------------- */
enum{
	SEND_NO,
	SEND_DATA_INFO,
	SEND_DATA,
	QUERY_STATUS,
};

typedef struct _Upload_Data_Queue_Info{
	volatile uint8_t 	have_one_data;	// 1正在发送一条数据；0没有发送
	volatile uint16_t 	wait_times;		// 等待时间（1秒）
	volatile uint8_t 	step;			// 0发送data_info;1发送data
	volatile uint8_t 	reconnect_server_flag;	// 重连服务器标志
	volatile uint8_t   	data_wait_time;	// 发送data时等待时间
	volatile uint8_t   	send_times;   // 发送次数，超出后就重连服务器
}Upload_Data_Queue_Info;

/* ----------------------- 全局变量 ------------------------------- */
extern Upload_Data_Queue_Info 	upload_data_queue_info;
extern volatile uint8_t 		_4g_queue_state;
/* ----------------------- 函数声明 ------------------------------- */
void 	Data_Loss_Prevention_Init();
bool 	Get_One_Data_Loss_Prevention(NodeData_S *nData);
bool 	Set_One_Data_Loss_Prevention( NodeData_S nData);
void 	Set_Queue_In_External_Flash_Data_Front_End_Value(uint8_t *id_s);
void 	Upload_Data_To_Platform_By_Queue();
bool 	_4G_Recv_Data_Upload_Message_From_Platform_Reply(uint8_t *string);

#endif /* DATA_LOSS_PREVENTION_H_ */
