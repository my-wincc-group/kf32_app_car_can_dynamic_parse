/*
 * car_can_analysis.h
 *
 *  Created on: 2022-9-26
 *      Author: Administrator
 *  设计目的：通过平台（上位机或小程序）对解析CAN数据的配置，可实现设备对车身CAN数据的解析，做到动态捕获有效数据的目的
 */

#ifndef CAR_CAN_ANALYSIS_H_
#define CAR_CAN_ANALYSIS_H_
#include "system_init.h"
#include "stdbool.h"

#define CAR_CAN_ANALY_IS_ENABLE		1	// 车CAN动态解析功能【0：关闭】【1：开启】
// 车CAN动态配置功能是否启用
enum {
	CAR_CAN_UNUSED,	// 不使用 或 未破解
	CAR_CAN_USED,	// 使用（使能，肯定被破解了）
};
// 车CAN动态字节顺序
enum {
	CAN_INTEL = 0,	// 小端模式
	CAR_MOTOROLA,	// 大端模式
};

// 功能宏定义
enum {
	CAR_CAN_ANALY_FUNC_NO,
	CAR_CAN_ANALY_CAR_SPEED,
	CAR_CAN_ANALY_TURN_LEFT,
	CAR_CAN_ANALY_TURN_RIGHT,
	CAR_CAN_ANALY_DRIVER_BRAKE,
	CAR_CAN_ANALY_GEAR,
	CAR_CAN_ANALY_ODOM,
	CAR_CAN_ANALY_STEER_WHEEL,
	CAR_CAN_ANALY_THROTTAL_DEPTH,
	CAR_CAN_ANALY_BRAKE_DEPTH,
	CAR_CAN_ANALY_YAW_ANGLE,
	CAR_CAN_ANALY_ENGINE_SPEED,
	CAR_CAN_ANALY_PITCH_ANGLE,
	CAR_CAN_ANALY_ACCELERATION,
};

// 对CAN解析特定变量的定义（vector table）
// 例如，解析档位信号时，【1：P档】【2：R档】【3：N档】【4：D档】，其中这里的1/2/3/4就是vector table，表示（属性定义-汉字）就是协议中定义的内容
typedef struct _Analysis_Set_Value_Str
{
	// 如下配置参数根据实际进行配置
	uint8_t value1;
	uint8_t value2;
	uint8_t value3;
	uint8_t value4;
}Analysis_Set_Value_Str;
/*
 * 车身CAN动态解析（配置）结构体
 */
typedef struct _Car_Can_Dymc_Cfg_Str
{
	uint16_t config_info;	// 包括是否使用、字节顺序、单位|值定义、预留
	uint32_t frame_id;		// 帧ID
	uint8_t start_bit;		// bit中起始地址
	uint8_t bit_count;		// 有效bit长度
	float factor;			// 比例因数
	float offset;			// 补偿值
	float min;				// 最小值
	float max;				// 最大值
	uint32_t invalid;		// 无效值（无效值使能位（最高bit-31bit）及无效值数据）
}Car_Can_Dymc_Cfg_Str;

/*
 * 车身CAN解析对象结构体
 */
typedef struct _Car_Can_Object_Str
{
	// 顺序不能变，因为涉及到初始化加载默认参数值
	Car_Can_Dymc_Cfg_Str car_speed;				// 车速
	Car_Can_Dymc_Cfg_Str car_turn_left;			// 车左转向
	Car_Can_Dymc_Cfg_Str car_turn_right;		// 车右转向
	Car_Can_Dymc_Cfg_Str car_driver_brake;		// 司机刹车
	Car_Can_Dymc_Cfg_Str car_gear;				// 档位
	Car_Can_Dymc_Cfg_Str car_odom;				// 里程数
	Car_Can_Dymc_Cfg_Str car_steer_wheel_angle;	// 方向转角
	Car_Can_Dymc_Cfg_Str car_throttle_depth;	// 油门深度
	Car_Can_Dymc_Cfg_Str car_brake_depth;		// 刹车深度
	Car_Can_Dymc_Cfg_Str car_yaw_angle;			// 偏航角
	Car_Can_Dymc_Cfg_Str car_engine_speed;		// 发动机转速
	Car_Can_Dymc_Cfg_Str car_pitch_angle;		// 俯仰角
	Car_Can_Dymc_Cfg_Str car_accelerated_speed;	// 加速度
}Car_Can_Object_Str;


//拟制结构体进行赋值
typedef struct _Car_CAN_Cfg_Param_Template
{
	uint8_t is_used;
	uint8_t byte_order;
	uint32_t frame_id;
	uint8_t start_bit;
	uint8_t bit_count;
	float factor;
	float offset;
	float min;
	float max;

	Analysis_Set_Value_Str set_value;
	uint8_t set_value_num;
	uint32_t invalid;
}Car_CAN_Cfg_Param_Template;


bool Request_Get_Car_Can_Analysis_Cfg_Param_Info_ASCII();
bool Reply_Platform_Request_Car_Can_Analysis_Cfg_Param_Info_ASCII(uint16_t seq_num);
bool Analysis_From_Platform_Car_Can_Analysis_Info(uint8_t *data, uint16_t data_len);

Car_Can_Object_Str *Get_Car_Can_Analysis_Cfg_Param();

void Debug_Print_Car_Can_Analysis_Info();
bool Car_Can_Analysis_Init();

float Car_Can_Decode_Signal_Data(uint8_t signal_id, uint8_t *p_data);

#endif /* CAR_CAN_ANALYSIS_H_ */
