/*
 * can_task.c
 *
 *  Created on: 2021-6-17
 *      Author: shuai
 */
#include "can_task.h"
#include "stdio.h"
#include "canhl.h"
#include "usart.h"
#include "gpio.h"
#include "flash.h"
#include "AEB_CMS_alg.h"
#include "aebs_version_management.h"
#include "stereo_camera.h"
#include "AEB_upload_info.h"
#include "aeb_sensor_management.h"
#include "srr_can.h"
#include "wt_jy901_imu.h"
#include "board_config.h"
#include "tool.h"
#include "srr.h"
#include "imu.h"
#include "car_can_analysis.h"
//uint8_t 	SN[21] = {"1.0.0.S1C4"};
//uint8_t		SN_Display[3];

uint8_t CAN0_BUS_OFF_Count = 0;
uint8_t CAN1_BUS_OFF_Count = 0;
uint8_t CAN2_BUS_OFF_Count = 0;
uint8_t CAN3_BUS_OFF_Count = 0;
uint8_t CAN4_BUS_OFF_Count = 0;
uint8_t CAN5_BUS_OFF_Count = 0;
//gcz 2022-08-10
uint8_t CAN5_BUS_OFF_SRR_Count = 0;

uint32_t CAN0_BUS_ON_TimeStamp = 0;
uint32_t CAN1_BUS_ON_TimeStamp = 0;
uint32_t CAN2_BUS_ON_TimeStamp = 0;
uint32_t CAN3_BUS_ON_TimeStamp = 0;
uint32_t CAN4_BUS_ON_TimeStamp = 0;
uint32_t CAN5_BUS_ON_TimeStamp = 0;
//gcz 2022-08-10
uint32_t CAN5_BUS_ON_SRR_TimeStamp = 0;

static _STRUCT_VEHICLE_MESSAGE Proportional_valve_status;
static _STRUCT_VEHICLE_MESSAGE Proportional_valve_fault;
//__SET_PARA parameterset;
// BYD
struct can_frame  stVehicleCanSpeedDataBYD;
struct can_frame  stVehicleCanStateDataBYD;
struct can_frame  stVehicleCanGearDataBYD;

struct can_frame  stVehicleCanData;
struct can_frame  stVehicleCanSpeedData;	// 车速
struct can_frame  stVehicleCanOdomData;		// 总里程计
struct can_frame  stVehicleCanTurnData;
struct can_frame  stVehicleCanTurnData1;
struct can_frame  stVehicleCanBreakData;
struct can_frame	stVehicleCanAbsData;
struct can_frame	stVehicleCanWarningData;
struct can_frame	stVehicleCanSWAData;	// 方向盘转角
//struct can_frame	stVehicleCanAbsData;
struct can_frame	stCammeraCanData;
struct can_frame	stUraderCanData[4];		//超声波雷达
struct can_frame	stToolingCanData[6];	//工装测试CAN
struct can_frame  stVehicleCanSpeedData_JF1939;

static _STRUCT_VALVE_PARA    stValveParas;//比例阀反馈数据
_STRUCT_CAN_COMM_STA 	stCanCommSta;

static uint8_t ucFlagSpeedJumpErr = 0;   /* 记录车速跳变 */
static uint8_t ucFlagSpeedErr = 0;       /* 记录车速异常 */
static uint8_t ucFlagBrakePedalErr = 0;  /* 记录刹车踏板异常 */

extern Sensor_MGT g_sensor_mgt;

float g_f_srr_fVehicleSpeed;
//extern uint8_t g_AEB_ON_OFF_Displayer;
//extern uint8_t g_LDW_ON_OFF_Displayer;

#if CAR_CAN_ANALY_IS_ENABLE			// add lmz 20221017
	CAR_CAN_Parse_Handle g_cc_ph;

	void Car_Can_Dynamic_Parse_In_Can2_Recv(struct can_frame rx_frame);
	void Car_Can_Dynamic_Parse_In_Vehicle_Parameter_Analysis(uint32_t ulSysTime);
#endif

void Set_Displayer_Stamp(uint32_t stamp);

void Camera_Can_Data(struct can_frame *rx_frame)
{
	if(rx_frame->flgBoxRxEnd == 1)
	{
		rx_frame->flgBoxRxEnd = 0;
		//stCanCommSta.stCamera.oldSysTime = SystemtimeClock;
		Set_Camera_Stamp(SystemtimeClock);
		Camera_CAN_Analysis(rx_frame);
		CAN_Analysis_Camera_Version(rx_frame);
	}
}

/*
 * 摄像头数据解析
 * */
void can0_receive(struct can_frame rx_frame)
{
	uint8_t i;

	if((rx_frame.TargetID == 0x79F)|
			(rx_frame.TargetID == 0x7A0)|
			(rx_frame.TargetID == 0x7A1)|
			(rx_frame.TargetID == 0x7A2)|
			(rx_frame.TargetID == 0x7A3)|
			(rx_frame.TargetID == 0x7A4)|
			(rx_frame.TargetID == 0x7A5)|
			(rx_frame.TargetID == 0x7A6)|
			(rx_frame.TargetID == 0x7A7)|
			(rx_frame.TargetID == 0x79E)/*gcz 2022-05-30新增SN*/)
	{
		stCammeraCanData.flgBoxRxEnd = 1;
		stCammeraCanData.TargetID = rx_frame.TargetID;
		for(i = 0;i < 8;i ++)
			stCammeraCanData.data[i] = rx_frame.data[i];
		/*if(rx_frame.TargetID == 0x7A0){
			fprintf(USART1_STREAM,"%d\r\n",rx_frame.data[1]);

		}*/

		//fprintf(USART1_STREAM, "--\r\n");
		Camera_Can_Data(&stCammeraCanData);
	}
	else if(rx_frame.TargetID == 0x317)// yuhong 0x317 for the tooling test
	{
		stToolingCanData[0].TargetID = rx_frame.TargetID;
		stToolingCanData[0].flgBoxRxEnd = 1;
		//fprintf(USART1_STREAM,"CAN0 Got ID [%d]\r\n",stToolingCanData[0].TargetID);
		for(i = 0;i < 8;i ++)
		{
			stToolingCanData[0].data[i] = rx_frame.data[i];
		}
	}
}

void Ureader_receive(struct can_frame rx_frame)
{
	uint8_t i;

	if((rx_frame.TargetID == 0x700) |
			(rx_frame.TargetID == 0x701) |
			(rx_frame.TargetID == 0x702) |
			(rx_frame.TargetID == 0x703))
	{
		stCanCommSta.stHRadar.oldSysTime = SystemtimeClock;
		if(rx_frame.TargetID == 0x700)
		{
			stUraderCanData[0].flgBoxRxEnd = 1;
			stUraderCanData[0].TargetID = rx_frame.TargetID;
			for(i = 0;i < 8;i ++)
				stUraderCanData[0].data[i] = rx_frame.data[i];

			g_check_ult_radar_exist_hearbeat_cnt[ID_700] = 1;
			g_upload_info_check_ult_radar_exist_hearbeat_cnt[ID_700] = 1;
		}
		if(rx_frame.TargetID == 0x701)
		{
			stUraderCanData[1].flgBoxRxEnd = 1;
			stUraderCanData[1].TargetID = rx_frame.TargetID;
			for(i = 0;i < 8;i ++)
				stUraderCanData[1].data[i] = rx_frame.data[i];

			g_check_ult_radar_exist_hearbeat_cnt[ID_701] = 1;
			g_upload_info_check_ult_radar_exist_hearbeat_cnt[ID_701] = 1;
		}
		if(rx_frame.TargetID == 0x702)
		{
			stUraderCanData[2].flgBoxRxEnd = 1;
			stUraderCanData[2].TargetID = rx_frame.TargetID;
			for(i = 0;i < 8;i ++)
				stUraderCanData[2].data[i] = rx_frame.data[i];
		}
		if(rx_frame.TargetID == 0x703)
		{
			stUraderCanData[3].flgBoxRxEnd = 1;
			stUraderCanData[3].TargetID = rx_frame.TargetID;
			for(i = 0;i < 8;i ++)
				stUraderCanData[3].data[i] = rx_frame.data[i];
		}
		if(rx_frame.TargetID == 0x700){
			CAN_Analysis_URadar_Version(&rx_frame);
		}
	}
	else if(rx_frame.TargetID == 0x317)//yuhong 0x317 for the tooling test
	{
		fprintf(USART1_STREAM,"%s>>Get the CAN1 Data is 0x%x.\r\n",__FUNCTION__,rx_frame.data[0]);
		stToolingCanData[1].TargetID = rx_frame.TargetID;
		stToolingCanData[1].flgBoxRxEnd = 1;
		for(i = 0;i < 8;i ++)
		{
			stToolingCanData[1].data[i] = rx_frame.data[i];
		}
	}
	else if(rx_frame.TargetID == 0x318)//yuhong 0x318 for the tooling test
	{
		fprintf(USART1_STREAM,"%s>>Get the CAN5 Data is 0x%x.\r\n",__FUNCTION__,rx_frame.data[0]);
		stToolingCanData[5].TargetID = rx_frame.TargetID;
		stToolingCanData[5].flgBoxRxEnd = 1;
		for(i = 0;i < 8;i ++)
		{
			stToolingCanData[5].data[i] = rx_frame.data[i];
		}
	}
}
//gcz 2022-08-10
void srr_receive(struct can_frame *rx_frame)
{
	stCanCommSta.stSrr.oldSysTime = SystemtimeClock;
	receiveSrrCanInfo(rx_frame);
}
/*
 * 函数名称：can2_receive
 * 函数功能：can2收数据
 * 输入：struct can_frame rx_frame
 * 输出：无
 * */
void can2_receive(struct can_frame rx_frame) // WHY don't use "swith()"????
{
#if CAR_CAN_ANALY_IS_ENABLE
		Car_Can_Dynamic_Parse_In_Can2_Recv(rx_frame);			// add lmz 20221019
#endif

	uint8_t i;
	if((rx_frame.TargetID == 0x18A70017) ||			//金旅转向
			(rx_frame.TargetID == 0x0CFE6CEE) ||			//金旅车速
			(rx_frame.TargetID == 0x18FEC1EE) ||			//金旅里程计
			(rx_frame.TargetID == 0x1C01F002) ||			//BYD Speed
			(rx_frame.TargetID == 0x1C01F019) ||			//BYD
			(rx_frame.TargetID == 0x1C01F004) ||			//BYD State
			(rx_frame.TargetID == 0x1C01F020) ||
			(rx_frame.TargetID == 0x1C01F021) ||
			(rx_frame.TargetID == 0x1C01F022)	||
			(rx_frame.TargetID == 0x1C01F023)	||
			(rx_frame.TargetID == 0x310)	||			//vv6转向信息
			(rx_frame.TargetID == 0x235)	||			//vv6拐角信息
			(rx_frame.TargetID == 0x240)	||			//vv6车速信息
			(rx_frame.TargetID == 0x310)	||			//vv6刹车信息
			(rx_frame.TargetID == 0x237)
			|| (rx_frame.TargetID == 0x18FEF100)		//解放gcz 2022-08-19
			)
	{
		//stVehicleCanData.flgBoxRxEnd = 1;
		//if(stSpeedPara.lId == stTurnPara.lId)
#if 	(CAR_CAN_ANALY_IS_ENABLE != 1)
		if (server_info.vehicle_type == JIEFANG_TRUCK)
		{
			if(rx_frame.TargetID == 0x18FEF100)//121
			{
				stVehicleCanSpeedData_JF1939.flgBoxRxEnd = 1;
				for(i = 0;i < 8;i ++)
					stVehicleCanSpeedData_JF1939.data[i] = rx_frame.data[i] ;
			}
		}
		else
		{
			if(rx_frame.TargetID == stSpeedPara.lId)	// 金旅车速 0x0CFE6CEE
			{
				stVehicleCanSpeedData.flgBoxRxEnd = 1;
				for(i = 0;i < 8;i ++)
					stVehicleCanSpeedData.data[i] = rx_frame.data[i];
			}
		}

		if(rx_frame.TargetID == 0x1C01F002) // BYD Speed
		{
			//fprintf(USART1_STREAM,"BYD\r\n");
			stVehicleCanSpeedDataBYD.flgBoxRxEnd = 1;
			for(i = 0;i < 8;i ++)
				stVehicleCanSpeedDataBYD.data[i] = rx_frame.data[i];
		}
		if(rx_frame.TargetID == 0x1C01F003) // BYD PRND Gear
		{
			//fprintf(USART1_STREAM,"BYD\r\n");
			stVehicleCanGearDataBYD.flgBoxRxEnd = 1;
			for(i = 0;i < 8;i ++)
				stVehicleCanGearDataBYD.data[i] = rx_frame.data[i];
		}
		if(rx_frame.TargetID == 0x1C01F004) // BYD State
		{
			//fprintf(USART1_STREAM,"BYD\r\n");
			stVehicleCanStateDataBYD.flgBoxRxEnd = 1;
			for(i = 0;i < 8;i ++)
				stVehicleCanStateDataBYD.data[i] = rx_frame.data[i];
		}
		if(rx_frame.TargetID == stVehicleCanOdomData.TargetID)	// 金旅大巴里程计 no value
		{
			stVehicleCanOdomData.flgBoxRxEnd = 1;
			for(i = 0;i < 8;i ++)
				stVehicleCanOdomData.data[i] = rx_frame.data[i];
		}
		if(rx_frame.TargetID == stTurnPara.lId)	// 0x18A70017
		{
			stVehicleCanTurnData.flgBoxRxEnd = 1;
			for(i = 0;i < 8;i ++)
				stVehicleCanTurnData.data[i] = rx_frame.data[i];
		}

		if(rx_frame.TargetID == stTurnPara.lId1)	// 0x18A70017
		{
			stVehicleCanTurnData1.flgBoxRxEnd = 1;
			for(i = 0;i < 8;i ++)
				stVehicleCanTurnData1.data[i] = rx_frame.data[i];
		}
		if(rx_frame.TargetID == stBrakePara.lId)	// 0x18A70017
		{
			stVehicleCanBreakData.flgBoxRxEnd = 1;
			for(i = 0;i < 8;i ++)
				stVehicleCanBreakData.data[i] = rx_frame.data[i];
		}
//		if(rx_frame.TargetID == stAbsPara.lId)		// 0x18F0010B
//		{
//			stVehicleCanAbsData.flgBoxRxEnd = 1;
//			for(i = 0;i < 8;i ++)
//				stVehicleCanAbsData.data[i] = rx_frame.data[i];
//		}
#endif
		if(rx_frame.TargetID == stWarningPara.lId)	// no value
		{
			stVehicleCanWarningData.flgBoxRxEnd = 1;
			for(i = 0;i < 8;i ++)
				stVehicleCanWarningData.data[i] = rx_frame.data[i];
		}
		if(rx_frame.TargetID == stSwaPara.lId)		// 0x1C01F022
		{
			stVehicleCanSWAData.flgBoxRxEnd = 1;
			for(i = 0;i < 8;i ++)
				stVehicleCanSWAData.data[i] = rx_frame.data[i];
		}
	}
	else if(rx_frame.TargetID == 0x317)//yuhong 0x317 for the tooling test
	{
		fprintf(USART1_STREAM,"%s>>Get the CAN2 Data is 0x%x.\r\n",__FUNCTION__,rx_frame.data[0]);
		stToolingCanData[2].TargetID = rx_frame.TargetID;
		stToolingCanData[2].flgBoxRxEnd = 1;
		for(i = 0;i < 8;i ++)
		{
			stToolingCanData[2].data[i] = rx_frame.data[i];
		}
	}
}

void can3_receive(struct can_frame rx_frame)
{
	uint8_t i;

	switch(rx_frame.TargetID)
	{
		case can3message0:
			if(!g_sensor_mgt.m_actuator.m_isOpen)// 执行机构关闭，不接收消息
				break;
			Set_ValveProportionalComm_StaStamp(SystemtimeClock);
			if(Proportional_valve_status.flgBoxRxEnd == 0)
			{
				for(i = 0;i < 8;i ++)
					Proportional_valve_status.Box[i] = rx_frame.data[i];
				Proportional_valve_status.flgBoxRxEnd = 1;
			}
			break;

		case can3message1:
			if(!g_sensor_mgt.m_actuator.m_isOpen)// 执行机构关闭，不接收消息
				break;
			Set_ValveProportionalComm_StaStamp(SystemtimeClock);
			if(Proportional_valve_fault.flgBoxRxEnd == 0)
			{

				for(i = 0;i < 8;i ++)
					Proportional_valve_fault.Box[i] = rx_frame.data[i];
				Proportional_valve_fault.flgBoxRxEnd = 1;
			}
			break;
		case 0x317: // yuhong 0x317 for the tooling test
			stToolingCanData[3].TargetID = rx_frame.TargetID;
			for(i = 0;i < 8;i ++)
			{
				stToolingCanData[3].data[i] = rx_frame.data[i];
			}
			stToolingCanData[3].flgBoxRxEnd = 1;
		case 0x31A: //yuhong 0x31A for the tooling EMC test
			stToolingCanData[3].TargetID = rx_frame.TargetID;
			for(i = 0;i < 8;i ++)
			{
				stToolingCanData[3].data[i] = rx_frame.data[i];
			}
			stToolingCanData[3].flgBoxRxEnd = 1;
		default:
			break;
	}
}


void Displayer_CANRx_Analysis(struct can_frame *rx_frame)
{
	struct can_frame tmp_frame = *rx_frame;
	if(tmp_frame.lenth < 8)
		return;
	if(tmp_frame.TargetID == DISPLAYER_FEEDBACK_ID)
	{
//		isAEBTaskClose = tmp_frame.data[0] & 0x01;
//		isLDWTaskClose = (tmp_frame.data[0] & 0x02) >> 1;
		//20220928 yuhong Keep the original value getting method
		//g_AEB_ON_OFF_Displayer = tmp_frame.data[0] & 0x01;
		//20220928 yuhong cancel the displayer to close the AEB On/Off
		g_AEB_ON_OFF_Displayer = 1;
		g_LDW_ON_OFF_Displayer = (tmp_frame.data[0] & 0x02) >> 1;
		//fprintf(USART1_STREAM,"AEB_ON_OFF:%d LDW_ON_OFF:%d\r\n",g_AEB_ON_OFF_Displayer,g_LDW_ON_OFF_Displayer);
	}
	if(tmp_frame.TargetID == DISPLAYER_READ_SNPN_ID)
	{

	}
	CAN_Analysis_Displayer_Version(rx_frame);
	return;
}
void can4_receive(struct can_frame rx_frame)
{
	if(rx_frame.TargetID == 0x18FFED81)
	{
		if((rx_frame.data[0] & 0x01) == 0)
			warning_status.AEBstatus = warning_status.AEBstatus | 0x02;
		else
			warning_status.AEBstatus = warning_status.AEBstatus & 0xFD;

		if((rx_frame.data[0] & 0x02) == 0)
			warning_status.LDWstatus = 1;
		else
			warning_status.LDWstatus = 0;
		Set_Displayer_Stamp(SystemtimeClock);
		Displayer_CANRx_Analysis(&rx_frame);
	}
	else if(rx_frame.TargetID == 0x317)//yuhong 0x317 for the tooling test
	{
		stToolingCanData[4].TargetID = rx_frame.TargetID;
		for(int i = 0;i < 8;i ++)
		{
			stToolingCanData[4].data[i] = rx_frame.data[i];
		}
		stToolingCanData[4].flgBoxRxEnd = 1;
	}
	else if(rx_frame.TargetID == 0x31B)//yuhong 0x31B for the tooling EMC test
	{
		stToolingCanData[4].TargetID = rx_frame.TargetID;
		for(int i = 0;i < 8;i ++)
		{
			stToolingCanData[4].data[i] = rx_frame.data[i];
		}
		stToolingCanData[4].flgBoxRxEnd = 1;
	}
	else
	{
//		Can_Config_Parameter(rx_frame);
	}
}
/*
 * 比例阀通信时间
 * */
void Set_ValveProportionalComm_StaStamp(uint32_t stamp)
{
	stCanCommSta.Proportional_valve.oldSysTime = stamp;
}
/*
 * 整车CAN通信时间
 * */
//void Set_ValveComm_StaStamp(uint32_t stamp)
//{
//	stCanCommSta.stVehicle.oldSysTime = stamp;
//}

//void Set_Valvespeed_Stamp(uint32_t stamp)
//{
//	stCanCommSta.stSpeed.oldSysTime = stamp;
//}

void Set_Camera_Stamp(uint32_t stamp)
{
	stCanCommSta.stCamera.oldSysTime = stamp;
}
void Set_Displayer_Stamp(uint32_t stamp)
{
	stCanCommSta.stDisplayer.oldSysTime = stamp;
}

float fVehiclespeedKalmanFilter(float original_speed)
{
	float kg = 0;
	float x_now = 0;
	float x_pre= 0;
	static float p_last = 1;
	static float x_last = 0;
	float p_pre = 0;
	float p_now = 0;

	x_pre = x_last;									/*预测*/
    /*Get convariance */
    p_pre = p_last + VEHICLE_SPEED_NIOSE_Q;			/*Q=0.25 协方差预测*/
    kg = p_pre/(p_pre + VEHICLE_SPEED_NIOSE_R);		/*NIOSE_R=1 卡尔曼增益*/
    x_now = x_pre + kg*(original_speed - x_pre);	/*状态估计*/
    p_now = (1-kg)*p_pre;							/*协方差更新*/

    p_last = p_now;
    x_last = x_now;

    return x_now;
}

float fVehicleSpeedFilter(float fVeSpeed)
{
	float fSmoothSpeed = 0;
	static float fPreSmoothSpeed = 0;

	fSmoothSpeed = fVehiclespeedKalmanFilter(fVeSpeed);

	if((fPreSmoothSpeed - fSmoothSpeed) > VEHICLE_MAX_SPEED_ERROR)
	{
		fSmoothSpeed = fPreSmoothSpeed - VEHICLE_MAX_SPEED_ERROR;
	}
#ifdef SPEED_SOURCE_FROM_ANOLOG
	else if((fPreSmoothSpeed - fSmoothSpeed) < (-(1.2f/3.6)))
	{
		fSmoothSpeed = fPreSmoothSpeed + VEHICLE_MAX_SPEED_ERROR;
	}
#endif
	fPreSmoothSpeed = fSmoothSpeed;

	return fSmoothSpeed;
}
#if (CAR_CAN_ANALY_IS_ENABLE != 1)	//  lmz del 20221019
//==================================================================================================
// 解析CAN报文的车速信号
// BYD
//--------------------------------------------------------------------------------------------------
void DecodeCanSpeedBYD(uint8_t *ptr)
{
	uint8_t BYD_Speed_data_1;
	uint8_t BYD_Speed_data_2;

	BYD_Speed_data_1 = *(ptr);
	BYD_Speed_data_2 = *(ptr +1);

	stVehicleParas.fVehicleSpeed = ((float)(BYD_Speed_data_1))*0.06875 + ((float)(BYD_Speed_data_2 & 0x0F)*256*0.06875);
	//fprintf(USART1_STREAM,"v=%0.2f\r\n",stVehicleParas.fVehicleSpeed);
	if(stVehicleParas.fVehicleSpeed > 280.0){
		stVehicleParas.fVehicleSpeed = 0.0;
	}
	BYD_Speed_data_1 = *(ptr + 4);
	BYD_Speed_data_2 = *(ptr + 5);
	uint16_t hex_steer_angle = BYD_Speed_data_1 | BYD_Speed_data_2<<8;
	if(0x7FFF == hex_steer_angle)//Fault value
		stVehicleParas.steer_wheel_angle =  0.0;
	else
		stVehicleParas.steer_wheel_angle =  ((int16_t)(hex_steer_angle))*0.1;

}
//==================================================================================================
// 解析CAN报文的转向与司机刹车信号
// BYD
//--------------------------------------------------------------------------------------------------
void DecodeCanTurnAndBrakeBYD(uint8_t *ptr){
	uint8_t BYD_Turn_data_2;
	uint8_t BYD_Brake_data_4;

	BYD_Turn_data_2  = *(ptr + 1);
	BYD_Brake_data_4 = *(ptr + 3);
	//yuhong 20220902 根据BYD协议需要将数据移位
	stVehicleParas.LeftFlagTemp  = (BYD_Turn_data_2 & 0x08) >>3;
	stVehicleParas.RightFlagTemp = (BYD_Turn_data_2 & 0x10) >>4;
	if((BYD_Brake_data_4 & 0x0C) != 0x00)
		stVehicleParas.BrakeFlag = 0x01;
	else
		stVehicleParas.BrakeFlag = 0x00;
}
//==================================================================================================
// 解析CAN报文的挡位信号
// BYD
//--------------------------------------------------------------------------------------------------
void DecodeCanBackBYD(uint8_t *ptr){
	uint8_t BYD_Back_data_4;
	BYD_Back_data_4 = *(ptr + 3);
	uint8_t PRND = BYD_Back_data_4 & 0xF0;
	if(PRND == 0x02)
		stVehicleParas.ReverseGear = 0x01;
	else
		stVehicleParas.ReverseGear = 0x00;
}
//==================================================================================================
// 解析CAN报文的车速信号
//--------------------------------------------------------------------------------------------------
void DecodeCanSpeed(uint8_t *ptr)
{
	short i = 0;
	float fFilteredSpeed;
	static float fPreSpeed = 0.0f;
	static uint32_t ulSpeedJumpTime = 0;
	static uint32_t ulPreTime = 0;
	uint32_t ulCurrentTime = 0;

	ulCurrentTime = SystemtimeClock;
	//stVehicleParas.fVehicleSpeed = ptr[7];

	if (ulCurrentTime - ulPreTime >= VEHICLE_CAN_OFFLINE_PERIOD)
	{
		fPreSpeed = 0; // 发现车Can掉线，将上次车速清零
	}
	ulPreTime = ulCurrentTime;

	if(stSpeedPara.ucByteLth==2)							// 两个字节长度
	{
		if(stSpeedPara.ucByteOrder==SPEED_HIGH_BYTE_FISRT)
		{
			i = *(ptr+stSpeedPara.ucStartByte);
			i = (i<<8) + *(ptr+stSpeedPara.ucStartByte+1);
		}
		else
		{
			i = *(ptr+stSpeedPara.ucStartByte+1);
			i = (i<<8) + *(ptr+stSpeedPara.ucStartByte);
		}
	}
	else
	{
		i = stVehicleCanData.data[stSpeedPara.ucStartByte];
	}
	stVehicleParas.fVehicleSpeed = (float)i / 256.f;

	//stVehicleParas.fVehicleSpeed /= (256.0);
	//stVehicleParas.fVehicleSpeed = ((float)i)*stSpeedPara.ucCoeffA
											// /stSpeedPara.ucCoeffB/3.6;


/*
	// 车速异常检查
	if (stVehicleParas.fVehicleSpeed * 3.6 >= VEHICLE_MAX_SPEED_VALID)
	{
		ucFlagSpeedErr = 1;
	}
	else
	{
		ucFlagSpeedErr = 0;
	}

	// 车速跳变异常检查
	if ((stVehicleParas.fVehicleSpeed - fPreSpeed) >= VEHICLE_SPEED_JUMP_THRESHOLD / 3.6)
	{
		ucFlagSpeedJumpErr = 1;
		fPreSpeed = stVehicleParas.fVehicleSpeed;
		ulSpeedJumpTime = ulCurrentTime;
	}
	else
	{
		if ((ulCurrentTime - ulSpeedJumpTime >= VEHICLE_SPEED_JUMP_HOLD_TIME)
			&& (1 == ucFlagSpeedJumpErr))
		{
			ucFlagSpeedJumpErr = 0;
			ulSpeedJumpTime = 0;

		}
		fPreSpeed = stVehicleParas.fVehicleSpeed;
	}

	//车速滤波
	fFilteredSpeed = fVehicleSpeedFilter(stVehicleParas.fVehicleSpeed);
	stVehicleParas.fVehicleSpeed = fFilteredSpeed + 0.001; // To solve precision loss problem
	*/
}

void DecodeJF1939CanSpeed(uint8_t *ptr)
{
	short i = 0;
	float fFilteredSpeed;
	static float fPreSpeed = 0.0f;
	static uint32_t ulSpeedJumpTime = 0;
	static uint32_t ulPreTime = 0;
	uint32_t ulCurrentTime = 0;

	ulCurrentTime = SystemtimeClock;
	//stVehicleParas.fVehicleSpeed = ptr[7];

	if (ulCurrentTime - ulPreTime >= VEHICLE_CAN_OFFLINE_PERIOD)
	{
		fPreSpeed = 0; // 发现车Can掉线，将上次车速清零
	}
	ulPreTime = ulCurrentTime;

	i = stVehicleCanSpeedData_JF1939.data[2] << 8 | stVehicleCanSpeedData_JF1939.data[1];
	stVehicleParas.fVehicleSpeed = (float)i / 256.f;
	//stVehicleParas.fVehicleSpeed /= (256.0);
}
#endif
bool g_b_can_sp_show_flag = false;
#if (CAR_CAN_ANALY_IS_ENABLE != 1)	//  lmz del 20221019
void show_jf1939_can_speed();
void show_jf1939_can_speed()
{
	if (g_b_can_sp_show_flag == true)
	{
	//	USART1_Bebug_Print_Flt("CAN_SP:",stVehicleParas.fVehicleSpeed,1,1);
	//	if(stVehicleCanSpeedData_JF1939.flgBoxRxEnd == 1)
		{
	//		stVehicleCanSpeedData_JF1939.flgBoxRxEnd 		= 0;

		//	DecodeJF1939CanSpeed(stVehicleCanSpeedData_JF1939.data);

			uint8_t *ptr = stVehicleCanSpeedData_JF1939.data;
			short i = 0;

			float show_speed = 0.0;
			i = stVehicleCanSpeedData_JF1939.data[2] << 8 | stVehicleCanSpeedData_JF1939.data[1];
			show_speed = (float)i;
			show_speed /= (256.0);

			USART1_Bebug_Print_Flt("CAN_SP:",show_speed,1,1);
		}
	}
}

void DecodeCanTurn(uint8_t *ptr,uint8_t test)
{
	uint8_t byte_val,signal_val;

	//if(stTurnPara.Source == 0)
	{
		if(test == 0)
			byte_val = *(ptr + stTurnPara.ucByteNo);
		else
			byte_val = *(ptr + stTurnPara.ucByteNo1);
		signal_val =   (byte_val>>stTurnPara.ucLeftStartBit)
					 & (0xff>>(8-stTurnPara.ucLeftBitLth));
		if(signal_val==stTurnPara.ucLeftValid)
		{
			stVehicleParas.LeftFlagTemp = 1;
		}
		else
		{
			stVehicleParas.LeftFlagTemp = 0;
		}
		//-----------------------------------------------------------
		signal_val =   (byte_val>>stTurnPara.ucRightStartBit)
					 & (0xff>>(8-stTurnPara.ucRightBitLth));
		if(signal_val == stTurnPara.ucRightValid)
		{
			stVehicleParas.RightFlagTemp = 1;
		}
		else
		{
			stVehicleParas.RightFlagTemp = 0;
		}
		if(((ptr[2] >> 2) & 0x03) == 0x01)
			stVehicleParas.ReverseGear = 1;
		else
			stVehicleParas.ReverseGear = 0;

	}
}
void DecodeCanTurnL(uint8_t *ptr)
{
	uint8_t byte_val,signal_val;

	if(stTurnPara.Source == 0)
	{
		byte_val = *(ptr + stTurnPara.ucByteNo);
		signal_val =   (byte_val>>stTurnPara.ucLeftStartBit)
					 & (0xff>>(8-stTurnPara.ucLeftBitLth));
		if(signal_val==stTurnPara.ucLeftValid)
		{
			stVehicleParas.LeftFlagTemp = 1;
		}
		else
		{
			stVehicleParas.LeftFlagTemp = 0;
		}
	}
}
void DecodeCanTurnR(uint8_t *ptr)
{
	uint8_t byte_val,signal_val;

	if(stTurnPara.Source == 0)
	{
		signal_val =   (byte_val>>stTurnPara.ucRightStartBit)
					 & (0xff>>(8-stTurnPara.ucRightBitLth));
		if(signal_val == stTurnPara.ucRightValid)
		{
			stVehicleParas.RightFlagTemp = 1;
		}
		else
		{
			stVehicleParas.RightFlagTemp = 0;
		}
	}
}

void DecodeCanBrake(uint8_t *ptr)
{
	uint8_t byte_val,signal_val;

	if(stBrakePara.Source==0)
	{
		byte_val = *(ptr + stBrakePara.ucByteNo);
		if(stBrakePara.Type == 0)						// 数值型
		{
			signal_val =   (byte_val>>stBrakePara.ucStartBit)
					 	 & (0xff>>(8-stBrakePara.ucBitLth));
			if(signal_val == stBrakePara.ucValid)
			{
				stVehicleParas.BrakeFlag = 1;
			}
			else
			{
				stVehicleParas.BrakeFlag = 0;
			}
		}
		else													// 百分比型
		{
			if(byte_val != 0x00)
			{
				stVehicleParas.BrakeFlag = 1;
			}
			else
			{
				stVehicleParas.BrakeFlag = 0;
			}
		}
	}
}

//void DecodeCanAbs(uint8_t *ptr)
//{
//	uint8_t byte_val = 0;
//	uint8_t signal_val = 0;
//
//	if(0 == stAbsPara.Source)
//	{
//		byte_val = *(ptr + stAbsPara.ucByteNo);
//
//		signal_val =   (byte_val >> stAbsPara.ucAbsWarningStartBit)
//					 & (0xff >>(8 - stAbsPara.ucAbsWarningBitLth));
//		if(signal_val == stAbsPara.ucAbsWarningValid)
//		{
//			stVehicleParas.AbsErrorFlag = 1;
//		}
//		else
//		{
//			stVehicleParas.AbsErrorFlag = 0;
//		}
//
//	}
//}
#endif
void DecodeCanSwa(uint8_t *ptr)
{
//	stSWAParas.SWADegree = (uint16_t)(ptr[0] << 8) | ptr[1];
	stVehicleParas.steer_wheel_angle = (uint16_t)(ptr[0] << 8) | ptr[1];	// update lmz 20221019
}
#if 0	//  lmz del 20221019
//double readSpeedSensorValue(void)
//{
//	double DW_PLUSE_WIDTH=0;//捕捉到PWM周期变量
//	uint32_t DW_PLUSE_WIDTH2=0;
//	uint32_t temp=0;
//
//	DW_PLUSE_WIDTH = (double)1/first_time;//CCP_Get_Capture_Result(CCP21_SFR,CCP_CHANNEL_2);
//	/*
//	if(DW_PLUSE_WIDTH < 5)
//	{
//		DW_PLUSE_WIDTH=0;
//	}
//	else if((DW_PLUSE_WIDTH > 5) && (DW_PLUSE_WIDTH < 100))
//	{
//		temp = DW_PLUSE_WIDTH;
//		DW_PLUSE_WIDTH2 = temp;
//	}
//	else
//	{
//		;
//	}
//	*/
//	return DW_PLUSE_WIDTH;
//}

//float speedConvertSensorValue(double speedSensorValue)
//{
//	float speedConvertedValue = 0;
//
//	//sensorvalue =speedSensorValue;
//	if(speedSensorValue < 0.0001)
//	{
//		speedConvertedValue = 0;
//	}
//	else
//	{
//		speedConvertedValue = (float)((float)(stSpeedPara.ucCoeffA) / stSpeedPara.ucCoeffB *(10000000/(36*speedSensorValue)));//((float)speedSensorValue/1000)*stSpeedConfigPara.ucCoeffA
//		//speedConvertedValue = (float)(stSpeedPara.ucCoeffA * sensorvalue);
//		//-speedConvertedValue = speedConvertedValue / ((float)stSpeedPara.ucCoeffB);
//		//speedConvertedValue = (float)((stSpeedPara.ucCoeffA) * sensorvalue/ stSpeedPara.ucCoeffB);// *(10000000/(36*speedSensorValue)));
//	}
//
//	return speedConvertedValue;
//
//}

//void DecodeHetSpeed(void)
//{
//
////	static float fPreSpeed = 0.0f;
////	static uint32_t ulSpeedJumpTime = 0;
//	double iFilteredSpeed = 0.0;
////	float fFilteredSpeed = 0.0;
////	static uint32_t ulPreSystemTime = 0;
////	uint32_t ulCurrentSystemTime;
//
//	iFilteredSpeed = readSpeedSensorValue();
//	stVehicleParas.fVehicleSpeed = speedConvertSensorValue(iFilteredSpeed);
//}
#endif
bool g_b_cal_sp_show = false;

void Vehicle_Parameter_Analysis(uint32_t ulSysTime)
{
#if CAR_CAN_ANALY_IS_ENABLE				// add lmz 2001019
	Car_Can_Dynamic_Parse_In_Vehicle_Parameter_Analysis(ulSysTime);
#endif
	bool real_time_flag = false;
#if (CAR_CAN_ANALY_IS_ENABLE != 1)		// add lmz 2001019
	if(stVehicleCanSpeedDataBYD.flgBoxRxEnd != 1)
	{
	//	frame_interval = ulSysTime - stCanCommSta.stSpeed.oldSysTime;
		stVehicleCanSpeedDataBYD.flgBoxRxEnd 	= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stCanCommSta.stSpeed.oldSysTime 		= ulSysTime;
		DecodeCanSpeedBYD(stVehicleCanSpeedDataBYD.data);	// 0x1C01F002
		real_time_flag = true;
	}
	if(stVehicleCanGearDataBYD.flgBoxRxEnd == 1)
	{
	//	frame_interval = ulSysTime - stCanCommSta.stSpeed.oldSysTime;
		stVehicleCanGearDataBYD.flgBoxRxEnd 	= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stCanCommSta.stSpeed.oldSysTime 		= ulSysTime;
		DecodeCanBackBYD(stVehicleCanGearDataBYD.data);	// 0x1C01F003
	}
	if(stVehicleCanStateDataBYD.flgBoxRxEnd == 1)
	{
		stVehicleCanStateDataBYD.flgBoxRxEnd 	= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stCanCommSta.stSpeed.oldSysTime 		= ulSysTime;
		DecodeCanTurnAndBrakeBYD(stVehicleCanStateDataBYD.data);	// 0x1C01F004
	}

	if(stVehicleCanSpeedData.flgBoxRxEnd == 1)
	{
		stVehicleCanSpeedData.flgBoxRxEnd 		= 0;
		if(stSpeedPara.uSingnalType == 0)
		{
		//	frame_interval = ulSysTime - stCanCommSta.stSpeed.oldSysTime;
			stCanCommSta.stVehicle.oldSysTime 	= ulSysTime;
			stCanCommSta.stSpeed.oldSysTime 	= ulSysTime;
			DecodeCanSpeed(stVehicleCanSpeedData.data);	// 0x0CFE6CEE
		}
	}
	if(stVehicleCanSpeedData_JF1939.flgBoxRxEnd == 1)
	{
	//	frame_interval = ulSysTime - stCanCommSta.stSpeed.oldSysTime;
		stVehicleCanSpeedData_JF1939.flgBoxRxEnd 		= 0;

		stCanCommSta.stVehicle.oldSysTime 	= ulSysTime;
		stCanCommSta.stSpeed.oldSysTime 	= ulSysTime;
		DecodeJF1939CanSpeed(stVehicleCanSpeedData_JF1939.data);	// 0x18FEF100
		show_jf1939_can_speed();
		real_time_flag = true;
	}

	if(stVehicleCanOdomData.flgBoxRxEnd == 1)	// 金旅大巴里程计
	{
		stVehicleCanOdomData.flgBoxRxEnd 	= 0;
		// Byte4~1是累计行驶里程；Byte8~5是当前行驶里程;分辨率为5m/bit,偏移量为0;
		for(uint8_t i=0;i<4;i++){
			uint32_t temp 						= stVehicleCanOdomData.data[i];	// no value
			stVehicleParas.VehicleOdom_Total 	+= (temp<<(8*i))*5;	// 单位米
			temp 								= stVehicleCanOdomData.data[i+4];
			stVehicleParas.VehicleOdom_Current 	+= (temp<<(8*i))*5;	// 单位米
		}
	}
	if(stVehicleCanTurnData.flgBoxRxEnd == 1)
	{
		stVehicleCanTurnData.flgBoxRxEnd 		= 0;
		if(stTurnPara.lId == stTurnPara.lId1){	// 0x18A70017
			if(stTurnPara.Source == 0){	// 该值一直都为0
				stCanCommSta.stVehicle.oldSysTime = ulSysTime;
				DecodeCanTurn(stVehicleCanTurnData.data,0);		// 0x18A70017
			}
		}else{
			DecodeCanTurnL(stVehicleCanTurnData.data);
		}
	}
	if(stVehicleCanTurnData1.flgBoxRxEnd == 1)
	{
		stVehicleCanTurnData1.flgBoxRxEnd 		= 0;
		if(stTurnPara.lId == stTurnPara.lId1){	// 0x18A70017
			if(stTurnPara.Source1 == 0){	// 该值一直都为0
				stCanCommSta.stVehicle.oldSysTime = ulSysTime;
				DecodeCanTurn(stVehicleCanTurnData1.data,1);	// 0x18A70017
			}
		}else{
			DecodeCanTurnR(stVehicleCanTurnData1.data);
		}
	}

	if(stVehicleCanBreakData.flgBoxRxEnd == 1)
	{
		stVehicleCanBreakData.flgBoxRxEnd 		= 0;
		if(stBrakePara.Source == 0){	// 该值一直都为0
			stCanCommSta.stVehicle.oldSysTime = ulSysTime;
			DecodeCanBrake(stVehicleCanBreakData.data);	// 0x18A70017
		}
	}

//	if(stVehicleCanAbsData.flgBoxRxEnd == 1)
//	{
//		stVehicleCanAbsData.flgBoxRxEnd 		= 0;
//		if(stAbsPara.Source == 0){	// 该值一直都为0
//			stCanCommSta.stVehicle.oldSysTime = ulSysTime;
//			DecodeCanAbs(stVehicleCanAbsData.data);	// 0x18F0010B
//		}
//	}
#endif
	if(stVehicleCanSWAData.flgBoxRxEnd == 1)
	{
		stVehicleCanSWAData.flgBoxRxEnd 		= 0;
		DecodeCanSwa(stVehicleCanSWAData.data);		// 0x1C01F022
	}
#if 0	//  lmz del 20221019
//	if(stSpeedPara.uSingnalType == 1)	// 该值一直都为0
//	{
//		DecodeHetSpeed();
//	}
//
//	if(stTurnPara.Source == 1)			// 该值一直都为0
//	{
//		stVehicleParas.LeftFlagTemp 	= Read_turnleft_signal();
//		stVehicleParas.RightFlagTemp 	= Read_turnright_signal();
//	}
//
//	if(stBrakePara.Source == 1)			// 该值一直都为0
//	{
//		stVehicleParas.BrakeFlag 		= Read_stop_signal();//刹车线
//	}
//
//	if(stAbsPara.Source == 1)			// 该值一直都为0
//	{
//		stVehicleParas.AbsErrorFlag 	= Read_AEB_switch();
//	}
#endif
	//测试代码
#if 0
	static int64_t cur_clk;
	static float cnt = 0.0;
	static int state = 0;
	static float sp_temp;
	frame_interval = 10;
	stVehicleParas.fVehicleSpeed = sp_temp;
	if ((SystemtimeClock - cur_clk) >= 100)
	{
		stVehicleParas.fVehicleSpeed = 10.0+cnt;
		cur_clk = SystemtimeClock;
		real_time_flag = true;

		if (state == 0)
		{
			cnt += 0.1;
			if (stVehicleParas.fVehicleSpeed >= 40)
			{
				cnt = 0.0;
				state = 1;
			}
		}
		else if (state == 1)
		{
			cnt -= 0.3;
			if (stVehicleParas.fVehicleSpeed <= 0)
			{
				cnt = 0.0;
				state = 0;
				stVehicleParas.fVehicleSpeed = 0;
			}
		}
		sp_temp = stVehicleParas.fVehicleSpeed;
		gcz_serial_v1_dma_printf(SERIAL_UART1,"%f  %f\r\n",stVehicleParas.fVehicleSpeed,cnt);
	}
	else
	{
		real_time_flag = false;
	}
#endif
	static int64_t sp_calc_frq_clk;
//	struct ImuData s_imuData;
//	GetImuData(&s_imuData);
	if (real_time_flag == false)
	{
		if (((SystemtimeClock - sp_calc_frq_clk) >= 10) && (stVehicleParas.fVehicleSpeed >= 5.0) && (g_st_imu_klm_filter_data.acc[X_P] < 0.0))
		{
			uint32_t interval = SystemtimeClock - sp_calc_frq_clk;
			sp_calc_frq_clk = SystemtimeClock;
			g_f_srr_fVehicleSpeed  = g_f_srr_fVehicleSpeed + g_st_imu_klm_filter_data.acc[X_P]*interval*3.6/1000.0;
			if (g_b_cal_sp_show)
			{
				gcz_serial_v1_dma_printf(SERIAL_UART1,"[CALC_SP]ACC:%0.8f ",g_st_imu_klm_filter_data.acc[X_P]);
				gcz_serial_v1_dma_printf(SERIAL_UART1,"%0.8f ",g_st_imu_klm_filter_data.acc[X_P]*interval/1000.0);
				gcz_serial_v1_dma_printf(SERIAL_UART1,"SYSCLK:%d ",SystemtimeClock);
				gcz_serial_v1_dma_printf(SERIAL_UART1,"ORG_SP:%0.8f ",stVehicleParas.fVehicleSpeed);
				gcz_serial_v1_dma_printf(SERIAL_UART1,"SP:%0.8f\r\n",g_f_srr_fVehicleSpeed);
			}
		}
	}
	else
	{
		sp_calc_frq_clk = SystemtimeClock;
		g_f_srr_fVehicleSpeed  = stVehicleParas.fVehicleSpeed;
		if (g_b_cal_sp_show)
		{
			gcz_serial_v1_dma_printf(SERIAL_UART1,"[ORG_SP]ACC:%0.8f %0.8f ",g_st_imu_klm_filter_data.acc[X_P],
												g_st_imu_klm_filter_data.acc[X_P]*100/1000.0);
			gcz_serial_v1_dma_printf(SERIAL_UART1,"SYSCLK:%d ",SystemtimeClock);
			gcz_serial_v1_dma_printf(SERIAL_UART1,"ORG_SP:%0.8f ",stVehicleParas.fVehicleSpeed);
			gcz_serial_v1_dma_printf(SERIAL_UART1,"SP:%0.8f\r\n",g_f_srr_fVehicleSpeed);
		}
	}
	//sp_temp = stVehicleParas.fVehicleSpeed;
}

void Proprot_RxValve_Data_Analysis(uint32_t ulSysTime)
{
    uint16_t i;
    uint8_t  j;
	if(Proportional_valve_status.flgBoxRxEnd == 1)
    {
		Proportional_valve_status.flgBoxRxEnd = 0;
		//fprintf(USART1_STREAM,"file \r\n");

		stValveParas.TimeStamp = ulSysTime;

    	j = Proportional_valve_status.Box[0];
    	stValveParas.Valve_State = j;


    	j = Proportional_valve_status.Box[1];
    	stValveParas.Internal_Temp = (float)(j-40);

    	i = (uint16_t)(Proportional_valve_status.Box[3] << 8);
    	i = i | Proportional_valve_status.Box[2];
    	stValveParas.Actual_Pressure = (float)(i) / 128 * 100;

    	i = (uint16_t)(Proportional_valve_status.Box[5] << 8);
    	i = i | Proportional_valve_status.Box[4];
    	stValveParas.Target_Pressure = (float)(i) / 128 * 100;

    	j = Proportional_valve_status.Box[6];
    	stValveParas.Fault_Code = j;
/*
    	if((stValveParas.Fault_Code == 1) |
    			(stValveParas.Fault_Code == 4) |
    			(stValveParas.Fault_Code == 8) |
    			(stValveParas.Fault_Code == 9) |
    			(stValveParas.Fault_Code == 10)	|
    			(stValveParas.Fault_Code == 11))
    	{
    		GPIO_Set_Output_Data_Bits(GPIOE_SFR,GPIO_PIN_MASK_3, 0);
    		Delay(2,2);
    		GPIO_Set_Output_Data_Bits(GPIOE_SFR,GPIO_PIN_MASK_3, 1);
    	}*/
    }
	if(Proportional_valve_fault.flgBoxRxEnd == 1)
	{
		//stCanCommSta.Proportional_valve.oldSysTime = ulSysTime;
		Proportional_valve_fault.flgBoxRxEnd == 0;
	}
}
//gcz
//CAN外设提示信息。并清除版本号
void can_device_prompt_msg_and_clear_version()
{
	static bool car_can_msg = FALSE;
	if (stCanCommSta.stVehicle.status == ONLINE)
	{
		car_can_msg = TRUE;
	}
	else
	{
		if (car_can_msg == TRUE)
		{
			car_can_msg = FALSE;

//			USART_Send(USART1_SFR,"Vehicle CAN COMM loss\r\n",strlen("Vehicle CAN COMM loss\r\n"));
			// uint8_t buffer[]="Vehicle CAN COMM loss";
			//memset(buffer,0,sizeof(buffer));
			//sprintf(buffer,"Vehicle CAN COMM loss\r\n");
			// Usart1_DMA_Transmit("Vehicle CAN COMM loss",strlen("Vehicle CAN COMM loss"));
		}
	}

	static bool proportional_valve_can_msg = FALSE;
	if (stCanCommSta.Proportional_valve.status == ONLINE)
	{
		proportional_valve_can_msg = TRUE;
	}
	else
	{
		if (proportional_valve_can_msg == TRUE)
		{
			proportional_valve_can_msg = FALSE;

//			USART_Send(USART1_SFR,"Proportional valve CAN COMM loss\r\n",strlen("Proportional valve CAN COMM loss\r\n"));
			// uint8_t buffer[]="Proportional valve CAN COMM loss\r\n";
			//memset(buffer,0,sizeof(buffer));
			//sprintf(buffer,"Proportional valve CAN COMM loss\r\n");
			// Usart1_DMA_Transmit("Proportional valve CAN COMM loss\r\n",strlen("Proportional valve CAN COMM loss\r\n"));
		}
	}

	static bool displayer_can_msg = FALSE;
	if (stCanCommSta.stDisplayer.status == ONLINE)
	{
		displayer_can_msg = TRUE;
	}
	else
	{
		if (displayer_can_msg == TRUE)
		{
			sw_hw_version.Displayer_SW_Version.v0 = 0;
			sw_hw_version.Displayer_SW_Version.v1 = 0;
			sw_hw_version.Displayer_SW_Version.v2 = 0;

			sw_hw_version.Displayer_SN[0] = 0;
			sw_hw_version.Displayer_SN[1] = 0;
			sw_hw_version.Displayer_SN[2] = 0;
			sw_hw_version.Displayer_SN[3] = 0;
			sw_hw_version.Displayer_PN[0] = 0;
			sw_hw_version.Displayer_PN[1] = 0;
			sw_hw_version.Displayer_PN[2] = 0;
			sw_hw_version.Displayer_PN[3] = 0;
			displayer_can_msg = FALSE;

//			USART_Send(USART1_SFR,"Displayer CAN COMM loss\r\n",strlen("Displayer CAN COMM loss\r\n"));
			// uint8_t buffer[50];
			// memset(buffer,0,sizeof(buffer));
			// sprintf(buffer,"Displayer CAN COMM loss\r\n");
			// Usart1_DMA_Transmit(buffer,strlen(buffer));
		}
	}

	static bool mmradar_can_msg = FALSE;
	if (stCanCommSta.stRadar.status == ONLINE)
	{
		mmradar_can_msg = TRUE;
	}
	else
	{
		if (mmradar_can_msg == TRUE)
		{
			sw_hw_version.MRadar_Version.v0 = 0;
			sw_hw_version.MRadar_Version.v1 = 0;
			sw_hw_version.MRadar_Version.v2 = 0;
			mmradar_can_msg = FALSE;

//			USART_Send(USART1_SFR,"MMRadar CAN COMM loss\r\n",strlen("MMRadar CAN COMM loss\r\n"));
			// uint8_t buffer[50];
			// memset(buffer,0,sizeof(buffer));
			// sprintf(buffer,"MMRadar CAN COMM loss\r\n");
			// Usart1_DMA_Transmit(buffer,strlen(buffer));

		}
	}

	static bool ultradar_can_msg = FALSE;
	if (stCanCommSta.stHRadar.status == ONLINE)
	{
		ultradar_can_msg = TRUE;
	}
	else
	{
		if (ultradar_can_msg == TRUE)
		{
			sw_hw_version.URadar_Version.v0 = 0;
			sw_hw_version.URadar_Version.v1 = 0;
			ultradar_can_msg = FALSE;

//			USART_Send(USART1_SFR,"UltRadar CAN COMM loss\r\n",strlen("UltRadar CAN COMM loss\r\n"));
			// uint8_t buffer[50];
			// memset(buffer,0,sizeof(buffer));
			// sprintf(buffer,"UltRadar CAN COMM loss\r\n");
			// Usart1_DMA_Transmit(buffer,strlen(buffer));
		}
	}

	static bool cam_can_msg = FALSE;
	if (stCanCommSta.stCamera.status == ONLINE)
	{
		cam_can_msg = TRUE;
	}
	else
	{
		if (cam_can_msg == TRUE)
		{
			sw_hw_version.Camera_SW_Version.v0 = 0;
			sw_hw_version.Camera_SW_Version.v1 = 0;
			sw_hw_version.Camera_SW_Version.v2 = 0;
			sw_hw_version.Camera_SW_Version.v3 = 0;

			sw_hw_version.Camera_HW_Version.v0 = 0;
			sw_hw_version.Camera_HW_Version.v1 = 0;
			sw_hw_version.Camera_HW_Version.v2 = 0;
			sw_hw_version.Camera_HW_Version.v3 = 0;

			comm_protocol_version.Camera_CAN_ProtocoI_Version.v0 = 0;
			comm_protocol_version.Camera_CAN_ProtocoI_Version.v1 = 0;
			comm_protocol_version.Camera_CAN_ProtocoI_Version.v2 = 0;
			comm_protocol_version.Camera_CAN_ProtocoI_Version.v3 = 0;
			cam_can_msg = FALSE;
		//	fprintf(USART1_STREAM, "@@:%d\r\n",SystemtimeClock);

//			USART_Send(USART1_SFR,"CAMERA CAN COMM loss\r\n",strlen("CAMERA CAN COMM loss\r\n"));
			// uint8_t buffer[50];
			// memset(buffer,0,sizeof(buffer));
			// sprintf(buffer,"CAMERA CAN COMM loss\r\n");
			// Usart1_DMA_Transmit(buffer,strlen(buffer));
		
		}
	}

}
//gcz 2022-05-24
//****************************************************************************
typedef struct __state_ctrl
{
	uint8_t state;
	uint32_t timer;
}STATE_CTRL;
enum
{
	STATE_INIT = 0,
	STATE_POWER_OFF,
	STATE_WAIT_POWER_OFF,
	STATE_POWER_ON,
	STATE_POWER_ON_DELAY,
	STATE_IDLE
};
enum
{
	STATE_SYS_FIRST_RUN = 0,
	STATE_SYS_FIRST_RUN_WAIT,
	STATE_SYS_NORMAL_RUN,
	STATE_SYS_NORMAL_WAIT,
	STATE_SYS_NORMAL_PWR_OFF_DELAY,
	STATE_SYS_NORMAL_PWR_OFF
};
static STATE_CTRL s_st_proporval_offline_state;
static STATE_CTRL s_st_power_ctrl_state;
#define PWR_OFF_TIMEOUT_COUNT			2000					//断电时长
#define PWR_ON_TIMEOUT_COUNT			2000					//上电时长
#define FIRST_RUN_TIMEOUT_COUNT			2000					//等待通讯失联最大时长
#define TIMEOUT_COUNT					30000					//等待通讯失联最大时长

//初始化比例阀值
void init_propor_var()
{
	stCanCommSta.Proportional_valve.oldSysTime = -3000;
	stCanCommSta.Proportional_valve.status = OFFLINE;
}
//状态机变量初始化
static void power_ctrl_struct_init()
{
	s_st_power_ctrl_state.state = STATE_INIT;
	s_st_power_ctrl_state.timer = 0;
}
//设置状态机状态
static void set_power_state_machine_status(uint8_t state)
{
	s_st_power_ctrl_state.state = state;
//	fprintf(USART1_STREAM, "SET STATE:%d\r\n",s_st_power_ctrl_state.state);
}
//获取状态机状态
static uint8_t get_power_state_machine_status()
{
	return s_st_power_ctrl_state.state;
}
static void set_power_gpio_state(BitAction BitsValue)
{
	GPIO_Set_Output_Data_Bits(GPIOE_SFR,GPIO_PIN_MASK_3, BitsValue);
	uint8_t buffer[25];
	memset(buffer,0,sizeof(buffer));
	sprintf(buffer, "**PWR---%s\r\n",(BitsValue == Bit_RESET) ? "OFF" : "ON");

//	USART_Send(USART1_SFR,buffer,strlen(buffer));
	Usart1_DMA_Transmit(buffer,strlen(buffer));	
}
//电源开关控制状态机
static void power_ctrl_state_machine()
{
	uint32_t currentTime;
	currentTime = SystemtimeClock;
	switch(s_st_power_ctrl_state.state)
	{
	case STATE_INIT:					power_ctrl_struct_init();
										set_power_state_machine_status(STATE_IDLE);
										//fprintf(USART1_STREAM, "STATE_INIT\r\n");
										break;
	case STATE_POWER_OFF:				set_power_gpio_state(Bit_RESET);		//关电源
										s_st_power_ctrl_state.timer = currentTime;					//记录时间
										set_power_state_machine_status(STATE_WAIT_POWER_OFF);		//设置状态
										init_propor_var();									//清空时间戳//设置比例阀状态为离线
										//fprintf(USART1_STREAM, "1111111111111111111111111111\r\n");
										//fprintf(USART1_STREAM, "STATE_POWER_OFF:%d timer:%ld\r\n",Bit_RESET,s_st_power_ctrl_state.timer);
										break;
	case STATE_WAIT_POWER_OFF:			if ((currentTime - s_st_power_ctrl_state.timer) >= PWR_OFF_TIMEOUT_COUNT)
										{
											s_st_power_ctrl_state.timer = 0;
											set_power_state_machine_status(STATE_POWER_ON);
										//	fprintf(USART1_STREAM, "STATE_WAIT_POWER_OFF-timer:%ld\r\n",currentTime);
										}
										break;
	case STATE_POWER_ON:				set_power_gpio_state(Bit_SET);
										set_power_state_machine_status(STATE_POWER_ON_DELAY);
										s_st_power_ctrl_state.timer = currentTime;
										//fprintf(USART1_STREAM, "STATE_POWER_ON:%d\r\n",Bit_SET);
										break;
	case STATE_POWER_ON_DELAY:			if ((currentTime - s_st_power_ctrl_state.timer) >= PWR_ON_TIMEOUT_COUNT)
										{
											set_power_state_machine_status(STATE_INIT);
										//	USART_Send(USART1_SFR,"POWER ON WAIT\r\n",strlen("POWER ON WAIT\r\n"));
										}
										break;
	case STATE_IDLE:
					  	  	  	  	  	break;
	default:							set_power_state_machine_status(STATE_INIT);
										break;
	}
}

//设置比例阀断电重启
static void set_proporvalve_pwr_off_restart()
{
	set_power_state_machine_status(STATE_POWER_OFF);
	s_st_proporval_offline_state.state = STATE_SYS_NORMAL_PWR_OFF;
}
//比例阀离线断电控制
//设计思路：
//（1）首次系统上电，等待30s一直未通讯，断电一次，此后若不上线则不再断电
//（2）上电后，30s内通讯正常后，若发现断线则断电一次，此后若不上线则不再断电
//设计说明：
//（1）系统首次上电30s内没在线过，则断电一次，此后持续侦测是否在线。
//（2）系统上电后，比例阀通讯由在线变离线后，等待30s不恢复，则断电一次，此后不断电，持续检测是否变为在线，变为在线后，持续侦测是否离线，若离线等待30s，30s内若不在线则断电一次，若在线则回到持续侦测离线状态

static void proporvalve_offline_ctrl_machine_state()
{
	uint32_t currentTime;
	currentTime = SystemtimeClock;
	switch(s_st_proporval_offline_state.state)
	{
	//1.第一次系统上电，启动在线离线侦测
	case STATE_SYS_FIRST_RUN:			s_st_proporval_offline_state.timer = currentTime;
										s_st_proporval_offline_state.state = STATE_SYS_FIRST_RUN_WAIT;
										//fprintf(USART1_STREAM, "--STATE_SYS_FIRST_RUN\r\n");
										break;
	//2.第一次系统上电后，在线时跳转到3.正常运行状态，离线时大于30s断电一次进入6.断电状态，然后进入4.正常运行等待状态
	case STATE_SYS_FIRST_RUN_WAIT:
//										if (stCanCommSta.Proportional_valve.status == ONLINE)
//										{
//											s_st_proporval_offline_state.state = STATE_SYS_NORMAL_RUN;
//											s_st_proporval_offline_state.timer = currentTime;
//										}
//										else
										{
											if ((currentTime - s_st_proporval_offline_state.timer) > FIRST_RUN_TIMEOUT_COUNT)//大于2s,断电一次
											{
												s_st_proporval_offline_state.timer = currentTime;
												set_power_state_machine_status(STATE_POWER_OFF);
												s_st_proporval_offline_state.state = STATE_SYS_NORMAL_PWR_OFF;
												//fprintf(USART1_STREAM, "--STATE_SYS_FIRST_RUN_WAIT over\r\n");
											}
										}
										break;
	//3.正常运行状态，实时侦测是否变为离线，若离线则启动5.正常掉段延迟状态，等待30s内，若一直离线，则断电
	case STATE_SYS_NORMAL_RUN:			if (stCanCommSta.Proportional_valve.status == OFFLINE)//实时监测比例阀通讯是否异常，异常时，若超过30s则断电一次
										{
											s_st_proporval_offline_state.state = STATE_SYS_NORMAL_PWR_OFF_DELAY;
											//fprintf(USART1_STREAM, "--STATE_SYS_NORMAL_PWR_OFF_DELAY start\r\n");
										}
										s_st_proporval_offline_state.timer = currentTime;
										break;
	//4.正常运行等待状态，此状态为离线后，实时侦测，若发现上线，则回到3.正常运行状态
	case STATE_SYS_NORMAL_WAIT:			if (stCanCommSta.Proportional_valve.status == ONLINE)//什么时候恢复通讯，什么时候启动检测掉线断电
											s_st_proporval_offline_state.state = STATE_SYS_NORMAL_RUN;

										s_st_proporval_offline_state.timer = currentTime;
										break;

	//5.正常掉段延迟状态，此状态为在发送离线后，等待30s，若30s内上线，则不断电并回到3.正常运行状态，若超过30s则进入6.断电状态，然后进入4.正常运行等待状态
	case STATE_SYS_NORMAL_PWR_OFF_DELAY:if (stCanCommSta.Proportional_valve.status == ONLINE)
										{
											s_st_proporval_offline_state.state = STATE_SYS_NORMAL_RUN;
											s_st_proporval_offline_state.timer = currentTime;
										}
										else
										{
											if ((currentTime - s_st_proporval_offline_state.timer) > TIMEOUT_COUNT)//大于30s,断电一次
											{
												s_st_proporval_offline_state.timer = currentTime;
												set_power_state_machine_status(STATE_POWER_OFF);
												s_st_proporval_offline_state.state = STATE_SYS_NORMAL_PWR_OFF;
												//fprintf(USART1_STREAM, "--STATE_SYS_NORMAL_PWR_OFF_DELAY over\r\n");
											}
										}
										break;
	//6.断电状态
	case STATE_SYS_NORMAL_PWR_OFF:		if (get_power_state_machine_status() != STATE_IDLE)//启动断电流程，此流程不使用delay_ms()死循环函数，不影响总线通讯
										{
											power_ctrl_state_machine();
										}
										else
										{
											s_st_proporval_offline_state.state = STATE_SYS_NORMAL_WAIT;
										}
										s_st_proporval_offline_state.timer = currentTime;
										break;

	default:							s_st_proporval_offline_state.state = STATE_SYS_FIRST_RUN;
										break;
	}
}
//---------------------------------------------------------------------------------------------

void Check_SysErr_Alarm(void)
{
	int64_t currentTime		= 0;
//	static uint32_t oldSysTime 	= 0;
//	static uint16_t usRuningTime = 0;

	currentTime = SystemtimeClock;

	/*
	 * 整车CAN连接情况
	 * */
	if(currentTime-stCanCommSta.stVehicle.oldSysTime >= VEHICLE_CAN_OFFLINE_PERIOD)
	{
		if((CAN2_SFR->CTLR & (0x00000001<<23))!=0){
			if(CAN2_BUS_OFF_Count < 5){
				CAN_check_busoff(CAN2_SFR);//CAN2_INIT(stCanPara.can2rate,NULL);//比例阀
				CAN2_BUS_OFF_Count+=1;
			}else{
				if((SystemtimeClock - CAN2_BUS_ON_TimeStamp) > 1000){
					CAN_check_busoff(CAN2_SFR);//CAN2_INIT(stCanPara.can2rate,NULL);//比例阀
					CAN2_BUS_ON_TimeStamp = SystemtimeClock;
				}
			}
		}
		stCanCommSta.stVehicle.status = OFFLINE;
	}
	else
	{
		if(CAN2_BUS_OFF_Count != 0){
			if((SystemtimeClock - CAN2_BUS_ON_TimeStamp) > 10000){
				CAN2_BUS_OFF_Count = 0x00;
			}
		}else{
			CAN2_BUS_ON_TimeStamp = SystemtimeClock;
		}
		stCanCommSta.stVehicle.status = ONLINE;
	}

	/*
	 * 车速can链接情况
	 * */
	if(currentTime - stCanCommSta.stSpeed.oldSysTime >= VEHICLE_CAN_OFFLINE_PERIOD)
	{
		if((CAN2_SFR->CTLR & (0x00000001<<23))!=0){
			if(CAN2_BUS_OFF_Count < 5){
				CAN_check_busoff(CAN2_SFR);//CAN2_INIT(stCanPara.can2rate,NULL);//比例阀
				CAN2_BUS_OFF_Count+=1;
			}else{
				if((SystemtimeClock - CAN2_BUS_ON_TimeStamp) > 1000){
					CAN_check_busoff(CAN2_SFR);//CAN2_INIT(stCanPara.can2rate,NULL);//比例阀
					CAN2_BUS_ON_TimeStamp = SystemtimeClock;
				}
			}
		}
		stCanCommSta.stSpeed.status = OFFLINE;
		//gcz 2022-05-19 CAN丢失档位需要报出异常状态
		if(rParms.Vehicle_State_Obtain_Method == 0)
			stVehicleParas.ReverseGear = -1;
		//stVehicleParas.fVehicleSpeed = 0;
	}
	else
	{
		if(CAN2_BUS_OFF_Count != 0){
			if((SystemtimeClock - CAN2_BUS_ON_TimeStamp) > 10000){
				CAN2_BUS_OFF_Count = 0x00;
			}
		}else{
			CAN2_BUS_ON_TimeStamp = SystemtimeClock;
		}
		stCanCommSta.stSpeed.status = ONLINE;
	}

	/*
	 * 转向CAN
	 *
	if(currentTime - stCanCommSta.stTurnSig.oldSysTime >= VEHICLE_CAN_OFFLINE_PERIOD)
	{
		stCanCommSta.stTurnSig.status = OFFLINE;
	}
	if(currentTime - stCanCommSta.stBrakeSig.oldSysTime >= VEHICLE_CAN_OFFLINE_PERIOD)
	{
		stCanCommSta.stBrakeSig.status = OFFLINE;
	}
	 */
	/*
	 * 比例阀CAN
	 * */
	static uint8_t state = 0;
	if(currentTime - stCanCommSta.Proportional_valve.oldSysTime >= VALVE_CAN_OFFLINE_PERIOD)
	{
		if((CAN3_SFR->CTLR & (0x00000001<<23))!=0){
			if(CAN3_BUS_OFF_Count < 5){
				CAN_check_busoff(CAN3_SFR);//CAN3_INIT(stCanPara.can3rate,NULL);//比例阀
				CAN3_BUS_OFF_Count+=1;
			}else{
				if((SystemtimeClock - CAN3_BUS_ON_TimeStamp) > 1000){
					CAN_check_busoff(CAN3_SFR);//CAN3_INIT(stCanPara.can3rate,NULL);//比例阀
					CAN3_BUS_ON_TimeStamp = SystemtimeClock;
				}
			}
		}
		stCanCommSta.Proportional_valve.status = OFFLINE;
		state = 0;
	}
	else
	{
		if(CAN3_BUS_OFF_Count != 0){
			if((SystemtimeClock - CAN3_BUS_ON_TimeStamp) > 10000){
				CAN3_BUS_OFF_Count = 0x00;
			}
		}else{
			CAN3_BUS_ON_TimeStamp = SystemtimeClock;
		}

		stCanCommSta.Proportional_valve.status = ONLINE;

		//gcz 2022-06-09 兼容比例阀启动初始化报02状态的BUG，简易状态机
//		_STRUCT_VALVE_PARA  *p_st_valve_para = get_valveparas();
//		if ((p_st_valve_para->Valve_State == 0x02)//0x02 Fail Safe状态需要断电重启
//			|| ((p_st_valve_para->Fault_Code == 1)//Internal fault
//			||(p_st_valve_para->Fault_Code == 2)//Low voltage
//			||(p_st_valve_para->Fault_Code == 3)//High voltage
//			||(p_st_valve_para->Fault_Code == 4)//Big pressure delta
//			||(p_st_valve_para->Fault_Code == 8)//Pressure sensor open circuit
//			||(p_st_valve_para->Fault_Code == 9)//Pressure sensor short circuit
//			||(p_st_valve_para->Fault_Code == 10)//Proportional valve open circuit
//			||(p_st_valve_para->Fault_Code == 11)))//Proportional valve short circuit
//		{
//			uint8_t buffer[50];
//			memset(buffer,0,sizeof(buffer));
//			sprintf(buffer,"1.state:[%d] Fault_Code:[%d]\r\n",p_st_valve_para->Valve_State,p_st_valve_para->Fault_Code);
//			USART_Send(USART1_SFR,buffer,strlen(buffer));
//		}
		static uint32_t timer = 0;
		if (state == 0)//初始化状态，此时获取是否完成上电，上电完成后，过滤2秒不再检测02状态
		{
			state = 1;//进入2秒等待
			timer = currentTime;
		//	USART_Send(USART1_SFR,"--begin wait 2s\r\n",strlen("begin wait 2s\r\n"));
		}
		else if (state == 1)
		{
			if ((currentTime - timer) >= 2000)
			{
				state = 2;//进入正常检测状态
		//		USART_Send(USART1_SFR,"++finish wait 2s\r\n",strlen("finish wait 2s\r\n"));
			}
		}
		else if (state == 2)
		{
			//gcz 2022-05-24
			_STRUCT_VALVE_PARA  *p_st_valve_para = get_valveparas();
			if ((p_st_valve_para->Valve_State == 0x02)//0x02 Fail Safe状态需要断电重启
				|| ((p_st_valve_para->Fault_Code == 1)//Internal fault
				||(p_st_valve_para->Fault_Code == 2)//Low voltage
				||(p_st_valve_para->Fault_Code == 3)//High voltage
				||(p_st_valve_para->Fault_Code == 4)//Big pressure delta
				||(p_st_valve_para->Fault_Code == 8)//Pressure sensor open circuit
				||(p_st_valve_para->Fault_Code == 9)//Pressure sensor short circuit
				||(p_st_valve_para->Fault_Code == 10)//Proportional valve open circuit
				||(p_st_valve_para->Fault_Code == 11)))//Proportional valve short circuit

			{
				//已经关过一次电的则不再重复触发，必须每次执行完成后才能再次触发
				if ((s_st_proporval_offline_state.state != STATE_SYS_NORMAL_PWR_OFF)
					&& (get_power_state_machine_status() == STATE_IDLE))
				{
					set_proporvalve_pwr_off_restart();

					uint8_t buffer[50];
					memset(buffer,0,sizeof(buffer));
					sprintf(buffer,"state:[%d] Fault_Code:[%d]\r\n",p_st_valve_para->Valve_State,p_st_valve_para->Fault_Code);
//					USART_Send(USART1_SFR,buffer,strlen(buffer));
					Usart1_DMA_Transmit(buffer,strlen(buffer));

				}
			}
		}
	}
	//gcz 2022-05-24
	//比例阀离线控制状态机，发现离线超过30秒，则断电一次，仅断电一次，不循环断电。变为在线后重新此侦测是否离线
	proporvalve_offline_ctrl_machine_state();
	/*
	 * 小屏幕CAN
	 * */
	if(currentTime - stCanCommSta.stDisplayer.oldSysTime >= VALVE_CAN_OFFLINE_PERIOD)
	{
		if((CAN4_SFR->CTLR & (0x00000001<<23))!=0){
			if(CAN4_BUS_OFF_Count < 5){
				CAN_check_busoff(CAN4_SFR);//CAN4_INIT(stCanPara.can4rate,NULL);//比例阀
				CAN4_BUS_OFF_Count+=1;
			}else{
				if((SystemtimeClock - CAN4_BUS_ON_TimeStamp) > 1000){
					CAN_check_busoff(CAN4_SFR);//CAN4_INIT(stCanPara.can4rate,NULL);//比例阀
					CAN4_BUS_ON_TimeStamp = SystemtimeClock;
				}
			}
		}
		stCanCommSta.stDisplayer.status = OFFLINE;
		sw_hw_version.Got_Version_Info_Displayer = 0x00;
	}
	else
	{
		if(CAN4_BUS_OFF_Count != 0){
			if((SystemtimeClock - CAN4_BUS_ON_TimeStamp) > 10000){
				CAN4_BUS_OFF_Count = 0x00;
			}
		}else{
			CAN4_BUS_ON_TimeStamp = SystemtimeClock;
		}

		stCanCommSta.stDisplayer.status = ONLINE;
		sw_hw_version.Got_Version_Info_Displayer = 0x00;
	}

	/*
	 * 毫米波雷达can
	 * */
	if(currentTime - stCanCommSta.stRadar.oldSysTime >= RADAR_CAN_OFFLINE_PERIOD)
	{
		if((CAN1_SFR->CTLR & (0x00000001<<23))!=0){
			if(CAN1_BUS_OFF_Count < 5){
				CAN_check_busoff(CAN1_SFR);//CAN_check_busoff(CAN0_SFR);//CAN0_INIT(stCanPara.can0rate,NULL);//比例阀
				CAN1_BUS_OFF_Count+=1;
			}else{
				if((SystemtimeClock - CAN1_BUS_ON_TimeStamp) > 1000){
					CAN_check_busoff(CAN1_SFR);//CAN_check_busoff(CAN0_SFR);//CAN0_INIT(stCanPara.can0rate,NULL);//比例阀
					CAN1_BUS_ON_TimeStamp = SystemtimeClock;
				}
			}
		}
		stCanCommSta.stRadar.status = OFFLINE;
		sw_hw_version.Got_Version_Info_MRadar = 0x00;
	}
	else
	{
		if(CAN1_BUS_OFF_Count != 0){
			if((SystemtimeClock - CAN1_BUS_ON_TimeStamp) > 10000){
				CAN1_BUS_OFF_Count = 0x00;
			}
		}else{
			CAN1_BUS_ON_TimeStamp = SystemtimeClock;
		}
		stCanCommSta.stRadar.status = ONLINE;
		sw_hw_version.Got_Version_Info_MRadar = 0x00;
	}

	/*
	 *超声波雷达
	 * */
	if(currentTime - stCanCommSta.stHRadar.oldSysTime >= HRADAR_CAN_OFFLINE_PERIOD)
	{
		if((CAN5_SFR->CTLR & (0x00000001<<23))!=0){
			if(CAN5_BUS_OFF_Count < 5){
				CAN_check_busoff(CAN5_SFR);//CAN5_INIT(stCanPara.can5rate,NULL);//比例阀
				CAN5_BUS_OFF_Count+=1;
			}else{
				if((SystemtimeClock - CAN5_BUS_ON_TimeStamp) > 1000){
					CAN_check_busoff(CAN5_SFR);//CAN5_INIT(stCanPara.can5rate,NULL);//比例阀
					CAN5_BUS_ON_TimeStamp = SystemtimeClock;
				}
			}
		}
		stCanCommSta.stHRadar.status = OFFLINE;
		sw_hw_version.Got_Version_Info_URadar = 0x00;
	}
	else
	{
		if(CAN5_BUS_OFF_Count != 0){
			if((SystemtimeClock - CAN5_BUS_ON_TimeStamp) > 10000){
				CAN5_BUS_OFF_Count = 0x00;
			}
		}else{
			CAN5_BUS_ON_TimeStamp = SystemtimeClock;
		}
		stCanCommSta.stHRadar.status = ONLINE;
		sw_hw_version.Got_Version_Info_URadar = 0x00;
	}
	//摄像机can
	if(currentTime - stCanCommSta.stCamera.oldSysTime >= CAMERA_CAN_OFFLINE_PERIOD)
	{
		if((CAN0_SFR->CTLR & (0x00000001<<23))!=0){
			if(CAN0_BUS_OFF_Count < 5){
				CAN_check_busoff(CAN0_SFR);//CAN1_INIT(stCanPara.can1rate,NULL);//比例阀
				CAN0_BUS_OFF_Count+=1;
			}else{
				if((SystemtimeClock - CAN0_BUS_ON_TimeStamp) > 1000){
					CAN_check_busoff(CAN0_SFR);//CAN1_INIT(stCanPara.can1rate,NULL);//比例阀
					CAN0_BUS_ON_TimeStamp = SystemtimeClock;
				}
			}
		}
		stCanCommSta.stCamera.status = OFFLINE;

		sw_hw_version.Got_Version_Info_Camera = 0x00;

	}
	else
	{
		if(CAN0_BUS_OFF_Count != 0){
			if((SystemtimeClock - CAN1_BUS_ON_TimeStamp) > 10000){
				CAN0_BUS_OFF_Count = 0x00;
			}
		}else{
			CAN0_BUS_ON_TimeStamp = SystemtimeClock;
		}
		stCanCommSta.stCamera.status = ONLINE;

		sw_hw_version.Got_Version_Info_Camera = 0x00;
	}

	//gcz 2022-08-10
	//角雷达can
	if(currentTime - stCanCommSta.stSrr.oldSysTime >= ANGLE_RADAR_CAN_OFFLINE_PERIOD)
	{
		if((CAN5_SFR->CTLR & (0x00000001<<23))!=0){
			if(CAN5_BUS_OFF_SRR_Count < 5){
				CAN_check_busoff(CAN5_SFR);//CAN_check_busoff(CAN0_SFR);//CAN0_INIT(stCanPara.can0rate,NULL);//比例阀
				CAN5_BUS_OFF_SRR_Count+=1;
			}else{
				if((SystemtimeClock - CAN5_BUS_ON_SRR_TimeStamp) > 1000){
					CAN_check_busoff(CAN5_SFR);//CAN_check_busoff(CAN0_SFR);//CAN0_INIT(stCanPara.can0rate,NULL);//比例阀
					CAN5_BUS_ON_SRR_TimeStamp = SystemtimeClock;
				}
			}
		}
		stCanCommSta.stSrr.status = OFFLINE;
	}
	else
	{
		if(CAN5_BUS_OFF_SRR_Count != 0){
			if((SystemtimeClock - CAN5_BUS_ON_SRR_TimeStamp) > 10000){
				CAN5_BUS_OFF_SRR_Count = 0x00;
			}
		}else{
			CAN5_BUS_ON_SRR_TimeStamp = SystemtimeClock;
		}
		stCanCommSta.stSrr.status = ONLINE;
	}
	//4G网络连接
	if(server_info.isConnect) stCanCommSta.stWireless.oldSysTime = currentTime;	// 控制4G灯指示状态
	if(currentTime - stCanCommSta.stWireless.oldSysTime >= VALVE_NET_OFFLINE_PERIOD)
	{
		stCanCommSta.stWireless.status = OFFLINE;
	}
	else
	{
		stCanCommSta.stWireless.status = ONLINE;
	}
	//gcz 2022-05-23
	//判断版本是否需要清零
	can_device_prompt_msg_and_clear_version();
	//gcz 2022-05-30
	//动态感知版本信息，对其状态变化时刷新版本
	refresh_sys_version_info(&device_version_str);
}

void DisPlay_Message_Analy(uint8_t *buf)
{
	if((buf[0] & 0x01) == 0)
		warning_status.AEBstatus = warning_status.AEBstatus | 0x02;
	else
		warning_status.AEBstatus = warning_status.AEBstatus & 0xFD;

	if((buf[0] & 0x02) == 0)
		warning_status.LDWstatus = 1;
	else
		warning_status.LDWstatus = 0;

	//fprintf(USART1_STREAM,"%s",buf);
}

void Send_Display_Message(void)
{
	static uint32_t DisplayOldtime 	= 0;
	static uint32_t DisplayOldtime2 = 0;
	static bool flag 				= false;
	struct can_frame displayer;

	// 在OTA分包下载过程中，不与小屏幕通讯
	if(upgrade_info.step == UPGRADE_URL) return ;
	if(upgrade_info.step == UPGRADE_SUB_DMN) return ;

	if((SystemtimeClock - DisplayOldtime) > 100)
	{
		DisplayOldtime 	= SystemtimeClock;
		DisplayOldtime2 = SystemtimeClock;
		flag 			= true;
		Displayer_CANTx_Get(&displayer);
		CAN_Transmit_DATA(CAN4_SFR,displayer);
	}

	if( flag && ((SystemtimeClock - DisplayOldtime2) > 1))
	{
		flag 			= false;
		Displayer_CANTx_Radar_Get(&displayer);
		CAN_Transmit_DATA(CAN4_SFR,displayer);

	}
}

void Send_Display_Message_WriteSNPN(uint32_t sn, uint32_t pn){
	struct can_frame write_snpn;
	Displayer_CANTx_WriteSNPN_Get(&write_snpn, sn, pn);
	CAN_Transmit_DATA(CAN4_SFR,write_snpn);
}
void Send_Display_Message_VoiveNum(uint8_t voice_num){
	struct can_frame frame_voicenum;
	Displayer_CANTx_VoiceNum_Get(&frame_voicenum, voice_num);
	CAN_Transmit_DATA(CAN4_SFR, frame_voicenum);
}


void Send_Brake_Control(void)
{
	struct can_frame tx_frame_valve;
//	float pressure_output;
	Deceleration_Compare decelerate_output;
//	uint16_t can_data_byd_aeb_dec;
	Deceleration_Get(&decelerate_output);
//	if (decelerate_output > 0)
//		gcz_serial_v1_dma_printf(SERIAL_UART1,"dec:%0.4f\r\n",decelerate_output);
	Deceleration_To_Vehicle(&tx_frame_valve, &decelerate_output, server_info.vehicle_type);
	/*
	 * if(Brake_Mothod == PWM){
		pressure_output = decelerate_output * rParms.Ratio_Force_To_Deceleration;
		PWM_Control_Dec_to_pwm(pressure_output);
	}
	*/
	if(g_sensor_mgt.m_actuator.m_isOpen)
	{
		switch(server_info.vehicle_type){
		case JINLV_BUS:
				CAN_Transmit_DATA(CAN3_SFR, tx_frame_valve);
			break;
		case BYD_E6:
				CAN_Transmit_DATA(CAN2_SFR, tx_frame_valve);
			break;
		case JIEFANG_TRUCK:
				CAN_Transmit_DATA(CAN3_SFR, tx_frame_valve);
			break;
		case TEST_VEHICLE:
				CAN_Transmit_DATA(CAN3_SFR, tx_frame_valve);
			break;
		default:
				CAN_Transmit_DATA(CAN3_SFR, tx_frame_valve);
			break;
		}
	}
}

void Sys_Send_Brake(void)
{
	static uint32_t ulPreTime = 0;

	if (SystemtimeClock - ulPreTime >= 20)
	{
		ulPreTime = SystemtimeClock;
		/* 输出刹车控制 */
		Send_Brake_Control();
	}
}

//gcz
//说明：获取stValveParas指针
//返回：stValveParas指针
_STRUCT_VALVE_PARA *get_valveparas()
{
	return &stValveParas;
}

/*********************************** 车CAN动态解析 **********************************************/

#if CAR_CAN_ANALY_IS_ENABLE		// add lmz 20221017
// 获取车CAN解析句柄
CAR_CAN_Parse_Handle * Get_Car_CAN_Dynamic_Parse_Handle()
{
	return &g_cc_ph;
}
// 拷贝过程
void Car_Can_Dynamic_Parse_In_Can2_Recv(struct can_frame rx_frame)
{
	Car_Can_Object_Str *l_cc_cfg_handle = Get_Car_Can_Analysis_Cfg_Param();
	// 获取车速
	if((l_cc_cfg_handle->car_speed.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_speed.frame_id){
			g_cc_ph.car_speed.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.car_speed.data, rx_frame.data, 8);
		}
	}

	// 车左转向
	if((l_cc_cfg_handle->car_turn_left.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_turn_left.frame_id){
			g_cc_ph.turn_left.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.turn_left.data, rx_frame.data, 8);
		}
	}

	// 车右转向
	if((l_cc_cfg_handle->car_turn_right.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_turn_right.frame_id){
			g_cc_ph.turn_right.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.turn_right.data, rx_frame.data, 8);
		}
	}

	// 司机刹车
	if((l_cc_cfg_handle->car_driver_brake.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_driver_brake.frame_id){
			g_cc_ph.driver_brake.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.driver_brake.data, rx_frame.data, 8);
		}
	}

	// 档位
	if((l_cc_cfg_handle->car_gear.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_gear.frame_id){
			g_cc_ph.car_gear.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.car_gear.data, rx_frame.data, 8);
		}
	}

	// 里程数
	if((l_cc_cfg_handle->car_odom.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_odom.frame_id){
			g_cc_ph.car_odom.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.car_odom.data, rx_frame.data, 8);
		}
	}

	// 方向盘转角
	if((l_cc_cfg_handle->car_steer_wheel_angle.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_steer_wheel_angle.frame_id){
			g_cc_ph.steer_wheel_angle.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.steer_wheel_angle.data, rx_frame.data, 8);
		}
	}

	// 油门深度
	if((l_cc_cfg_handle->car_throttle_depth.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_throttle_depth.frame_id){
			g_cc_ph.throttle_depth.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.throttle_depth.data, rx_frame.data, 8);
		}
	}

	// 刹车深度
	if((l_cc_cfg_handle->car_brake_depth.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_brake_depth.frame_id){
			g_cc_ph.brake_depth.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.brake_depth.data, rx_frame.data, 8);
		}
	}

	// 航偏角
	if((l_cc_cfg_handle->car_yaw_angle.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_yaw_angle.frame_id){
			g_cc_ph.yaw_angle.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.yaw_angle.data, rx_frame.data, 8);
		}
	}

	// 发动机转速
	if((l_cc_cfg_handle->car_engine_speed.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_engine_speed.frame_id){
			g_cc_ph.engine_speed.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.engine_speed.data, rx_frame.data, 8);
		}
	}

	// 俯仰角
	if((l_cc_cfg_handle->car_pitch_angle.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_pitch_angle.frame_id){
			g_cc_ph.pitch_angle.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.pitch_angle.data, rx_frame.data, 8);
		}
	}

	// 加速度
	if((l_cc_cfg_handle->car_accelerated_speed.config_info & 0x01) == CAR_CAN_USED){	// 车CAN功能开启
		if(rx_frame.TargetID == l_cc_cfg_handle->car_accelerated_speed.frame_id){
			g_cc_ph.accelerated_speed.flgBoxRxEnd = 1;
			memcpy(g_cc_ph.accelerated_speed.data, rx_frame.data, 8);
		}
	}
}
// 解析过程
void Car_Can_Dynamic_Parse_In_Vehicle_Parameter_Analysis(uint32_t ulSysTime)
{
#define 	CC_DEBUG_PRINT		1		// debug print [0:close] [1:open]
	// 车速
	if(g_cc_ph.car_speed.flgBoxRxEnd == 1){
		g_cc_ph.car_speed.flgBoxRxEnd 			= 0;
		if(stSpeedPara.uSingnalType == 0){			//信号来源	0:CAN报文；1：I/O输入
			stCanCommSta.stVehicle.oldSysTime 	= ulSysTime;
			stCanCommSta.stSpeed.oldSysTime 	= ulSysTime;
			stVehicleParas.fVehicleSpeed = Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_CAR_SPEED, g_cc_ph.car_speed.data);
			//车速滤波
//			float fFilteredSpeed = fVehicleSpeedFilter(stVehicleParas.fVehicleSpeed);
//			stVehicleParas.fVehicleSpeed = fFilteredSpeed + 0.001; // To solve precision loss problem
#if CC_DEBUG_PRINT
			USART1_Bebug_Print_Flt("fVehicleSpeed", stVehicleParas.fVehicleSpeed, 1, 1);	// debug print
#endif
		}
	}
	// 左转向
	if(g_cc_ph.turn_left.flgBoxRxEnd == 1){
		g_cc_ph.turn_left.flgBoxRxEnd 			= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.LeftFlag = (uint8_t)Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_TURN_LEFT, g_cc_ph.turn_left.data);
		if(stVehicleParas.LeftFlag != 0)	// debug print
#if CC_DEBUG_PRINT
			USART1_Bebug_Print_Num("LeftFlag", stVehicleParas.LeftFlag, 1, 1);
#endif
	}

	// 右转向
	if(g_cc_ph.turn_right.flgBoxRxEnd == 1){
		g_cc_ph.turn_right.flgBoxRxEnd 			= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.RightFlag = (uint8_t)Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_TURN_RIGHT, g_cc_ph.turn_right.data);
		if(stVehicleParas.RightFlag != 0)	// debug print
#if CC_DEBUG_PRINT
			USART1_Bebug_Print_Num("RightFlag", stVehicleParas.RightFlag, 1, 1);
#endif
	}

	// 司机刹车
	if(g_cc_ph.driver_brake.flgBoxRxEnd == 1){
		g_cc_ph.driver_brake.flgBoxRxEnd 		= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.BrakeFlag = (uint8_t)Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_DRIVER_BRAKE, g_cc_ph.driver_brake.data);
		if(stVehicleParas.BrakeFlag != 0)	// debug print
#if CC_DEBUG_PRINT
			USART1_Bebug_Print_Num("BrakeFlag", stVehicleParas.BrakeFlag, 1, 1);
#endif
	}

	// 档位
	if(g_cc_ph.car_gear.flgBoxRxEnd == 1){
		g_cc_ph.car_gear.flgBoxRxEnd 			= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.car_gear = (uint8_t)Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_GEAR, g_cc_ph.car_gear.data);
#if CC_DEBUG_PRINT
//		USART1_Bebug_Print_Num("car_gear", stVehicleParas.car_gear, 1, 1);
		switch(stVehicleParas.car_gear){		// debug print
		case 1:USART1_Bebug_Print("P Gear", "", 1);break;
		case 2:USART1_Bebug_Print("R Gear", "", 1);break;
		case 3:USART1_Bebug_Print("N Gear", "", 1);break;
		case 4:USART1_Bebug_Print("D Gear", "", 1);break;
		default:USART1_Bebug_Print("Unkown Gear", "", 1);break;
		}
#endif
	}

	// 里程数
	if(g_cc_ph.car_odom.flgBoxRxEnd == 1){
		g_cc_ph.car_odom.flgBoxRxEnd 			= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.VehicleOdom_Total = Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_ODOM, g_cc_ph.car_odom.data);
		// 单位是米，需要转成千米
		Car_Can_Object_Str *l_cc_cfg_handle = Get_Car_Can_Analysis_Cfg_Param();
		if(((l_cc_cfg_handle->car_odom.config_info >> 2) & 0x07) == 1){
			stVehicleParas.VehicleOdom_Total /= 1000;
		}
#if CC_DEBUG_PRINT
		USART1_Bebug_Print_Flt("VehicleOdom_Total", stVehicleParas.VehicleOdom_Total, 1, 1);	// debug print
#endif
	}

	// 方向盘转角
	if(g_cc_ph.steer_wheel_angle.flgBoxRxEnd == 1){
		g_cc_ph.steer_wheel_angle.flgBoxRxEnd 	= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.steer_wheel_angle = Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_STEER_WHEEL, g_cc_ph.steer_wheel_angle.data);

		// 判断方向盘右打时计数表示，（右打计数增加，当计数增加时就是右打；否则就是左打）
		static float steer_wheel_angle_old = 0.0f;
		Car_Can_Object_Str *l_cc_cfg_handle = Get_Car_Can_Analysis_Cfg_Param();
		if(((l_cc_cfg_handle->car_steer_wheel_angle.config_info >> 5) & 0x07) == 0){	// 计数增加为右打
			if(stVehicleParas.steer_wheel_angle > steer_wheel_angle_old){					// 右打
				stVehicleParas.steer_wheel_direction = 1;
			}else{																		// 左打
				stVehicleParas.steer_wheel_direction = 0;
			}
		}else{																			// 计数减少为右打
			if(stVehicleParas.steer_wheel_angle < steer_wheel_angle_old){					// 右打
				stVehicleParas.steer_wheel_direction = 1;
			}else{																		// 左打
				stVehicleParas.steer_wheel_direction = 0;
			}
		}
		steer_wheel_angle_old = stVehicleParas.steer_wheel_angle;
#if CC_DEBUG_PRINT
		USART1_Bebug_Print_Flt("steering_angle", stVehicleParas.steer_wheel_angle, 1, 1);
#endif
	}

	// 油门深度
	if(g_cc_ph.throttle_depth.flgBoxRxEnd == 1){
		g_cc_ph.throttle_depth.flgBoxRxEnd 		= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.ThrottleOpening = (uint8_t)Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_THROTTAL_DEPTH, g_cc_ph.throttle_depth.data);
#if CC_DEBUG_PRINT
		USART1_Bebug_Print_Num("ThrottleOpening", stVehicleParas.ThrottleOpening, 1, 1);	// debug print
#endif
	}

	// 刹车深度
	if(g_cc_ph.brake_depth.flgBoxRxEnd == 1){
		g_cc_ph.brake_depth.flgBoxRxEnd 		= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.BrakeOpening = (uint8_t)Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_BRAKE_DEPTH, g_cc_ph.brake_depth.data);
#if CC_DEBUG_PRINT
		USART1_Bebug_Print_Num("BrakeOpening", stVehicleParas.BrakeOpening, 1, 1);	// debug print
#endif
	}

	// 偏航角
	if(g_cc_ph.yaw_angle.flgBoxRxEnd == 1){
		g_cc_ph.yaw_angle.flgBoxRxEnd 			= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.yaw_angle = Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_YAW_ANGLE, g_cc_ph.yaw_angle.data);
#if CC_DEBUG_PRINT
		USART1_Bebug_Print_Flt("yaw_angle", stVehicleParas.yaw_angle, 1, 1);	// debug print
#endif
	}

	// 发送机转速
	if(g_cc_ph.engine_speed.flgBoxRxEnd == 1){
		g_cc_ph.engine_speed.flgBoxRxEnd 		= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.engine_speed = Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_ENGINE_SPEED, g_cc_ph.engine_speed.data);
#if CC_DEBUG_PRINT
		USART1_Bebug_Print_Flt("engine_speed", stVehicleParas.engine_speed, 1, 1);	// debug print
#endif
	}

	// 俯仰角
	if(g_cc_ph.pitch_angle.flgBoxRxEnd == 1){
		g_cc_ph.pitch_angle.flgBoxRxEnd 		= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.pitch_angle = Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_PITCH_ANGLE, g_cc_ph.pitch_angle.data);
#if CC_DEBUG_PRINT
		USART1_Bebug_Print_Flt("pitch_angle", stVehicleParas.pitch_angle, 1, 1);	// debug print
#endif
	}

	// 加速度
	if(g_cc_ph.accelerated_speed.flgBoxRxEnd == 1){
		g_cc_ph.accelerated_speed.flgBoxRxEnd 	= 0;
		stCanCommSta.stVehicle.oldSysTime 		= ulSysTime;
		stVehicleParas.accelerated_speed = Car_Can_Decode_Signal_Data(CAR_CAN_ANALY_ACCELERATION, g_cc_ph.accelerated_speed.data);
#if CC_DEBUG_PRINT
		if(stVehicleParas.accelerated_speed != 0.0)	// debug print
			USART1_Bebug_Print_Flt("accelerated_speed", stVehicleParas.accelerated_speed, 1, 1);
#endif
	}
}

#endif
