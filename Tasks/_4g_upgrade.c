/*
 * _4g_upgrade.c
 *
 *  Created on: 2021-12-3
 *      Author: Administrator
 * 功能说明：OTA升级
 */
#include "_4g_upgrade.h"
#include "usart.h"
#include "stdlib.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "utc_time.h"
#include "common.h"
#include "_4g_data_upload.h"
#include "_4g_para_config.h"
#include "_4g_server.h"
#include "flash.h"
#include "aeb_sensor_management.h"
/****************************宏、枚举定义**************************************/
#define SCREEN_OTA_START_FRAME		0x7F0
#define SCREEN_OTA_TRANS_FRAME		0x7F1
#define SCREEN_OTA_END_FRAME		0x7F2
#define SCREEN_OTA_REPLY_FRAME		0x7F3
/****************************结构体定义**************************************/

/****************************全局变量**************************************/
// OTA
Upgrade_Info 	upgrade_info 		= {0};
OTA_RX_PKG 		down_pkg 			= {0};		// OTA接收包
NodeData_S		noteData_upgrade_share = {0};
/****************************函数声明**************************************/
//void 	Set_EC200U_Baud_OTA(bool isNormalBaud);
bool  	Check_Whether_Upgrade();
void 	AT_QHTTPURL_Set_URL_Length();
void 	AT_QHTTPURL_Set_URL_Content();
void 	AT_QHTTPURL_Set_Sub_PKG_Info();
void 	AT_QHTTPURL_Get_Sub_PKG_Data();
bool 	Request_OTA_Request_Update_CMD_ASCII();

/*
 * sscanf()函数会调用该函数
 */
char getche(void)
{
	return getchar();
}
/****************************OTA****************************************/
void OTA_Sub_PKG_DWN_Overtime()
{
	if(down_pkg.cnt == 0){
		USART1_Bebug_Print("ERROR", "OTA Over Time.", 1);
//		USART1_Bebug_Print_Num("down_pkg.cnt", down_pkg.cnt, 1, 1);

		AT_QHTTPURL_Set_Sub_PKG_Info();
	}
}
/*
 * 升级过程中提醒
 * 说明：升级中，控制4G指示灯闪烁
 */
uint32_t upgrade_Warning_Clk = 0;
volatile uint8_t overtime_times = 0;
volatile uint8_t overtime_next_times = 0;
void Upgrade_Warning_And_OTA_Overtime_Judge()
{
	if(SystemtimeClock - upgrade_Warning_Clk > 50){
		upgrade_Warning_Clk = SystemtimeClock;

		GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_0);	// 4G指示灯

		if(down_pkg.overtime_flag){
			if(overtime_times > 19){
				overtime_times = 0;
				OTA_Sub_PKG_DWN_Overtime();
			}else{
				if(down_pkg.cnt != 0) overtime_times = 0;
				else overtime_times++;
			}
		}
	}
}
/*
 * 4G模块初始化
 * 直吐模式
 */
void EC200U_Upgrade_Init()
{
	upgrade_info.isUpgradeOK 	= 0;

	// 获取升级版本号
	upgrade_info.size 			= 0;
	upgrade_info.rx_mode		= RECV_CMD;
	upgrade_info.upgrade_mode	= UPGRADE_NO;
	upgrade_info.step			= WAIT;
	upgrade_info.ota_reconnect_server  		= 0;

	// 上电后，APP（应用程序）需要检测OTA升级回复平台状态
	upgrade_info.app_isUpgrade = Get_APP_IS_Upgrade_Flag();
	switch(upgrade_info.app_isUpgrade){		// update lmz 20221021
	case 1:{USART1_Bebug_Print("APP Upgrade", "OTA Success", 1);}break;
	case 2:{USART1_Bebug_Print("APP Upgrade", "OTA Failed", 1);}break;
	default:{upgrade_info.ota_status = 0;}break;
	}

	upgrade_info.ota_type = OTA_BOOTLd;

	// 获取bootloader运行版本
	if(!Get_BOOT_Run_Version(upgrade_info.boot_v)){
		USART1_Bebug_Print("ERROR", "Get Bootloader Run Version Failed.", 1);
	}
	// 获取小屏幕运行版本
	if(!Get_SCREEN_Run_Version(upgrade_info.screen_v)){
		USART1_Bebug_Print("ERROR", "Get Screen Run Version Failed.", 1);
	}

	USART1_Bebug_Print("BOOT_V", upgrade_info.boot_v, 1);
	// 小屏幕升级标志
//	upgrade_info.screen_isUpgrade = Get_SCREEN_IS_Upgrade_Flag();
//	USART1_Bebug_Print("SCREEN", upgrade_info.screen_v, 1);
//	USART1_Bebug_Print("SCREEN Upgrade", (upgrade_info.screen_isUpgrade ? " OTA Success":" OTA Failed"), 1);
//	USART1_Bebug_Print("APP IS Upgrade", (upgrade_info.ota_status?" Yes":" No"), 1);

	memset(upgrade_info.plat_upgrade_v, 0, sizeof(upgrade_info.plat_upgrade_v));
}
/*
 * OTA开始工作
 * 参数：UPGRADE_OTA上电升级；UPGRADE_OTA_CMD指令升级
 */
void OTA_Start()
{
	upgrade_info.upgrade_mode 	= UPGRADE_OTA;
	upgrade_info.isUpgradeOK	= false;

	{
		down_pkg.pkg_id = 1;
		down_pkg.times = 0;
		down_pkg.remain = 0;
		down_pkg.pkg_total_times = 0;
		down_pkg.cnt = 0;
		down_pkg.clc_chk = 0;
		down_pkg.src_chk = 0;
		memset((uint8_t *)down_pkg.data, 0, sizeof(down_pkg.data));

		upgrade_info.rx_mode = RECV_CMD;
		upgrade_info.need_upgrade = 0;
		upgrade_info.ota_reconnect_server = 0;
		upgrade_info.ota_status = 0;
	}
	// 升级信息参数
	upgrade_info.size 				= 0;
	upgrade_info.play_sound			= 1;
	upgrade_info.inPlatform_status 	= 0;

//	USART1_Bebug_Print_Num("[ota_type]", upgrade_info.ota_type, 1, 1);
	Request_OTA_Request_Update_CMD_ASCII();// 请求

}
/*
 * OTA结束工作
 */
void OTA_End()
{
	upgrade_info.isUpgradeOK 	= 1;
	upgrade_info.upgrade_mode 	= UPGRADE_NO;
	upgrade_info.step 			= WAIT;
	down_pkg.cnt 		= 0;
	server_info.step_step 		= 0;
	 
	USART1_Bebug_Print("OTA", "OTA end.", 1);
}

/*
 * OTA升级步骤
 * 参数：4G阶段标志位
 * 返回：false直接返回；true成功或放弃OTA升级
 * 说明：连接上服务器后，才执行
 */
bool OTA_Upgrade_ASCII()
{
	static uint32_t ota_clk	= 0;
	static uint8_t 	loop_times = 1;
	static uint8_t  loop_err_times = 0;
	if(SystemtimeClock - ota_clk < 200) return 0;			// 500ms
	ota_clk = SystemtimeClock;

	if(!server_info.isConnect) return false;
	if(upgrade_info.isUpgradeOK) return false;				// 完成升级，直接返回

	switch(upgrade_info.step)
	{
	case REQUEST:{
#if 1
		switch(upgrade_info.inPlatform_status){
		case 0:{								// 没有接收反馈
			if(loop_times >= 7){				// 1.4S判断一次
				loop_times = 0;
				if(loop_err_times > 10){
					loop_err_times = 0;
					USART1_Bebug_Print("REQUEST", "Request a Timeout, Reconnect Server.", 1);
					Reconnect_Server(0);
				}else {
					loop_err_times++;
					Request_OTA_Request_Update_CMD_ASCII();
				}
			}else loop_times++;
		}break;
		case 1:{								// 需要升级
			upgrade_info.inPlatform_status = 3;
			loop_times = 0;
			if(Check_Whether_Upgrade()){		// 检测是否升级
				AT_QHTTPURL_Set_URL_Length();
			}else{
				goto DWN_NEXT_PKG;
			}
		}break;
		case 2:{								// 已升级过
DWN_NEXT_PKG:
			upgrade_info.inPlatform_status = 3;
			loop_times = 0;

			// 访问下一包是否需要下载
			switch(upgrade_info.ota_type){
			case OTA_BOOTLd: {
				USART1_Bebug_Print("UPGRADE", "[Bootloader] Platform Have Been Upgrade.", 1);

				// 断电前已经升级过，直接回复平台即可；否则获取APP版本信息
				upgrade_info.ota_type = OTA_APP;
				if(upgrade_info.app_isUpgrade == 1){		// bootloader中move升级包成功 update lmz 20221022
					upgrade_info.ota_status = 1;
					Request_OTA_Finish_CMD_ASCII();
				}else if(upgrade_info.app_isUpgrade == 2){	// bootloader中move升级包失败
					upgrade_info.ota_status = 0;
					Request_OTA_Finish_CMD_ASCII();
				}else{
					OTA_Start();
				}
			}break;
			case OTA_APP: {
				upgrade_info.ota_type = OTA_SCREEN;
				USART1_Bebug_Print("UPGRADE", "[APP] Platform Have Been Upgrade.", 1);
				OTA_Start();
			}break;
			case OTA_SCREEN:{
				if(upgrade_info.screen_isUpgrade) {	// 小屏需要升级
					upgrade_info.rx_mode 		= RECV_BIN;
					upgrade_info.upgrade_mode 	= UPGRADE_OTA;
				}else{
					upgrade_info.ota_type = OTA_NO;
					USART1_Bebug_Print("UPGRADE", "[Screen] Platform Have Been Upgrade.", 1);
					OTA_End();
					return true;
				}
			}break;
			}
		}break;
		}
#endif
	}break;
	case UPGRADE_URL:{
#if 1
		switch(upgrade_info.step_step)
		{
		case 0:{	// 设置URL地址长度
			if(_4g_message_state == _4G_CONNECT){
				_4g_message_state = _4G_WAIT;
				loop_err_times = 0;
				AT_QHTTPURL_Set_URL_Content();
				return false;
			}

			// 超时1秒，没有收到+QHTTPGET:
			if(loop_times > 4){		// 等待1秒
				loop_times = 0;
				AT_QHTTPURL_Set_URL_Length();
			}else loop_times++;
		}break;
		case 1:{	// 设置URL地址内容
			if(_4g_message_state == _4G_OK){
				_4g_message_state 	= _4G_WAIT;
				AT_QHTTPURL_Set_Sub_PKG_Info();
				return false;
			}

			if(loop_err_times > 1){
				loop_err_times = 0;
				AT_QHTTPURL_Set_URL_Length();
			}

			// 超时1秒，没有收到+QHTTPGET:
			if(loop_times > 4){
				loop_times = 0;
				loop_err_times++;
				AT_QHTTPURL_Set_URL_Content();
			}else loop_times++;
		}break;
		}
#endif
	}break;
	case UPGRADE_SUB_DMN:{
#if 1
		switch(upgrade_info.step_step){
		case 0:{	// 分包信息设置
			if(_4g_message_state == _4G_ERROR_714){
				_4g_message_state = _4G_WAIT;
				AT_QHTTPURL_Set_URL_Length();
				return false;
			}

//			if(_4g_message_state == _4G_HTTPGET){
//				_4g_message_state = _4G_WAIT;
//				loop_times = 0;
//				AT_QHTTPURL_Get_Sub_PKG_Data();
//				return false;
//			}


			// 超时1秒，没有收到+QHTTPGET:
//			if(loop_times > 4){
//				loop_times = 0;
//				AT_QHTTPURL_Set_Sub_PKG_Info();
//			}else loop_times++;
		}break;
//		case 1:{	// 获取分包数据
//			// 1秒后，没有收到CONNECT，就重发
//			if(loop_times > 2){
//				loop_times = 0;
//				AT_QHTTPURL_Set_Sub_PKG_Info(pkg_id);
//			}else loop_times++;
//		}break;
		}
#endif
	}break;
	case RESPONSE:{
		if(_4g_message_state == _4G_UPGRADE_OK){
			_4g_message_state = _4G_WAIT;

			// 执行下一个升级
			switch(upgrade_info.ota_type){
			case OTA_APP: {
				upgrade_info.ota_type = OTA_SCREEN;
				if(upgrade_info.app_isUpgrade >= 1){	// update lmz 20221021
					upgrade_info.app_isUpgrade = 0;
					Set_APP_IS_Upgrade_Flag(0);
				}
				USART1_Bebug_Print("UPGRADE", "[APP] Device Have Been Upgrade.", 1);

				// 播报语音
				if(upgrade_info.ota_status){
					upgrade_info.ota_status = 0;
					Send_Display_Message_VoiveNum(DATA_DOWNLOAD_OK_S);// 语音：数据下载成功
				}else Send_Display_Message_VoiveNum(DATA_DOWNLOAD_ERR_S);// 语音：数据下载失败
			}break;
			case OTA_SCREEN:{
				upgrade_info.ota_type = OTA_NO;

				USART1_Bebug_Print("UPGRADE", "[Screen] Device Have Been Upgrade.", 1);

				// 播报语音
				if(upgrade_info.ota_status){
					upgrade_info.ota_status = 0;
					Send_Display_Message_VoiveNum(DATA_DOWNLOAD_OK_S);// 语音：数据下载成功
				}else Send_Display_Message_VoiveNum(DATA_DOWNLOAD_ERR_S);// 语音：数据下载失败
				OTA_End();

				return true;
			}break;
			case OTA_BOOTLd:{
				upgrade_info.ota_type = OTA_APP;

				USART1_Bebug_Print("UPGRADE", "[Bootloader] Device Have Been Upgrade.", 1);

				// 播报语音
				if(upgrade_info.ota_status){
					upgrade_info.ota_status = 0;
					Send_Display_Message_VoiveNum(DATA_DOWNLOAD_OK_S);// 语音：数据下载成功
				}else Send_Display_Message_VoiveNum(DATA_DOWNLOAD_ERR_S);// 语音：数据下载失败
			} break;
			}

			Request_OTA_Request_Update_CMD_ASCII();
			return false;
		}

		// 超时1秒，没有收到回复:
		if(loop_times > 4){
			loop_times = 0;
			Request_OTA_Finish_CMD_ASCII();
			static uint8_t count = 0;
			count ++;
			if(count >= 3){
				upgrade_info.step = WAIT;
			}

		}else loop_times++;
	}break;
	default:break;
	}

	return false;
}

bool Write_APP_Configure_Attributes()
{
	// 版本信息和包大小
	if(!Set_APP_UP_Version(upgrade_info.plat_upgrade_v)){
		Set_APP_UP_Version(upgrade_info.plat_upgrade_v);
	}

	if(!Set_APP_UP_PKG_size(upgrade_info.size)){
		Set_APP_UP_PKG_size(upgrade_info.size);
	}

	// 在内部FLASH中设置OTA升级标志位置
	if(!Set_Flahs_Way_InFlash(1)){
		Set_Flahs_Way_InFlash(1);
	}

	// 为了兼容低版本
	if(!Set_APP_UP_Bin_Chk_Value_Compatible(down_pkg.src_chk)){
		Set_APP_UP_Bin_Chk_Value_Compatible(down_pkg.src_chk);
	}

	// 升级标志位
	upgrade_info.app_isUpgrade = 1;
	if(!Set_APP_IS_Upgrade_Flag(upgrade_info.app_isUpgrade)){
		Set_APP_IS_Upgrade_Flag(upgrade_info.app_isUpgrade);
	}
}
uint8_t data_tt[IN_FLASH_SECTOR_SIZE] = {0};

uint8_t move_buf[IN_FLASH_SECTOR_SIZE] = {0};
uint8_t Backup_Bootloader_Program_Before_Move_Operation()
{
	// 内部FLASH（0x000）-->内部FLASH（0x70000）
	volatile uint32_t write_addr = IN_FLASH_BOOT_TMP_START;
	volatile uint32_t read_addr = IN_FLASH_BOOTLOADER_START;

	// move 60KB 大小，预留出4KB的配置参数区域
	for(uint8_t pkg_id=0; pkg_id < 60; pkg_id++){
		memset((uint8_t *)down_pkg.data, 0, OTA_SIZE);
		// 内FLASH 读1024
		Interal_Flash_8_Byte_To_Read_One_Sector(read_addr, (uint8_t *)down_pkg.data);

		// 内FLASH 写1024
		Interal_Flash_8_Byte_to_Write_one_Sector(write_addr, (uint8_t *)down_pkg.data);

		// 地址递增1024
		write_addr 	+= IN_FLASH_SECTOR_SIZE;
		read_addr 	+= IN_FLASH_SECTOR_SIZE;
	}

	USART1_Bebug_Print("Bootloader Backup", "Success", 1);
	return 1;
}

uint8_t Recover_Bootloader_Program_If_Copy_Failed()
{
	// 内部FLASH（0x70000）-->内部FLASH（0x000）
	volatile uint32_t write_addr = IN_FLASH_BOOTLOADER_START;
	volatile uint32_t read_addr = IN_FLASH_BOOT_TMP_START;

	// move 60KB 大小，预留出4KB的配置参数区域
	for(uint8_t pkg_id=0; pkg_id < 60; pkg_id++){
//		if(pkg_id < BOOT_SECTION_SIZE-4){	// 前60KB数据正常写
			memset((uint8_t *)down_pkg.data, 0, IN_FLASH_SECTOR_SIZE);
			// 内FLASH 读1024B
			Interal_Flash_8_Byte_To_Read_One_Sector(read_addr, (uint8_t *)down_pkg.data);

			// 内FLASH 写1024B
			Interal_Flash_8_Byte_to_Write_one_Sector(write_addr, (uint8_t *)down_pkg.data);
//		}else{								// 最后4KB数据填写0xFF
//			memset((uint8_t *)move_buf, 0xFF, IN_FLASH_SECTOR_SIZE);
//
//			// 内FLASH 写1024
//			Interal_Flash_8_Byte_to_Write_one_Sector(write_addr, (uint8_t *)move_buf, 1);
//		}

		// 地址递增1024
		write_addr 	+= IN_FLASH_SECTOR_SIZE;
		read_addr 	+= IN_FLASH_SECTOR_SIZE;
	}

	USART1_Bebug_Print("Bootloader Restore", "Success", 1);
	return 1;
}

uint8_t Upgrade_Bootloader_Program()
{
	// 文件拷贝前，先备份
	Backup_Bootloader_Program_Before_Move_Operation();

	// 文件拷贝工作
	// 步骤1：将所有数据-->内部FLASH中
	volatile uint32_t read_addr 	= OUT_FLASH_BOOT_START;
	volatile uint32_t write_addr 	= IN_FLASH_BOOTLOADER_START;
	const uint8_t move_times 		= upgrade_info.size / IN_FLASH_SECTOR_SIZE;

	// 将外部FLASH的64KB数据(0x70000)直接拷贝到内部FLASH处（0x00000）
	for(uint8_t i=0; i < move_times; i++){
		memset((uint8_t *)down_pkg.data, 0, OTA_SIZE);

		// 外FLASH 读1024
		W25QXX_Read_No_Chk((uint8_t *)down_pkg.data, read_addr, IN_FLASH_SECTOR_SIZE);
//		USART1_Bebug_Print_Num("read_addr", read_addr, 3, 1);

		// 内FLASH 写1024
		Interal_Flash_8_Byte_to_Write_one_Sector(write_addr, (uint8_t *)down_pkg.data);
//		USART1_Bebug_Print_Num("write_addr", write_addr, 3, 1);

		// 地址递增1024
		write_addr 	+= IN_FLASH_SECTOR_SIZE;
		read_addr 	+= IN_FLASH_SECTOR_SIZE;
	}


	// 步骤2：对拷贝数据做校验（判断拷贝成功的依据）
	read_addr = IN_FLASH_BOOTLOADER_START;
	down_pkg.clc_chk = 0;
	for(uint8_t i=0; i < move_times; i++){
		memset((uint8_t *)down_pkg.data, 0, OTA_SIZE);
		// 内FLASH 读1024
		Interal_Flash_8_Byte_To_Read_One_Sector(read_addr, (uint8_t *)down_pkg.data);

		// 地址递增1024
		read_addr += IN_FLASH_SECTOR_SIZE;

		// 累加求校验值
		for(uint16_t j=0; j<IN_FLASH_SECTOR_SIZE; j++){
			down_pkg.clc_chk += down_pkg.data[j];
		}
	}

//	USART1_Bebug_Print_Num("clc_chk", down_pkg.clc_chk, 3, 1);
//	USART1_Bebug_Print_Num("src_chk", down_pkg.src_chk, 3, 1);

	if(down_pkg.src_chk == down_pkg.clc_chk){
		USART1_Bebug_Print("Bootloader Upgrade", "Success", 1);
		return 1;
	}

 	USART1_Bebug_Print("Bootloader Upgrade", "Failed", 1);

	// 异常处理：拷贝失败，将备份还原
	Recover_Bootloader_Program_If_Copy_Failed();

	return 0;
}

bool Write_Screen_Configure_Attributes()
{
	// 版本信息和包大小
	if(!Set_SCREEN_UP_Version(upgrade_info.plat_upgrade_v)){
		Set_SCREEN_UP_Version(upgrade_info.plat_upgrade_v);
	}

	if(!Set_SCREEN_UP_PKG_size(upgrade_info.size)){
		Set_SCREEN_UP_PKG_size(upgrade_info.size);
	}

	if(!Set_SCREEN_UP_Bin_Chk_Value(down_pkg.src_chk)){
		Set_SCREEN_UP_Bin_Chk_Value(down_pkg.src_chk);
	}
	upgrade_info.screen_isUpgrade = 1;
	Set_SCREEN_IS_Upgrade_Flag(upgrade_info.screen_isUpgrade);	// 写小屏幕需要升级的标志位，防止异常传送失败后，下次上电后继续
}
/*
 * 分包下载逻辑
 * 说明:第一包需要校验包头8B，大小为4104Byte，求校验值（正常包数据累加）；
 * 中间包直接接收，大小为4096Byte，求校验值（正常包数据累加）；
 * 尾包需要取校验4B和包尾8B，大小为remain+12 Byte，求校验值（正常包数据累加）；
 */
uint8_t OTA_Sub_PKG_Dwn_Write_Data_In_Main()
{
	if(down_pkg.cnt >= down_pkg.recv_size){
		down_pkg.overtime_flag = 0;
		// 解析有效字段，并将有效内容存放到外部FLASH中
		volatile uint16_t front_addr = 0;
		volatile uint16_t end_addr = 0;
		volatile uint16_t end_pkg_size = 0;
#if 1	// 找头尾
		// 找到头
		for(uint16_t i=10; i<down_pkg.cnt; i++){
			if(down_pkg.data[i]==0x0A && down_pkg.data[i-1]==0x0D && down_pkg.data[i-2]==0x54 \
				&& down_pkg.data[i-3]==0x43 && down_pkg.data[i-4]==0x45 && down_pkg.data[i-5]==0x4E){
				front_addr = i+1;
				break;
			}
		}
		// 根据包长度，找到尾
		if(down_pkg.pkg_id == 1){
			end_pkg_size = FIRST_PKG_SIZE + front_addr - 1;
			down_pkg.clc_chk = 0;
		}else if(down_pkg.pkg_id == down_pkg.pkg_total_times){
			if(down_pkg.remain == 0){
				end_pkg_size = SUB_PKG_SIZE + front_addr - 1;
			}else{
				end_pkg_size = down_pkg.remain + front_addr - 1;
			}
		}else{
			end_pkg_size = SUB_PKG_SIZE + front_addr - 1;
		}
//		USART1_Bebug_Print_Num("2[end_pkg_size]", end_pkg_size, 1);
		for(uint16_t i=end_pkg_size; i<down_pkg.cnt; i++){
			if(down_pkg.data[i]==0x0D && down_pkg.data[i+1]==0x0A && down_pkg.data[i+2]==0x4F \
				&& down_pkg.data[i+3]==0x4B && down_pkg.data[i+4]==0x0D && down_pkg.data[i+5]==0x0A){
				end_addr = i;
				break;
			}
		}

		if(front_addr == 0 || end_addr == 0){	// 重发当前包
			USART1_Bebug_Print_Num("[front_addr]", front_addr, 0, 1);
			USART1_Bebug_Print_Num("[end_addr]", end_addr, 1, 1);
			USART1_Bebug_Print("[end_addr]", "Resend Current pkg.", 1);
			AT_QHTTPURL_Set_Sub_PKG_Info();
			return 0;
		}

		// 判断第一包的包头和最后一包的校验和尾
		if(down_pkg.pkg_id == 1){
			// 测试打印
			for(uint8_t i=0; i<8; i++){
				fprintf(USART1_STREAM, "%02X ", down_pkg.data[front_addr + i]);
			}
			fprintf(USART1_STREAM, "\r\n");

			front_addr += 8;
		}else if(down_pkg.pkg_id == down_pkg.pkg_total_times){
			// 测试打印
			for(uint16_t i=end_addr-12; i<end_addr; i++){
				fprintf(USART1_STREAM, "%02X ", down_pkg.data[i]);
			}
			fprintf(USART1_STREAM, "\r\n");

			down_pkg.src_chk = down_pkg.data[end_addr-12]<<24 | down_pkg.data[end_addr-11]<<16 | \
					down_pkg.data[end_addr-10]<<8 | down_pkg.data[end_addr-9];
			end_addr -= 12;
		}
#endif

		// 写数据（先擦除后写入）
		down_pkg.write_len = end_addr - front_addr;

		// 测试写入数据
//		USART1_Bebug_Print_Num("[write_len]", down_pkg.write_len, 1, 1);
//		USART1_Bebug_Print_Num("[addr]", down_pkg.addr, 3, 1);

		// 求校验值
		for(uint16_t i=0; i<down_pkg.write_len; i++){
			down_pkg.clc_chk += down_pkg.data[front_addr + i];
		}

		// 不同的升级包对应的删除文件的空间是不一样的
//		W25QXX_Erase_Sector(down_pkg.addr/SUB_PKG_SIZE);	// 擦除

		uint16_t erase_sector_id = 0;
		switch(upgrade_info.ota_type){
		case OTA_APP: erase_sector_id = down_pkg.pkg_id - 1;break;
		case OTA_BOOTLd: erase_sector_id = OUT_FLASH_BOOT_START/SUB_PKG_SIZE + down_pkg.pkg_id - 1; break;
		case OTA_SCREEN: erase_sector_id = OUT_FLASH_SCREEN_START/SUB_PKG_SIZE + down_pkg.pkg_id - 1; break;
		}
		W25QXX_Erase_Sector(erase_sector_id);

//		USART1_Bebug_Print_Num("[erase_sector_id]", erase_sector_id, 1, 1);
//		USART1_Bebug_Print_Num("[write addr]", down_pkg.addr, 3, 1);

		erase_sector_id = down_pkg.addr/SUB_PKG_SIZE;
//		USART1_Bebug_Print_Num("[erase_sector_id]", erase_sector_id, 1, 1);

		W25QXX_Write_NoCheck((uint8_t *)&down_pkg.data[front_addr], down_pkg.addr, down_pkg.write_len);	// 写入
		down_pkg.addr += down_pkg.write_len;				// 地址偏移

		// test print
//		for(uint16_t i=0; i<down_pkg.write_len; i++){
//			if(i < 8) fprintf(USART1_STREAM, "%02X ", down_pkg.data[i+front_addr]);
//			else if(i == down_pkg.write_len-9) fprintf(USART1_STREAM, "\r\n");
//			else if(i >= down_pkg.write_len-8) fprintf(USART1_STREAM, "%02X ", down_pkg.data[i+front_addr]);
//		}
//		fprintf(USART1_STREAM, "\r\n");

		IWDT_Feed_The_Dog();	// 踢狗

		// 下发下一包，和 判最后一包
		if(down_pkg.pkg_id == down_pkg.pkg_total_times){
#if 1
			USART1_Bebug_Print_Num("[clc_chk]", down_pkg.clc_chk, 3, 1);
			USART1_Bebug_Print_Num("[src_chk]", down_pkg.src_chk, 3, 1);

			if(down_pkg.src_chk == down_pkg.clc_chk){	// 写属性值
				switch(upgrade_info.ota_type){
				case OTA_APP: {
					Write_APP_Configure_Attributes();
					upgrade_info.ota_status = 1;
//					USART1_Bebug_Print("DEBUG", "Device Restart Test", 1);
					SYSCTL_System_Reset_Enable(TRUE);	// 控制MCU软重启
				}break;
				case OTA_BOOTLd: {
					if(Upgrade_Bootloader_Program()){
						upgrade_info.ota_status = 1;
//						SYSCTL_System_Reset_Enable(TRUE);	// 控制MCU软重启
					}else{
						upgrade_info.ota_status = 0;
					}
				}break;
				case OTA_SCREEN:{
					Write_Screen_Configure_Attributes();

					upgrade_info.ota_status = 1;
				}break;
				}
				// test print
//				USART1_Bebug_Print("plat_upgrade_v", upgrade_info.plat_upgrade_v, 1);
//				USART1_Bebug_Print_Num("[size]", upgrade_info.size, 1, 1);
//				USART1_Bebug_Print_Num("[src_chk]", down_pkg.src_chk, 3, 1);
//				USART1_Bebug_Print_Num("[ota_status]", upgrade_info.ota_status, 1, 1);

				// 重开中断
				Reopen_Interrupt_Function();

				upgrade_info.need_upgrade = 0;
				upgrade_info.ota_reconnect_server = 1;
				upgrade_info.step = WAIT;
				// 先重连服务器后，在回复
				Reconnect_Server(0);

				USART1_Bebug_Print("SUCCESS", "Write Finish.", 1);
			}else{	// 擦除已经写的数据
				switch(upgrade_info.ota_type){
				case OTA_APP:{
					for(uint8_t i=0; i<down_pkg.pkg_total_times; i++)
						W25QXX_Erase_Sector(i);
				}break;
				case OTA_BOOTLd:{
					for(uint8_t i=0; i<down_pkg.pkg_total_times; i++)
						W25QXX_Erase_Sector(OUT_FLASH_BOOT_START/SUB_PKG_SIZE + i);
				} break;
				case OTA_SCREEN:{
					for(uint8_t i=0; i<down_pkg.pkg_total_times; i++)
						W25QXX_Erase_Sector(OUT_FLASH_SCREEN_START/SUB_PKG_SIZE + i);
				} break;
				}

				// 重开中断
				Reopen_Interrupt_Function();

				upgrade_info.need_upgrade = 0;
				upgrade_info.ota_status = 0;
				upgrade_info.step = WAIT;
				// 下载完毕后，需要重连服务器，之后再回复平台
				upgrade_info.ota_reconnect_server = 1;
				Reconnect_Server(0);

				USART1_Bebug_Print("FAILED", "Write Failed.", 1);
			}
#endif
		}else{
			down_pkg.pkg_id++;
			USART1_Bebug_Print_Num("[pkg_id]", down_pkg.pkg_id, 0, 1);
			USART1_Bebug_Print_Num("[pkg_total_times]", down_pkg.pkg_total_times, 1, 1);

			// 发送获取下一包，发送设置包信息
			AT_QHTTPURL_Set_Sub_PKG_Info();
		}
	}
}

/*
 * HTTP升级操作：阶段1
 */
void AT_QHTTPURL_Set_URL_Length()
{
	// 发送语音
	if(upgrade_info.play_sound){
		upgrade_info.play_sound = 0;
		Send_Display_Message_VoiveNum(DATA_DOWNLOADING_S);		// 数据下载中 语音

		// 关闭中断
		Close_Interrupt_Function();
	}

	upgrade_info.rx_mode 	= RECV_CMD;
	upgrade_info.step 		= UPGRADE_URL;
	upgrade_info.step_step 	= 0;
	uint8_t cmd[30]			= {0};
	sprintf(cmd, "AT+QHTTPURL=%d, 80\r\n", strlen(upgrade_info.https));
//	USART1_Bebug_Print("QHTTPURL", cmd, 1);
	EC200U_SendData(cmd, strlen(cmd), 1);
}

/*
 * HTTP升级操作：阶段2
 */
void AT_QHTTPURL_Set_URL_Content()
{
	upgrade_info.step 		= UPGRADE_URL;
	upgrade_info.step_step 	= 1;
	uint8_t data[100] 		= {0};

	sprintf(data, "%s\r\n", upgrade_info.https);
//	USART1_Bebug_Print("HTTPS", data, 1);

	EC200U_SendData(data, strlen(data), 1);
}
/*
 * HTTP升级操作：阶段3
 */

void AT_QHTTPURL_Set_Sub_PKG_Info()
{
	down_pkg.overtime_flag = 1;

	upgrade_info.rx_mode 	= RECV_CMD;
	upgrade_info.step 		= UPGRADE_SUB_DMN;
	upgrade_info.step_step 	= 0;

	uint8_t cmd[50]			= {0};
	volatile uint32_t addr_offset = 0;
	volatile uint32_t read_size = 0;

	const uint8_t data_offset = 34;	// 34

	down_pkg.cnt = 0;
	memset((uint8_t *)down_pkg.data, 0, sizeof(down_pkg.data));

	if(down_pkg.pkg_id == 1) down_pkg.recv_size = FIRST_PKG_SIZE + data_offset;
	else if(down_pkg.pkg_id == down_pkg.pkg_total_times){
		if(down_pkg.remain == 0) down_pkg.recv_size = SUB_PKG_SIZE + data_offset;
		else down_pkg.recv_size = down_pkg.remain + data_offset;
	}else{
		down_pkg.recv_size = SUB_PKG_SIZE + data_offset;
	}
//		USART1_Bebug_Print_Num("[pkg_id]", down_pkg.pkg_id, 0, 1);
//		USART1_Bebug_Print_Num("[pkg_total_times]", down_pkg.pkg_total_times, 1, 1);
//		USART1_Bebug_Print_Num("[recv_size]", down_pkg.recv_size, 1, 1);

	switch(down_pkg.pkg_id){
	case 0: USART1_Bebug_Print("ERROR", "Package ID Range [1,75]", 1);return;
	case 1:	addr_offset = 0; read_size = FIRST_PKG_SIZE; break;
	default:{
		if(down_pkg.pkg_total_times == down_pkg.pkg_id){		// 最后一包
			if(down_pkg.remain == 0){
				addr_offset = FIRST_PKG_SIZE + SUB_PKG_SIZE * (down_pkg.pkg_id - 2);
				read_size = SUB_PKG_SIZE  + END_PKG_CHK_SIZE;
			}else{
				addr_offset = FIRST_PKG_SIZE + SUB_PKG_SIZE * (down_pkg.pkg_id - 2);
				read_size = down_pkg.remain + END_PKG_CHK_SIZE;
			}
		}else{	// 非第一包，非最后一包
			addr_offset = FIRST_PKG_SIZE + SUB_PKG_SIZE * (down_pkg.pkg_id - 2);
			read_size = SUB_PKG_SIZE;
		}
	}break;
	}

	// AT+QHTTPGETEX=80,8200,4096
	sprintf(cmd, "AT+QHTTPGETEX=80,%d,%d\r\n", addr_offset, read_size);

//	USART1_Bebug_Print("PKG GET", cmd, 1);

	EC200U_SendData(cmd, strlen(cmd), 0);
}
/*
 * HTTP升级操作：阶段4
 * 接收包
 */
void AT_QHTTPURL_Get_Sub_PKG_Data()
{
	upgrade_info.step 		= UPGRADE_SUB_DMN;
	upgrade_info.rx_mode 	= RECV_BIN;
	upgrade_info.step_step 	= 1;
	down_pkg.overtime_flag 	= 1;
	uint8_t cmd[30]			= {0};
	sprintf(cmd, "AT+QHTTPREAD=80\r\n");

//	USART1_Bebug_Print("PKG READ", cmd, 1);

	EC200U_SendData(cmd, strlen(cmd), 1);
}

/*
 * 检测是否升级
 * 参数：
 * 返回：false不升级；true升级
 */
bool Check_Whether_Upgrade()
{
	switch(upgrade_info.ota_type){
	case OTA_APP:{
		USART1_Bebug_Print("Run_v", upgrade_info.run_v, 1);
		USART1_Bebug_Print("Up_v", upgrade_info.plat_upgrade_v, 1);

		if(Version_Compare(upgrade_info.run_v, upgrade_info.plat_upgrade_v)){
			USART1_Bebug_Print("UPGRDE", "[APP]OTA Need Upgrade...", 1);
			upgrade_info.need_upgrade = 1;
			down_pkg.pkg_id = 1;
			return true;
		}
	}break;
	case OTA_BOOTLd:{
		USART1_Bebug_Print("Boot_v", upgrade_info.boot_v, 1);
		USART1_Bebug_Print("Up_v", upgrade_info.plat_upgrade_v, 1);

		if(Version_Compare(upgrade_info.boot_v, upgrade_info.plat_upgrade_v)){
			USART1_Bebug_Print("UPGRDE", "[Bootloader]OTA Need Upgrade...", 1);
			upgrade_info.need_upgrade = 1;
			down_pkg.pkg_id = 1;
			return true;
		}
	}break;
	case OTA_SCREEN:{
		USART1_Bebug_Print("Screen_v", upgrade_info.screen_v, 1);
		USART1_Bebug_Print("Up_v", upgrade_info.plat_upgrade_v, 1);

		if(Version_Compare(upgrade_info.screen_v, upgrade_info.plat_upgrade_v)){
			USART1_Bebug_Print("UPGRDE", "[Screen]OTA Need Upgrade...", 1);
			upgrade_info.need_upgrade = 1;
			down_pkg.pkg_id = 1;
			return true;
		}
	}break;
	}

	return false;
}

void Analysis_USART1_4G_CMD()
{
	// OTA 指令升级 ,升级到最新版本，重新检测一下
	if(strstr((const char*)User_Rxbuffer,(const char*)"ZKHYCHK*UPDATE:NEWEST")){
		upgrade_info.ota_type = OTA_BOOTLd;
		upgrade_info.app_isUpgrade = 0;
		OTA_Start();
		USART1_Bebug_Print(">>", "Upgrade...", 1);
	}else if(strstr((const char*)User_Rxbuffer,(const char*)"ZKHYCHK*BOOTVERSION")){
		// 获取BOOTLoader版本号
		if(Get_BOOT_Run_Version(upgrade_info.boot_v)){
			fprintf(USART1_STREAM, ">> BOOT_V:%s\r\n", upgrade_info.boot_v);
		}else{
			USART1_Bebug_Print("ERROR", "Get Bootloader Version Failed.", 1);
		}
	}else if(strstr((const char*)User_Rxbuffer,(const char*)("ZKHYCHK*RECONNECT"))){
		Reconnect_Server(0);
		USART1_Bebug_Print(">>", "Reconnecting...", 1);
	}else if(strstr((const char*)User_Rxbuffer, (const char*)("ZKHYCHK*RESTART_4G"))){
		server_info.err_loop_times 	= 20;
		server_info.step 			= EC200U_RESTART;
		server_info.isConnect 		= 0;
		USART1_Bebug_Print(">>", "Restart 4G Module...", 1);
	}else if(strstr((const char*)User_Rxbuffer, (const char*)("ZKHYCHK*RECONGPS"))){
		GPS_Restart();
		USART1_Bebug_Print(">>", "Reconnect GPS ...", 1);
	}else if(strstr((const char*)User_Rxbuffer, (const char*)("ZKHYSET*ERASE"))){	// W25QXX_Erase_Sector
		uint8_t id = User_Rxbuffer[15] - 0x30;
		switch(id){
		case 1:{
			for(uint8_t i=0; i<APP_SECTION_SIZE; i++){
				W25QXX_Erase_Sector(i);
			}
			USART1_Bebug_Print(">>", "Erase App Storage Room.", 1);
		}break;
		case 2:{
			for(uint8_t i=0; i<BOOT_SECTION_SIZE; i++){
				W25QXX_Erase_Sector(OUT_FLASH_BOOT_START/4096 + i);
			}
			USART1_Bebug_Print(">>", "Erase Bootloader Storage Room.", 1);
		}break;
		case 3:{
			for(uint8_t i=0; i<SCREEN_SECTION_SIZE; i++){
				W25QXX_Erase_Sector(OUT_FLASH_SCREEN_START/4096 + i);
			}
			USART1_Bebug_Print(">>", "Erase Screen Storage Room.", 1);
		}break;
		default:{
			USART1_Bebug_Print_Num("[id]", id, 0, 1);
			USART1_Bebug_Print("[ERROR]", "Erase Storage Room Failed.", 1);
		}
		}
	}else if(strstr((const char*)User_Rxbuffer, (const char*)("ZKHYSET*RESTART_SYS"))){	// 控制设备软重启 20220913 lmz add
		USART1_Bebug_Print(">>", "The Device Is Restarting....", 1);
//		USART1_Bebug_Print("DEBUG", "Device Restart Test", 1);
		SYSCTL_System_Reset_Enable(TRUE);	// 控制MCU软重启
	}
}


bool Request_OTA_Request_Update_CMD_ASCII()
{
	upgrade_info.step = REQUEST;
	memset(&noteData_upgrade_share, 0, sizeof(NodeData_S));

	static uint16_t 		otaRequest_seqNum = 0;
	if(otaRequest_seqNum > UPLOAD_MAX_SEQ_NUM) otaRequest_seqNum = 1;
	else otaRequest_seqNum++;
	// 填数
	if(!Request_Protocal_Head_ASCII(otaRequest_seqNum, OTA_REQUEST_FRAME, noteData_upgrade_share.data)) return false;

	upgrade_info.step_step = 1;

	req_index = 31;

	// 行程ID 20个字节  upgrade_info.itinerary_id
	Set_Request_ASCII_Info_Template(&noteData_upgrade_share, server_info.itinerary_id, CHAR, 20);

	// type  固件类型：【0：控制器】、【1：显示器】、【2：摄像头】、【2：bootloader】
	uint8_t ota_type = upgrade_info.ota_type;
//	USART1_Bebug_Print_Num("ota_type", ota_type, 1, 1);
	Set_Request_ASCII_Info_Template(&noteData_upgrade_share, &ota_type, UINT_8, 1);	// 固件类型

	// version
	switch(upgrade_info.ota_type){
	case OTA_APP: Set_Request_ASCII_Info_Template(&noteData_upgrade_share, upgrade_info.run_v, CHAR, 20); break;
	case OTA_SCREEN: Set_Request_ASCII_Info_Template(&noteData_upgrade_share, upgrade_info.screen_v, CHAR, 20); break;
	case OTA_BOOTLd: Set_Request_ASCII_Info_Template(&noteData_upgrade_share, upgrade_info.boot_v, CHAR, 20); break;
	}

	//校验
	Set_Request_ASCII_Info_Template(&noteData_upgrade_share, upgrade_info.run_v, CRC16, 2);

	// 放到队列中进行发送
	sprintf(noteData_upgrade_share.data_info, "AT+QISEND=%d, %d\r\n", CONNECT_ID, noteData_upgrade_share.data_len);

	// test print
//	fprintf(USART1_STREAM, "\r\n-----------Request_Update-----start--------------\r\n ");
//	USART1_Bebug_Print_Num("[upgrade_info.ota_type]", upgrade_info.ota_type, 1, 1);
//	for(uint16_t i=0; i<noteData_upgrade_share.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", noteData_upgrade_share.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n------------Request_Update-----end-------------\r\n ");

	Usart0_DMA_Transmit(noteData_upgrade_share.data_info, strlen(noteData_upgrade_share.data_info));
	delay_ms(1);
	Usart0_DMA_Transmit(noteData_upgrade_share.data, noteData_upgrade_share.data_len);

	return true;
}


bool Request_OTA_Finish_CMD_ASCII()
{
	Enable_Usart_Interrupt(USART0_SFR, INT_USART0);
	upgrade_info.rx_mode= RECV_CMD;
	upgrade_info.step 	= RESPONSE;
	upgrade_info.step_step = 1;

	memset(&noteData_upgrade_share, 0, sizeof(NodeData_S));

	static uint16_t 		otaFinish_seqNum = 0;
	if(otaFinish_seqNum > UPLOAD_MAX_SEQ_NUM) otaFinish_seqNum = 1;
	else otaFinish_seqNum++;

	// 填数
	if(!Request_Protocal_Head_ASCII(otaFinish_seqNum, OTA_REPLY_FRAME, noteData_upgrade_share.data))
		return false;

	req_index = 31;

	// 行程ID 20个字节  upgrade_info.itinerary_id
	Set_Request_ASCII_Info_Template(&noteData_upgrade_share, server_info.itinerary_id, CHAR, 20);

	// type 固件类型：【0：控制器】、【1：显示器】、【2：摄像头】
	uint8_t type = upgrade_info.ota_type;
	Set_Request_ASCII_Info_Template(&noteData_upgrade_share, &type, UINT_8, 1);	// 固件类型

	// version
	Set_Request_ASCII_Info_Template(&noteData_upgrade_share, upgrade_info.plat_upgrade_v, CHAR, 20);

	// OTA 升级状态：1:-成功；0-失败
	uint8_t ota_status = upgrade_info.ota_status;
	USART1_Bebug_Print_Num("[otaState]", ota_status, 1, 1);
	Set_Request_ASCII_Info_Template(&noteData_upgrade_share, &ota_status, UINT_8, 1);

	//校验
	Set_Request_ASCII_Info_Template(&noteData_upgrade_share,  &ota_status, CRC16, 2);

	// 放到队列中进行发送
	sprintf(noteData_upgrade_share.data_info, "AT+QISEND=%d, %d\r\n", CONNECT_ID, noteData_upgrade_share.data_len);

	Usart0_DMA_Transmit(noteData_upgrade_share.data_info, strlen(noteData_upgrade_share.data_info));
	delay_ms(1);
	Usart0_DMA_Transmit(noteData_upgrade_share.data, noteData_upgrade_share.data_len);
//	noteData_upgrade_share.is_wait_reply = NEED_WAIT_REPLY;
//	noteData_upgrade_share.is_store = NEED_STORED;
//	noteData_upgrade_share.is_single_cmd = DOUBLE_CMD;
//	insertQueue(&dataUp_q, &noteData_upgrade_share);
	return true;
}

void Analysis_Reply_UpgradePackage_Info_ASCII(uint8_t *data, uint16_t contentLen)
{
	// 升级状态
	if(data[0] < 2){	// 0:未升级；1：已下发；2：升级成功
		upgrade_info.inPlatform_status = 1;
	}

	// 固件类型	1Byte
	upgrade_info.ota_type = data[1];

	// 包大小 4Byte
	uint32_t pkgSize =  data[2] | data[3]<<8 | data[4]<<16 | data[5]<<24;
	upgrade_info.size = pkgSize - 20;
//*256*256
	// 该设备在平台中绑定最新版本号,20B
	uint8_t j = 0;
	for(uint8_t i=6; i<26; i++){
		if(data[i] != 0){
			upgrade_info.plat_upgrade_v[j] = data[i];
			j++;
		}
	}
	upgrade_info.plat_upgrade_v[j] = '\0';

	// 升级包连接地址https
	j = 0;
	for(uint8_t i=28; i<contentLen; i++){
		upgrade_info.https[j] = data[i];
		j++;
	}
	upgrade_info.https[j] = '\0';


	// 计算
	down_pkg.times = upgrade_info.size / SUB_PKG_SIZE;
	down_pkg.remain = upgrade_info.size % SUB_PKG_SIZE;
	if(down_pkg.remain){		// 有多余的包，必是1024的倍数
		down_pkg.pkg_total_times = down_pkg.times + 1;
	}else {
		down_pkg.pkg_total_times = down_pkg.times;
	}

	// 根据类型开始对包数据赋值
	switch(upgrade_info.ota_type){
	case OTA_APP:{
		down_pkg.addr = OUT_FLASH_APP_START;
	}break;
	case OTA_SCREEN:{
		down_pkg.addr = OUT_FLASH_SCREEN_START;
	}break;
	case OTA_BOOTLd:{
		down_pkg.addr = OUT_FLASH_BOOT_START;
	}break;
	default:{
		USART1_Bebug_Print_Num("[upgrade_info.ota_type]", upgrade_info.ota_type, 1, 1);
		upgrade_info.ota_type = 3;
		down_pkg.addr = OUT_FLASH_BOOT_START;
	}break;
	}

	USART1_Bebug_Print_Num("[upgrade_info.ota_type]", upgrade_info.ota_type, 1, 1);
	USART1_Bebug_Print_Num("[DWN PKG]times", down_pkg.times, 0, 1);
	USART1_Bebug_Print_Num("remain", down_pkg.remain, 0, 1);
//	USART1_Bebug_Print_Num("pkg_total_times", down_pkg.pkg_total_times, 0);
	USART1_Bebug_Print_Num("addr", down_pkg.addr, 3, 1);
//	USART1_Bebug_Print("https", upgrade_info.https, 1);
	upgrade_info.step = REQUEST;
}

/*
 * 在USART0中断函数中，字符串解析
 * 参数：接收到结束符的一条字符串
 */
bool _4G_Recv_OTA_Message(uint8_t *string)
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return false;

	if(!server_info.isConnect) return false;
	if(upgrade_info.isUpgradeOK) return false;

	char *strx = NULL;

	switch(upgrade_info.step)
	{
	case UPGRADE_URL:{
//		USART1_Bebug_Print("UPGRADE_URL", string, 1);
		if(strstr((const char*)string, (const char*)"CONNECT")){
//			USART1_Bebug_Print("UPGRADE_URL", string, 1);
			_4g_message_state = _4G_CONNECT;
			return true;
		}

		if(strstr((const char*)string, (const char*)"OK")){
//			USART1_Bebug_Print("UPGRADE_URL", string, 1);
			_4g_message_state = _4G_OK;
			return true;
		}
	}break;
	case UPGRADE_SUB_DMN:{
		if(strstr((const char*)string, (const char*)"+QHTTPGET:")){
//			USART1_Bebug_Print("UPGRADE_SUB_DMN", string, 1);
//			_4g_message_state = _4G_HTTPGET;
			down_pkg.overtime_flag = 0;
			AT_QHTTPURL_Get_Sub_PKG_Data();
			return true;
		}

		if(strstr((const char*)string, (const char*)"714")){
//			USART1_Bebug_Print("UPGRADE_SUB_DMN", string, 1);
			_4g_message_state = _4G_ERROR_714;
			return true;
		}

		if(strstr((const char*)string, (const char*)"+CME ERROR")){	// 异常处理
//			USART1_Bebug_Print("UPGRADE_SUB_DMN", string, 1);
			_4g_message_state = _4G_ERROR_714;
			return true;
		}
	}break;
	default:break;
	}
	return false;
}

/************************小屏幕升级************************************/
typedef struct _Screen_Pkg_Info{
	uint32_t 	addr ;				// 小屏幕Bin包在外FLASH存储起始地址
	uint8_t 	data[SUB_PKG_SIZE];	// 单包大小
	uint16_t 	pkg_index;			// 分包数据下标索引,包ID时下标从1开始，但读数据地址时下包从0开始
	uint16_t 	recv_pkg_index; 	// 接收包的索引，验证
	uint32_t 	clac_chk;			// 单包的校验和
	uint8_t 	pkg_num;			// 分包总数
	uint8_t 	send_state;			// 发送标志位【0：初始化】【1：取数据】【2：发送开始帧】【3：发送数据帧】【4：发送结束帧】
	uint8_t 	recv_state;			// 接收标志位，【0：初始化】【1：需发送下一帧】【2：异常重发】
	uint8_t 	run_v[SH_INFO_SIZE];// 运行版本
	uint8_t 	up_v[SH_INFO_SIZE];	// 升级版本
	// 发送等待
	uint8_t 	delay_times;		// 同一条指令，重发发送时间间隔
}Screen_Single_Pkg_Info;

struct can_frame screen_ota_frame;
Screen_Single_Pkg_Info screen_single_pkg_info = {0};

void Send_PKG_Start_Frame_To_Small_Screen_By_CAN4(uint16_t pkg_id, uint16_t pkg_len, uint16_t pkg_total)
{
	// 起始帧0x7F0:包ID(2B)+包长(2B)+分包总数(2B)+预留(2B)
	screen_ota_frame.TargetID 	= SCREEN_OTA_START_FRAME;
	screen_ota_frame.lenth 		= 8;
	screen_ota_frame.MsgType 	= CAN_DATA_FRAME;
	screen_ota_frame.RmtFrm 	= CAN_FRAME_FORMAT_SFF;
//	screen_ota_frame.RefreshRate = 100;

	screen_ota_frame.data[0] 	= (pkg_id >> 8) & 0xFF;
	screen_ota_frame.data[1] 	= pkg_id & 0xFF;
	screen_ota_frame.data[2] 	= (pkg_len >> 8) & 0xFF;
	screen_ota_frame.data[3] 	= pkg_len & 0xFF;
	screen_ota_frame.data[4] 	= (pkg_total >> 8) & 0xFF;
	screen_ota_frame.data[5] 	= pkg_total & 0xFF;
	screen_ota_frame.data[6] 	= 0;
	screen_ota_frame.data[7] 	= 0;

	CAN_Transmit_DATA(CAN4_SFR, screen_ota_frame);
}

void Send_PKG_Transfer_Frame_To_Small_Screen_By_CAN4(uint8_t *data)
{
	// 传送阵0x7F1:数据(8B)
	screen_ota_frame.TargetID 	= SCREEN_OTA_TRANS_FRAME;
	screen_ota_frame.lenth 		= 8;
	screen_ota_frame.MsgType 	= CAN_DATA_FRAME;
	screen_ota_frame.RmtFrm 	= CAN_FRAME_FORMAT_SFF;
//	screen_ota_frame.RefreshRate = 100;

	memcpy(screen_ota_frame.data, data, 8);

	CAN_Transmit_DATA(CAN4_SFR, screen_ota_frame);
}

void Send_PKG_End_Frame_To_Small_Screen_By_CAN4(uint16_t pkg_id, uint32_t src_chk)
{
	// 结束帧0x7F2:包ID(2B)+包校验(4B)+预留(2B)
	screen_ota_frame.TargetID 	= SCREEN_OTA_END_FRAME;
	screen_ota_frame.lenth 		= 8;
	screen_ota_frame.MsgType 	= CAN_DATA_FRAME;
	screen_ota_frame.RmtFrm 	= CAN_FRAME_FORMAT_SFF;
//	screen_ota_frame.RefreshRate = 100;

	screen_ota_frame.data[0] 	= (pkg_id >> 8) & 0xFF;
	screen_ota_frame.data[1] 	= pkg_id & 0xFF;
	screen_ota_frame.data[2] 	= (src_chk >> 24) & 0xFF;
	screen_ota_frame.data[3] 	= (src_chk >> 16) & 0xFF;
	screen_ota_frame.data[4] 	= (src_chk >> 8) & 0xFF;
	screen_ota_frame.data[5] 	= src_chk & 0xFF;
	screen_ota_frame.data[6] 	= 0;
	screen_ota_frame.data[7] 	= 0;

	CAN_Transmit_DATA(CAN4_SFR, screen_ota_frame);
}

bool Send_Package_To_Small_Screen_By_Can_Way()
{
	// 分包发送，每包大小为1024Byte。对应4个帧，分别为起始帧0x7F0,传送帧0x7F1(发送1024Byte包数据),结束帧0x7F2,应答帧0x7F3
	// 应答帧0x7F3:包ID(2B)+小屏幕状态(1B)+预留(5B)

	static uint32_t send_Pkg_Info_To_Screen_clk = 0;
	if(SystemtimeClock - send_Pkg_Info_To_Screen_clk < 10) return false;	// 10ms
	send_Pkg_Info_To_Screen_clk = SystemtimeClock;

	// 【0：初始化】【1：取数据】【2：发送数据】
	switch(screen_single_pkg_info.send_state)
	{
	case SCREEN_SEND_INIT:{	// 初始化
		screen_single_pkg_info.addr = OUT_FLASH_SCREEN_START;
		screen_single_pkg_info.send_state = SCREEN_READ_DATA;
		screen_single_pkg_info.pkg_index = 0;
		screen_single_pkg_info.delay_times = 0;
		screen_single_pkg_info.pkg_num = upgrade_info.size / SUB_PKG_SIZE;
	}break;
	case SCREEN_READ_DATA:{	// 取数
		memset(screen_single_pkg_info.data, 0, SUB_PKG_SIZE);
		if(screen_single_pkg_info.pkg_index <= 1){
			screen_single_pkg_info.addr = OUT_FLASH_SCREEN_START;
			screen_single_pkg_info.pkg_index = 1;
		}
		else {
			screen_single_pkg_info.addr = OUT_FLASH_SCREEN_START + (screen_single_pkg_info.pkg_index-0) * SUB_PKG_SIZE;
			screen_single_pkg_info.pkg_index++;
		}
		W25QXX_Read(screen_single_pkg_info.data, screen_single_pkg_info.addr, SUB_PKG_SIZE);

		for(uint16_t i=0; i<SUB_PKG_SIZE; i++){	// 求校验
			screen_single_pkg_info.clac_chk += screen_single_pkg_info.data[i];
		}
		screen_single_pkg_info.send_state = SEND_STRAT_FRAME;

		// 发送开始帧
		Send_PKG_Start_Frame_To_Small_Screen_By_CAN4(screen_single_pkg_info.pkg_index, SUB_PKG_SIZE, screen_single_pkg_info.pkg_num);
	}break;
	case SEND_STRAT_FRAME:{			// 发送开始帧
		if(screen_single_pkg_info.recv_state == SCREEN_RECV_OK)	// 起始帧，需等待回复
		{
			screen_single_pkg_info.recv_state = SCREEN_RECV_INIT;
			screen_single_pkg_info.send_state = SEND_DATA_FRAME;
		}else{
			if(screen_single_pkg_info.delay_times > 1){			// 20ms
				screen_single_pkg_info.delay_times = 0;
				Send_PKG_Start_Frame_To_Small_Screen_By_CAN4(screen_single_pkg_info.pkg_index, SUB_PKG_SIZE, screen_single_pkg_info.pkg_num);
			}else screen_single_pkg_info.delay_times++;
		}
	}break;
	case SEND_DATA_FRAME:{			// 发送数据帧后，直接发送结束帧，并等待反馈
		for(uint16_t i=0; i<SUB_PKG_SIZE/8; i++){
			uint8_t data[8] = {0};
			memcpy(data, screen_single_pkg_info.data + i*8, 8);
			Send_PKG_Transfer_Frame_To_Small_Screen_By_CAN4(data);
		}
		screen_single_pkg_info.send_state = SEND_END_FRAME;
	}break;
	case SEND_END_FRAME:{			// 发送结束帧
		IWDT_Feed_The_Dog();		// 踢狗
		if(screen_single_pkg_info.recv_state == SCREEN_RECV_OK)			// 结束帧，需等待回复
		{
			screen_single_pkg_info.recv_state = SCREEN_RECV_INIT;

			// 最后一包
			if(screen_single_pkg_info.pkg_index == screen_single_pkg_info.pkg_num){
				USART1_Bebug_Print("", "Small SCreen OTA Successful.", 1);
				upgrade_info.screen_isUpgrade = 0;
				upgrade_info.rx_mode = 0;
				return true;
			}else{
				screen_single_pkg_info.send_state = SCREEN_READ_DATA;	// 发送下一包，取数
			}
		}else if(screen_single_pkg_info.recv_state == SCREEN_RECV_ERR){	// 重发
			screen_single_pkg_info.recv_state = SCREEN_RECV_INIT;
			screen_single_pkg_info.send_state = SEND_STRAT_FRAME;		// 重发当前包
		}else{															// 重发结束帧指令
			if(screen_single_pkg_info.delay_times > 1){					// 20ms
				screen_single_pkg_info.delay_times = 0;
				Send_PKG_End_Frame_To_Small_Screen_By_CAN4(screen_single_pkg_info.pkg_index, screen_single_pkg_info.clac_chk);
			}else screen_single_pkg_info.delay_times++;
		}
	}break;
	}
}


void Analysis_CAN4_Screen_OTA_CMD(uint32_t frameID, uint8_t *data)
{
	if(frameID == SCREEN_OTA_REPLY_FRAME){
		// 包ID
		screen_single_pkg_info.recv_pkg_index = data[1]<<8 | data[0];
		// 包状态
		screen_single_pkg_info.recv_state = data[2];
		if(data[2] == 0) screen_single_pkg_info.recv_state = SCREEN_RECV_OK;
		else if(data[2] == 1) screen_single_pkg_info.recv_state = SCREEN_RECV_ERR;

		if(screen_single_pkg_info.recv_pkg_index != screen_single_pkg_info.pkg_index){
			USART1_Bebug_Print_Num("[RECV]Package Index Is Different, sendIndex", screen_single_pkg_info.pkg_index, 0, 1);
			USART1_Bebug_Print_Num("recvIndex", screen_single_pkg_info.recv_pkg_index, 1, 1);
			screen_single_pkg_info.pkg_index = screen_single_pkg_info.recv_pkg_index - 1;	// 从反馈中的包ID中再次取数再发送
		}
	}
}
