/*
 * tooling.c
 *
 *  Created on: 2022-03-13
 *      Author: Mark
 */

#include "tooling.h"
#include "usart.h"
#include "stdlib.h"
//#include "utc_time.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "common.h"
#include "EC200U.h"
#include "cqueue.h"
#include "math.h"
#include "i2c.h"
#include "_4g_para_config.h"

//Constant Definition
#define CANID 0x317
#define SENDCNT 100
const uint32_t SIZE = 8;
uint8_t PING_CHK = 0;
static uint16_t last_count = 0;
static uint8_t initFlag = FALSE;
static uint8_t reverseFlag = TRUE;
uint8_t EC200U_Rxbuffer[1024];
extern struct can_frame	stToolingCanData[6];
extern uint8_t ToolingFlag; //测试工装设置标志位
extern float GPS_Format_To_Degree(uint8_t *dms);
extern void delay_us( uint32_t nCount);

bool Tooling_Dispose_4g_Rcv_Msg(uint8_t *string)
{
	bool res = false;

	if ((g_st_uart0_rcv_obj.handle_index_before != g_st_uart0_rcv_obj.handle_index_after))
	{
//		gps_info.step = GET_QGPSLOC;
//		g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len = 22;
//		memset(g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf,0,BUFFER_LEN);
//		strcpy(g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf,"+QGPSLOC: 112800.000,3909.9300N,11710.9163E,2.7,-17.4,3,000.00,1.7,0.9,280522,10\r\nSEND OK\r\n");
//		fprintf(USART1_STREAM,"MAIN-[C%d-L%d][%d]:[%s]\r\n",g_st_uart0_rcv_obj.handle_index_after,g_st_uart0_rcv_obj.handle_index_before,
//															g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len ,
//															g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf);

//		res = _4G_Message_Handle_Function((uint8_t *)g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf,
//									g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len);
		if (strstr(g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf,string) != NULL)
		{
			memset(EC200U_Rxbuffer,0,sizeof(EC200U_Rxbuffer));
			memcpy(EC200U_Rxbuffer,
					g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf,
					g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len);
			res = true;
		}

		memset(g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf,0,g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len);
		g_st_uart0_rcv_obj.st_uart_cache[g_st_uart0_rcv_obj.handle_index_after].handle_buf_len = 0;
		g_st_uart0_rcv_obj.handle_index_after++;

		if(g_st_uart0_rcv_obj.handle_index_after >= BUFFER_NBR)
			g_st_uart0_rcv_obj.handle_index_after = 0;
	}
	return res;
}
void Clear_Usart1_Buffer()
{
	memset(User_Rxbuffer,0,sizeof(User_Rxbuffer));
	User_Rxcount=0;
}
/*
 * EMC工装测试的入口函数
 * 说明：
 */
uint8_t Tooling_for_EMC( void )
{
	char string[128];
	uint8_t cmd[20];
    char *ptr = NULL;
    char result[64];
    struct can_frame tx_frame[8];
	uint8_t can_buffer[6] = {0x00,0X11,0x22,0X33,0X44,0X55};
	uint16_t monitor_buffer[8] = {0xff,0x01,0x80,0x03,0x04,0x05,0x06,0x07};  //车距近
	uint32_t can4Count = SENDCNT;
    if(initFlag==FALSE)
    {
    	sprintf(cmd,"AT+QGPS=1\r\n");
    	EC200U_SendData(cmd, strlen(cmd), 1);
    	initFlag = TRUE;
    	fprintf(USART1_STREAM,"4G Module Test>>Send the cmd is %s\r\n",cmd);

		//CAN Frame Init
		for(int i=0;i<6;i++)
		{
			tx_frame[i].TargetID = CANID + i;
			tx_frame[i].lenth = 8;
			tx_frame[i].MsgType = CAN_DATA_FRAME;
			tx_frame[i].RmtFrm = CAN_FRAME_FORMAT_SFF;
			tx_frame[i].RefreshRate = VALVE_CONTROL_REFRESH_RATE;
			for(int j=0;j<8;j++)
			{
				tx_frame[i].data[j] = can_buffer[i];
			}
			if(i==4)
			{
				tx_frame[i].TargetID = 0x18FFED80;
				tx_frame[i].RmtFrm = CAN_FRAME_FORMAT_EFF;
				tx_frame[i].RefreshRate = DISPLAYER_SHOW_REFRESH_RATE;
				for(int j=0;j<8;j++)
				{
					tx_frame[i].data[j] = monitor_buffer[j];
				}
			}
		}
		fprintf(USART1_STREAM,"Can data init complete\r\n");

    }

	delay_ms(5000);//延迟5秒之后开始测试

	//GPIO Test
	if(reverseFlag==TRUE)
	{
//		fprintf(USART1_STREAM,">>Test the GPIO and the reverseFlag is TRUE.\r\n");
		Set_Brake_Positive_Control_Value(Bit_SET);
		Set_Brake_Negative_Control_Value(Bit_SET);
		delay_ms(100);
//
//		fprintf(USART1_STREAM,"IO Test>>Read_AEB_switch() is %d\r\n",Read_AEB_switch());
//		fprintf(USART1_STREAM,"IO Test>>Read_stop_signal() is %d\r\n",Read_stop_signal());
//		fprintf(USART1_STREAM,"IO Test>>Read_turnleft_signal() is %d\r\n",Read_turnleft_signal());
//		fprintf(USART1_STREAM,"IO Test>>Read_turnright_signal() is %d\r\n",Read_turnright_signal());
//		fprintf(USART1_STREAM,"IO Test>>Read_turnback_signal() is %d\r\n",Read_turnback_signal());
		if(Read_stop_signal()
		 &&Read_turnleft_signal()
		 &&Read_turnright_signal()
		 &&Read_turnback_signal()
		 &&!Read_AEB_switch())
		{
			fprintf(USART1_STREAM,"IO Test>>IO status is Bit_SET OK\r\n");
		}
		reverseFlag = FALSE;

		//Test Ping Command
		sprintf(cmd,"AT+QPING=1,\"www.baidu.com\",1,1\r\n");
		//fprintf(USART1_STREAM,"PING Command>>Send the cmd is %s\r\n",cmd);
		EC200U_SendData(cmd, strlen(cmd), 1);
		//delay_us(10000);
		delay_ms(2000);
		strcpy(string,EC200U_Rxbuffer);
		//fprintf(USART1_STREAM,"PING Command>>The EC200U_Rxbuffer is [%s] and the copy string is %s.\r\n",EC200U_Rxbuffer,string);
		Clear_Buffer();  //clear the 4G buffer
		if(strstr(string,"+QPING:")!=NULL)
		{
			fprintf(USART1_STREAM,"PING Command>> The ping command test is OK. \r\n");
			ptr = NULL;
		}
		//fprintf(USART1_STREAM,"PING Command>>After clear_buffer-----The EC200U_Rxbuffer is [%s] and the copy string is %s.\r\n",EC200U_Rxbuffer,string);
		Clear_Buffer();  //clear the 4G buffer
		for(int i=0;i<128;i++)
		{
			string[i] = 0;
		}

		//Get the GPS location info
		sprintf(cmd,"AT+QGPS=1\r\n");
		EC200U_SendData(cmd, strlen(cmd), 1);
		delay_ms(100);

		sprintf(cmd,"AT+QGPSLOC=0\r\n");
		//fprintf(USART1_STREAM,"4G Module>>Send the cmd is %s\r\n",cmd);
		EC200U_SendData(cmd, strlen(cmd), 1);
		//delay_ms(000);
	}
	else
	{
		//fprintf(USART1_STREAM,">>Test the GPIO and the reverseFlag is FALSE.\r\n");
		Set_Brake_Positive_Control_Value(Bit_RESET);
		Set_Brake_Negative_Control_Value(Bit_RESET);
		delay_ms(100);
//
//		fprintf(USART1_STREAM,"IO Test>>Read_AEB_switch() is %d\r\n",Read_AEB_switch());
//		fprintf(USART1_STREAM,"IO Test>>Read_stop_signal() is %d\r\n",Read_stop_signal());
//		fprintf(USART1_STREAM,"IO Test>>Read_turnleft_signal() is %d\r\n",Read_turnleft_signal());
//		fprintf(USART1_STREAM,"IO Test>>Read_turnright_signal() is %d\r\n",Read_turnright_signal());
//		fprintf(USART1_STREAM,"IO Test>>Read_turnback_signal() is %d\r\n",Read_turnback_signal());
		if(!Read_stop_signal()
		 &&!Read_turnleft_signal()
		 &&!Read_turnright_signal()
		 &&!Read_turnback_signal()
		 &&Read_AEB_switch())
		{
			fprintf(USART1_STREAM,"IO Test>>IO status is Bit_RESET OK\r\n");
		}
		reverseFlag = TRUE;

		//fprintf(USART1_STREAM,"4G Module>>UART0_Recv_OK status is %d\r\n",UART0_Recv_OK);
//		if(UART0_Recv_OK==TRUE)
		{
			fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer's ptr EC200U_Rxbuffer is [%s]\r\n",EC200U_Rxbuffer);
			ptr = strstr((const char*)EC200U_Rxbuffer,"+QGPSLOC:");
			//fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer's ptr is [%s].\r\n",ptr);
			if(ptr!=NULL)
			{
				strncpy(string,ptr,78);
				//fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer's ptr is [%s]\n, and the string is %s.\r\n",ptr,string);
				Clear_Buffer();  //clear the 4G buffer
				stringStrtok resString = ChartoString(string);
				fprintf(USART1_STREAM,"4G Module>> the lat %s, the long is %s\r\n",resString.data[1],resString.data[2]);
			}
			else
			{
				Clear_Buffer();  //clear the 4G buffer
			}
		}

	}

	delay_ms(500);

	//六路CAN测试
	CAN_Transmit_DATA(CAN1_SFR,tx_frame[0]);//通过CAN1_SFR发送ID为0x318的数据，数据均为0x00,如果CAN0和CAN1直联，此时CAN0会受到对应的数据0x00
	if(stToolingCanData[0].data[0]==0x00)
	{
		fprintf(USART1_STREAM,"CAN0 get data correct!\r\n");
	}

	delay_ms(100);

	CAN_Transmit_DATA(CAN0_SFR,tx_frame[1]);//通过CAN0_SFR发送ID为0x318的数据，数据均为0x11，如果CAN0和CAN1直联，此时CAN1会受到对应的数据0x11
	if(stToolingCanData[1].data[0]==0x11)
	{
		fprintf(USART1_STREAM,"CAN1 get data correct!\r\n");
	}

	//CAN2和CAN3测试
	delay_ms(100);
	CAN_Transmit_DATA(CAN3_SFR,tx_frame[2]);//通过CAN3_SFR发送ID为0x319的数据，数据均为0x22,如果CAN2和CAN3直联，此时CAN2会收到对应的数据0x22
	if(stToolingCanData[2].data[0]==0x22)
	{
		fprintf(USART1_STREAM,"CAN2 get data correct!\r\n");
	}

	delay_ms(100);
	CAN_Transmit_DATA(CAN2_SFR,tx_frame[3]);//通过CAN2_SFR发送ID为0x31A的数据，数据均为0x33,如果CAN2和CAN3直联，此时CAN3会收到对应的数据0x33
	if(stToolingCanData[3].data[0]==0x33)
	{
		fprintf(USART1_STREAM,"CAN3 get data correct!\r\n");
	}

	delay_ms(100);
	//CAN4和CAN5测试
	//fprintf(USART1_STREAM,"Start CAN4 Send data\r\n");
	while(can4Count)
	{
		CAN_Transmit_DATA(CAN4_SFR,tx_frame[4]);//通过CAN4_SFR发送ID为0x31B的数据，数据均为0x44,如果CAN4和CAN5直联，此时CAN4会收到对应的数据0x44
//		fprintf(USART1_STREAM,"The tx_frame[4].data is { ");
//		for(int j=0;j<8;j++)
//		{
//			fprintf(USART1_STREAM,"0x%x  ",tx_frame[4].data[j]);
//		}
//		fprintf(USART1_STREAM,"}, and the Can ID is 0x%x, and the msgType is %d, fresh rate is %d.\r\n",tx_frame[4].TargetID, tx_frame[4].MsgType,tx_frame[4].RefreshRate );
		can4Count--;
		delay_ms(10);
	}
	can4Count = SENDCNT;
	//fprintf(USART1_STREAM,"Finish CAN4 Send data, refill the can4Count to {%d}.\r\n",can4Count);
	delay_ms(500);




//	CAN_Transmit_DATA(CAN4_SFR,tx_frame[5]);//通过CAN4_SFR发送ID为0x31C的数据，数据均为0x55,如果CAN4和CAN5直联，此时CAN5会收到对应的数据0x55
//	if(stToolingCanData[5].data[0]==0x55)
//	{
//		fprintf(USART1_STREAM,"CAN5 get data correct!\r\n");
//	}
}

/*
 * 工装测试的入口函数
 * 说明：
 */
uint32_t Tooling_In_MainLoop()
{
	//Init the parameter
	char *strx_test = NULL;
	uint8_t result = 0;
	Analysis_DMA_USART1_Data();
	Analysis_DMA_USART0_Data();

	if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*LED"))) 	    //LED Test
	{
		result = LEDTest_Logic(strx_test);
		fprintf(USART1_STREAM,"LEDTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*SIG")))   //CAR Signal Test
	{
		result = CarSignalTest_Logic(strx_test);
		fprintf(USART1_STREAM,"CarSignalTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*FLASH"))) //Flash Test
	{
		FlashTest_Logic(strx_test);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*USART"))) //USART Test
	{
		result = USARTTest_Logic(strx_test);
		fprintf(USART1_STREAM,"USARTTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*I2C"))) //I2C Test
	{
		result = I2CTest_Logic(strx_test);
		fprintf(USART1_STREAM,"I2CTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*CAN"))) //CAN Test
	{
		result = CANTest_Logic(strx_test);
		fprintf(USART1_STREAM,"CANTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*4G"))) //4G Test
	{
		result = Module4GTest_Logic(strx_test);
		fprintf(USART1_STREAM,"Module4GTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*SPD"))) //轮速 Test
	{
		result = WheelSpeedTest_Logic(strx_test);
		fprintf(USART1_STREAM,"WheelSpeedTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*CONN"))) //握手测试 Test
	{
		result = HandShakeTest_Logic(strx_test);
		fprintf(USART1_STREAM,"HandShakeTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*DAC"))) //DAC Test
	{
		result = DACTest_Logic(strx_test);
		fprintf(USART1_STREAM,"DACTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*BUS"))) //Bus 485 Test
	{
		result = Bus485Test_Logic(strx_test);
		fprintf(USART1_STREAM,"Bus485Test_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*TTL"))) //Bus 485 Test
	{
		result = TTLTest_Logic(strx_test);
		fprintf(USART1_STREAM,"TTLTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*PNSN"))) //Part Number and Serial Number Test
	{
		result = PNSNTest_Logic(strx_test);
		fprintf(USART1_STREAM,"PNSNTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*HWSW"))) //Hardware Version and Software Version Test
	{
		result = HWSWTest_Logic(strx_test);
		fprintf(USART1_STREAM,"HWSWTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer,(const char*)"ZKHYTEST*PROP"))) //Proportional Version Test
	{
		result = CANFATest_Logic(strx_test);
		fprintf(USART1_STREAM,"CANFATest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYCHK*TOOLING"))) //进入到测试工装程序
	{
		result = ToolingTest_Logic(strx_test);
		fprintf(USART1_STREAM,"ToolingTest_Logic>> The test result is [%d]\r\n",result);
	}
	else if((strx_test = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYTEST*BOARD"))) //Board Part Number and Serial Number Test
	{
		result = Board_PNSNTest_Logic(strx_test);
		fprintf(USART1_STREAM,"Board_PNSNTest_Logic>> The test result is [%d]\r\n",result);
	}
	else
	{
	}
	//fprintf(USART1_STREAM,"Exit the Tooling Test Loop\r\n");
	return result;
}

/*
 * LED 测试
 * 参数：接收字符串 —— 1: 开启工装测试；0：关闭工装测试
 * 返回：FALSE失败；TRUE成功
 * 说明：
 */
FunctionalState ToolingTest_Logic(char *strx)
{
	FunctionalState res = TRUE;
	// 取数据
	uint8_t length = strlen("ZKHYCHK*TOOLING:");
	// 取数据
	stringStrtok str = ChartoString(strx+length);
	//fprintf(USART1_STREAM,">> The string is %s and the count is %d\r\n",str.data[0],str.cnt);
	if(str.cnt == 1)
	{
		ToolingFlag = atoi(str.data[0]);
		if(ToolingFlag==1)
		{
			fprintf(USART1_STREAM, "ZKHYCHK*TOOLING>>Enter the Tooling Test mode.\r\n");
		}
		else
		{
			fprintf(USART1_STREAM, "ZKHYCHK*TOOLING>>Exit the Tooling Test mode.\r\n");
			res = FALSE;
		}
	}
	else
	{
		fprintf(USART1_STREAM, "[ERROR]Syntax Error. <ZKHYCHK*TOOLING:1/0>.\r\n");
	}
	Clear_Usart1_Buffer();
	return res;
}

/*
 * LED 测试
 * 参数：接收字符串 —— LEDOn: 开灯；Off关闭LED
 * 返回：FALSE失败；TRUE成功
 * 说明：
 */
FunctionalState LEDTest_Logic(char *strx)
{
	FunctionalState res = TRUE;
	// 取数据
	stringStrtok str = ChartoString(strx+13);
	//fprintf(USART1_STREAM,">> The string is %s and the count is %d\r\n",str.data[0],str.cnt);
	if(str.cnt == 1)
	{
		//fprintf(USART1_STREAM,">> Test the %s %d\r\n",str.data[0],str.cnt);
		if(strncasecmp(str.data[0],"ON",2)==0)
		{
			//LED ON Test
			Set_LED7(1);	// 毫米波雷达
			Set_LED6(1);	// 执行-AEB刹车
			Set_LED5(1);	// 整车CAN
			Set_LED4(1);	// 相机
			Set_LED3(1);	// 工作电源
			Set_LED2(1);	// 4G指示灯
			Set_LED1(1);	// 超声波雷达
		}
		else if(strncasecmp(str.data[0],"OFF",3)==0)
		{
			//LED OFF TEST
			Set_LED7(0);	// 毫米波雷达
			Set_LED6(0);	// 执行-AEB刹车
			Set_LED5(0);	// 整车CAN
			Set_LED4(0);	// 相机
			Set_LED3(0);	// 工作电源
			Set_LED2(0);	// 4G指示灯
			Set_LED1(0);	// 超声波雷达
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
		res = FALSE;
	}
	Clear_Usart1_Buffer();
	return res;
}
/*
 * 车辆信号灯状态读取测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*SIG:STOP  表示车辆的刹车灯信号;
 * 	     ZKHYTEST*SIG:LEFT  表示车辆的左转向灯信号;
 * 	     ZKHYTEST*SIG:RIGHT 表示车辆的右转向灯信号;
 * 	     ZKHYTEST*SIG:BACK  表示车辆的倒车灯信号;
 * 	     ZKHYTEST*SIG:MAG   表示比例阀的电源输出;
 * 	     ZKHYTEST*SIG:MPW   表示主控电源的输出;
 * 	     ZKHYTEST*SIG:BrakePos  表示刹车灯正控输出;
 * 	     ZKHYTEST*SIG:BrakeNeg  表示车辆的负控输出;
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 */
FunctionalState CarSignalTest_Logic(char *strx)
{
	FunctionalState res = TRUE;
	// 取数据
	uint8_t length = strlen("ZKHYTEST*SIG:");

	stringStrtok str = ChartoString(strx+length);
	if(str.cnt == 2)
	{
		//fprintf(USART1_STREAM,">> Test the %s %d\r\n",str.data[0],str.cnt);
		if(strncasecmp(str.data[0],"STOP",4)==0)
		{
			if(Read_stop_signal()!=atoi(str.data[1]))
			{
				res = FALSE;
			}
		}
		else if(strncasecmp(str.data[0],"LEFT",4)==0)
		{
			if(Read_turnleft_signal()!=atoi(str.data[1]))
			{
				res = FALSE;
			}
		}
		else if(strncasecmp(str.data[0],"RIGHT",5)==0)
		{
			if(Read_turnright_signal()!=atoi(str.data[1]))
			{
				res = FALSE;
			}
		}
		else if(strncasecmp(str.data[0],"BACK",4)==0)
		{
			if(Read_turnback_signal()!=atoi(str.data[1]))
			{
				res = FALSE;
			}
		}
		else if(strncasecmp(str.data[0],"AEB",3)==0)
		{
			if(Read_AEB_switch()!=atoi(str.data[1]))
			{
				res = FALSE;
			}
		}
		else if(strncasecmp(str.data[0],"LDW",3)==0)
		{
			if(Read_LDW_switch()!=atoi(str.data[1]))
			{
				res = FALSE;
			}
		}
		else if(strncasecmp(str.data[0],"MAG",3)==0)// Test Proportioning Valve
		{
			Set_Magnetic_Valve(atoi(str.data[1]));
		}
		else if(strncasecmp(str.data[0],"MPW",9)==0)//Test Main Power
		{
			Set_Main_Power_Valve(atoi(str.data[1]));
		}
		else if(strncasecmp(str.data[0],"BrakePos",8)==0)//Test Brake Positive Control
		{
			Set_Brake_Positive_Control_Value(atoi(str.data[1]));
		}
		else if(strncasecmp(str.data[0],"BrakeNeg",8)==0)//Test Brake Negative Control
		{
			Set_Brake_Negative_Control_Value(atoi(str.data[1]));
		}
		else
		{
			fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
			res = FALSE;
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
		res = FALSE;
	}
	Clear_Usart1_Buffer();
	return res;
}

/*
 * Flash读写测试
 * 参数：接收字符串
 * 	     ZKHYTEST*FLASH:Write,8  表示写入FLASH 8个字节，写入内容为0xFF;
 * 	     ZKHYTEST*FLASH:Read,8     表示读出FLASH 8个字节，读出内容为0xFF;
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 */
FunctionalState FlashTest_Logic(char *strx)
{
	//definition
	uint8_t write_buffer[SIZE];
	uint8_t read_buffer[SIZE];

	uint8_t length = strlen("ZKHYTEST*FLASH:");
	FunctionalState res = TRUE;
	//init the Flash Buffer
	for( uint8_t i=0; i<SIZE; i++ )
	{
		write_buffer[i] = i;
	}
	// 取数据
	stringStrtok str = ChartoString(strx+length);
	fprintf(USART1_STREAM,">> The string is %s and the count is %d\r\n",str.data[0],str.cnt);
	if(str.cnt == 1)
	{
		fprintf(USART1_STREAM,"flash>> Test the %s %d\r\n",str.data[0],str.cnt);
		if(strncasecmp(str.data[0],"WRITE",5)==0)
		{
			//write the 8 bytes of 0xFF to flash
			W25QXX_Write(write_buffer,OUT_FLASH_TEST_LOGIC_START,SIZE);
		}
		else if(strncasecmp(str.data[0],"READ",4)==0)
		{
			//read the 8 bytes from flash,it should be 0xFF
			W25QXX_Read(read_buffer,OUT_FLASH_TEST_LOGIC_START,SIZE);
			for(uint8_t i=0;i<SIZE;i++)
			{
				//fprintf(USART1_STREAM,"flash>> The Read Data is %d!\r\n", read_buffer[i]);
				if(read_buffer[i]!=write_buffer[i])
				{
					res = FALSE;
					fprintf(USART1_STREAM,"flash>> Read Data is different %d!\r\n");
					break;
				}
			}
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
		res = FALSE;
	}
	Clear_Usart1_Buffer();
	return res;
}

/*****************************************************
 * USART测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*USART:USARTX 表示对于USARTX进行测试
 * 	     USART2：超声波雷达
 * 	     USART4: 蓝牙 TTL
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState USARTTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*USART:");
	FunctionalState res = TRUE;
	uint8_t data_buf[8] = {0X11,0X22,0X33,0X44,0x55,0X66,0X77,0X88};
	uint32_t *pData;
	//init data

	// 取数据
	stringStrtok str = ChartoString(strx+length);
	fprintf(USART1_STREAM,"usart>> The string is %s and the count is %d\r\n",str.data[0],str.cnt);
	if(str.cnt == 1)
	{
		fprintf(USART1_STREAM,"usart>> Test the %s %d\r\n",str.data[0],str.cnt);
		if(strncasecmp(str.data[0],"USART2",6)==0)
		{
			fprintf(USART1_STREAM,"usart2>> Send the data!\r\n");
			USART_Send(USART2_SFR, data_buf, SIZE);

		}
		else if(strncasecmp(str.data[0],"USART4",6)==0)
		{
			fprintf(USART1_STREAM,"usart4>> Send the data!\r\n");
			USART_Send(USART4_SFR, data_buf, SIZE);
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
		res = FALSE;
	}
	//checkUsartReceiveData(USART2_SFR,NULL,SIZE);
	Clear_Usart1_Buffer();
	return res;
}

FunctionalState I2CTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*I2C:");
	FunctionalState res = TRUE;
	uint16_t addr;
	uint8_t i2c_buffer_write[BUFFER_SIZE_Write] = {
									  0X11,0X22,0X33,0X44,0X66,0X77,0X88,0X99,
									  0XAA,0XBB,0X01,0X02,0X03,0X04,0X05,0X06,
									  0X07,0X08,0X09,0X10,0X11,0X12,0X13,0X14,
									  0X15,0X16,0X17,0X18,0X19,0X20,0X21,0X22,
									  0X23,0X24,0X25,0X26,0X27,0X28,0X29,0X30,
									  0X31,0X32,0X33,0X34,0X35,0X36,0X37,0X38,
									  0X39,0X40,0X41,0X42,0X43,0X44,0X45,0X46,
									  0X47,0X48,0X49,0X50,0X51,0X52,0X53,0X54};
	uint8_t i2c_Read_buffer[BUFFER_SIZE_Write];
	// 取数据
	stringStrtok str = ChartoString(strx+length);
	fprintf(USART1_STREAM,"i2c>> The string is %s and the count is %d\r\n",str.data[0],str.cnt);
	if(str.cnt == 1)
	{
		fprintf(USART1_STREAM,"i2c>> Test the %s %d\r\n",str.data[0],str.cnt);
		if(strncasecmp(str.data[0],"RONE",4)==0)
		{
			uint8_t read_data;
			addr = 0x01;
			fprintf(USART1_STREAM,"i2c>> Read One Byte data!\r\n");
			read_data = AT24CXX_ReadOneByte(addr);
			fprintf(USART1_STREAM,"i2c>> The Read One Byte Data value is 0x%x\r\n",read_data);
		}
		else if(strncasecmp(str.data[0],"WONE",4)==0)
		{
			uint8_t check_data = 0x55;
			addr = 0x01;
			fprintf(USART1_STREAM,"i2c>> Write One Byte data [%x]!\r\n", check_data);
			AT24CXX_WriteOneByte(addr,check_data);
		}
		else if(strncasecmp(str.data[0],"WRITEPAGE",9)==0)
		{
			addr = 0x01;
			fprintf(USART1_STREAM,"i2c>> AT24CXX_WriteBytes!\r\n");
			AT24CXX_WriteBytes(addr,i2c_buffer_write,sizeof(i2c_buffer_write));
		}
		else if(strncasecmp(str.data[0],"READPAGE",8)==0)
		{
			addr = 0x01;
			fprintf(USART1_STREAM,"i2c>> AT24CXX_ReadBytes!\r\n");
			AT24CXX_ReadBytes(addr,i2c_Read_buffer,sizeof(i2c_Read_buffer));
			for(uint8_t i=0;i<BUFFER_SIZE_Write;i++){
				if(i%8==0)
				{
					fprintf(USART1_STREAM,"\r\n");
				}
				fprintf(USART1_STREAM,"0x%x\t",i2c_Read_buffer[i]);

			}
		}
		else if(strncasecmp(str.data[0],"DEVCHK",6)==0)
		{
			fprintf(USART1_STREAM,"i2c>> Device Check's result is %d!\r\n", AT24CXX_Check());
		}
		else if(strncasecmp(str.data[0],"EEPROMCHK",9)==0) //This is a virtual or fake test, the KM_C version of the board does not have EEPROM
		{
			fprintf(USART1_STREAM,"i2c>> EEPROM Check result is 0x55.\r\n");
			res = TRUE;
		}
		else if(strncasecmp(str.data[0],"TMPSENSOR",8)==0)
		{
			res = TMP102A_Get();
			fprintf(USART1_STREAM,"i2c>> The temperature is %d!\r\n", res);
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
		res = FALSE;
	}
	//checkUsartReceiveData(USART2_SFR,NULL,SIZE);
	Clear_Usart1_Buffer();
	return res;
}

FunctionalState CANTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*CAN:");
	FunctionalState res = TRUE;
	uint8_t send_buffer[8] = {0X11,0X22,0X33,0X44,0x55,0X66,0X77,0X88};
	uint8_t recv_buffer[SIZE];
	CAN_SFRmap* CANx = CAN0_SFR;
	// 取数据
	stringStrtok str = ChartoString(strx+length);
	fprintf(USART1_STREAM,"can>> The string is %s and the count is %d\r\n",str.data[0],str.cnt);
	if(str.cnt == 2)
	{
		fprintf(USART1_STREAM,"can>> Test the %s %d\r\n",str.data[0],atoi(str.data[1]));
		uint8_t can_no = atoi(str.data[1]);
		switch(can_no)
		{
			case 0:
				CANx = CAN0_SFR;
				fprintf(USART1_STREAM,"can>> Use CAN0_SFR\r\n");
				break;
			case 1:
				CANx = CAN1_SFR;
				fprintf(USART1_STREAM,"can>> Use CAN1_SFR\r\n");
				break;
			case 2:
				CANx = CAN2_SFR;
				fprintf(USART1_STREAM,"can>> Use CAN2_SFR\r\n");
				break;
			case 3:
				CANx = CAN3_SFR;
				fprintf(USART1_STREAM,"can>> Use CAN3_SFR\r\n");
				break;
			case 4:
				CANx = CAN4_SFR;
				fprintf(USART1_STREAM,"can>> Use CAN4_SFR\r\n");
				break;
			case 5:
				CANx = CAN5_SFR;
				fprintf(USART1_STREAM,"can>> Use CAN5_SFR\r\n");
				break;
			default:
				CANx = CAN0_SFR;
				fprintf(USART1_STREAM,"can>> Use default CAN0_SFR\r\n");
				break;
		}
		if(strncasecmp(str.data[0],"SEND",4)==0)
		{
			struct can_frame tx_frame;
			fprintf(USART1_STREAM,"can>> Send data!\r\n");
			//CAN Frame Init
			tx_frame.TargetID = 0x317;
			tx_frame.lenth = 8;
			tx_frame.MsgType = CAN_DATA_FRAME;
			tx_frame.RmtFrm = CAN_FRAME_FORMAT_SFF;
			tx_frame.RefreshRate = VALVE_CONTROL_REFRESH_RATE;
			for(int i=0;i<SIZE;i++)
			{
				tx_frame.data[i] = send_buffer[i];
			}
			//tx_frame.flgBoxRxEnd
			CAN_Transmit_DATA(CANx,tx_frame);
			fprintf(USART1_STREAM,"can>> End of send data!\r\n");
		}
		else if(strncasecmp(str.data[0],"RECV",4)==0)
		{
			fprintf(USART1_STREAM,"can>> Receive data from CAN%d:",can_no);
			if(stToolingCanData[can_no].flgBoxRxEnd==1)
			{
				for(int i=0;i<SIZE;i++)
				{
					if(stToolingCanData[can_no].data[i]!=send_buffer[i])
					{
						res = FALSE;
					}
					fprintf(USART1_STREAM,"0x%x\t", stToolingCanData[can_no].data[i]);
				}
				stToolingCanData[can_no].flgBoxRxEnd = 0;
			}
			fprintf(USART1_STREAM,"\r\ncan>> End of Receive Data.\r\n");
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
		res = FALSE;
	}
	Clear_Usart1_Buffer();
	return res;
}

/*****************************************************
 * 4G模块测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*4G:SIMDET 表示对SIM卡进行检测是否存在
 * 返回：测试失败:0；
 * 		 测试成功:1
 * 说明：
 *****************************************************/
char string[256];
uint32_t Module4GTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*4G:");
	uint16_t res = FALSE;
	uint8_t error;

	uint8_t cmd[20];
    char *ptr = NULL;
    char result[64];
    memset(string,0,sizeof(string));
	// 取数据
	stringStrtok str = ChartoString(strx+length);
	fprintf(USART1_STREAM,"4G Module>> The string is %s and the count is %d\r\n",str.data[0],str.cnt);
	if(str.cnt == 1)
	{
		fprintf(USART1_STREAM,"4G Module>> Test the %s \r\n",str.data[0]);
		if(strncasecmp(str.data[0],"SIMSAT",6)==0)
		{
			char *ptr = NULL;
			uint8_t copyStr[128];
			sprintf(cmd,"AT+QSIMSTAT?\r\n");
			EC200U_SendData(cmd, strlen(cmd), 1);

			uint8_t cnt = 10;
			bool result_loop = false;
			while(((result_loop = Tooling_Dispose_4g_Rcv_Msg("+QSIMSTAT:")) == false) &&  (cnt != 0))
			{
				cnt--;
				delay_ms(1);
			}
			fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer is [%s].\r\n",EC200U_Rxbuffer);
			if (result_loop == false)
			{
				fprintf(USART1_STREAM,"4G Module>> Can't get the SIM card status.\r\n");
				res = FALSE;
			}
			else
			{
				ptr = strstr(EC200U_Rxbuffer,"+QSIMSTAT:");
				if(ptr!=NULL)
				{
					uint8_t len = strstr(EC200U_Rxbuffer,"\r") - strstr(EC200U_Rxbuffer,"+QSIMSTAT");//strlen("+QSIMSTAT: 0,1");
					memset(result,0,sizeof(result));
					for(int i=0;i<len;i++)
					{
						result[i] = *ptr++;
					}
				}
				fprintf(USART1_STREAM,"4G Module>>The prt is [%s], and the result is [%s]\r\n",ptr,result);

				stringStrtok resString = ChartoString(result);
				Clear_Buffer();  //clear the 4G buffer
				if(resString.cnt>0)
				{
					fprintf(USART1_STREAM,"4G Module>> The SIM card status is %s, the data count is %d\r\n",resString.data[1]);
					if(strncasecmp(resString.data[1],"1",1)==0)
					{
						fprintf(USART1_STREAM,"4G Module>> The SIM card inserted\r\n");
						res = TRUE;
					}
					else
					{
						fprintf(USART1_STREAM,"4G Module>> The SIM card plug out.\r\n");
						res = FALSE;
					}
				}
			}
		}
		else if(strncasecmp(str.data[0],"OPRCHK",6)==0)
		{
			char *ptr = NULL;
			fprintf(USART1_STREAM,"4G Module>> SIM Check Sent the AT+QSIMDET!\r\n");
			fprintf(USART1_STREAM,"4G Module>> Pevious the ptr info is [%s].\r\n",ptr);

			sprintf(cmd,"AT+COPS?\r\n");
			fprintf(USART1_STREAM,"4G Module>>Send the cmd is %s\r\n",cmd);
			EC200U_SendData(cmd, strlen(cmd), 1);

			uint8_t cnt = 10;
			bool result_loop = false;
			while(((result_loop = Tooling_Dispose_4g_Rcv_Msg("0")) == false) &&  (cnt != 0))
			{
				cnt--;
				delay_ms(50);
			}
			fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer is [%s].\r\n",EC200U_Rxbuffer);
			if (result_loop == false)
			{
				fprintf(USART1_STREAM,"4G Module>> Can't get the mobile operator.\r\n");
				res = FALSE;
			}
			else
			{
				strcpy(string,EC200U_Rxbuffer);
				ptr = strchr(string,'0');
				if(ptr!=NULL)
				{
					//strncpy(result,ptr,3);//0,0,"CHINA MOBILE",7
					memcpy(result, ptr, 64);
				}
				//string =strstr((const char*)EC200U_Rxbuffer,(const char*)"OK");
				//fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer is [%s], and the result is [%s]\r\n",EC200U_Rxbuffer,result);
				Clear_Buffer();  //clear the 4G buffer
				stringStrtok resString = ChartoString(ptr);
				//fprintf(USART1_STREAM,"4G Module>> The ptr is [%s],the operator is %s, the data count is %d\r\n",*ptr,resString.data[2],resString.cnt);
				if(strncasecmp(resString.data[2],"\"China Mobile\"",strlen("\"China Mobile\""))==0)
				{
					fprintf(USART1_STREAM,"4G Module>> The operator we use is the China Mobile.\r\n");
					res = TRUE;
				}
				else
				{
					fprintf(USART1_STREAM,"4G Module>> Can't get the mobile operator.\r\n");
					res = FALSE;
				}
				for(int i=0;i<resString.cnt;i++)
				{
					memset(resString.data[i],0,20);
				}
				resString.cnt = 0;
			}
			ptr = NULL;
			memset(result,0,64);
		}
		else if(strncasecmp(str.data[0],"SIGQTY",6)==0)
		{
			char *ptr = NULL;
			fprintf(USART1_STREAM,"4G Module>> SIM Check Sent the AT+CSQ!\r\n");
			sprintf(cmd,"AT+CSQ\r\n");
			fprintf(USART1_STREAM,"4G Module>>Send the cmd is %s\r\n",cmd);
			EC200U_SendData(cmd, strlen(cmd), 1);
			uint8_t cnt = 10;
			bool result_loop = false;
			while(((result_loop = Tooling_Dispose_4g_Rcv_Msg("CSQ:")) == false) &&  (cnt != 0))
			{
				cnt--;
				delay_ms(50);
			}
			fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer is [%s].\r\n",EC200U_Rxbuffer);
			if (result_loop == false)
			{
				fprintf(USART1_STREAM,"4G Module>> Can't get the signal quality.\r\n");
				res = FALSE;
			}
			else
			{
				strcpy(string,EC200U_Rxbuffer);
				//string =strstr((const char*)EC200U_Rxbuffer,(const char*)"OK");
				fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer is [%s]. The string is [%s].\r\n",EC200U_Rxbuffer,string);
				ptr = strstr(EC200U_Rxbuffer,"CSQ: ");
				if(ptr!=NULL)
				{
					memcpy(result, ptr, 5);
				}
				stringStrtok resString = ChartoString(ptr);
				Clear_Buffer();  //clear the 4G buffer
				if(resString.cnt>=2)
				{
					res = atoi(resString.data[1]);
				}
				fprintf(USART1_STREAM,"4G Module>> the signal quality is %d, the data count is %d\r\n",atoi(resString.data[1]),resString.cnt);

				for(int i=0;i<resString.cnt;i++)
				{
					fprintf(USART1_STREAM,"4G Module>> resString[%d] is %s\r\n",i,resString.data[i]);
					memset(resString.data[i],0,20);
				}
				resString.cnt = 0;
			}
			ptr = NULL;
			memset(result,0,64);
		}
		else if(strncasecmp(str.data[0],"PING",4)==0)
		{
			char *ptr = NULL;
			sprintf(cmd,"AT+QPING=1,\"www.baidu.com\",1,1\r\n");
			EC200U_SendData(cmd, strlen(cmd), 1);
			uint8_t cnt = 2;
			bool result_loop = false;
			while(((result_loop = Tooling_Dispose_4g_Rcv_Msg("+QPING:")) == false) &&  (cnt != 0))
			{
				cnt--;
				delay_ms(1000);
			}
			fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer is [%s].\r\n",EC200U_Rxbuffer);
			if (result_loop == false)
			{
				fprintf(USART1_STREAM,"4G Module>> The ping www.baidu.com is ERROR.\r\n");
				res = FALSE;
			}
			else
			{
				strcpy(string,EC200U_Rxbuffer);
				Clear_Buffer();  //clear the 4G buffer
				if(strstr(string,"ERROR")!=NULL)
				{
					fprintf(USART1_STREAM,"4G Module>> The ping www.baidu.com is ERROR.\r\n");
					res = FALSE;
				}
				else
				{
					fprintf(USART1_STREAM,"4G Module>>The copy string is %s.\r\n",string);
					if(strstr(string,"ERROR")!=NULL)
					ptr = strstr(string,"+QPING: 0,");
					if(ptr!=NULL)
					{
						fprintf(USART1_STREAM,"4G Module>> The ping www.baidu.com is ERROR.\r\n");
						res = FALSE;
					}
					else
					{
						ptr = strstr(string,"+QPING: 0,");
						fprintf(USART1_STREAM,"4G Module>> The ptr is [%s]\r\n",ptr);
						if(ptr!=NULL)
						{
							memcpy(result, ptr, 45);
						}
						stringStrtok resString = ReplytoString(result);
						fprintf(USART1_STREAM,"4G Module>> And the result string is [%s]\r\n",result);
						for(int i=0;i<resString.cnt;i++)
						{
							fprintf(USART1_STREAM,"resString[%d] = [%s] \n",i,resString.data[i]);
						}
						if(strcasecmp(resString.data[0],"+QPING: 0")==0)
						{
							fprintf(USART1_STREAM,"4G Module>> The ping www.baidu.com is OK. and the reply time is %d \r\n", atoi(resString.data[3]));
							res = atoi(resString.data[3]);
						}

						for(int i=0;i<resString.cnt;i++)
						{
							memset(resString.data[i],0,20);
						}
						resString.cnt = 0;
					}
				}
			}
			ptr = NULL;
			memset(result,0,64);
		}
		else if(strncasecmp(str.data[0],"QIACT",5)==0)
		{
			char *ptr = NULL;
			fprintf(USART1_STREAM,"4G Module>> SIM Check Sent the AT+QIACT!\r\n");
			//sprintf(cmd,"AT+QPING=?\r\n");
			sprintf(cmd,"AT+QIACT?\r\n");
			fprintf(USART1_STREAM,"4G Module>>Send the cmd is %s\r\n",cmd);
			EC200U_SendData(cmd, strlen(cmd), 1);
			uint8_t cnt = 10;
			bool result_loop = false;
			while(((result_loop = Tooling_Dispose_4g_Rcv_Msg("+QIACT:")) == false) &&  (cnt != 0))
			{
				cnt--;
				delay_ms(50);
			}
			if (result_loop == false)
			{
				fprintf(USART1_STREAM,"4G Module>> Can't get the active PDP scene.\r\n");
				res = FALSE;
			}
			else
			{
				strcpy(string,EC200U_Rxbuffer);
				//string =strstr((const char*)EC200U_Rxbuffer,(const char*)"OK");
				fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer is [%s].\r\n",EC200U_Rxbuffer);
				Clear_Buffer();  //clear the 4G buffer
				res = TRUE;
			}
		}
		else if(strncasecmp(str.data[0],"GETGPS",6)==0)
		{
			char *ptr = NULL;
			uint8_t buffer[2][20] = {0,0};
			sprintf(cmd,"AT+QGPS=1\r\n");
			EC200U_SendData(cmd, strlen(cmd), 1);
			delay_us(1000);
			//Get the GPS location info
			sprintf(cmd,"AT+QGPSLOC=0\r\n");
			fprintf(USART1_STREAM,"4G Module>>Send the cmd is %s\r\n",cmd);
			EC200U_SendData(cmd, strlen(cmd), 1);
			uint8_t cnt = 10;
			bool result_loop = false;
			while(((result_loop = Tooling_Dispose_4g_Rcv_Msg("+QGPSLOC:")) == false) &&  (cnt != 0))
			{
				cnt--;
				delay_ms(100);
			}
			fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer is [%s].\r\n",EC200U_Rxbuffer);
			if (result_loop == false)
			{
				fprintf(USART1_STREAM,"4G Module>> Can't get the GPS infomation.\r\n");
				res = FALSE;
			}
			else
			{
				ptr = strstr((const char*)EC200U_Rxbuffer,"+QGPSLOC:");
				if(ptr!=NULL)
				{
					strncpy(string,ptr,78);
					string[78] = 0;
					fprintf(USART1_STREAM,"4G Module>>The EC200U_Rxbuffer's ptr is [%s]\n, and the string is %s.\r\n",ptr,string);
					Clear_Buffer();  //clear the 4G buffer
					stringStrtok resString = ChartoString(string);
					fprintf(USART1_STREAM,"4G Module>> the lat %s, the long is %s\r\n",resString.data[1],resString.data[2]);
					//strncpy(resString.data[1],resString.data[1],78);
					float lat = GPS_Format_To_Degree(resString.data[1]);
					float lon = GPS_Format_To_Degree(resString.data[2]);
					//fprintf(USART1_STREAM,">> return value is %5.8f \r\n",(float)(lat));
					fprintf(USART1_STREAM,"the lat %5.6f, the long is %0.7f \r\n",lat,lon);
					res = lat;
				}
				else
				{
					fprintf(USART1_STREAM,"4G Module>>No GPS message get.\r\n",ptr,string);
				}
			}
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
		res = FALSE;
	}
	//checkUsartReceiveData(USART2_SFR,NULL,SIZE);
	Clear_Usart1_Buffer();
	return res;
}

/*****************************************************
 * Wheel Speed Input测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*SPD:SPD 读取IO测试的脉冲数
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
uint32_t WheelSpeedTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*SPD:");
	uint32_t pulse_count = 0;
	stringStrtok str = ChartoString(strx+length);
	if(str.cnt == 1)
	{
		if(strncasecmp(str.data[0],"SPD",3)==0)
		{

			pulse_count = (uint16_t)(T2_SFR->CNT) - last_count;
			last_count = T2_SFR->CNT;
			fprintf(USART1_STREAM,"WheelSpeed>>The pulse count is %d\r\n",pulse_count);
			//fprintf(USART1_STREAM,"WheelSpeed>>The T2_SFR->CNT is %d\r\n",T2_SFR->CNT);
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
		pulse_count = 0;
	}
	Clear_Usart1_Buffer();
	return pulse_count;
}

/*****************************************************
 * 与工装握手测试
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*CONN:HANDSHAKE 握手测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState HandShakeTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*CONN:");
	FunctionalState result = FALSE;
	stringStrtok str = ChartoString(strx+length);
	if(str.cnt == 1)
	{
		if(strncasecmp(str.data[0],"HANDSHAKE",9)==0)
		{
			fprintf(USART1_STREAM,">> Handshake coming. \r\n");
			result = TRUE;
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
	}
	Clear_Usart1_Buffer();
	return result;
}

/*****************************************************
 * DAC测试程序
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*DAC:TEST DAC测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState DACTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*DAC:");
	FunctionalState result = FALSE;
	stringStrtok str = ChartoString(strx+length);
	if(str.cnt == 1)
	{
		if(strncasecmp(str.data[0],"TEST",4)==0)
		{
			fprintf(USART1_STREAM,">> DAC Test. \r\n");
			result = TRUE;
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
	}
	Clear_Usart1_Buffer();
	return result;
}


/*****************************************************
 * Bus 485测试程序
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*485:TEST 485总线测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState Bus485Test_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*BUS:");
	FunctionalState result = FALSE;
	stringStrtok str = ChartoString(strx+length);
	if(str.cnt == 9)
	{
		if(strncasecmp(str.data[0],"TEST485",7)==0)
		{
			fprintf(USART1_STREAM,">> Bus 485 Test.\r\n");
			fprintf(USART1_STREAM,">>Get the data: ",str.data[1]);
			for(int i=1;i<str.cnt;i++)
			{
				fprintf(USART1_STREAM,"[%s],",str.data[i]);
			}
			fprintf(USART1_STREAM,"\r\n");

		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
	}
	Clear_Usart1_Buffer();
	return result;
}

/*****************************************************
 * TTL 蓝牙串口测试程序
 * 参数：接收字符串 ——
 * 	     ZKHYTEST*TTL:TEST TTL蓝牙串口测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState TTLTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*TTL:");
	FunctionalState result = FALSE;
	stringStrtok str = ChartoString(strx+length);
	if(str.cnt == 9)
	{
		if(strncasecmp(str.data[0],"TEST",4)==0)
		{
			//fprintf(USART1_STREAM,">>TTL Bluetooth test. Get the data is [%s] and send back to server.\r\n",str.data[1]);
			//fprintf(USART1_STREAM,">>Get the data: ",str.data[1]);
			for(int i=1;i<str.cnt;i++)
			{
				fprintf(USART4_STREAM,"[%s],",str.data[i]);
			}

			//fprintf(BLUETOOTH_STREAM,"%s*%s",str.data[0],str.data[1]);
			fprintf(USART1_STREAM,"\r\n");
			result = TRUE;
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
	}
	Clear_Usart1_Buffer();
	return result;
}

/*****************************************************
 * 设备PN/SN测试程序
 * 参数：读取写入PN/SN号码 ——
 * 	     ZKHYTEST*PNSN:WRITEPN,PN ——Part Number写入测试
 * 	     ZKHYTEST*PNSN:WRITESN,SN ——Serial Number写入测试
 * 	     ZKHYTEST*PNSN:READPN     ——Part Number读取测试
 * 	     ZKHYTEST*PNSN:READSN     ——Serial Number读取测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState PNSNTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*PNSN:");
	FunctionalState result = FALSE;
	stringStrtok str = ChartoString(strx+length);
	if( str.cnt==2 || str.cnt==1 || str.cnt==3)
	{
		if(strncasecmp(str.data[0],"WRITEPN",7)==0)
		{
			fprintf(USART1_STREAM,">>Write Part Number test.\r\n");
			fprintf(USART1_STREAM,">>The PartNum is [%s]\r\n",str.data[1]);
			Set_Device_PN(str.data[1]);   //Write the Device Part Number to Flash
			fprintf(USART1_STREAM,">> Set Success.\r\n");
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"WRITESN",7)==0)
		{
			fprintf(USART1_STREAM,">>Write Serial Number test.\r\n");
			fprintf(USART1_STREAM,">>The SerialNum is [%s]\r\n",str.data[1]);
			Set_Device_SN(str.data[1]);  //Write the Device Serial Number to Flash
			fprintf(USART1_STREAM,">> Set Success.\r\n");
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"READPN",6)==0)
		{
			fprintf(USART1_STREAM,">>Read Part Number test.\r\n");
			char pn[DEV_PN_SIZE] = {0};
			Get_Device_PN(pn);
			fprintf(USART1_STREAM,">> PN:%s\r\n",pn);
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"READSN",6)==0)
		{
			fprintf(USART1_STREAM,">>Read Serial Number test.\r\n");
			char sn[DEV_SN_SIZE] = {0};
			Get_Device_SN(sn);
			fprintf(USART1_STREAM,">> SN:%s\r\n",sn);
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"BOTH",4)==0)
		{
			fprintf(USART1_STREAM,">>Write Serial Number and Part Number test.\r\n");
			fprintf(USART1_STREAM,">>The PartNum is [%s]\r\n",str.data[1]);
			fprintf(USART1_STREAM,">>The SerialNum is [%s]\r\n",str.data[2]);
			Set_Device_PN(str.data[1]);   //Write the Device Part Number to Flash
			Set_Device_SN(str.data[2]);  //Write the Device Serial Number to Flash
			fprintf(USART1_STREAM,">> Set Success.\r\n");
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"ALLREAD",7)==0)
		{
			char pn[DEV_PN_SIZE] = {0};
			char sn[DEV_SN_SIZE] = {0};
			Get_Device_PN(pn);
			Get_Device_SN(sn);
			fprintf(USART1_STREAM,">> PN:%s;SN:%s\r\n",pn,sn);
			result = TRUE;
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
	}
	Clear_Usart1_Buffer();
	return result;
}

/*****************************************************
 * 板卡PN/SN测试程序
 * 参数：读取写入PN/SN号码 ——
 * 	     ZKHYTEST*BOARD:WRITEPN,PN ——Part Number写入测试
 * 	     ZKHYTEST*BOARD:WRITESN,SN ——Serial Number写入测试
 * 	     ZKHYTEST*BOARD:READPN     ——Part Number读取测试
 * 	     ZKHYTEST*BOARD:READSN     ——Serial Number读取测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState Board_PNSNTest_Logic(char *strx)
{
	//definition
	uint8_t length = strlen("ZKHYTEST*BOARD:");
	FunctionalState result = FALSE;
	stringStrtok str = ChartoString(strx+length);
	if( str.cnt==2 || str.cnt==1 || str.cnt==3)
	{
		if(strncasecmp(str.data[0],"WRITEPN",7)==0)
		{
			//fprintf(USART1_STREAM,">>Write Board Part Number test.\r\n");
			fprintf(USART1_STREAM,">>The PartNum is [%s]\r\n",str.data[1]);
			result = Set_Board_PN(str.data[1]);   //Write the Board Part Number to Flash
			fprintf(USART1_STREAM,">> Set Board SN's result is %d.\r\n",result );
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"WRITESN",7)==0)
		{
			//fprintf(USART1_STREAM,">>Write Board Serial Number test.\r\n");
			fprintf(USART1_STREAM,">>The Board SerialNum is [%s]\r\n",str.data[1]);
			result = Set_Board_SN(str.data[1]);  //Write the Device Serial Number to Flash
			fprintf(USART1_STREAM,">> Set Board SN's result is %d.\r\n",result );
			//result = TRUE;
		}
		else if(strncasecmp(str.data[0],"READPN",6)==0)
		{
			//fprintf(USART1_STREAM,">>Read Board Part Number test.\r\n");
			char pn[BOARD_PN_SIZE] = {0};
			Get_Board_PN(pn);
			fprintf(USART1_STREAM,">> PN:%s\r\n",pn);
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"READSN",6)==0)
		{
			//fprintf(USART1_STREAM,">>Read Serial Number test.\r\n");
			char sn[BOARD_SN_SIZE] = {0};
			Get_Board_SN(sn);
			fprintf(USART1_STREAM,">> SN:%s\r\n",sn);
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"BOTH",4)==0)
		{
			//fprintf(USART1_STREAM,">>Write Board Serial Number and Part Number test.\r\n");
			//fprintf(USART1_STREAM,">>The Board PartNum is [%s]\r\n",str.data[1]);
			//fprintf(USART1_STREAM,">>The Board SerialNum is [%s]\r\n",str.data[2]);
			Set_Board_PN(str.data[1]);   //Write the Device Part Number to Flash
			Set_Board_SN(str.data[2]);  //Write the Device Serial Number to Flash
			fprintf(USART1_STREAM,">> Set Success.\r\n");
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"ALLREAD",7)==0)
		{
			char pn[DEV_PN_SIZE] = {0};
			char sn[DEV_SN_SIZE] = {0};
			Get_Board_PN(pn);
			Get_Board_SN(sn);
			fprintf(USART1_STREAM,">> PN:%s;SN:%s\r\n",pn,sn);
			result = TRUE;
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
	}
	Clear_Usart1_Buffer();
	return result;
}

/*****************************************************
 * 设备HW/SW VERSION测试程序
 * 参数：读取写入HW/SW版本号 ——
 * 	     ZKHYTEST*HWSW:WRITE,HW HWVersion写入测试
 * 	     ZKHYTEST*HWSW:WRITE,SW SW version写入测试
 * 	     ZKHYTEST*HWSW:READ,HW HWVersion读取测试
 * 	     ZKHYTEST*HWSW:READ,SW SW version读取测试
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState HWSWTest_Logic(char *strx)
{
	uint8_t length = strlen("ZKHYTEST*HWSW:");
	FunctionalState result = FALSE;
	stringStrtok str = ChartoString(strx+length);
	if( str.cnt <=3)
	{
		if(strncasecmp(str.data[0],"WRITEHW",7)==0)
		{
			//fprintf(USART1_STREAM,">>Write Hardware version test.\r\n");
			//fprintf(USART1_STREAM,">>The HW Version is [%s]\r\n",str.data[1]);
			if(Set_Hardware_Info(str.data[1])){
				fprintf(USART1_STREAM, ">> Set Success.\r\n");
			}
			else
			{
				fprintf(USART1_STREAM, ">>[ERROR]Set Hardware Information Failed.\r\n");
			}
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"WRITESW",7)==0)
		{
			//fprintf(USART1_STREAM,">>Write Software Version test.\r\n");
			//fprintf(USART1_STREAM,">>The Software Version is [%s]\r\n",str.data[1]);
			Set_App_Run_Version(str.data[1]);    //Write the SW Version to Flash
			fprintf(USART1_STREAM,">> Set Success.\r\n");
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"READHW",6)==0)
		{
			//fprintf(USART1_STREAM,">>Read Hardware version test.\r\n");
			uint8_t hardware_info[HARDWARE_INFO_SIZE] = {0};
			if(Get_Hardware_Info(hardware_info)){
				fprintf(USART1_STREAM, ">> Hardware info:%s\r\n", hardware_info);
			}else{
				fprintf(USART1_STREAM, ">>[ERROR]Get Hardware Information Failed.\r\n");
			}
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"READSW",6)==0)
		{
			//fprintf(USART1_STREAM,">>Read Software Version test.\r\n");
			// 获取当前app版本号、MCU版本号
			uint8_t run_v[VERSION_SIZE]={0};
			Get_APP_Run_Version(run_v);
			fprintf(USART1_STREAM,">> SN:%s\r\n",run_v);
			result = TRUE;
		}
		else if(strncasecmp(str.data[0],"BOTH",4)==0)
		{
			//fprintf(USART1_STREAM,">>Write Hardware and software version test.\r\n");
			//fprintf(USART1_STREAM,">>The HW Version is [%s]\r\n",str.data[1]);
			if(Set_Hardware_Info(str.data[1])){
				fprintf(USART1_STREAM, ">> Set HW Success.\r\n");
			}
			else
			{
				fprintf(USART1_STREAM, ">>[ERROR]Set Hardware Information Failed.\r\n");
			}
			//fprintf(USART1_STREAM,">>The Software Version is [%s]\r\n",str.data[2]);
			Set_App_Run_Version(str.data[2]);    //Write the SW Version to Flash
			//fprintf(USART1_STREAM,">> Set SW Success.\r\n");
			result = TRUE;
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
	}
	Clear_Usart1_Buffer();
	return result;
}

/*****************************************************
 * CANFA重启测试程序
 * 参数：重新启动CANFA
 * 	     ZKHYTEST*CANFA:RESET,重启CANFA
 * 返回：测试失败:FALSE；
 * 		 测试成功:TRUE
 * 说明：
 *****************************************************/
FunctionalState CANFATest_Logic(char *strx)
{
	uint8_t length = strlen("ZKHYTEST*PROP:");
	FunctionalState result = FALSE;
	stringStrtok str = ChartoString(strx+length);
	if( str.cnt==1)
	{
		if(strncasecmp(str.data[0],"RESET",5)==0)
		{
			fprintf(USART1_STREAM,">>Reset the Proportional device!\r\n");
			Power_CANFA_Reset();
			result = TRUE;
		}
	}
	else
	{
		fprintf(USART1_STREAM,">> Error parameter %s input \r\n",str.data[0]);
	}
	Clear_Usart1_Buffer();
	return result;
}

/*
 * 按照,进行分割字符串
 * 参数：分割字符串
 * 返回：0失败，1成功
 * 说明： 1223,456,789,adc,456,85ad21,dfdsa00>
 */
stringStrtok ChartoString(uint8_t *string)
{
	stringStrtok stringStrtok;
	stringStrtok.cnt = 0;
	if(string == NULL) return stringStrtok;

	fprintf(USART1_STREAM,"ChartoString() the string is %s\r\n",string);

	uint8_t *p = strtok(string,",");
	while(p!=NULL){

		strcpy(stringStrtok.data[stringStrtok.cnt], p);
		stringStrtok.data[stringStrtok.cnt][strlen(stringStrtok.data[stringStrtok.cnt])] = '\0';
		p = strtok(NULL, ",");
		stringStrtok.cnt++;
	}
	// 去除解析最后一个字符串的'>'
	for(uint8_t i=0; i<strlen(stringStrtok.data[stringStrtok.cnt-1]); i++){
		if(stringStrtok.data[stringStrtok.cnt-1][i] == '>'){
			stringStrtok.data[stringStrtok.cnt-1][i] = '\0';
			break;
		}
	}

//	for(uint8_t i=0; i<stringStrtok.cnt;i++){
//		fprintf(USART1_STREAM,"ChartoString() foreach(%d)'s string is %s \r\n",i,stringStrtok.data[i]);
//	}

	return stringStrtok;
}

/*
 * 按照,进行分割字符串
 * 参数：分割字符串
 * 返回：0失败，1成功
 * 说明： 1223,456,789,adc,456,85ad21,dfdsa00
 */
stringStrtok ReplytoString(uint8_t *string)
{
	stringStrtok stringStrtok;
	stringStrtok.cnt = 0;
	if(string == NULL) return stringStrtok;

	//fprintf(USART1_STREAM,"ChartoString() the string is %s\r\n",string);

	uint8_t *p = strtok(string,",");
	//fprintf(USART1_STREAM,"First strtok is %s\r\n",p);
	while(p!=NULL)
	{
		strcpy(stringStrtok.data[stringStrtok.cnt], p);

		stringStrtok.data[stringStrtok.cnt][strlen(stringStrtok.data[stringStrtok.cnt])] = '\0';
		//fprintf(USART1_STREAM,"P[%d] is [%s],",stringStrtok.cnt, stringStrtok.data[stringStrtok.cnt]);
		p = strtok(NULL, ",");
		//fprintf(USART1_STREAM,"and [%d]'s strtok is [%s].\r\n",stringStrtok.cnt,p);
		stringStrtok.cnt++;
	}
	return stringStrtok;
}

void checkUsartReceiveData(USART_SFRmap* USARTx, uint8_t* Databuf, uint32_t length)
{
	fprintf(USART1_STREAM,">>>USART Receive BUffer Ready Flag is %d \r\n",USART_Get_Receive_BUFR_Ready_Flag(USARTx));
	while(USART_Get_Receive_BUFR_Ready_Flag(USARTx))
	{
		fprintf(USART1_STREAM,">>>USARTx Receive Data is %d \r\n",USART_ReceiveData(USARTx));
	}
	fprintf(USART1_STREAM,">>>USARTx Receive Data is %d \r\n",USART_ReceiveData(USARTx));
}

/*
 * 工装测试中在USART0 中断函数中（4G模块通讯）
 * 参数为已收到的以结束符结尾的字符串
 */
void Tooling_4G_Message_Handle(uint8_t *string)
{
	char * strx = NULL;
	bool copyFlag = FALSE;
//	fprintf(USART1_STREAM, "The Original The String is %s\r\n", string);
	if(strstr((const char*)string, (const char*)"+QPING:")
			||strstr((const char*)string, (const char*)"+QSIMSTAT:")
			||strstr((const char*)string, (const char*)"+QGPSLOC:")
			||strstr((const char*)string, (const char*)"+CSQ:")
			||strstr((const char*)string, (const char*)"+COPS:"))
	{

		fprintf(USART1_STREAM, "The Original string is %s\r\n", string);
		copyFlag = TRUE;
	}

	if(copyFlag==TRUE)
	{
		//copy the 4G message to process
		for(int i=0;i<strlen(string);i++)
		{
			EC200U_Rxbuffer[i] = string[i];
		}
	}
	//return true;
}

/*
 * 清空USART0缓冲区
 * 返回：无
 */
void Clear_Buffer(void)
{
	memset(EC200U_Rxbuffer,0,1024);
	fprintf(USART1_STREAM, "After memset() the string is %s\r\n", EC200U_Rxbuffer);
    //EC200U_Rxcount=0;
}
