/*
 * _4g_data_upload.c
 *
 *  Created on: 2021-12-13
 *      Author: Administrator
 */

#include "_4g_data_upload.h"
#include "usart.h"
#include "stdlib.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
#include "common.h"
#include "gpio.h"
#include "EC200U.h"
#include "math.h"
#include <stdbool.h>
#include "at_parse_alg_para.h"
#include <malloc.h>
#include "modbus_crc16.h"
#include "_4g_server.h"
#include "AEB_upload_info.h"
#include "data_loss_prevention.h"
#include "aeb_sensor_management.h"
#include "car_dvr.h"

typedef struct _Accumulate_Meliage
{
	volatile float speed_last;
	volatile float total_mileage_1_min;	// 总计里程数 KM/H
	volatile uint8_t loop_times;
}Accumulate_Meliage;
/****************************** 全局变量 ******************************/
volatile uint8_t _4g_message_state 	= _4G_WAIT;
volatile uint8_t _4g_gps_state		= _4G_WAIT;

// 数据上传
Warning_Event_S 	fcw 	= {0};
Warning_Event_S 	hmw_L1 	= {0};
Warning_Event_S 	hmw_L2 	= {0};
Warning_Event_S 	ldw 	= {0};
Warning_Event_S 	aeb_CAM = {0};
Warning_Event_S 	aeb_MMW = {0};
Warning_Event_S 	aeb_ULT = {0};
Warning_Event_S		driver_brake = {0};

Warning_Event_S 	aeb_bsd 	= {0};
Warning_Event_S 	aeb_bsd_L1 	= {0};
Warning_Event_S 	aeb_bsd_L2 	= {0};
Warning_Event_S 	aeb_bsd_L3 	= {0};

// 测试变量
uint8_t vBaseInfo_trig = 0;

// GPS
GPS_INFO	gps_info					= {0};
bool 		update_utctime 				= false;
uint8_t 	gps_dispose_buf[90] 		= {0};
volatile uint8_t gps_Interval_coef 		= 0;		// GPS 发送平台的时间的间隔：gps_Interval_coef = gps_interval * 1000 / 500
// 4G信号强度
_4G_Signal_Strength_S signal_strength 	= {0};
// 获取车行驶里程数
Accumulate_Meliage g_acc_meilage 		= {0};

volatile uint16_t upload_vehicleBase_seqNum = 1;	// 范围 [1, 60000]
volatile uint16_t upload_vehicleBody_seqNum = 1;
volatile uint16_t upload_warningInfo_seqNum = 1;
volatile uint16_t upload_gpsInfo_seqNum 	= 1;
volatile uint16_t upload_heartBeat_seqNum 	= 1;
volatile uint16_t Upload_gpsInterval_seqNum = 1;

/****************************** 全局函数声明 ******************************/
// GPS
void 	Enable_GNSS();
void 	Disable_GNSS();
void    Send_Config_AGPS();
void    Send_Enable_AGPS();
void    Send_Enable_GNSS();

void 	Send_Gps_Type_Cmd();
extern float 	GPS_Format_To_Degree(uint8_t *dms);
bool 	GPSLOC_Logic(char *gps_string);

// 4G signal intensity
void 	Send_4G_Signal_Strength();

/****************************数据上传****************************************/
bool Request_GPS_Info_ASCII(uint16_t seqNum);
bool Request_VehicleBody_Info_ASCII(uint16_t seqNum, AEB_UPLOAD_INFO *p);
bool Request_VehicleBase_Info_ASCII(uint16_t seqNum, AEB_UPLOAD_INFO *p);
bool Request_Warning_Info_ASCII(uint16_t seqNum, AEB_UPLOAD_INFO *p);
bool Request_Heartbeat_ASCII(uint16_t seqNum);
void Upload_Warn_Condition_ASCII(uint8_t condition, Warning_Event_S *warn_s, TRIGGER_EVENT event);

/*****************************GPS****************************************/
/*
 * 获取GPS的信息
 */
GPS_INFO Get_GPS_Info()
{
	return gps_info;
}
/*
 * 配置AGPS
 */
void Config_AGPS()
{
	gps_info.step 	= CONFIG_AGPS;
	Send_Single_Data_By_Queue("AT+QAGPSCFG=1,\"http://quectel-api1.rx-networks.cn/rxn-api/locationApi/rtcm\",\"wLgWwv6JQt\",\"Quectel\",\"aFltUERDZzZxeTY5cEp2eA==\"");
//	Send_Single_Data_By_Queue("AT+QAGPSCFG=1,\"http://221.176.0.55\",\"\",\"\",\"\"");
//	Send_Single_Data_By_Queue("AT+QAGPSCFG=1,\"http://supl.qxwz.com/\",,,");//备用的域名
//	Send_Single_Data_By_Queue("AT+QAGPSCFG=1,\"http://221.176.0.55/\",,,");//备用的域名
//	Send_Single_Data_By_Queue("AT+QAGPSCFG=1,\"http://supl.qxwz.com/\",\"wLgWwv6JQt","Quectel\",\"aFltUERDZzZxeTY5cEp2eA==\");
//	221.176.0.55
}
/*
 * 使能AGPS
 */
void Enable_AGPS()
{
	gps_info.step 	= ENABLE_AGPS;
	Send_Single_Data_By_Queue("AT+QAGPS=1");
}


/*
 * 使能GNSS
 */
void Enable_GNSS()
{
	gps_info.step 	= ENABLE_GNSS;
	Send_Single_Data_By_Queue("AT+QGPS=1");
}
/*
 * 发送配置AGPS
 */
void Send_Config_AGPS()
{
//	gps_info.step 	= CONFIG_AGPS;
	Send_Single_Data_By_Queue("AT+QAGPSCFG=1,\"http://quectel-api1.rx-networks.cn/rxn-api/locationApi/rtcm\",\"wLgWwv6JQt\",\"Quectel\",\"aFltUERDZzZxeTY5cEp2eA==\"");
}
/*
 * 发送使能AGPS
 */
void Send_Enable_AGPS()
{
//	gps_info.step 	= ENABLE_AGPS;
	Send_Single_Data_By_Queue("AT+QAGPS=1");
}


/*
 * 发送使能GNSS
 */
void Send_Enable_GNSS()
{
//	gps_info.step 	= ENABLE_GNSS;
	Send_Single_Data_By_Queue("AT+QGPS=1");
}


/*
 * 关闭使能GNSS
 */
void Disable_GNSS()
{
	gps_info.step	= DISABLE_GNSS;
	Send_Single_Data_By_Queue("AT+QGPSEND");
}

/*
 * 获取GPxxx语句
 * ddmm.mmmmN/S,dddmm.mmmmE/W
 */
void Send_Gps_Type_Cmd()
{
	gps_info.step = GET_QGPSLOC;
	Send_Single_Data_By_Queue("AT+QGPSLOC=0");
}
/*
 * GPS重启开始
 */
void GPS_Restart()
{
	_4g_gps_state 		= _4G_WAIT;
	gps_info.isConnect 	= 0;
	Disable_GNSS();
	USART1_Bebug_Print("Restart", "GPS Restart", 1);
}
/*
 * GPS开始
 */
void GPS_Start()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();

	if(m_sensor_mgt.m_gps.m_isOpen){
		_4g_gps_state 		= _4G_WAIT;
		gps_info.isConnect 	= 0;
	//	Enable_GNSS();
	    Config_AGPS();
	}
}

/*
 * 对手动输入GPS指令解析
 * 参数：
 * 返回：0无异常；1异常
 * 说明：
 */
char * g_strx = NULL;
uint8_t Analysis_USART1_Data_Upload_CMD()
{
	// 获取当前GPS的经纬度信息
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYCHK*GPSINFO")){
		GPS_INFO gpsInfo 		= Get_GPS_Info();
		uint8_t location[14] 	= {0};
		uint8_t gps_print[120] 	= {0};
		uint8_t ew_s[5] 		= {0};
		uint8_t ns_s[5] 		= {0};

		switch(gpsInfo.fs){
		case 1: strcpy(location, "单点定位"); break;
		case 2: strcpy(location, "2D定位"); break;
		case 3: strcpy(location, "3D定位"); break;
		case 4: strcpy(location, "RTK定位"); break;
		default: strcpy(location, "未定位"); break;
		}

		// add lmz 20221024
		if(strncmp(gpsInfo.ew, "W", 1) == 0){
			sprintf(ew_s, "西经");
		}else{
			sprintf(ew_s, "东经");		// 默认 东经
		}
		if(strncmp(gpsInfo.ns, "S", 1) == 0){
			sprintf(ns_s, "南纬");
		}else{
			sprintf(ns_s, "北纬");		// 默认 北纬
		}
		// update lmz 20221024
		sprintf(gps_print, "(%s:%0.6f, %s:%0.6f, %s, 卫星数:%d, 海拔:%0.1f, 速度(节):%0.2f, 速度:%0.2f, hdop:%0.2f, 航向角(正北):%0.2f)\r\n", ew_s, GPS_Format_To_Degree(gpsInfo.lon), \
				ns_s, GPS_Format_To_Degree(gpsInfo.lat), location, gpsInfo.sateNum, gpsInfo.altitude, gpsInfo.sog*1852.0/3600, gpsInfo.kph, gpsInfo.hdop, gpsInfo.cogt);
		USART1_Bebug_Print("GPS INFO", gps_print, 1);
	}else
	// 设置GPS上报时间间隔
	if((g_strx = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*GPSINVERVAL"))){
		StrtokStr str = StrtokString(g_strx + 20);
		g_strx = NULL;

		if(str.cnt == 1){
			 uint8_t gpsInterval = atoi(str.data[0]);

			if(gpsInterval >= 5 && gpsInterval <= 60){
				Set_GPS_Interval(gpsInterval);							// write

				gps_Interval_coef = gpsInterval * 1000 / GPS_TIME_INTERVAL;
				// 触发一次上报GPS时间间隔信息
				Request_Set_Gps_Interval_ASCII(gpsInterval);

				USART1_Bebug_Print_Num("[>>]Set GPS Interval", gpsInterval, 1, 1);
				return 0;
			}
		}

		USART1_Bebug_Print("ERROR", "Valid Range:[5, 60]", 1);
	}else
	// 获取4G信号强度
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYCHK*4GSTRENGTH")){
		USART1_Bebug_Print_Num("[>>]RSSI", signal_strength.rssi, 0, 1);
		USART1_Bebug_Print_Num("BER", signal_strength.ber, 1, 1);
	}else
	// 方便预警触发测试 <ZKHYSET*WARNING:0, 1>
	// 0FCW, 1HMW, 2LDW, 3AEB, 4刹车灯   |   1事件开始，0事件结束
	if((g_strx = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*WARNING"))){
		StrtokStr str = StrtokString(g_strx + 16);
		g_strx = NULL;

		if(str.cnt == 2){
			uint8_t event	= atoi(str.data[0]);
			uint8_t sw		= atoi(str.data[1]);
			USART1_Bebug_Print_Num("[DEBUG]1", event, 0, 1);
			USART1_Bebug_Print_Num("2", sw, 1, 1);
			switch(event){
			// 预警信息
			case 0:	g_CMS_ttc_warning 	= sw; break;			// FCW
			case 1:	g_CMS_hmw_warning 	= sw; break;			// hmw
			case 2:	camera_data.LeftLDW = sw; break;			// ldw，后期将算法程序移植过来要优化，displayer_show_info.LDW
			case 3:	Brake_state_share 	= sw; break;			// aeb
			// 车身信息
			case 4:	stVehicleParas.BrakeFlag = sw; break;		// 刹车灯
//			case 5:	stVehicleParas.LeftFlag = sw; break;		// 左右转向灯
			case 5:{
				if(sw){
					static uint16_t seq = 1;
					AEB_UPLOAD_INFO *p = NULL;
					Request_VehicleBody_Info_ASCII(seq, p);
					seq++;

					// 上报序号 范围 [1, 60000]
//					upload_vehicleBody_seqNum ++;
//					if(upload_vehicleBody_seqNum > UPLOAD_MAX_SEQ_NUM) upload_vehicleBody_seqNum = 1;
				}
			}
//			case 6: driver_brake_trig = sw; break;	// 模拟司机刹车
			}
		}
	}else
	// 车辆基本信息
	if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*VBASEINFO")){
		vBaseInfo_trig = 1;
	}else
	if(g_strx = strstr((const char*)User_Rxbuffer, (const char*)"ZKHYSET*MILEAGE")){
		StrtokStr str = StrtokString(g_strx + 16);
		g_strx = NULL;

		if(str.cnt == 1){
			float mileage	= atof(str.data[0]);
			if(!Set_Vehicle_Mileage(mileage)){
				USART1_Bebug_Print_Flt(">> Set Mileage Failed", mileage, 1, 1);
			}else{
				USART1_Bebug_Print_Flt(">> Set Mileage OK", mileage, 1, 1);
			}
		}
	}else if(strstr((const char*)User_Rxbuffer, (const char*)"ZKHYGET*MILEAGE")){
		USART1_Bebug_Print_Flt(">> Current Mileage", Get_Vehicle_Mileage(), 1, 1);
	}
}
/*
 * 解析GPS数据
 * 参数：无
 * 返回：
 * 说明：2s检索一次,会在OTA升级完成后，才执行
 */
volatile uint8_t fixed_times 	= 0;
volatile uint8_t disable_times 	= 0;
void Get_GPS_Info_In_2_Second()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_gps.m_isOpen) return ;

	if(upgrade_info.step == UPGRADE_SUB_DMN) return ;
	if(upgrade_info.step == UPGRADE_URL) return ;

	static uint32_t gps_clk = 0;
	if(SystemtimeClock - gps_clk < 2000) return ;	// 2s
	gps_clk = SystemtimeClock;

	switch(gps_info.step)
	{
	case CONFIG_AGPS:{
		// 回话正在进行，就发送获取命令
		if(_4g_gps_state == _4G_ERROR_504)
		{
			_4g_gps_state = _4G_WAIT;
			Send_Gps_Type_Cmd();
			return ;
		}

		if((_4g_gps_state == _4G_OK)||fixed_times == 3)
		{
			_4g_gps_state = _4G_WAIT;
			Enable_AGPS();
			fixed_times = 0;
		}else
		{
			Config_AGPS();
		}
		fixed_times++;
	}break;
	case ENABLE_AGPS:
	{
		// 回话正在进行，就发送获取命令
		if(_4g_gps_state == _4G_ERROR_504)
		{
			_4g_gps_state = _4G_WAIT;
			Send_Gps_Type_Cmd();
			return ;
		}

		if((_4g_gps_state == _4G_OK) || (fixed_times == 3))
		{
			_4g_gps_state = _4G_WAIT;
			Enable_GNSS();
			fixed_times =0;
		}
		else
		{
			Enable_AGPS();
		}
		fixed_times++;
	}break;
	case ENABLE_GNSS:{
		// 回话正在进行，就发送获取命令
		if(_4g_gps_state == _4G_ERROR_504){
			_4g_gps_state = _4G_WAIT;
			Send_Gps_Type_Cmd();
			return ;
		}

		if(_4g_gps_state == _4G_OK){
			_4g_gps_state = _4G_WAIT;
			Send_Gps_Type_Cmd();
		}else
			Enable_GNSS();
	}break;
	case GET_QGPSLOC:{
		// 会话进行中
		if(_4g_gps_state == _4G_ERROR_504){
			_4g_gps_state = _4G_WAIT;
			Send_Gps_Type_Cmd();
			return ;
		}

		// 会话未激活
		if(_4g_gps_state == _4G_ERROR_505){
			_4g_gps_state = _4G_WAIT;
			Enable_GNSS();
			return ;
		}
		// 连续收到_4G_ERROR_50 和 _4G_ERROR_516的次数超出20次，就重启GPS
		if(_4g_gps_state == _4G_ERROR_50 || _4g_gps_state == _4G_ERROR_516){
			_4g_gps_state = _4G_WAIT;

			USART1_Bebug_Print("GPS", "GPS No Fixed.", 1);

			if(fixed_times > 19){
				fixed_times = 0;
				Disable_GNSS();
				return ;
			}

			fixed_times++;

			Send_Gps_Type_Cmd();
			return ;
		}

		fixed_times = 0;

		Send_Gps_Type_Cmd();
	}break;
	case DISABLE_GNSS:{
		gps_info.isConnect = 0;
		// 会话未激活
		if(_4g_gps_state == _4G_ERROR_505){
			_4g_gps_state = _4G_WAIT;
			Enable_GNSS();
			return ;
		}
		// 接收到OK，就使能；否则连续6次也使能
		if(_4g_gps_state == _4G_OK){
			_4g_gps_state = _4G_WAIT;
			Enable_GNSS();
		}else{
			disable_times++;
			if(disable_times > 5){
				disable_times = 0;
				Enable_GNSS();
			}else{
				disable_times++;
				Disable_GNSS();
			}
		}
	}break;
	default:break;
	}
}

/*
 * 获取VTG逻辑
 * 参数：接收字符串
 * 返回：FALSE失败；TRUE成功
 * 说明：+QGPSLOC: 070807.000,3858.1866N,11713.5805E,2.0,2.4,3,000.00,0.7,0.4,301021,07
 */
bool GPSLOC_Logic(char *gps_string)
{
	uint8_t len = strlen(gps_string);

	// 判断逗号(,)个数10，和点(.)个数8
	uint8_t comma_num 	= 0;
	uint8_t dot_num 	= 0;
	for(uint8_t i=0; i<len; i++){
		if(gps_string[i] == '.'){
			dot_num++;
		}else if(gps_string[i] == ','){
			comma_num++;
		}
	}

	if(len >= 78 && dot_num >= 8 && comma_num >= 10){
		// 截取$GPGGA字符串
		// +QGPSLOC: 070807.000, 3858.1866N, 11713.5805E, 2.0, 2.4, 3, 000.00, 0.7, 0.4, 301021, 07
		// +QGPSLOC: <UTC>, <latitude>, <longitude>, <HDOP>, <altitude>, <fix>, <COG>, <spkm>, <1>, <date>, <nsat>
		//  		  0		1   		2			3		4		 5		6 	  7		8		9		10
		uint8_t string[85] = {0};
		for(uint8_t i=0; i<strlen(gps_string)-10; i++){
			if(gps_string[9+i] != '\r')			// 去掉尾巴
				string[i] = gps_string[9+i];
			else{
				string[i] = '\0';
				break;
			}
		}
		// 解析
		uint8_t pos = 0;
		uint8_t *p = strtok(string,  ",");
		while(p)
		{
			switch(pos){
			case 0:	{	// UTC，引自GPGGA
				TrimLeftBlank(p);//gcz 2022-06-06 增加字符去掉多余空格的问题，原有程序未做此步骤，会导致解析异常值
				strcpy(gps_info.hms, p);
				gps_info.hms[strlen(gps_info.hms)-4] = '\0';
			}break;
			case 1:{		// 纬度+N
				strncpy(gps_info.lat, p, strlen(p)-1);
				gps_info.lat[strlen(gps_info.lat)] = '\0';
				gps_info.ns[0] = p[strlen(p)-1];
				gps_info.ns[1] = '\0';
			}break;
			case 2:{		// 经度+E
				strncpy(gps_info.lon, p, strlen(p)-1);
				gps_info.lon[strlen(gps_info.lon)] = '\0';
				gps_info.ew[0] = p[strlen(p)-1];
				gps_info.ew[1] = '\0';
			}break;
			case 3:		// HDOP
				gps_info.hdop = atof(p);
				break;
			case 4:		// 海拔
				gps_info.altitude = atof(p);
				break;
			case 5:		// fix
				gps_info.fs = atoi(p);
				break;
			case 6:		// COG 对地航向角，以正北方位对地航向
				gps_info.cogt = atof(p);
				break;
			case 7:		// spkm，对地速度。精确到小数点后一位。单位：千米/时（引自GPVTG语句）。
				gps_info.kph = atof(p);
				break;
			case 8:		// spkn，对地速度。精确到小数点后一位。单位：节（引自GPVTG语句）。
				gps_info.sog = atof(p);
				break;
			case 9:{		// date，ddmmyy, 引自GPRMC
				strcpy(gps_info.dmy, p);
				gps_info.dmy[strlen(gps_info.dmy)] = '\0';
			}break;
			case 10:	// nsat, 卫星数量。固定两位数，前导位数不足则补0（引自GPGGA语句）
				gps_info.sateNum = atoi(p);
				break;
			default:break;
			}
			p = strtok(NULL,  ",");
			pos++;
		}

//		strcpy(gps_info.head, "GPSLOC");
		pos = 0;
		gps_info.isConnect = 1;
		// 更新UTC时间
		if(!update_utctime){
			if(GPS_Time_To_UTCTIME(gps_info.dmy,  gps_info.hms,  gps_info.lon,  gps_info.ew)){
				// 需要若GPS的时间，小于UTC的时间，就不更新啦
				update_utctime = true;
			}else{
				update_utctime = false;
			}
		}
//		uint8_t print_info[100] = {0};
//		sprintf(print_info, "dmy:%s, hms:%s\r\n",gps_info.dmy, gps_info.hms);
//		Usart1_DMA_Transmit(print_info, strlen(print_info));
//
//		sprintf(print_info, "lat:%s, ns:%s, lon:%s, ew:%s\r\n",gps_info.lat, gps_info.ns, gps_info.lon, gps_info.ew);
//		Usart1_DMA_Transmit(print_info, strlen(print_info));
//
//		USART1_Bebug_Print_Num("fs", gps_info.fs, 1, 1);
//		USART1_Bebug_Print_Num("sateNum", gps_info.sateNum, 1, 1);
//
//		USART1_Bebug_Print_Flt("altitude", gps_info.altitude, 3, 1);
//		USART1_Bebug_Print_Flt("kph", gps_info.kph, 1, 1);
//		USART1_Bebug_Print_Flt("sog", gps_info.sog, 1, 1);
//
//		USART1_Bebug_Print_Flt("hdop", gps_info.hdop, 1, 1);
//		USART1_Bebug_Print_Flt("cogt", gps_info.cogt, 3, 1);
		return true;
	}
	return false;
}
/*
 * GPS格式转换
 * 参数：度分秒参数
 * 说明：将GPS的 度分 格式转化成 度 格式
 * ddmm.mmmm	--->    dd.dddddd
 */
float GPS_Format_To_Degree(uint8_t *dms)
{
	// 拆分数据
	uint8_t dot_id = 0;
	for(uint8_t i=0;i<strlen(dms);i++){
		if(dms[i] == '.'){	// 找到小数点的下标ID
			dot_id = i;
			break;
		}
	}
	uint8_t deg_t[4] = {0};
	uint8_t min_t[10] = {0};

	switch(dot_id){
	case 3:			// 858.1730
		memcpy(deg_t,dms,1);
		deg_t[1] = '\0';
		break;
	case 4:			// 3858.1730
		memcpy(deg_t,dms,2);
		deg_t[2] = '\0';
		break;
	case 5:			// 11713.5696
		memcpy(deg_t,dms,3);
		deg_t[3] = '\0';
		break;
	}
	memcpy(min_t,dms+dot_id-2,strlen(dms) - dot_id + 2);
	min_t[strlen(dms) - dot_id + 2] = '\0';
	// 取数据
	int degree = atoi(deg_t);
	float minute = atof(min_t)/60;
	// 计算
	return (float)(degree + minute);
}
/////////////////////////////////////////////////////////////////////////////////////////////
// 获取4G信号强度
/////////////////////////////////////////////////////////////////////////////////////////////
void Send_4G_Signal_Strength()
{
	Send_Single_Data_By_Queue("AT+CSQ");
}
/*
 * 解析获取4G信号强度信息
 * 说明：+CSQ: 28,99
 */
bool _4G_Recv_Signal_Intensity_Message(uint8_t *string,uint32_t length)//gcz 2022-06-03 增加参数：数据长度的输入
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return false;

	if(upgrade_info.step == UPGRADE_SUB_DMN) return true;
	if(upgrade_info.step == UPGRADE_URL) return true;
	//gcz   2022-06-03增加格式校验严谨性检查
	char * str = strstr((const char*)string, (const char*)"+CSQ:");
	char * str2 = strstr((const char*)string, (const char*)",");
	char * str3 = strstr((const char*)string, (const char*)"\r\n");
	if ((str != NULL)
		&& (str2 != NULL)
		&& (str3 != NULL)
		&&((str3 > str2)
			&&(str2 > str))
		&& ((str3 - str + 2) <= length))
	{
		// 防止通过指令获取4G模块信息时，通过strtok执行时打破了字符串的完整性
		uint8_t rssi[4] = {0};
		uint8_t ber[4] = {0}, j = 0;
		bool flag = false;
		//gcz 2022-06-04 卡死点位修改，长度使用CSQ实际长度
		for(uint8_t i=6; i< (str3 - str + 2)/*strlen(str)*/; i++)
		{
			if(!flag){
				if(str[i] != ','){
					rssi[j] = str[i]; j++;
				}else if(str[i] == ','){
					rssi[j] = '\0'; j = 0;
					flag = true;
					signal_strength.rssi 	= atoi(rssi);
				}
			}else if(str[i] != '\r'){
				ber[j] = str[i]; j++;
			}else{
				ber[j] = '\0'; j = 0;
				signal_strength.ber 	= atoi(ber);
			}
		}

//		USART1_Bebug_Print_Num("[Signal]rssi", signal_strength.rssi, 0);
//		USART1_Bebug_Print_Num("ber", signal_strength.ber, 1);
		return true;
	}
	return false;
}
/*
 * 获取4G信号强度
 * 时间间隔为4秒
 */
void Get_4G_Signal_Strength_In_4_Second()
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_4g.m_isOpen) return ;

	if(upgrade_info.step == UPGRADE_SUB_DMN) return ;
	if(upgrade_info.step == UPGRADE_URL) return ;

	static uint32_t _4g_signal_sth_clk = 0;
	if(SystemtimeClock - _4g_signal_sth_clk < 4000) return ;	// 4s
	_4g_signal_sth_clk = SystemtimeClock;

	if(!signal_strength.isStart) return ;

	Send_4G_Signal_Strength();
}

/*
 * 获取累计里程数
 * 返回：所有累计里程数。单位KM/H
 */
float Get_Accumulated_Mileage()
{
	return g_acc_meilage.total_mileage_1_min;
}

/*
 * 计算车辆本次行驶的里程计
 * 1秒积分一次，算出距离，但是1分钟存储一次
 * 单位：KM/H
 */
void Accumulate_Car_Meliage_In_1_Second()
{
	static uint32_t meliage_clk = 0;
	if(SystemtimeClock - meliage_clk < 1000) return ;
	meliage_clk = SystemtimeClock;

	if((stVehicleParas.fVehicleSpeed > 0) && (g_acc_meilage.speed_last != stVehicleParas.fVehicleSpeed)){
		g_acc_meilage.speed_last = stVehicleParas.fVehicleSpeed;
		g_acc_meilage.total_mileage_1_min += g_acc_meilage.speed_last / 3600;	// s = v * second/3600; 单位KM
		// 存储
		if(g_acc_meilage.loop_times >= 59){
			g_acc_meilage.loop_times = 0;
			if(!Set_Vehicle_Mileage(g_acc_meilage.total_mileage_1_min)){
				USART1_Bebug_Print("ERROR", "Set Vehicle Mileage Failed.", 1);
			}
		}else g_acc_meilage.loop_times++;
	}
}

/*
 * 在USART0中断函数，接收GPS数据
 * 参数：接收到结束符的字符串
 */
bool _4G_Recv_GPS_Message(uint8_t *string,uint32_t length)//gcz 2022-06-03 增加参数：数据长度的输入
{
	Sensor_MGT m_sensor_mgt = Get_Sensor_MGT_Param();
	if(!m_sensor_mgt.m_gps.m_isOpen) return false;

	if(upgrade_info.step == UPGRADE_SUB_DMN) return false;
	if(upgrade_info.step == UPGRADE_URL) return false;

	switch(gps_info.step)
	{
        case CONFIG_AGPS:{
            if(strstr((const char*)string, (const char*)"OK")){
		//			USART1_Bebug_Print("ENABLE GPS", string);
                _4g_gps_state = _4G_OK;
                return true;
            }
        }
        case ENABLE_AGPS:{
            if(strstr((const char*)string, (const char*)"OK")){
		//			USART1_Bebug_Print("ENABLE GPS", string);
                _4g_gps_state = _4G_OK;
                return true;
            }
        }
	case ENABLE_GNSS:{
		// 回话正在进行，就发送获取命令
		if(strstr((const char*)string, (const char*)"ERROR: 504")){
//			USART1_Bebug_Print("ENABLE GPS", string);
			_4g_gps_state = _4G_ERROR_504;
			return true;
		}

		if(strstr((const char*)string, (const char*)"OK")){
//			USART1_Bebug_Print("ENABLE GPS", string);
			_4g_gps_state = _4G_OK;
			return true;
		}
	}break;
	case GET_QGPSLOC:{
		// 会话进行中
		if(strstr((const char*)string,  (const char*)"ERROR: 504")){
//			USART1_Bebug_Print("GET GPS", string);
			_4g_gps_state = _4G_ERROR_504;
			return true;
		}
		// 会话未激活
		if(strstr((const char*)string,  (const char*)"ERROR: 505")){
//			USART1_Bebug_Print("GET GPS", string);
			_4g_gps_state = _4G_ERROR_505;
			return true;
		}

		// 当前未定位
		if(strstr((const char*)string,  (const char*)"ERROR: 516")){
//			USART1_Bebug_Print("GET GPS", string);
			_4g_gps_state = _4G_ERROR_516;
			return true;
		}

		// 当前未定位
		if(strstr((const char*)string,  (const char*)"ERROR: 50")){
//			USART1_Bebug_Print("GET GPS", string);
			_4g_gps_state = _4G_ERROR_50;
			return true;
		}

		// 解析GPS信息
		char *strx = NULL;
		if((strx = strstr((const char*)string, (const char*)"+QGPSLOC:"))){
			//gcz 2022-06-03 当校验收到的格式异常时，借用_4G_ERROR_504状态，重新发送AT+QGPSLOC以获取GPS
			//+QGPSLOC: 112800.000,3909.9300N,11710.9163E,2.7,-17.4,3,000.00,1.7,0.9,280522,10\r\n
			char *str1 = strstr((const char*)string,"\r\n");
			char *str2 = FindIndexPosStrng(string,',',10);
			if ((str1 != NULL) && (str2 != NULL) && (str1 > str2))
			{
	//			_4g_gps_state = _4G_GPS_INFO;
				memset(gps_dispose_buf, 0, strlen(gps_dispose_buf));
				memcpy(gps_dispose_buf, strx, str1 - strx + 1);

				GPSLOC_Logic(gps_dispose_buf);				// 这一部分建议整体放到while(1)循环体中进行执行

				Enable_Usart_Interrupt(USART0_SFR, INT_USART0);	// 开中断  gcz 2022-06-03去除
			}
			else//格式错误，重新发送AT命令获取值
				_4g_gps_state = _4G_ERROR_504;

			return true;
		}
	}break;
	case DISABLE_GNSS:{
		// 会话未激活
		if(strstr((const char*)string,  (const char*)"ERROR: 505")){
//			USART1_Bebug_Print("DISABLE GPS", string);
			_4g_gps_state = _4G_ERROR_505;
			return true;
		}

		if(strstr((const char*)string, (const char*)"OK")){
//			USART1_Bebug_Print("DISABLE GPS", string);
			_4g_gps_state = _4G_OK;
			return true;
		}
	}break;
	default:break;
	}
	return false;
}

void EC200U_Data_Upload_Init()
{
	initQueue(&dataUp_q, 10);				// init data upload queue

	Data_Loss_Prevention_Init();

	gps_info.isConnect = 0;
	signal_strength.isStart = false;

	uint8_t gpsTime = Get_GPS_Interval();

	gps_Interval_coef = gpsTime * 1000 / GPS_TIME_INTERVAL;	// gps时间间隔默认30秒

	// 预警信息
	fcw.cnt = 0;
	fcw.isStart = false;

	hmw_L1.cnt = 0;
	hmw_L1.isStart = false;

	hmw_L2.cnt = 0;
	hmw_L2.isStart = false;

	ldw.cnt = 0;
	ldw.isStart = false;

	aeb_CAM.cnt = 0;
	aeb_CAM.isStart = false;

	aeb_MMW.cnt = 0;
	aeb_MMW.isStart = false;

	aeb_ULT.cnt = 0;
	aeb_ULT.isStart = false;

	// 获取里程数
//	if(!Set_Vehicle_Mileage(120.0f)){
//		USART1_Bebug_Print("[ERROR]","Set Vehicle Mileage Failed.");
//	}
	g_acc_meilage.speed_last = 0.0f;
	g_acc_meilage.loop_times = 0;
	g_acc_meilage.total_mileage_1_min = Get_Vehicle_Mileage();
	USART1_Bebug_Print_Flt("Toatal Mileage", g_acc_meilage.total_mileage_1_min, 1, 1);
}

// 数据上报-GPS信息(0xB3)
bool Request_GPS_Info_ASCII(uint16_t seqNum)
{
	is_reply = false;
	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(seqNum, GPS_FRAME, nodeData.data)) return false;

	req_index = 31;

	// 内容
	// 行程ID 20个字节  upgrade_p.info.itinerary_idc
	Set_Request_ASCII_Info_Template(&nodeData, server_info.itinerary_id, CHAR, 20);

	// 车速 stVehicleParas.fVehicleSpeed
	if(stVehicleParas.fVehicleSpeed > 250.0f) stVehicleParas.fVehicleSpeed = 250.0f;
	else if(stVehicleParas.fVehicleSpeed < 0.0f) stVehicleParas.fVehicleSpeed = 0.0f;
	char *pSpeed = (char*)&stVehicleParas.fVehicleSpeed;
	Set_Request_ASCII_Info_Template(&nodeData, pSpeed, FLOAT, 4);

	GPS_INFO gpsInfo = Get_GPS_Info();
	// GPS速度
	if(gpsInfo.kph > 250.0f) gpsInfo.kph = 250.0f;
	else if(gpsInfo.kph < 0.0f) gpsInfo.kph = 0.0f;
	char *gpsSpeed = (char*)&gpsInfo.kph;
	Set_Request_ASCII_Info_Template(&nodeData, gpsSpeed, FLOAT, 4);

	// 里程计
	float mileage = Get_Accumulated_Mileage();
	char *pMileage = (char*)&mileage;
	Set_Request_ASCII_Info_Template(&nodeData, pMileage, FLOAT, 4);

	// 经度
	float lon = GPS_Format_To_Degree(gpsInfo.lon);
	char *pLon = (char*)&lon;
	Set_Request_ASCII_Info_Template(&nodeData, pLon, FLOAT, 4);

	// 纬度
	float lat = GPS_Format_To_Degree(gpsInfo.lat);
	char *pLat = (char*)&lat;
	Set_Request_ASCII_Info_Template(&nodeData, pLat, FLOAT, 4);

	// 海拔高度
	char *alt = (char*)&(gpsInfo.altitude);
	Set_Request_ASCII_Info_Template(&nodeData, alt, FLOAT, 4);

	// 卫星数
	uint8_t sateNum = gpsInfo.sateNum;
	Set_Request_ASCII_Info_Template(&nodeData, &sateNum, UINT_8, 1);

	// 水平精度因子
	char *hdop = (char*)&(gpsInfo.hdop);
	Set_Request_ASCII_Info_Template(&nodeData, hdop, FLOAT, 4);

	// GPS定位模式【0：初始化】【1：单点定位】【2：2D定位】，【3：3D定位】,【4：RTK定位】
	uint8_t gpsMode = gpsInfo.fs & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &gpsMode, UINT_8, 1);

	// 对地航向角
	char *orientation = (char*)&(gpsInfo.cogt);
	Set_Request_ASCII_Info_Template(&nodeData, orientation, FLOAT, 4);

	// gps_time_ymd
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.dmy, CHAR, 6);

	// gps_time_hms
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.hms, CHAR, 6);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.hms, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(GPS_FRAME, seqNum);
	nodeData.is_wait_reply = NEED_WAIT_REPLY;
	nodeData.is_store = NEED_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

	return true;
}


// 数据上报-报警信息(0xB4)
/*
 * 填写预警信息
 * 参数1：记录上报多少条，范围[1, 60000]
 * 参数2：事件类型
 * 参数3：事件开始/结束标志位
 * 参数4：协助平台区分事件的开始及结束的编号
 */
bool Request_Warning_Info_ASCII(uint16_t seqNum, AEB_UPLOAD_INFO *p)
{
	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(seqNum, WARN_FRAME, nodeData.data)) return false;

	req_index = 31;

	// 行程ID 20个字节
	Set_Request_ASCII_Info_Template(&nodeData, server_info.itinerary_id, CHAR, 20);

	// 车速 stVehicleParas.fVehicleSpeed
	if(stVehicleParas.fVehicleSpeed > 250) stVehicleParas.fVehicleSpeed = 250.0f;
	else if(stVehicleParas.fVehicleSpeed < 0) stVehicleParas.fVehicleSpeed = 0.0f;
	char *pSpeed = (char*)&stVehicleParas.fVehicleSpeed;
	Set_Request_ASCII_Info_Template(&nodeData, pSpeed, FLOAT, 4);

	// ttc
//	if(CameraMessage.ttc > 6.3f) CameraMessage.ttc = 6.3f;
//	else if(CameraMessage.ttc < 0) CameraMessage.ttc = 0.0f;
//	char *pTtc = (char*)&CameraMessage.ttc;
	//gcz 2022-08-31 修改HMW未缓存上报导致与报警信息不同步
	char *pTtc = (char*)& p->_ST_road_info._F_TTC_time;
	Set_Request_ASCII_Info_Template(&nodeData, pTtc, FLOAT, 4);

	// hmw
//	if(camera_data.HMW> 6.3) camera_data.HMW = 6.3f;
//	else if(camera_data.HMW < 0) camera_data.HMW = 0.0f;
//	char *pHmw = (char*)&camera_data.HMW;
//	gcz 2022-08-31 修改HMW未缓存上报导致与报警信息不同步
	char *pHmw = (char*)& p->_ST_road_info._F_HMW_time;
	Set_Request_ASCII_Info_Template(&nodeData, pHmw, FLOAT, 4);

	// verticalDistance
	if(camera_share.ObsInormation.DistanceZ > 200.0f) camera_share.ObsInormation.DistanceZ = 200.0f;
	else if(camera_share.ObsInormation.DistanceZ < 0) camera_share.ObsInormation.DistanceZ = 0;
	char *pVerDis = (char*)&camera_share.ObsInormation.DistanceZ;
	Set_Request_ASCII_Info_Template(&nodeData, pVerDis, FLOAT, 4);

	// horizontalDistance
	uint16_t horizontalDistance = 0;
	if(camera_share.ObsInormation.DistanceX > 200.0f) camera_share.ObsInormation.DistanceX = 200.0f;
	else if(camera_share.ObsInormation.DistanceX < 0) camera_share.ObsInormation.DistanceX = 0.0f;
	horizontalDistance = (uint16_t)(camera_share.ObsInormation.DistanceX * 100);
//	USART1_Bebug_Print_Num("[horizontalDistance]uint16_t", horizontalDistance, 1);
	uint8_t *horin = (uint8_t *)&horizontalDistance;
	Set_Request_ASCII_Info_Template(&nodeData, horin, UINT_16, 2);

	// throttle
	Set_Request_ASCII_Info_Template(&nodeData, &stVehicleParas.ThrottleOpening, UINT_8, 1);

	// brake
	Set_Request_ASCII_Info_Template(&nodeData, &stVehicleParas.BrakeOpening, UINT_8, 1);

	// steeringWheel
	uint8_t *swa = (uint8_t *)&stVehicleParas.steer_wheel_angle;	//(uint8_t *)&stSWAParas.SWADegree; // update lmz 20221019
	Set_Request_ASCII_Info_Template(&nodeData, swa, UINT_16, 2);

	// steeringWheelStatus
	uint8_t direction = stVehicleParas.steer_wheel_direction & 0x01; //stSWAParas.Direction & 0x01;		// update lmz 20221019
//	USART1_Bebug_Print_Num("SteeringWheelStatus", direction, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &direction, UINT_8, 1);

	// leftLaneStyle
	uint8_t leftLaneStyle = camera_LDW_data.LeftLaneStyle &0x0F;
//	USART1_Bebug_Print_Num("leftLaneStyle", leftLaneStyle, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &leftLaneStyle, UINT_8, 1);

	// rightLaneStyle
	uint8_t rightLaneStyle = camera_LDW_data.RightLaneStyle &0x0F;
//	USART1_Bebug_Print_Num("rightLaneStyle", rightLaneStyle, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &rightLaneStyle, UINT_8, 1);

	// obstacleType
	uint8_t obstacleType = camera_share.ObsInormation.ObstacleType &0x0F;
	Set_Request_ASCII_Info_Template(&nodeData, &obstacleType, UINT_8, 1);

	// warningSide
	uint8_t warningSide = p->_ST_status._ST_AEB_status._U8_LDW_action & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &warningSide, UINT_8, 1);

	// 预警类型：【1：fcw】、【2：hmw】、【3：ldw】、【4：aeb】
	// aebsStatus[0无预警][1双目制动][2毫米波雷达制动][3超声波雷达制动][6角雷达制动]
	uint8_t warningType = p->_ST_status._ST_AEB_status._U8_event_type & 0x07; //& 0x07低三位

	if(warningType == 4){ // 只有在触发AEB预警时才上报制动类型
		uint8_t aebsStatus = p->_ST_status._ST_AEB_status._U8_AEB_brake_status & 0x07;
//		USART1_Bebug_Print_Num("aebsStatus", aebsStatus, 1);
		Set_Request_ASCII_Info_Template(&nodeData, &aebsStatus, UINT_8, 1);
	}else{
		uint8_t aebsStatus = 0;
		Set_Request_ASCII_Info_Template(&nodeData, &aebsStatus, UINT_8, 1);
	}

	// gearStatus
	uint8_t gearStatus = p->_ST_status._ST_vehicle_status._U8_gear_status & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &gearStatus, UINT_8, 1);

	// isBegin  【0：结束】、【1：开始】
	uint8_t isBegin = p->_ST_status._ST_AEB_status._U8_event_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &isBegin, UINT_8, 1);

	// warningType 触发事件 string【1：fcw】、【2：hmw】、【3：ldw】、【4：aeb】
//	uint8_t warningType = p->_ST_status._ST_AEB_status._U8_event_type & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &warningType, UINT_8, 1);

	// warningLevel 0：无预警；1：一级；2：二级，3:三级 
	uint8_t warningLevel = 0;
	if(p->_ST_status._ST_AEB_status._U8_event_type == 1)		// FCW 预警等级
		warningLevel = p->_ST_status._ST_AEB_status._U8_FCW_level;
	else if(p->_ST_status._ST_AEB_status._U8_event_type == 2)	// HMW 预警等级
		warningLevel = p->_ST_status._ST_AEB_status._U8_HMW_level;
	else if(p->_ST_status._ST_AEB_status._U8_event_type == 6)	// HMW 预警等级
		warningLevel = p->_ST_status._ST_AEB_status._U8_BSD_level;

	Set_Request_ASCII_Info_Template(&nodeData, &warningLevel, UINT_8, 1);

	// ultrasonicDistance
	if(stVehicleParas.Ultrasonicdistance > 5.0f) stVehicleParas.Ultrasonicdistance = 5.0f;
	else if(stVehicleParas.Ultrasonicdistance < 0.0f) stVehicleParas.Ultrasonicdistance = 0.0f;
	uint16_t Ultrasonicdistance = (uint16_t)(stVehicleParas.Ultrasonicdistance * 100);	// 20220909 lmz add 修复超声波雷达距离BUG
	uint8_t *ultra = (uint8_t *)&Ultrasonicdistance;
	Set_Request_ASCII_Info_Template(&nodeData, ultra, UINT_16, 2);

	// which_ultrasonic_brake 哪个超声波雷达制动，最右侧Bit为1号雷达，最左侧为32号雷达
	uint32_t which_ultrasonic_brake = 0;
	uint8_t ultr_id = p->_ST_brake._ST_ult_radar_brake_info._U8_ult_id;
	if(ultr_id == 0 || ultr_id > 32){
		which_ultrasonic_brake = 0;
	}else{
		which_ultrasonic_brake |= (1 << (ultr_id - 1));
	}
	uint8_t *temp = (uint8_t *)&which_ultrasonic_brake;
	Set_Request_ASCII_Info_Template(&nodeData, temp, UINT_32, 4);

	// acc_velocity  加速度单位m/s2 无效值0xFFFFFFFF
	float acc_velocity = 0.0f;
	uint8_t *velocity = (uint8_t *)&acc_velocity;
	Set_Request_ASCII_Info_Template(&nodeData, velocity, FLOAT, 4);

	// alarmID	报警id， 同一次报警的开始和结束 报警id应相同
	uint8_t alarmID = p->event_id;
	Set_Request_ASCII_Info_Template(&nodeData, &alarmID, UINT_8, 1);

	// mileage
	float mileage = Get_Accumulated_Mileage();
	char *pMileage = (char*)&mileage;
	Set_Request_ASCII_Info_Template(&nodeData, pMileage, FLOAT, 4);

	// GPS信息
	GPS_INFO gpsInfo = Get_GPS_Info();

	// GPS速度
	if(gpsInfo.kph > 250.0f) gpsInfo.kph = 250.0f;
	else if(gpsInfo.kph < 0.0f) gpsInfo.kph = 0.0f;
//	USART1_Bebug_Print_Num("gpsSpeed", gpsInfo.kph, 1);
	char *gpsSpeed = (char*)&gpsInfo.kph;
	Set_Request_ASCII_Info_Template(&nodeData, gpsSpeed, FLOAT, 4);

	// 经度
	float lon = GPS_Format_To_Degree(gpsInfo.lon);
	char *pLon = (char*)&lon;
	Set_Request_ASCII_Info_Template(&nodeData, pLon, FLOAT, 4);

	// 纬度
	float lat = GPS_Format_To_Degree(gpsInfo.lat);
	char *pLat = (char*)&lat;
	Set_Request_ASCII_Info_Template(&nodeData, pLat, FLOAT, 4);

	// 磁偏角
	char *orientation = (char*)&(gpsInfo.cogt);
	Set_Request_ASCII_Info_Template(&nodeData, orientation, FLOAT, 4);

	// gps_time_ymd
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.dmy, CHAR, 6);

	// gps_time_hms
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.hms, CHAR, 6);

	// camera_failure_id 相机类的故障，该项移到预警信息中进行上报，相机遮挡问题
	Set_Request_ASCII_Info_Template(&nodeData, &p->_ST_fault._ST_periph._U8_periphfault_camera, UINT_8, 1);

	//fprintf(USART1_STREAM, "_U8_periphfault_camera: %d\r\n",p->_ST_fault._ST_periph._U8_periphfault_camera);

	// time_ms,方便平台计算加速度
	uint8_t *time = (char*)&(SystemtimeClock);
	Set_Request_ASCII_Info_Template(&nodeData, time, UINT_32, 4);
	//bsd_warning_direction,角雷达报警方向

	uint8_t direction_bsd = (uint8_t)(p->_ST_status._ST_AEB_status._U8_BSD_warning_direction);

	Set_Request_ASCII_Info_Template(&nodeData, &direction_bsd, UINT_8, 1);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.hms, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(WARN_FRAME, seqNum);
	nodeData.is_wait_reply = NEED_WAIT_REPLY;
	nodeData.is_store = NEED_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);
//	fprintf(USART1_STREAM, "\r\n********warning***********start***********\r\n");
//	for(uint16_t i=0;i<nodeData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n********warning***********end***********\r\n");
//	fprintf(USART1_STREAM, "\r\n********direction_bsd*********%d\r\n",p->_ST_status._ST_AEB_status._U8_BSD_warning_direction);

	return true;
}

// 数据上报-设备车身信息(0xB5)
bool Request_VehicleBody_Info_ASCII(uint16_t seqNum, AEB_UPLOAD_INFO *p)
{
	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(seqNum, VBODY_FRAME, nodeData.data)) return false;

	req_index = 31;
	// 内容
	// 行程ID 20个字节
	Set_Request_ASCII_Info_Template(&nodeData, server_info.itinerary_id, CHAR, 20);

	//	verticalSpeed
	if(stVehicleParas.fVehicleSpeed > 250.0f) stVehicleParas.fVehicleSpeed = 250.0f;
	else if(stVehicleParas.fVehicleSpeed < 0.0f) stVehicleParas.fVehicleSpeed = 0.0f;
	char *verticalSpeed = (char*)&stVehicleParas.fVehicleSpeed;
	Set_Request_ASCII_Info_Template(&nodeData, verticalSpeed, FLOAT, 4);

	//	aebsOn
	uint8_t aebsOn = p->_ST_status._ST_AEB_status._U8_AEB_on_off_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &aebsOn, UINT_8, 1);

	// signalRightOn 右车道偏离
	uint8_t signalRightOn = stVehicleParas.RightFlagTemp & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &signalRightOn, UINT_8, 1);

	// signalLeftOn 左车道偏离
	uint8_t signalLeftOn = stVehicleParas.LeftFlagTemp & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &signalLeftOn, UINT_8, 1);

	// signalBreakOn 刹车灯状态
	uint8_t signalBreakOn = p->_ST_status._ST_vehicle_status._U8_brake_light_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &signalBreakOn, UINT_8, 1);

	// gearStatus
	uint8_t gearStatus = p->_ST_status._ST_vehicle_status._U8_gear_status & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &gearStatus, UINT_8, 1);

	// throttle
	Set_Request_ASCII_Info_Template(&nodeData, &stVehicleParas.ThrottleOpening, UINT_8, 1);

	// brake
	Set_Request_ASCII_Info_Template(&nodeData, &stVehicleParas.BrakeOpening, UINT_8, 1);

	// steeringWheel
	uint8_t *swa = (uint8_t *)&stVehicleParas.steer_wheel_angle;	//(uint8_t *)&stSWAParas.SWADegree; // update lmz 20221019
	Set_Request_ASCII_Info_Template(&nodeData, swa, UINT_16, 2);

	// steeringWheelStatus
	uint8_t direction = stVehicleParas.steer_wheel_direction & 0x01; //stSWAParas.Direction & 0x01;		// update lmz 20221019
	Set_Request_ASCII_Info_Template(&nodeData, &direction, UINT_8, 1);

	// mileage
	float mileage = Get_Accumulated_Mileage();
	char *pMileage = (char*)&mileage;
	Set_Request_ASCII_Info_Template(&nodeData, pMileage, FLOAT, 4);

	// GPS信息
	GPS_INFO gpsInfo = Get_GPS_Info();
	// GPS速度
	if(gpsInfo.kph > 250.0f) gpsInfo.kph = 250.0f;
	else if(gpsInfo.kph < 0.0f) gpsInfo.kph = 0.0f;
	char *gpsSpeed = (char*)&gpsInfo.kph;
	Set_Request_ASCII_Info_Template(&nodeData, gpsSpeed, FLOAT, 4);

	// 经度
	float lon = GPS_Format_To_Degree(gpsInfo.lon);
	char *pLon = (char*)&lon;
	Set_Request_ASCII_Info_Template(&nodeData, pLon, FLOAT, 4);

	// 纬度
	float lat = GPS_Format_To_Degree(gpsInfo.lat);
	char *pLat = (char*)&lat;
	Set_Request_ASCII_Info_Template(&nodeData, pLat, FLOAT, 4);

	// orientation 磁偏角
	char *pCog = (char*)&(gpsInfo.cogt);
	Set_Request_ASCII_Info_Template(&nodeData, pCog, FLOAT, 4);

	// gps_time_dmy
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.dmy, CHAR, 6);

	// gps_time_hms
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.hms, CHAR, 6);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.hms, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(VBODY_FRAME, seqNum);
	nodeData.is_wait_reply = NEED_WAIT_REPLY;
	nodeData.is_store = NEED_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

//	fprintf(USART1_STREAM, "\r\n********body***********start***********\r\n");
//	for(uint16_t i=0;i<nodeData.data_len; i++){
//		fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	}
//	fprintf(USART1_STREAM, "\r\n********body***********end***********\r\n");
	return true;
}

// 数据上报-车辆设备基本信息(0xB6)
bool Request_VehicleBase_Info_ASCII(uint16_t seqNum, AEB_UPLOAD_INFO *p)
{
	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(seqNum, VBASE_FRAMR, nodeData.data)) return false;

	req_index = 31;

	// 内容
	// 行程ID 20个字节  upgrade_p.info.itinerary_id
	Set_Request_ASCII_Info_Template(&nodeData, server_info.itinerary_id, CHAR, 20);

	// camera_failure_id 相机类的故障，该项移到预警信息中进行上报，相机遮挡问题
	Set_Request_ASCII_Info_Template(&nodeData, &p->_ST_fault._ST_periph._U8_periphfault_camera, UINT_8, 1);

	// proportion_failure_id
	uint8_t proportion_failure_id = p->_ST_fault._ST_periph._U8_periphfault_proporvalve & 0x0F;
	Set_Request_ASCII_Info_Template(&nodeData, &proportion_failure_id, UINT_8, 1);

	//	vehicle_speed_is_valid
	uint8_t vehicle_speed_is_valid = 1;
	Set_Request_ASCII_Info_Template(&nodeData, &vehicle_speed_is_valid, UINT_8, 1);

	//	AEB_switch_status
	uint8_t AEB_switch_status = p->_ST_status._ST_AEB_status._U8_AEB_on_off_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &AEB_switch_status, UINT_8, 1);

	//	LDW_switch_status
	uint8_t LDW_switch_status = p->_ST_status._ST_AEB_status._U8_LDW_on_off_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &LDW_switch_status, UINT_8, 1);

	//	AEB_switch_source
	uint8_t AEB_switch_source = p->_ST_status._ST_AEB_status._U8_AEB_switch_source & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &AEB_switch_source, UINT_8, 1);

	//	LDW_switch_source
	uint8_t LDW_switch_source = p->_ST_status._ST_AEB_status._U8_LDW_switch_source & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &LDW_switch_source, UINT_8, 1);

	//	camera_online_status
	uint8_t camera_online_status = p->_ST_status._ST_perph_status._ST_camre._U8_online_status & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &camera_online_status, UINT_8, 1);

	//	camera_external_env_brightness
	uint8_t camera_external_env_brightness = p->_ST_status._ST_perph_status._ST_camre._U8_ambientLuminance_status & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &camera_external_env_brightness, UINT_8, 1);

	//	proportion_online_status
	uint8_t proportion_online_status = p->_ST_status._ST_perph_status._ST_propovavle._U8_online_status & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &proportion_online_status, UINT_8, 1);

	//	ultrasonic_online_status
	uint8_t ultrasonic_online_status = p->_ST_status._ST_perph_status._ST_ult_radar._U8_online_status & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &ultrasonic_online_status, UINT_8, 1);

	//	milimeter_wave_online_status
	uint8_t milimeter_wave_online_status = p->_ST_status._ST_perph_status._ST_mmw_radar._U8_online_status & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &milimeter_wave_online_status, UINT_8, 1);

	//	screen_online_status
	uint8_t screen_online_status = p->_ST_status._ST_perph_status._ST_displayer._U8_online_status & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &screen_online_status, UINT_8, 1);

	//	_4G_online_status
	uint8_t _4G_online_status = p->_ST_status._ST_perph_status._ST_4g._U8_online_status & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &_4G_online_status, UINT_8, 1);

	//	GPS_online_status
	uint8_t GPS_online_status = p->_ST_status._ST_perph_status._ST_gps._U8_online_status & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &GPS_online_status, UINT_8, 1);

	//	BT_online_status
	uint8_t BT_online_status = p->_ST_status._ST_perph_status._ST_bt._U8_online_status & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &BT_online_status, UINT_8, 1);

	//	vehicle_body_failure_id
	uint8_t vehicle_body_failure_id = p->_ST_fault._ST_vehicle._U8_fault_vehicle & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &vehicle_body_failure_id, UINT_8, 1);

	//	controler_failure_status
	uint8_t controler_failure_status = p->_ST_fault._ST_vehicle._U8_fault_controller & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_failure_status, UINT_8, 1);

	//	controler_CAN0_status
	uint8_t controler_CAN0_status = p->_ST_fault._ST_can[0]._U8_fault_can_bus_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN0_status, UINT_8, 1);

	//	controler_CAN0_comm_error_status
	uint8_t controler_CAN0_comm_error_status = p->_ST_fault._ST_can[0]._U8_fault_can_err_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN0_comm_error_status, UINT_8, 1);

	//	controler_CAN0_comm_error_id
	uint8_t controler_CAN0_comm_error_id = p->_ST_fault._ST_can[0]._U8_fault_can_err_code & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN0_comm_error_id, UINT_8, 1);

	//	controler_CAN0_comm_error_point
	uint8_t controler_CAN0_comm_error_point = p->_ST_fault._ST_can[0]._U8_fault_can_err_direction & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN0_comm_error_point, UINT_8, 1);

	//	controler_CAN1_status
	uint8_t controler_CAN1_status = p->_ST_fault._ST_can[1]._U8_fault_can_bus_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN1_status, UINT_8, 1);

	//	controler_CAN1_comm_error_status
	uint8_t controler_CAN1_comm_error_status = p->_ST_fault._ST_can[1]._U8_fault_can_err_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN1_comm_error_status, UINT_8, 1);

	//	controler_CAN1_comm_error_id
	uint8_t controler_CAN1_comm_error_id = p->_ST_fault._ST_can[1]._U8_fault_can_err_code & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN1_comm_error_id, UINT_8, 1);

	//	controler_CAN1_comm_error_point
	uint8_t controler_CAN1_comm_error_point = p->_ST_fault._ST_can[1]._U8_fault_can_err_direction & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN1_comm_error_point, UINT_8, 1);

	//	controler_CAN2_status
	uint8_t controler_CAN2_status = p->_ST_fault._ST_can[2]._U8_fault_can_bus_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN2_status, UINT_8, 1);

	//	controler_CAN2_comm_error_status
	uint8_t controler_CAN2_comm_error_status = p->_ST_fault._ST_can[2]._U8_fault_can_err_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN2_comm_error_status, UINT_8, 1);

	//	controler_CAN2_comm_error_id
	uint8_t controler_CAN2_comm_error_id = p->_ST_fault._ST_can[2]._U8_fault_can_err_code & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN2_comm_error_id, UINT_8, 1);

	//	controler_CAN2_comm_error_point
	uint8_t controler_CAN2_comm_error_point = p->_ST_fault._ST_can[2]._U8_fault_can_err_direction & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN2_comm_error_point, UINT_8, 1);

	//	controler_CAN3_status
	uint8_t controler_CAN3_status = p->_ST_fault._ST_can[3]._U8_fault_can_bus_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN3_status, UINT_8, 1);

	//	controler_CAN3_comm_error_status
	uint8_t controler_CAN3_comm_error_status = p->_ST_fault._ST_can[3]._U8_fault_can_err_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN3_comm_error_status, UINT_8, 1);

	//	controler_CAN3_comm_error_id
	uint8_t controler_CAN3_comm_error_id = p->_ST_fault._ST_can[3]._U8_fault_can_err_code & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN3_comm_error_id, UINT_8, 1);

	//	controler_CAN3_comm_error_point
	uint8_t controler_CAN3_comm_error_point = p->_ST_fault._ST_can[3]._U8_fault_can_err_direction & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN3_comm_error_point, UINT_8, 1);

	//	controler_CAN4_status
	uint8_t controler_CAN4_status = p->_ST_fault._ST_can[4]._U8_fault_can_bus_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN4_status, UINT_8, 1);

	//	controler_CAN4_comm_error_status
	uint8_t controler_CAN4_comm_error_status = p->_ST_fault._ST_can[4]._U8_fault_can_err_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN4_comm_error_status, UINT_8, 1);

	//	controler_CAN4_comm_error_id
	uint8_t controler_CAN4_comm_error_id = p->_ST_fault._ST_can[4]._U8_fault_can_err_code & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN4_comm_error_id, UINT_8, 1);

	//	controler_CAN4_comm_error_point
	uint8_t controler_CAN4_comm_error_point = p->_ST_fault._ST_can[4]._U8_fault_can_err_direction & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN4_comm_error_point, UINT_8, 1);

	//	controler_CAN5_status
	uint8_t controler_CAN5_status = p->_ST_fault._ST_can[5]._U8_fault_can_bus_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN5_status, UINT_8, 1);

	//	controler_CAN5_comm_error_status
	uint8_t controler_CAN5_comm_error_status = p->_ST_fault._ST_can[5]._U8_fault_can_err_status & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN5_comm_error_status, UINT_8, 1);

	//	controler_CAN5_comm_error_id
	uint8_t controler_CAN5_comm_error_id = p->_ST_fault._ST_can[5]._U8_fault_can_err_code & 0x07;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN5_comm_error_id, UINT_8, 1);

	//	controler_CAN5_comm_error_point
	uint8_t controler_CAN5_comm_error_point = p->_ST_fault._ST_can[5]._U8_fault_can_err_direction & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &controler_CAN5_comm_error_point, UINT_8, 1);

	//	ultrasonic_failure_id,超声波故障码，目前仅有一个
	uint8_t ultrasonic_failure_id = p->_ST_fault._ST_periph._U8_periphfault_ult_radar[0] & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &ultrasonic_failure_id, UINT_8, 1);

	//	milimeter_wave_failure_id
	uint8_t milimeter_wave_failure_id = p->_ST_fault._ST_periph._U8_periphfault_MMW_radar & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &milimeter_wave_failure_id, UINT_8, 1);

	//	screen_failure_id
	uint8_t screen_failure_id = p->_ST_fault._ST_periph._U8_periphfault_displayer & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &screen_failure_id, UINT_8, 1);

	//	_4G_failure_id
	uint8_t _4G_failure_id = p->_ST_fault._ST_periph._U8_periphfault_4G_COMM & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &_4G_failure_id, UINT_8, 1);

	//	GPS_failure_id
	uint8_t GPS_failure_id = p->_ST_fault._ST_periph._U8_periphfault_GPS_aerial & 0x01;
	Set_Request_ASCII_Info_Template(&nodeData, &GPS_failure_id, UINT_8, 1);

	//	BT_failure_id
	uint8_t BT_failure_id = p->_ST_fault._ST_periph._U8_periphfault_BT & 0x03;
	Set_Request_ASCII_Info_Template(&nodeData, &BT_failure_id, UINT_8, 1);

	// mileage
	float mileage = Get_Accumulated_Mileage();
	char *pMileage = (char*)&mileage;
	Set_Request_ASCII_Info_Template(&nodeData, pMileage, FLOAT, 4);

	// verticalSpeed
	if(stVehicleParas.fVehicleSpeed > 250.0f) stVehicleParas.fVehicleSpeed = 250.0f;
	else if(stVehicleParas.fVehicleSpeed < 0.0f) stVehicleParas.fVehicleSpeed = 0.0f;
	char *verticalSpeed = (char*)&stVehicleParas.fVehicleSpeed;
	Set_Request_ASCII_Info_Template(&nodeData, verticalSpeed, FLOAT, 4);

	// GPS信息
	GPS_INFO gpsInfo = Get_GPS_Info();

	// GPS速度
	if(gpsInfo.kph > 250.0f) gpsInfo.kph = 250.0f;
	else if(gpsInfo.kph < 0.0f) gpsInfo.kph = 0.0f;
	char *gpsSpeed = (char*)&gpsInfo.kph;
	Set_Request_ASCII_Info_Template(&nodeData, gpsSpeed, FLOAT, 4);

	// 经度
	float lon = GPS_Format_To_Degree(gpsInfo.lon);
	char *pLon = (char*)&lon;
	Set_Request_ASCII_Info_Template(&nodeData, pLon, FLOAT, 4);

	// 纬度
	float lat = GPS_Format_To_Degree(gpsInfo.lat);
	char *pLat = (char*)&lat;
	Set_Request_ASCII_Info_Template(&nodeData, pLat, FLOAT, 4);

	// 磁偏角
	char *pCog = (char*)&(gpsInfo.cogt);
	Set_Request_ASCII_Info_Template(&nodeData, pCog, FLOAT, 4);

	// gps_time_ymd
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.dmy, CHAR, 6);

	// gps_time_hms
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.hms, CHAR, 6);

	// IMU 故障码	20220928 lmz add
	Set_Request_ASCII_Info_Template(&nodeData, &p->_ST_fault._ST_periph._U8_periphfault_IMU, UINT_8, 1);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, gpsInfo.hms, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(VBASE_FRAMR, seqNum);
	nodeData.is_wait_reply = NEED_WAIT_REPLY;
	nodeData.is_store = NEED_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

	return true;
}

bool Analysis_Request_Set_GPS_Interval_ASCII(uint8_t *data)
{
	if(data[0] >= 5 && data[0] <= 60){
		USART1_Bebug_Print_Num("[GPS]Interval", data[0], 1, 1);
		gps_Interval_coef = data[0] * 1000 / GPS_TIME_INTERVAL;

		// 存储
		if(!Set_GPS_Interval(data[0])){
			Set_GPS_Interval(data[0]);
		}

		return true;
	}
	USART1_Bebug_Print_Num("[GPS] Interval Value Out Of Range", data[0], 1, 1);

	return false;
}

bool Analysis_Reply_Set_GPS_Interval_ASCII(uint8_t *data, uint8_t dataLen)
{
	if(data[0] == 1){	// 0-失败；1-成功
		uint8_t gpsInterval = data[1];

		if(gpsInterval >= 5 && gpsInterval <= 60){
			USART1_Bebug_Print_Num("[GPS]Interval", gpsInterval, 1, 1);
			// 赋值全局变量
			uint8_t gpsTime = data[0];
			gps_Interval_coef = gpsInterval * 1000 / GPS_TIME_INTERVAL;

			// 存储
			if(!Set_GPS_Interval(gpsInterval)){
				Set_GPS_Interval(gpsInterval);
			}

			return true;
		}else{
			USART1_Bebug_Print("ERROR", "GPS Interval Value Out Of Range.", 1);
		}
	}else{
		USART1_Bebug_Print("ERROR", "Set GPS Interval Failed.", 1);
	}

	return false;
}

/*
 * 推送设置GPS时间间隔
 * 单位：秒，范围[5,60]
 */
bool Request_Set_Gps_Interval_ASCII(uint8_t interval)
{
	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(Upload_gpsInterval_seqNum, GPS_INTERVAL_FRAME, nodeData.data)) return false;

	if(Upload_gpsInterval_seqNum > UPLOAD_MAX_SEQ_NUM) Upload_gpsInterval_seqNum = 1;
	else Upload_gpsInterval_seqNum++;

	req_index = 31;

	// 时间间隔
	Set_Request_ASCII_Info_Template(&nodeData, &interval, UINT_8, 1);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, &interval, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(GPS_INTERVAL_FRAME, Upload_gpsInterval_seqNum);
	nodeData.is_wait_reply = NEED_WAIT_REPLY;
	nodeData.is_store = NEED_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

	return true;
}

/*
 * 心跳机制，时间间隔为5秒
 * 内容有：1、接收信号强度；2、信号误码率
 */
bool Request_Heartbeat_ASCII(uint16_t seqNum)
{
	// 填数
	NodeData_S nodeData = {0};
	if(!Request_Protocal_Head_ASCII(seqNum, HEARTBEAT_FRAME, nodeData.data)) return false;

	req_index = 31;

	// 内容
	Set_Request_ASCII_Info_Template(&nodeData, &signal_strength.rssi, UINT_8, 1);
	Set_Request_ASCII_Info_Template(&nodeData, &signal_strength.ber, UINT_8, 1);

	//校验
	Set_Request_ASCII_Info_Template(&nodeData, &signal_strength.ber, CRC16, 2);

	// 放到队列中进行发送
	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
	Test_Print_CMD_Seq(HEARTBEAT_FRAME, seqNum);
	nodeData.is_wait_reply = NO_WAIT_REPLY;
	nodeData.is_store = NOT_STORED;
	nodeData.is_single_cmd = DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

	server_info.offline_times ++;

	return true;
}


/*
 * 回复平台
 */
bool Send_Reply_Platform_ASCII(uint8_t cmdCode, uint16_t stateCode, uint32_t seqNum, uint8_t *data, uint16_t dataLen)
{
//	USART1_Bebug_Print_Num("[stateCode]", stateCode, 0, 1);
//	USART1_Bebug_Print_Num("[seqNum]", seqNum, 0, 1);
//	USART1_Bebug_Print_Num("[cmdCode]", cmdCode, 2, 1);

	NodeData_S nodeData = {0};
	if(!Reply_Protocal_Head_ASCII(seqNum, cmdCode, stateCode, nodeData.data)) return false;	// 31

	// 内容长度
	nodeData.data[31] = dataLen;
	nodeData.data[32] = dataLen>>8;

	// 内容
	for(uint8_t i=0; i<dataLen; i++){
		nodeData.data[33+i] = data[i];
	}

	uint16_t crc16_modbus = MODBUS_CRC16_v3(nodeData.data, 33+dataLen);
	nodeData.data[33+dataLen] = crc16_modbus;
	nodeData.data[34+dataLen] = crc16_modbus>>8;

	// 发数据
	nodeData.data[35+dataLen] = 0x0D;
	nodeData.data[36+dataLen] = 0x0A;
	nodeData.data_len = 37+dataLen;
//	 放到队列中进行发送
//	 fprintf(USART1_STREAM, "\r\n************55 AA send queue***********\r\n");
//	 for(uint16_t i=0; i<37+dataLen; i++){
//	 	fprintf(USART1_STREAM, "%02X ", nodeData.data[i]);
//	 }
//	 fprintf(USART1_STREAM, "\r\n************55 AA send queue***********\r\n");

	sprintf(nodeData.data_info, "AT+QISEND=%d,%d\r\n", CONNECT_ID, nodeData.data_len);
//	Test_Print_CMD_Seq(cmdCode, seqNum);
	nodeData.is_wait_reply 	= NEED_WAIT_REPLY_REPLY;
	nodeData.is_store 		= NEED_STORED;
	nodeData.is_single_cmd 	= DOUBLE_CMD;
	insertQueue(&dataUp_q, &nodeData);

	return true;
}

/*
 * 预警信息
 * 触发条件：FCW、HMW、LDW、AEB(双目、毫米波、超声波)
 */
void Collection_Warning_Info_ASCII(AEB_UPLOAD_INFO *p)		// 预警
{
//	if(!server_info.isConnect) return ;

	//gcz 2022-05-25  二进制协议接口函数使用参考WARNING_BODY_D内的example
	//gcz 2022-05-21 二进制协议版本上传，解耦合后函数接口用法
//	judgement_all_warning_event_is_need_to_be_insert();		//判断报警事件，并插入缓存
//	AEB_UPLOAD_INFO *p = other_user_get_upload_info();		//从缓存查询事件并导入接口数据结构对象中
	//根据返回获取到含有“事件的开始与结束”——p->_ST_status._ST_AEB_status._U8_event_status,p->_ST_status、“事件分类”——p->_ST_status._ST_AEB_status._U8_event_type、
	//以及原有的循环发送的ID——p->event_id
	if (p->_ST_status._ST_AEB_status._U8_event_type != 0)
	{

		//gcz  2022-06-04
//		uint8_t buffer[64];
//		memset(buffer,0,sizeof(buffer));
//		sprintf(buffer,"---------------[%d]-status: %s--event: %d ID:%d bsd_lvl:%d\r\n", p->event_id,
//				(p->_ST_status._ST_AEB_status._U8_event_status == 1) ? "begin" :"finish",
//				p->_ST_status._ST_AEB_status._U8_event_type,p->_ST_brake._ST_ult_radar_brake_info._U8_ult_id,
//				p->_ST_status._ST_AEB_status._U8_BSD_level);
//	//	USART1_Bebug_Print("WARNING", buffer,1);
//		fprintf(USART1_STREAM,buffer);
//		fprintf(USART1_STREAM,"_U8_AEB_brake_status %d\r\n",p->_ST_status._ST_AEB_status._U8_AEB_brake_status);



		Request_Warning_Info_ASCII(upload_warningInfo_seqNum, p);
		// 让记录仪拍照 AEB FCW HMW      和告警信息必须绑定，否则无法对应视频与告警信息

//		fprintf(USART1_STREAM, "upload_warningInfo_seqNum: %d\r\n",upload_warningInfo_seqNum);
//		fprintf(USART1_STREAM, "p->_ST_status._ST_AEB_status._U8_event_type: %d\r\n",p->_ST_status._ST_AEB_status._U8_event_type);
		if(g_sensor_mgt.m_car_dvr.m_isOpen != 0)
		{
			if (p->_ST_status._ST_AEB_status._U8_event_status== 1)
			{
				switch(p->_ST_status._ST_AEB_status._U8_event_type)
				{
					case 1:
					{
						if(car_dvr_param.warning_fcw)
						{
							Request_CARDVR_Take_Picture_D1_ASCII();
						}
					}break;
					case 2:
					{
						if(car_dvr_param.warning_hmw)
						{
							Request_CARDVR_Take_Picture_D1_ASCII();
						}
					}break;
					case 3:
					{
						if(car_dvr_param.warning_ldw)
						{
							Request_CARDVR_Take_Picture_D1_ASCII();
						}
					}break;
					case 4:
					{
						if(car_dvr_param.warning_aeb)
						{
							Request_CARDVR_Take_Picture_D1_ASCII();
						}
					}break;
				}
			}
		}
		// 上报序号 范围 [1, 60000]
		upload_warningInfo_seqNum++;
		if(upload_warningInfo_seqNum > UPLOAD_MAX_SEQ_NUM) upload_warningInfo_seqNum = 1;
	}
}

/*
 * 车身信息
 * 触发条件：打左右转向灯、司机踩下刹车
 */
void Collection_VehicleBody_Info_ASCII(AEB_UPLOAD_INFO *p)	// 车身信息
{
//	if(!server_info.isConnect) return ;

	//gcz 2022-05-25  二进制协议接口函数使用参考WARNING_BODY_D内的example
	if (judge_vehiclebody_trigger_action_does_it_happen() == TRUE)
	{
		Request_VehicleBody_Info_ASCII(upload_vehicleBody_seqNum, p);

		// 上报序号 范围 [1, 60000]
		upload_vehicleBody_seqNum ++;
		if(upload_vehicleBody_seqNum > UPLOAD_MAX_SEQ_NUM) upload_vehicleBody_seqNum = 1;
	}
}

void Collection_VehicleBase_Info_ASCII(AEB_UPLOAD_INFO *p)	// 车辆基本信息
{
	if(!server_info.isConnect) return ;

	if(vBaseInfo_trig){
		vBaseInfo_trig = 0;
	}else{
		static uint32_t collection_vehiclebase_clk = 0;
		if(SystemtimeClock - collection_vehiclebase_clk < 600000) return ;	// 扫描周期600000ms = 600S = 10min
		collection_vehiclebase_clk = SystemtimeClock;
	}

	Request_VehicleBase_Info_ASCII(upload_vehicleBase_seqNum, p);

	// 上报序号 范围 [1, 60000]
	upload_vehicleBase_seqNum ++;
	if(upload_vehicleBase_seqNum > UPLOAD_MAX_SEQ_NUM) upload_vehicleBase_seqNum = 1;
}

volatile uint8_t times_250ms_gps_cnt = 0;
void Collection_GPS_Info_ASCII()			// GPS信息
{
	if(!gps_info.isConnect) return;

	static uint32_t collection_gps_clk = 0;
	if(SystemtimeClock - collection_gps_clk < 250) return ;	// 扫描周期250ms
	collection_gps_clk = SystemtimeClock;

	times_250ms_gps_cnt++;

	if(times_250ms_gps_cnt % gps_Interval_coef == 0){	// 5S = 5000ms = 50*100ms
		times_250ms_gps_cnt = 0;
		Request_GPS_Info_ASCII(upload_gpsInfo_seqNum);

		// 上报序号 范围 [1, 60000]
		upload_gpsInfo_seqNum ++;
		if(upload_gpsInfo_seqNum > UPLOAD_MAX_SEQ_NUM) upload_gpsInfo_seqNum = 1;
	}
}

void Collection_HeartBeat_ASCII()			// 心跳信息
{
	if(!server_info.isConnect) return;
	if(!upgrade_info.isUpgradeOK) return;

	static uint32_t collection_heartBeat_clk = 0;
	if(SystemtimeClock - collection_heartBeat_clk < 30000) return ;	// 扫描周期10000ms = 30S
	collection_heartBeat_clk = SystemtimeClock;

	Request_Heartbeat_ASCII(upload_heartBeat_seqNum);

	// 上报序号 范围 [1, 60000]
	upload_heartBeat_seqNum ++;
	if(upload_heartBeat_seqNum > UPLOAD_MAX_SEQ_NUM) upload_heartBeat_seqNum = 1;
}





