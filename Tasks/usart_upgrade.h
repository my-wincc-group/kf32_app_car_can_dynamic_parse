/*
 * usart_upgrade.h
 *
 *  Created on: 2021-11-17
 *      Author: Administrator
 */

#ifndef USART_UPGRADE_H_
#define USART_UPGRADE_H_
#include "upgrade_common.h"
#include "w25qxx.h"
// #include "w25qxx_dma.h"
/*******************************宏定义**********************************************/
#define LINE_BUFFER_SIZE			4101			// (4096+5)		// 手动升级时，+4B校验+1B编号
/* -----------------------全局函数声明------------------------------- */
void	Line_Upgrade_Init();
void 	Line_Upgrade_Start();
void 	Line_Upgrade_End();

void 	Line_Load_Upgrade();
void 	Analysis_USART1_Config_CMD();
void 	Line_Upgrade_Write_Operation();
bool 	check_is_valid_uart_pkg(uint8_t *pkg,uint16_t length);
#endif /* USART_UPGRADE_H_ */
